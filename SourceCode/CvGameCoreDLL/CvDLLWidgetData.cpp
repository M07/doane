#include "CvGameCoreDLL.h"
#include "CvGameCoreUtils.h"
#include "CvCity.h"
#include "CvUnit.h"
#include "CvGlobals.h"
#include "CvGameAI.h"
#include "CvMap.h"
#include "CvPlot.h"
#include "CvTeamAI.h"
#include "CvPlayerAI.h"
#include "CvGameCoreUtils.h"
#include "CyArgsList.h"
#include "CvGameTextMgr.h"
#include "CvDLLInterfaceIFaceBase.h"
#include "CvDLLPythonIFaceBase.h"
#include "CvDLLEngineIFaceBase.h"
#include "CvDLLEventReporterIFaceBase.h"
#include "CvDLLWidgetData.h"
#include "CvPopupInfo.h"
#include "CvCityJob.h"
#include "CvTradeRoute.h"
#include "CvAgreement.h"
#include "CvTempUnit.h"
#include "FProfiler.h"

CvDLLWidgetData* CvDLLWidgetData::m_pInst = NULL;

CvDLLWidgetData& CvDLLWidgetData::getInstance()
{
	if (m_pInst == NULL)
	{
		m_pInst = new CvDLLWidgetData;
	}
	return *m_pInst;
}

void CvDLLWidgetData::freeInstance()
{
	delete m_pInst;
	m_pInst = NULL;
}

void CvDLLWidgetData::parseHelp(CvWStringBuffer &szBuffer, CvWidgetDataStruct &widgetDataStruct)
{
	switch (widgetDataStruct.m_eWidgetType)
	{
	case WIDGET_PLOT_LIST:
		parsePlotListHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PLOT_LIST_SHIFT:
		if (widgetDataStruct.m_iData1 != 0)
		{
			szBuffer.assign(gDLL->getText("TXT_KEY_MISC_CTRL_SHIFT", (GC.getDefineINT("MAX_PLOT_LIST_SIZE") - 1)));
		}
		break;

	case WIDGET_CITY_SCROLL:
		break;

	case WIDGET_LIBERATE_CITY:
		parseLiberateCityHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_CITY_NAME:
		parseCityNameHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_UNIT_NAME:
		szBuffer.append(gDLL->getText("TXT_KEY_CHANGE_NAME"));
		break;

	case WIDGET_CREATE_GROUP:
		szBuffer.append(gDLL->getText("TXT_KEY_WIDGET_CREATE_GROUP"));
		break;

	case WIDGET_DELETE_GROUP:
		szBuffer.append(gDLL->getText("TXT_KEY_WIDGET_DELETE_GROUP"));
		break;

	case WIDGET_TRAIN:
		parseTrainHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_CONSTRUCT:
		parseConstructHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_HURRY:
		parseHurryHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_MENU_ICON:
		szBuffer.append(gDLL->getText("TXT_KEY_MAIN_MENU"));
		break;

	case WIDGET_ACTION:
		parseActionHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_CITIZEN:
		parseCitizenHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_CONTACT_CIV:
		parseContactCivHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_SCORE_BREAKDOWN:
		parseScoreHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_CREATE_TRADE_ROUTE:
		szBuffer.append(gDLL->getText("TXT_KEY_CREATE_TRADE_ROUTE"));
		break;

	case WIDGET_EDIT_TRADE_ROUTE:
		szBuffer.append(gDLL->getText("TXT_KEY_EDIT_TRADE_ROUTE"));
		break;

	case WIDGET_YIELD_IMPORT_EXPORT:
		parseImportExportHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_HOSPITAL:
		parseHospitalHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_YIELD_PRODUCTION:
		parseYieldProductionHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_TAVERN:
		parseTavernHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_AGREEMENT_LIST:
		szBuffer.append(gDLL->getText("TXT_KEY_WIDGET_AGREEMENT_LIST"));
		break;

	case WIDGET_SEEDLING_LIST:
		szBuffer.append(gDLL->getText("TXT_KEY_WIDGET_SEEDLING_LIST"));
		break;

	case WIDGET_DESTRUCTION:
		parseDestructionHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_ARSENAL_MANAGEMENT:
		parseArsenalManagementHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_BUY_SHIP_USED:
		parseBuyShipUsedHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_BUY_NEW_SHIP:
		parseBuyNewShipHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_EJECT_CITIZEN:
		parseEjectCitizenHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_SHIP_CARGO:
	case WIDGET_NON_OPERATIONAL_SHIP:
		parseShipCargoUnitHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_GROUP_SHIP:
		parseGroupShipHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_ONE_SHIP:
		parseOneShipHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_TRADE_ROUTE_IMP_EXP_LIST:
		parseTradeRouteImpExpListHelp(widgetDataStruct, szBuffer);
		break;
	case WIDGET_TRADE_ROUTE_IMP_EXP_BOX:
		parseTradeRouteImpExpBoxHelp(widgetDataStruct, szBuffer);
		break;
	case WIDGET_TRADE_ROUTE_BEST_YIELD:
		parseTradeRouteBestYieldHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_DOCK:
	case WIDGET_ONLY_INFO_UNIT:
		parseEuropeUnitHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_NOTORIETY:
		parseNotorietyHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_CHANGE_SEAWAY_POSITION:
		parseChangeSeawayPositionHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_RECRUIT_CREW:
	case WIDGET_DO_PROPOSAL_TO_IMMIGRANT:
		parseEuropeRecruitCrewHelp(widgetDataStruct, szBuffer);
		break;
	case WIDGET_ENROL_UNIT_TYPE:
	case WIDGET_TIPS_ENROL_UNIT_TYPE:
		parseEuropeEnrolUnitTypeHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_TRANSFERT_EUROPE_TO_WAREHOUSE:
	case WIDGET_TRANSFERT_EUROPE_TO_SHIP:
		parseTransfertEuropeToWarehouseHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_RENAME_SHIP:
		parseRenameShipHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_SAIL:
		szBuffer.append(gDLL->getText("TXT_KEY_SAIL"));
		break;

	case WIDGET_GOTO_CITY:
		{
			CvCity* pCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(widgetDataStruct.m_iData1);
			if (pCity != NULL)
			{
				szBuffer.append(gDLL->getText("TXT_KEY_GO_TO_CITY", pCity->getNameKey()));
			}
		}
		break;

	case WIDGET_ASSIGN_CITIZEN_TO_PLOT:
	case WIDGET_YIELD_INFO:
		break;

	case WIDGET_ZOOM_CITY:
		szBuffer.append(gDLL->getText("TXT_KEY_ZOOM_CITY_HELP"));
		break;

	case WIDGET_END_TURN:
		szBuffer.append(gDLL->getText("TXT_KEY_WIDGET_END_TURN"));
		break;

	case WIDGET_AUTOMATE_CITIZENS:
		parseAutomateCitizensHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_AUTOMATE_PRODUCTION:
		parseAutomateProductionHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_EMPHASIZE:
		parseEmphasizeHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_CHOOSE_QUANTITY_TRADE_IN_CITY:
	case WIDGET_CHOOSE_QUANTITY_TRADE_IN_UNIT:
		parseChooseQuantityTradeHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_TRADE_ITEM:
		parseTradeItem(widgetDataStruct, szBuffer);
		break;

	case WIDGET_UNIT_MODEL:
		parseUnitModelHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_HELP_DEFENSE:
		parseCityDefenseHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_HELP_POPULATION:
		parsePopulationHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_HELP_REBEL:
		parseRebelHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_HELP_GREAT_GENERAL:
		parseGreatGeneralHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_HELP_SELECTED:
		parseSelectedHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_UNIT:
		parseUnitHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_PROFESSION:
		parseProfessionHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_BUILDING:
		parseBuildingHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_BACK:
		// parsePediaBack(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_FORWARD:
		// parsePediaForward(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_BONUS:
		parseBonusHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_MAIN:
		break;

	case WIDGET_PEDIA_JUMP_TO_PROMOTION:
	case WIDGET_HELP_PROMOTION:
		parsePromotionHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_HELP_UNIT_PROMOTION:
		parseUnitPromotionHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_HELP_YIELD:
	case WIDGET_LOAD_ALL_THE_YIELD_IN_GROUP:
		parseCityYieldHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_CHOOSE_EVENT:
		parseEventHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_IMPROVEMENT:
		parseImprovementHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_CIVIC:
		parseCivicHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_CIV:
		parseCivilizationHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_LEADER:
		parseLeaderHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_TERRAIN:
		parseTerrainHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_YIELDS:
		parseYieldHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_JUMP_TO_FEATURE:
		parseFeatureHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_DESCRIPTION:
		parseDescriptionHelp(widgetDataStruct, szBuffer, false);
		break;

	case WIDGET_CLOSE_SCREEN:
		//parseCloseScreenHelp(szBuffer);
		break;

	case WIDGET_DEAL_KILL:
		parseKillDealHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_PEDIA_DESCRIPTION_NO_HELP:
		//parseDescriptionHelp(widgetDataStruct, szBuffer, true);
		break;

	case WIDGET_MINIMAP_HIGHLIGHT:
		break;

	case WIDGET_PRODUCTION_MOD_HELP:
		parseProductionModHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_LEADERHEAD:
		parseLeaderheadHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_LEADERHEAD_KING:
		parseLeaderheadKingHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_LEADER_LINE:
		parseLeaderLineHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_CITY_UNIT_ASSIGN_PROFESSION:
		parseSpecialBuildingHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_MOVE_CARGO_TO_TRANSPORT:
		parseCityYieldHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_TRADE_YIELD_WITH_NATIF:
		parseTradeYieldWithNatifHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_TRADE_ROUTE_MOVE_TRANSPORT_TO_TRANSPORT:
		parseGroupShipHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_ADD_TRANSPORT_AREA_TO_TRADE_ROUTES:
		//parseCityYieldHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_CHOOSE_TRADE_ROUTE:
		parseChooseTradeRoute(widgetDataStruct, szBuffer);
		break;

	case WIDGET_ASSIGN_TRADE_ROUTE:
		parseAssignTradeRoute(widgetDataStruct, szBuffer);
		break;

	case WIDGET_RECEIVE_MOVE_CARGO_TO_TRANSPORT:
		parseReceiveMoveCargoToTransportHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_UNLOAD_NEW_CARGO:
		parseUnloadNewCargoHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_UNLOAD_YIELD:
		parseCityYieldHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_SEED_INFO:
		parseSeedInfoHelp(widgetDataStruct, szBuffer);
		break;

	case WIDGET_HELP_AGREEMENT:
		parseAgreementHelp(widgetDataStruct, szBuffer);
		break;
	case WIDGET_HELP_TRADE_ROUTES:
		parseTradeRoutesHelp(widgetDataStruct, szBuffer);
		break;
	case WIDGET_HELP_ROB_YIELD:
		parseRobYieldHelp(widgetDataStruct, szBuffer);
		break;
	case WIDGET_HELP_DESTINATION_CITY:
		parseDestinationCityHelp(widgetDataStruct, szBuffer);
		break;
	case WIDGET_PLAYER_HURRY:
		break;
	}
}

// Protected Functions...
bool CvDLLWidgetData::executeAction( CvWidgetDataStruct &widgetDataStruct )
{
	bool bHandled = false;			//	Right now general bHandled = false;  We can specific case this to true later.  Game will run with this = false;

	switch (widgetDataStruct.m_eWidgetType)
	{

	case WIDGET_PLOT_LIST:
		doPlotList(widgetDataStruct);
		break;

	case WIDGET_PLOT_LIST_SHIFT:
		if (widgetDataStruct.m_iData1 == 0)
		{
			gDLL->getInterfaceIFace()->toggleMultiRowPlotList();
		}
		else
		{
			gDLL->getInterfaceIFace()->changePlotListColumn(widgetDataStruct.m_iData1 * ((gDLL->ctrlKey()) ? (GC.getDefineINT("MAX_PLOT_LIST_SIZE") - 1) : 1));
		}
		break;

	case WIDGET_CITY_SCROLL:
		if ( widgetDataStruct.m_iData1 > 0 )
		{
			GC.getGameINLINE().doControl(CONTROL_NEXTCITY);
		}
		else
		{
			GC.getGameINLINE().doControl(CONTROL_PREVCITY);
		}
		break;

	case WIDGET_LIBERATE_CITY:
		doLiberateCity();
		break;

	case WIDGET_CITY_NAME:
		doRenameCity();
		break;

	case WIDGET_UNIT_NAME:
		doRenameUnit();
		break;

	case WIDGET_CREATE_GROUP:
		doCreateGroup();
		break;

	case WIDGET_DELETE_GROUP:
		doDeleteGroup();
		break;

	case WIDGET_TRAIN:
		doTrain(widgetDataStruct);
		break;

	case WIDGET_CONSTRUCT:
		doConstruct(widgetDataStruct);
		break;

	case WIDGET_HURRY:
		doHurry(widgetDataStruct);
		break;

	case WIDGET_MENU_ICON:
		doMenu();
		break;

	case WIDGET_ACTION:
		doAction(widgetDataStruct);
		break;

	case WIDGET_CITIZEN:
		if (widgetDataStruct.m_iData2 != -1)
		{
			doDoubleClickCitizen(widgetDataStruct);
		}
		break;

	case WIDGET_CONTACT_CIV:
		doContactCiv(widgetDataStruct);
		break;

	case WIDGET_END_TURN:
		GC.getGameINLINE().doControl(CONTROL_FORCEENDTURN);
		break;

	case WIDGET_AUTOMATE_CITIZENS:
		doAutomateCitizens(widgetDataStruct);
		break;

	case WIDGET_AUTOMATE_PRODUCTION:
		doAutomateProduction();
		break;

	case WIDGET_EMPHASIZE:
		doEmphasize(widgetDataStruct);
		break;

	case WIDGET_CHOOSE_QUANTITY_TRADE_IN_CITY:
		doChooseQuantityTradeInCity(widgetDataStruct);
		break;

	case WIDGET_CHOOSE_QUANTITY_TRADE_IN_UNIT:
		doChooseQuantityTradeInUnit(widgetDataStruct);
		break;

	case WIDGET_DIPLOMACY_RESPONSE:
		break;

	case WIDGET_TRADE_ITEM:
		break;

	case WIDGET_UNIT_MODEL:
		doUnitModel();
		break;

	case WIDGET_HELP_SELECTED:
		doSelected(widgetDataStruct);
		break;

	case WIDGET_PEDIA_JUMP_TO_UNIT:
		if (widgetDataStruct.m_iData2 != 1)
		{
			doPediaUnitJump(widgetDataStruct);
		}
		break;

	case WIDGET_PEDIA_JUMP_TO_PROFESSION:
		doPediaProfessionJump(widgetDataStruct);
		break;

	case WIDGET_PEDIA_JUMP_TO_BUILDING:
		doPediaBuildingJump(widgetDataStruct);
		break;
	case WIDGET_PEDIA_BACK:
		doPediaBack();
		break;
	case WIDGET_PEDIA_FORWARD:
		doPediaForward();
		break;

	case WIDGET_PEDIA_JUMP_TO_BONUS:
		doPediaBonusJump(widgetDataStruct);
		break;

	case WIDGET_PEDIA_MAIN:
		doPediaMain(widgetDataStruct);
		break;

	case WIDGET_PEDIA_JUMP_TO_PROMOTION:
		doPediaPromotionJump(widgetDataStruct);
		break;

	case WIDGET_PEDIA_JUMP_TO_IMPROVEMENT:
		doPediaImprovementJump(widgetDataStruct);
		break;

	case WIDGET_PEDIA_JUMP_TO_CIVIC:
		doPediaCivicJump(widgetDataStruct);
		break;

	case WIDGET_PEDIA_JUMP_TO_CIV:
		doPediaCivilizationJump(widgetDataStruct);
		break;

	case WIDGET_PEDIA_JUMP_TO_LEADER:
		doPediaLeaderJump(widgetDataStruct);
		break;

	case WIDGET_PEDIA_JUMP_TO_TERRAIN:
		doPediaTerrainJump(widgetDataStruct);
		break;

	case WIDGET_PEDIA_JUMP_TO_YIELDS:
		doPediaYieldJump(widgetDataStruct);
		break;

	case WIDGET_PEDIA_JUMP_TO_FEATURE:
		doPediaFeatureJump(widgetDataStruct);
		break;

	case WIDGET_PEDIA_DESCRIPTION:
	case WIDGET_PEDIA_DESCRIPTION_NO_HELP:
		doPediaDescription(widgetDataStruct);
		break;

	case WIDGET_TURN_EVENT:
		doGotoTurnEvent(widgetDataStruct);
		break;

	case WIDGET_DEAL_KILL:
		doDealKill(widgetDataStruct);
		break;

	case WIDGET_MINIMAP_HIGHLIGHT:
		doRefreshMilitaryAdvisor(widgetDataStruct);
		break;

	case WIDGET_UNLOAD_NEW_CARGO:
		doUnloadNewCargo(widgetDataStruct);
		break;

	case WIDGET_UNLOAD_YIELD:
		doUnloadYield(widgetDataStruct);
		break;

  	case WIDGET_CHOOSE_EVENT:
		break;

	case WIDGET_ZOOM_CITY:
		break;
	case WIDGET_HELP_YIELD:
	case WIDGET_SEED_INFO:
	case WIDGET_HELP_AGREEMENT:
	case WIDGET_HELP_TRADE_ROUTES:
	case WIDGET_HELP_ROB_YIELD:
	case WIDGET_HELP_DESTINATION_CITY:
	case WIDGET_HELP_PROMOTION:
	case WIDGET_HELP_UNIT_PROMOTION:
	case WIDGET_LEADERHEAD:
	case WIDGET_LEADERHEAD_KING:
	case WIDGET_LEADER_LINE:
	case WIDGET_CLOSE_SCREEN:
	case WIDGET_SCORE_BREAKDOWN:
	case WIDGET_YIELD_INFO:
	case WIDGET_ASSIGN_CITIZEN_TO_PLOT:
		//	Nothing on clicked
		break;
	case WIDGET_LOAD_ALL_THE_YIELD_IN_GROUP:
		doLoadAllTheYieldInGroup(widgetDataStruct);
		break;
	case WIDGET_CREATE_TRADE_ROUTE:
		doCreateTradeRoute(widgetDataStruct);
		break;
	case WIDGET_EDIT_TRADE_ROUTE:
		//doEditTradeRoute(widgetDataStruct);
		break;
	case WIDGET_YIELD_IMPORT_EXPORT:
		doYieldImportExport(widgetDataStruct);
		break;
	case WIDGET_HOSPITAL:
		doControlHospital(widgetDataStruct);
		break;
	case WIDGET_YIELD_PRODUCTION:
		doYieldProduction(widgetDataStruct);
		break;
	case WIDGET_TAVERN:
		doControlTavern(widgetDataStruct);
		break;
	case WIDGET_AGREEMENT_LIST:
		doAgreementList(widgetDataStruct);
		break;
	case WIDGET_SEEDLING_LIST:
		doSeedlingList(widgetDataStruct);
		break;
	case WIDGET_DESTRUCTION:
		doDestroyCity(widgetDataStruct);
		break;
	case WIDGET_ARSENAL_MANAGEMENT:
		doArsenalManagement(widgetDataStruct);
		break;
	case WIDGET_BUY_SHIP_USED:
		doBuyShipUsed(widgetDataStruct);
		break;
	case WIDGET_BUY_NEW_SHIP:
		doBuyNewShip(widgetDataStruct);
		break;
	case WIDGET_TRANSFERT_EUROPE_TO_WAREHOUSE:
		doTransfertEuropeToWarehouse(widgetDataStruct);
		break;
	case WIDGET_RECRUIT_CREW:
		doRecruitCrew(widgetDataStruct);
		break;
	case WIDGET_ENROL_UNIT_TYPE:
		doEnrolUnitType(widgetDataStruct, true);
		break;
	case WIDGET_TIPS_ENROL_UNIT_TYPE:
		doTipsEnrolUnitType(widgetDataStruct);
		break;
	case WIDGET_DO_PROPOSAL_TO_IMMIGRANT:
		doProposalToImmigrant(widgetDataStruct);
		break;
	case WIDGET_CHOOSE_TRADE_ROUTE:
		doChooseTradeRoute(widgetDataStruct);
		break;
	case WIDGET_ASSIGN_TRADE_ROUTE:
		doAssignTradeRoute(widgetDataStruct);
		break;
	case WIDGET_RENAME_SHIP:
		doClickRenameShip(widgetDataStruct);
		break;
	case WIDGET_CHANGE_SEAWAY_POSITION:
		doSendUpSeaway(widgetDataStruct);
		break;
	case WIDGET_TRADE_ROUTE_IMP_EXP_BOX:
		doAddNewResourceIntoTradeRoute(widgetDataStruct);
		break;
	case WIDGET_GOTO_CITY:
		doGoToCity(widgetDataStruct);
		break;
	case WIDGET_PLAYER_HURRY:
		break;
	}

	return bHandled;
}

//	right clicking action
bool CvDLLWidgetData::executeAltAction( CvWidgetDataStruct &widgetDataStruct )
{
	CvWidgetDataStruct widgetData = widgetDataStruct;

	bool bHandled = true;
	switch (widgetDataStruct.m_eWidgetType)
	{
	case WIDGET_TRAIN:
		doPediaTrainJump(widgetDataStruct);
		break;
	case WIDGET_CONSTRUCT:
		doPediaConstructJump(widgetDataStruct);
		break;
	case WIDGET_PEDIA_JUMP_TO_UNIT:
		doPediaUnitJump(widgetDataStruct);
		break;
	case WIDGET_PEDIA_JUMP_TO_PROFESSION:
		doPediaProfessionJump(widgetDataStruct);
		break;
	case WIDGET_PEDIA_JUMP_TO_BUILDING:
		doPediaBuildingJump(widgetDataStruct);
		break;
	case WIDGET_PEDIA_JUMP_TO_PROMOTION:
		doPediaPromotionJump(widgetDataStruct);
		break;
	case WIDGET_LEADERHEAD:
		doContactCiv(widgetDataStruct);
		break;
	case WIDGET_ENROL_UNIT_TYPE:
		doEnrolUnitType(widgetDataStruct, false);
		break;
	case WIDGET_CHANGE_SEAWAY_POSITION:
		doSendDownSeaway(widgetDataStruct);
		break;
	case WIDGET_DOCK:
		doSelectDestinationColony(widgetDataStruct);
		break;

	default:
		bHandled = false;
		break;
	}

	return (bHandled);
}

bool CvDLLWidgetData::executeDropOn(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData)
{
	bool bHandled = true;
	switch (sourceWidgetData.m_eWidgetType)
	{
	case WIDGET_CITIZEN:
		doCityUnitAssignCitizen(destinationWidgetData, sourceWidgetData);
		break;

	case WIDGET_MOVE_CARGO_TO_TRANSPORT:
		doMoveCargoToTransport(destinationWidgetData, sourceWidgetData);
		break;

	case WIDGET_MOVE_CARGO_TO_CITY:
		doMoveCargoToCity(destinationWidgetData, sourceWidgetData);
		break;

	case WIDGET_SHIP_CARGO:
		doMoveShipCargo(destinationWidgetData, sourceWidgetData);
		break;

	case WIDGET_GROUP_SHIP:
		doMoveShipToGroup(destinationWidgetData, sourceWidgetData);
		break;

	case WIDGET_ONE_SHIP:
		doMoveShipCargo(destinationWidgetData, sourceWidgetData);
		break;

	case WIDGET_DOCK:
		doMoveDockUnit(destinationWidgetData, sourceWidgetData);
		break;

	case WIDGET_TRADE_ROUTE_BEST_YIELD:
		doMoveYieldImportExport(destinationWidgetData, sourceWidgetData);
		break;

	case WIDGET_EJECT_CITIZEN:
		doUnitIntoCity(destinationWidgetData, sourceWidgetData);

	case WIDGET_TRADE_ROUTE_MOVE_TRANSPORT_TO_TRANSPORT:
		doTradeRouteMoveTransportToTransport(destinationWidgetData, sourceWidgetData);

	default:
		bHandled = false;
		break;
	}

	return (bHandled);
}

//	right clicking action
bool CvDLLWidgetData::executeDoubleClick(const CvWidgetDataStruct& widgetDataStruct)
{
	bool bHandled = true;
	switch (widgetDataStruct.m_eWidgetType)
	{
	case WIDGET_CITIZEN:
		doDoubleClickCitizen(widgetDataStruct);
		break;

	case WIDGET_DOCK:
		doDoubleClickDock(widgetDataStruct);
		break;

	default:
		bHandled = false;
		break;
	}

	return (bHandled);
}

bool CvDLLWidgetData::isLink(const CvWidgetDataStruct &widgetDataStruct) const
{
	bool bLink = false;
	switch (widgetDataStruct.m_eWidgetType)
	{
	case WIDGET_PEDIA_JUMP_TO_BUILDING:
	case WIDGET_PEDIA_JUMP_TO_UNIT:
	case WIDGET_PEDIA_JUMP_TO_PROFESSION:
	case WIDGET_PEDIA_JUMP_TO_PROMOTION:
	case WIDGET_PEDIA_JUMP_TO_BONUS:
	case WIDGET_PEDIA_JUMP_TO_IMPROVEMENT:
	case WIDGET_PEDIA_JUMP_TO_CIVIC:
	case WIDGET_PEDIA_JUMP_TO_CIV:
	case WIDGET_PEDIA_JUMP_TO_LEADER:
	case WIDGET_PEDIA_JUMP_TO_TERRAIN:
	case WIDGET_PEDIA_JUMP_TO_YIELDS:
	case WIDGET_PEDIA_JUMP_TO_FEATURE:
	case WIDGET_PEDIA_FORWARD:
	case WIDGET_PEDIA_BACK:
	case WIDGET_PEDIA_MAIN:
	case WIDGET_TURN_EVENT:
	case WIDGET_PEDIA_DESCRIPTION:
	case WIDGET_PEDIA_DESCRIPTION_NO_HELP:
	case WIDGET_MINIMAP_HIGHLIGHT:
		bLink = (widgetDataStruct.m_iData1 >= 0);
		break;
	case WIDGET_DEAL_KILL:
		{
			CvDeal* pDeal = GC.getGameINLINE().getDeal(widgetDataStruct.m_iData1);
			bLink = (NULL != pDeal && true);//pDeal->isCancelable(GC.getGameINLINE().getActivePlayer()));
		}
		break;
	case WIDGET_GENERAL:
		bLink = (1 == widgetDataStruct.m_iData1);
		break;
	}
	return (bLink);
}


void CvDLLWidgetData::doPlotList(CvWidgetDataStruct &widgetDataStruct)
{
	PROFILE_FUNC();

	CvUnit* pUnit;
	int iUnitIndex = widgetDataStruct.m_iData1 + gDLL->getInterfaceIFace()->getPlotListColumn() - gDLL->getInterfaceIFace()->getPlotListOffset();

	CvPlot *selectionPlot = gDLL->getInterfaceIFace()->getSelectionPlot();
	pUnit = gDLL->getInterfaceIFace()->getInterfacePlotUnit(selectionPlot, iUnitIndex);

	if (pUnit != NULL)
	{
		if (pUnit->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			if (!gDLL->getInterfaceIFace()->isCityScreenUp())
			{
				gDLL->getInterfaceIFace()->selectGroup(pUnit, gDLL->shiftKey(), gDLL->ctrlKey(), gDLL->altKey());
			}
			else
			{
				pUnit->joinCity();
			}
		}
	}
}


void CvDLLWidgetData::doLiberateCity()
{
	GC.getGameINLINE().selectedCitiesGameNetMessage(GAMEMESSAGE_DO_TASK, TASK_LIBERATE, 0);

	gDLL->getInterfaceIFace()->clearSelectedCities();
}


void CvDLLWidgetData::doRenameCity()
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL)
	{
		if (pHeadSelectedCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getEventReporterIFace()->cityRename(pHeadSelectedCity);
		}
	}
}


void CvDLLWidgetData::doRenameUnit()
{
	CvUnit* pHeadSelectedUnit = gDLL->getInterfaceIFace()->getHeadSelectedUnit();
	if (pHeadSelectedUnit != NULL)
	{
		if (pHeadSelectedUnit->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getEventReporterIFace()->unitRename(pHeadSelectedUnit);
		}
	}
}


void CvDLLWidgetData::doCreateGroup()
{
	GC.getGameINLINE().selectionListGameNetMessage(GAMEMESSAGE_JOIN_GROUP);
}


void CvDLLWidgetData::doDeleteGroup()
{
	GC.getGameINLINE().selectionListGameNetMessage(GAMEMESSAGE_JOIN_GROUP, -1, -1, -1, 0, false, true);
}


void CvDLLWidgetData::doTrain(CvWidgetDataStruct &widgetDataStruct)
{
	UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(GC.getGameINLINE().getActiveCivilizationType()).getCivilizationUnits(widgetDataStruct.m_iData1);

	if (widgetDataStruct.m_iData2 != FFreeList::INVALID_INDEX)
	{
		gDLL->sendPushOrder(widgetDataStruct.m_iData2, ORDER_TRAIN, eUnit, false, false, false);
	}
	else
	{
		GC.getGameINLINE().selectedCitiesGameNetMessage(GAMEMESSAGE_PUSH_ORDER, ORDER_TRAIN, eUnit, -1, false, gDLL->altKey(), gDLL->shiftKey(), gDLL->ctrlKey());
	}
}


void CvDLLWidgetData::doConstruct(CvWidgetDataStruct &widgetDataStruct)
{
	BuildingTypes eBuilding;

	eBuilding = ((BuildingTypes)(GC.getCivilizationInfo(GC.getGameINLINE().getActiveCivilizationType()).getCivilizationBuildings(widgetDataStruct.m_iData1)));

	if (widgetDataStruct.m_iData2 != FFreeList::INVALID_INDEX)
	{
		gDLL->sendPushOrder(widgetDataStruct.m_iData2, ORDER_CONSTRUCT, eBuilding, false, false, false);
	}
	else
	{
		GC.getGameINLINE().selectedCitiesGameNetMessage(GAMEMESSAGE_PUSH_ORDER, ORDER_CONSTRUCT, eBuilding, -1, false, gDLL->altKey(), gDLL->shiftKey(), gDLL->ctrlKey());
	}
}

void CvDLLWidgetData::doHurry(CvWidgetDataStruct &widgetDataStruct)
{
	GC.getGameINLINE().selectedCitiesGameNetMessage(GAMEMESSAGE_DO_TASK, TASK_HURRY, widgetDataStruct.m_iData1);
}


void CvDLLWidgetData::doPlayerHurry(CvWidgetDataStruct &widgetDataStruct)
{
	gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_HURRY, widgetDataStruct.m_iData1, widgetDataStruct.m_iData2, -1);
}


void CvDLLWidgetData::doAction(CvWidgetDataStruct &widgetDataStruct)
{
	int iAction = widgetDataStruct.m_iData1;
	if (GC.getActionInfo(iAction).getControlType() != NO_CONTROL) {
		if (GC.getActionInfo(iAction).getControlType() != CONTROL_RETIRE) {
			GC.getGameINLINE().doControl((ControlTypes)(GC.getActionInfo(iAction).getControlType()), widgetDataStruct.m_iData2);
			return;
		}
	}
	GC.getGameINLINE().handleAction(widgetDataStruct.m_iData1);
}

void CvDLLWidgetData::doContactCiv(CvWidgetDataStruct &widgetDataStruct)
{
	if (gDLL->isDiplomacy() || gDLL->isMPDiplomacyScreenUp())
	{
		return;
	}
	bool bOnlyInfo = widgetDataStruct.m_iData2 == 1;

	if (bOnlyInfo)
	{
		return;
	}

	//	Do not execute this if we are trying to contact ourselves...
	if (GC.getGameINLINE().getActivePlayer() == widgetDataStruct.m_iData1)
	{
		doFlag();
		return;
	}

	if (gDLL->shiftKey())
	{
		if (GET_PLAYER((PlayerTypes)widgetDataStruct.m_iData1).isHuman())
		{
			if (widgetDataStruct.m_iData1 != GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->showTurnLog((ChatTargetTypes)widgetDataStruct.m_iData1);
			}
		}
		return;
	}

	if (gDLL->altKey())
	{
		if (GET_TEAM(GC.getGameINLINE().getActiveTeam()).canDeclareWar(GET_PLAYER((PlayerTypes)widgetDataStruct.m_iData1).getTeam()))
		{
			gDLL->sendChangeWar(GET_PLAYER((PlayerTypes)widgetDataStruct.m_iData1).getTeam(), true);
		}
		return;
	}

	GET_PLAYER(GC.getGameINLINE().getActivePlayer()).contact((PlayerTypes)widgetDataStruct.m_iData1);
}

void CvDLLWidgetData::doAutomateCitizens(CvWidgetDataStruct &widgetDataStruct)
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL)
	{
		if (pHeadSelectedCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			GC.getGameINLINE().selectedCitiesGameNetMessage(GAMEMESSAGE_DO_TASK, TASK_SET_ALL_CITIZENS_AUTOMATED, widgetDataStruct.m_iData1, -1);
		}
	}
}

void CvDLLWidgetData::doAutomateProduction()
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL)
	{
		if (pHeadSelectedCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			GC.getGameINLINE().selectedCitiesGameNetMessage(GAMEMESSAGE_DO_TASK, TASK_SET_AUTOMATED_PRODUCTION, -1, -1, !pHeadSelectedCity->isProductionAutomated(), gDLL->altKey(), gDLL->shiftKey(), gDLL->ctrlKey());
		}
	}
}

EmphasizeTypes CvDLLWidgetData::getNextEmphasis(CvCity* pCity, YieldTypes eYield)
{
	int iCurrentEmphasis = pCity->AI_getEmphasizeYieldCount(eYield);
	if (iCurrentEmphasis == 0)
	{
		// No current emphasis - return positive emphasis
		for (int i = 0; i < GC.getNumEmphasizeInfos(); i++)
		{
			EmphasizeTypes eLoopEmphasis = (EmphasizeTypes) i;
			int iYieldChange = GC.getEmphasizeInfo(eLoopEmphasis).getYieldChange(eYield);
			if (iYieldChange > 0)
			{
				return eLoopEmphasis;
			}
		}
	}
	else if (iCurrentEmphasis > 0)
	{
		// Positive current emphasis - return negative emphasis
		for (int i = 0; i < GC.getNumEmphasizeInfos(); i++)
		{
			EmphasizeTypes eLoopEmphasis = (EmphasizeTypes) i;
			int iYieldChange = GC.getEmphasizeInfo(eLoopEmphasis).getYieldChange(eYield);
			if (iYieldChange < 0)
			{
				return eLoopEmphasis;
			}
		}
	}

	// Negative current emphasis - return no emphasis
	return NO_EMPHASIZE;
}


void CvDLLWidgetData::doEmphasize(CvWidgetDataStruct &widgetDataStruct)
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();

	YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
	if (pHeadSelectedCity != NULL)
	{
		if (eYield == NO_YIELD)
		{
			EmphasizeTypes eEmphasize = (EmphasizeTypes) widgetDataStruct.m_iData2;
			if (eEmphasize != NO_EMPHASIZE)
			{
				GC.getGameINLINE().selectedCitiesGameNetMessage(GAMEMESSAGE_DO_TASK, TASK_SET_EMPHASIZE, eEmphasize, -1, !(pHeadSelectedCity->AI_isEmphasize(eEmphasize)));
			}
			else
			{
				for (int iEmphasize = 0; iEmphasize < GC.getNumEmphasizeInfos(); ++iEmphasize)
				{
					GC.getGameINLINE().selectedCitiesGameNetMessage(GAMEMESSAGE_DO_TASK, TASK_SET_EMPHASIZE, iEmphasize, -1, false);
				}
			}
		}
		else
		{
			EmphasizeTypes eNextEmphasis = getNextEmphasis(pHeadSelectedCity, eYield);
			for (int i = 0; i < GC.getNumEmphasizeInfos(); i++)
			{
				// turn off all emphasis except eNextEmphasis
				if (GC.getEmphasizeInfo((EmphasizeTypes) i).getYieldChange(eYield) != 0)
				{
					GC.getGameINLINE().selectedCitiesGameNetMessage(GAMEMESSAGE_DO_TASK, TASK_SET_EMPHASIZE, i, -1, i == eNextEmphasis);
				}
			}
		}
	}
}

void CvDLLWidgetData::doChooseQuantityTradeInCity(CvWidgetDataStruct &widgetDataStruct)
{
	if (!gDLL->shiftKey())
	{
		PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
		if (ePlayer != NO_PLAYER)
		{
			CvPlayer& kPlayer = GET_PLAYER(ePlayer);
			CvUnit* pUnit = kPlayer.getUnit(widgetDataStruct.m_iData2);
			YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
			if (pUnit != NULL) {
				YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
				CvPlot* pPlot = pUnit->plot();
				CvCity* pCity = pPlot->getPlotCity();
				int iNewCargo = pCity->getYieldStored(eYield);
				kPlayer.setIATradeProposition(eYield, iNewCargo);
				gDLL->getInterfaceIFace()->setDirty(NativeYieldTrade_DIRTY_BIT, true);
			}
		}
	} else {
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_QUANTITY_TRADE, widgetDataStruct.m_iData2, widgetDataStruct.m_iData1, 0);
		gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true, true);
	}
}

void CvDLLWidgetData::doChooseQuantityTradeInUnit(CvWidgetDataStruct &widgetDataStruct)
{
	if (!gDLL->shiftKey())
	{
		PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
		if (ePlayer != NO_PLAYER)
		{
			CvPlayer& kPlayer = GET_PLAYER(ePlayer);
			CvUnit* pUnit = kPlayer.getUnit(widgetDataStruct.m_iData2);
			YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
			if (pUnit != NULL) {
				YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
				int iNewCargo = pUnit->getNewCargoYield(eYield);
				kPlayer.setHumanTradeProposition(eYield, iNewCargo);
				
				gDLL->getInterfaceIFace()->setDirty(NativeYieldTrade_DIRTY_BIT, true);
			}
		}
	} else {
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_QUANTITY_TRADE, widgetDataStruct.m_iData2, widgetDataStruct.m_iData1, 1);
		gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true, true);
	}
}

void CvDLLWidgetData::doUnitModel()
{
	if (gDLL->getInterfaceIFace()->isFocused())
	{
		//	Do NOT execute if a screen is up...
		return;
	}

	gDLL->getInterfaceIFace()->lookAtSelectionPlot();
}


void CvDLLWidgetData::doFlag()
{
	GC.getGameINLINE().doControl(CONTROL_SELECTCAPITAL);
}

void CvDLLWidgetData::doSelected(CvWidgetDataStruct &widgetDataStruct)
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL)
	{
		GC.getGameINLINE().selectedCitiesGameNetMessage(GAMEMESSAGE_POP_ORDER, widgetDataStruct.m_iData1);
	}
}
void CvDLLWidgetData::doPediaUnitJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;

	argsList.add(widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToUnit", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaProfessionJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;

	argsList.add(widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToProfession", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaBuildingJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToBuilding", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaTerrainJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToTerrain", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaYieldJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToYields", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaFeatureJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToFeature", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaTrainJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(GC.getCivilizationInfo(GC.getGameINLINE().getActiveCivilizationType()).getCivilizationUnits(widgetDataStruct.m_iData1));

	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToUnit", argsList.makeFunctionArgs());
}


void CvDLLWidgetData::doPediaConstructJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(GC.getCivilizationInfo(GC.getGameINLINE().getActiveCivilizationType()).getCivilizationBuildings(widgetDataStruct.m_iData1));

	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToBuilding", argsList.makeFunctionArgs());
}


void CvDLLWidgetData::doPediaBack()
{
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaBack");
}

void CvDLLWidgetData::doPediaForward()
{
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaForward");
}

void CvDLLWidgetData::doPediaBonusJump(CvWidgetDataStruct &widgetDataStruct, bool bData2)
{
	CyArgsList argsList;
	if (bData2)
	{
		argsList.add(widgetDataStruct.m_iData2);
	}
	else
	{
		argsList.add(widgetDataStruct.m_iData1);
	}
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToBonus", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaMain(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1 < 0 ? 0 : widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaMain", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaPromotionJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToPromotion", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaImprovementJump(CvWidgetDataStruct &widgetDataStruct, bool bData2)
{
	CyArgsList argsList;
	if (bData2)
	{
		argsList.add(widgetDataStruct.m_iData2);
	}
	else
	{
		argsList.add(widgetDataStruct.m_iData1);
	}
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToImprovement", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaCivicJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToCivic", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaCivilizationJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToCiv", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaLeaderJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToLeader", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaDescription(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1);
	argsList.add(widgetDataStruct.m_iData2);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaShowHistorical", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doPediaBuildJump(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;

	ImprovementTypes eImprovement = NO_IMPROVEMENT;
	BuildTypes eBuild = (BuildTypes)widgetDataStruct.m_iData2;
	if (NO_BUILD != eBuild)
	{
		eImprovement = (ImprovementTypes)GC.getBuildInfo(eBuild).getImprovement();
	}

	if (NO_IMPROVEMENT != eImprovement)
	{
		argsList.add(eImprovement);
		gDLL->getPythonIFace()->callFunction(PYScreensModule, "pediaJumpToImprovement", argsList.makeFunctionArgs());
	}
}

void CvDLLWidgetData::doGotoTurnEvent(CvWidgetDataStruct &widgetDataStruct)
{
	CvPlot* pPlot = GC.getMapINLINE().plotINLINE(widgetDataStruct.m_iData1, widgetDataStruct.m_iData2);

	if (NULL != pPlot && !gDLL->getEngineIFace()->isCameraLocked())
	{
		if (pPlot->isRevealed(GC.getGameINLINE().getActiveTeam(), false))
		{
			gDLL->getEngineIFace()->cameraLookAt(pPlot->getPoint());
		}
	}
}

void CvDLLWidgetData::doMenu( void )
{
	if (!gDLL->isGameInitializing())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_MAIN_MENU);
		gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
	}
}

//
//	HELP PARSING FUNCTIONS
//

void CvDLLWidgetData::parsePlotListHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	PROFILE_FUNC();

	CvUnit* pUnit;

	int iUnitIndex = widgetDataStruct.m_iData1 + gDLL->getInterfaceIFace()->getPlotListColumn() - gDLL->getInterfaceIFace()->getPlotListOffset();

	CvPlot *selectionPlot = gDLL->getInterfaceIFace()->getSelectionPlot();
	pUnit = gDLL->getInterfaceIFace()->getInterfacePlotUnit(selectionPlot, iUnitIndex);

	if (pUnit != NULL)
	{
		GAMETEXT.setUnitHelp(szBuffer, pUnit);

		if (pUnit->plot()->plotCount(PUF_isUnitType, pUnit->getUnitType(), -1, pUnit->getOwnerINLINE()) > 1)
		{
			szBuffer.append(NEWLINE);
			szBuffer.append(gDLL->getText("TXT_KEY_MISC_CTRL_SELECT", GC.getUnitInfo(pUnit->getUnitType()).getTextKeyWide()));
		}

		if (pUnit->plot()->plotCount(NULL, -1, -1, pUnit->getOwnerINLINE()) > 1)
		{
			szBuffer.append(NEWLINE);
			szBuffer.append(gDLL->getText("TXT_KEY_MISC_ALT_SELECT"));
		}
	}
}


void CvDLLWidgetData::parseLiberateCityHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL)
	{
		PlayerTypes ePlayer = pHeadSelectedCity->getLiberationPlayer(false);
		if (NO_PLAYER != ePlayer)
		{
			szBuffer.append(gDLL->getText("TXT_KEY_LIBERATE_CITY_HELP", pHeadSelectedCity->getNameKey(), GET_PLAYER(ePlayer).getNameKey()));
		}
	}
}

void CvDLLWidgetData::parseCityNameHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL)
	{
		szBuffer.append(pHeadSelectedCity->getName());

		szBuffer.append(NEWLINE);
		szBuffer.append(gDLL->getText("TXT_KEY_CITY_POPULATION", pHeadSelectedCity->getRealPopulation()));

		CvWString szTempBuffer;
		GAMETEXT.setTimeStr(szTempBuffer, pHeadSelectedCity->getGameTurnFounded(), false);
		szBuffer.append(NEWLINE);
		szBuffer.append(gDLL->getText("TXT_KEY_CITY_FOUNDED", szTempBuffer.GetCString()));

		szBuffer.append(NEWLINE);
		szBuffer.append(gDLL->getText("TXT_KEY_CHANGE_NAME"));
		if (GC.getGameINLINE().isDebugMode()) {
			szBuffer.append(NEWLINE);
			szBuffer.append(CvWString::format(L"ID: %d", pHeadSelectedCity->getID()));

			szBuffer.append(NEWLINE);
			szBuffer.append(CvWString::format(L"Disturbation Value %d", pHeadSelectedCity->AI_getNavalDisturbationCount()));

			for (uint i = 0; i < (uint) pHeadSelectedCity->getNumCityJobs(); ++i)
			{
				CvCityJob* pJob = pHeadSelectedCity->getCityJobByIndex(i);
				if (pJob != NULL) 
				{
					szBuffer.append(NEWLINE);
					const wchar* professionName = pJob->getProfessionHelp();
					UnitClassTypes eUnitClassType = pJob->getRequiredUnitClassType();
					const wchar* unitClassName = eUnitClassType != NO_UNITCLASS ? GC.getUnitClassInfo(eUnitClassType).getDescription() : L"";
					CvWString szTempJobString;
					getJobString(szTempJobString, pJob->getJobType());
					szBuffer.append(CvWString::format(L"%s(%s) N:%d R:%d T:%s", szTempJobString.GetCString(), professionName, pJob->getNumUnitNeeded(), pJob->getNumRequired(), unitClassName));
				}
			}
		}
	}
}



void CvDLLWidgetData::parseTrainHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pHeadSelectedCity;
	UnitTypes eUnit;

	if (widgetDataStruct.m_iData2 != FFreeList::INVALID_INDEX)
	{
		pHeadSelectedCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(widgetDataStruct.m_iData2);
	}
	else
	{
		pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	}

	if (pHeadSelectedCity != NULL)
	{
		eUnit = (UnitTypes)GC.getCivilizationInfo(pHeadSelectedCity->getCivilizationType()).getCivilizationUnits(widgetDataStruct.m_iData1);
		GAMETEXT.setUnitHelp(szBuffer, eUnit, false, widgetDataStruct.m_bOption, pHeadSelectedCity);
	}
}


void CvDLLWidgetData::parseConstructHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pHeadSelectedCity;
	BuildingTypes eBuilding;

	if (widgetDataStruct.m_iData2 != FFreeList::INVALID_INDEX)
	{
		pHeadSelectedCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(widgetDataStruct.m_iData2);
	}
	else
	{
		pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	}

	if (pHeadSelectedCity != NULL)
	{
		eBuilding = (BuildingTypes)GC.getCivilizationInfo(pHeadSelectedCity->getCivilizationType()).getCivilizationBuildings(widgetDataStruct.m_iData1);
		GAMETEXT.setBuildingHelp(szBuffer, eBuilding, false, widgetDataStruct.m_bOption, pHeadSelectedCity);
	}
}

void CvDLLWidgetData::parseHurryHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	HurryTypes eHurry = (HurryTypes) widgetDataStruct.m_iData1;
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();

	if (pHeadSelectedCity != NULL)
	{
		if (!(pHeadSelectedCity->isProductionUnit()) && !(pHeadSelectedCity->isProductionBuilding()))
		{
			szBuffer.append(NEWLINE);
			szBuffer.assign(gDLL->getText("TXT_KEY_MISC_UNIT_BUILDING_HURRY"));
		}
		else
		{
			szBuffer.assign(gDLL->getText("TXT_KEY_MISC_HURRY_PROD", pHeadSelectedCity->getProductionNameKey()));

			int iHurryGold = pHeadSelectedCity->hurryGold(eHurry);
			if (iHurryGold > 0)
			{
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_MISC_HURRY_GOLD", iHurryGold));
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_FINANCIAL_ADVISOR_TREASURY", GET_PLAYER(pHeadSelectedCity->getOwnerINLINE()).getGold()));
			}

			int iHurryPopulation = pHeadSelectedCity->hurryPopulation(eHurry);
			if (iHurryPopulation > 0)
			{
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_MISC_HURRY_POP", iHurryPopulation));

				if (iHurryPopulation > pHeadSelectedCity->maxHurryPopulation())
				{
					szBuffer.append(gDLL->getText("TXT_KEY_MISC_MAX_POP_HURRY", pHeadSelectedCity->maxHurryPopulation()));
				}
			}

			for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
			{
				int iAmountNeeded = pHeadSelectedCity->hurryYield(eHurry, (YieldTypes) iYield);
				if (iAmountNeeded > 0)
				{
					szBuffer.append(NEWLINE);
					szBuffer.append(gDLL->getText("TXT_KEY_MISC_HURRY_YIELD", iAmountNeeded, GC.getYieldInfo((YieldTypes) iYield).getChar()));
				}
			}
			bool bFirst = true;
			/*
			if (!(GET_PLAYER(pHeadSelectedCity->getOwnerINLINE()).canHurry((HurryTypes)(widgetDataStruct.m_iData1), -1)))
			{
				for (int iI = 0; iI < GC.getNumCivicInfos(); iI++)
				{
					if (GC.getCivicInfo((CivicTypes)iI).isHurry(widgetDataStruct.m_iData1))
					{
						CvWString szTempBuffer = NEWLINE + gDLL->getText("TXT_KEY_REQUIRES");
						setListHelp(szBuffer, szTempBuffer, GC.getCivicInfo((CivicTypes)iI).getDescription(), gDLL->getText("TXT_KEY_OR").c_str(), bFirst);
						bFirst = false;
					}
				}

				for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
				{
					YieldTypes eYield = (YieldTypes) iYield;
					if (GC.getYieldInfo(eYield).isCargo() && !GET_PLAYER(pHeadSelectedCity->getOwnerINLINE()).isYieldEuropeTradable(eYield))
					{
						int iAmountNeeded = pHeadSelectedCity->getProductionNeeded(eYield);
						if (iAmountNeeded > pHeadSelectedCity->getYieldStored(eYield) + pHeadSelectedCity->getYieldRushed(eYield))
						{
							szBuffer.append(NEWLINE + gDLL->getText("TXT_KEY_REQUIRES"));
							szBuffer.append(CvWString::format(L"%d%c", iAmountNeeded, GC.getYieldInfo(eYield).getChar()));
							bFirst = false;
						}
					}
				}
			}*/

			for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
			{
				YieldTypes eYield = (YieldTypes) iYield;
				if (GC.getYieldInfo(eYield).isCargo() && !GET_PLAYER(pHeadSelectedCity->getOwnerINLINE()).isYieldEuropeTradable(eYield))
				{
					int iAmountNeeded = pHeadSelectedCity->getProductionNeeded(eYield);
					if (iAmountNeeded > pHeadSelectedCity->getYieldStored(eYield) + pHeadSelectedCity->getYieldRushed(eYield))
					{
						szBuffer.append(NEWLINE + gDLL->getText("TXT_KEY_REQUIRES"));
						szBuffer.append(CvWString::format(L"%d%c", iAmountNeeded, GC.getYieldInfo(eYield).getChar()));
						bFirst = false;
					}
				}
			}

			if (!bFirst)
			{
				szBuffer.append(ENDCOLR);
			}
		}
	}
}

void CvDLLWidgetData::parseActionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CLLNode<IDInfo>* pSelectedUnitNode;
	CvCity* pMissionCity;
	CvUnit* pHeadSelectedUnit;
	CvUnit* pSelectedUnit;
	CvPlot* pMissionPlot;
	CvPlot* pLoopPlot;
	CvWString szTempBuffer;
	CvWString szFirstBuffer;
	ImprovementTypes eImprovement;
	ImprovementTypes eFinalImprovement;
	PlayerTypes eGiftPlayer;
	BuildTypes eBuild;
	RouteTypes eRoute;
	bool bAlt;
	bool bShift;
	bool bValid;
	int iYield;
	int iMovementCost;
	int iFlatMovementCost;
	int iMoves;
	int iFlatMoves;
	int iNowWorkRate;
	int iThenWorkRate;
	int iTurns;
	int iPrice;
	int iLast;
	int iRange;
	int iDX, iDY;
	int iI;

	bAlt = gDLL->altKey();
	bShift = gDLL->shiftKey();
	CvActionInfo kInfo = GC.getActionInfo(widgetDataStruct.m_iData1);
	switch(kInfo.getControlType()) {
		case CONTROL_EUROPE_SCREEN:
		case CONTROL_EUROPE_C1_SCREEN:
		case CONTROL_EUROPE_C2_SCREEN:
		case CONTROL_EUROPE_C3_SCREEN:
			if (widgetDataStruct.m_iData2 > 0) {
				switch(widgetDataStruct.m_iData2) {
					case 1:
						szBuffer.append(gDLL->getText("TXT_KEY_MISC_EUROPE_INFO_CAPTAINCY_1"));
						break;
					case 2:
						szBuffer.append(gDLL->getText("TXT_KEY_MISC_EUROPE_INFO_CAPTAINCY_2"));
						break;
					case 3:
						szBuffer.append(gDLL->getText("TXT_KEY_MISC_EUROPE_INFO_SHIP_1"));
						break;
					case 4:
						szBuffer.append(gDLL->getText("TXT_KEY_MISC_EUROPE_INFO_SHIP_2"));
						break;
					case 5:
						szBuffer.append(gDLL->getText("TXT_KEY_MISC_EUROPE_INFO_TRADE_1"));
						break;
					case 6:
						szBuffer.append(gDLL->getText("TXT_KEY_MISC_EUROPE_INFO_TRADE_2"));
						break;
					case 7:
						szBuffer.append(gDLL->getText("TXT_KEY_MISC_EUROPE_INFO_IMMIGRATION_1"));
						break;
					case 8:
						szBuffer.append(gDLL->getText("TXT_KEY_MISC_EUROPE_INFO_IMMIGRATION_2"));
						break;
				}
				return;
			}
			break;
		case CONTROL_TRADE_ROUTE_SCREEN:
			if (widgetDataStruct.m_iData2 > 0) {
				CvPlayer& pPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
				int iGroupId = widgetDataStruct.m_iData2;
				CvSelectionGroup* pGroup = pPlayer.getSelectionGroup(iGroupId);
				if (pGroup != NULL) {
					szBuffer.append(gDLL->getText(pGroup->getNumUnits() <= 1 ? "TXT_KEY_CHANGE_STATE_OF_SHIP" : "TXT_KEY_CHANGE_STATE_OF_SHIPS"));
				}
				return;
			}
			break;
	}

	CvWString szTemp;
	szTemp.Format(SETCOLR L"%s" ENDCOLR , TEXT_COLOR("COLOR_HIGHLIGHT_TEXT"), GC.getActionInfo(widgetDataStruct.m_iData1).getHotKeyDescription().c_str());
	szBuffer.assign(szTemp);

	pHeadSelectedUnit = gDLL->getInterfaceIFace()->getHeadSelectedUnit();

	if (pHeadSelectedUnit != NULL)
	{
		if (GC.getActionInfo(widgetDataStruct.m_iData1).getMissionType() != NO_MISSION)
		{
			if (bShift && gDLL->getInterfaceIFace()->mirrorsSelectionGroup())
			{
				pMissionPlot = pHeadSelectedUnit->getGroup()->lastMissionPlot();
			}
			else
			{
				pMissionPlot = pHeadSelectedUnit->plot();
			}

			pMissionCity = pMissionPlot->getPlotCity();

			if (GC.getActionInfo(widgetDataStruct.m_iData1).getMissionType() == MISSION_HEAL)
			{
				iTurns = 0;

				pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();

				while (pSelectedUnitNode != NULL)
				{
					pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);
					iTurns = std::max(iTurns, pSelectedUnit->healTurns(pMissionPlot));

					pSelectedUnitNode = gDLL->getInterfaceIFace()->nextSelectionListNode(pSelectedUnitNode);
				}

				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_MISC_TURN_OR_TURNS", iTurns));
			}
			else if (GC.getActionInfo(widgetDataStruct.m_iData1).getMissionType() == MISSION_PILLAGE)
			{
				if (pMissionPlot->getImprovementType() != NO_IMPROVEMENT)
				{
					szBuffer.append(NEWLINE);
					szBuffer.append(gDLL->getText("TXT_KEY_ACTION_DESTROY_IMP", GC.getImprovementInfo(pMissionPlot->getImprovementType()).getTextKeyWide()));
				}
				else if (pMissionPlot->getRouteType() != NO_ROUTE)
				{
					szBuffer.append(NEWLINE);
					szBuffer.append(gDLL->getText("TXT_KEY_ACTION_DESTROY_IMP", GC.getRouteInfo(pMissionPlot->getRouteType()).getTextKeyWide()));
				}
			}
			else if (GC.getActionInfo(widgetDataStruct.m_iData1).getMissionType() == MISSION_FOUND)
			{
				if (!(GET_PLAYER(pHeadSelectedUnit->getOwnerINLINE()).canFound(pMissionPlot->getX_INLINE(), pMissionPlot->getY_INLINE())))
				{
					bValid = true;

					iRange = GC.getMIN_CITY_RANGE();

					for (iDX = -(iRange); iDX <= iRange; iDX++)
					{
						for (iDY = -(iRange); iDY <= iRange; iDY++)
						{
							pLoopPlot	= plotXY(pMissionPlot->getX_INLINE(), pMissionPlot->getY_INLINE(), iDX, iDY);

							if (pLoopPlot != NULL)
							{
								if (pLoopPlot->isCity())
								{
									bValid = false;
								}
							}
						}
					}

					if (!bValid)
					{
						szBuffer.append(NEWLINE);
						szBuffer.append(gDLL->getText("TXT_KEY_ACTION_CANNOT_FOUND", GC.getMIN_CITY_RANGE()));
					}
				}
			}
			else if (GC.getActionInfo(widgetDataStruct.m_iData1).getMissionType() == MISSION_JOIN_CITY)
			{
				if (!pHeadSelectedUnit->canJoinCity(pMissionPlot))
				{
					if (pHeadSelectedUnit->hasMoved())
					{
						szBuffer.append(NEWLINE);
						szBuffer.append(gDLL->getText("TXT_KEY_ACTION_CANNOT_JOIN_MOVE"));
					}

					CvCity* pCity = pMissionPlot->getPlotCity();
					if (pCity != NULL)
					{
						if (pCity->foodDifference() < 0 && !pHeadSelectedUnit->canJoinStarvingCity(*pCity))
						{
							szBuffer.append(NEWLINE);
							szBuffer.append(gDLL->getText("TXT_KEY_ACTION_CANNOT_JOIN_STARVING"));
						}
					}
				}
			}
			else if (GC.getActionInfo(widgetDataStruct.m_iData1).getMissionType() == MISSION_LEAD)
			{
				if (pHeadSelectedUnit->getUnitInfo().getLeaderExperience() > 0)
				{
					int iNumUnits = pHeadSelectedUnit->canGiveExperience(pHeadSelectedUnit->plot());
					if (iNumUnits > 0)
					{
						szBuffer.append(NEWLINE);
						szBuffer.append(gDLL->getText("TXT_KEY_ACTION_LEAD_TROOPS", pHeadSelectedUnit->getStackExperienceToGive(iNumUnits)));
					}
				}
				if (pHeadSelectedUnit->getUnitInfo().getLeaderPromotion() != NO_PROMOTION)
				{
					szBuffer.append(NEWLINE);
					szBuffer.append(gDLL->getText("TXT_KEY_PROMOTION_WHEN_LEADING"));
					GAMETEXT.parsePromotionHelp(szBuffer, (PromotionTypes)pHeadSelectedUnit->getUnitInfo().getLeaderPromotion(), L"\n   ");
				}
			}
			else if (GC.getActionInfo(widgetDataStruct.m_iData1).getMissionType() == MISSION_BUILD)
			{
				eBuild = ((BuildTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getMissionData()));
				FAssert(eBuild != NO_BUILD);
				eImprovement = pMissionPlot->getImprovementType();
				ImprovementTypes eBuildImprovement = (ImprovementTypes) GC.getBuildInfo(eBuild).getImprovement();
				if (eBuildImprovement != NO_IMPROVEMENT)
				{
					eImprovement = eBuildImprovement;
				}
				eRoute = pMissionPlot->getRouteType();
				RouteTypes eBuildRoute = (RouteTypes) GC.getBuildInfo(eBuild).getRoute();
				if (eBuildRoute != NO_ROUTE)
				{
					eRoute = eBuildRoute;
				}
				FeatureTypes eFeature = pMissionPlot->getFeatureType();
				bool bIgnoreFeature = (eFeature != NO_FEATURE && GC.getBuildInfo(eBuild).isFeatureRemove(eFeature));
				for (iI = 0; iI < NUM_YIELD_TYPES; iI++)
				{
					iYield = pMissionPlot->calculatePotentialYield((YieldTypes) iI, GC.getGameINLINE().getActivePlayer(), eImprovement, bIgnoreFeature, eRoute, NO_UNIT, false);
					iYield -= pMissionPlot->calculatePotentialYield((YieldTypes) iI, GC.getGameINLINE().getActivePlayer(), pMissionPlot->getImprovementType(), false, pMissionPlot->getRouteType(), NO_UNIT, false);

					if (iYield != 0)
					{
						szTempBuffer.Format(L", %s%d%c", ((iYield > 0) ? L"+" : L""), iYield, GC.getYieldInfo((YieldTypes) iI).getChar());
						szBuffer.append(szTempBuffer);
					}
				}

				bValid = false;

				pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();

				while (pSelectedUnitNode != NULL)
				{
					pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);

					if (pSelectedUnit->canBuild(pMissionPlot, eBuild))
					{
						bValid = true;
						break;
					}

					pSelectedUnitNode = gDLL->getInterfaceIFace()->nextSelectionListNode(pSelectedUnitNode);
				}

				if (!bValid)
				{
					if (eBuildImprovement != NO_IMPROVEMENT)
					{
						if (pMissionPlot->getTeam() != pHeadSelectedUnit->getTeam())
						{
							if (GC.getImprovementInfo(eBuildImprovement).isOutsideBorders())
							{
								if (pMissionPlot->getTeam() != NO_TEAM)
								{
									szBuffer.append(NEWLINE);
									szBuffer.append(gDLL->getText("TXT_KEY_ACTION_NEEDS_OUT_RIVAL_CULTURE_BORDER"));
								}
							}
							else
							{
								szBuffer.append(NEWLINE);
								szBuffer.append(gDLL->getText("TXT_KEY_ACTION_NEEDS_CULTURE_BORDER"));
							}
						}
					}
				}

				if (eBuildImprovement != NO_IMPROVEMENT)
				{
					if (pMissionPlot->getImprovementType() != NO_IMPROVEMENT)
					{
						szBuffer.append(NEWLINE);
						szBuffer.append(gDLL->getText("TXT_KEY_ACTION_WILL_DESTROY_IMP", GC.getImprovementInfo(pMissionPlot->getImprovementType()).getTextKeyWide()));
					}
				}

				if ((BonusTypes)GC.getBuildInfo(eBuild).getBonusType() != NO_BONUS || (FeatureTypes)GC.getBuildInfo(eBuild).getFeatureType() != NO_FEATURE) {
					if (pMissionPlot->getTeam() != pHeadSelectedUnit->getTeam()) {
						szBuffer.append(NEWLINE);
						szBuffer.append(gDLL->getText("TXT_KEY_ACTION_NEEDS_CULTURE_BORDER"));
					}
				}
				if ((FeatureTypes)GC.getBuildInfo(eBuild).getFeatureType() != NO_FEATURE) {//We don't want have more than one agronomist for plant trees
					CLLNode<IDInfo>* pUnitNode;
					CvUnit* pLoopUnit;
					pUnitNode = pMissionPlot->headUnitNode();
					while (pUnitNode != NULL) {
						pLoopUnit = ::getUnit(pUnitNode->m_data);
						pUnitNode = pMissionPlot->nextUnitNode(pUnitNode);
						if (pHeadSelectedUnit != pLoopUnit && pLoopUnit->getBuildType() == eBuild) {
							szBuffer.append(NEWLINE);
							szBuffer.append(gDLL->getText("TXT_KEY_ONLY_ONE_UNIT", pLoopUnit->getUnitInfo().getDescription()));
							break;
						}
					}
				}

				if (GC.getBuildInfo(eBuild).isKill())
				{
					szBuffer.append(NEWLINE);
					szBuffer.append(gDLL->getText("TXT_KEY_ACTION_CONSUME_UNIT"));
				}

				if (pMissionPlot->getFeatureType() != NO_FEATURE)
				{
					if (GC.getBuildInfo(eBuild).isFeatureRemove(pMissionPlot->getFeatureType()))
					{
						CvCity* pCity = NULL;
						for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
						{
							YieldTypes eYield = (YieldTypes) iYield;
							if (GC.getYieldInfo(eYield).isCargo())
							{
								int iYieldProduction = pMissionPlot->getFeatureYield(eBuild, eYield, pHeadSelectedUnit->getTeam(), &pCity);
								if (iYieldProduction > 0)
								{
									szBuffer.append(NEWLINE);
									szBuffer.append(gDLL->getText("TXT_KEY_ACTION_CHANGE_PRODUCTION", iYieldProduction, pCity->getNameKey(), GC.getYieldInfo(eYield).getChar()));
								}
							}
						}

						szBuffer.append(NEWLINE);
						szBuffer.append(gDLL->getText("TXT_KEY_ACTION_REMOVE_FEATURE", GC.getFeatureInfo(pMissionPlot->getFeatureType()).getTextKeyWide()));
					}

				}

				if (eBuildImprovement != NO_IMPROVEMENT)
				{
					if (pMissionPlot->getBonusType() == NO_BONUS)
					{
						iLast = 0;

						FAssert((0 < GC.getNumBonusInfos()) && "GC.getNumBonusInfos() is not greater than zero but an array is being allocated in CvDLLWidgetData::parseActionHelp");
						for (iI = 0; iI < GC.getNumBonusInfos(); iI++)
						{
							if (GC.getImprovementInfo(eBuildImprovement).getImprovementBonusDiscoverRand(iI) > 0)
							{
								szFirstBuffer.Format(L"%s%s", NEWLINE, gDLL->getText("TXT_KEY_ACTION_CHANCE_DISCOVER").c_str());
								szTempBuffer.Format(L"%c", GC.getBonusInfo((BonusTypes) iI).getChar());
								setListHelp(szBuffer, szFirstBuffer, szTempBuffer, L", ", (GC.getImprovementInfo(eBuildImprovement).getImprovementBonusDiscoverRand(iI) != iLast));
								iLast = GC.getImprovementInfo(eBuildImprovement).getImprovementBonusDiscoverRand(iI);
							}
						}
					}

					if (eBuildRoute == NO_ROUTE)
					{
						for (iI = 0; iI < GC.getNumRouteInfos(); iI++)
						{
							if (pMissionPlot->getRouteType() != ((RouteTypes)iI))
							{
								GAMETEXT.setYieldChangeHelp(szBuffer, GC.getRouteInfo((RouteTypes)iI).getDescription(), L": ", L"", GC.getImprovementInfo(eBuildImprovement).getRouteYieldChangesArray((RouteTypes)iI));
							}
						}
					}

					if (GC.getImprovementInfo(eBuildImprovement).getDefenseModifier() != 0)
					{
						szBuffer.append(NEWLINE);
						szBuffer.append(gDLL->getText("TXT_KEY_ACTION_DEFENSE_MODIFIER", GC.getImprovementInfo(eBuildImprovement).getDefenseModifier()));
					}

					if (GC.getImprovementInfo(eBuildImprovement).getImprovementUpgrade() != NO_IMPROVEMENT)
					{
						iTurns = pMissionPlot->getUpgradeTimeLeft(eBuildImprovement, pHeadSelectedUnit->getOwnerINLINE());

						szBuffer.append(NEWLINE);
						szBuffer.append(gDLL->getText("TXT_KEY_ACTION_BECOMES_IMP", GC.getImprovementInfo((ImprovementTypes) GC.getImprovementInfo(eBuildImprovement).getImprovementUpgrade()).getTextKeyWide(), iTurns));
					}
				}

				if (eBuildRoute != NO_ROUTE)
				{
					eFinalImprovement = eBuildImprovement;

					if (eFinalImprovement == NO_IMPROVEMENT)
					{
						eFinalImprovement = pMissionPlot->getImprovementType();
					}

					if (eFinalImprovement != NO_IMPROVEMENT)
					{
						GAMETEXT.setYieldChangeHelp(szBuffer, GC.getImprovementInfo(eFinalImprovement).getDescription(), L": ", L"", GC.getImprovementInfo(eFinalImprovement).getRouteYieldChangesArray(eBuildRoute));
					}

					iMovementCost = GC.getRouteInfo(eBuildRoute).getMovementCost();
					iFlatMovementCost = GC.getRouteInfo(eBuildRoute).getFlatMovementCost();

					if (iMovementCost > 0)
					{
						iMoves = (GC.getMOVE_DENOMINATOR() / iMovementCost);

						if ((iMoves * iMovementCost) < GC.getMOVE_DENOMINATOR())
						{
							iMoves++;
						}
					}
					else
					{
						iMoves = GC.getMOVE_DENOMINATOR();
					}

					if (iFlatMovementCost > 0)
					{
						iFlatMoves = (GC.getMOVE_DENOMINATOR() / iFlatMovementCost);

						if ((iFlatMoves * iFlatMovementCost) < GC.getMOVE_DENOMINATOR())
						{
							iFlatMoves++;
						}
					}
					else
					{
						iFlatMoves = GC.getMOVE_DENOMINATOR();
					}

					if ((iMoves > 1) || (iFlatMoves > 1))
					{
						if (iMoves >= iFlatMoves)
						{
							szBuffer.append(NEWLINE);
							szBuffer.append(gDLL->getText("TXT_KEY_ACTION_MOVEMENT_COST", iMoves));
						}
						else
						{
							szBuffer.append(NEWLINE);
							szBuffer.append(gDLL->getText("TXT_KEY_ACTION_FLAT_MOVEMENT_COST", iFlatMoves));
						}
					}
				}

				iNowWorkRate = 0;
				iThenWorkRate = 0;

				pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();

				if (NULL != pHeadSelectedUnit)
				{
					int iCost = GET_PLAYER(pHeadSelectedUnit->getOwnerINLINE()).getBuildCost(pMissionPlot, eBuild);
					if (iCost > 0)
					{
						szBuffer.append(NEWLINE);
						if (GET_PLAYER(pHeadSelectedUnit->getOwnerINLINE()).getGold() < iCost)
						{
							szBuffer.append(gDLL->getText("TXT_KEY_BUILD_CANNOT_AFFORD", iCost, gDLL->getSymbolID(GOLD_CHAR)));
						}
						else
						{
							szBuffer.append(gDLL->getText("TXT_KEY_BUILD_COST", iCost));
						}
					}
					ImprovementTypes eImprovement = (ImprovementTypes)GC.getBuildInfo(eBuild).getRequiredImprovement();
					if (eImprovement != NO_IMPROVEMENT) {
						if (pMissionPlot->getImprovementType() != eImprovement) {
							szBuffer.append(NEWLINE);
							szBuffer.append(gDLL->getText("TXT_KEY_BUILD_REQUIRED_IMPROVEMENT", GC.getImprovementInfo(eImprovement).getDescription()));
						}
					}
				}

				while (pSelectedUnitNode != NULL)
				{
					pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);

					iNowWorkRate += pSelectedUnit->workRate(false);
					iThenWorkRate += pSelectedUnit->workRate(true);

					pSelectedUnitNode = gDLL->getInterfaceIFace()->nextSelectionListNode(pSelectedUnitNode);
				}

				iTurns = pMissionPlot->getBuildTurnsLeft(eBuild, iNowWorkRate, iThenWorkRate);


				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_ACTION_NUM_TURNS", iTurns));

				if (!isEmpty(GC.getBuildInfo(eBuild).getHelp()))
				{
					szBuffer.append(CvWString::format(L"%s%s", NEWLINE, GC.getBuildInfo(eBuild).getHelp()).c_str());
				}
			}

			if (!isEmpty(GC.getMissionInfo((MissionTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getMissionType())).getHelp()))
			{
				szBuffer.append(CvWString::format(L"%s%s", NEWLINE, GC.getMissionInfo((MissionTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getMissionType())).getHelp()).c_str());
			}
		}

		if (GC.getActionInfo(widgetDataStruct.m_iData1).getCommandType() != NO_COMMAND)
		{
			if (GC.getActionInfo(widgetDataStruct.m_iData1).getCommandType() == COMMAND_PROMOTION)
			{
				GAMETEXT.parsePromotionHelp(szBuffer, ((PromotionTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getCommandData())));
			} else if (GC.getActionInfo(widgetDataStruct.m_iData1).getCommandType() == COMMAND_UPGRADE) {
				GAMETEXT.setBasicUnitHelp(szBuffer, ((UnitTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getCommandData())));

				if (bAlt && GC.getCommandInfo((CommandTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getCommandType())).getAll())
				{
					iPrice = GET_PLAYER(pHeadSelectedUnit->getOwnerINLINE()).upgradeAllPrice(((UnitTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getCommandData())), pHeadSelectedUnit->getUnitType());
				}
				else
				{
					iPrice = 0;

					pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();

					while (pSelectedUnitNode != NULL)
					{
						pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);

						if (pSelectedUnit->canUpgrade(((UnitTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getCommandData())), true))
						{
							iPrice += pSelectedUnit->upgradePrice((UnitTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getCommandData()));
						}

						pSelectedUnitNode = gDLL->getInterfaceIFace()->nextSelectionListNode(pSelectedUnitNode);
					}
				}

				szTempBuffer.Format(L"%s%d %c", NEWLINE, iPrice, gDLL->getSymbolID(GOLD_CHAR));
				szBuffer.append(szTempBuffer);
			}
			else if (GC.getActionInfo(widgetDataStruct.m_iData1).getCommandType() == COMMAND_GIFT)
			{
				eGiftPlayer = pHeadSelectedUnit->plot()->getOwnerINLINE();

				if (eGiftPlayer != NO_PLAYER)
				{
					szBuffer.append(NEWLINE);
					szBuffer.append(gDLL->getText("TXT_KEY_ACTION_GOES_TO_CIV"));

					szTempBuffer.Format(SETCOLR L"%s" ENDCOLR, GET_PLAYER(eGiftPlayer).getPlayerTextColorR(), GET_PLAYER(eGiftPlayer).getPlayerTextColorG(), GET_PLAYER(eGiftPlayer).getPlayerTextColorB(), GET_PLAYER(eGiftPlayer).getPlayerTextColorA(), GET_PLAYER(eGiftPlayer).getCivilizationShortDescription());
					szBuffer.append(szTempBuffer);

					pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();

					while (pSelectedUnitNode != NULL)
					{
						pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);

						if (!(GET_PLAYER(eGiftPlayer).AI_acceptUnit(pSelectedUnit)))
						{
							szBuffer.append(NEWLINE);
							szBuffer.append(gDLL->getText("TXT_KEY_REFUSE_GIFT", GET_PLAYER(eGiftPlayer).getNameKey()));
							break;
						}

						pSelectedUnitNode = gDLL->getInterfaceIFace()->nextSelectionListNode(pSelectedUnitNode);
					}
				}
			}
			else if (GC.getActionInfo(widgetDataStruct.m_iData1).getCommandType() == COMMAND_LEARN)
			{
				CvPlot* pPlot = pHeadSelectedUnit->plot();
				if (pPlot != NULL)
				{
					UnitTypes eLearnUnit = pHeadSelectedUnit->getLearnUnitType(pPlot);
					if (eLearnUnit != NO_UNIT)
					{
						szBuffer.append(NEWLINE);
						szBuffer.append(gDLL->getText("TXT_KEY_COMMAND_LEARN_HELP_1", GC.getUnitInfo(eLearnUnit).getTextKeyWide()));
					}
				}
			}
			else if (GC.getActionInfo(widgetDataStruct.m_iData1).getCommandType() == COMMAND_ESTABLISH_MISSION)
			{
				CvUnit* pMissionary = NULL;
				pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();
				while (pSelectedUnitNode != NULL)
				{
					pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);
					if (pSelectedUnit->canEstablishMission())
					{
						pMissionary = pSelectedUnit;
						break;
					}

					pSelectedUnitNode = gDLL->getInterfaceIFace()->nextSelectionListNode(pSelectedUnitNode);
				}


				if (pMissionary != NULL)
				{
					szBuffer.append(NEWLINE);
					szBuffer.append(gDLL->getText("TXT_KEY_TALK_NATIVES_POPUP_MISSION2", std::min(100, pMissionary->getMissionarySuccessPercent())));
				}
			}

			if (GC.getCommandInfo((CommandTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getCommandType())).getAll())
			{
				szBuffer.append(gDLL->getText("TXT_KEY_ACTION_ALL_UNITS"));
			}

			if (!isEmpty(GC.getCommandInfo((CommandTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getCommandType())).getHelp()))
			{
				szBuffer.append(CvWString::format(L"%s%s", NEWLINE, GC.getCommandInfo((CommandTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getCommandType())).getHelp()).c_str());
			}
		}

		if (GC.getActionInfo(widgetDataStruct.m_iData1).getAutomateType() != NO_AUTOMATE)
		{
			if (!isEmpty(GC.getAutomateInfo((ControlTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getAutomateType())).getHelp()))
			{
				szBuffer.append(CvWString::format(L"%s%s", NEWLINE, GC.getAutomateInfo((ControlTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getAutomateType())).getHelp()).c_str());
			}
		}
	}

	if (GC.getActionInfo(widgetDataStruct.m_iData1).getControlType() != NO_CONTROL)
	{
		if (!isEmpty(GC.getControlInfo((ControlTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getControlType())).getHelp()))
		{
			szBuffer.append(CvWString::format(L"%s%s", NEWLINE, GC.getControlInfo((ControlTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getControlType())).getHelp()).c_str());
		}
	}

	if (GC.getActionInfo(widgetDataStruct.m_iData1).getInterfaceModeType() != NO_INTERFACEMODE)
	{
		if (!isEmpty(GC.getInterfaceModeInfo((InterfaceModeTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getInterfaceModeType())).getHelp()))
		{
			szBuffer.append(CvWString::format(L"%s%s", NEWLINE, GC.getInterfaceModeInfo((InterfaceModeTypes)(GC.getActionInfo(widgetDataStruct.m_iData1).getInterfaceModeType())).getHelp()).c_str());
		}
	}
}

void CvDLLWidgetData::parseCitizenHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(widgetDataStruct.m_iData2);
	if (pCity == NULL)
	{
		pCity =	gDLL->getInterfaceIFace()->getHeadSelectedCity();
	}
	if (pCity != NULL)
	{
		CvUnit* pUnit = pCity->getPopulationUnitById(widgetDataStruct.m_iData1);
		if (NULL == pUnit)
		{
			return;
		}

		GAMETEXT.setCitizenHelp(szBuffer, *pCity, *pUnit);
	}
}

void CvDLLWidgetData::parseContactCivHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	//	Do not execute this if we are trying to contact ourselves...
	if (widgetDataStruct.m_iData1 >= MAX_PLAYERS)
	{
		return;
	}

	CvPlayer& otherPlayer = GET_PLAYER((PlayerTypes)widgetDataStruct.m_iData1);

	if (otherPlayer.getCivilizationType() == NO_CIVILIZATION)
	{
		return;
	}
	bool bOnlyInfo = widgetDataStruct.m_iData2 == 1;
	if (!bOnlyInfo)
	{
		if (GC.getGameINLINE().getActivePlayer() == otherPlayer.getID())
		{
			parseFlagHelp(widgetDataStruct, szBuffer);
			parseScoreHelp(widgetDataStruct, szBuffer);
			return;
		}


		CvWString szNameKey;
		if (otherPlayer.isEurope())
		{
			szNameKey = "TXT_KEY_GENERIC_KING";
		}
		else
		{
			szNameKey = otherPlayer.getNameKey();
		}
		szBuffer.assign(gDLL->getText("TXT_KEY_MISC_CONTACT_LEADER", szNameKey.GetCString(), otherPlayer.getCivilizationShortDescription()));

		szBuffer.append(NEWLINE);
		GAMETEXT.parsePlayerTraits(szBuffer, otherPlayer.getID());

	}
	if (!(GET_TEAM(GC.getGameINLINE().getActiveTeam()).isHasMet(otherPlayer.getTeam())))
	{
		szBuffer.append(NEWLINE);
		szBuffer.append(gDLL->getText("TXT_KEY_MISC_HAVENT_MET_CIV"));
	}
	else
	{
		if (!otherPlayer.isHuman())
		{
			if (!otherPlayer.AI_isWillingToTalk(GC.getGameINLINE().getActivePlayer()))
			{
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_MISC_REFUSES_TO_TALK"));
			}
			if (!bOnlyInfo)
			{
				szBuffer.append(NEWLINE);
			}
			GAMETEXT.getAttitudeString(szBuffer, otherPlayer.getID(), GC.getGameINLINE().getActivePlayer());

			if (bOnlyInfo)
			{
				return;
			}
			if (!GC.getGameINLINE().isOption(GAMEOPTION_ALWAYS_WAR) && !otherPlayer.isEurope())
			{
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_MISC_CTRL_TRADE"));
			}
		}

		if (otherPlayer.getTeam() != GC.getGameINLINE().getActiveTeam() && !::atWar(GC.getGameINLINE().getActiveTeam(), otherPlayer.getTeam()))
		{
			if (GET_TEAM(GC.getGameINLINE().getActiveTeam()).canDeclareWar(otherPlayer.getTeam()))
			{
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_MISC_ALT_DECLARE_WAR"));
			}
			else if (!GET_TEAM(otherPlayer.getTeam()).isParentOf(GC.getGameINLINE().getActiveTeam()))
			{
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_MISC_CANNOT_DECLARE_WAR"));
			}
		}
	}

	if (otherPlayer.isHuman())
	{
		szBuffer.append(NEWLINE);
		szBuffer.append(gDLL->getText("TXT_KEY_MISC_SHIFT_SEND_CHAT"));
	}

	if ((gDLL->getChtLvl() > 0) && gDLL->shiftKey())
	{
		CvPlayerAI& player = GET_PLAYER((PlayerTypes)widgetDataStruct.m_iData1);

		szBuffer.append(CvWString::format(L"\nPlayer %d, Team %d", player.getID(), player.getTeam()));
		szBuffer.append(CvWString::format(L"\n%d%c %d%c", player.getGold(), gDLL->getSymbolID(GOLD_CHAR), GET_TEAM(player.getTeam()).getRebelPercent(), gDLL->getSymbolID(POWER_CHAR)));
		szBuffer.append(CvWString::format(L"\nCities = %d, Units = %d, Pop = %d, AIPop = %d", player.getNumCities(), player.getNumUnits(), player.getTotalPopulation(), player.AI_getPopulation()));
		szBuffer.append(CvWString::format(L"\nIncome = %d, Hurry Spending = %d", player.AI_getTotalIncome(), player.AI_getHurrySpending()));
		for (int i = 0; i < NUM_STRATEGY_TYPES; ++i)
		{
			if (player.AI_isStrategy((StrategyTypes) i))
			{
				szBuffer.append(CvWString::format(L"\nStrategy %d, Duration %d", i, player.AI_getStrategyDuration((StrategyTypes) i)));
			}
		}

		if (player.isNative())
		{
			szBuffer.append(CvWString::format(L"\nOverpopulation : %d", player.AI_getOverpopulationPercent()));
		}

		CvTeamAI& kTeam = GET_TEAM(player.getTeam());
		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes)i);

			if (kLoopPlayer.isAlive())
			{
				TeamTypes eLoopTeam = kLoopPlayer.getTeam();
				if (eLoopTeam != player.getTeam() && GET_TEAM(eLoopTeam).isAlive())
				{
					WarPlanTypes eWarplan = kTeam.AI_getWarPlan(eLoopTeam);

					if (eWarplan != NO_WARPLAN)
					{
						CvWStringBuffer szTempString;
						GAMETEXT.getWarplanString(szTempString, eWarplan);
						szBuffer.append(CvWString::format(L"\n%s : %s", kLoopPlayer.getName(), szTempString));

					}
				}
			}
		}
	}
}

void CvDLLWidgetData::parseNotorietyHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	GAMETEXT.getNotorietyString(szBuffer, (PlayerTypes)widgetDataStruct.m_iData1);
}

void CvDLLWidgetData::parseChangeSeawayPositionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 == 0) { // We can only move down the seaway
		szBuffer.assign(gDLL->getText("TXT_KEY_ONLY_MOVE_DOWN_SEAWAY"));
	} else if (widgetDataStruct.m_iData2 == 1)  {// We can only move up the seaway
		szBuffer.assign(gDLL->getText("TXT_KEY_ONLY_MOVE_UP_SEAWAY"));
	} else if (widgetDataStruct.m_iData2 == 2)  {// We can do twice
		szBuffer.assign(gDLL->getText("TXT_KEY_ONLY_MOVE_DOWN_UP_SEAWAY"));
	}
}


void CvDLLWidgetData::parseAutomateCitizensHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData1 == 0)
	{
		szBuffer.assign(gDLL->getText("TXT_KEY_MISC_OFF_CITIZEN_AUTO"));
	}
	else
	{
		szBuffer.assign(gDLL->getText("TXT_KEY_MISC_ON_CITIZEN_AUTO"));
	}
}

void CvDLLWidgetData::parseAutomateProductionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pHeadSelectedCity;

	pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();

	if (pHeadSelectedCity != NULL)
	{
		if (pHeadSelectedCity->isProductionAutomated())
		{
			szBuffer.assign(gDLL->getText("TXT_KEY_MISC_OFF_PROD_AUTO"));
		}
		else
		{
			szBuffer.assign(gDLL->getText("TXT_KEY_MISC_ON_PROD_AUTO"));
		}
	}
}

void CvDLLWidgetData::parseEmphasizeHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	szBuffer.clear();

	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();

	if (pHeadSelectedCity != NULL)
	{
		YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
		if (eYield != NO_YIELD)
		{
			szBuffer.append(gDLL->getText("TXT_KEY_CLICK_TO"));
			EmphasizeTypes eNextEmphasis = getNextEmphasis(pHeadSelectedCity, eYield);
			if (eNextEmphasis == NO_EMPHASIZE)
			{
				szBuffer.append(gDLL->getText("TXT_KEY_EMPHASIZE_OFF"));
			}
			else
			{
				szBuffer.append(GC.getEmphasizeInfo(eNextEmphasis).getDescription());
			}
		}
		else
		{
			EmphasizeTypes eEmphasize = (EmphasizeTypes) widgetDataStruct.m_iData2;
			if (eEmphasize != NO_EMPHASIZE)
			{
				szBuffer.append(GC.getEmphasizeInfo(eEmphasize).getDescription());
			}
			else
			{
				szBuffer.append(gDLL->getText("TXT_KEY_CLICK_TO"));
				szBuffer.append(gDLL->getText("TXT_KEY_EMPHASIZE_OFF"));
			}
		}
	}
}

void CvDLLWidgetData::parseChooseQuantityTradeHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	szBuffer.clear();
	szBuffer.assign(gDLL->getText("TXT_KEY_CHOOSE_QUANTITY_TRADE_HELP"));

}

void CvDLLWidgetData::parseTradeItem(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvWString szTempBuffer;
	TradeData item;
	PlayerTypes eWhoFrom = NO_PLAYER;
	PlayerTypes eWhoTo = NO_PLAYER;
	PlayerTypes eWhoDenies;

	szBuffer.clear();

	if (widgetDataStruct.m_bOption)
	{
		if ( gDLL->isDiplomacy())
		{
			eWhoFrom = (PlayerTypes) gDLL->getDiplomacyPlayer();
		}
		else if (gDLL->isMPDiplomacyScreenUp())
		{
			eWhoFrom = (PlayerTypes) gDLL->getMPDiplomacyPlayer();
		}
		eWhoTo = GC.getGameINLINE().getActivePlayer();
	}
	else
	{
		eWhoFrom = GC.getGameINLINE().getActivePlayer();
		if ( gDLL->isDiplomacy() )
		{
			eWhoTo = (PlayerTypes) gDLL->getDiplomacyPlayer();
		}
		else if (gDLL->isMPDiplomacyScreenUp())
		{
			eWhoTo = (PlayerTypes) gDLL->getMPDiplomacyPlayer();
		}
	}

	eWhoDenies = eWhoFrom;

	IDInfo kTransport;
	if ( gDLL->isDiplomacy())
	{
		kTransport = gDLL->getDiplomacyTransport();
	}
	else if (gDLL->isMPDiplomacyScreenUp())
	{
		kTransport = gDLL->getMPDiplomacyTransport();
	}

	if ((eWhoFrom != NO_PLAYER) && (eWhoTo != NO_PLAYER))
	{
		//	Data1 is the heading
		switch (widgetDataStruct.m_iData1)
		{
		case TRADE_CITIES:
			szBuffer.assign(gDLL->getText("TXT_KEY_TRADE_CITIES"));
			eWhoDenies = (widgetDataStruct.m_bOption ? eWhoFrom : eWhoTo);
			break;
		case TRADE_PEACE:
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_MAKE_PEACE", GET_TEAM(GET_PLAYER(eWhoFrom).getTeam()).getName().GetCString(), GET_TEAM((TeamTypes)widgetDataStruct.m_iData2).getName().GetCString()));
			break;
		case TRADE_WAR:
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_MAKE_WAR", GET_TEAM(GET_PLAYER(eWhoFrom).getTeam()).getName().GetCString(), GET_TEAM((TeamTypes)widgetDataStruct.m_iData2).getName().GetCString()));
			break;
		case TRADE_EMBARGO:
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_STOP_TRADING", GET_TEAM(GET_PLAYER(eWhoFrom).getTeam()).getName().GetCString(), GET_TEAM((TeamTypes)widgetDataStruct.m_iData2).getName().GetCString()));
			break;
		case TRADE_GOLD:
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_GOLD"));
			break;
		case TRADE_YIELD:
			GAMETEXT.setYieldPriceHelp(szBuffer, GC.getGameINLINE().getActivePlayer(), (YieldTypes) widgetDataStruct.m_iData2);
			break;
		case TRADE_MAPS:
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_MAPS"));
			break;
		case TRADE_OPEN_BORDERS:
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_OPEN_BORDERS"));
			break;
		case TRADE_SHARE_VISION:
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_SHARE_VISION_STRING"));
			break;
		case TRADE_DEFENSIVE_PACT:
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_DEFENSIVE_PACT"));
			break;
		case TRADE_PERMANENT_ALLIANCE:
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_PERMANENT_ALLIANCE"));
			break;
		case TRADE_PEACE_TREATY:
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_PEACE_TREATY", GC.getDefineINT("PEACE_TREATY_LENGTH")));
			break;
		}

		setTradeItem(&item, ((TradeableItems)(widgetDataStruct.m_iData1)), widgetDataStruct.m_iData2, &kTransport);

		DenialTypes eDenial = GET_PLAYER(eWhoFrom).getTradeDenial(eWhoTo, item);
		if (eDenial != NO_DENIAL)
		{
			szTempBuffer.Format(L"%s: " SETCOLR L"%s" ENDCOLR, GET_PLAYER(eWhoDenies).getName(), TEXT_COLOR("COLOR_WARNING_TEXT"), GC.getDenialInfo(eDenial).getDescription());
			szBuffer.append(NEWLINE);
			szBuffer.append(szTempBuffer);
		}
	}
}


void CvDLLWidgetData::parseUnitModelHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvUnit* pHeadSelectedUnit = gDLL->getInterfaceIFace()->getHeadSelectedUnit();
	if (pHeadSelectedUnit != NULL)
	{
		GAMETEXT.setUnitHelp(szBuffer, pHeadSelectedUnit);
	}
}

void CvDLLWidgetData::parseCityDefenseHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL)
	{
		szBuffer.append(gDLL->getText("TXT_KEY_PLOT_BONUS", pHeadSelectedCity->getTotalDefense()));
	}
}


void CvDLLWidgetData::parseFlagHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvWString szTempBuffer;

	szTempBuffer.Format(SETCOLR L"%s" ENDCOLR, TEXT_COLOR("COLOR_HIGHLIGHT_TEXT"), GC.getCivilizationInfo(GC.getGameINLINE().getActiveCivilizationType()).getDescription());
	szBuffer.append(szTempBuffer);
	szBuffer.append(NEWLINE);

	GAMETEXT.parseLeaderTraits(szBuffer, GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getLeaderType(), GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCivilizationType());
}

void CvDLLWidgetData::parsePopulationHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pHeadSelectedCity;

	pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();

	if (pHeadSelectedCity != NULL)
	{
		szBuffer.assign(gDLL->getText("TXT_KEY_MISC_FOOD_THRESHOLD", pHeadSelectedCity->getFood(), pHeadSelectedCity->growthThreshold()));
	}
}

void CvDLLWidgetData::parseRebelHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData1 == 1)
	{
		CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
		if (pHeadSelectedCity != NULL)
		{
			szBuffer.assign(gDLL->getText("TXT_KEY_MISC_REBEL_HELP", pHeadSelectedCity->getRebelPercent()));
		}
	}
	else
	{
		szBuffer.assign(gDLL->getText("TXT_KEY_MISC_REBEL_HELP", GET_TEAM(GC.getGameINLINE().getActiveTeam()).getRebelPercent()));
	}
}


void CvDLLWidgetData::parseGreatGeneralHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (NO_PLAYER != GC.getGame().getActivePlayer())
	{
		GAMETEXT.parseGreatGeneralHelp(szBuffer, GET_PLAYER(GC.getGame().getActivePlayer()));
	}
}


void CvDLLWidgetData::parseSelectedHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pHeadSelectedCity;
	CvUnit* pHeadSelectedUnit;
	OrderData* pOrder;

	pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	pHeadSelectedUnit = gDLL->getInterfaceIFace()->getHeadSelectedUnit();

	if (pHeadSelectedCity != NULL)
	{
		pOrder = pHeadSelectedCity->getOrderFromQueue(widgetDataStruct.m_iData1);

		if (pOrder != NULL)
		{
			switch (pOrder->eOrderType)
			{
			case ORDER_TRAIN:
				GAMETEXT.setUnitHelp(szBuffer, ((UnitTypes)(pOrder->iData1)), false, false, pHeadSelectedCity);
				break;
			case ORDER_CONSTRUCT:
				GAMETEXT.setBuildingHelp(szBuffer, ((BuildingTypes)(pOrder->iData1)), false, false, pHeadSelectedCity);
				break;
			default:
				FAssertMsg(false, "eOrderType did not match valid options");
				break;
			}
		}
	}
}


void CvDLLWidgetData::parseBuildingHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.setBuildingHelp(szBuffer, ((BuildingTypes)(widgetDataStruct.m_iData1)), false, false, gDLL->getInterfaceIFace()->getHeadSelectedCity());
	}
}

void CvDLLWidgetData::parseSpecialBuildingHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity != NULL)
	{
		for (int iBuildingClass = 0; iBuildingClass < GC.getNumBuildingClassInfos(); ++iBuildingClass)
		{
			BuildingTypes eBuilding = (BuildingTypes) GC.getCivilizationInfo(pCity->getCivilizationType()).getCivilizationBuildings(iBuildingClass);
			if (eBuilding != NO_BUILDING)
			{
				if (GC.getBuildingInfo(eBuilding).getSpecialBuildingType() == widgetDataStruct.m_iData1)
				{
					if (pCity->isHasBuilding(eBuilding))
					{
						GAMETEXT.setBuildingHelp(szBuffer, eBuilding, false, false, pCity);
					}
				}
			}
		}
	}
}

void CvDLLWidgetData::parseTerrainHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.setTerrainHelp(szBuffer, (TerrainTypes)widgetDataStruct.m_iData1);
	}
}

void CvDLLWidgetData::parseYieldHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.setYieldsHelp(szBuffer, (YieldTypes)widgetDataStruct.m_iData1);
	}
}

void CvDLLWidgetData::parseFeatureHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.setFeatureHelp(szBuffer, (FeatureTypes)widgetDataStruct.m_iData1);
	}
}

void CvDLLWidgetData::parseUnitHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.setUnitHelp(szBuffer, (UnitTypes)widgetDataStruct.m_iData1, false, false, gDLL->getInterfaceIFace()->getHeadSelectedCity());
	}
}

void CvDLLWidgetData::parseShipCargoUnitHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	CvPlayer& pPlayer = GET_PLAYER(ePlayer);
	
	CvUnit* pUnit = pPlayer.getUnit(widgetDataStruct.m_iData1);
	if (pUnit != NULL)
	{
		GAMETEXT.setUnitHelp(szBuffer, pUnit, false, true);
	}
}

void CvDLLWidgetData::parseOneShipHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	PlayerTypes ePlayer =(PlayerTypes)widgetDataStruct.m_iData2;
	if (ePlayer != NO_PLAYER) {
		CvPlayer& kPlayer = GET_PLAYER(ePlayer);
		
		CvUnit* pUnit = kPlayer.getUnit(widgetDataStruct.m_iData1);
		if (pUnit != NULL)
		{
			GAMETEXT.setUnitHelp(szBuffer, pUnit, false, true);
		}
	}
}

void CvDLLWidgetData::parseTradeRouteImpExpListHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	
}
void CvDLLWidgetData::parseTradeRouteImpExpBoxHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	szBuffer.append(gDLL->getText("TXT_KEY_TRADE_ROUTE_IMP_EXP_BOX_HELP"));
}
void CvDLLWidgetData::parseTradeRouteBestYieldHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
	
	GAMETEXT.setYieldPriceHelp(szBuffer, GC.getGameINLINE().getActivePlayer(), eYield);
	CvWString szTemp;
			
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvTradeRoute* pTradeRoute = kPlayer.getTradeRoute(kPlayer.getCurrentTradeRouteId());
	if (pTradeRoute != NULL) {
		CvCity* pSouceCity = ::getCity(pTradeRoute->getSourceCity());
		CvCity* pDestCity = ::getCity(pTradeRoute->getDestinationCity());
		if (pSouceCity != NULL) {
			szBuffer.append(NEWLINE);
			szTemp.Format(SETCOLR L"%s\n" ENDCOLR , TEXT_COLOR("COLOR_GREEN"), pSouceCity->getName().GetCString());
			szBuffer.append(szTemp);
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_ROUTES_QUANTITY_HELP", pSouceCity->getYieldStored(eYield), pSouceCity->calculateNetYield(eYield)));
		}
		if (pDestCity != NULL) {
			szBuffer.append(NEWLINE);
			szTemp.Format(SETCOLR L"%s\n" ENDCOLR , TEXT_COLOR("COLOR_GREEN"), pDestCity->getName().GetCString());
			szBuffer.append(szTemp);
			szBuffer.append(gDLL->getText("TXT_KEY_TRADE_ROUTES_QUANTITY_HELP", pDestCity->getYieldStored(eYield), pDestCity->calculateNetYield(eYield)));
		}
	}
}

void CvDLLWidgetData::parseGroupShipHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	PlayerTypes ePlayer =(PlayerTypes)widgetDataStruct.m_iData2;
	if (ePlayer != NO_PLAYER) {
		CvPlayer& kPlayer = GET_PLAYER(ePlayer);
		
		CvUnit* pUnit = kPlayer.getUnit(widgetDataStruct.m_iData1);
		if (pUnit != NULL)
		{
			if (pUnit->getGroup() != NULL && pUnit->getGroup()->getNumUnits() > 1 ) {			
				GAMETEXT.setGroupShipHelp(szBuffer, pUnit);
			} else {
				GAMETEXT.setUnitHelp(szBuffer, pUnit, false, true);
			}
		}
	}
}

void CvDLLWidgetData::parseEuropeUnitHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	PlayerTypes ePlayer = (PlayerTypes)widgetDataStruct.m_iData2;
	if (ePlayer == NO_PLAYER) {
		ePlayer = GC.getGameINLINE().getActivePlayer();
	}
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getEuropeUnitById(widgetDataStruct.m_iData1);
	if (pUnit != NULL)
	{
		if (pUnit->getCrewFormationTurn() == 0)
		{
			GAMETEXT.setUnitHelp(szBuffer, pUnit, false, true);
		}
		else
		{
			szBuffer.append(gDLL->getText("TXT_KEY_NAME_IS_BEING_TRAINED", pUnit->getCrewFormationTurn()));
		}
	}
}

void CvDLLWidgetData::parseEuropeRecruitCrewHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	PlayerTypes ePlayer = (PlayerTypes)widgetDataStruct.m_iData2;
	if (ePlayer != NO_PLAYER) {
		CvTempUnit* pTempUnit = GET_PLAYER(ePlayer).getTempUnit(widgetDataStruct.m_iData1);
		if (pTempUnit != NULL) {
			GAMETEXT.setTempUnitHelp(szBuffer, pTempUnit);
		}
	}
}
void CvDLLWidgetData::parseEuropeEnrolUnitTypeHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	PlayerTypes ePlayer = (PlayerTypes)widgetDataStruct.m_iData2;
	if (ePlayer != NO_PLAYER)
	{
		CvPlayerAI& kPlayer = GET_PLAYER(ePlayer);
		UnitTypes eUnit = (UnitTypes) widgetDataStruct.m_iData1;
		if (NO_UNIT != eUnit)
		{
			int iCost = kPlayer.getEuropeUnitBuyPrice(eUnit);
			if (iCost >= 0)
			{
				iCost += iCost*GET_PLAYER(kPlayer.getParent()).getUnitMarketPrice((UnitClassTypes)GC.getUnitInfo(eUnit).getUnitClassType())/100;
				szBuffer.append(gDLL->getText("TXT_KEY_EUROPE_UNIT_BUY_PRICE", GC.getUnitInfo(eUnit).getTextKeyWide(), iCost));
				szBuffer.append(NEWLINE);
			}
			int iProba = kPlayer.calculateProbaImmigration(eUnit);
			szBuffer.append(gDLL->getText("TXT_KEY_EUROPE_UNIT_PERCENT_IMMIGRATION", iProba));
			if (gDLL->getChtLvl() > 0) {
				UnitClassTypes eUnitClass = (UnitClassTypes) GC.getUnitInfo(eUnit).getUnitClassType();
				szBuffer.append(NEWLINE);
				szBuffer.append(CvWString::format(L"\n getNumNeededUnitClass : %d", kPlayer.getNumNeededUnitClass(eUnitClass)));
				szBuffer.append(NEWLINE);
				szBuffer.append(CvWString::format(L"\n AI_EnrolledUnitWeight : %d", kPlayer.AI_EnrolledUnitWeight(eUnitClass)));
			}
			if (kPlayer.getEnrolUnitType((UnitClassTypes)GC.getUnitInfo(eUnit).getUnitClassType()) == 0) {
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_EUROPE_ENROL_UNIT_TYPE_HELP", GC.getUnitInfo(eUnit).getDescription()));
			} else {
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_EUROPE_RELATION_POINT_HELP", kPlayer.getImmigrationRelationPointRequired(eUnit)));
			}
		}
	}
}

void CvDLLWidgetData::parseTransfertEuropeToWarehouseHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvPlayer& pPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	
	YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
	int iCargoValue = widgetDataStruct.m_iData2;//can be neg if Transfert Warehouse to Europe
	if (eYield != NO_YIELD)
	{
		int iPrice = iCargoValue > 0 ? pPlayer.getBuyPriceForYield(eYield, iCargoValue) : -pPlayer.getSellPriceForYield(eYield, -iCargoValue);
		szBuffer.assign(CvWString::format(L"%d%c", iPrice, gDLL->getSymbolID(GOLD_CHAR)));
	}
}

void CvDLLWidgetData::parseRenameShipHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(widgetDataStruct.m_iData1);
	if (pUnit != NULL)
	{
		szBuffer.assign(gDLL->getText("TXT_KEY_MISC_PEDIA_RENAME_SHIP"));
	}
}

void CvDLLWidgetData::parseProfessionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.setProfessionHelp(szBuffer, (ProfessionTypes)widgetDataStruct.m_iData1);
	}
}

void CvDLLWidgetData::parsePediaBack(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
//	szBuffer = "Back";
	szBuffer.assign(gDLL->getText("TXT_KEY_MISC_PEDIA_BACK"));
}

void CvDLLWidgetData::parsePediaForward(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
//	szBuffer = "Forward";
	szBuffer.assign(gDLL->getText("TXT_KEY_MISC_PEDIA_FORWARD"));
}

void CvDLLWidgetData::parseBonusHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.setBonusHelp(szBuffer, (BonusTypes)widgetDataStruct.m_iData1);
	}
}

void CvDLLWidgetData::parsePromotionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.setPromotionHelp(szBuffer, (PromotionTypes)widgetDataStruct.m_iData1);
	}
}

void CvDLLWidgetData::parseUnitPromotionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvUnit* pUnit = ::getUnit(IDInfo((PlayerTypes) widgetDataStruct.m_iData1, widgetDataStruct.m_iData2));
	if (pUnit != NULL)
	{
		GAMETEXT.setUnitPromotionHelp(szBuffer, pUnit);
	}
}

void CvDLLWidgetData::parseCityYieldHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvCity* pCity = kPlayer.getCity(widgetDataStruct.m_iData2);
	if (NULL == pCity)
	{
		pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	}

	if (NULL != pCity)
	{
		GAMETEXT.setYieldHelp(szBuffer, *pCity, eYield);
	}
	else
	{
		GAMETEXT.setYieldPriceHelp(szBuffer, GC.getGameINLINE().getActivePlayer(), eYield);
	}

	PlayerTypes eActivePlayer = GC.getGameINLINE().getActivePlayer();
	for (int i = 0; i < GC.getNumProfessionInfos(); ++i)
	{
		ProfessionTypes eProfession = (ProfessionTypes) i;
		if (GET_PLAYER(eActivePlayer).isProfessionValid(eProfession, NO_UNIT))
		{
			int iNumRequired = GET_PLAYER(eActivePlayer).getYieldEquipmentAmount(eProfession, eYield);
			if (iNumRequired > 0)
			{
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_YIELD_NEEDED_FOR_PROFESSION", iNumRequired, GC.getProfessionInfo(eProfession).getTextKeyWide()));
			}
		}
	}
}

void CvDLLWidgetData::parseTradeYieldWithNatifHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvCity* pCity = kPlayer.getCity(widgetDataStruct.m_iData2);
	if (NULL == pCity)
	{
		pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	}
	GAMETEXT.setTradeYieldWithNatifHelp(szBuffer, *pCity, eYield);
}

void CvDLLWidgetData::parseRobYieldHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	GAMETEXT.setYieldPriceHelp(szBuffer, GC.getGameINLINE().getActivePlayer(), eYield);

	for (int i = 0; i < GC.getNumProfessionInfos(); ++i) {
		ProfessionTypes eProfession = (ProfessionTypes) i;
		if (kPlayer.isProfessionValid(eProfession, NO_UNIT)) {
			int iNumRequired = kPlayer.getYieldEquipmentAmount(eProfession, eYield);
			if (iNumRequired > 0) {
				szBuffer.append(NEWLINE);
				szBuffer.append(gDLL->getText("TXT_KEY_YIELD_NEEDED_FOR_PROFESSION", iNumRequired, GC.getProfessionInfo(eProfession).getTextKeyWide()));
			}
		}
	}
	szBuffer.append(NEWLINE);
	szBuffer.append(NEWLINE);
	if (widgetDataStruct.m_iData2 == 1) {
		szBuffer.append(gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_YIELD_HELP1"));
	} else if (widgetDataStruct.m_iData2 == 2) {
		szBuffer.append(gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_YIELD_HELP2"));
	}
}

void CvDLLWidgetData::parseDestinationCityHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvCity* pCity = kPlayer.getCity(widgetDataStruct.m_iData1);
	bool hasAutomaticTransportToEurope = widgetDataStruct.m_iData2;
	if (pCity != NULL) {
		szBuffer.append(gDLL->getText(hasAutomaticTransportToEurope ? "TXT_KEY_HELP_DESTINATION_CITY_AUTOMATIC_TRANSPORT" : "TXT_KEY_HELP_DESTINATION_CITY_NO_AUTOMATIC_TRANSPORT"));
	}
}

void CvDLLWidgetData::parseSeedInfoHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvWString szTempBuffer;
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	BonusTypes eBonus = (BonusTypes) widgetDataStruct.m_iData1;

	if (eBonus != NO_BONUS)
	{
		szTempBuffer.Format(L"%s", GC.getBonusInfo(eBonus).getDescription());
		szBuffer.append(szTempBuffer);
		szBuffer.append(NEWLINE);
		szBuffer.append(gDLL->getText("TXT_KEY_POPUP_RECEIVE_SEEDLING_HELP", GC.getBonusInfo(eBonus).getMinIdealLatitude(), GC.getBonusInfo(eBonus).getMaxIdealLatitude()));
		szBuffer.append(NEWLINE);
		szBuffer.append(gDLL->getText("TXT_KEY_POPUP_RECEIVE_SEEDLING_TYPES_OF_LAND_HELP"));
		
		int iNum = 0;
		for (int iI = 0; iI < GC.getNumTerrainInfos(); ++iI) {
			if (GC.getBonusInfo(eBonus).isTerrain((TerrainTypes) iI)) {
				if (iNum > 0) {
					szTempBuffer.Format(L", %s", GC.getTerrainInfo((TerrainTypes) iI).getDescription());
				} else {
					szTempBuffer.Format(L"%s", GC.getTerrainInfo((TerrainTypes) iI).getDescription());
				}
				szBuffer.append(szTempBuffer);
				iNum++;
			}
		}
		szBuffer.append(NEWLINE);
		szBuffer.append(NEWLINE);

		int iLoop;
		for (CvCity* pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
		{
			CvPlot* pPlot = pLoopCity->plot();
			szBuffer.append(gDLL->getText("TXT_KEY_MISC_CITY_LATITUDE", pLoopCity->getNameKey(), pPlot->getLatitude()));
			szBuffer.append(NEWLINE);
		}
		
		YieldTypes eYield = (YieldTypes) GC.getBonusInfo(eBonus).getMainYield();
		if (eYield != NO_YIELD) {
			szBuffer.append(NEWLINE);
			GAMETEXT.setYieldPriceHelp(szBuffer, kPlayer.getID(), eYield);
		}
	}
}

void CvDLLWidgetData::parseAgreementHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvWString szTempBuffer;
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(widgetDataStruct.m_iData1);
	bool bUnit = false;
	if (pUnit != NULL)
	{
		bUnit = true;
	}
	CvAgreement* pAgreement = kPlayer.getAgreement(widgetDataStruct.m_iData2);

	szBuffer.append(gDLL->getText("TXT_KEY_POPUP_AGREEMENT_HELP_1"));
	szBuffer.append(NEWLINE);
	szBuffer.append(gDLL->getText("TXT_KEY_POPUP_AGREEMENT_HELP_5", pAgreement->getSourceCityNameKey(), pAgreement->getDestinationCityNameKey(), GET_PLAYER(pAgreement->getDestinationCity().eOwner).getCivilizationAdjectiveKey()));
	szBuffer.append(NEWLINE);

	YieldTypes eYield1 = NO_YIELD;
	YieldTypes eYield2 = NO_YIELD;

	//Système provisoire tant qu'il n'y a que deux ressources
	if (pAgreement->getYieldOfType(TRADE_IMPORT, &eYield1) == 1 && pAgreement->getYieldOfType(TRADE_EXPORT, &eYield2) == 1)
	{
		int iDestYield = pAgreement->getOriginalAmount(eYield1) - pAgreement->getActualAmount(eYield1);
		int iSourceYield = pAgreement->getOriginalAmount(eYield2) - pAgreement->getActualAmount(eYield2);

		szBuffer.append(gDLL->getText("TXT_KEY_POPUP_AGREEMENT_HELP_2", iDestYield, GC.getYieldInfo(eYield1).getChar()));
		szBuffer.append(NEWLINE);

		szBuffer.append(gDLL->getText("TXT_KEY_POPUP_AGREEMENT_HELP_3", iSourceYield, GC.getYieldInfo(eYield2).getChar()));
		szBuffer.append(NEWLINE);

		int iTurn = std::max(0, pAgreement->getTurnCreated() + pAgreement->getTurnMax() - GC.getGameINLINE().getGameTurn());
		szBuffer.append(gDLL->getText("TXT_KEY_POPUP_AGREEMENT_HELP_4", iTurn));
		szBuffer.append(NEWLINE);
		if (pAgreement->getNumAssignedGroups() == 0)
		{
			szBuffer.append(gDLL->getText("TXT_KEY_POPUP_AGREEMENT_HELP_6"));
			szBuffer.append(NEWLINE);
		}
	}
}

void CvDLLWidgetData::parseTradeRoutesHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvWString szTempBuffer;
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(widgetDataStruct.m_iData1);
	bool bUnit = false;
	if (pUnit != NULL)
	{
		bUnit = true;
	}
	CvTradeRoute* pTradeRoute = kPlayer.getTradeRoute(widgetDataStruct.m_iData2);
	if (pTradeRoute == NULL) {
		return;
	}
		
	szTempBuffer.Format(gDLL->getText("TXT_KEY_TRADE_ROUTES_LOAD_AVERAGE"));
	szBuffer.append(szTempBuffer);
	szBuffer.append(NEWLINE);
	szTempBuffer.Format(gDLL->getText("TXT_KEY_TRADE_ROUTES_DESC_LOAD_AVERAGE_SHORT", pTradeRoute->getLoadedCargoPercent()));
	szBuffer.append(szTempBuffer);
	szBuffer.append(NEWLINE);
	szTempBuffer.Format(gDLL->getText("TXT_KEY_TRADE_ROUTES_DESC_AVERAGE_LOAD_X_ROTATION_SHORT", pTradeRoute->getLoadedCargoHistPercent(), GC.getNUM_TRADE_ROUTE_HIST_TURN()));
	szBuffer.append(szTempBuffer);
	szBuffer.append(NEWLINE);
	szTempBuffer.Format(gDLL->getText("TXT_KEY_POPUP_EXPORTED_RESOURCES"));
	szBuffer.append(szTempBuffer);
	szBuffer.append(NEWLINE);
	bool bExportedYield = false;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield) {
		YieldTypes eYield = (YieldTypes)iYield;
		if (pTradeRoute->getTradeType(eYield) == TRADE_EXPORT) {
			szTempBuffer.Format(L"%s %c\n", GC.getYieldInfo(eYield).getDescription(), GC.getYieldInfo(eYield).getChar());
			szBuffer.append(szTempBuffer);
			bExportedYield = true;
		}
	}
	if (!bExportedYield) {
		szTempBuffer.Format(gDLL->getText("TXT_KEY_POPUP_NO_RESOURCE"));
		szBuffer.append(szTempBuffer);
		szBuffer.append(NEWLINE);
	}
	
	szTempBuffer.Format(gDLL->getText("TXT_KEY_POPUP_IMPORTED_RESOURCES"));
	szBuffer.append(szTempBuffer);
	szBuffer.append(NEWLINE);
	bool bImportedYield = false;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield) {
		YieldTypes eYield = (YieldTypes)iYield;
		if (pTradeRoute->getTradeType(eYield) == TRADE_IMPORT) {
			szTempBuffer.Format(L"%s %c\n", GC.getYieldInfo(eYield).getDescription(), GC.getYieldInfo(eYield).getChar());
			szBuffer.append(szTempBuffer);
			bImportedYield = true;
		}
	}
	if (!bImportedYield) {
		szTempBuffer.Format(gDLL->getText("TXT_KEY_POPUP_NO_RESOURCE"));
		szBuffer.append(szTempBuffer);
	}
}

void CvDLLWidgetData::parseChooseTradeRoute(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(widgetDataStruct.m_iData1);
	if (pUnit != NULL)
	{
		szBuffer.assign(gDLL->getText("TXT_KEY_CHOOSE_TRADE_ROUTE"));
	}
}

void CvDLLWidgetData::parseAssignTradeRoute(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	/*CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(widgetDataStruct.m_iData1);
	if (pUnit != NULL)
	{
		if (pUnit->getGroup()->isAssignedTradeRoute(widgetDataStruct.m_iData2))
		{
			szBuffer.assign(gDLL->getText("TXT_KEY_UNASSIGN_ROUTE"));
		}
		else
		{
			szBuffer.assign(gDLL->getText("TXT_KEY_ASSIGN_ROUTE"));
		}
	}*/
}

void CvDLLWidgetData::parseReceiveMoveCargoToTransportHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(widgetDataStruct.m_iData1);
	if (pUnit != NULL)
	{
		GAMETEXT.setUnitHelp(szBuffer, pUnit, false, false);
	}
}

void CvDLLWidgetData::parseUnloadNewCargoHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	szBuffer.assign(gDLL->getText("TXT_KEY_COMMAND_UNLOAD_NEW_CARGO"));
}

void CvDLLWidgetData::parseEventHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	GAMETEXT.setEventHelp(szBuffer, (EventTypes)widgetDataStruct.m_iData1, widgetDataStruct.m_iData2, GC.getGameINLINE().getActivePlayer());
}

void CvDLLWidgetData::parseImprovementHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.setImprovementHelp(szBuffer, (ImprovementTypes)widgetDataStruct.m_iData1);
	}
}

void CvDLLWidgetData::parseCivicHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.parseCivicInfo(szBuffer, (CivicTypes)widgetDataStruct.m_iData1);
	}
}

void CvDLLWidgetData::parseCivilizationHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != 0)
	{
		GAMETEXT.parseCivInfos(szBuffer, (CivilizationTypes)widgetDataStruct.m_iData1);
	}
}

void CvDLLWidgetData::parseLeaderHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	if (widgetDataStruct.m_iData2 != -1)
	{
		GAMETEXT.parseLeaderTraits(szBuffer, (LeaderHeadTypes)widgetDataStruct.m_iData1, (CivilizationTypes)widgetDataStruct.m_iData2);
	}
}

void CvDLLWidgetData::parseCloseScreenHelp(CvWStringBuffer& szBuffer)
{
	szBuffer.assign(gDLL->getText("TXT_KEY_MISC_CLOSE_SCREEN"));
}

void CvDLLWidgetData::parseDescriptionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer, bool bMinimal)
{
	CivilopediaPageTypes eType = (CivilopediaPageTypes)widgetDataStruct.m_iData1;
	switch (eType)
	{
	case CIVILOPEDIA_PAGE_UNIT:
		{
			UnitTypes eUnit = (UnitTypes)widgetDataStruct.m_iData2;
			if (NO_UNIT != eUnit)
			{
				szBuffer.assign(bMinimal ? GC.getUnitInfo(eUnit).getDescription() : gDLL->getText("TXT_KEY_MISC_HISTORICAL_INFO", GC.getUnitInfo(eUnit).getTextKeyWide()));
			}
		}
		break;
	case CIVILOPEDIA_PAGE_PROFESSION:
		{
			ProfessionTypes eProfession = (ProfessionTypes)widgetDataStruct.m_iData2;
			if (NO_PROFESSION != eProfession)
			{
				szBuffer.assign(bMinimal ? GC.getProfessionInfo(eProfession).getDescription() : gDLL->getText("TXT_KEY_MISC_HISTORICAL_INFO", GC.getProfessionInfo(eProfession).getTextKeyWide()));
			}
		}
		break;
	case CIVILOPEDIA_PAGE_BUILDING:
		{
			BuildingTypes eBuilding = (BuildingTypes)widgetDataStruct.m_iData2;
			if (NO_BUILDING != eBuilding)
			{
				szBuffer.assign(bMinimal ? GC.getBuildingInfo(eBuilding).getDescription() : gDLL->getText("TXT_KEY_MISC_HISTORICAL_INFO", GC.getBuildingInfo(eBuilding).getTextKeyWide()));
			}
		}
		break;
	case CIVILOPEDIA_PAGE_BONUS:
		{
			BonusTypes eBonus = (BonusTypes)widgetDataStruct.m_iData2;
			if (NO_BONUS != eBonus)
			{
				szBuffer.assign(bMinimal ? GC.getBonusInfo(eBonus).getDescription() : gDLL->getText("TXT_KEY_MISC_HISTORICAL_INFO", GC.getBonusInfo(eBonus).getTextKeyWide()));
			}
		}
		break;
	case CIVILOPEDIA_PAGE_IMPROVEMENT:
		{
			ImprovementTypes eImprovement = (ImprovementTypes)widgetDataStruct.m_iData2;
			if (NO_IMPROVEMENT != eImprovement)
			{
				szBuffer.assign(bMinimal ? GC.getImprovementInfo(eImprovement).getDescription() : gDLL->getText("TXT_KEY_MISC_HISTORICAL_INFO", GC.getImprovementInfo(eImprovement).getTextKeyWide()));
			}
		}
		break;
	case CIVILOPEDIA_PAGE_PROMOTION:
		{
			PromotionTypes ePromo = (PromotionTypes)widgetDataStruct.m_iData2;
			if (NO_PROMOTION != ePromo)
			{
				szBuffer.assign(bMinimal ? GC.getPromotionInfo(ePromo).getDescription() : gDLL->getText("TXT_KEY_MISC_HISTORICAL_INFO", GC.getPromotionInfo(ePromo).getTextKeyWide()));
			}
		}
		break;
	case CIVILOPEDIA_PAGE_CIV:
		{
			CivilizationTypes eCiv = (CivilizationTypes)widgetDataStruct.m_iData2;
			if (NO_CIVILIZATION != eCiv)
			{
				szBuffer.assign(bMinimal ? GC.getCivilizationInfo(eCiv).getDescription() : gDLL->getText("TXT_KEY_MISC_HISTORICAL_INFO", GC.getCivilizationInfo(eCiv).getTextKeyWide()));
			}
		}
		break;
	case CIVILOPEDIA_PAGE_LEADER:
		{
			LeaderHeadTypes eLeader = (LeaderHeadTypes)widgetDataStruct.m_iData2;
			if (NO_LEADER != eLeader)
			{
				szBuffer.assign(bMinimal ? GC.getLeaderHeadInfo(eLeader).getDescription() : gDLL->getText("TXT_KEY_MISC_HISTORICAL_INFO", GC.getLeaderHeadInfo(eLeader).getTextKeyWide()));
			}
		}
		break;
	case CIVILOPEDIA_PAGE_CIVIC:
		{
			CivicTypes eCivic = (CivicTypes)widgetDataStruct.m_iData2;
			if (NO_CIVIC != eCivic)
			{
				szBuffer.assign(bMinimal ? GC.getCivicInfo(eCivic).getDescription() : gDLL->getText("TXT_KEY_MISC_HISTORICAL_INFO", GC.getCivicInfo(eCivic).getTextKeyWide()));
			}
		}
		break;
	case CIVILOPEDIA_PAGE_CONCEPT:
		{
			ConceptTypes eConcept = (ConceptTypes)widgetDataStruct.m_iData2;
			if (NO_CONCEPT != eConcept)
			{
				szBuffer.assign(GC.getConceptInfo(eConcept).getDescription());
			}
		}
		break;
	default:
		break;
	}
}

void CvDLLWidgetData::parseKillDealHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
//	szBuffer = "Click to cancel";
	CvWString szTemp;
	szTemp = szBuffer.getCString();
	CvDeal* pDeal = GC.getGameINLINE().getDeal(widgetDataStruct.m_iData1);
	if (NULL != pDeal)
	{
		if (pDeal->isCancelable(GC.getGameINLINE().getActivePlayer(), &szTemp))
		{
			szTemp = gDLL->getText("TXT_KEY_MISC_CLICK_TO_CANCEL");
		}
	}

	szBuffer.assign(szTemp);
}


void CvDLLWidgetData::doDealKill(CvWidgetDataStruct &widgetDataStruct)
{
	CvDeal* pDeal = GC.getGameINLINE().getDeal(widgetDataStruct.m_iData1);
	if (pDeal != NULL)
	{
		if (!pDeal->isCancelable(GC.getGameINLINE().getActivePlayer()))
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_TEXT);
			pInfo->setText(gDLL->getText("TXT_KEY_POPUP_CANNOT_CANCEL_DEAL"));
			gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), true);
		}
		else
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_DEAL_CANCELED);
			pInfo->setData1(pDeal->getID());
			pInfo->setOption1(false);
			gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), true);
		}
	}
}


void CvDLLWidgetData::doRefreshMilitaryAdvisor(CvWidgetDataStruct &widgetDataStruct)
{
	CyArgsList argsList;
	argsList.add(widgetDataStruct.m_iData1);
	argsList.add(widgetDataStruct.m_iData2);
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "refreshMilitaryAdvisor", argsList.makeFunctionArgs());
}

void CvDLLWidgetData::doCityUnitAssignCitizen(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData)
{
	// citizen dragged and dropped somewhere
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity == NULL || pCity->getOwnerINLINE() != GC.getGameINLINE().getActivePlayer())
	{
		return;
	}

	switch (destinationWidgetData.m_eWidgetType)
	{
	case WIDGET_CITY_UNIT_ASSIGN_PROFESSION:
		if (destinationWidgetData.m_iData2 > -1)
		{
			CvUnit* pUnit = pCity->getPopulationUnitById(sourceWidgetData.m_iData1);
			if (pUnit != NULL) {
				ProfessionTypes eProfession = (ProfessionTypes) destinationWidgetData.m_iData2;

				if (pCity->isAvailableProfessionSlot(eProfession, pUnit))
				{
					gDLL->sendDoTask(pCity->getID(), TASK_CHANGE_PROFESSION, sourceWidgetData.m_iData1, eProfession, false, false, false, false);
				} else {
					for (int i = 0; i < pCity->getPopulation(); ++i) {
						CvUnit* pUnit = pCity->getPopulationUnitByIndex(i);
						if (pUnit->getProfession() == destinationWidgetData.m_iData2) {
							gDLL->sendDoTask(pCity->getID(), TASK_REPLACE_CITIZEN, sourceWidgetData.m_iData1, pUnit->getID(), false, false, false, false);
							break;
						}
					}
				}
				if (eProfession == PROFESSION_STUDENT && pUnit->getProfession() != PROFESSION_STUDENT) {
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_EDUCATION, pCity->getID(), pUnit->getID(), 0);
					gDLL->getInterfaceIFace()->addPopup(pInfo, pUnit->getOwner(), true, true);
				}
			}
		}
		break;

	case WIDGET_ASSIGN_CITIZEN_TO_PLOT:
		doAssignCitizenToPlot(gDLL->getInterfaceIFace()->getHeadSelectedCity(), destinationWidgetData.m_iData1, sourceWidgetData.m_iData1);
		break;

	case WIDGET_CITIZEN:
		gDLL->sendDoTask(pCity->getID(), TASK_REPLACE_CITIZEN, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, true, false, false, false);
		break;

	case WIDGET_EJECT_CITIZEN:
		{
			CvUnit* pUnit = pCity->getPopulationUnitById(sourceWidgetData.m_iData1);
			if (pUnit != NULL)
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_PROFESSION, pCity->getID(), pUnit->getID(), 0);
				gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
			}
		}
		break;

	case WIDGET_RECEIVE_MOVE_CARGO_TO_TRANSPORT:
		{
			CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
			CvUnit* pUnit = pCity->getPopulationUnitById(sourceWidgetData.m_iData1);
			CvUnit* pTransport = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(destinationWidgetData.m_iData1);

			if (NULL != pUnit && NULL != pTransport)
			{
				if (pUnit->canLoadUnit(pTransport, pTransport->plot(), true))
				{
					gDLL->sendDoTask(pCity->getID(), TASK_EJECT_TO_TRANSPORT, pUnit->getID(), pTransport->getID(), false, false, false, false);
				}
			}
		}
		break;

	default:
		break;
	}
}

void CvDLLWidgetData::doMoveCargoToCity(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData)
{
	// cargo inside the city screen dropped somewhere

	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity == NULL || pCity->getOwnerINLINE() != GC.getGameINLINE().getActivePlayer())
	{
		return;
	}

	switch (destinationWidgetData.m_eWidgetType)
	{
	case WIDGET_HELP_YIELD:
	case WIDGET_RECEIVE_MOVE_CARGO_TO_CITY:
	case WIDGET_MOVE_CARGO_TO_TRANSPORT:
		if (gDLL->shiftKey())
		{
			CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(sourceWidgetData.m_iData2);
			if (pUnit != NULL)
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SELECT_YIELD_AMOUNT, pUnit->getYield(), sourceWidgetData.m_iData2, COMMAND_UNLOAD);
				pInfo->setOption1(false);
				gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
			}
		}
		else
		{
			gDLL->sendDoCommand(sourceWidgetData.m_iData2, COMMAND_UNLOAD, -1, -1, false);
		}
		break;

	case WIDGET_CITY_UNIT_ASSIGN_PROFESSION:
		{
			int iUnitId = sourceWidgetData.m_iData2;
			CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(iUnitId);
			if (pUnit == NULL) {
				break;
			}
			ProfessionTypes eProfession = (ProfessionTypes) destinationWidgetData.m_iData2;
			if (eProfession == NO_PROFESSION) {
				break;
			}

			if (pUnit->canHaveProfession(eProfession, true, pCity->plot())) {
				if (pCity->isAvailableProfessionSlot(eProfession, pUnit)) {
					gDLL->sendDoCommand(iUnitId, COMMAND_PROFESSION, eProfession, -1, false);
				} else {
					for (int i = 0; i < pCity->getPopulation(); ++i) {
						CvUnit* pLoopUnit = pCity->getPopulationUnitByIndex(i);
						if (pLoopUnit->getProfession() == eProfession) {
							gDLL->sendDoTask(pCity->getID(), TASK_REPLACE_CITIZEN, iUnitId, pLoopUnit->getID(), false, false, false, false);
							break;
						}
					}
				}
				if (eProfession == PROFESSION_STUDENT) {
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_EDUCATION, pCity->getID(), pUnit->getID(), 0);
					gDLL->getInterfaceIFace()->addPopup(pInfo, pUnit->getOwner(), true, true);
				}
			}
		}
		break;

	case WIDGET_ASSIGN_CITIZEN_TO_PLOT:
		doAssignCitizenToPlot(gDLL->getInterfaceIFace()->getHeadSelectedCity(), destinationWidgetData.m_iData1, sourceWidgetData.m_iData2);
		break;

	case WIDGET_CITIZEN:
		{
			CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(sourceWidgetData.m_iData2);
			if (NULL != pUnit)
			{
				gDLL->sendDoTask(pCity->getID(), TASK_REPLACE_CITIZEN, sourceWidgetData.m_iData2, destinationWidgetData.m_iData1, true, false, false, false);
			}
		}
		break;

	case WIDGET_EJECT_CITIZEN:
		{
			CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(sourceWidgetData.m_iData2);
			if (pUnit != NULL)
			{
				gDLL->sendDoCommand(sourceWidgetData.m_iData2, COMMAND_UNLOAD, -1, -1, false);
			}
		}
		break;

	case WIDGET_RECEIVE_MOVE_CARGO_TO_TRANSPORT:
		{
			CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(sourceWidgetData.m_iData2);
			CvUnit* pTransport = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(destinationWidgetData.m_iData1);

			if (NULL != pUnit && NULL != pTransport)
			{
				gDLL->sendDoCommand(sourceWidgetData.m_iData2, COMMAND_LOAD_UNIT, pTransport->getOwnerINLINE(), destinationWidgetData.m_iData1, false);
			}
		}
		break;
	default:
		break;
	}
}

void CvDLLWidgetData::doMoveShipCargo(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData)
{
	switch (destinationWidgetData.m_eWidgetType)
	{
	case WIDGET_SHIP_CARGO:
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_TRANSFER_UNIT_IN_EUROPE, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, -1);
			// Move cargo from one ship to another (shift split?)
		}
		break;
	case WIDGET_ONE_SHIP:
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_TRANSFER_UNIT_IN_EUROPE, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, -1);
			// Move cargo from one ship to another (shift split?)
		}
		break;
	case WIDGET_GROUP_SHIP:
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SHIP_JOIN_GROUP, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, -1);
		}
		break;
	case WIDGET_DOCK:
		{
			CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(sourceWidgetData.m_iData1);
			if (pUnit != NULL && pUnit->isCargo())
			{
				if (!pUnit->isGoods())
				{
					gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_UNLOAD_UNIT_TO_EUROPE, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, -1);
				}
				else
				{
					if (gDLL->shiftKey())
					{
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SELECT_YIELD_AMOUNT, pUnit->getYield(), sourceWidgetData.m_iData1, PLAYER_ACTION_SELL_YIELD_UNIT);
						pInfo->setOption1(false);
						pInfo->setOption2(true);
						gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
					}
					else
					{
						gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SELL_YIELD_UNIT, 0, pUnit->getYieldStored(), sourceWidgetData.m_iData1);
					}
				}
			}
		}
		break;
	case WIDGET_MOVE_CARGO_TO_TRANSPORT:
		{
			PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
			CvUnit* pUnit = GET_PLAYER(ePlayer).getUnit(sourceWidgetData.m_iData1);
			if (pUnit != NULL && pUnit->isGoods())
			{
				if (gDLL->shiftKey())
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SELECT_YIELD_AMOUNT, pUnit->getYield(), sourceWidgetData.m_iData1, PLAYER_ACTION_SELL_YIELD_UNIT);
					pInfo->setOption1(false);
					pInfo->setOption2(true);
					gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
				}
				else
				{
					gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SELL_YIELD_UNIT, 0, pUnit->getYieldStored(), sourceWidgetData.m_iData1);
				}
			}
		}
		break;

	case WIDGET_SAIL:
		if (sourceWidgetData.m_iData2 == -1)
		{
			gDLL->sendDoCommand(sourceWidgetData.m_iData1, COMMAND_SAIL_TO_EUROPE, destinationWidgetData.m_iData1, -1, false);
		}
		break;

	default:
		break;
	}
}

void CvDLLWidgetData::doMoveShipToGroup(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData)
{
	switch (destinationWidgetData.m_eWidgetType)
	{
	case WIDGET_SAIL:
		if (sourceWidgetData.m_iData2 == -1)
		{
			gDLL->sendDoCommand(sourceWidgetData.m_iData1, COMMAND_SAIL_TO_EUROPE, destinationWidgetData.m_iData1, -1, false);
		}
		break;
	case WIDGET_ONE_SHIP:
	case WIDGET_GROUP_SHIP:
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_TRADE_ROUTE_MOVE_TRANSPORT_TO_TRANSPORT, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, -1);
		}
		break;
	
	default:
		break;
	}
}

void CvDLLWidgetData::doMoveDockUnit(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData)
{
	switch (destinationWidgetData.m_eWidgetType)
	{
	case WIDGET_SHIP_CARGO:
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_LOAD_UNIT_FROM_EUROPE, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, -1);
		}
		break;

	default:
		break;
	}
}

void CvDLLWidgetData::doMoveYieldImportExport(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData)
{
	switch (destinationWidgetData.m_eWidgetType)
	{
	case WIDGET_TRADE_ROUTE_IMP_EXP_LIST:
	case WIDGET_TRADE_ROUTE_IMP_EXP_BOX:
		{
			int iTradeRouteId = destinationWidgetData.m_iData1;
			int iTradeType = destinationWidgetData.m_iData2;
			int iYield = sourceWidgetData.m_iData1;
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_CHANGE_TRADE_TYPE, iTradeRouteId, iTradeType, iYield);
		}
		break;

	default:
		break;
	}
}

void CvDLLWidgetData::doUnitIntoCity(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData)
{
	// garrison from city screen citizen dragged and dropped somewhere

	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity == NULL || pCity->getOwnerINLINE() != GC.getGameINLINE().getActivePlayer())
	{
		return;
	}

	switch (destinationWidgetData.m_eWidgetType)
	{
	case WIDGET_CITY_UNIT_ASSIGN_PROFESSION:
		{
			int iUnitId = sourceWidgetData.m_iData1;
			CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(iUnitId);
			if (pUnit == NULL)
			{
				break;
			}
			ProfessionTypes eProfession = (ProfessionTypes) destinationWidgetData.m_iData2;
			if (eProfession == NO_PROFESSION)
			{
				break;
			}

			if (pUnit->canHaveProfession(eProfession, true, pCity->plot()))
			{
				if (pCity->isAvailableProfessionSlot(eProfession, pUnit))
				{
					gDLL->sendDoCommand(iUnitId, COMMAND_PROFESSION, eProfession, -1, false);
				}
				else
				{
					for (int i = 0; i < pCity->getPopulation(); ++i)
					{
						CvUnit* pLoopUnit = pCity->getPopulationUnitByIndex(i);
						if (pLoopUnit->getProfession() == eProfession)
						{
							gDLL->sendDoTask(pCity->getID(), TASK_REPLACE_CITIZEN, iUnitId, pLoopUnit->getID(), false, false, false, false);
							break;
						}
					}
				}
				if (eProfession == PROFESSION_STUDENT) {
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_EDUCATION, pCity->getID(), pUnit->getID(), 0);
					gDLL->getInterfaceIFace()->addPopup(pInfo, pUnit->getOwner(), true, true);
				}
			}
		}
		break;

	case WIDGET_ASSIGN_CITIZEN_TO_PLOT:
		doAssignCitizenToPlot(gDLL->getInterfaceIFace()->getHeadSelectedCity(), destinationWidgetData.m_iData1, sourceWidgetData.m_iData1);
		break;

	case WIDGET_CITIZEN:
		{
			CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(sourceWidgetData.m_iData1);
			if (NULL != pUnit)
			{
				gDLL->sendDoTask(pCity->getID(), TASK_REPLACE_CITIZEN, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, true, false, false, false);
				CvUnit* pDestUnit = pCity->getPopulationUnitById(destinationWidgetData.m_iData1);
				if (pDestUnit != NULL) {
					if (pDestUnit->getProfession() == PROFESSION_STUDENT) {
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_EDUCATION, pCity->getID(), pUnit->getID(), 0);
						gDLL->getInterfaceIFace()->addPopup(pInfo, pUnit->getOwner(), true, true);
					}
				}
			}
		}
		break;

	case WIDGET_RECEIVE_MOVE_CARGO_TO_TRANSPORT:
		{
			CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(sourceWidgetData.m_iData1);
			CvUnit* pTransport = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(destinationWidgetData.m_iData1);

			if (NULL != pUnit && NULL != pTransport)
			{
				gDLL->sendDoCommand(sourceWidgetData.m_iData1, COMMAND_LOAD_UNIT, pTransport->getOwnerINLINE(), destinationWidgetData.m_iData1, false);
			}
		}
		break;

	default:
		break;
	}
}

void CvDLLWidgetData::doMoveCargoToTransport(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData)
{
	// a yield dropped somewhere (could be in Europe or in City)

	switch (destinationWidgetData.m_eWidgetType)
	{
	case WIDGET_RECEIVE_MOVE_CARGO_TO_TRANSPORT:
	case WIDGET_MOVE_CARGO_TO_CITY:
		// City screen
		{
			CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
			if (pCity == NULL || pCity->getOwnerINLINE() != GC.getGameINLINE().getActivePlayer())
			{
				break;
			}

			if (gDLL->shiftKey())
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SELECT_YIELD_AMOUNT, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, COMMAND_LOAD_YIELD);
				pInfo->setOption1(true);
				gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
			}
			else
			{
				gDLL->sendDoCommand(destinationWidgetData.m_iData1, COMMAND_LOAD_YIELD, sourceWidgetData.m_iData1, -1, false);
			}
		}
		break;

	case WIDGET_SHIP_CARGO:
		// Europe
		if (gDLL->shiftKey())
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SELECT_YIELD_AMOUNT, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, PLAYER_ACTION_BUY_YIELD_UNIT);
			pInfo->setOption1(true);
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
		else
		{
			PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
			CvUnit* pUnit = GET_PLAYER(ePlayer).getUnit(destinationWidgetData.m_iData1);
			int iAmount = pUnit->getLoadYieldAmount((YieldTypes) sourceWidgetData.m_iData1);

			if (iAmount > 0)
			{
				gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_BUY_YIELD_UNIT, sourceWidgetData.m_iData1, iAmount, destinationWidgetData.m_iData1);
			}
		}
		break;

	default:
		break;
	}
}
void CvDLLWidgetData::doTradeRouteMoveTransportToTransport(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData)
{
	switch (destinationWidgetData.m_eWidgetType)
	{
	case WIDGET_TRADE_ROUTE_MOVE_TRANSPORT_TO_TRANSPORT:
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_TRADE_ROUTE_MOVE_TRANSPORT_TO_TRANSPORT, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, -1);
		}
		break;

	case WIDGET_ADD_TRANSPORT_AREA_TO_TRADE_ROUTES:
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_CHOOSE_TRADE_ROUTE, sourceWidgetData.m_iData1, destinationWidgetData.m_iData1, true);
		}
		break;

	default:
		break;
	}
}


void CvDLLWidgetData::doDoubleClickCitizen(const CvWidgetDataStruct& widgetDataStruct)
{
	CvCity* pHeadSelectedCity;
	if (widgetDataStruct.m_iData2 != -1)
	{
		pHeadSelectedCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(widgetDataStruct.m_iData2);
	}
	else
	{
		pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	}
	if (pHeadSelectedCity != NULL && pHeadSelectedCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		CvUnit* pUnit = pHeadSelectedCity->getPopulationUnitById(widgetDataStruct.m_iData1);
		if (pUnit != NULL)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_PROFESSION, pHeadSelectedCity->getID(), pUnit->getID());
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
	}
}

void CvDLLWidgetData::doDoubleClickDock(const CvWidgetDataStruct& widgetDataStruct)
{
	CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getEuropeUnitById(widgetDataStruct.m_iData1);
	if (pUnit != NULL)
	{
		if (pUnit->getCrewFormationTurn() == 0)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_PROFESSION_IN_EUROPE, -1, pUnit->getID(), 0);
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
	}
}

void CvDLLWidgetData::doClickRenameShip(const CvWidgetDataStruct& widgetDataStruct)
{
	CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(widgetDataStruct.m_iData1);
	if (pUnit != NULL)
	{
		gDLL->getEventReporterIFace()->unitRename(pUnit);
	}
}

void CvDLLWidgetData::doCreateTradeRoute(const CvWidgetDataStruct& widgetDataStruct)
{
	if (GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getNumCities() >= 2)
	{
		gDLL->getEventReporterIFace()->createTradeRoute(GC.getGameINLINE().getActivePlayer());
	}
}

void CvDLLWidgetData::doYieldImportExport(const CvWidgetDataStruct& widgetDataStruct)
{
	/*CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL && pHeadSelectedCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_YIELD_IMPORT_EXPORT, gDLL->getInterfaceIFace()->getHeadSelectedCity()->getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
	}*/
}

void CvDLLWidgetData::doControlHospital(const CvWidgetDataStruct& widgetDataStruct)
{
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity != NULL && pCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer() && pCity->canManageHospital())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_HOSPITAL, pCity->getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
	}
}

void CvDLLWidgetData::doYieldProduction(const CvWidgetDataStruct& widgetDataStruct)
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL && pHeadSelectedCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_YIELD_PRODUCTION, gDLL->getInterfaceIFace()->getHeadSelectedCity()->getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
	}
}

void CvDLLWidgetData::doControlTavern(const CvWidgetDataStruct& widgetDataStruct)
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL && pHeadSelectedCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		/*CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_TAVERN, gDLL->getInterfaceIFace()->getHeadSelectedCity()->getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);*/
	}
}

void CvDLLWidgetData::doAgreementList(const CvWidgetDataStruct &widgetDataStruct)
{
	CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_AGREEMENT_LIST);
	gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
}

void CvDLLWidgetData::doSeedlingList(const CvWidgetDataStruct &widgetDataStruct)
{
	CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SEEDLING_LIST);
	gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
}

void CvDLLWidgetData::doDestroyCity(const CvWidgetDataStruct& widgetDataStruct)
{
	CvCity* pHeadSelectedCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pHeadSelectedCity != NULL && pHeadSelectedCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		if (pHeadSelectedCity->canBeAbandoned()) {
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_DESTROY_CITY, gDLL->getInterfaceIFace()->getHeadSelectedCity()->getID());
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
	}
}
void CvDLLWidgetData::doRecruitCrew(const CvWidgetDataStruct& widgetDataStruct)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer != NO_PLAYER && ePlayer == (PlayerTypes)widgetDataStruct.m_iData2) {
		CvTempUnit* pTempUnit = GET_PLAYER(ePlayer).getTempUnit(widgetDataStruct.m_iData1);
		if (pTempUnit != NULL) {
			if (pTempUnit->requestTraining()) {
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ASK_SAILOR_FORMATION, pTempUnit->getID());
				gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
			} else {
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_RECRUIT_CREW, pTempUnit->getID(), pTempUnit->getPrice(), 0);
				gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
			}
		}
	}
}
void CvDLLWidgetData::doLoadAllTheYieldInGroup(const CvWidgetDataStruct& widgetDataStruct)
{
	YieldTypes eYield =  (YieldTypes) widgetDataStruct.m_iData1;
	if (eYield != NO_YIELD) {
		CvSelectionGroup* pSelectionGroup = gDLL->getInterfaceIFace()->getSelectionList();
		if (NULL == pSelectionGroup)
		{
			return;
		}

		CvPlot* pPlot = pSelectionGroup->plot();
		if (NULL == pPlot)
		{
			return;
		}

		CvCity* pCity = pPlot->getPlotCity();
		if (NULL == pCity)
		{
			return;
		}
		int iYieldStored = pCity->getYieldStored(eYield);
		int iTotalYieldLoaded = 0;
		
		CvUnit* pTransportWithSpace = NULL;

		int iNumUnits = pSelectionGroup->getNumUnits();
		for (int i = 0; i < iNumUnits; ++i)
		{
			CvUnit* pLoopUnit = pSelectionGroup->getUnitAt(i);
			if (NULL != pLoopUnit)
			{
				if (pLoopUnit->canLoadYield(pLoopUnit->plot(), eYield, false))
				{
					int iMaxAmount = pLoopUnit->getMaxLoadYieldAmount(eYield, -1, -1);
					if (iMaxAmount > 0) {
						int iTransfert = std::min(iMaxAmount, iYieldStored-iTotalYieldLoaded);
						if (iTransfert > 0) {
							int iSpace = pLoopUnit->getSpaceNewCargo();
							gDLL->sendDoCommand(pLoopUnit->getID(), COMMAND_LOAD_YIELD, eYield, iTransfert, false);
							iTotalYieldLoaded += iTransfert;
							iSpace -= iTransfert;
							if (iSpace > 0){
								pTransportWithSpace = pLoopUnit;
							} 
						}
					}
				}
			}
		}
		if (pTransportWithSpace != NULL) {
			gDLL->sendDoCommand(pTransportWithSpace->getID(), COMMAND_LOAD_CARGO, -1, -1, false);
		}
	}
}
void CvDLLWidgetData::doEnrolUnitType(const CvWidgetDataStruct& widgetDataStruct, bool bIncrease)
{
	UnitTypes eUnit = (UnitTypes) widgetDataStruct.m_iData1;
	if (NO_UNIT != eUnit && GC.getGameINLINE().getActivePlayer() == (PlayerTypes)widgetDataStruct.m_iData2)
	{
		gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_ENROL_UNIT_TYPE, GC.getUnitInfo(eUnit).getUnitClassType(), bIncrease, -1);
	}
}
void CvDLLWidgetData::doTipsEnrolUnitType(const CvWidgetDataStruct& widgetDataStruct)
{
	UnitTypes eUnit = (UnitTypes) widgetDataStruct.m_iData1;
	if (NO_UNIT != eUnit && GC.getGameINLINE().getActivePlayer() == (PlayerTypes)widgetDataStruct.m_iData2)
	{
		gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_ENROL_UNIT_TYPE, GC.getUnitInfo(eUnit).getUnitClassType(), true, -1);
		gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_Enrol_DIRTY_BIT, true);
	}
}
void CvDLLWidgetData::doSendUpSeaway(const CvWidgetDataStruct& widgetDataStruct)
{
	CvPlayer& pPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	int iType = widgetDataStruct.m_iData2;
	if (iType == 1 || iType == 2) {
		gDLL->sendPlayerAction(pPlayer.getID(), PLAYER_ACTION_MOVE_SEAWAY_RANK, widgetDataStruct.m_iData1, 1, -1);
	}
}
void CvDLLWidgetData::doSendDownSeaway(const CvWidgetDataStruct& widgetDataStruct)
{
	CvPlayer& pPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	int iType = widgetDataStruct.m_iData2;
	if (iType == 0 || iType == 2) {
		gDLL->sendPlayerAction(pPlayer.getID(), PLAYER_ACTION_MOVE_SEAWAY_RANK, widgetDataStruct.m_iData1, 2, -1);
	}
}
void CvDLLWidgetData::doSelectDestinationColony(const CvWidgetDataStruct& widgetDataStruct)
{
	CvPlayer& pPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = pPlayer.getEuropeUnitById(widgetDataStruct.m_iData1);
	if (pUnit != NULL) {
		if (pUnit->getCrewFormationTurn() == 0) {
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_CITY_DESTINATION_IN_EUROPE, pUnit->getID(), -1, -1);
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
	}
}
void CvDLLWidgetData::doAddNewResourceIntoTradeRoute(const CvWidgetDataStruct& widgetDataStruct)
{
	CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_TRADE_ROUTE_ADD_RESOURCE_POPUP, widgetDataStruct.m_iData1, widgetDataStruct.m_iData2, -1);
	gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
}
void CvDLLWidgetData::doProposalToImmigrant(const CvWidgetDataStruct& widgetDataStruct)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer != NO_PLAYER) {
		CvTempUnit* pTempUnit = GET_PLAYER(ePlayer).getTempUnit(widgetDataStruct.m_iData1);
		if (pTempUnit != NULL) {
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_RECRUIT_IMMIGRANT, widgetDataStruct.m_iData1, pTempUnit->getPrice(), 0);
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
	}
}

void CvDLLWidgetData::doArsenalManagement(const CvWidgetDataStruct& widgetDataStruct)
{
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity != NULL && pCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer() && pCity->canManageArsenal())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ARSENAL_MANAGEMENT, pCity->getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
	}
}

void CvDLLWidgetData::doBuyShipUsed(const CvWidgetDataStruct& widgetDataStruct)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer != NO_PLAYER) {
		CvTempUnit* pTempUnit = GET_PLAYER(ePlayer).getTempUnit(widgetDataStruct.m_iData1);
		if (pTempUnit != NULL) {
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_BUY_USED_SHIP, widgetDataStruct.m_iData1, pTempUnit->getPrice(), 0);
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
	}
}
void CvDLLWidgetData::doBuyNewShip(const CvWidgetDataStruct& widgetDataStruct)
{
	CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_BUY_NEW_SHIP_IN_EUROPE, widgetDataStruct.m_iData1);
	gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
}

void CvDLLWidgetData::doTransfertEuropeToWarehouse(const CvWidgetDataStruct& widgetDataStruct)
{
	CvPlayer& pPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	PlayerTypes eEuropePlayer = pPlayer.getParent();
	if (eEuropePlayer != NO_PLAYER)
	{
		CvPlayer& pEuropePlayer = GET_PLAYER(eEuropePlayer);
		YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
		int iCargoValue = widgetDataStruct.m_iData2;//can be neg if Transfert Warehouse to Europe
		if (eYield != NO_YIELD)
		{
			int iBuyPrice = pPlayer.getBuyPriceForYield(eYield, iCargoValue);
			int iSellPrice = pPlayer.getSellPriceForYield(eYield, -iCargoValue);
			pEuropePlayer.changeEuropeWarehouseYield(eYield, -iCargoValue);
			pPlayer.changeEuropeWarehouseYield(eYield, iCargoValue);
			if (iCargoValue > 0)
			{
				pPlayer.changeEuropeBuyTradeYield(eYield, iCargoValue);
				pPlayer.changeEuropeGoldBuyTradeYield(eYield, iBuyPrice);
				pPlayer.changeGold(-iBuyPrice);
			}
			else
			{
				int iTaxSellGold = iSellPrice*(pPlayer.getTaxRate() + pPlayer.getEuropeLoanPercent())/100;
				int iSellGold = iSellPrice - iTaxSellGold;
				pPlayer.changeEuropeTaxTradeYield(eYield, iTaxSellGold);
				pPlayer.changeEuropeSellTradeYield(eYield, -iCargoValue);
				pPlayer.changeEuropeGoldSellTradeYield(eYield, iSellGold);
				pPlayer.changeGold(iSellGold);
			}
			gDLL->getInterfaceIFace()->setDirty(EuropeC2Screen_DIRTY_BIT, true);
		}
	}
}
void CvDLLWidgetData::doUnloadNewCargo(const CvWidgetDataStruct& widgetDataStruct)
{
	CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(widgetDataStruct.m_iData1);
	if (pUnit != NULL)
	{
		gDLL->sendDoCommand(pUnit->getID(), COMMAND_UNLOAD_NEW_CARGO, -1, -1, false);
	}
}

void CvDLLWidgetData::doUnloadYield(const CvWidgetDataStruct& widgetDataStruct)
{
	CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(widgetDataStruct.m_iData2);
	YieldTypes eYield = (YieldTypes) widgetDataStruct.m_iData1;
	if (pUnit != NULL && eYield != NO_YIELD) {
		CvPlot* pPlot = pUnit->plot();
		if (pPlot != NULL) {
			CvCity* pCity = pPlot->getPlotCity();
			if (pCity != NULL) {
				gDLL->sendDoCommand(pUnit->getID(), COMMAND_UNLOAD_NEW_CARGO_IN_CITY, eYield, 0, false);
			}
		}
	}
}

void CvDLLWidgetData::doChooseTradeRoute(const CvWidgetDataStruct& widgetDataStruct)
{
	CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(widgetDataStruct.m_iData1);
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	
	if (pUnit != NULL && pCity != NULL && pCity->getOwner() == GC.getGameINLINE().getActivePlayer()) {
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_TRADE_ROUTES, pUnit->getID(), pCity->getID(), -1);
		gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true, true);
	}
}
void CvDLLWidgetData::doAssignTradeRoute(const CvWidgetDataStruct& widgetDataStruct)
{
	/*CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(widgetDataStruct.m_iData1);
	if (pUnit != NULL)
	{
		gDLL->sendDoCommand(pUnit->getID(), COMMAND_ASSIGN_TRADE_ROUTE, widgetDataStruct.m_iData2, !pUnit->getGroup()->isAssignedTradeRoute(widgetDataStruct.m_iData2), false);
	}*/
}

void CvDLLWidgetData::parseProductionModHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (NULL != pCity)
	{
		GAMETEXT.setProductionHelp(szBuffer, *pCity);
	}
}

void CvDLLWidgetData::parseLeaderheadHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	GAMETEXT.parseLeaderHeadHelp(szBuffer, (PlayerTypes)widgetDataStruct.m_iData1, (PlayerTypes)widgetDataStruct.m_iData2);
}

void CvDLLWidgetData::parseLeaderheadKingHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	GAMETEXT.parseLeaderheadKingHelp(szBuffer, (PlayerTypes)widgetDataStruct.m_iData1, (YieldTypes) widgetDataStruct.m_iData2);
}

void CvDLLWidgetData::parseLeaderLineHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer)
{
	GAMETEXT.parseLeaderLineHelp(szBuffer, (PlayerTypes)widgetDataStruct.m_iData1, (PlayerTypes)widgetDataStruct.m_iData2);
}

void CvDLLWidgetData::parseScoreHelp(CvWidgetDataStruct& widgetDataStruct, CvWStringBuffer& szBuffer)
{
	GAMETEXT.setScoreHelp(szBuffer, (PlayerTypes)widgetDataStruct.m_iData1);
}

void CvDLLWidgetData::parseImportExportHelp(CvWidgetDataStruct& widgetDataStruct, CvWStringBuffer& szBuffer)
{
	szBuffer.append(gDLL->getText("TXT_KEY_HELP_EDIT_IMPORTS"));
}

void CvDLLWidgetData::parseHospitalHelp(CvWidgetDataStruct& widgetDataStruct, CvWStringBuffer& szBuffer)
{
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity != NULL && pCity->canManageHospital()) {
		szBuffer.append(gDLL->getText("TXT_KEY_HELP_HOSPITAL_PRESENT"));
	} else {
		szBuffer.append(gDLL->getText("TXT_KEY_HELP_HOSPITAL_NONE"));
	}
}

void CvDLLWidgetData::parseYieldProductionHelp(CvWidgetDataStruct& widgetDataStruct, CvWStringBuffer& szBuffer)
{
	szBuffer.append(gDLL->getText("TXT_KEY_HELP_EDIT_YIELD_PRODUCTION"));
}

void CvDLLWidgetData::parseTavernHelp(CvWidgetDataStruct& widgetDataStruct, CvWStringBuffer& szBuffer)
{
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity != NULL)
	{
		BuildingTypes cBuilding = (BuildingTypes) -1;
		for (int iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
		{
			BuildingTypes eBuilding = (BuildingTypes)GC.getCivilizationInfo(pCity->getCivilizationType()).getCivilizationBuildings(iI);
			if (eBuilding != NO_BUILDING)
			{
				if (GC.getBuildingInfo(eBuilding).getRefBuilding() == 4)
				{
					if (pCity->isHasRealBuilding(eBuilding))
					{
						szBuffer.append(gDLL->getText("TXT_KEY_HELP_TAVERN_PRESENT"));
					}
					else
					{
						szBuffer.append(gDLL->getText("TXT_KEY_HELP_TAVERN_NONE"));
					}
				}
			}
		}
	}
}

void CvDLLWidgetData::parseDestructionHelp(CvWidgetDataStruct& widgetDataStruct, CvWStringBuffer& szBuffer)
{
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity != NULL && pCity->canBeAbandoned()) {
		szBuffer.append(gDLL->getText("TXT_KEY_HELP_DESTROY_CITY"));
	} else {
		szBuffer.append(gDLL->getText("TXT_KEY_HELP_CAN_NOT_DESTROY_CITY"));
	}
}

void CvDLLWidgetData::parseArsenalManagementHelp(CvWidgetDataStruct& widgetDataStruct, CvWStringBuffer& szBuffer)
{
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity != NULL && pCity->canManageArsenal()) {
		szBuffer.append(gDLL->getText("TXT_KEY_HELP_ARSENAL_PRESENT"));
	} else {
		szBuffer.append(gDLL->getText("TXT_KEY_HELP_ARSENAL_NONE"));
	}
}

void CvDLLWidgetData::parseBuyShipUsedHelp(CvWidgetDataStruct& widgetDataStruct, CvWStringBuffer& szBuffer)
{
	szBuffer.append(gDLL->getText("TXT_KEY_HELP_BUY_SHIP_USED"));
}

void CvDLLWidgetData::parseBuyNewShipHelp(CvWidgetDataStruct& widgetDataStruct, CvWStringBuffer& szBuffer)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	UnitTypes eUnit = (UnitTypes)widgetDataStruct.m_iData1;
	if (eUnit == NO_UNIT)
	{
		return;
	}
	int iTurn = kPlayer.getUnitTurnRemaining((UnitClassTypes)GC.getUnitInfo(eUnit).getUnitClassType());
	if (iTurn > 0)
	{
		szBuffer.append(gDLL->getText("TXT_KEY_HELP_BUY_NEW_SHIP_TURN", iTurn));
	}
	else
	{
		szBuffer.append(gDLL->getText("TXT_KEY_HELP_BUY_NEW_SHIP"));

	}
}

void CvDLLWidgetData::parseEjectCitizenHelp(CvWidgetDataStruct& widgetDataStruct, CvWStringBuffer& szBuffer)
{
	CvCity* pCity = gDLL->getInterfaceIFace()->getHeadSelectedCity();
	if (pCity != NULL)
	{
		CvUnit* pUnit = GET_PLAYER(pCity->getOwnerINLINE()).getUnit(widgetDataStruct.m_iData1);
		if (pUnit != NULL)
		{
			GAMETEXT.setUnitHelp(szBuffer, pUnit, false, false);
		}
		else
		{
			szBuffer.append(gDLL->getText("TXT_KEY_EJECT_CITIZEN"));
		}
	}
}

void CvDLLWidgetData::doAssignCitizenToPlot(CvCity* pCity, int iPlotIndex, int iUnitId)
{
	if (NULL != pCity)
	{
		if (pCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			CvUnit* pOtherUnit = pCity->getUnitWorkingPlot(iPlotIndex);
			if (pOtherUnit != NULL)
			{
				gDLL->sendDoTask(pCity->getID(), TASK_REPLACE_CITIZEN, iUnitId, pOtherUnit->getID(), true, false, false, false);
			}
			else
			{
				gDLL->sendDoTask(pCity->getID(), TASK_CHANGE_WORKING_PLOT, iPlotIndex, iUnitId, true, false, false, false);
			}
		}
	}
}

void CvDLLWidgetData::doGoToCity(const CvWidgetDataStruct &widgetDataStruct)
{
	CvCity* pCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(widgetDataStruct.m_iData1);
	if (pCity != NULL)
	{
		gDLL->getInterfaceIFace()->selectCity(pCity);
	}
}
