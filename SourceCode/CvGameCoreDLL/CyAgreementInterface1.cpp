#include "CvGameCoreDLL.h"
#include "CyAgreement.h"
//
// published python interface for CyAgreement
//
void CyAgreementPythonInterface1(python::class_<CyAgreement>& x)
{
	OutputDebugString("Python Extension Module - CyAgreementPythonInterface1\n");
	x
		.def("getID", &CyAgreement::getID, "int ()")
		.def("getMerchantName", &CyAgreement::getMerchantName, "wstring ()")
		.def("getSourceCity", &CyAgreement::getSourceCity, "IDInfo ()")
		.def("getDestinationCity", &CyAgreement::getDestinationCity, "IDInfo ()")
		.def("getSourceCityName", &CyAgreement::getSourceCityName, "wstring ()")
		.def("getDestinationCityName", &CyAgreement::getDestinationCityName, "wstring ()")
		.def("getTurnMax", &CyAgreement::getTurnMax, "int ()")
		.def("getTurnCreated", &CyAgreement::getTurnCreated, "int ()")
		.def("getImportYield", &CyAgreement::getImportYield, "int ()")
		.def("getExportYield", &CyAgreement::getExportYield, "int ()")
		.def("getFixedPrice", &CyAgreement::getFixedPrice, "int ()")
		.def("getPrime", &CyAgreement::getPrime, "int ()")
		.def("getNumAssignedGroups", &CyAgreement::getNumAssignedGroups, "int ()")
		.def("getTurnLeft", &CyAgreement::getTurnLeft, "int ()")
		.def("getOriginalAmount", &CyAgreement::getOriginalAmount, "int (int)")
		.def("getActualAmount", &CyAgreement::getActualAmount, "int (int)")
		;
}
