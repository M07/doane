// selectionGroupAI.cpp

#include "CvGameCoreDLL.h"
#include "CvSelectionGroupAI.h"
#include "CvDLLButtonPopup.h"
#include "CvPlayerAI.h"
#include "CvMap.h"
#include "CvPlot.h"
#include "CvDLLInterfaceIFaceBase.h"
#include "CvTeamAI.h"
#include "CvDLLEntityIFaceBase.h"
#include "CvGameTextMgr.h"
#include "CvGameCoreUtils.h"
#include "FProfiler.h"
#include "CVInfos.h"
#include "CvTradeRoute.h"
#include "CvAgreement.h"
#include "CvSeaway.h"
#include "CvConvoy.h"

// Public Functions...

CvSelectionGroupAI::CvSelectionGroupAI()
{
	AI_reset();
}

CvSelectionGroupAI::~CvSelectionGroupAI()
{
	AI_uninit();
}


void CvSelectionGroupAI::AI_init()
{
	AI_reset();

	//--------------------------------
	// Init other game data
}

void CvSelectionGroupAI::AI_uninit()
{
}

void CvSelectionGroupAI::AI_reset()
{
	AI_uninit();

	m_iMissionAIX = INVALID_PLOT_COORD;
	m_iMissionAIY = INVALID_PLOT_COORD;
	m_iGroupIdForMerging = -1;
	m_iSeawayId = -1;

	m_bForceSeparate = false;
	m_bShouldWaitForMerging = false;

	m_bStopConvoy = false;
	//m_bResupplyAuto = true;
	m_bAutoHeal = true;

	m_eMissionAIType = NO_MISSIONAI;

	m_missionAIUnit.reset();

	m_bGroupAttack = false;
	m_iGroupAttackX = -1;
	m_iGroupAttackY = -1;
}


void CvSelectionGroupAI::AI_separate()
{
	CLLNode<IDInfo>* pEntityNode;
	CvUnit* pLoopUnit;

	pEntityNode = headUnitNode();

	while (pEntityNode != NULL)
	{
		pLoopUnit = ::getUnit(pEntityNode->m_data);
		pEntityNode = nextUnitNode(pEntityNode);

		pLoopUnit->joinGroup(NULL);
		if (pLoopUnit->plot()->getTeam() == getTeam())
		{
			pLoopUnit->getGroup()->pushMission(MISSION_SKIP);
		}
	}
}

void CvSelectionGroupAI::AI_seperateNonAI(UnitAITypes eUnitAI)
{
	CLLNode<IDInfo>* pEntityNode;
	CvUnit* pLoopUnit;

	pEntityNode = headUnitNode();

	while (pEntityNode != NULL)
	{
		pLoopUnit = ::getUnit(pEntityNode->m_data);
		pEntityNode = nextUnitNode(pEntityNode);
		if (pLoopUnit->AI_getUnitAIType() != eUnitAI)
		{
			pLoopUnit->joinGroup(NULL);
			if (pLoopUnit->plot()->getTeam() == getTeam())
			{
				pLoopUnit->getGroup()->pushMission(MISSION_SKIP);
			}
		}
	}
}

void CvSelectionGroupAI::AI_seperateAI(UnitAITypes eUnitAI)
{
	CLLNode<IDInfo>* pEntityNode;
	CvUnit* pLoopUnit;

	pEntityNode = headUnitNode();

	while (pEntityNode != NULL)
	{
		pLoopUnit = ::getUnit(pEntityNode->m_data);
		pEntityNode = nextUnitNode(pEntityNode);
		if (pLoopUnit->AI_getUnitAIType() == eUnitAI)
		{
			pLoopUnit->joinGroup(NULL);
			if (plot()->getTeam() == getTeam())
			{
				pLoopUnit->getGroup()->pushMission(MISSION_SKIP);
			}
		}
	}
}
// Returns true if the group has become busy...
bool CvSelectionGroupAI::AI_update()
{
	CLLNode<IDInfo>* pEntityNode;
	CvUnit* pLoopUnit;
	bool bDead;
	bool bFollow;

	PROFILE_FUNC();

	FAssert(getOwnerINLINE() != NO_PLAYER);

	if (!AI_isControlled())
	{
		return false;
	}

	if (getNumUnits() == 0)
	{
		return false;
	}

	if (isForceUpdate())
	{
		clearMissionQueue(); // XXX ???
		setActivityType(ACTIVITY_AWAKE);
		setForceUpdate(false);

		// if we are in the middle of attacking with a stack, cancel it
		AI_cancelGroupAttack();
	}

	FAssert(!(GET_PLAYER(getOwnerINLINE()).isAutoMoves()));

	int iTempHack = 0; // XXX

	bDead = false;

	bool bFailedAlreadyFighting = false;
	while ((m_bGroupAttack && !bFailedAlreadyFighting) || readyToMove())
	{
		iTempHack++;
		if (iTempHack > 2)
		{
			FAssert(false);
			CvUnit* pHeadUnit = getHeadUnit();
			if (NULL != pHeadUnit)
			{
				if (GC.getLogging())
				{
					TCHAR szOut[1024];
					CvWString szTempString;
					getUnitAIString(szTempString, pHeadUnit->AI_getUnitAIType());
					sprintf(szOut, "Unit stuck in loop: %S(%S)[%d, %d] (%S)", pHeadUnit->getName().GetCString(), GET_PLAYER(pHeadUnit->getOwnerINLINE()).getName(),
						pHeadUnit->getX_INLINE(), pHeadUnit->getY_INLINE(), szTempString.GetCString());
					gDLL->messageControlLog(szOut);
				}

				pHeadUnit->finishMoves();
			}
			break;
		}

		// if we want to force the group to attack, force another attack
		if (m_bGroupAttack)
		{
			m_bGroupAttack = false;

			groupAttack(m_iGroupAttackX, m_iGroupAttackY, MOVE_DIRECT_ATTACK, bFailedAlreadyFighting);
		}
		// else pick AI action
		else
		{
			CvUnit* pHeadUnit = getHeadUnit();

			if (pHeadUnit == NULL || pHeadUnit->isDelayedDeath())
			{
				break;
			}

			if (pHeadUnit->AI_update())
			{
				// AI_update returns true when we should abort the loop and wait until next slice
				break;
			}
		}

		if (doDelayedDeath())
		{
			bDead = true;
			break;
		}

		// if no longer group attacking, and force separate is true, then bail, decide what to do after group is split up
		// (UnitAI of head unit may have changed)
		if (!m_bGroupAttack && AI_isForceSeparate())
		{
			AI_separate();	// pointers could become invalid...
			return true;
		}
	}

	if (!bDead)
	{
		CvUnit* pHeadUnit = getHeadUnit();
		if (pHeadUnit != NULL && pHeadUnit->movesLeft() > 0)
		{
			if ((pHeadUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE) && AI_isControlled())
			{
				pEntityNode = headUnitNode();

				while (pEntityNode != NULL)
				{
					pLoopUnit = ::getUnit(pEntityNode->m_data);
					pEntityNode = nextUnitNode(pEntityNode);
					if (AI_isControlled())
					{
						pLoopUnit->AI_europeUpdate();
					}
				}
			}
		}

		if (!isHuman())
		{
			bFollow = false;

			// if we not group attacking, then check for follow action
			if (!m_bGroupAttack)
			{
				pEntityNode = headUnitNode();

				while ((pEntityNode != NULL) && readyToMove(true))
				{
					pLoopUnit = ::getUnit(pEntityNode->m_data);
					pEntityNode = nextUnitNode(pEntityNode);

					if (pLoopUnit->canMove())
					{
						if (pLoopUnit->AI_follow())
						{
							bFollow = true;
							break;
						}
					}
				}
			}

			if (doDelayedDeath())
			{
				bDead = true;
			}

			if (!bDead)
			{
				if (!bFollow && readyToMove(true))
				{
					pushMission(MISSION_SKIP);
				}
			}
		}
	}

	if (bDead)
	{
		return true;
	}

	return (isBusy() || isCargoBusy());
}


// Returns attack odds out of 100 (the higher, the better...)
int CvSelectionGroupAI::AI_attackOdds(const CvPlot* pPlot, bool bPotentialEnemy) const
{
	CvUnit* pAttacker;

	FAssert(getOwnerINLINE() != NO_PLAYER);

	int iOdds = 0;
	pAttacker = AI_getBestGroupAttacker(pPlot, bPotentialEnemy, iOdds);

	if (pAttacker == NULL)
	{
		return 0;
	}

	if (pPlot->getBestDefender(NO_PLAYER, getOwnerINLINE(), pAttacker, !bPotentialEnemy, bPotentialEnemy) == NULL)
	{
		return 100;
	}

	return iOdds;
}


CvUnit* CvSelectionGroupAI::AI_getBestGroupAttacker(const CvPlot* pPlot, bool bPotentialEnemy, int& iUnitOdds, bool bForce, bool bNoBlitz) const
{
	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pLoopUnit;
	CvUnit* pBestUnit;
	int iValue;
	int iBestValue;
	int iOdds;
	int iBestOdds;

	iBestValue = 0;
	iBestOdds = 0;
	pBestUnit = NULL;

	pUnitNode = headUnitNode();

	bool bIsHuman = (pUnitNode != NULL) ? GET_PLAYER(::getUnit(pUnitNode->m_data)->getOwnerINLINE()).isHuman() : true;

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);

		if (!pLoopUnit->isDead())
		{
			bool bCanAttack = false;
			bCanAttack = pLoopUnit->canAttack();

			if (bCanAttack && bNoBlitz && pLoopUnit->isBlitz() && pLoopUnit->isMadeAttack())
			{
				bCanAttack = false;
			}

			if (bCanAttack)
			{
				if (bForce || pLoopUnit->canMove())
				{
					if (bForce || pLoopUnit->canMoveInto(pPlot, /*bAttack*/ true, /*bDeclareWar*/ bPotentialEnemy))
					{
						iOdds = pLoopUnit->AI_attackOdds(pPlot, bPotentialEnemy);

						iValue = iOdds;
						FAssertMsg(iValue > 0, "iValue is expected to be greater than 0");

						// if non-human, prefer the last unit that has the best value (so as to avoid splitting the group)
						if (iValue > iBestValue || (!bIsHuman && iValue > 0 && iValue == iBestValue))
						{
							iBestValue = iValue;
							iBestOdds = iOdds;
							pBestUnit = pLoopUnit;
						}
					}
				}
			}
		}
	}

	iUnitOdds = iBestOdds;
	return pBestUnit;
}

CvUnit* CvSelectionGroupAI::AI_getBestGroupSacrifice(const CvPlot* pPlot, bool bPotentialEnemy, bool bForce, bool bNoBlitz) const
{
	int iBestValue = 0;
	CvUnit* pBestUnit = NULL;

	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);

		if (!pLoopUnit->isDead())
		{
			bool bCanAttack = false;
			bCanAttack = pLoopUnit->canAttack();

			if (bCanAttack && bNoBlitz && pLoopUnit->isBlitz() && pLoopUnit->isMadeAttack())
			{
				bCanAttack = false;
			}

			if (bCanAttack)
			{
				if (bForce || pLoopUnit->canMove())
				{
					if (bForce || pLoopUnit->canMoveInto(pPlot, true))
					{
						int iValue = pLoopUnit->AI_sacrificeValue(pPlot);
						FAssertMsg(iValue > 0, "iValue is expected to be greater than 0");

						// we want to pick the last unit of highest value, so pick the last unit with a good value
						if (iValue >= iBestValue)
						{
							iBestValue = iValue;
							pBestUnit = pLoopUnit;
						}
					}
				}
			}
		}
	}

	return pBestUnit;
}
CvUnit* CvSelectionGroupAI::AI_getArtilleryUnit(const CvPlot* pPlot) const
{
	CvUnit* pBestUnit = NULL;


	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);
		ProfessionTypes pProfession = pLoopUnit->getProfession();

		if (!pLoopUnit->isDead())
		{
			bool bCanAttack = false;
			bCanAttack = pLoopUnit->canAttack();

			if (bCanAttack && pLoopUnit->isMadeAttack())
			{
				bCanAttack = false;
			}

			if (bCanAttack)
			{
				if (pLoopUnit->canMove())
				{
					if (pLoopUnit->canMoveInto(pPlot, true))
					{
						if (pProfession != NO_PROFESSION && GC.getProfessionInfo(pProfession).getYieldEquipmentAmount(YIELD_CANNON) > 0)
						{
							return pLoopUnit;
						}
					}
				}
			}
		}
	}

	return pBestUnit;
}

// Returns ratio of strengths of stacks times 100
// (so 100 is even ratio, numbers over 100 mean this group is more powerful than the stack on a plot)
int CvSelectionGroupAI::AI_compareStacks(const CvPlot* pPlot, bool bPotentialEnemy, bool bCheckCanAttack, bool bCheckCanMove) const
{
	FAssert(pPlot != NULL);

	int	compareRatio;
	DomainTypes eDomainType = getDomainType();

	// choose based on the plot, not the head unit (mainly for transport carried units)
	if (pPlot->isWater())
		eDomainType = DOMAIN_SEA;
	else
		eDomainType = DOMAIN_LAND;

	compareRatio = AI_sumStrength(pPlot, eDomainType, bCheckCanAttack, bCheckCanMove);
	compareRatio *= 100;

	PlayerTypes eOwner = getOwnerINLINE();
	if (eOwner == NO_PLAYER)
	{
		eOwner = getHeadOwner();
	}
	FAssert(eOwner != NO_PLAYER);

	int defenderSum = pPlot->AI_sumStrength(NO_PLAYER, getOwnerINLINE(), eDomainType, true, !bPotentialEnemy, bPotentialEnemy);
	compareRatio /= std::max(1, defenderSum);

	return compareRatio;
}

int CvSelectionGroupAI::AI_sumStrength(const CvPlot* pAttackedPlot, DomainTypes eDomainType, bool bCheckCanAttack, bool bCheckCanMove) const
{
	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pLoopUnit;
	int	strSum = 0;

	pUnitNode = headUnitNode();

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);

		if (!pLoopUnit->isDead())
		{
			bool bCanAttack = pLoopUnit->canAttack();

			if (!bCheckCanAttack || bCanAttack)
			{
				if (!bCheckCanMove || pLoopUnit->canMove())
					if (!bCheckCanMove || pAttackedPlot == NULL || pLoopUnit->canMoveInto(pAttackedPlot, /*bAttack*/ true, /*bDeclareWar*/ true))
						if (eDomainType == NO_DOMAIN || pLoopUnit->getDomainType() == eDomainType)
							strSum += pLoopUnit->currEffectiveStr(pAttackedPlot, pLoopUnit);
			}
		}
	}

	return strSum;
}

int CvSelectionGroupAI::AI_sumBaseCombatStr() const
{
	CLLNode<IDInfo>* pUnitNode;
	int	strSum = 0;

	CvUnit* pLoopUnit;
	pUnitNode = headUnitNode();

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);

		if (pLoopUnit->canAttack())
		{
			strSum += pLoopUnit->baseCombatStr();
		}
	}

	return strSum;
}

void CvSelectionGroupAI::AI_queueGroupAttack(int iX, int iY)
{
	m_bGroupAttack = true;

	m_iGroupAttackX = iX;
	m_iGroupAttackY = iY;
}

inline void CvSelectionGroupAI::AI_cancelGroupAttack()
{
	m_bGroupAttack = false;
}

inline bool CvSelectionGroupAI::AI_isGroupAttack()
{
	return m_bGroupAttack;
}

bool CvSelectionGroupAI::AI_isControlled()
{
	return (!isHuman() || isAutomated());
}


bool CvSelectionGroupAI::AI_isDeclareWar(const CvPlot* pPlot)
{
	FAssert(getHeadUnit() != NULL);

	if (isHuman())
	{
		return false;
	}
	else
	{
		bool bLimitedWar = false;
		if (pPlot != NULL)
		{
			TeamTypes ePlotTeam = pPlot->getTeam();
			if (ePlotTeam != NO_TEAM)
			{
				WarPlanTypes eWarplan = GET_TEAM(getTeam()).AI_getWarPlan(ePlotTeam);
				if (eWarplan == WARPLAN_LIMITED)
				{
					bLimitedWar = true;
				}
			}
		}

		CvUnit* pHeadUnit = getHeadUnit();

		if (pHeadUnit != NULL)
		{
			switch (pHeadUnit->AI_getUnitAIType())
			{
			case UNITAI_UNKNOWN:
			case UNITAI_COLONIST:
			case UNITAI_SETTLER:
			case UNITAI_WORKER:
			case UNITAI_AGRONOMIST:
			case UNITAI_NATIVE_LEARN:
			case UNITAI_MISSIONARY:
			case UNITAI_SCOUT:
			case UNITAI_WAGON:
			case UNITAI_WAGON_ARMY:
			case UNITAI_TREASURE:
			case UNITAI_YIELD:
			case UNITAI_GENERAL:
				return false;
				break;

			case UNITAI_DEFENSIVE:
			case UNITAI_OFFENSIVE:
			case UNITAI_COUNTER:
			case UNITAI_RESSOURCES_DESTROYER:
			case UNITAI_ASSAULT_COLONY:
				return true;
				break;

			case UNITAI_TRANSPORT_SEA:
			case UNITAI_NATIVE_TRADE:
			case UNITAI_NAVAL_DISCOVERING:
				return false;
				break;

			case UNITAI_ASSAULT_SEA:
			case UNITAI_COMBAT_SEA:
			case UNITAI_PIRATE_SEA:
				return true;
				break;

			default:
				FAssert(false);
				break;
			}
		}
	}

	return false;
}


CvPlot* CvSelectionGroupAI::AI_getMissionAIPlot()
{
	return GC.getMapINLINE().plotSorenINLINE(m_iMissionAIX, m_iMissionAIY);
}


bool CvSelectionGroupAI::AI_isForceSeparate()
{
	return m_bForceSeparate;
}


void CvSelectionGroupAI::AI_makeForceSeparate()
{
	m_bForceSeparate = true;
}

bool CvSelectionGroupAI::AI_shouldWaitForMerging() const
{
	return m_bShouldWaitForMerging;
}

void CvSelectionGroupAI::AI_setShouldWaitForMerging(bool bNewValue)
{
	m_bShouldWaitForMerging = bNewValue;
}

// Options for trade routes
bool CvSelectionGroupAI::AI_shouldStopConvoy() const
{
	return m_bStopConvoy;
}

void CvSelectionGroupAI::AI_setShouldStopConvoy(bool bNewValue)
{
	m_bStopConvoy = bNewValue;
}
/*bool CvSelectionGroupAI::AI_shouldResupplyAuto() const
{
return m_bResupplyAuto;
}

void CvSelectionGroupAI::AI_setShouldResupplyAuto(bool bNewValue)
{
m_bResupplyAuto = bNewValue;
}*/
bool CvSelectionGroupAI::AI_shouldAutoHeal() const
{
	return m_bAutoHeal;
}

void CvSelectionGroupAI::AI_setShouldAutoHeal(bool bNewValue)
{
	m_bAutoHeal = bNewValue;
}
//END

int CvSelectionGroupAI::AI_getGroupIdForMerging()
{
	return m_iGroupIdForMerging;
}

void CvSelectionGroupAI::AI_setGroupIdForMerging(int iNewValue)
{
	m_iGroupIdForMerging = iNewValue;
}


int CvSelectionGroupAI::AI_getSeawayId()
{
	return m_iSeawayId;
}

void CvSelectionGroupAI::AI_setSeawayId(int iNewValue)
{
	m_iSeawayId = iNewValue;
}


MissionAITypes CvSelectionGroupAI::AI_getMissionAIType()
{
	return m_eMissionAIType;
}


void CvSelectionGroupAI::AI_setMissionAI(MissionAITypes eNewMissionAI, CvPlot* pNewPlot, CvUnit* pNewUnit)
{
	m_eMissionAIType = eNewMissionAI;

	if (pNewPlot != NULL)
	{
		m_iMissionAIX = pNewPlot->getX_INLINE();
		m_iMissionAIY = pNewPlot->getY_INLINE();
	}
	else
	{
		m_iMissionAIX = INVALID_PLOT_COORD;
		m_iMissionAIY = INVALID_PLOT_COORD;
	}

	if (pNewUnit != NULL)
	{
		m_missionAIUnit = pNewUnit->getIDInfo();
	}
	else
	{
		m_missionAIUnit.reset();
	}
}


CvUnit* CvSelectionGroupAI::AI_getMissionAIUnit()
{
	return getUnit(m_missionAIUnit);
}

bool CvSelectionGroupAI::AI_isFull()
{
	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pLoopUnit;

	if (getNumUnits() > 0)
	{
		UnitAITypes eUnitAI = getHeadUnitAI();
		// do two passes, the first pass, we ignore units with speical cargo
		int iSpecialCargoCount = 0;
		int iCargoCount = 0;

		// first pass, count but ignore special cargo units
		pUnitNode = headUnitNode();

		while (pUnitNode != NULL)
		{
			pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = nextUnitNode(pUnitNode);
			if (pLoopUnit->AI_getUnitAIType() == eUnitAI)
			{
				if (pLoopUnit->cargoSpace() > 0)
				{
					iCargoCount++;
				}

				if (pLoopUnit->specialCargo() != NO_SPECIALUNIT)
				{
					iSpecialCargoCount++;
				}
				else if (!(pLoopUnit->isFull()))
				{
					return false;
				}
			}
		}

		// if every unit in the group has special cargo, then check those, otherwise, consider ourselves full
		if (iSpecialCargoCount >= iCargoCount)
		{
			pUnitNode = headUnitNode();
			while (pUnitNode != NULL)
			{
				pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = nextUnitNode(pUnitNode);

				if (pLoopUnit->AI_getUnitAIType() == eUnitAI)
				{
					if (!(pLoopUnit->isFull()))
					{
						return false;
					}
				}
			}
		}

		return true;
	}

	return false;
}


bool CvSelectionGroupAI::AI_launchAssault(CvPlot* pTargetCityPlot)
{
	std::multimap<int, CvUnit*, std::greater<int> > units;
	std::multimap<int, CvUnit*, std::greater<int> >::iterator units_it;

	CLLNode<IDInfo>* pUnitNode = plot()->headUnitNode();
	CvUnit* pLoopUnit;

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = plot()->nextUnitNode(pUnitNode);

		if (pLoopUnit->isCargo())
		{
			if (pLoopUnit->getTransportUnit()->getGroup() == this)
			{
				int iValue = pLoopUnit->baseCombatStr();
				if (pLoopUnit->canAttack())
				{
					iValue *= 10;
				}
				iValue *= 100;

				units.insert(std::make_pair(iValue, pLoopUnit));
			}
		}
	}

	if (units.empty())
	{
		return false;
	}

	bool bAction = false;
	for (units_it = units.begin(); units_it != units.end(); ++units_it)
	{
		pLoopUnit = units_it->second;
		if (pLoopUnit->canMove() && pLoopUnit->canAttack())
		{
			//			if (pLoopUnit->AI_attackFromTransport(NULL, 40, 80))
			//			{
			//			    bAction = true;
			//			}
			int iPriority = 41;
			pLoopUnit->AI_setMovePriority(iPriority);
		}
	}
	//    for (units_it = units.begin(); units_it != units.end(); ++units_it)
	//    {
	//        pLoopUnit = units_it->second;
	//        if (pLoopUnit->canMove())
	//        {
	//            if (pLoopUnit->AI_moveFromTransport(NULL))
	//            {
	//                bAction = true;
	//            }
	//        }
	//    }
	//    for (units_it = units.begin(); units_it != units.end(); ++units_it)
	//    {
	//        pLoopUnit = units_it->second;
	//		if (pLoopUnit->canMove() && pLoopUnit->canAttack())
	//		{
	//			if (pLoopUnit->AI_attackFromTransport(NULL, 0, 100))
	//			{
	//			    bAction = true;
	//			}
	//		}
	//    }
	if (bAction)
	{
		//pushMission(MISSION_SKIP);
		return true;
	}

	return false;

}

void CvSelectionGroupAI::AI_groupBombard()
{
	CLLNode<IDInfo>* pEntityNode = headUnitNode();
	CvUnit* pLoopUnit = NULL;

	while (pEntityNode != NULL)
	{
		pLoopUnit = ::getUnit(pEntityNode->m_data);
		pEntityNode = nextUnitNode(pEntityNode);
		CvCity* pCity = pLoopUnit->bombardTarget(plot());

		if (pCity != NULL && pLoopUnit->canBombard(pCity->plot()))
		{
			pLoopUnit->bombard(pCity->getX(), pCity->getY());
		}
	}
}

//The return value is the amount of treasure on board.
int CvSelectionGroupAI::AI_getYieldsLoaded(short* piYields)
{
	if (piYields != NULL)
	{
		for (int i = 0; i < NUM_YIELD_TYPES; ++i)
		{
			piYields[i] = 0;
		}
	}
	int iAmount = 0;

	CLinkList<IDInfo> unitList;
	buildCargoUnitList(unitList);

	CLLNode<IDInfo>* pUnitNode = unitList.head();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = unitList.next(pUnitNode);

		if (pLoopUnit->getYieldStored() > 0)
		{
			if (pLoopUnit->getUnitInfo().isTreasure())
			{
				iAmount += pLoopUnit->getYieldStored();
			}
		}
	}
	CLLNode<IDInfo>* pEntityNode = headUnitNode();
	CvUnit* pLoopUnit = NULL;

	while (pEntityNode != NULL)
	{
		pLoopUnit = ::getUnit(pEntityNode->m_data);
		pEntityNode = nextUnitNode(pEntityNode);
		if (pLoopUnit != NULL)
		{
			for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
			{
				YieldTypes eYield = (YieldTypes) iYield;
				int iNbYield = pLoopUnit->getNewCargoYield(eYield);
				if (iNbYield > 0)
				{
					piYields[eYield] += iNbYield;
				}
			}
		}
	}


	return iAmount;
}

bool CvSelectionGroupAI::AI_agreements()
{
	CvCity* pPlotCity = plot()->getPlotCity();
	CvPlayerAI& kOwner = GET_PLAYER(getOwnerINLINE());
	const IDInfo kEurope(kOwner.isEurope() ? kOwner.getID() : kOwner.getParent(), CvTradeRoute::EUROPE_CITY_ID);
	std::set<int>::iterator it;
	std::vector<CvAgreement*> aiAgreements;
	kOwner.getAgreementsOfGroup(aiAgreements, getID());
	bool bDidSomething = false;

	if (pPlotCity == NULL)
	{
		if ((getDomainType() == DOMAIN_LAND || getHeadTransportSeaUnit()->AI_getUnitAIState() != UNITAI_STATE_SAIL) && (headMissionQueueNode() == NULL || headMissionQueueNode()->m_data.eMissionType != MISSION_MOVE_TO))
		{
			for (uint i = 0; i < aiAgreements.size(); ++i)
			{
				CvAgreement* pAgreement = aiAgreements[i];
				CvCity* pCity = ::getCity(pAgreement->getSourceCity());
				if (generatePath(plot(), pCity->plot(), 0, true))
				{
					pushMission(MISSION_MOVE_TO, pCity->getX_INLINE(), pCity->getY_INLINE(), 0, false, false, MISSIONAI_TRANSPORT, pCity->plot());
				}
				else
				{
					finishMoves();
				}
				break;
			}
		}
		return true;
	}

	std::vector<bool> yieldsDelivered(NUM_YIELD_TYPES, false);
	std::vector<int> yieldsLoaded(NUM_YIELD_TYPES, 0);

	//On décharge les marchandises
	if (hasCargo())
	{
		for (uint i = 0; i < aiAgreements.size(); ++i)
		{
			CvAgreement* pAgreement = aiAgreements[i];
			CLLNode<IDInfo>* pUnitNode = plot()->headUnitNode();
			while (pUnitNode != NULL)
			{
				CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = plot()->nextUnitNode(pUnitNode);

				if (pLoopUnit != NULL && pLoopUnit->getGroup() == this)
				{
					for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
					{
						YieldTypes eYield = (YieldTypes)iI;
						int iNbYield = pLoopUnit->getNewCargoYield(eYield);
						bool bUnload = false, bChangeAgreement = false;
						if (iNbYield > 0)
						{
							if (pAgreement->getTradeState(eYield) == TRADE_IMPORT && !pAgreement->isYieldOver(eYield))
							{
								if (::getCity(pAgreement->getSourceCity()) == pPlotCity)
								{
									bUnload = true;
								}
								yieldsDelivered[eYield] = true;
							}

							if (pAgreement->getTradeState(eYield) == TRADE_EXPORT && !pAgreement->isYieldOver(eYield))
							{
								if (::getCity(pAgreement->getDestinationCity()) == pPlotCity)
								{
									bUnload = true;
									bChangeAgreement = true;
								}
								yieldsDelivered[eYield] = true;
							}

							if (bUnload) 
							{
								if (bChangeAgreement) 
								{
									int iUnloadMax = std::max(0, pAgreement->getOriginalAmount(eYield) - pAgreement->getActualAmount(eYield));
									iNbYield = std::min(iUnloadMax, iNbYield);
									pAgreement->changeActualAmount(eYield, iNbYield);
								}

								pPlotCity->changeYieldStored(eYield, iNbYield);
								pLoopUnit->changeNewCargoYield(eYield, -iNbYield);

								if (iNbYield > 0) 
								{
									bDidSomething = true;
								}
							}
						}
					}
				}
			}
		}
	}

	if (pPlotCity->getOwner() == getOwnerINLINE())
	{
		//On décharge les ressources inutiles pour le(s) contrat(s)
		for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
		{
			YieldTypes eYield = (YieldTypes)iI;
			if (!yieldsDelivered[eYield])
			{
				CLLNode<IDInfo>* pUnitNode = plot()->headUnitNode();
				while (pUnitNode != NULL)
				{
					CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = plot()->nextUnitNode(pUnitNode);

					if (pLoopUnit != NULL && pLoopUnit->getGroup() == this)
					{
						pPlotCity->changeYieldStored(eYield, pLoopUnit->getNewCargoYield(eYield));
						pLoopUnit->setNewCargoYield(eYield, 0);
					}
				}
			}
		}
	}

	//On charge les marchandises
	for (uint i = 0; i < aiAgreements.size(); ++i)
	{
		CvAgreement* pAgreement = aiAgreements[i];
		CLLNode<IDInfo>* pUnitNode = plot()->headUnitNode();
		bool isOnSourceCity = ::getCity(pAgreement->getSourceCity()) == pPlotCity;
		bool isOnDestinationCity = ::getCity(pAgreement->getDestinationCity()) == pPlotCity;
		while (pUnitNode != NULL)
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = plot()->nextUnitNode(pUnitNode);

			if (pLoopUnit != NULL && pLoopUnit->getGroup() == this)
			{
				for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
				{
					YieldTypes eYield = (YieldTypes)iI;

					int iNbYield = pLoopUnit->getNewCargoYield(eYield);
					bool bLoad = false, bChangeAgreement = false;
					if (pAgreement->getTradeState(eYield) == TRADE_EXPORT && !pAgreement->isYieldOver(eYield) && isOnSourceCity)
					{
						bLoad = true;
					}

					if (pAgreement->getTradeState(eYield) == TRADE_IMPORT && !pAgreement->isYieldOver(eYield) && isOnDestinationCity)
					{
						bLoad = true;
						bChangeAgreement = true;
					}

					if (bLoad)
					{
						if (pLoopUnit->canLoadYield(plot(), eYield, true))
						{
							int iLoadMax = std::max(0, pAgreement->getOriginalAmount(eYield) - pAgreement->getActualAmount(eYield) - yieldsLoaded[eYield]);
							if (!bChangeAgreement)
							{
								iLoadMax = std::min(iLoadMax, pAgreement->getMaxLoadOfExportedYield());
							}

							int iNbYield = pLoopUnit->getNewCargoYield(eYield);
							pLoopUnit->loadYield(eYield, true, iLoadMax);
							iNbYield = std::max(0, pLoopUnit->getNewCargoYield(eYield) - iNbYield);
							yieldsLoaded[eYield] += iNbYield;

							if (bChangeAgreement)
							{
								pAgreement->changeActualAmount(eYield, iNbYield);
							}

							if (iNbYield)
							{
								bDidSomething = true;
							}
						}
					}
					else if (pPlotCity->isNative() && (isOnSourceCity || isOnDestinationCity)) 
					{
						if (kOwner.AI_isYieldForSale(eYield) && pLoopUnit->canLoadYield(plot(), eYield, true)) 
						{
							pLoopUnit->loadYield(eYield, true);
							bDidSomething = true;
						}
					}
				}
			}
		}
	}

	// we check if one of the agreements is over only if the unit has done somtheming in order to avoid two units execute this code
	if (pPlotCity->getOwner() != getOwnerINLINE() && bDidSomething) 
	{
		bool bOver = false;
		// An agreement can be over only inside a colony or a village
		for (uint i = 0; i < aiAgreements.size(); ++i) 
		{
			CvAgreement* pAgreement = aiAgreements[i];
			if (pAgreement->isOver() && !pAgreement->isDeadline()) 
			{
				if (GET_PLAYER(pPlotCity->getOwner()).isNative()) 
				{
					pPlotCity->randomWantedYield();
				}

				if (kOwner.isHuman()) 
				{
					pushMission(MISSION_SKIP);
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_END_OF_AGREEMENT, pAgreement->getID(), -1, kOwner.getID());
					gDLL->getInterfaceIFace()->addPopup(pInfo, kOwner.getID());
				} 
				else 
				{
					kOwner.removeAgreement(pAgreement->getID());
					getHeadUnit()->AI_processAgreementWithNatives();
				}
				bOver = true;
			}
		}
		if (bOver) 
		{
			return true;
		}
	}

	if (!bDidSomething && hasMoved()) 
	{
		finishMoves();
		return true;		
	}

	aiAgreements.clear(); // We clear agreements
	kOwner.getAgreementsOfGroup(aiAgreements, getID()); // The we refresh them
	IDInfo kBestDestination(NO_PLAYER, -1);
	IDInfo kLastSource(NO_PLAYER, -1);
	IDInfo kBetterSource(NO_PLAYER, -1);
	int iBestSourceValue = 0;
	int iBestDestinationValue = 0;
	short aiYieldsLoaded[NUM_YIELD_TYPES];
	AI_getYieldsLoaded(aiYieldsLoaded);

	for (uint i = 0; i < aiAgreements.size(); ++i)
	{
		CvAgreement* pAgreement = aiAgreements[i];
		CvCity* pSourceCity = ::getCity(pAgreement->getSourceCity());
		CvCity* pDestCity = ::getCity(pAgreement->getDestinationCity());
		if (pDestCity != pPlotCity && !pAgreement->isOver())
		{
			int iMissingYieldImport = pAgreement->getMissingYieldsPercent(TRADE_IMPORT);
			int iMissingYieldExport = pAgreement->getMissingYieldsPercent(TRADE_EXPORT);
			for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
			{
				YieldTypes eYield = (YieldTypes)iI;

				if (pAgreement->getTradeState(eYield) == TRADE_EXPORT)
				{
					int iValue;
					if (pAgreement->isYieldOver(eYield) || iMissingYieldImport + 10 >= iMissingYieldExport)
					{
						iValue = std::max(1, iMissingYieldImport*20/100);
					}
					else
					{
						iValue = 5*aiYieldsLoaded[eYield];
					}
					if (pDestCity != NULL)
					{
						if (generatePath(plot(), pDestCity->plot(), 0, true))
						{
							iValue /= 1 + kOwner.AI_plotTargetMissionAIs(pDestCity->plot(), MISSIONAI_TRANSPORT, this, 0);
						}
						else
						{
							iValue = 0;
						}
					}
					if (iBestDestinationValue < iValue)
					{
						kBestDestination = pAgreement->getDestinationCity();
						iBestDestinationValue = iValue;
					}
				}
			}
		}
		if (pSourceCity != NULL && pSourceCity != pPlotCity)
		{
			for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
			{
				YieldTypes eYield = (YieldTypes)iI;

				if (pAgreement->getTradeState(eYield) == TRADE_EXPORT)
				{
					int iValue = 5*pSourceCity->getYieldStored(eYield);
					if (pSourceCity != NULL)
					{
						if (generatePath(plot(), pSourceCity->plot(), 0, true))
						{
							iValue /= 1 + kOwner.AI_plotTargetMissionAIs(pSourceCity->plot(), MISSIONAI_TRANSPORT, this, 0);
						}
						else
						{
							iValue = 0;
						}
					}
					if (iBestSourceValue < iValue)
					{
						kBetterSource = pAgreement->getSourceCity();
						iBestSourceValue = iValue;
					}
				}
			}
		}
		kLastSource = pAgreement->getSourceCity();
	}

	//Si on ne trouve pas de destinations, on regarde s'il on peut prendre des ressources dans d'autres colonies, et sinon on va à la "dernière" de nos colonies
	if (kBestDestination.eOwner == NO_PLAYER)
	{
		if (kBetterSource.eOwner == NO_PLAYER)
		{
			kBestDestination = kLastSource;
		}
		else
		{
			kBestDestination = kBetterSource;
		}
	}

	if (kBestDestination != kEurope)
	{
		CvCity* pBestDestinationCity = ::getCity(kBestDestination);
		if (pBestDestinationCity != NULL)
		{
			if ( atPlot(pBestDestinationCity->plot()))
			{
				finishMoves();
				return true;
			} 
			else if (generatePath(plot(), pBestDestinationCity->plot(), 0, true)) 
			{
				pushMission(MISSION_MOVE_TO, pBestDestinationCity->getX_INLINE(), pBestDestinationCity->getY_INLINE(), 0, false, false, MISSIONAI_TRANSPORT, pBestDestinationCity->plot());
			} 
			else
			{
				finishMoves();
				return true;
			}

			if (atPlot(pBestDestinationCity->plot()))
			{
				//Unload any goods if required (we can always pick them back up if this is an i+e city).
				std::vector<CvUnit*> units;

				CLLNode<IDInfo>* pUnitNode = plot()->headUnitNode();
				CvUnit* pLoopUnit;
				while (pUnitNode != NULL)
				{
					pLoopUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = plot()->nextUnitNode(pUnitNode);
					YieldTypes eYield = pLoopUnit->getYield();

					if ((eYield != NO_YIELD) && pLoopUnit->isCargo())
					{
						if (pLoopUnit->getTransportUnit()->getGroup() == this && pLoopUnit->canUnload())
						{
							units.push_back(pLoopUnit);
						}
					}
				}

				for (uint i = 0; i < units.size(); ++i)
				{
					units[i]->unload();
				}
			}

			return true;
		}
	}
	else
	{
		getHeadTransportSeaUnit()->AI_setUnitAIState(UNITAI_STATE_SAIL);
	}

	return false;
}

bool CvSelectionGroupAI::AI_tradeRoutes()
{
	PROFILE_FUNC();

	const IDInfo kEurope(getOwnerINLINE(), CvTradeRoute::EUROPE_CITY_ID);

	CvTradeRoute* pRoute = getHeadUnit()->getTradeRoute();

	if (pRoute == NULL) 
	{
		setAutomateType(NO_AUTOMATE);
		return true;
	}

	CvPlayerAI& kOwner = GET_PLAYER(getOwnerINLINE());
	int iFlag = kOwner.isSecondaryOption(SECONDARYPLAYEROPTION_KEPT_AUTO_IN_WARTIME) ? MOVE_IGNORE_DANGER : 0;
	CvCity* pCity = plot()->getPlotCity();
	if (pCity != NULL) 
	{
		unloadAll(false); // We don't unload yields, just units

		AI_unloadYields(pRoute);

		AI_loadYields(pRoute);

		if (!AI_isFullyResupply()) 
		{
			int iMaxMunitions = pCity->getYieldStored(YIELD_AMMUNITION);
			if (iMaxMunitions > 0) 
			{
				int iAmmunitions = AI_resupplyShips(iMaxMunitions);
				pCity->changeYieldStored(YIELD_AMMUNITION, iAmmunitions);
			}
		}

		if (AI_shouldAutoHeal() && isHurt()) 
		{
			finishMoves();
			return true;
		}

		if (AI_shouldStopConvoy()) 
		{
			AI_setShouldStopConvoy(false);
			setAutomateType(NO_AUTOMATE);
			AI_setShouldWaitForMerging(false);
			AI_setGroupIdForMerging(-1);
			finishMoves();
			return true;
		}
	}				

	IDInfo kBestDestination(NO_PLAYER, -1);
	int iBestDestinationValue = 0;
	TradeTypes eEuropeType = NO_TRADE;

	if (pRoute->getDestinationCity() == kEurope) 
	{
		eEuropeType = TRADE_EXPORT;
	}

	if (pRoute->getSourceCity() == kEurope) 
	{
		eEuropeType = TRADE_IMPORT;
	}

	bool hasRessourcesToSellInEurope = false;
	if (eEuropeType != NO_TRADE) 
	{
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield) 
		{
			YieldTypes eYield = (YieldTypes) iYield;
			if (pRoute->getTradeType(eYield) == eEuropeType) 
			{
				CLLNode<IDInfo>* pUnitNode = headUnitNode();
				while (pUnitNode != NULL) 
				{
					CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = nextUnitNode(pUnitNode);

					if (pLoopUnit != NULL) 
					{
						if (pLoopUnit->getOnlyNewCargo() > 0 && pLoopUnit->getNewCargoYield(eYield) > 0)
						{
							hasRessourcesToSellInEurope = true;
							break;
						}
					}
				}
				if (hasRessourcesToSellInEurope)
				{
					break;
				}
			}
		}
	}

	if (!hasRessourcesToSellInEurope) 
	{
		for (int iI = 0; iI < 2; ++iI) 
		{
			CvCity* pCity = ::getCity(iI == 0 ? pRoute->getSourceCity() : pRoute->getDestinationCity());
			TradeTypes eType = iI == 0 ? TRADE_EXPORT : TRADE_IMPORT;
			if (pCity != NULL) 
			{
				int iValue = 100;
				if (!atPlot(pCity->plot()) && generatePath(plot(), pCity->plot(), iFlag, true)) 
				{
					iValue /= 1 + kOwner.AI_plotTargetMissionAIs(pCity->plot(), MISSIONAI_TRANSPORT, this, 0);
				}
				else 
				{
					iValue = 0;
				}

				if (iValue > iBestDestinationValue)
				{
					iBestDestinationValue = iValue;
					kBestDestination = pCity->getIDInfo();
				}
			} 
		}
	}

	if (iBestDestinationValue == 0 && eEuropeType != NO_TRADE) 
	{
		if (hasRessourcesToSellInEurope || pRoute->getNumYieldsByTradeTypes(eEuropeType == TRADE_IMPORT ? TRADE_EXPORT : TRADE_IMPORT) > 0  || kOwner.getNumEuropeUnitsWithoutSailors() > 0) 
		{
			kBestDestination = kEurope;
		}
	}

	CvUnit* pHeadUnit = getHeadUnit();
	if (NULL != pHeadUnit) 
	{
		if (kBestDestination != kEurope)
		{
			CvCity* pBestDestinationCity = ::getCity(kBestDestination);
			if (pBestDestinationCity != NULL)
			{
				FAssert(!atPlot(pBestDestinationCity->plot()));
				pushMission(MISSION_MOVE_TO, pBestDestinationCity->getX_INLINE(), pBestDestinationCity->getY_INLINE(), iFlag, false, false, MISSIONAI_TRANSPORT, pBestDestinationCity->plot());
				return true;
			}

			if (pCity != NULL)
			{
				finishMoves();
			}
			
			return true; 
		}
		else
		{

			pHeadUnit->AI_setUnitAIState(UNITAI_STATE_SAIL);
			
			if (pHeadUnit->AI_sailToEurope())
			{

				if (pHeadUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
				{
					AI_europeAutomated();
				}

			} 
			else
			{
				finishMoves();
			}
			
			return true;
		}
	}

	return false;
}

bool CvSelectionGroupAI::AI_hasScout() 
{
	CLLNode<IDInfo>* pUnitNode = plot()->headUnitNode();
	CvUnit* pLoopUnit;

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = plot()->nextUnitNode(pUnitNode);

		if (pLoopUnit->isCargo() && pLoopUnit->getTransportUnit()->getGroup() == this && pLoopUnit->AI_getUnitAIType() == UNITAI_SCOUT)
		{
			return true;
		}
	}
	return false;
}

void CvSelectionGroupAI::AI_unloadYields(CvTradeRoute* pRoute) 
{
	CvCity* pPlotCity = plot()->getPlotCity();
	TradeTypes eType = NO_TRADE;

	if (::getCity(pRoute->getDestinationCity()) == pPlotCity) 
	{
		eType = TRADE_EXPORT;
	}

	if (::getCity(pRoute->getSourceCity()) == pPlotCity) 
	{
		eType = TRADE_IMPORT;
	}

	if (eType == NO_TRADE)
	{
		return;
	}

	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL) 
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);

		if (pLoopUnit != NULL && pLoopUnit->isOnlyDefensive())
		{			
			int iCargoAmount = 0;
			int iMaxCargoAmount = pLoopUnit->getUnitInfo().getCargoNewSpace();
			for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
			{
				YieldTypes eYield = (YieldTypes) iYield;
				int iNbYield = pLoopUnit->getNewCargoYield(eYield);
				if (iNbYield > 0) 
				{
					pPlotCity->changeYieldStored(eYield, iNbYield);
					pLoopUnit->setNewCargoYield(eYield, 0);
					if (eType == TRADE_EXPORT) 
					{
						iCargoAmount += iNbYield;
					}
				}
			}
			CvConvoy* pConvoy = pLoopUnit->getConvoy();
			if (pConvoy != NULL && eType == TRADE_EXPORT)
			{
				pConvoy->processTravelRotation(iCargoAmount, iMaxCargoAmount, 0);
			}
		}
	}
}

struct OrderYields {YieldTypes m_eYield; int m_iWeight;};
bool sortOrderYields(const OrderYields orderYields1, const OrderYields orderYields2)
{
	return (orderYields1.m_iWeight > orderYields2.m_iWeight);
}

void CvSelectionGroupAI::AI_loadYields(CvTradeRoute* pRoute) 
{
	CvCity* pPlotCity = plot()->getPlotCity();
	CvCity* pOtherCity = NULL;
	TradeTypes eType = NO_TRADE;

	if (::getCity(pRoute->getDestinationCity()) == pPlotCity)
	{
		eType = TRADE_IMPORT;
		pOtherCity = ::getCity(pRoute->getSourceCity());
	}

	if (::getCity(pRoute->getSourceCity()) == pPlotCity) 
	{
		eType = TRADE_EXPORT;
		pOtherCity = ::getCity(pRoute->getDestinationCity());
	}

	if (eType == NO_TRADE) 
	{
		return;
	}

	std::vector<OrderYields> bestYields;

	bool abYields[NUM_YIELD_TYPES] = {};
	int aiInCommingYields[NUM_YIELD_TYPES] = {};
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield) 
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (pRoute->getTradeType(eYield) == eType) 
		{				
			OrderYields oy;
			oy.m_eYield = eYield;
			oy.m_iWeight = pPlotCity->getYieldStored(eYield);
			bestYields.push_back(oy);
			if (pRoute->getSelectedRadioOption(eYield) == ENSURE_THE_NEEDS)
			{
				abYields[eYield] = true;
			}
		}
	}
	if ( pOtherCity != NULL ) 
	{
		pOtherCity->getInCommingYields(abYields, aiInCommingYields, eType);
	}

	std::sort(bestYields.begin(), bestYields.end(), sortOrderYields);
	bool bContinue = true;
	bool bRefresh = false;
	while (!isFull() && bContinue)
	{
		bContinue = false;
		CLLNode<IDInfo>* pUnitNode = headUnitNode();
		while (pUnitNode != NULL)
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = nextUnitNode(pUnitNode);

			if (pLoopUnit != NULL)
			{				
				for (int iI = 0; iI < (int)bestYields.size(); iI++)
				{
					YieldTypes eYield = bestYields[iI].m_eYield;
					if (eYield != NO_YIELD && pLoopUnit->canLoadYield(plot(), eYield, true))
					{
						TradeRouteRadioTypes eTradeRouteRadioType = pRoute->getSelectedRadioOption(eYield);
						int iAmount = -1;
						bool bUpdateInCommingYields = false;
						if (eTradeRouteRadioType == SEND_FIXED_QUANTITY)
						{
							iAmount = pLoopUnit->getMaxLoadYieldAmount(eYield, pRoute->getRadioOption(eYield, eTradeRouteRadioType).iQuantity);
							pRoute->changeRadioOptionQuantity(eYield, eTradeRouteRadioType, -iAmount);
						} 

						if (pOtherCity != NULL) 
						{
							if (eTradeRouteRadioType == ENSURE_THE_NEEDS)
							{
								int iBufferStock = pRoute->getRadioOption(eYield, eTradeRouteRadioType).iQuantity;
								FAssert(iBufferStock >= 0);
								int iIdealValue = iBufferStock + pOtherCity->getYieldNeededs(eYield) - aiInCommingYields[eYield];
								iAmount = std::max(0, iIdealValue - pOtherCity->getYieldStored(eYield));
								bUpdateInCommingYields = true;
							}

							if (pRoute->getCheckBoxOption(eYield, DO_NOT_EXCEED).isActivate)
							{
								if (iAmount >= 0) 
								{
									iAmount = std::max(0, std::min(iAmount, pRoute->getCheckBoxOption(eYield, DO_NOT_EXCEED).iQuantity - pOtherCity->getYieldStored(eYield) - aiInCommingYields[eYield]));
								} 
								else 
								{
									iAmount = std::max(0, pRoute->getCheckBoxOption(eYield, DO_NOT_EXCEED).iQuantity - pOtherCity->getYieldStored(eYield) - aiInCommingYields[eYield]);
								}

								bUpdateInCommingYields = true;
							}							
						}

						if (pRoute->getCheckBoxOption(eYield, ALWAYS_KEEP).isActivate) 
						{
							if (iAmount >= 0) 
							{
								iAmount = std::max(0, std::min(iAmount, pPlotCity->getYieldStored(eYield) - pRoute->getCheckBoxOption(eYield, ALWAYS_KEEP).iQuantity));
							} 
							else
							{
								iAmount = std::max(0, pPlotCity->getYieldStored(eYield) - pRoute->getCheckBoxOption(eYield, ALWAYS_KEEP).iQuantity);
							}
						}

						if (pRoute->getCheckBoxOption(eYield, CONSERVE_NECESSARY_RESOURCES).isActivate)
						{
							int iNecessary = std::max(0, -pPlotCity->calculateNetYield(eYield));
							if (iAmount == -1) 
							{
								iAmount = pLoopUnit->getMaxLoadYieldAmount(eYield);
							}
							iAmount = std::max(0, iAmount - iNecessary);
						}

						if (bUpdateInCommingYields && iAmount > 0) 
						{								
							iAmount = pLoopUnit->getMaxLoadYieldAmount(eYield, iAmount);
							aiInCommingYields[eYield] += iAmount;
						}

						if (iAmount != 0) 
						{
							pLoopUnit->loadYield(eYield, true, iAmount);
							bestYields[iI].m_iWeight = pPlotCity->getYieldStored(eYield);
							bContinue = true;
							bRefresh = true;
							if (eTradeRouteRadioType == SEND_FIXED_QUANTITY && pRoute->getRadioOption(eYield, eTradeRouteRadioType).iQuantity == 0) 
							{
								bestYields[iI].m_eYield = NO_YIELD;//We don't trade this yield anymore
								bContinue = false;
							}
						}
					}
				}

				if (bRefresh) 
				{
					std::sort(bestYields.begin(), bestYields.end(), sortOrderYields);
					bRefresh = false;
				} 
			}
		}
	}	
}

void CvSelectionGroupAI::AI_europeAutomated()
{
	CvTradeRoute* pRoute = getHeadUnit()->getTradeRoute();
	if (pRoute != NULL) 
	{
		AI_europeTradeRoutes(pRoute);
		return;
	}

	AI_separate();	// pointers could become invalid...
}

void CvSelectionGroupAI::AI_europeNativeTradeAutomated()
{
	int iTotalPrice = 0;
	int iTotalAmmount = 0;
	CvPlayerAI& kOwner = GET_PLAYER(getOwnerINLINE());
	int aSellCargo[NUM_YIELD_TYPES] = {};
	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL) 
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);

		if (pLoopUnit != NULL && pLoopUnit->isOnlyDefensive()) 
		{
			int iCargoAmount = 0;
			int iProfits = 0;
			for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
			{
				YieldTypes eYield = (YieldTypes)iI;
				int iNbYield = pLoopUnit->getNewCargoYield(eYield);
				if (iNbYield > 0 && kOwner.AI_isYieldForSale(eYield)) 
				{
					iCargoAmount += iNbYield;
					aSellCargo[eYield] += iNbYield;
					pLoopUnit->transfertEuropeToShip(eYield, -iNbYield);
					int iPrice = kOwner.getSellPriceForYield(eYield, iNbYield);
					int iTaxGold = iPrice * (kOwner.getTaxRate() + kOwner.getEuropeLoanPercent()) / 100;
					iProfits += iPrice-iTaxGold;
				}
			}
		}
	}

	for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
	{
		YieldTypes eYield = (YieldTypes)iI;	
		int iAmount = aSellCargo[eYield];
		if (iAmount > 0) 
		{
			CvWStringBuffer szMessage;
			CvWString szString;
			GAMETEXT.setEuropeYieldSoldHelp(szMessage, kOwner, eYield, iAmount, kOwner.getTaxRate() + kOwner.getEuropeLoanPercent());
			szString.Format(L"%s", szMessage.getCString());
			kOwner.addEuropeTradeMessage(szString);
		}
	}

	if (isHurt())
	{
		pUnitNode = headUnitNode();
		while (pUnitNode != NULL) 
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = nextUnitNode(pUnitNode);

			if (pLoopUnit != NULL && pLoopUnit->isOnlyDefensive()) 
			{
				if (pLoopUnit->isHurt()) 
				{
					if (!pLoopUnit->isInEuropeDrydock()) 
					{
						int iCost = pLoopUnit->getReparationCostInEurope(true);
						if (iCost < kOwner.getGold()) 
						{
							kOwner.repairShip(pLoopUnit, true);
						}
					}
				}
			}
		}

		pushMission(MISSION_SKIP, -1, -1, 0, false);
		return;
	}

	CvCity* pDestinationCity = NULL;
	if ( kOwner.getNumEuropeUnitsWithoutSailors() >= 5) 
	{
		int iLoop;
		int iBestValue = 0;
		for (CvCity* pLoopCity = kOwner.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kOwner.nextCity(&iLoop))
		{
			int iCount = 0;
			if (pLoopCity->AI_getTradeRouteToEurope() != NULL)
			{
				int iImmigrantsAmount = AI_getImmigrantsToEmbarkForDestinationCity(pLoopCity).size();

				if (iImmigrantsAmount > iBestValue) 
				{
					iBestValue = iImmigrantsAmount;
					pDestinationCity = pLoopCity;
				}
			}
		}
	}

	CvUnit* pUnit = getHeadTransportSeaUnit();

	if (pDestinationCity != NULL) 
	{
		std::deque<CvUnit*> immigrants = AI_getImmigrantsToEmbarkForDestinationCity(pDestinationCity);
		AI_embarkImmigrantsForDestinationCity(pDestinationCity, immigrants);

		if (pUnit != NULL) 
		{
			pUnit->setDestinationCity(pDestinationCity->getID());
			CvSeaway* pSeaway = kOwner.getSeawayNearCity(pDestinationCity);
			if (pSeaway != NULL) 
			{
				CvPlot* pSeawayPlot = pSeaway->plot();
				if (pSeawayPlot != plot())
				{
					pUnit->moveToSeaway(pSeaway);
				}
			}
		}
	}

	pUnitNode = headUnitNode();
	while (pUnitNode != NULL) 
	{
		CvUnit* pLoopUnit =  ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);

		if (pLoopUnit != NULL && pLoopUnit->isOnlyDefensive()) 
		{
			pLoopUnit->AI_europeBuyNativeYields();
		}
	}

	if (pUnit != NULL) 
	{
		pUnit->crossOcean(UNIT_TRAVEL_STATE_FROM_EUROPE);
	}
}

bool CvSelectionGroupAI::AI_shouldAddUnitToGroup(CvUnit* pUnit) const
{
	int iMaxMerchantUnitByGroup = 4;
	int iMaxMilitaryUnitByGroup = 2;

	bool isMerchantUnit = pUnit->isOnlyDefensive();
	bool isMilitaryUnit = !isMerchantUnit;

	if (isMerchantUnit && getNumDefensiveUnit() >= iMaxMerchantUnitByGroup)
	{
		return false;
	}

	if (isMilitaryUnit && getNumMilitaryUnit() >= iMaxMilitaryUnitByGroup)
	{
		return false;
	}

	return true;
}

void CvSelectionGroupAI::AI_europeTradeRoutes(CvTradeRoute* pRoute)
{
	CvPlayerAI& kOwner = GET_PLAYER(getOwnerINLINE());
	CvPlayer& kPlayerEurope = GET_PLAYER(kOwner.getParent());
	const IDInfo kEurope(kOwner.getID(), CvTradeRoute::EUROPE_CITY_ID);

	//Traderoute part
	TradeTypes eType = NO_TRADE;
	if (pRoute->getDestinationCity() == kEurope)
	{
		eType = TRADE_EXPORT;
	}
	if (pRoute->getSourceCity() == kEurope) 
	{
		eType = TRADE_IMPORT;
	}
	std::vector<CvUnit*> aUnitToSells;
	if (eType != NO_TRADE) 
	{
		int aSellCargo[NUM_YIELD_TYPES] = {};
		CLLNode<IDInfo>* pUnitNode = headUnitNode();
		while (pUnitNode != NULL)
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = nextUnitNode(pUnitNode);

			if (pLoopUnit->mustStayInEurope())
			{
				aUnitToSells.push_back(pLoopUnit);
			}

			if (pLoopUnit->isOnlyDefensive()) 
			{
				int iCargoAmount = 0;
				int iMaxCargoAmount = pLoopUnit->getUnitInfo().getCargoNewSpace();
				int iProfits = 0;
				for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
				{
					YieldTypes eYield = (YieldTypes)iI;
					if (pRoute->getTradeType(eYield) == eType) 
					{
						int iNbYield = pLoopUnit->getNewCargoYield(eYield);
						if (iNbYield > 0) 
						{						
							iCargoAmount += iNbYield;
							aSellCargo[eYield] += iNbYield;
							pLoopUnit->transfertEuropeToShip(eYield, -iNbYield);
							int iPrice = kOwner.getSellPriceForYield(eYield, iNbYield);
							int iTaxGold = iPrice * (kOwner.getTaxRate() + kOwner.getEuropeLoanPercent()) / 100;
							iProfits += iPrice-iTaxGold;
						}
					}
				}
				CvConvoy* pConvoy = pLoopUnit->getConvoy();
				if (pConvoy != NULL) 
				{
					pConvoy->processTravelRotation(iCargoAmount, iMaxCargoAmount, iProfits);
				}
			}
		}

		int iTotalPrice = 0;
		int iTotalAmmount = 0;
		for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI) 
		{
			YieldTypes eYield = (YieldTypes)iI;
			if (pRoute->getTradeType(eYield) == eType) 
			{			
				int iAmount = aSellCargo[eYield];
				if (iAmount > 0) 
				{
					CvWStringBuffer szMessage;
					CvWString szString;
					GAMETEXT.setEuropeYieldSoldHelp(szMessage, kOwner, eYield, iAmount, kOwner.getTaxRate() + kOwner.getEuropeLoanPercent());
					szString.Format(L"%s", szMessage.getCString());
					kOwner.addEuropeTradeMessage(szString);
					int iPrice =  kOwner.getSellPriceForYield(eYield, iAmount);
					int iTaxGold = iPrice * (kOwner.getTaxRate() + kOwner.getEuropeLoanPercent()) / 100;
					iTotalPrice += iPrice-iTaxGold;
					iTotalAmmount += iAmount;
					pRoute->changeTotalProfitsYield(eYield, iPrice-iTaxGold);
				}					
			}
		}
	}

	// Selling deprecatedUnits
	for (uint i = 0; i < aUnitToSells.size(); ++i)
	{
		CvUnit* pLoopUnit = aUnitToSells[i];
		CvConvoy* pConvoy = pLoopUnit->getConvoy();
		int iPrice = pLoopUnit->getSellPrice() / 2;
		kOwner.sellShip(pLoopUnit, iPrice);		
		if (pConvoy != NULL) 
		{
			pConvoy->removeUnit(pLoopUnit, true);
		}
		kOwner.processPurchaseBid(pLoopUnit, iPrice);
	}

	if (!AI_isFullyResupply()) 
	{
		int iAmmunitionPrice = kOwner.getBuyPriceForYield(YIELD_AMMUNITION);
		if (iAmmunitionPrice > 0) 
		{
			int iMaxMunitions = kOwner.getGold() / iAmmunitionPrice;
			if (iMaxMunitions > 0) 
			{
				int iBuyAmmunition = AI_resupplyShips(iMaxMunitions);
				CvWString szString;
				CvWStringBuffer szMessage;
				GAMETEXT.setEuropeYieldBoughtHelp(szMessage, kOwner, YIELD_AMMUNITION, iBuyAmmunition);
				szString.Format(L"%s", szMessage.getCString());
				kOwner.addEuropeTradeMessage(szString);
			}
		}
	}

	if (AI_shouldAutoHeal() && isHurt()) 
	{
		pushMission(MISSION_SKIP);
		return;
	}

	if (AI_shouldStopConvoy()) 
	{
		AI_setShouldStopConvoy(false);
		setAutomateType(NO_AUTOMATE);
		return;
	}

	eType = NO_TRADE;
	CvCity* pDestinationCity = NULL;

	if (pRoute->getDestinationCity() == kEurope) 
	{
		eType = TRADE_IMPORT;
		pDestinationCity = ::getCity(pRoute->getSourceCity());
	}

	if (pRoute->getSourceCity() == kEurope)
	{
		eType = TRADE_EXPORT;
		pDestinationCity = ::getCity(pRoute->getDestinationCity());
	}

	if (pDestinationCity != NULL) 
	{
		std::deque<CvUnit*> immigrants = AI_getImmigrantsToEmbarkForDestinationCity(pDestinationCity);

		int shouldTakeImmgrantFirst = immigrants.size() > 3;// If we have many immigrants that are waiting in the dock, we choose to take them first

		if (shouldTakeImmgrantFirst) 
		{
			AI_embarkImmigrantsForDestinationCity(pDestinationCity, immigrants);
			AI_buyRequiredYieldsForTheTradeRoute(pRoute, pDestinationCity, eType);
		} 
		else 
		{
			AI_buyRequiredYieldsForTheTradeRoute(pRoute, pDestinationCity, eType);
			AI_embarkImmigrantsForDestinationCity(pDestinationCity, immigrants);
		}
	}

	finishMoves();
}

std::deque<CvUnit*> CvSelectionGroupAI::AI_getImmigrantsToEmbarkForDestinationCity(CvCity* pDestinationCity)
{
	CvPlayerAI& kOwner = GET_PLAYER(getOwnerINLINE());
	std::deque<CvUnit*> immigrants;

	for (int iLoopUnit = 0; iLoopUnit < kOwner.getNumEuropeUnits(); ++iLoopUnit) 
	{
		CvUnit* pLoopUnit = kOwner.getEuropeUnit(iLoopUnit);
		int iUnitDestinationCity = pLoopUnit->getDestinationCity();

		// If units should work inside the destination city, we add them
		if (iUnitDestinationCity == pDestinationCity->getID())
		{
			immigrants.push_back(pLoopUnit);
		}
			
		// If scouts are waiting to be embarked
		if (!kOwner.isHuman() && pLoopUnit->getProfession() == PROFESSION_SCOUT && iUnitDestinationCity == -1)
		{
			immigrants.push_back(pLoopUnit);
		}
			
		// If there is no trade route with this destination city
		if (iUnitDestinationCity != -1)
		{
			CvCity* pUnitDestinationCity = kOwner.getCity(iUnitDestinationCity);
			if (pUnitDestinationCity != NULL) 
			{
				CvTradeRoute* pTradeRoute = pUnitDestinationCity->AI_getTradeRouteToCoastalCity();
				if (pTradeRoute != NULL && pDestinationCity->getIDInfo() == pTradeRoute->getDestinationCity())
				{
					immigrants.push_back(pLoopUnit);
				}
			}
		}
	}

	return immigrants;
}

void CvSelectionGroupAI::AI_embarkImmigrantsForDestinationCity(CvCity* pDestinationCity, std::deque<CvUnit*> colonists)
{
	CvPlayerAI& kOwner = GET_PLAYER(getOwnerINLINE());

	while (!colonists.empty())
	{
		CvUnit* pLoopUnit = colonists.front();
		colonists.pop_front();
		FAssert (pLoopUnit != NULL);
		CLLNode<IDInfo>* pUnitNode = headUnitNode();
		while (pUnitNode != NULL)
		{
			CvUnit* pTransportUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = nextUnitNode(pUnitNode);

			if (pTransportUnit != NULL && pLoopUnit->canLoadUnit(pTransportUnit, pTransportUnit->plot(), false))
			{
				kOwner.loadUnitFromEurope(pLoopUnit, pTransportUnit);
				break;
			}
		}
	}
}
void CvSelectionGroupAI::AI_buyRequiredYieldsForTheTradeRoute(CvTradeRoute* pRoute, CvCity* pDestinationCity, TradeTypes eType)
{
	CvPlayerAI& kOwner = GET_PLAYER(getOwnerINLINE());
	CvPlayer& kPlayerEurope = GET_PLAYER(kOwner.getParent());
	if (eType == NO_TRADE)
	{
		return;		
	}
	std::vector<OrderYields> bestYields;
	int aSellCargo[NUM_YIELD_TYPES] = {};
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield) 
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (pRoute->getTradeType(eYield) == eType)
		{
			TradeRouteRadioEuropeanTypes eRadioEuropeanType = pRoute->getSelectedEuropeanRadioOption(eYield);

			if (eRadioEuropeanType == BUY_QUANTITY || eRadioEuropeanType == BUY_UNTIL_QUANTITY_REACHED)
			{				
				OrderYields oy;
				oy.m_eYield = eYield;
				oy.m_iWeight = kOwner.getBuyPriceForYield(eYield);
				bestYields.push_back(oy);
			}
		}
	}

	std::sort(bestYields.begin(), bestYields.end(), sortOrderYields);
	bool bContinue = true;
	while (!isFull() && bContinue)
	{
		bContinue = false;
		CLLNode<IDInfo>* pUnitNode = headUnitNode();
		while (pUnitNode != NULL)
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = nextUnitNode(pUnitNode);

			if (pLoopUnit != NULL && pLoopUnit->isOnlyDefensive())
			{
				for (int iI = 0; iI < (int)bestYields.size(); iI++)
				{
					YieldTypes eYield = bestYields[iI].m_eYield;
					if (eYield != NO_YIELD)
					{
						TradeRouteRadioEuropeanTypes eRadioEuropeanType = pRoute->getSelectedEuropeanRadioOption(eYield);
						AdvancedOptionInfo advancedOption = pRoute->getEuropeanRadioOption(eYield, eRadioEuropeanType);
						int iAllowedGoldToUse = kOwner.getGold() - pRoute->getGoldAmountToAlwaysConserve();
						int iEstimateBuyPrice = kOwner.getBuyPriceForYield(eYield);
						if (iEstimateBuyPrice > 0 && iAllowedGoldToUse > 0)
						{									
							int iQuantity = std::min(advancedOption.iQuantity, pLoopUnit->getSpaceNewCargo());

							iQuantity = std::min(iQuantity, kPlayerEurope.getEuropeWarehouseYield(eYield));
							iQuantity = std::min(iQuantity, iAllowedGoldToUse/iEstimateBuyPrice);

							if (eRadioEuropeanType == BUY_UNTIL_QUANTITY_REACHED)
							{
								iQuantity = std::min(iQuantity, advancedOption.iQuantity - pDestinationCity->getYieldStored(eYield) - advancedOption.iQuantityPerTurnLeft);
							}

							OptionInfo eMaxQuantityPerTurnOption = pRoute->getCheckBoxOption(eYield, MAINTAIN_QUANTITY);
							if (eMaxQuantityPerTurnOption.isActivate )
							{
								int iMaxQuantity = std::max(0, eMaxQuantityPerTurnOption.iQuantity - advancedOption.iQuantityPerTurnLeft);
								iQuantity = std::min(iMaxQuantity, iQuantity);
							}

							if (iQuantity > 0)
							{
								aSellCargo[eYield] += iQuantity;
								pLoopUnit->transfertEuropeToShip(eYield, iQuantity);
								pRoute->changeEuropeanRadioOptionQuantity(eYield, eRadioEuropeanType, -iQuantity);
								bContinue = advancedOption.iQuantity > 0;
							}
						}
					}
				}
			}
		}
	}
	for (int iI = 0; iI < (int)bestYields.size(); iI++)
	{
		YieldTypes eYield = bestYields[iI].m_eYield;
		if (eYield != NO_YIELD && aSellCargo[eYield] > 0)
		{
			CvWString szString;
			CvWStringBuffer szMessage;
			GAMETEXT.setEuropeYieldBoughtHelp(szMessage, kOwner, eYield, aSellCargo[eYield]);
			szString.Format(L"%s", szMessage.getCString());
			kOwner.addEuropeTradeMessage(szString);
		}
	}
}

void CvSelectionGroupAI::AI_europeAgreements(std::vector<CvAgreement*> aiAgreements) 
{
	CvPlayerAI& kOwner = GET_PLAYER(getOwnerINLINE());
	CvPlayer& kPlayerEurope = GET_PLAYER(kOwner.getParent());
	const IDInfo kEurope(kOwner.isEurope() ? kOwner.getID() : kOwner.getParent(), CvTradeRoute::EUROPE_CITY_ID);

	bool bDidSomething = false;
	// Unloading yields
	if (hasCargo())
	{
		for (uint i = 0; i < aiAgreements.size(); ++i)
		{
			CvAgreement* pAgreement = aiAgreements[i];
			if (pAgreement->getDestinationCity() == kEurope)
			{
				CLLNode<IDInfo>* pUnitNode = plot()->headUnitNode();
				while (pUnitNode != NULL)
				{
					CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = plot()->nextUnitNode(pUnitNode);

					if (pLoopUnit != NULL && pLoopUnit->getGroup() == this && pLoopUnit->getOnlyNewCargo() > 0)
					{
						for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
						{
							YieldTypes eYield = (YieldTypes)iI;
							int iNbYield = pLoopUnit->getNewCargoYield(eYield);
							if (iNbYield > 0)
							{
								if (pAgreement->getTradeState(eYield) == TRADE_EXPORT && !pAgreement->isYieldOver(eYield))
								{
									CvPlayer& kAgreementOwner = GET_PLAYER(pAgreement->getOwner());
									int iUnloadMax = std::max(0, pAgreement->getOriginalAmount(eYield) - pAgreement->getActualAmount(eYield));
									iNbYield = std::min(iUnloadMax, iNbYield);
									pLoopUnit->changeNewCargoYield(eYield, -iNbYield);
									int iPrice = pAgreement->getFixedPrice()*iNbYield;
									iPrice -= iPrice*kAgreementOwner.getTaxRate()/100;
									kAgreementOwner.changeGold(iPrice);
									// For now ressources only disapear
									pAgreement->changeActualAmount(eYield, iNbYield);
									bDidSomething = true;
								}
							}
						}
					}
				}
			}
		}
	}

	if (!AI_isFullyResupply())
	{
		int iAmmunitionPrice = kOwner.getBuyPriceForYield(YIELD_AMMUNITION);
		if (iAmmunitionPrice > 0)
		{
			int iMaxMunitions = kOwner.getGold() / iAmmunitionPrice;
			if (iMaxMunitions > 0)
			{
				int iBuyAmmunition = AI_resupplyShips(iMaxMunitions);
				CvWString szString;
				CvWStringBuffer szMessage;
				GAMETEXT.setEuropeYieldBoughtHelp(szMessage, kOwner, YIELD_AMMUNITION, iBuyAmmunition);
				szString.Format(L"%s", szMessage.getCString());
				kOwner.addEuropeTradeMessage(szString);
			}
		}
	}

	if (AI_shouldAutoHeal() && isHurt())
	{
		pushMission(MISSION_SKIP);
		return;
	}

	// Loading yields
	for (uint i = 0; i < aiAgreements.size(); ++i)
	{
		CvAgreement* pAgreement = aiAgreements[i];
		if (pAgreement->getDestinationCity() == kEurope)
		{
			CLLNode<IDInfo>* pUnitNode = plot()->headUnitNode();
			while (pUnitNode != NULL)
			{
				CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = plot()->nextUnitNode(pUnitNode);

				if (pLoopUnit != NULL && pLoopUnit->getGroup() == this && pLoopUnit->isOnlyDefensive())
				{
					for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
					{
						YieldTypes eYield = (YieldTypes)iI;

						int iNbYield = pLoopUnit->getNewCargoYield(eYield);
						if (pAgreement->getTradeState(eYield) == TRADE_IMPORT && !pAgreement->isYieldOver(eYield))
						{
							if (pLoopUnit->getSpaceNewCargo() > 0)
							{
								int iLoadMax = std::max(0, pAgreement->getOriginalAmount(eYield) - pAgreement->getActualAmount(eYield));
								int iMaxAmount = std::min(30, iLoadMax);
								iMaxAmount = std::min(iMaxAmount, pLoopUnit->getSpaceNewCargo());
								pLoopUnit->changeNewCargoYield(eYield, iMaxAmount);
								// For now yields appears by magic
								pAgreement->changeActualAmount(eYield, iMaxAmount);
								bDidSomething = true;
							}
						}
					}
				}
			}
		}
	}

	if (bDidSomething)
	{
		// Checking if a contract is over
		for (uint i = 0; i < aiAgreements.size(); ++i)
		{
			CvAgreement* pAgreement = aiAgreements[i];
			if (pAgreement->isOver())
			{
				if (!kOwner.isEurope())
				{
					if (kOwner.isHuman())
					{
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_END_OF_AGREEMENT, pAgreement->getID(), -1, kOwner.getID());
						gDLL->getInterfaceIFace()->addPopup(pInfo, kOwner.getID());
					}
					else
					{
						kOwner.removeAgreement(pAgreement->getID());
					}
				}
				else
				{
					assignAgreement(pAgreement->getID(), false);
					if (!isAssignedAgreements())
					{
						CLLNode<IDInfo>* pUnitNode = headUnitNode();
						while (pUnitNode != NULL)
						{
							CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
							pUnitNode = nextUnitNode(pUnitNode);

							if (pLoopUnit != NULL)
							{
								// TODO pLoopUnit->setMustStayInEurope(true);
							}
						}
					}
					return;
				}
			}
		}
	}

	CvUnit* pUnit = getHeadTransportSeaUnit();
	if (pUnit != NULL)
	{
		int iSeawayId = AI_getSeawayId();
		if (iSeawayId != -1)
		{
			CvSeaway* pSeaway = kOwner.getSeawayByID(iSeawayId);
			if (pSeaway != NULL)
			{
				CvPlot* pSeawayPlot = pSeaway->plot();
				if (pSeawayPlot != plot())
				{
					pUnit->moveToSeaway(pSeaway);
				}
			}
		}
		pUnit->crossOcean(UNIT_TRAVEL_STATE_FROM_EUROPE);
	}
}

bool CvSelectionGroupAI::AI_isFullyResupply() const
{
	CLLNode<IDInfo>* pEntityNode;
	CvUnit* pLoopUnit;

	pEntityNode = headUnitNode();

	while (pEntityNode != NULL)
	{
		pLoopUnit = ::getUnit(pEntityNode->m_data);
		pEntityNode = nextUnitNode(pEntityNode);

		int iMaxMunition = pLoopUnit->getMaxMunition();
		if (iMaxMunition > 0 && iMaxMunition - pLoopUnit->getMunition() > 0)
		{
			return false;
		}
	}
	return true;
}

///*****************************************************
///
///	iMaxMunitions : max munitions units can load 
///	return munitions units have loadded
///
///*****************************************************
int CvSelectionGroupAI::AI_resupplyShips(int iMaxMunitions)
{
	CLLNode<IDInfo>* pEntityNode;
	CvUnit* pLoopUnit;

	pEntityNode = headUnitNode();

	int iLoadedAmmunitions = 0;
	while (pEntityNode != NULL)
	{
		pLoopUnit = ::getUnit(pEntityNode->m_data);
		pEntityNode = nextUnitNode(pEntityNode);

		int iMaxAmmunition = pLoopUnit->getMaxMunition();
		int iMissingAmmunitions = iMaxAmmunition - pLoopUnit->getMunition();
		int iRemainingAmmunitions = iMaxAmmunition - iLoadedAmmunitions;
		if (iMaxAmmunition > 0 && iMissingAmmunitions > 0 && iRemainingAmmunitions > 0)
		{
			int iAmmunitionToLoad = std::min(iMissingAmmunitions, iRemainingAmmunitions);
			pLoopUnit->changeMunition(iAmmunitionToLoad);
			iLoadedAmmunitions += iAmmunitionToLoad;
		}
	}
	return iLoadedAmmunitions;
}

CvUnit* CvSelectionGroupAI::AI_ejectBestDefender(CvPlot* pDefendPlot)
{
	CLLNode<IDInfo>* pEntityNode;
	CvUnit* pLoopUnit;

	pEntityNode = headUnitNode();

	CvUnit* pBestUnit = NULL;
	int iBestUnitValue = 0;

	while (pEntityNode != NULL)
	{
		pLoopUnit = ::getUnit(pEntityNode->m_data);
		pEntityNode = nextUnitNode(pEntityNode);

		if (!pLoopUnit->noDefensiveBonus())
		{
			int iValue = pLoopUnit->currEffectiveStr(pDefendPlot, NULL) * 100;

			if (pDefendPlot->isCity(true, getTeam()))
			{
				iValue *= 100 + pLoopUnit->cityDefenseModifier();
				iValue /= 100;
			}

			iValue *= 100;
			iValue /= (100 + pLoopUnit->cityAttackModifier() + pLoopUnit->getExtraCityAttackPercent());

			iValue /= 2 + pLoopUnit->getLevel();

			if (iValue > iBestUnitValue)
			{
				iBestUnitValue = iValue;
				pBestUnit = pLoopUnit;
			}
		}
	}

	if (NULL != pBestUnit && getNumUnits() > 1)
	{
		pBestUnit->joinGroup(NULL);
	}

	return pBestUnit;
}

int CvSelectionGroupAI::AI_getCargoNewSpace() const
{
	int iTotalAmount = 0;
	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);

		if (pLoopUnit != NULL && pLoopUnit->isOnlyDefensive())
		{
			iTotalAmount += pLoopUnit->getUnitInfo().getCargoNewSpace();
		}
	}
	return iTotalAmount;
}

// Protected Functions...

void CvSelectionGroupAI::read(FDataStreamBase* pStream)
{
	CvSelectionGroup::read(pStream);

	uint uiFlag=0;
	pStream->Read(&uiFlag);	// flags for expansion

	pStream->Read(&m_iMissionAIX);
	pStream->Read(&m_iMissionAIY);
	pStream->Read(&m_iGroupIdForMerging);
	pStream->Read(&m_iSeawayId);

	pStream->Read(&m_bForceSeparate);
	pStream->Read(&m_bShouldWaitForMerging);

	//Options for trade routes
	pStream->Read(&m_bStopConvoy);
	//pStream->Read(&m_bResupplyAuto);
	pStream->Read(&m_bAutoHeal);

	pStream->Read((int*)&m_eMissionAIType);

	m_missionAIUnit.read(pStream);

	pStream->Read(&m_bGroupAttack);
	pStream->Read(&m_iGroupAttackX);
	pStream->Read(&m_iGroupAttackY);
}


void CvSelectionGroupAI::write(FDataStreamBase* pStream)
{
	CvSelectionGroup::write(pStream);

	uint uiFlag=0;
	pStream->Write(uiFlag);		// flag for expansion

	pStream->Write(m_iMissionAIX);
	pStream->Write(m_iMissionAIY);
	pStream->Write(m_iGroupIdForMerging);
	pStream->Write(m_iSeawayId);

	pStream->Write(m_bForceSeparate);
	pStream->Write(m_bShouldWaitForMerging);

	//Options for trade routes
	pStream->Write(m_bStopConvoy);
	//pStream->Write(m_bResupplyAuto);
	pStream->Write(m_bAutoHeal);

	pStream->Write(m_eMissionAIType);

	m_missionAIUnit.write(pStream);

	pStream->Write(m_bGroupAttack);
	pStream->Write(m_iGroupAttackX);
	pStream->Write(m_iGroupAttackY);
}

// Private Functions...
