#pragma once

class CvCityJob
{
public:
	CvCityJob();
	~CvCityJob();

	void init(PlayerTypes ePlayer, const IDInfo& kCity, JobTypes eJob, unsigned char ucNumRequired, ProfessionTypes eProfession = NO_PROFESSION);
	void uninit();
	
	int getID() const;
	void setID(int iID);

	JobTypes getJobType() const;
	const IDInfo& getCity() const;
	void setCity(const IDInfo& kCity);

	void doTurn();

	void updateRequiredUnitClass();
	
	void updateColonistNeeded();
	void updateAgronomistNeeded();
	void updateScoutsNeeded();
	void updateWorkersNeeded();
	void updateDefendersNeeded();

	void adpaptCityDefenders();
	void checkIfUnitsShouldChangeCity();
	bool shouldBuyMuskets() const;

	ProfessionTypes getEuropeProfession() const;
	UnitAITypes getUnitAIType() const;
	bool shouldRecruitColonist() const;
	UnitClassTypes getUnitClassToRecruit() const;
	UnitClassTypes findBestUnitClass() const;
	PlayerTypes getOwner() const;
	ProfessionTypes getProfession() const;

	int getNumRequiredUnitClasses() const;
	UnitClassTypes getRequiredUnitClassType(int i = 0) const;
	int getRequiredUnitClassAmount(int i) const;
	void CvCityJob::addRequiredUnitType(UnitTypes eUnitType);
	void addRequiredUnitClass(UnitClassTypes eUnitClassType, byte iAmount);
	void setRequiredUnitClassAmount(int i, byte iAmount);

	int getNumUnitNeeded() const;
	byte getNumRequired() const;
	void setNumRequired(byte ucValue);
	void changeNumRequired(unsigned char ucChange);

	int getNumUnits() const;
	void addUnit(CvUnit* pUnit);
	void removeUnit(CvUnit* pUnit, bool bRemoveCityID = true);
	void cleanUnits();
	CLLNode<IDInfo>* headUnitNode() const;
	CLLNode<IDInfo>* nextUnitNode(CLLNode<IDInfo>* pNode) const;

	const wchar* getProfessionHelp() const;

	void read(FDataStreamBase* stream);
	void write(FDataStreamBase* stream) const;

private :
	ProfessionTypes getDefaultProfession(JobTypes eJob, ProfessionTypes eProfession) const;
	
protected:
	int m_iID;
	byte m_ucNumRequired;
	PlayerTypes m_eOwner;
	JobTypes m_eJob;
	ProfessionTypes m_eProfession;
	std::vector< std::pair<UnitClassTypes, byte> > m_aRequiredUnitClasses;
	IDInfo m_kCity;
	CLinkList<IDInfo> m_units;
};

