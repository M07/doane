//------------------------------------------------------------------------------------------------
//
//  ***************** CIV4 GAME ENGINE   ********************
//
//! \file		CvTrade.cpp
//! \author		M07
//! \brief		Implementation of trade in popups
//
//------------------------------------------------------------------------------------------------
//  Copyright (c) 2011 Fratelli Marconni, Inc. All rights reserved.
//------------------------------------------------------------------------------------------------

#include "CvGameCoreDLL.h"
#include "CvDLLPythonIFaceBase.h"
#include "CvDLLInterfaceIFaceBase.h"
#include "CvDLLEntityIFaceBase.h"
#include "CvGameCoreUtils.h"
#include "CvDLLEngineIFaceBase.h"
#include "CvDLLEventReporterIFaceBase.h"
#include "CvPlayerAI.h"
#include "CvGameAI.h"
#include "CvInfos.h"
#include "CvTrade.h"

//----------------------------------------------------------------------------
//
//	FUNCTION:	GetInstance()
//
//	PURPOSE:	Get the instance of this class.
//
//----------------------------------------------------------------------------
CvTrade& CvTrade::GetInstance()
{
	static CvTrade gs_GameTradeMgr;
	return gs_GameTradeMgr;
}

//----------------------------------------------------------------------------
//
//	FUNCTION:	CvTrade()
//
//	PURPOSE:	Constructor
//
//----------------------------------------------------------------------------
CvTrade::CvTrade()
{

}

CvTrade::~CvTrade()
{
}

//Local Context
ProposalTypes CvTrade::whatKindOfTradeProposal(PlayerTypes ePlayer, bool bAskProposition)
{
	bool bTestDon = false, bTestReq = false;
	ProposalTypes tResult = NO_PROPOSAL;

	if (ePlayer == NO_PLAYER)
	{
		return NO_PROPOSAL;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	if (bAskProposition)
	{
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
		{
			YieldTypes eYield = (YieldTypes) iYield;
			if (kPlayer.getHumanTradeProposition(eYield) > 0)
			{
				return PROPOSAL_SUGGEST_YIELDS;
			}
			if (kPlayer.getIATradeProposition(eYield) > 0)
			{
				return PROPOSAL_ASK_YIELDS;
			}
		}
		return PROPOSAL_FROM_SCRATCH;
	}

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (!bTestDon && kPlayer.getHumanTradeProposition(eYield) > 0)
		{
			bTestDon = true;
		}
		if (!bTestReq && kPlayer.getIATradeProposition(eYield) > 0)
		{
			bTestReq = true;
		}
	}
	if (bTestReq && bTestDon)
	{
		tResult = PROPOSAL_EXCHANGE;
	}
	else
	{
		if (bTestReq)
		{
			tResult = PROPOSAL_REQUEST;
		}
		if (bTestDon)
		{
			tResult = PROPOSAL_GIFT;
		}
	}
	return tResult;
}

void CvTrade::calculateGiftOrRequest(CvUnit* pUnit, CvCity* pCity, int aiYields[NUM_YIELD_TYPES], bool bGift, bool bRequest, bool bAsync) const
{
	FAssertMsg(pUnit != NULL, "pUnit must be different to null");
	FAssertMsg(pCity != NULL, "pCity must be different to null");
	FAssertMsg(bGift != bRequest, "bGift has to be diffferent to bRequest");

	PlayerTypes ePlayer = pUnit->getOwner();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvPlayer& kOtherPlayer = GET_PLAYER(pCity->getOwner());

	int iValueYields = 0;
	int iIncomingYield = 0;

	YieldTypes eProduceYield = pCity->getNativeYieldProduce();
	PlayerTypes eTradePlayer = pCity->tradeProducedYieldWith();

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		CvYieldInfo& kYield = GC.getYieldInfo(eYield);

		aiYields[eYield] = 0;

		int iYieldDesired = pCity->getYieldDesired(eYield);
		int iStoredYield = pCity->getYieldStored(eYield);
		if (bGift)
		{
			int HumanTradeProp = kPlayer.getHumanTradeProposition(eYield);//What we chose in our transport unit
			if (HumanTradeProp > 0)
			{
				int iValue = std::min(iYieldDesired, HumanTradeProp);
				iValueYields += kOtherPlayer.getPlayerValue(eYield, iValue);
				aiYields[eYield] = iValue;
			}
		}

		if (bRequest)
		{
			int AITradeProp = kPlayer.getIATradeProposition(eYield);//What we chose from the city stock
			if (AITradeProp > 0)
			{
				if (iStoredYield > iYieldDesired)
				{
					int iValue = std::min(iStoredYield - iYieldDesired, AITradeProp);
					iValueYields += kOtherPlayer.getPlayerValue(eYield, iValue);
					aiYields[eYield] = iValue;
					iIncomingYield += iValue;
				}
			}
		}
	}
	if (bRequest)
	{
		int iSpaceUnit = pUnit->getSpaceNewCargo();
		FAssertMsg(iSpaceUnit >= 0, "iSpaceUnit expected to be >= 0");

		if (iSpaceUnit < iIncomingYield)
		{
			int iQuantityToRemove = iIncomingYield - iSpaceUnit;
			int iOffsetYield = bAsync ? GC.getASyncRand().get(NUM_YIELD_TYPES, "iOffsetYield ASYNC") : GC.getGameINLINE().getSorenRandNum(NUM_YIELD_TYPES, "iOffsetYield");
			//We have still too much yield, so we will decrease chose yields
			for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
			{
				YieldTypes eYield = (YieldTypes) ((iYield+iOffsetYield)%NUM_YIELD_TYPES);
				if (aiYields[eYield] > 0)
				{
					int iDecrease = std::min(iQuantityToRemove, aiYields[eYield]);
					aiYields[eYield] -= iDecrease;
					iQuantityToRemove -= iDecrease;
					iIncomingYield -= iDecrease;
				}
			}
			FAssertMsg(iSpaceUnit == iIncomingYield, "iSpaceUnit should be equal to iIncomingYield");
		}
	}
}

void CvTrade::calculateWhatIsTheBetterDeal(CvUnit* pUnit, CvCity* pCity, int aiProposedYields[NUM_YIELD_TYPES], int aiUnitYields[NUM_YIELD_TYPES], bool bAIProp, bool bHumanProp, bool bAsync) const
{
	FAssertMsg(pUnit != NULL, "pUnit must be different to null");
	FAssertMsg(pCity != NULL, "pCity must be different to null");

	PlayerTypes ePlayer = pUnit->getOwner();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvPlayer& kOtherPlayer = GET_PLAYER(pCity->getOwner());

	int aiInitProposedYields[NUM_YIELD_TYPES];
	int aiInitUnitYields[NUM_YIELD_TYPES];

	int iProposedYieldsValue = 0;
	int iValueUnitCargo = 0;

	YieldTypes eProduceYield = pCity->getNativeYieldProduce();
	PlayerTypes eTradePlayer = pCity->tradeProducedYieldWith();

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		CvYieldInfo& kYield = GC.getYieldInfo(eYield);

		aiInitProposedYields[eYield] = 0;
		aiInitUnitYields[eYield] = 0;
		int iYieldCargo = pUnit->getNewCargoYield(eYield);
		int iYieldDesired = pCity->getYieldDesired(eYield);
		int iStoredYield = pCity->getYieldStored(eYield);
		if (iYieldDesired > 0)
		{
			if (bHumanProp)
			{
				int HumanTradeProp = kPlayer.getHumanTradeProposition(eYield);//What we chose in our transport unit
				if (HumanTradeProp > 0)
				{
					int iValue = std::min(iYieldDesired, HumanTradeProp);
					iValueUnitCargo += kOtherPlayer.getPlayerValue(eYield, iValue);
					aiInitUnitYields[eYield] = iValue;
				}
			}
			else
			{
				if (iYieldCargo > 0)
				{
					int iValue = std::min(iYieldDesired, iYieldCargo);
					iValueUnitCargo +=  kOtherPlayer.getPlayerValue(eYield, iValue);
					aiInitUnitYields[eYield] = iValue;
				}
			}
		}

		if (bAIProp)
		{
			int AITradeProp = kPlayer.getIATradeProposition(eYield);//What we chose from the city stock
			if (AITradeProp > 0)
			{
				if (iStoredYield > iYieldDesired)
				{
					int iValue = std::min(iStoredYield - iYieldDesired, AITradeProp);
					iProposedYieldsValue += kOtherPlayer.getPlayerValue(eYield, iValue);
					aiInitProposedYields[eYield] = iValue;
				}
			}
		}
		else
		{
			if (kYield.canBeNativeProposal() && !kPlayer.isHasYieldUnknown(eYield) && iStoredYield > 0)
			{
				if (eYield != eProduceYield || eTradePlayer == NO_PLAYER)
				{
					int iValue = iStoredYield - iYieldDesired;
					if (iValue > 0)
					{
						iProposedYieldsValue += kOtherPlayer.getPlayerValue(eYield, iValue);
						aiInitProposedYields[eYield] = iValue;
					}
				}
			}
		}
	}

	int iProposedPercent = 0;
	int iWantedPercent = 0; 
	if (iProposedYieldsValue > 0)
	{
		iProposedPercent = std::min(iValueUnitCargo*100/iProposedYieldsValue, 100);
	}
	if (iValueUnitCargo > 0)
	{
		iWantedPercent = std::min(iProposedYieldsValue*100/iValueUnitCargo, 100);
	}
	int iSpaceUnit = pUnit->getSpaceNewCargo();
	FAssertMsg(iSpaceUnit >= 0, "iSpaceUnit expected to be >= 0");

	int iIncomingYield = 0;
	int iTradedYield = 0;
	// We applies percent to each quantities to have a fair trade
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		aiProposedYields[eYield] = aiInitProposedYields[eYield]*iProposedPercent/100;
		aiUnitYields[eYield] = aiInitUnitYields[eYield]*iWantedPercent/100;
		iIncomingYield += aiProposedYields[eYield] - aiUnitYields[eYield];// We try to know how many yield will be added into transport unit
		iTradedYield += aiProposedYields[eYield] + aiUnitYields[eYield];// We try to know how many yield will be traded
	}

	if (iSpaceUnit < iIncomingYield)
	{
		FAssertMsg(iTradedYield > 0, "iTradedYield is expected to be > 0");		
		int iQuantityToRemove = iIncomingYield - iSpaceUnit;
		int iOffsetYield = bAsync ? GC.getASyncRand().get(NUM_YIELD_TYPES, "iOffsetYield ASYNC") : GC.getGameINLINE().getSorenRandNum(NUM_YIELD_TYPES, "iOffsetYield");
		//We have still too much yield, so we will decrease chose yields
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
		{
			YieldTypes eYield = (YieldTypes) ((iYield+iOffsetYield)%NUM_YIELD_TYPES);
			if (aiProposedYields[eYield] > 0)
			{
				int iDecrease = std::min(iQuantityToRemove, aiProposedYields[eYield]);
				aiProposedYields[eYield] -= iDecrease;
				iQuantityToRemove -= iDecrease;
				iIncomingYield -= iDecrease;
			}
		}
		FAssertMsg(iSpaceUnit == iIncomingYield, "iSpaceUnit should be equal to iIncomingYield");
	}
}

//Local Context
bool CvTrade::askGiftOrRequest(CvPopup* pPopup, CvUnit* pUnit, bool bGift, bool bRequest)
{
	FAssertMsg(bGift != bRequest, "bGift has to be diffferent to bRequest");
	PlayerTypes ePlayer = pUnit->getOwner();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvCity* pCity =  pUnit->plot()->getPlotCity();
	CvPlayer& kOtherPlayer = GET_PLAYER(pCity->getOwner());

	int aiYields[NUM_YIELD_TYPES];

	calculateGiftOrRequest(pUnit, pCity, aiYields, bGift, bRequest);

	int iValueYields = 0;

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;

		if (aiYields[eYield] > 0)
		{
			iValueYields += kOtherPlayer.getPlayerValue(eYield, aiYields[eYield]);
		}
	}

	//If they want nothing in our transport unit, we cancel the deal
	if (bGift && iValueYields == 0)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REAL_TRADE_8", kOtherPlayer.getCivilizationDescription()));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, 0, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
		return true;
	}

	//If they don"t want to give us any yield in their city, we cancel the deal
	if (bRequest && iValueYields == 0)
	{
		int iSpaceUnit = pUnit->getSpaceNewCargo();
		if (iSpaceUnit == 0)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_INCORRECTLY_TRANSACT2"));
		}
		else
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REFUSE_ALL_RESOURCES", kOtherPlayer.getCivilizationDescription()));
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, 0, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
		return true;
	}

	if (bRequest && ((kOtherPlayer.AI_getMemoryCount(ePlayer, MEMORY_AI_GIFT_TRADE) - iValueYields) < -25))
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REFUSE_REQUEST", kOtherPlayer.getCivilizationDescription()));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, 0, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
		return true;
	}

	CvWStringBuffer szYields;
	int iCountYield = 0;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (aiYields[eYield] > 0)
		{
			if (iCountYield > 0)
			{
				szYields.append(L", ");
			}
			szYields.append(gDLL->getText("TXT_KEY_TITLE_POPUP_LIST_RESOURCES", aiYields[eYield], GC.getYieldInfo(eYield).getChar()));
			iCountYield++;
		}
		if (bRequest)
		{
			kPlayer.setIATradeProposition(eYield, aiYields[eYield]);
		}
		if (bGift)
		{
			kPlayer.setHumanTradeProposition(eYield, aiYields[eYield]);
		}
	}
	if (bGift)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_ACCEPT_SOME_RESOURCES"));
	}
	if (bRequest)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_ACCEPT_SOME_RESOURCES_REQUESTS"));
	}
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szYields.getCString());
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("USER_DIPLO_ACCEPT_OFFER_1"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, -1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvTrade::askProposal(CvPopup* pPopup, CvUnit* pUnit, bool bCityYieldsChose, bool bUniYieldsChose)
{
	PlayerTypes ePlayer = pUnit->getOwner();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvCity* pCity =  pUnit->plot()->getPlotCity();
	CvPlayer& kOtherPlayer = GET_PLAYER(pCity->getOwner());

	int aiProposedYields[NUM_YIELD_TYPES];
	int aiUnitYields[NUM_YIELD_TYPES];

	calculateWhatIsTheBetterDeal(pUnit, pCity, aiProposedYields, aiUnitYields, bCityYieldsChose, bUniYieldsChose);

	int iValueProposition = 0, iValueUnitCargo = 0;

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;

		if (aiProposedYields[eYield] > 0)
		{
			iValueProposition += kOtherPlayer.getPlayerValue(eYield, aiProposedYields[eYield]);
		}

		//On calcule la veleur marchande des soutes de l'unit�
		if (aiUnitYields[eYield] > 0)
		{
			iValueUnitCargo += kOtherPlayer.getPlayerValue(eYield, aiUnitYields[eYield]);
		}
	}
	//If they want nothing in our transport unit, we cancel the deal
	if (iValueUnitCargo == 0)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REAL_TRADE_8", kOtherPlayer.getCivilizationDescription()));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, 0, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
		return true;
	}

	if (iValueProposition == 0)
	{
		//Nothing to propose
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REAL_TRADE_7", kOtherPlayer.getCivilizationDescription()));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, 0, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
		return true;
	}

	int iMemoryAiGiftTrade = kOtherPlayer.AI_getMemoryCount(ePlayer, MEMORY_AI_GIFT_TRADE);
	int iRelationTrade = kOtherPlayer.getRelationTradeTo(ePlayer); // Entre -15 et 15

	int iLimit = iRelationTrade >= 6 ? 20*iValueProposition/100 : 25*iValueProposition/100;

	//Si la valeur marchande des soutes n'est pas suffisante on refuse l'�change
	if (iValueUnitCargo < iLimit)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REAL_TRADE_9", kOtherPlayer.getCivilizationDescription()));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, 0, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
		return true;
	}

	int iCountProposedYield = 0, iCountWantedYield = 0;
	CvWStringBuffer szChoseYield, szWantedYield;

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (aiProposedYields[eYield] > 0)
		{
			if (iCountProposedYield > 0)
			{
				szChoseYield.append(L", ");
			}
			szChoseYield.append(gDLL->getText("TXT_KEY_TITLE_POPUP_LIST_RESOURCES", aiProposedYields[eYield], GC.getYieldInfo(eYield).getChar()));
			iCountProposedYield++;
		}
		kPlayer.setIATradeProposition(eYield, aiProposedYields[eYield]);
		if (aiUnitYields[eYield] > 0)
		{
			if (iCountWantedYield > 0)
			{
				szWantedYield.append(L", ");
			}
			szWantedYield.append(gDLL->getText("TXT_KEY_TITLE_POPUP_LIST_RESOURCES", aiUnitYields[eYield], GC.getYieldInfo(eYield).getChar()));
			iCountWantedYield++;
		}
		kPlayer.setHumanTradeProposition(eYield, aiUnitYields[eYield]);
	}
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REAL_TRADE_6", szChoseYield.getCString(), szWantedYield.getCString()));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("USER_DIPLO_ACCEPT_OFFER_1"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, -1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvTrade::AI_askProposal(CvUnit* pUnit, bool bAsync)
{
	PlayerTypes ePlayer = pUnit->getOwner();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvCity* pCity =  pUnit->plot()->getPlotCity();
	CvPlayer& kOtherPlayer = GET_PLAYER(pCity->getOwner());

	int aiProposedYields[NUM_YIELD_TYPES];
	int aiUnitYields[NUM_YIELD_TYPES];
	bool bCityYieldsChose = false;
	bool bUniYieldsChose = false;
	calculateWhatIsTheBetterDeal(pUnit, pCity, aiProposedYields, aiUnitYields, bCityYieldsChose, bUniYieldsChose, bAsync);

	int iValueProposition = 0, iValueUnitCargo = 0;

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;

		if (aiProposedYields[eYield] > 0)
		{
			iValueProposition += kOtherPlayer.getPlayerValue(eYield, aiProposedYields[eYield]);
		}

		//On calcule la veleur marchande des soutes de l'unit�
		if (aiUnitYields[eYield] > 0)
		{
			iValueUnitCargo += kOtherPlayer.getPlayerValue(eYield, aiUnitYields[eYield]);
		}
	}
	//If they want nothing in our transport unit, we cancel the deal
	if (iValueUnitCargo == 0)
	{
		return false;
	}

	if (iValueProposition == 0)
	{
		return false;
	}

	int iMemoryAiGiftTrade = kOtherPlayer.AI_getMemoryCount(ePlayer, MEMORY_AI_GIFT_TRADE);
	int iRelationTrade = kOtherPlayer.getRelationTradeTo(ePlayer); // Entre -15 et 15

	int iLimit = iRelationTrade >= 6 ? 20*iValueProposition/10 : 25*iValueProposition/100;

	//Si la valeur marchande des soutes n'est pas suffisante on refuse l'�change
	if (iValueUnitCargo < iLimit)
	{
		return false;
	}

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		kPlayer.setIATradeProposition(eYield, aiProposedYields[eYield]);
		kPlayer.setHumanTradeProposition(eYield, aiUnitYields[eYield]);
	}

	return true;
}

//Global Context
void CvTrade::processProposal(CvUnit* pUnit, int iButtonClicked)
{
	PlayerTypes ePlayer = pUnit->getOwner();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvCity* pCity = pUnit->plot()->getPlotCity();
	PlayerTypes eOtherPlayer = pCity->getOwner();
	CvPlayer& kOtherPlayer = GET_PLAYER(eOtherPlayer);

	if (iButtonClicked > 0)
	{
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
		{
			YieldTypes eYield = (YieldTypes) iYield;
			int HumanTradeProp = kPlayer.getHumanTradeProposition(eYield);//What we chose in our transport unit
			int	IATradeProp = kPlayer.getIATradeProposition(eYield);//What we chose in their city
			if (IATradeProp > 0)
			{
				pUnit->changeNewCargoYield(eYield, IATradeProp);
				pCity->changeYieldStored(eYield, -IATradeProp);				
				kOtherPlayer.AI_changeMemoryCount(kPlayer.getID(), MEMORY_AI_GIFT_TRADE, -kOtherPlayer.getPlayerValue(eYield, IATradeProp));
			}
			if (HumanTradeProp)
			{
				pUnit->changeNewCargoYield(eYield, -HumanTradeProp);
				pCity->changeYieldStored(eYield, HumanTradeProp);
				pCity->changeYieldDesired(eYield, -HumanTradeProp);
				kOtherPlayer.AI_changeMemoryCount(kPlayer.getID(), MEMORY_AI_GIFT_TRADE, kOtherPlayer.getPlayerValue(eYield, HumanTradeProp));
				kOtherPlayer.changePlayerValue(eYield, -HumanTradeProp);
			}
		}
		if (!kPlayer.getRegularityTradeInTurn(eOtherPlayer))
		{
			kPlayer.setRegularityTrade(eOtherPlayer, 0);
			kPlayer.setRegularityTradeInTurn(eOtherPlayer, true);
			if (kOtherPlayer.AI_getMemoryCount(ePlayer, MEMORY_AI_REGULARITY_TRADE) != 3)
			{
				kOtherPlayer.AI_changeMemoryCount(ePlayer, MEMORY_AI_REGULARITY_TRADE, 1);
			}
		}
	}
	if (iButtonClicked == -1)
	{
		//Incremente un compteur
		//Possibilite d'empecher une transact pendant plusieurs tours

		pCity->changeDissatisfiedCptTo(ePlayer, 1);
		int iValue = GC.getGameINLINE().getSorenRandNum(5, "Native Ask Transac")+1;
		if (iValue < pCity->getDissatisfiedCptTo(ePlayer))
		{
			pCity->setBanTurnTo(ePlayer, iValue);
			pCity->setDissatisfiedCptTo(ePlayer, 0);

			if (kPlayer.isHuman())
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_STOP_TRADE, pUnit->getID(), -1, -1);
				gDLL->getInterfaceIFace()->addPopup(pInfo, ePlayer, true);
			}
		}
	}
	if (kPlayer.isHuman())
	{
		gDLL->getInterfaceIFace()->setDirty(NativeYieldTradeResetTrade_DIRTY_BIT, true);
	}
}
