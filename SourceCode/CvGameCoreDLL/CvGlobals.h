#pragma once

// CvGlobals.h

#ifndef CIV4_GLOBALS_H
#define CIV4_GLOBALS_H

//#include "CvStructs.h"
//
// 'global' vars for Civ IV.  singleton class.
// All globals and global types should be contained in this class
// Author -	Mustafa Thamer
//

class CvDLLUtilityIFaceBase;
class CvRandom;
class CvGameAI;
class CMessageControl;
class CvDropMgr;
class CMessageQueue;
class CvSetupData;
class CvInitCore;
class CvMessageCodeTranslator;
class CvPortal;
class CvStatsReporter;
class CvDLLInterfaceIFaceBase;
class CvPlayerAI;
class CvDiplomacyScreen;
class CvCivicsScreen;
class CvWBUnitEditScreen;
class CvWBCityEditScreen;
class CMPDiplomacyScreen;
class FMPIManager;
class FAStar;
class CvInterface;
class CMainMenu;
class CvEngine;
class CvArtFileMgr;
class FVariableSystem;
class CvMap;
class CvPlayerAI;
class CvTeamAI;
class CvInterfaceModeInfo;
class CvWorldInfo;
class CvClimateInfo;
class CvSeaLevelInfo;
class CvEuropeInfo;
class CvColorInfo;
class CvPlayerColorInfo;
class CvRouteModelInfo;
class CvRiverModelInfo;
class CvWaterPlaneInfo;
class CvTerrainPlaneInfo;
class CvCameraOverlayInfo;
class CvAnimationPathInfo;
class CvAnimationCategoryInfo;
class CvEntityEventInfo;
class CvEffectInfo;
class CvAttachableInfo;
class CvUnitFormationInfo;
class CvGameText;
class CvLandscapeInfo;
class CvTerrainInfo;
class CvBonusInfo;
class CvFeatureInfo;
class CvCivilizationInfo;
class CvLeaderHeadInfo;
class CvTraitInfo;
class CvCursorInfo;
class CvSlideShowInfo;
class CvSlideShowRandomInfo;
class CvWorldPickerInfo;
class CvUnitInfo;
class CvSpecialUnitInfo;
class CvInfoBase;
class CvYieldInfo;
class CvRouteInfo;
class CvImprovementInfo;
class CvGoodyInfo;
class CvBuildInfo;
class CvHandicapInfo;
class CvGameSpeedInfo;
class CvAlarmInfo;
class CvTurnTimerInfo;
class CvBuildingClassInfo;
class CvBuildingInfo;
class CvSpecialBuildingInfo;
class CvUnitClassInfo;
class CvActionInfo;
class CvMissionInfo;
class CvControlInfo;
class CvCommandInfo;
class CvAutomateInfo;
class CvPromotionInfo;
class CvProfessionInfo;
class CvCivicInfo;
class CvDiplomacyInfo;
class CvEraInfo;
class CvNewEraInfo;
class CvEconomicTrendInfo;
class CvPirateInfo;
class CvWarPlanInfo;
class CvWarPlanStrategyInfo;
class CvArtilleryPenaltyInfo;
class CvAchieveInfo;
class CvContactInfo;
class CvAttitudeInfo;
class CvMemoryInfo;
class CvHurryInfo;
class CvEmphasizeInfo;
class CvCultureLevelInfo;
class CvVictoryInfo;
class CvGameOptionInfo;
class CvMPOptionInfo;
class CvForceControlInfo;
class CvPlayerOptionInfo;
class CvGraphicOptionInfo;
class CvEventTriggerInfo;
class CvEventInfo;
class CvFatherInfo;
class CvFatherPointInfo;
class CvUnitArtStyleTypeInfo;
class CvMainMenuInfo;


class CvGlobals
{
//	friend class CvDLLUtilityIFace;
	friend class CvXMLLoadUtility;
public:

	// singleton accessor
	DllExport inline static CvGlobals& getInstance();

	DllExport CvGlobals();
	DllExport virtual ~CvGlobals();
// PatchMod: Achievements START
protected:
	std::vector<CvAchieveInfo*> m_paAchieveInfo;
public:
	DllExport int getNumAchieveInfos();
	DllExport std::vector<CvAchieveInfo*>& getAchieveInfo();
	DllExport CvAchieveInfo& getAchieveInfo(AchieveTypes eAchieve);
	// PatchMod: Achievements END

	DllExport void init();
	DllExport void uninit();
	DllExport void clearTypesMap();

	DllExport CvDiplomacyScreen* getDiplomacyScreen();
	DllExport CMPDiplomacyScreen* getMPDiplomacyScreen();

	DllExport FMPIManager*& getFMPMgrPtr();
	DllExport CvPortal& getPortal();
	DllExport CvSetupData& getSetupData();
	DllExport CvInitCore& getInitCore();
	DllExport CvInitCore& getLoadedInitCore();
	DllExport CvInitCore& getIniInitCore();
	DllExport CvMessageCodeTranslator& getMessageCodes();
	DllExport CvStatsReporter& getStatsReporter();
	DllExport CvStatsReporter* getStatsReporterPtr();
	DllExport CvInterface& getInterface();
	DllExport CvInterface* getInterfacePtr();
	DllExport int getMaxCivPlayers() const;
#ifdef _USRDLL
	CvMap& getMapINLINE() { return *m_map; }				// inlined for perf reasons, do not use outside of dll
	CvGameAI& getGameINLINE() { return *m_game; }			// inlined for perf reasons, do not use outside of dll
#endif
	DllExport CvMap& getMap();
	DllExport CvGameAI& getGame();
	DllExport CvGameAI *getGamePointer();
	DllExport CvRandom& getASyncRand();
	DllExport CMessageQueue& getMessageQueue();
	DllExport CMessageQueue& getHotMessageQueue();
	DllExport CMessageControl& getMessageControl();
	DllExport CvDropMgr& getDropMgr();
	DllExport FAStar& getPathFinder();
	DllExport FAStar& getInterfacePathFinder();
	DllExport FAStar& getStepFinder();
	DllExport FAStar& getRouteFinder();
	DllExport FAStar& getBorderFinder();
	DllExport FAStar& getAreaFinder();
	DllExport NiPoint3& getPt3Origin();

	DllExport std::vector<CvInterfaceModeInfo*>& getInterfaceModeInfo();
	DllExport CvInterfaceModeInfo& getInterfaceModeInfo(InterfaceModeTypes e);

	DllExport NiPoint3& getPt3CameraDir();

	DllExport bool& getLogging();
	DllExport bool& getRandLogging();
	DllExport bool& getSynchLogging();
	DllExport bool& overwriteLogs();

	DllExport int* getPlotDirectionX();
	DllExport int* getPlotDirectionY();
	DllExport int* getPlotCardinalDirectionX();
	DllExport int* getPlotCardinalDirectionY();
	DllExport int* getCityPlotX();
	DllExport int* getCityPlotY();
	DllExport int* getCityPlotPriority();
	DllExport int getXYCityPlot(int i, int j);
	DirectionTypes* getTurnLeftDirection();
	DirectionTypes getTurnLeftDirection(int i);
	DirectionTypes* getTurnRightDirection();
	DirectionTypes getTurnRightDirection(int i);
	DllExport DirectionTypes getXYDirection(int i, int j);

	//
	// Global Infos
	// All info type strings are upper case and are kept in this hash map for fast lookup
	//
	DllExport int getInfoTypeForString(const char* szType, bool hideAssert = false) const;			// returns the infos index, use this when searching for an info type string
	DllExport void setInfoTypeFromString(const char* szType, int idx);
	DllExport void infoTypeFromStringReset();
	DllExport void addToInfosVectors(void *infoVector);
	DllExport void infosReset();

	DllExport int getNumWorldInfos();
	DllExport std::vector<CvWorldInfo*>& getWorldInfo();
	DllExport CvWorldInfo& getWorldInfo(WorldSizeTypes e);

	DllExport int getNumClimateInfos();
	DllExport std::vector<CvClimateInfo*>& getClimateInfo();
	DllExport CvClimateInfo& getClimateInfo(ClimateTypes e);

	DllExport int getNumSeaLevelInfos();
	DllExport std::vector<CvSeaLevelInfo*>& getSeaLevelInfo();
	DllExport CvSeaLevelInfo& getSeaLevelInfo(SeaLevelTypes e);

	DllExport int getNumEuropeInfos();
	DllExport std::vector<CvEuropeInfo*>& getEuropeInfo();
	DllExport CvEuropeInfo& getEuropeInfo(EuropeTypes e);

	DllExport int getNumColorInfos();
	DllExport std::vector<CvColorInfo*>& getColorInfo();
	DllExport CvColorInfo& getColorInfo(ColorTypes e);

	DllExport int getNumPlayerColorInfos();
	DllExport std::vector<CvPlayerColorInfo*>& getPlayerColorInfo();
	DllExport CvPlayerColorInfo& getPlayerColorInfo(PlayerColorTypes e);

	DllExport int getNumHints();
	DllExport std::vector<CvInfoBase*>& getHints();
	DllExport CvInfoBase& getHints(int i);

	DllExport int getNumMainMenus();
	DllExport std::vector<CvMainMenuInfo*>& getMainMenus();
	DllExport CvMainMenuInfo& getMainMenus(int i);

	DllExport int getNumRouteModelInfos();
	DllExport std::vector<CvRouteModelInfo*>& getRouteModelInfo();
	DllExport CvRouteModelInfo& getRouteModelInfo(int i);

	DllExport int getNumRiverModelInfos();
	DllExport std::vector<CvRiverModelInfo*>& getRiverModelInfo();
	DllExport CvRiverModelInfo& getRiverModelInfo(int i);

	DllExport int getNumWaterPlaneInfos();
	DllExport std::vector<CvWaterPlaneInfo*>& getWaterPlaneInfo();
	DllExport CvWaterPlaneInfo& getWaterPlaneInfo(int i);

	DllExport int getNumTerrainPlaneInfos();
	DllExport std::vector<CvTerrainPlaneInfo*>& getTerrainPlaneInfo();
	DllExport CvTerrainPlaneInfo& getTerrainPlaneInfo(int i);

	DllExport int getNumCameraOverlayInfos();
	DllExport std::vector<CvCameraOverlayInfo*>& getCameraOverlayInfo();
	DllExport CvCameraOverlayInfo& getCameraOverlayInfo(int i);

	DllExport int getNumAnimationPathInfos();
	DllExport std::vector<CvAnimationPathInfo*>& getAnimationPathInfo();
	DllExport CvAnimationPathInfo& getAnimationPathInfo(AnimationPathTypes e);

	DllExport int getNumAnimationCategoryInfos();
	DllExport std::vector<CvAnimationCategoryInfo*>& getAnimationCategoryInfo();
	DllExport CvAnimationCategoryInfo& getAnimationCategoryInfo(AnimationCategoryTypes e);

	DllExport int getNumEntityEventInfos();
	DllExport std::vector<CvEntityEventInfo*>& getEntityEventInfo();
	DllExport CvEntityEventInfo& getEntityEventInfo(EntityEventTypes e);

	DllExport int getNumEffectInfos();
	DllExport std::vector<CvEffectInfo*>& getEffectInfo();
	DllExport CvEffectInfo& getEffectInfo(int i);

	DllExport int getNumAttachableInfos();
	DllExport std::vector<CvAttachableInfo*>& getAttachableInfo();
	DllExport CvAttachableInfo& getAttachableInfo(int i);

	DllExport int getNumUnitFormationInfos();
	DllExport std::vector<CvUnitFormationInfo*>& getUnitFormationInfo();
	DllExport CvUnitFormationInfo& getUnitFormationInfo(int i);

	DllExport int getNumGameTextXML();
	DllExport std::vector<CvGameText*>& getGameTextXML();

	DllExport int getNumLandscapeInfos();
	DllExport std::vector<CvLandscapeInfo*>& getLandscapeInfo();
	DllExport CvLandscapeInfo& getLandscapeInfo(int iIndex);
	DllExport int getActiveLandscapeID();
	DllExport void setActiveLandscapeID(int iLandscapeID);

	DllExport int getNumTerrainInfos();
	DllExport std::vector<CvTerrainInfo*>& getTerrainInfo();
	DllExport CvTerrainInfo& getTerrainInfo(TerrainTypes eTerrainNum);

	DllExport int getNumBonusInfos();
	DllExport std::vector<CvBonusInfo*>& getBonusInfo();
	DllExport CvBonusInfo& getBonusInfo(BonusTypes eBonusNum);

	DllExport int getNumFeatureInfos();
	DllExport std::vector<CvFeatureInfo*>& getFeatureInfo();
	DllExport CvFeatureInfo& getFeatureInfo(FeatureTypes eFeatureNum);

	DllExport int getNumCivilizationInfos();
	DllExport std::vector<CvCivilizationInfo*>& getCivilizationInfo();
	DllExport CvCivilizationInfo& getCivilizationInfo(CivilizationTypes eCivilizationNum);

	DllExport int getNumLeaderHeadInfos();
	DllExport std::vector<CvLeaderHeadInfo*>& getLeaderHeadInfo();
	DllExport CvLeaderHeadInfo& getLeaderHeadInfo(LeaderHeadTypes eLeaderHeadNum);

	DllExport int getNumTraitInfos();
	DllExport std::vector<CvTraitInfo*>& getTraitInfo();
	DllExport	CvTraitInfo& getTraitInfo(TraitTypes eTraitNum);

	DllExport int getNumCursorInfos();
	DllExport std::vector<CvCursorInfo*>& getCursorInfo();
	DllExport	CvCursorInfo& getCursorInfo(CursorTypes eCursorNum);

	DllExport int getNumSlideShowInfos();
	DllExport std::vector<CvSlideShowInfo*>& getSlideShowInfo();
	DllExport	CvSlideShowInfo& getSlideShowInfo(int iIndex);

	DllExport int getNumSlideShowRandomInfos();
	DllExport std::vector<CvSlideShowRandomInfo*>& getSlideShowRandomInfo();
	DllExport	CvSlideShowRandomInfo& getSlideShowRandomInfo(int iIndex);

	DllExport int getNumWorldPickerInfos();
	DllExport std::vector<CvWorldPickerInfo*>& getWorldPickerInfo();
	DllExport	CvWorldPickerInfo& getWorldPickerInfo(int iIndex);

	DllExport int getNumUnitInfos();
	DllExport std::vector<CvUnitInfo*>& getUnitInfo();
	DllExport	CvUnitInfo& getUnitInfo(UnitTypes eUnitNum);

	DllExport int getNumSpecialUnitInfos();
	DllExport std::vector<CvSpecialUnitInfo*>& getSpecialUnitInfo();
	DllExport	CvSpecialUnitInfo& getSpecialUnitInfo(SpecialUnitTypes eSpecialUnitNum);

	DllExport int getNumConceptInfos();
	DllExport std::vector<CvInfoBase*>& getConceptInfo();
	DllExport CvInfoBase& getConceptInfo(ConceptTypes e);
	DllExport int getNumCalendarInfos();
	DllExport std::vector<CvInfoBase*>& getCalendarInfo();
	DllExport CvInfoBase& getCalendarInfo(CalendarTypes e);

	DllExport int getNumSeasonInfos();
	DllExport std::vector<CvInfoBase*>& getSeasonInfo();
	DllExport CvInfoBase& getSeasonInfo(SeasonTypes e);

	DllExport int getNumMonthInfos();
	DllExport std::vector<CvInfoBase*>& getMonthInfo();
	DllExport CvInfoBase& getMonthInfo(MonthTypes e);

	DllExport int getNumDenialInfos();
	DllExport std::vector<CvInfoBase*>& getDenialInfo();
	DllExport CvInfoBase& getDenialInfo(DenialTypes e);

	DllExport int getNumInvisibleInfos();
	DllExport std::vector<CvInfoBase*>& getInvisibleInfo();
	DllExport CvInfoBase& getInvisibleInfo(InvisibleTypes e);

	DllExport int getNumFatherInfos();
	DllExport std::vector<CvFatherInfo*>& getFatherInfo();
	DllExport CvFatherInfo& getFatherInfo(FatherTypes e);

	DllExport int getNumFatherPointInfos();
	DllExport std::vector<CvFatherPointInfo*>& getFatherPointInfo();
	DllExport CvFatherPointInfo& getFatherPointInfo(FatherPointTypes e);

	DllExport int getNumUnitCombatInfos();
	DllExport std::vector<CvInfoBase*>& getUnitCombatInfo();
	DllExport CvInfoBase& getUnitCombatInfo(UnitCombatTypes e);

	DllExport std::vector<CvInfoBase*>& getDomainInfo();
	DllExport CvInfoBase& getDomainInfo(DomainTypes e);

	DllExport std::vector<CvInfoBase*>& getUnitAIInfo();
	DllExport CvInfoBase& getUnitAIInfo(UnitAITypes eUnitAINum);

	DllExport int getNumFatherCategoryInfos();
	DllExport std::vector<CvInfoBase*>& getFatherCategoryInfo();
	DllExport	CvInfoBase& getFatherCategoryInfo(FatherCategoryTypes eFatherCategoryNum);

	DllExport int getNumGameOptionInfos();
	DllExport std::vector<CvGameOptionInfo*>& getGameOptionInfo();
	DllExport	CvGameOptionInfo& getGameOptionInfo(GameOptionTypes eGameOptionNum);

	DllExport int getNumMPOptionInfos();
	DllExport std::vector<CvMPOptionInfo*>& getMPOptionInfo();
	DllExport	CvMPOptionInfo& getMPOptionInfo(MultiplayerOptionTypes eMPOptionNum);

	DllExport int getNumForceControlInfos();
	DllExport std::vector<CvForceControlInfo*>& getForceControlInfo();
	DllExport	CvForceControlInfo& getForceControlInfo(ForceControlTypes eForceControlNum);

	DllExport std::vector<CvPlayerOptionInfo*>& getPlayerOptionInfo();
	DllExport	CvPlayerOptionInfo& getPlayerOptionInfo(PlayerOptionTypes ePlayerOptionNum);

	DllExport std::vector<CvGraphicOptionInfo*>& getGraphicOptionInfo();
	DllExport	CvGraphicOptionInfo& getGraphicOptionInfo(GraphicOptionTypes eGraphicOptionNum);
	
	DllExport int getNumYieldInfos();
	DllExport std::vector<CvYieldInfo*>& getYieldInfo();
	DllExport	CvYieldInfo& getYieldInfo(YieldTypes eYieldNum);
	
	DllExport int getNumRouteInfos();
	DllExport std::vector<CvRouteInfo*>& getRouteInfo();
	DllExport	CvRouteInfo& getRouteInfo(RouteTypes eRouteNum);

	DllExport int getNumImprovementInfos();
	DllExport std::vector<CvImprovementInfo*>& getImprovementInfo();
	DllExport CvImprovementInfo& getImprovementInfo(ImprovementTypes eImprovementNum);

	DllExport int getNumGoodyInfos();
	DllExport std::vector<CvGoodyInfo*>& getGoodyInfo();
	DllExport CvGoodyInfo& getGoodyInfo(GoodyTypes eGoodyNum);

	DllExport int getNumBuildInfos();
	DllExport std::vector<CvBuildInfo*>& getBuildInfo();
	DllExport CvBuildInfo& getBuildInfo(BuildTypes eBuildNum);

	DllExport int getNumHandicapInfos();
	DllExport std::vector<CvHandicapInfo*>& getHandicapInfo();
	DllExport CvHandicapInfo& getHandicapInfo(HandicapTypes eHandicapNum);

	DllExport int getNumGameSpeedInfos();
	DllExport std::vector<CvGameSpeedInfo*>& getGameSpeedInfo();
	DllExport CvGameSpeedInfo& getGameSpeedInfo(GameSpeedTypes eGameSpeedNum);

	DllExport int getNumAlarmInfos();
	DllExport std::vector<CvAlarmInfo*>& getAlarmInfo();
	DllExport CvAlarmInfo& getAlarmInfo(AlarmTypes eAlarm);

	DllExport int getNumTurnTimerInfos();
	DllExport std::vector<CvTurnTimerInfo*>& getTurnTimerInfo();
	DllExport CvTurnTimerInfo& getTurnTimerInfo(TurnTimerTypes eTurnTimerNum);

	DllExport int getNumBuildingClassInfos();
	DllExport std::vector<CvBuildingClassInfo*>& getBuildingClassInfo();
	DllExport CvBuildingClassInfo& getBuildingClassInfo(BuildingClassTypes eBuildingClassNum);

	DllExport int getNumBuildingInfos();
	DllExport std::vector<CvBuildingInfo*>& getBuildingInfo();
	DllExport CvBuildingInfo& getBuildingInfo(BuildingTypes eBuildingNum);

	DllExport int getNumSpecialBuildingInfos();
	DllExport std::vector<CvSpecialBuildingInfo*>& getSpecialBuildingInfo();
	DllExport CvSpecialBuildingInfo& getSpecialBuildingInfo(SpecialBuildingTypes eSpecialBuildingNum);

	DllExport int getNumUnitClassInfos();
	DllExport std::vector<CvUnitClassInfo*>& getUnitClassInfo();
	DllExport CvUnitClassInfo& getUnitClassInfo(UnitClassTypes eUnitClassNum);

	DllExport int getNumActionInfos();
	DllExport std::vector<CvActionInfo*>& getActionInfo();
	DllExport CvActionInfo& getActionInfo(int i);

	DllExport std::vector<CvMissionInfo*>& getMissionInfo();
	DllExport CvMissionInfo& getMissionInfo(MissionTypes eMissionNum);

	DllExport std::vector<CvControlInfo*>& getControlInfo();
	DllExport CvControlInfo& getControlInfo(ControlTypes eControlNum);

	DllExport std::vector<CvCommandInfo*>& getCommandInfo();
	DllExport CvCommandInfo& getCommandInfo(CommandTypes eCommandNum);

	DllExport int getNumAutomateInfos();
	DllExport std::vector<CvAutomateInfo*>& getAutomateInfo();
	DllExport CvAutomateInfo& getAutomateInfo(int iAutomateNum);

	DllExport int getNumPromotionInfos();
	DllExport std::vector<CvPromotionInfo*>& getPromotionInfo();
	DllExport CvPromotionInfo& getPromotionInfo(PromotionTypes ePromotionNum);

	DllExport int getNumProfessionInfos();
	DllExport std::vector<CvProfessionInfo*>& getProfessionInfo();
	DllExport CvProfessionInfo& getProfessionInfo(ProfessionTypes eProfessionNum);

	DllExport int getNumCivicOptionInfos();
	DllExport std::vector<CvInfoBase*>& getCivicOptionInfo();
	DllExport CvInfoBase& getCivicOptionInfo(CivicOptionTypes eCivicOptionNum);

	DllExport int getNumCivicInfos();
	DllExport std::vector<CvCivicInfo*>& getCivicInfo();
	DllExport CvCivicInfo& getCivicInfo(CivicTypes eCivicNum);

	DllExport int getNumDiplomacyInfos();
	DllExport std::vector<CvDiplomacyInfo*>& getDiplomacyInfo();
	DllExport	CvDiplomacyInfo& getDiplomacyInfo(int iDiplomacyNum);

	DllExport int getNumEraInfos();
	DllExport std::vector<CvEraInfo*>& getEraInfo();
	DllExport	CvEraInfo& getEraInfo(EraTypes eEraNum);

	DllExport int getNumNewEraInfos();
	DllExport std::vector<CvNewEraInfo*>& getNewEraInfo();
	DllExport CvNewEraInfo& getNewEraInfo(NewEraTypes eNewEraNum);
	
	DllExport int getNumEconomicTrendInfos();
	DllExport std::vector<CvEconomicTrendInfo*>& getEconomicTrendInfo();
	DllExport CvEconomicTrendInfo& getEconomicTrendInfo(EconomicTrendTypes eEconomicTrendType);
	
	DllExport int getNumPirateInfos();
	DllExport std::vector<CvPirateInfo*>& getPirateInfo();
	DllExport CvPirateInfo& getPirateInfo(PirateInfoTypes index);
	
	DllExport int getNumWarPlanInfos();
	DllExport std::vector<CvWarPlanInfo*>& getWarPlanInfo();
	DllExport CvWarPlanInfo& getWarPlanInfo(WarPlanInfoTypes index);

	DllExport int getNumWarPlanStrategyInfos();
	DllExport std::vector<CvWarPlanStrategyInfo*>& getWarPlanStrategyInfo();

	DllExport int getNumArtilleryPenaltyInfos();
	DllExport std::vector<CvArtilleryPenaltyInfo*>& getArtilleryPenaltyInfo();
	DllExport CvArtilleryPenaltyInfo& getArtilleryPenaltyInfo(ArtilleryPenaltyTypes eArtilleryPenaltyNum);

	DllExport int getNumHurryInfos();
	DllExport std::vector<CvHurryInfo*>& getHurryInfo();
	DllExport	CvHurryInfo& getHurryInfo(HurryTypes eHurryNum);

	DllExport int getNumEmphasizeInfos();
	DllExport std::vector<CvEmphasizeInfo*>& getEmphasizeInfo();
	DllExport	CvEmphasizeInfo& getEmphasizeInfo(EmphasizeTypes eEmphasizeNum);

	DllExport int getNumCultureLevelInfos();
	DllExport std::vector<CvCultureLevelInfo*>& getCultureLevelInfo();
	DllExport	CvCultureLevelInfo& getCultureLevelInfo(CultureLevelTypes eCultureLevelNum);

	DllExport int getNumVictoryInfos();
	DllExport std::vector<CvVictoryInfo*>& getVictoryInfo();
	DllExport	CvVictoryInfo& getVictoryInfo(VictoryTypes eVictoryNum);

	DllExport int getNumEventTriggerInfos();
	DllExport std::vector<CvEventTriggerInfo*>& getEventTriggerInfo();
	DllExport CvEventTriggerInfo& getEventTriggerInfo(EventTriggerTypes eEventTrigger);

	DllExport int getNumEventInfos();
	DllExport std::vector<CvEventInfo*>& getEventInfo();
	DllExport CvEventInfo& getEventInfo(EventTypes eEvent);
	
	std::vector<CvContactInfo*>& getContactInfo();
	DllExport CvContactInfo& getContactInfo(ContactTypes eContactType);

	std::vector<CvAttitudeInfo*>& getAttitudeInfo();
	DllExport CvAttitudeInfo& getAttitudeInfo(AttitudeTypes eAttitudeType);

	std::vector<CvMemoryInfo*>& getMemoryInfo();
	DllExport CvMemoryInfo& getMemoryInfo(MemoryTypes eMemoryType);
	
	DllExport int getNumUnitArtStyleTypeInfos();
	std::vector<CvUnitArtStyleTypeInfo*>& getUnitArtStyleTypeInfo();
	DllExport CvUnitArtStyleTypeInfo& getUnitArtStyleTypeInfo(UnitArtStyleTypes eUnitArtStyleTypeNum);
	

	DllExport int getNUM_ENGINE_DIRTY_BITS() const;
	DllExport int getNUM_INTERFACE_DIRTY_BITS() const;
	DllExport int getNUM_YIELD_TYPES() const;
	DllExport int getNUM_FORCECONTROL_TYPES() const;
	DllExport int getNUM_INFOBAR_TYPES() const;
	DllExport int getNUM_HEALTHBAR_TYPES() const;
	DllExport int getNUM_CONTROL_TYPES() const;
	DllExport int getNUM_LEADERANIM_TYPES() const;

	DllExport int& getNumEntityEventTypes();
	CvString*& getEntityEventTypes();
	DllExport CvString& getEntityEventTypes(EntityEventTypes e);

	DllExport int& getNumAnimationOperatorTypes();
	CvString*& getAnimationOperatorTypes();
	DllExport CvString& getAnimationOperatorTypes(AnimationOperatorTypes e);

	CvString*& getFunctionTypes();
	DllExport CvString& getFunctionTypes(FunctionTypes e);

	DllExport int& getNumArtStyleTypes();
	CvString*& getArtStyleTypes();
	DllExport CvString& getArtStyleTypes(ArtStyleTypes e);

	DllExport int& getNumCitySizeTypes();
	CvString*& getCitySizeTypes();
	DllExport CvString& getCitySizeTypes(int i);

	CvString*& getContactTypes();
	DllExport CvString& getContactTypes(ContactTypes e);

	CvString*& getDiplomacyPowerTypes();
	DllExport CvString& getDiplomacyPowerTypes(DiplomacyPowerTypes e);

	CvString*& getAutomateTypes();
	DllExport CvString& getAutomateTypes(AutomateTypes e);

	CvString*& getDirectionTypes();
	DllExport CvString& getDirectionTypes(AutomateTypes e);

	DllExport int& getNumFootstepAudioTypes();
	CvString*& getFootstepAudioTypes();
	DllExport CvString& getFootstepAudioTypes(int i);
	DllExport int getFootstepAudioTypeByTag(CvString strTag);

	CvString*& getFootstepAudioTags();
	DllExport CvString& getFootstepAudioTags(int i);

	CvString& getCurrentXMLFile();
	void setCurrentXMLFile(const TCHAR* szFileName);

	//
	///////////////// BEGIN global defines
	// THESE ARE READ-ONLY
	//

	DllExport FVariableSystem* getDefinesVarSystem();
	DllExport void cacheGlobals();

	// ***** EXPOSED TO PYTHON *****
	DllExport int getDefineINT( const char * szName ) const;
	DllExport float getDefineFLOAT( const char * szName ) const;
	DllExport const char * getDefineSTRING( const char * szName ) const;
	DllExport void setDefineINT( const char * szName, int iValue );
	DllExport void setDefineFLOAT( const char * szName, float fValue );
	DllExport void setDefineSTRING( const char * szName, const char * szValue );
	// ***** END EXPOSED TO PYTHON *****

	///////////////////////////////////////////////////////////////////////////
	////////*/	//DOANE XML Loading | Var System | Speed improvement	*//////
	///////////////////////////////////////////////////////////////////////////	
	/*NOTE: The exe doesn't use them, no need to export...*/
	//DOANE Start Cache
	void cacheAtStart();
	void uninitCacheAtStart();
	//DOANE Delayed Cache
	void delayedCacheAtStart();
	void uninitDelayedCacheAtStart();
	void resetDelayedCacheAtStart(bool bConstructorCall = false);
	//Is calling delayedCacheAtStart() allowed?
	inline bool getDelayedCacheInitialized() const { return m_bDelayedCacheInitialized; }
	//Allow or disallow future calls to delayedCacheAtStart()
	inline void setDelayedCacheInitialized(bool bValue)  { m_bDelayedCacheInitialized = bValue; }
	//Will CacheGlobals remove variables from FVariableSystem?
	inline bool getCacheClean() const { return m_bCacheGlobalsClean; }
	//Allow or disallow future calls to CacheGlobals() removing variables from FVariableSystem
	inline void setCacheClean(bool bValue)  { m_bCacheGlobalsClean = bValue; }
	// ***** EXPOSED TO PYTHON *****
	//Force CacheGlobals to load variables from FVariablSystem
	void forceCacheGlobals();
	//DOANE Global Define Booleans
	bool getDefineBOOL( const char * szName ) const;
	bool getDefineBOOL( const char * szName, bool* bReturn, bool bRemove = true );
	void setDefineBOOL( const char * szName, bool bValue, bool bForceCache = false );
	//DOANE Global Define Signed Char
	signed char getDefineSCHAR( const char * szName ) const;
	signed char getDefineSCHAR( const char * szName,  signed char* scReturn, bool bRemove = true );
	void setDefineSCHAR( const char * szName, signed char scValue, bool bForceCache = false );
	//DOANE Global Define Unsigned Char
	unsigned char getDefineUCHAR( const char * szName ) const;
	unsigned char getDefineUCHAR( const char * szName,  unsigned char* ucReturn, bool bRemove = true );
	void setDefineUCHAR( const char * szName, unsigned char scValue, bool bForceCache = false );
	//DOANE Global Define Short
	short getDefineSSHORT( const char * szName ) const;
	short getDefineSSHORT( const char * szName, short* wReturn, bool bRemove = true );
	void setDefineSSHORT( const char * szName, short wValue, bool bForceCache = false );
	//DOANE Global Define Unsigned Short
	unsigned short getDefineUSHORT( const char * szName ) const;
	unsigned short getDefineUSHORT( const char * szName,  unsigned short* uwReturn, bool bRemove = true );
	void setDefineUSHORT( const char * szName, unsigned short uwValue, bool bForceCache = false );
	//DOANE Global Define Signed Int
	int getDefineSINT( const char * szName, int* iReturn, bool bRemove = true );
	void setDefineSINT( const char * szName, int iValue, bool bForceCache = false );
	//DOANE Global Define Unsigned Int
	uint getDefineUINT( const char * szName ) const;
	uint getDefineUINT( const char * szName, uint* uiReturn, bool bRemove = true );
	void setDefineUINT( const char * szName, uint uiValue, bool bForceCache = false );
	// ***** END EXPOSED TO PYTHON *****

	//Helpers: calls to same functions with other names...
	//Signed short
	#define getDefineSWORD getDefineSSHORT
	#define setDefineSWORD getDefineSSHORT
	//Unsigned short
	#define getDefineUWORD getDefineUSHORT
	#define setDefineUWORD getDefineUSHORT
	///////////////////////////////////////////////////////////////////////////
	////////*/						//END DOANE							*//////
	///////////////////////////////////////////////////////////////////////////

	// ***** EXPOSED TO PYTHON *****
	DllExport int getMOVE_DENOMINATOR();
	DllExport int getFOOD_CONSUMPTION_PER_POPULATION();
	DllExport int getMAX_HIT_POINTS();
	DllExport int getHILLS_EXTRA_DEFENSE();
	DllExport int getRIVER_ATTACK_MODIFIER();
	DllExport int getAMPHIB_ATTACK_MODIFIER();
	DllExport int getHILLS_EXTRA_MOVEMENT();
	DllExport int getPEAK_EXTRA_MOVEMENT();
	DllExport int getMAX_PLOT_LIST_ROWS();
	DllExport int getUNIT_MULTISELECT_MAX();
	DllExport int getEVENT_MESSAGE_TIME();
	DllExport int getROUTE_FEATURE_GROWTH_MODIFIER();
	DllExport int getFEATURE_GROWTH_MODIFIER();
	DllExport int getMIN_CITY_RANGE();
	DllExport int getCITY_MAX_NUM_BUILDINGS();
	DllExport int getLAKE_MAX_AREA_SIZE();
	DllExport int getMIN_WATER_SIZE_FOR_OCEAN();
	DllExport int getFORTIFY_MODIFIER_PER_TURN();
	DllExport int getMAX_CITY_DEFENSE_DAMAGE();
	DllExport int getPEAK_SEE_THROUGH_CHANGE();
	DllExport int getHILLS_SEE_THROUGH_CHANGE();
	DllExport int getSEAWATER_SEE_FROM_CHANGE();
	DllExport int getPEAK_SEE_FROM_CHANGE();
	DllExport int getHILLS_SEE_FROM_CHANGE();
	DllExport int getMAX_REBEL_YIELD_MODIFIER();

	DllExport float getCAMERA_MIN_YAW();
	DllExport float getCAMERA_MAX_YAW();
	DllExport float getCAMERA_FAR_CLIP_Z_HEIGHT();
	DllExport float getCAMERA_MAX_TRAVEL_DISTANCE();
	DllExport float getCAMERA_START_DISTANCE();
	DllExport float getPLOT_SIZE();
	DllExport float getCAMERA_SPECIAL_PITCH();
	DllExport float getCAMERA_MAX_TURN_OFFSET();
	DllExport float getCAMERA_MIN_DISTANCE();
	DllExport float getCAMERA_UPPER_PITCH();
	DllExport float getCAMERA_LOWER_PITCH();
	DllExport float getFIELD_OF_VIEW();
	DllExport float getUNIT_MULTISELECT_DISTANCE();
	// ***** END EXPOSED TO PYTHON *****

	///////////////////////////////////////////////////////////////
	////////*		//DOANE Cache | Speed Improvement		*//////
	///////////////////////////////////////////////////////////////
	// ***** EXPOSED TO PYTHON *****
	//DOANE Enrol Unit
	unsigned char getMAX_ENROL_UNIT();
	//DOANE Enrol Unit
	unsigned char getMAX_MILITARY_PROFESSION_TURN();
	unsigned char getSTART_MILITARY_PROFESSION_PENALTY();
	//DOANE Fortification 
	unsigned char getFORTIFICATION_BREAK_DAMAGE();
	unsigned char getPOWERFUL_FORTIFICATION_PENALTY();
	//DOANE AI Help
	int getGOLD_BY_POPULATION_BY_TURN();
	int getNUM_DISCOVERED_MAP_TILES_BEFORE_DELIVERING();
	//DOANE Economy
	int getMAX_MANUFACTURED_YIELD_AMOUNT();
	//DOANE Builder Pack
	unsigned char getBUILDER_PACK_YIELD_COST_PERCENT();
	unsigned char getBuilderPackToolCost();
	unsigned char getBuilderPackLumberCost();
	//DOANE Survivors on Transport Sinking
	unsigned char getSURVIVORS_ON_TRANSPORT_SINKING_PROBABILITY();
	unsigned char getNUM_TRADE_ROUTE_HIST_TURN() const;
	unsigned char getAVANCED_NEW_ERA_BOOST_FOR_CONSTRUCTION() const;
	//DoaNE Immigration
	unsigned char getMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_AVAILABLE() const;
	unsigned char getMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_DOCK() const;
	unsigned char getMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_CARGO() const;
	unsigned char getPROBABILITY_NATURAL_IMMIGRATION_START() const;
	unsigned char getPROBABILITY_NATURAL_IMMIGRATION_LATE() const;
	unsigned char getPROBABILITY_NATURAL_IMMIGRATION_PERCENT() const;
	int getTURN_NATURAL_IMMIGRATION_CHANGE() const;
	//DoaNE Disturbation
	unsigned char getNAVAL_TRANSPORT_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY() const;
	unsigned char getNAVAL_MILITARY_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY() const;
	unsigned char getNAVAL_DISTURBATION_FOR_TRADE_ROUTE() const;
	unsigned char getNAVAL_DISTURBATION_FOR_MILITARY_SHIP_KILLED_PROTECTING_A_CITY() const;
	
	//Income and Expenditures
	unsigned char getEXPENDITURE_BY_LAND_FORCE_INSIDE_COLONIES() const;
	unsigned char getEXPENDITURE_BY_LAND_FORCE_OUTSIDE_COLONIES() const;
	unsigned char getEXPENDITURE_BY_UNEMPLOYED() const;
	unsigned char getEXPENDITURE_BY_SCOUT() const;
	unsigned char getEXPENDITURE_BY_AGRONOMIST() const;
	unsigned char getEXPENDITURE_BY_PIONEER() const;
	unsigned char getEXPENDITURE_BY_MERCHANT_SHIP() const;
	unsigned char getEXPENDITURE_BY_MILITARY_SHIP() const;

	// ***** END EXPOSED TO PYTHON *****
	///////////////////////////////////////////////////////////////
	////////*	DOANE Python Callbacks | Speed Improvement	*//////
	///////////////////////////////////////////////////////////////
	//Vanilla Callbacks
	inline bool getUSE_CAN_FOUND_CITIES_ON_WATER_CALLBACK() { return m_bUSE_CAN_FOUND_CITIES_ON_WATER_CALLBACK; }
	inline bool getUSE_CANNOT_DO_CIVIC_CALLBACK() { return m_bUSE_CANNOT_DO_CIVIC_CALLBACK; }
	inline bool getUSE_CAN_DO_CIVIC_CALLBACK() { return m_bUSE_CAN_DO_CIVIC_CALLBACK; }
	inline bool getUSE_CANNOT_CONSTRUCT_CALLBACK() { return m_bUSE_CANNOT_CONSTRUCT_CALLBACK; }
	inline bool getUSE_CAN_CONSTRUCT_CALLBACK() { return m_bUSE_CAN_CONSTRUCT_CALLBACK; }
	inline bool getUSE_CAN_DECLARE_WAR_CALLBACK() { return m_bUSE_CAN_DECLARE_WAR_CALLBACK; }
	inline bool getUSE_GET_UNIT_COST_MOD_CALLBACK() { return m_bUSE_GET_UNIT_COST_MOD_CALLBACK; }
	inline bool getUSE_GET_BUILDING_COST_MOD_CALLBACK() { return m_bUSE_GET_BUILDING_COST_MOD_CALLBACK; }
	inline bool getUSE_GET_CITY_FOUND_VALUE_CALLBACK() { return m_bUSE_GET_CITY_FOUND_VALUE_CALLBACK; }
	inline bool getUSE_CANNOT_HANDLE_ACTION_CALLBACK() { return m_bUSE_CANNOT_HANDLE_ACTION_CALLBACK; }
	inline bool getUSE_CAN_BUILD_CALLBACK() { return m_bUSE_CAN_BUILD_CALLBACK; }
	inline bool getUSE_CANNOT_TRAIN_CALLBACK() { return m_bUSE_CANNOT_TRAIN_CALLBACK; }
	inline bool getUSE_CAN_TRAIN_CALLBACK() { return m_bUSE_CAN_TRAIN_CALLBACK; }
	inline bool getUSE_UNIT_CANNOT_MOVE_INTO_CALLBACK() { return m_bUSE_UNIT_CANNOT_MOVE_INTO_CALLBACK; }
	//Harcoded callbacks: Warning: this is used by the exe!
	DllExport int getUSE_FINISH_TEXT_CALLBACK(); //Warning: this is used by the exe!
	DllExport int getUSE_ON_UNIT_SET_XY_CALLBACK(); //Warning: this is used by the exe!
	DllExport int getUSE_ON_UNIT_SELECTED_CALLBACK(); //Warning: this is used by the exe!
	DllExport int getUSE_ON_MISSIONARY_CONVERTED_UNIT_CALLBACK(); //Warning: this is used by the exe!
	DllExport int getUSE_ON_UPDATE_CALLBACK(); //Warning: this is used by the exe!
	DllExport int getUSE_ON_UNIT_CREATED_CALLBACK(); //Warning: this is used by the exe!
	DllExport int getUSE_ON_UNIT_LOST_CALLBACK(); //Warning: this is used by the exe!
	//CvCity
	inline bool getUSE_ON_CITY_DO_GROWTH_CALLBACK() { return m_bUSE_ON_CITY_DO_GROWTH_CALLBACK; }
	inline bool getUSE_ON_CITIES_DESTROY_FEATURES_CALLBACK() { return m_bUSE_ON_CITIES_DESTROY_FEATURES_CALLBACK; }
	inline bool getUSE_ON_DO_CULTURE_CALLBACK() { return m_bUSE_ON_DO_CULTURE_CALLBACK; }
	inline bool getUSE_ON_DO_PLOT_CULTURE_CALLBACK() { return m_bUSE_ON_DO_PLOT_CULTURE_CALLBACK; }
	inline bool getUSE_ON_DO_PRODUCTION_CALLBACK() { return m_bUSE_ON_DO_PRODUCTION_CALLBACK; }
	//CvCityAI
	inline bool getUSE_AI_CHOOSE_PRODUCTION_CALLBACK() { return m_bUSE_AI_CHOOSE_PRODUCTION_CALLBACK; }
	//CvGame
	inline bool getUSE_ON_UPDATE_COLORED_PLOTS_CALLBACK() { return m_bUSE_ON_UPDATE_COLORED_PLOTS_CALLBACK; }
	inline bool getUSE_CAN_NOT_SELECTION_LIST_MOVE_CALLBACK() { return m_bUSE_CAN_NOT_SELECTION_LIST_MOVE_CALLBACK; }
	inline bool getUSE_CAN_NOT_SELECTION_LIST_GAME_NET_MESSAGE_CALLBACK() { return m_bUSE_CAN_NOT_SELECTION_LIST_GAME_NET_MESSAGE_CALLBACK; }
	inline bool getUSE_CAN_NOT_DO_CONTROL_CALLBACK() { return m_bUSE_CAN_NOT_DO_CONTROL_CALLBACK; }
	inline bool getUSE_ON_DO_REVIVE_ACTIVE_PLAYER_CALLBACK() { return m_bUSE_ON_DO_REVIVE_ACTIVE_PLAYER_CALLBACK; }
	inline bool getUSE_ON_CHECK_IS_VICTORY_CALLBACK() { return m_bUSE_ON_CHECK_IS_VICTORY_CALLBACK; }
	inline bool getUSE_ON_CHECK_IS_VICTORY_TEST_CALLBACK() { return m_bUSE_ON_CHECK_IS_VICTORY_TEST_CALLBACK; }
	//CvDLLButtonPopup
	inline bool getUSE_ON_SKIP_PRODUCTION_POPUP_CALLBACK() { return m_bUSE_ON_SKIP_PRODUCTION_POPUP_CALLBACK; }
	inline bool getUSE_ON_SHOW_EXAMINE_CITY_BUTTON_CALLBACK() { return m_bUSE_ON_SHOW_EXAMINE_CITY_BUTTON_CALLBACK; }
	inline bool getUSE_GET_RECOMMENDED_UNIT_CALLBACK() { return m_bUSE_GET_RECOMMENDED_UNIT_CALLBACK; }
	inline bool getUSE_GET_RECOMMENDED_BUILDING_CALLBACK() { return m_bUSE_GET_RECOMMENDED_BUILDING_CALLBACK; }
	//CvPlayer
	inline bool getUSE_ON_DO_CITY_CAPTURE_GOLD_CALLBACK() { return m_bUSE_ON_DO_CITY_CAPTURE_GOLD_CALLBACK; }
	inline bool getUSE_CAN_RAZE_CITY_CALLBACK() { return m_bUSE_CAN_RAZE_CITY_CALLBACK; }
	inline bool getUSE_ON_DO_GOLD_CALLBACK() { return m_bUSE_ON_DO_GOLD_CALLBACK; }

	//CvPlayerAI
	inline bool getUSE_AI_DO_DIPLO_CALLBACK() { return m_bUSE_AI_DO_DIPLO_CALLBACK; }
	//CvSelectionGroup
	inline bool getUSE_ON_DO_COMBAT_CALLBACK() { return m_bUSE_ON_DO_COMBAT_CALLBACK; }
	//CvTeamAI
	inline bool getUSE_AI_DO_WAR_CALLBACK() { return m_bUSE_AI_DO_WAR_CALLBACK; }
	//CvUnit
	inline bool getUSE_ON_IS_ACTION_RECOMMENDED_CALLBACK() { return m_bUSE_ON_IS_ACTION_RECOMMENDED_CALLBACK; }
	inline bool getUSE_ON_DO_PILLAGE_GOLD_CALLBACK() { return m_bUSE_ON_DO_PILLAGE_GOLD_CALLBACK; }
	inline bool getUSE_GET_UPGRADE_PRICE_OVERRIDE_CALLBACK() { return m_bUSE_GET_UPGRADE_PRICE_OVERRIDE_CALLBACK; }
	inline bool getUSE_GET_EXPERIENCE_NEEDED_CALLBACK() { return m_bUSE_GET_EXPERIENCE_NEEDED_CALLBACK; }
	//CvUnitAI
	inline bool getUSE_AI_UNIT_UPDATE_CALLBACK() { return m_bUSE_AI_UNIT_UPDATE_CALLBACK; }

	//////////////////////////////////////////////////////////////
	////////*					END DOANE					*//////	
	///////////////////////////////////////////////////////////////	

	// ***** EXPOSED TO PYTHON *****
	DllExport int getMAX_CIV_PLAYERS();
	DllExport int getMAX_PLAYERS();
	DllExport int getMAX_CIV_TEAMS();
	DllExport int getMAX_TEAMS();
	DllExport int getINVALID_PLOT_COORD();
	DllExport int getNUM_CITY_PLOTS();
	DllExport int getCITY_HOME_PLOT();

	// ***** END EXPOSED TO PYTHON *****

	////////////// END DEFINES //////////////////

	DllExport void setDLLIFace(CvDLLUtilityIFaceBase* pDll);
#ifdef _USRDLL
	CvDLLUtilityIFaceBase* getDLLIFace() { return m_pDLL; }		// inlined for perf reasons, do not use outside of dll
#endif
	DllExport CvDLLUtilityIFaceBase* getDLLIFaceNonInl();

	DllExport bool IsGraphicsInitialized() const;
	DllExport void SetGraphicsInitialized(bool bVal);

	//
	// additional accessors for initting globals
	//

	DllExport void setInterface(CvInterface* pVal);
	DllExport void setDiplomacyScreen(CvDiplomacyScreen* pVal);
	DllExport void setMPDiplomacyScreen(CMPDiplomacyScreen* pVal);
	DllExport void setMessageQueue(CMessageQueue* pVal);
	DllExport void setHotJoinMessageQueue(CMessageQueue* pVal);
	DllExport void setMessageControl(CMessageControl* pVal);
	DllExport void setSetupData(CvSetupData* pVal);
	DllExport void setMessageCodeTranslator(CvMessageCodeTranslator* pVal);
	DllExport void setDropMgr(CvDropMgr* pVal);
	DllExport void setPortal(CvPortal* pVal);
	DllExport void setStatsReport(CvStatsReporter* pVal);
	DllExport void setPathFinder(FAStar* pVal);
	DllExport void setInterfacePathFinder(FAStar* pVal);
	DllExport void setStepFinder(FAStar* pVal);
	DllExport void setRouteFinder(FAStar* pVal);
	DllExport void setBorderFinder(FAStar* pVal);
	DllExport void setAreaFinder(FAStar* pVal);

	// So that CvEnums are moddable in the DLL
	DllExport int getNumDirections() const;
	DllExport int getNumResourceLayers() const;
	DllExport int getNumUnitLayerOptionTypes() const;
	DllExport int getNumGameOptions() const;
	DllExport int getNumMPOptions() const;
	DllExport int getNumSpecialOptions() const;
	DllExport int getNumGraphicOptions() const;
	DllExport int getNumTradeableItems() const;
	DllExport int getNumBasicItems() const;
	DllExport int getNumTradeableHeadings() const;
	DllExport int getNumCommandInfos() const;
	DllExport int getNumControlInfos() const;
	DllExport int getNumMissionInfos() const;
	DllExport int getNumPlayerOptionInfos() const;
	DllExport int getMaxNumSymbols() const;
	DllExport int getNumGraphicLevels() const;

	void deleteInfoArrays();

protected:

	bool m_bGraphicsInitialized;
	bool m_bLogging;
	bool m_bRandLogging;
	bool m_bSynchLogging;
	bool m_bOverwriteLogs;
	NiPoint3  m_pt3CameraDir;
	int m_iNewPlayers;

	CMainMenu* m_pkMainMenu;

	bool m_bZoomOut;
	bool m_bZoomIn;
	bool m_bLoadGameFromFile;

	FMPIManager * m_pFMPMgr;

	CvRandom* m_asyncRand;

	CvGameAI* m_game;

	CMessageQueue* m_messageQueue;
	CMessageQueue* m_hotJoinMsgQueue;
	CMessageControl* m_messageControl;
	CvSetupData* m_setupData;
	CvInitCore* m_iniInitCore;
	CvInitCore* m_loadedInitCore;
	CvInitCore* m_initCore;
	CvMessageCodeTranslator * m_messageCodes;
	CvDropMgr* m_dropMgr;
	CvPortal* m_portal;
	CvStatsReporter * m_statsReporter;
	CvInterface* m_interface;

	CvArtFileMgr* m_pArtFileMgr;

	CvMap* m_map;

	CvDiplomacyScreen* m_diplomacyScreen;
	CMPDiplomacyScreen* m_mpDiplomacyScreen;

	FAStar* m_pathFinder;
	FAStar* m_interfacePathFinder;
	FAStar* m_stepFinder;
	FAStar* m_routeFinder;
	FAStar* m_borderFinder;
	FAStar* m_areaFinder;

	NiPoint3 m_pt3Origin;

	//DOANE cache
	unsigned char* m_aiBuilderPackYieldRequire; //[NUM_YIELD_TYPES];
	//END DOANE
	int* m_aiPlotDirectionX;	// [NUM_DIRECTION_TYPES];
	int* m_aiPlotDirectionY;	// [NUM_DIRECTION_TYPES];
	int* m_aiPlotCardinalDirectionX;	// [NUM_CARDINALDIRECTION_TYPES];
	int* m_aiPlotCardinalDirectionY;	// [NUM_CARDINALDIRECTION_TYPES];
	int* m_aiCityPlotX;	// [NUM_CITY_PLOTS];
	int* m_aiCityPlotY;	// [NUM_CITY_PLOTS];
	int* m_aiCityPlotPriority;	// [NUM_CITY_PLOTS];
	int m_aaiXYCityPlot[CITY_PLOTS_DIAMETER][CITY_PLOTS_DIAMETER];

	DirectionTypes* m_aeTurnLeftDirection;	// [NUM_DIRECTION_TYPES];
	DirectionTypes* m_aeTurnRightDirection;	// [NUM_DIRECTION_TYPES];
	DirectionTypes m_aaeXYDirection[DIRECTION_DIAMETER][DIRECTION_DIAMETER];

	//InterfaceModeInfo m_aInterfaceModeInfo[NUM_INTERFACEMODE_TYPES] =
	std::vector<CvInterfaceModeInfo*> m_paInterfaceModeInfo;

	/***********************************************************************************************************************
	Globals loaded from XML
	************************************************************************************************************************/

	// all type strings are upper case and are kept in this hash map for fast lookup, Moose
	typedef stdext::hash_map<std::string /* type string */, int /* info index */> InfosMap;
	InfosMap m_infosMap;
	std::vector<std::vector<CvInfoBase *> *> m_aInfoVectors;

	std::vector<CvColorInfo*> m_paColorInfo;
	std::vector<CvPlayerColorInfo*> m_paPlayerColorInfo;
	std::vector<CvInfoBase*> m_paHints;
	std::vector<CvMainMenuInfo*> m_paMainMenus;
	std::vector<CvTerrainInfo*> m_paTerrainInfo;
	std::vector<CvLandscapeInfo*> m_paLandscapeInfo;
	int m_iActiveLandscapeID;
	std::vector<CvWorldInfo*> m_paWorldInfo;
	std::vector<CvClimateInfo*> m_paClimateInfo;
	std::vector<CvSeaLevelInfo*> m_paSeaLevelInfo;
	std::vector<CvEuropeInfo*> m_paEuropeInfo;
	std::vector<CvYieldInfo*> m_paYieldInfo;
	std::vector<CvRouteInfo*> m_paRouteInfo;
	std::vector<CvFeatureInfo*> m_paFeatureInfo;
	std::vector<CvBonusInfo*> m_paBonusInfo;
	std::vector<CvImprovementInfo*> m_paImprovementInfo;
	std::vector<CvGoodyInfo*> m_paGoodyInfo;
	std::vector<CvBuildInfo*> m_paBuildInfo;
	std::vector<CvHandicapInfo*> m_paHandicapInfo;
	std::vector<CvGameSpeedInfo*> m_paGameSpeedInfo;
	std::vector<CvAlarmInfo*> m_paAlarmInfo;
	std::vector<CvTurnTimerInfo*> m_paTurnTimerInfo;
	std::vector<CvCivilizationInfo*> m_paCivilizationInfo;
	std::vector<CvLeaderHeadInfo*> m_paLeaderHeadInfo;
	std::vector<CvTraitInfo*> m_paTraitInfo;
	std::vector<CvCursorInfo*> m_paCursorInfo;
	std::vector<CvSlideShowInfo*> m_paSlideShowInfo;
	std::vector<CvSlideShowRandomInfo*> m_paSlideShowRandomInfo;
	std::vector<CvWorldPickerInfo*> m_paWorldPickerInfo;
	std::vector<CvBuildingClassInfo*> m_paBuildingClassInfo;
	std::vector<CvBuildingInfo*> m_paBuildingInfo;
	std::vector<CvSpecialBuildingInfo*> m_paSpecialBuildingInfo;
	std::vector<CvUnitClassInfo*> m_paUnitClassInfo;
	std::vector<CvUnitInfo*> m_paUnitInfo;
	std::vector<CvSpecialUnitInfo*> m_paSpecialUnitInfo;
	std::vector<CvInfoBase*> m_paConceptInfo;
	std::vector<CvInfoBase*> m_paCalendarInfo;
	std::vector<CvInfoBase*> m_paSeasonInfo;
	std::vector<CvInfoBase*> m_paMonthInfo;
	std::vector<CvInfoBase*> m_paDenialInfo;
	std::vector<CvInfoBase*> m_paInvisibleInfo;
	std::vector<CvFatherInfo*> m_paFatherInfo;
	std::vector<CvFatherPointInfo*> m_paFatherPointInfo;
	std::vector<CvInfoBase*> m_paUnitCombatInfo;
	std::vector<CvInfoBase*> m_paDomainInfo;
	std::vector<CvInfoBase*> m_paUnitAIInfos;
	std::vector<CvInfoBase*> m_paAttitudeInfos;
	std::vector<CvInfoBase*> m_paFatherCategoryInfos;
	std::vector<CvInfoBase*> m_paFeatInfos;
	std::vector<CvGameOptionInfo*> m_paGameOptionInfos;
	std::vector<CvMPOptionInfo*> m_paMPOptionInfos;
	std::vector<CvForceControlInfo*> m_paForceControlInfos;
	std::vector<CvPlayerOptionInfo*> m_paPlayerOptionInfos;
	std::vector<CvGraphicOptionInfo*> m_paGraphicOptionInfos;
	std::vector<CvEmphasizeInfo*> m_paEmphasizeInfo;
	std::vector<CvCultureLevelInfo*> m_paCultureLevelInfo;
	std::vector<CvActionInfo*> m_paActionInfo;
	std::vector<CvMissionInfo*> m_paMissionInfo;
	std::vector<CvControlInfo*> m_paControlInfo;
	std::vector<CvCommandInfo*> m_paCommandInfo;
	std::vector<CvAutomateInfo*> m_paAutomateInfo;
	std::vector<CvPromotionInfo*> m_paPromotionInfo;
	std::vector<CvProfessionInfo*> m_paProfessionInfo;
	std::vector<CvInfoBase*> m_paCivicOptionInfo;
	std::vector<CvCivicInfo*> m_paCivicInfo;
	std::vector<CvDiplomacyInfo*> m_paDiplomacyInfo;
	std::vector<CvEraInfo*> m_aEraInfo;	// [NUM_ERA_TYPES];
	std::vector<CvNewEraInfo*> m_aNewEraInfo;
	std::vector<CvEconomicTrendInfo*> m_aEconomicTrendInfo;
	std::vector<CvPirateInfo*> m_aPirateInfo;
	std::vector<CvWarPlanInfo*> m_aWarPlanInfo;
	std::vector<CvWarPlanStrategyInfo*> m_aWarPlanStrategyInfo;
	std::vector<CvArtilleryPenaltyInfo*> m_aArtilleryPenaltyInfo;
	std::vector<CvHurryInfo*> m_paHurryInfo;
	std::vector<CvVictoryInfo*> m_paVictoryInfo;
	std::vector<CvRouteModelInfo*> m_paRouteModelInfo;
	std::vector<CvRiverModelInfo*> m_paRiverModelInfo;
	std::vector<CvWaterPlaneInfo*> m_paWaterPlaneInfo;
	std::vector<CvTerrainPlaneInfo*> m_paTerrainPlaneInfo;
	std::vector<CvCameraOverlayInfo*> m_paCameraOverlayInfo;
	std::vector<CvAnimationPathInfo*> m_paAnimationPathInfo;
	std::vector<CvAnimationCategoryInfo*> m_paAnimationCategoryInfo;
	std::vector<CvEntityEventInfo*> m_paEntityEventInfo;
	std::vector<CvUnitFormationInfo*> m_paUnitFormationInfo;
	std::vector<CvEffectInfo*> m_paEffectInfo;
	std::vector<CvAttachableInfo*> m_paAttachableInfo;
	std::vector<CvEventTriggerInfo*> m_paEventTriggerInfo;
	std::vector<CvEventInfo*> m_paEventInfo;
	std::vector<CvContactInfo*> m_paContactInfo;
	std::vector<CvAttitudeInfo*> m_paAttitudeInfo;
	std::vector<CvMemoryInfo*> m_paMemoryInfo;
	std::vector<CvUnitArtStyleTypeInfo*> m_paUnitArtStyleTypeInfo;

	// Game Text
	std::vector<CvGameText*> m_paGameTextXML;

	//////////////////////////////////////////////////////////////////////////
	// GLOBAL TYPES
	//////////////////////////////////////////////////////////////////////////

	// XXX These are duplicates and are kept for enumeration convenience - most could be removed, Moose
	CvString *m_paszEntityEventTypes2;
	CvString *m_paszEntityEventTypes;
	int m_iNumEntityEventTypes;

	CvString *m_paszAnimationOperatorTypes;
	int m_iNumAnimationOperatorTypes;

	CvString* m_paszFunctionTypes;
	
	CvString *m_paszArtStyleTypes;
	int m_iNumArtStyleTypes;

	CvString *m_paszCitySizeTypes;
	int m_iNumCitySizeTypes;

	CvString *m_paszContactTypes;

	CvString *m_paszDiplomacyPowerTypes;

	CvString *m_paszAutomateTypes;

	CvString *m_paszDirectionTypes;

	CvString *m_paszFootstepAudioTypes;
	int m_iNumFootstepAudioTypes;

	CvString *m_paszFootstepAudioTags;
	int m_iNumFootstepAudioTags;

	CvString m_szCurrentXMLFile;
	//////////////////////////////////////////////////////////////////////////
	// Formerly Global Defines
	//////////////////////////////////////////////////////////////////////////

	FVariableSystem* m_VarSystem;

	int m_iMOVE_DENOMINATOR;
	int m_iFOOD_CONSUMPTION_PER_POPULATION;
	int m_iMAX_HIT_POINTS;
	int m_iHILLS_EXTRA_DEFENSE;
	int m_iRIVER_ATTACK_MODIFIER;
	int m_iAMPHIB_ATTACK_MODIFIER;
	int m_iHILLS_EXTRA_MOVEMENT;
	int m_iPEAK_EXTRA_MOVEMENT;
	int m_iMAX_PLOT_LIST_ROWS;
	int m_iUNIT_MULTISELECT_MAX;
	int m_iEVENT_MESSAGE_TIME;
	int m_iROUTE_FEATURE_GROWTH_MODIFIER;
	int m_iFEATURE_GROWTH_MODIFIER;
	int m_iMIN_CITY_RANGE;
	int m_iCITY_MAX_NUM_BUILDINGS;
	int m_iLAKE_MAX_AREA_SIZE;
	int m_iMIN_WATER_SIZE_FOR_OCEAN;
	int m_iFORTIFY_MODIFIER_PER_TURN;
	int m_iMAX_CITY_DEFENSE_DAMAGE;
	int m_iPEAK_SEE_THROUGH_CHANGE;
	int m_iHILLS_SEE_THROUGH_CHANGE;
	int m_iSEAWATER_SEE_FROM_CHANGE;
	int m_iPEAK_SEE_FROM_CHANGE;
	int m_iHILLS_SEE_FROM_CHANGE;
	int m_iMAX_REBEL_YIELD_MODIFIER;

	float m_fCAMERA_MIN_YAW;
	float m_fCAMERA_MAX_YAW;
	float m_fCAMERA_FAR_CLIP_Z_HEIGHT;
	float m_fCAMERA_MAX_TRAVEL_DISTANCE;
	float m_fCAMERA_START_DISTANCE;
	float m_fPLOT_SIZE;
	float m_fCAMERA_SPECIAL_PITCH;
	float m_fCAMERA_MAX_TURN_OFFSET;
	float m_fCAMERA_MIN_DISTANCE;
	float m_fCAMERA_UPPER_PITCH;
	float m_fCAMERA_LOWER_PITCH;
	float m_fFIELD_OF_VIEW;
	float m_fUNIT_MULTISELECT_DISTANCE;

	///////////////////////////////////////////////////////////////
	////////*		//DOANE Cache | Speed Improvement		*//////
	///////////////////////////////////////////////////////////////
	//DOANE Delayed Cache
	bool m_bDelayedCacheInitialized;
	//DOANE Cache
	bool m_bCacheGlobalsClean;
	//DOANE Enrol Unit
	unsigned char m_ucMAX_ENROL_UNIT;
	unsigned char m_ucMAX_MILITARY_PROFESSION_TURN;
	unsigned char m_ucSTART_MILITARY_PROFESSION_PENALTY;
	//DOANEFortification
	unsigned char m_ucFORTIFICATION_BREAK_DAMAGE;
	unsigned char m_ucPOWERFUL_FORTIFICATION_PENALTY;
	//DOANE AI Help
	int m_iGOLD_BY_POPULATION_BY_TURN;
	int m_iNUM_DISCOVERED_MAP_TILES_BEFORE_DELIVERING;
	//DOANE Economy
	int m_iMAX_MANUFACTURED_YIELD_AMOUNT;
	//DOANE Builder Pack
	unsigned char m_ucBUILDER_PACK_YIELD_COST_PERCENT;
	unsigned char m_ucBuilderPackToolCost;
	unsigned char m_ucBuilderPackLumberCost;
	//DOANE Inmigration Events
	bool m_bIMMIGRATION_TEXTS_ACTIVATION;
	//DOANE Survivors on Transport Sinking
	unsigned char m_ucSURVIVORS_ON_TRANSPORT_SINKING_PROBABILITY;
	//DoaNE num history turn for trade routes stats
	unsigned char m_ucNUM_TRADE_ROUTE_HIST_TURN;
	//DoaNE  Percentage of boost building construction if current era is above of required era for building
	unsigned char m_ucAVANCED_NEW_ERA_BOOST_FOR_CONSTRUCTION;
	//DoaNE Immigration
	unsigned char m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_AVAILABLE;
	unsigned char m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_DOCK;
	unsigned char m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_CARGO;
	unsigned char m_ucPROBABILITY_NATURAL_IMMIGRATION_START;
	unsigned char m_ucPROBABILITY_NATURAL_IMMIGRATION_LATE;
	unsigned char m_ucPROBABILITY_NATURAL_IMMIGRATION_PERCENT;
	int m_iTURN_NATURAL_IMMIGRATION_CHANGE;
	// City disturbation
	unsigned char m_ucNAVAL_TRANSPORT_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY;
	unsigned char m_ucNAVAL_MILITARY_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY;
	unsigned char m_ucNAVAL_DISTURBATION_FOR_TRADE_ROUTE;
	unsigned char m_ucNAVAL_DISTURBATION_FOR_MILITARY_SHIP_KILLED_PROTECTING_A_CITY;
	//Income and Expenditures
	unsigned char m_ucEXPENDITURE_BY_LAND_FORCE_INSIDE_COLONIES;
	unsigned char m_ucEXPENDITURE_BY_LAND_FORCE_OUTSIDE_COLONIES;
	unsigned char m_ucEXPENDITURE_BY_UNEMPLOYED;
	unsigned char m_ucEXPENDITURE_BY_SCOUT;
	unsigned char m_ucEXPENDITURE_BY_AGRONOMIST;
	unsigned char m_ucEXPENDITURE_BY_PIONEER;
	unsigned char m_ucEXPENDITURE_BY_MERCHANT_SHIP;
	unsigned char m_ucEXPENDITURE_BY_MILITARY_SHIP;

	///////////////////////////////////////////////////////////////
	////////*	//DOANE Python Callbacks | Speed Improvement*//////
	///////////////////////////////////////////////////////////////
	//Vanilla Callbacks
	bool m_bUSE_CAN_FOUND_CITIES_ON_WATER_CALLBACK;
	bool m_bUSE_CANNOT_DO_CIVIC_CALLBACK;
	bool m_bUSE_CAN_DO_CIVIC_CALLBACK;
	bool m_bUSE_CANNOT_CONSTRUCT_CALLBACK;
	bool m_bUSE_CAN_CONSTRUCT_CALLBACK;
	bool m_bUSE_CAN_DECLARE_WAR_CALLBACK;
	bool m_bUSE_GET_UNIT_COST_MOD_CALLBACK;
	bool m_bUSE_GET_BUILDING_COST_MOD_CALLBACK;
	bool m_bUSE_GET_CITY_FOUND_VALUE_CALLBACK;
	bool m_bUSE_CANNOT_HANDLE_ACTION_CALLBACK;
	bool m_bUSE_CAN_BUILD_CALLBACK;
	bool m_bUSE_CANNOT_TRAIN_CALLBACK;
	bool m_bUSE_CAN_TRAIN_CALLBACK;
	bool m_bUSE_UNIT_CANNOT_MOVE_INTO_CALLBACK;
	int m_iUSE_FINISH_TEXT_CALLBACK; //Warning: this is used by the exe!
	int m_iUSE_ON_UNIT_SET_XY_CALLBACK; //Warning: this is used by the exe!
	int m_iUSE_ON_UNIT_SELECTED_CALLBACK; //Warning: this is used by the exe!
	int m_iUSE_ON_MISSIONARY_CONVERTED_UNIT_CALLBACK; //Warning: this is used by the exe!
	int m_iUSE_ON_UPDATE_CALLBACK; //Warning: this is used by the exe!
	int m_iUSE_ON_UNIT_CREATED_CALLBACK; //Warning: this is used by the exe!
	int m_iUSE_ON_UNIT_LOST_CALLBACK; //Warning: this is used by the exe!
	//CvCity
	bool m_bUSE_ON_CITY_DO_GROWTH_CALLBACK;
	bool m_bUSE_ON_CITIES_DESTROY_FEATURES_CALLBACK;
	bool m_bUSE_ON_DO_CULTURE_CALLBACK;
	bool m_bUSE_ON_DO_PLOT_CULTURE_CALLBACK;
	bool m_bUSE_ON_DO_PRODUCTION_CALLBACK;
	//CvCityAI
	bool m_bUSE_AI_CHOOSE_PRODUCTION_CALLBACK;
	//CvGame
	bool m_bUSE_ON_UPDATE_COLORED_PLOTS_CALLBACK;
	bool m_bUSE_CAN_NOT_SELECTION_LIST_MOVE_CALLBACK;
	bool m_bUSE_CAN_NOT_SELECTION_LIST_GAME_NET_MESSAGE_CALLBACK;
	bool m_bUSE_CAN_NOT_DO_CONTROL_CALLBACK;
	bool m_bUSE_ON_DO_REVIVE_ACTIVE_PLAYER_CALLBACK;
	bool m_bUSE_ON_CHECK_IS_VICTORY_CALLBACK;
	bool m_bUSE_ON_CHECK_IS_VICTORY_TEST_CALLBACK;
	//CvDLLButtonPopup
	bool m_bUSE_ON_SKIP_PRODUCTION_POPUP_CALLBACK;
	bool m_bUSE_ON_SHOW_EXAMINE_CITY_BUTTON_CALLBACK;
	bool m_bUSE_GET_RECOMMENDED_UNIT_CALLBACK;
	bool m_bUSE_GET_RECOMMENDED_BUILDING_CALLBACK;
	//CvPlayer
	bool m_bUSE_ON_DO_CITY_CAPTURE_GOLD_CALLBACK;
	bool m_bUSE_CAN_RAZE_CITY_CALLBACK;
	bool m_bUSE_ON_DO_GOLD_CALLBACK;
	//CvPlayerAI
	bool m_bUSE_AI_DO_DIPLO_CALLBACK;
	//CvSelectionGroup
	bool m_bUSE_ON_DO_COMBAT_CALLBACK;
	//CvTeamAI
	bool m_bUSE_AI_DO_WAR_CALLBACK;
	//CvUnit
	bool m_bUSE_ON_IS_ACTION_RECOMMENDED_CALLBACK;
	bool m_bUSE_ON_DO_PILLAGE_GOLD_CALLBACK;
	bool m_bUSE_GET_UPGRADE_PRICE_OVERRIDE_CALLBACK;
	bool m_bUSE_GET_EXPERIENCE_NEEDED_CALLBACK;
	//CvUnitAI
	bool m_bUSE_AI_UNIT_UPDATE_CALLBACK;
	///////////////////////////////////////////////////////////////
	////////*					//END DOANE					*//////
	///////////////////////////////////////////////////////////////

	// DLL interface
	CvDLLUtilityIFaceBase* m_pDLL;
};

extern CvGlobals gGlobals;	// for debugging

//
// inlines
//
inline CvGlobals& CvGlobals::getInstance()
{
	return gGlobals;
}


//
// helpers
//
#define GC CvGlobals::getInstance()
#ifndef _USRDLL
#define gDLL GC.getDLLIFaceNonInl()
#else
#define gDLL GC.getDLLIFace()
#endif

#ifndef _USRDLL
#define NUM_DIRECTION_TYPES (GC.getNumDirections())
#define NUM_RESOURCE_LAYERS (GC.getNumResourceLayers())
#define NUM_UNIT_LAYER_OPTION_TYPES (GC.getNumUnitLayerOptionTypes())
#define NUM_GAMEOPTION_TYPES (GC.getNumGameOptions())
#define NUM_MPOPTION_TYPES (GC.getNumMPOptions())
#define NUM_SPECIALOPTION_TYPES (GC.getNumSpecialOptions())
#define NUM_GRAPHICOPTION_TYPES (GC.getNumGraphicOptions())
#define NUM_TRADEABLE_ITEMS (GC.getNumTradeableItems())
#define NUM_BASIC_ITEMS (GC.getNumBasicItems())
#define NUM_TRADEABLE_HEADINGS (GC.getNumTradeableHeadings())
#define NUM_COMMAND_TYPES (GC.getNumCommandInfos())
#define NUM_CONTROL_TYPES (GC.getNumControlInfos())
#define NUM_MISSION_TYPES (GC.getNumMissionInfos())
#define NUM_PLAYEROPTION_TYPES (GC.getNumPlayerOptionInfos())
#define MAX_NUM_SYMBOLS (GC.getMaxNumSymbols())
#define NUM_GRAPHICLEVELS (GC.getNumGraphicLevels())
#define NUM_TRADE_ROUTE_HIST_TURN (GC.getNUM_TRADE_ROUTE_HIST_TURN())
#endif

//DOANE Europe Types
#ifndef _USRDLL
#define NUM_EUROPE_TYPES (GC.getNumEuropeInfos())
#endif

#define bCL m_bCacheGlobalsClean //DOANE Cache

//helper functions
template <class T>
void deleteInfoArray(std::vector<T*>& array)
{
	for (std::vector<T*>::iterator it = array.begin(); it != array.end(); ++it)
	{
		SAFE_DELETE(*it);
	}

	array.clear();
}

template <class T>
bool readInfoArray(FDataStreamBase* pStream, std::vector<T*>& array, const char* szClassName)
{
	GC.addToInfosVectors(&array);

	int iSize;
	pStream->Read(&iSize);
	FAssertMsg(iSize==sizeof(T), CvString::format("class size doesn't match cache size - check info read/write functions:%s", szClassName).c_str());
	if (iSize!=sizeof(T))
		return false;
	pStream->Read(&iSize);

	deleteInfoArray(array);

	for (int i = 0; i < iSize; ++i)
	{
		array.push_back(new T);
	}

	int iIndex = 0;
	for (std::vector<T*>::iterator it = array.begin(); it != array.end(); ++it)
	{
		(*it)->read(pStream);
		GC.setInfoTypeFromString((*it)->getType(), iIndex);
		++iIndex;
	}

	return true;
}

template <class T>
bool writeInfoArray(FDataStreamBase* pStream,  std::vector<T*>& array)
{
	int iSize = sizeof(T);
	pStream->Write(iSize);
	pStream->Write(array.size());
	for (std::vector<T*>::iterator it = array.begin(); it != array.end(); ++it)
	{
		(*it)->write(pStream);
	}
	return true;
}

#endif
