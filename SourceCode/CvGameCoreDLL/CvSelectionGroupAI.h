#pragma once

// selectionGroupAI.h

#ifndef CIV4_SELECTION_GROUP_AI_H
#define CIV4_SELECTION_GROUP_AI_H

#include "CvSelectionGroup.h"
class CvAgreement;

class CvSelectionGroupAI : public CvSelectionGroup
{

public:

	DllExport CvSelectionGroupAI();
	DllExport virtual ~CvSelectionGroupAI();

	void AI_init();
	void AI_uninit();
	void AI_reset();

	void AI_separate();
	void AI_seperateNonAI(UnitAITypes eUnitAI);
	void AI_seperateAI(UnitAITypes eUnitAI);

	bool AI_update();

	int AI_attackOdds(const CvPlot* pPlot, bool bPotentialEnemy) const;
	CvUnit* AI_getBestGroupAttacker(const CvPlot* pPlot, bool bPotentialEnemy, int& iUnitOdds, bool bForce = false, bool bNoBlitz = false) const;
	CvUnit* AI_getBestGroupSacrifice(const CvPlot* pPlot, bool bPotentialEnemy, bool bForce = false, bool bNoBlitz = false) const;
	CvUnit* AI_getArtilleryUnit(const CvPlot* pPlot) const;
	int AI_compareStacks(const CvPlot* pPlot, bool bPotentialEnemy, bool bCheckCanAttack = false, bool bCheckCanMove = false) const;
	int AI_sumStrength(const CvPlot* pAttackedPlot = NULL, DomainTypes eDomainType = NO_DOMAIN, bool bCheckCanAttack = false, bool bCheckCanMove = false) const;
	int AI_sumBaseCombatStr() const;
	void AI_queueGroupAttack(int iX, int iY);
	void AI_cancelGroupAttack();
	bool AI_isGroupAttack();

	bool AI_isControlled();
	bool AI_isDeclareWar(const CvPlot* pPlot = NULL);

	CvPlot* AI_getMissionAIPlot();

	bool AI_isForceSeparate();
	void AI_makeForceSeparate();

	bool AI_shouldWaitForMerging() const;
	void AI_setShouldWaitForMerging(bool bNewValue);

	bool AI_shouldStopConvoy() const;
	void AI_setShouldStopConvoy(bool bNewValue);
	/*bool AI_shouldResupplyAuto() const;
	void AI_setShouldResupplyAuto(bool bNewValue);*/
	bool AI_shouldAutoHeal() const;
	void AI_setShouldAutoHeal(bool bNewValue);

	int AI_getGroupIdForMerging();
	void AI_setGroupIdForMerging(int iNewValue);

	int AI_getSeawayId();
	void AI_setSeawayId(int iNewValue);

	MissionAITypes AI_getMissionAIType();
	void AI_setMissionAI(MissionAITypes eNewMissionAI, CvPlot* pNewPlot, CvUnit* pNewUnit);
	CvUnit* AI_ejectBestDefender(CvPlot* pTargetPlot);

	CvUnit* AI_getMissionAIUnit();

	bool AI_isFull();

	bool AI_launchAssault(CvPlot* pTargetCityPlot = NULL);
	void AI_groupBombard();

	int AI_getYieldsLoaded(short* piYields);

	bool AI_agreements();
	bool AI_tradeRoutes();

	bool AI_hasScout();

	void AI_unloadYields(CvTradeRoute* pRoute);
	void AI_loadYields(CvTradeRoute* pRoute); 
	
	void AI_europeAutomated();
	void AI_europeNativeTradeAutomated();
	bool AI_selectTradeRouteIfNotAffected();
	bool AI_shouldAddUnitToGroup(CvUnit* pUnit) const;
	void AI_europeTradeRoutes(CvTradeRoute* pRoute);
	std::deque<CvUnit*> AI_getImmigrantsToEmbarkForDestinationCity(CvCity* pDestinationCity);
	void AI_embarkImmigrantsForDestinationCity(CvCity* pDestinationCity, std::deque<CvUnit*> colonists);
	void AI_buyRequiredYieldsForTheTradeRoute(CvTradeRoute* pRoute, CvCity* pDestinationCity, TradeTypes eType);
	void AI_europeAgreements(std::vector<CvAgreement*> aiAgreements);

	bool AI_isFullyResupply() const;
	int AI_resupplyShips(int iMaxMunitions);

	int AI_getCargoNewSpace() const;

	void read(FDataStreamBase* pStream);
	void write(FDataStreamBase* pStream);

protected:

	int m_iMissionAIX;
	int m_iMissionAIY;
	int m_iGroupIdForMerging;
	int m_iSeawayId;

	bool m_bForceSeparate;
	bool m_bShouldWaitForMerging;

	bool m_bStopConvoy;
	//bool m_bResupplyAuto;
	bool m_bAutoHeal;

	MissionAITypes m_eMissionAIType;

	IDInfo m_missionAIUnit;

	bool m_bGroupAttack;
	int m_iGroupAttackX;
	int m_iGroupAttackY;
};

#endif
