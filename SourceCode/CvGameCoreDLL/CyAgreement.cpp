#include "CvGameCoreDLL.h"
#include "CvAgreement.h"
#include "CyAgreement.h"
#include "CyCity.h"


CyAgreement::CyAgreement() :
m_pAgreement(NULL)
{
}

CyAgreement::CyAgreement(CvAgreement* pAgreement) :
	m_pAgreement(pAgreement)
{
}

int CyAgreement::getID() const
{
	return m_pAgreement ? m_pAgreement->getID() : -1;
}

std::wstring CyAgreement::getMerchantName() const
{
	return gDLL->getObjectText(/*m_pAgreement ? m_pAgreement->getMerchantName() : */L"", 0);
}

IDInfo CyAgreement::getSourceCity() const
{
	return (m_pAgreement ? m_pAgreement->getSourceCity() : IDInfo());
}

IDInfo CyAgreement::getDestinationCity() const
{
	return (m_pAgreement ? m_pAgreement->getDestinationCity() : IDInfo());
}

std::wstring CyAgreement::getSourceCityName() const
{
	return gDLL->getObjectText(m_pAgreement ? m_pAgreement->getSourceCityNameKey() : L"", 0);
}

std::wstring CyAgreement::getDestinationCityName() const
{
	return gDLL->getObjectText(m_pAgreement ? m_pAgreement->getDestinationCityNameKey() : L"", 0);
}

int CyAgreement::getTurnMax() const
{
	return m_pAgreement ? m_pAgreement->getTurnMax() : -1;
}

int CyAgreement::getTurnCreated() const
{
	return m_pAgreement ? m_pAgreement->getTurnCreated() : -1;
}

int CyAgreement::getImportYield() const
{
	return m_pAgreement ? m_pAgreement->getImportYield() : -1;
}

int CyAgreement::getExportYield() const
{
	return m_pAgreement ? m_pAgreement->getExportYield() : -1;
}
int CyAgreement::getFixedPrice() const
{
	return m_pAgreement ? m_pAgreement->getFixedPrice() : -1;
}
int CyAgreement::getPrime() const
{
	return m_pAgreement ? m_pAgreement->getPrime() : -1;
}
int CyAgreement::getNumAssignedGroups() const
{
	return m_pAgreement ? m_pAgreement->getNumAssignedGroups() : -1;
}
int CyAgreement::getTurnLeft() const
{
	return m_pAgreement ? m_pAgreement->getTurnLeft() : -1;
}
int CyAgreement::getOriginalAmount(/*YieldTypes*/int eYield) const
{
	return m_pAgreement ? m_pAgreement->getOriginalAmount((YieldTypes)eYield) : -1;
}

int CyAgreement::getActualAmount(/*YieldTypes*/int eYield) const
{
	return m_pAgreement ? m_pAgreement->getActualAmount((YieldTypes)eYield) : -1;
}
