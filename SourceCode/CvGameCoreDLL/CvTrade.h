#pragma once

#ifndef CVTRADE_H
#define CVTRADE_H

#include "CvString.h"
#include "CvDLLButtonPopup.h"

class CvUnit;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  CLASS:      CvTrade
//!  \brief		Trade with europeans or natives
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//
// Class:		CvTrade
// Purpose:		Manages Trade Text...
class CvTrade
{
	friend class CvGlobals;
public:
	// singleton accessor
	DllExport static CvTrade& GetInstance();

	DllExport CvTrade();
	DllExport virtual ~CvTrade();

	ProposalTypes whatKindOfTradeProposal(PlayerTypes ePlayer, bool bAskProposition = 0);
	void calculateGiftOrRequest(CvUnit* pUnit, CvCity* pCity, int aiYields[NUM_YIELD_TYPES], bool bGift, bool bRequest, bool bAsync = true) const;
	void calculateWhatIsTheBetterDeal(CvUnit* pUnit, CvCity* pCity, int aiProposedYields[NUM_YIELD_TYPES], int aiUnitYields[NUM_YIELD_TYPES], bool bAIProp, bool bHumanProp, bool bAsync = true) const;

	bool askGiftOrRequest(CvPopup* pPopup, CvUnit* pUnit, bool bGift, bool bRequest);
	
	bool askProposal(CvPopup* pPopup, CvUnit* pUnit, bool bCityYieldsChose, bool bUniYieldsChose);
	bool AI_askProposal(CvUnit* pUnit, bool bAsync);

	void processProposal(CvUnit* pUnit, int iButtonClicked = 1);
};

// Singleton Accessor
#define GAMETRADE CvTrade::GetInstance()

#endif
