#pragma once

#ifndef CVHOSPITALS_H
#define CVHOSPITALS_H

#include "CvString.h"
#include "CvIdVector.h"

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  CLASS:      CvHospitals
//!  \brief		Manage hospitals
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class DllExport CvHospitals
{
public:
	CvHospitals();

	~CvHospitals();

	void init(PlayerTypes eOwner);
	
	int getID() const;
	void setID(int iId);
	
	PlayerTypes getOwner() const;

	int getStock() const;
	void setStock(int iNewValue);
	void changeStock(int iChange);
	
	int getConsumption() const;
	void setConsumption(int iNewValue);
	void changeConsumption(int iChange);
	
	BuildingTypes getBuilding() const;
	int getTotalNumber() const;
	
	int getHealRate() const;
	int getFoodKeptRate() const;
	int getBirthRate(int iFoodProduction) const;
	
	int calculateClothNetYield() const;
	int getNetClothYield(bool bAuto, int iStockCity, int iProduction) const;
	int getMaxStock() const;
	int getMaxConsumption() const;
	void transferToCity(CvCity* pCity, int iChange);
	
	void doTurn();
	
	void read(FDataStreamBase* pStream);
	void write(FDataStreamBase* pStream);
		
	static const int ANY_HOSPITALS_ID = -2;

protected:
	int m_iId;
	int m_iStock;
	int m_iConsumption;
	
	PlayerTypes m_eOwner;
};


#endif	// CVHOSPITALS_H
