#pragma once

#ifndef CVSEAWAY_H
#define CVSEAWAY_H

#include "CvString.h"

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  CLASS:      CvSeaway
//!  \brief		Seaways for ships
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class DllExport CvSeaway
{
public:
	CvSeaway();

	void init(const CvWString & szName, int iX, int iY);
	int getID() const;
	int getX() const;
	int getY() const;
	void setID(int iId);
	const wchar* getNameKey() const;
	const CvWString & getName() const;
	void setName(const CvWString & szName);
	bool isSamePlot(CvPlot* pPlot) const;
	CvPlot* plot() const;
	int stepDistanceToPlot(CvPlot* destinationPlot) const;
	bool isKnownTo(PlayerTypes ePlayer) const;
	void read(FDataStreamBase* pStream);
	void write(FDataStreamBase* pStream);

protected:
	static int m_iTotalSeaway;
	int			m_iId;
	int			m_iX;
	int			m_iY;
	CvWString	m_szName;
};


#endif	// CVSEAWAY_H
