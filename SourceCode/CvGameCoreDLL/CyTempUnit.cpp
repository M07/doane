//
// Python wrapper class for CvTempUnit
//
//
#include "CvGameCoreDLL.h"
#include "CyTempUnit.h"
#include "CvTempUnit.h"

CyTempUnit::CyTempUnit() : m_pTempUnit(NULL)
{
}
CyTempUnit::CyTempUnit(CvTempUnit* pTempUnit) : m_pTempUnit(pTempUnit)
{
}

int CyTempUnit::getID()
{
	return m_pTempUnit ? m_pTempUnit->getID() : -1;
}

int CyTempUnit::getGameTurnCreated() const
{
	return m_pTempUnit ? m_pTempUnit->getGameTurnCreated() : -1;
}

int CyTempUnit::getPrice() const 
{
	return m_pTempUnit ? m_pTempUnit->getPrice() : -1;
}

int CyTempUnit::getDamage() const
{
	return m_pTempUnit ? m_pTempUnit->getDamage() : -1;
}

bool CyTempUnit::requestTraining() const
{
	return m_pTempUnit ? m_pTempUnit->requestTraining() : false;
}

int /*UnitTypes*/ CyTempUnit::getUnitType()
{
	return m_pTempUnit ? (int)m_pTempUnit->getUnitType() : -1;
}

int /*ProfessionTypes*/ CyTempUnit::getProfession()
{
	return m_pTempUnit ? (int)m_pTempUnit->getProfession() : -1;
}

int /*ImmigrationTypes*/ CyTempUnit::getImmigrationType()
{
	return m_pTempUnit ? (int)m_pTempUnit->getImmigrationType() : (int) NO_IMMIGRATION;
}

std::string CyTempUnit::getFullLengthIcon() const
{
	return m_pTempUnit ? m_pTempUnit->getFullLengthIcon() : "";
}