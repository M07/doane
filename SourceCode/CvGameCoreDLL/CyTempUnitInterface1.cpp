#include "CvGameCoreDLL.h"
#include "CyTempUnit.h"
//
// published python interface for CyTempUnit
//
void CyTempUnitPythonInterface1(python::class_<CyTempUnit>& x)
{
	OutputDebugString("Python Extension Module - CyTempUnitPythonInterface1\n");
	x
		.def("isNone", &CyTempUnit::isNone, "bool () - Is this a valid unit instance?")
		.def("getID", &CyTempUnit::getID, "int ()")
		.def("getGameTurnCreated", &CyTempUnit::getGameTurnCreated, "int ()")
		.def("getPrice", &CyTempUnit::getPrice, "int ()")
		.def("getDamage", &CyTempUnit::getDamage, "int ()")
		.def("requestTraining", &CyTempUnit::requestTraining, "bool ()")
		.def("getUnitType", &CyTempUnit::getUnitType, "int ()")
		.def("getProfession", &CyTempUnit::getProfession, "int ()")
		.def("getImmigrationType", &CyTempUnit::getImmigrationType, "int ()")		
		.def("getFullLengthIcon", &CyTempUnit::getFullLengthIcon, "std::string ()")
		;
}
