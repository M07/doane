#pragma once
#ifndef CySeaway_h
#define CySeaway_h
//
// Python wrapper class for CySeaway
//

#include "CyPlot.h"
class CvSeaway;

class CySeaway
{
public:
	CySeaway();

	DllExport CySeaway(CvSeaway* pSeaway);		// Call from C++
	CvSeaway* getSeaway() { return m_pSeaway;	};	// Call from C++
	const CvSeaway* getSeaway() const { return m_pSeaway;	};	// Call from C++
	bool isNone() { return (m_pSeaway==NULL); }
	int getID();
	std::wstring getName();
	void setName(std::wstring szNewValue);
	CyPlot* plot();
	int stepDistanceToPlot(CyPlot* destinationPlot);
	bool isKnownTo(int /*PlayerTypes*/ ePlayer);
protected:
	CvSeaway* m_pSeaway;
};
#endif	// #ifndef CySeaway
