//------------------------------------------------------------------------------------------------
//
//  ***************** CIV4 GAME ENGINE   ********************
//
//! \file		CvCityJob.cpp
//! \author		M07
//! \brief		Implementation
//
//------------------------------------------------------------------------------------------------
//  Copyright (c) 2015 Fratelli Marconni, Inc. All rights reserved.
//------------------------------------------------------------------------------------------------

#include "CvGameCoreDLL.h"
#include "CvCityJob.h"
#include "CvInfos.h"
#include "CvPlayerAI.h"
#include "CvGameAI.h"
#include "CvTradeRoute.h"

CvCityJob::CvCityJob() :
	m_iID(0),
	m_ucNumRequired(0),
	m_eOwner(NO_PLAYER),
	m_eJob(CITIZEN_JOB),
	m_eProfession(NO_PROFESSION)
{
}


CvCityJob::~CvCityJob()
{
	//uninit();
	//if (!gDLL->GetDone() && GC.IsGraphicsInitialized())						// don't need to remove entity when the app is shutting down, or crash can occur
	//{
	//	CvDLLEntity::removeEntity();		// remove entity from engine
	//}

	//CvDLLEntity::destroyEntity();
}

void CvCityJob::init(PlayerTypes ePlayer, const IDInfo& kCity, JobTypes eJob, unsigned char ucNumRequired, ProfessionTypes eProfession)
{
	m_eOwner = ePlayer;
	m_kCity = kCity;
	m_eJob = eJob;
	m_eProfession = getDefaultProfession(eJob, eProfession);
	m_ucNumRequired = ucNumRequired;

	m_aRequiredUnitClasses.clear();
}

ProfessionTypes CvCityJob::getDefaultProfession(JobTypes eJob, ProfessionTypes eProfession) const
{
	if (eProfession != NO_PROFESSION)
	{
		return eProfession;
	}
	switch(eJob) 
	{
	case WORKERS_JOB:
		return PROFESSION_PIONEER;
	case SCOUTS_JOB:
		return PROFESSION_SCOUT;
	case AGRONOMIST_JOB:
		return PROFESSION_COLONIST;
	default:
		return NO_PROFESSION;
	}
}


void CvCityJob::uninit()
{
	m_units.clear();
}

JobTypes CvCityJob::getJobType() const
{
	return m_eJob;
}

const IDInfo& CvCityJob::getCity() const
{
	return m_kCity;
}

void CvCityJob::setCity(const IDInfo& kCity)
{
	if (getCity() != kCity)
	{
		m_kCity = kCity;
	}
}

int CvCityJob::getID() const
{
	return m_iID;
}
void CvCityJob::setID(int iID)
{
	m_iID = iID;
}

void CvCityJob::doTurn() 
{
	switch(getJobType())
	{
	case WORKERS_JOB:
		updateWorkersNeeded();
		break;
	case MILITARY_JOB:
		updateDefendersNeeded();
		adpaptCityDefenders();
		updateDefendersNeeded();
		break;
	case SCOUTS_JOB:
		updateScoutsNeeded();
		break;
	case AGRONOMIST_JOB:
		updateAgronomistNeeded();
		break;
	case COLONISTS_JOB:
		updateColonistNeeded();
		break;
	}

	if (getNumUnits() >= getNumRequired()) 
	{
		return;
	}

	updateRequiredUnitClass();
}

void CvCityJob::updateRequiredUnitClass()
{
	if (getRequiredUnitClassType() == NO_UNITCLASS) 
	{
		switch(getJobType()) 
		{
		case SCOUTS_JOB:
		case COLONISTS_JOB:
			addRequiredUnitType(UNIT_COLONIST);
			break;
		case AGRONOMIST_JOB:
			addRequiredUnitType(UNIT_AGRONOME);
			break;
		case MILITARY_JOB:
			addRequiredUnitType(UNIT_COLONIST);
			addRequiredUnitType(UNIT_VETERAN);
			addRequiredUnitType(UNIT_ARTILLERYMAN_EXPERT);
			addRequiredUnitType(UNIT_DOCTOR);
			addRequiredUnitType(UNIT_DOCTOR_EXPERT);
			break;
		default:
			addRequiredUnitClass(getUnitClassToRecruit(), 0);
		}
	}
}

void CvCityJob::updateColonistNeeded()
{
	CvCity* pCity = ::getCity(getCity());
	if (pCity != NULL) 
	{
		int iNumRequired = 0;

		iNumRequired += pCity->getNumProfessionBuildingSlots(PROFESSION_STATESMAN) - pCity->professionCount(PROFESSION_STATESMAN);
		iNumRequired += pCity->getNumProfessionBuildingSlots(PROFESSION_PREACHER) - pCity->professionCount(PROFESSION_PREACHER);

		setNumRequired(iNumRequired);
	}
}

void CvCityJob::updateAgronomistNeeded()
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvCity* pCity = ::getCity(getCity());
	if (pCity != NULL) 
	{
		int iNumRequired = pCity->AI_getAgronomistsNeededHere();
		setNumRequired(iNumRequired);
	}
}

void CvCityJob::updateScoutsNeeded()
{
	const int maxScouts = 3;
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	int iNumRequired = maxScouts - kPlayer.getNumCities();

	setNumRequired(std::max(iNumRequired, 0));
}

void CvCityJob::updateWorkersNeeded()
{
	CvCity* pCity = ::getCity(getCity());
	setNumRequired(pCity->AI_getWorkersNeeded());
}

void CvCityJob::updateDefendersNeeded()
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvCity* pCity = ::getCity(getCity());
	int iPopulation = pCity->getPopulation();
	int iNumRequired = 0;
	int iMinPolulationToHaveDefender = 5 - kPlayer.getNumCities();
	if (iPopulation >= iMinPolulationToHaveDefender) 
	{
		iNumRequired = (iPopulation - iMinPolulationToHaveDefender) / 2 + 1;
	}
	setNumRequired(iNumRequired);
}


void CvCityJob::adpaptCityDefenders() 
{
	CvCityAI* pCity = (CvCityAI*) ::getCity(getCity());
	CvPlot* pPlotCity = pCity->plot();
	CvPlayerAI& kPlayer = GET_PLAYER(getOwner());
	CLLNode<IDInfo>* pUnitNode;

	CvUnit* pLoopUnit;

	checkIfUnitsShouldChangeCity();

	// We add unused units to military
	if (getNumUnits() < getNumRequired()) 
	{
		CvUnit* pBestUnit = NULL;

		pUnitNode = pPlotCity->headUnitNode();
		int iBestUnitValue = 0;
		while (pUnitNode != NULL)
		{
			pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlotCity->nextUnitNode(pUnitNode);
			int iValue = 0;
			ProfessionTypes eProfession = getProfession();
			if (pLoopUnit != NULL && pLoopUnit->getCityJobID() == -1) 
			{
				switch(pLoopUnit->getUnitType()) 
				{
				case UNIT_VETERAN:
					iValue = 200;
					break;
				case UNIT_CRIMINAL:
					iValue = 150;
					break;
				case UNIT_COLONIST:
					iValue = 100;
					break;
				}

				if (iValue > iBestUnitValue)
				{
					pBestUnit = pLoopUnit;
					iBestUnitValue = iValue;
				}
			}
		}

		if (pBestUnit != NULL) 
		{
			int iBestValue = 0;
			ProfessionTypes eBestProfession = NO_PROFESSION;
			for (int iProfession = 0; iProfession < GC.getNumProfessionInfos(); ++iProfession)
			{
				ProfessionTypes eProfession = (ProfessionTypes) iProfession;
				if (GC.getProfessionInfo(eProfession).isCityDefender())
				{
					int iValue = kPlayer.AI_professionValue(eProfession, UNITAI_DEFENSIVE);
					if (iValue > iBestValue)
					{
						if (pBestUnit->canHaveProfession(eProfession, false))
						{
							iBestValue = iValue;
							eBestProfession = eProfession;
						}
					}
				}
			}
			if (eBestProfession != NO_PROFESSION) 
			{
				pBestUnit->setProfession(eBestProfession);
				pBestUnit->AI_setUnitAIType(UNITAI_DEFENSIVE);
				addUnit(pBestUnit);
			}
		}
	}

	int iTurn = GC.getGameINLINE().getGameTurn();
	//Improve profession if needed
	if (iTurn % 3 == 0) 
	{
		pUnitNode = headUnitNode();
		while (pUnitNode != NULL)
		{
			pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = nextUnitNode(pUnitNode);
			if (pLoopUnit != NULL)
			{
				ProfessionTypes eProfession = pCity->AI_getBestProfessionForSoldier(pLoopUnit); 

				if (eProfession != pLoopUnit->getProfession())
				{
					pLoopUnit->setProfession(eProfession);
				}
			}
		}
	}

	CvTradeRoute* pTradeRoute = pCity->AI_getTradeRouteToEurope();
	if (pTradeRoute != NULL) 
	{
		if (shouldBuyMuskets()) 
		{
			pTradeRoute->buyUntilQuantityReached(YIELD_MUSKETS, 50, 25);
		} 
		else 
		{
			pTradeRoute->setTradeType(YIELD_MUSKETS, NO_TRADE);
		}
	} 	
}

bool CvCityJob::shouldBuyMuskets() const
{
	int iNumUnits = getNumUnits();
	if (iNumUnits < 4) 
	{
		return false;
	}

	int iSwordmenCount = 0;
	CvUnit* pLoopUnit;
	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);
		if (pLoopUnit != NULL && pLoopUnit->getProfession() == PROFESSION_SWORDMAN) 
		{
			iSwordmenCount++;
		}
	}

	return iSwordmenCount > iNumUnits / 2;
}

void CvCityJob::checkIfUnitsShouldChangeCity()
{
	CvCityAI* pCity = (CvCityAI*) ::getCity(getCity());
	CvPlot* pPlotCity = pCity->plot();
	CvPlayerAI& kPlayer = GET_PLAYER(getOwner());
	CLLNode<IDInfo>* pUnitNode;

	CvUnit* pLoopUnit;
	CvUnit* pBestUnit = NULL;
	
	std::vector<CvUnit*> aUnits;

	pUnitNode = headUnitNode();
	int iBestUnitValue = 0;
	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);
		if (pLoopUnit != NULL && pLoopUnit->getCityJob() == NULL) 
		{
			CvPlot* pUnitPlot = pLoopUnit->plot();
			if (pUnitPlot != NULL && pUnitPlot != pPlotCity) 
			{
				CvCity* pUnitCity = pUnitPlot->getPlotCity();
				if (pUnitCity != NULL && pUnitCity->getOwner() == getOwner())
				{
					CvCityJob* pJob = pUnitCity->getCityJobByType(MILITARY_JOB);
					if (pJob != NULL) 
					{
						pJob->addUnit(pLoopUnit);
						aUnits.push_back(pLoopUnit);
					}
				}
			}
		}
	}

	for (uint i = 0; i < aUnits.size(); ++i)
	{
		removeUnit(aUnits[i], false);
	}
}

ProfessionTypes CvCityJob::getEuropeProfession() const
{
	ProfessionTypes eProfession = getProfession();
	
	if (getJobType() == CITIZEN_JOB) 
	{
		CvPlayer& kPlayer = GET_PLAYER(getOwner());
		CivilizationTypes eCivilization = kPlayer.getCivilizationType();
		UnitClassTypes eUnitClass = findBestUnitClass();
		if (eUnitClass == NO_UNITCLASS) 
		{
			return NO_PROFESSION;
		}

		UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(kPlayer.getCivilizationType()).getCivilizationUnits(eUnitClass);
		if (NO_UNIT != eUnit && GC.getUnitInfo(eUnit).getProbaImmigration() == 0) 
		{
			//IF unit cannot be recruited in Europe
			eProfession = PROFESSION_SCOUT;
		}
	}
	else if (getJobType() == MILITARY_JOB) 
	{
		eProfession = PROFESSION_SWORDMAN;
	}

	return eProfession;
}

UnitAITypes CvCityJob::getUnitAIType() const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	UnitAITypes eUnitAI = NO_UNITAI;
	UnitClassTypes eUnitClass = findBestUnitClass();
	if (eUnitClass == NO_UNITCLASS)
	{
		return NO_UNITAI;
	}
	UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(kPlayer.getCivilizationType()).getCivilizationUnits(eUnitClass);
	switch(getJobType())
	{
	case CITIZEN_JOB:
		if (NO_UNIT != eUnit && GC.getUnitInfo(eUnit).getProbaImmigration() == 0) 
		{ 
			//IF unit cannot be recruited in Europe
			eUnitAI = UNITAI_NATIVE_LEARN;
		}
		break;
	case MILITARY_JOB:
		eUnitAI = UNITAI_DEFENSIVE;
		break;
	}
	return eUnitAI;
}

bool CvCityJob::shouldRecruitColonist() const
{
	switch(getJobType()) 
	{
	case MILITARY_JOB:
	case COLONISTS_JOB:
		return true;
	}
	return false;
}

UnitClassTypes CvCityJob::getUnitClassToRecruit() const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	UnitClassTypes eUnitClassColonist = (UnitClassTypes) GC.getUnitInfo(UNIT_COLONIST).getUnitClassType();
	UnitClassTypes eUnitClass = findBestUnitClass();

	if (eUnitClass == NO_UNITCLASS)
	{
		return eUnitClassColonist;
	}

	if (getJobType() == MILITARY_JOB) 
	{
		return eUnitClass;
	}

	UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(kPlayer.getCivilizationType()).getCivilizationUnits(eUnitClass);

	return GC.getUnitInfo(eUnit).getProbaImmigration() > 0 ? eUnitClass : eUnitClassColonist;
}

UnitClassTypes CvCityJob::findBestUnitClass() const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	bool bTestProfession = getProfession() != NO_PROFESSION;
	for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass) 
	{
		UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(kPlayer.getCivilizationType()).getCivilizationUnits(iUnitClass);
		if (NO_UNIT != eUnit)
		{
			CvUnitInfo& kUnit = GC.getUnitInfo(eUnit);
			if (bTestProfession && kUnit.getIdealProfession() == getProfession())
			{
				return (UnitClassTypes) iUnitClass;
			}

			if (getJobType() == MILITARY_JOB) 
			{
				if (kUnit.getUnitAIType(UNITAI_DEFENSIVE)) 
				{
					return (UnitClassTypes) iUnitClass;
				}
			}
		}
	}
	return NO_UNITCLASS;
}

PlayerTypes CvCityJob::getOwner() const
{
	return m_eOwner;
}

ProfessionTypes CvCityJob::getProfession() const
{
	return m_eProfession;
}

int CvCityJob::getNumRequiredUnitClasses() const
{
	return m_aRequiredUnitClasses.size();
}

UnitClassTypes CvCityJob::getRequiredUnitClassType(int i) const
{
	if (i >= getNumRequiredUnitClasses())
	{
		return NO_UNITCLASS;
	}

	return m_aRequiredUnitClasses[i].first;
}

int CvCityJob::getRequiredUnitClassAmount(int i) const
{
	FAssert(i >= 0 && i < getNumRequiredUnitClasses());

	return m_aRequiredUnitClasses[i].second;
}

void CvCityJob::addRequiredUnitType(UnitTypes eUnitType)
{
	UnitClassTypes eUnitClassType = (UnitClassTypes) GC.getUnitInfo(eUnitType).getUnitClassType();
	addRequiredUnitClass(eUnitClassType, 0);
}

void CvCityJob::addRequiredUnitClass(UnitClassTypes eUnitClassType, byte iAmount)
{
	m_aRequiredUnitClasses.push_back(std::make_pair(eUnitClassType, iAmount));
}

void CvCityJob::setRequiredUnitClassAmount(int i, byte iAmount)
{
	m_aRequiredUnitClasses[i].second = iAmount;
}

int CvCityJob::getNumUnitNeeded() const
{
	return std::max(getNumRequired() - getNumUnits(), 0);
}

unsigned char CvCityJob::getNumRequired() const 
{
	return m_ucNumRequired;
}

void CvCityJob::setNumRequired(unsigned char ucValue) 
{
	m_ucNumRequired = ucValue;
}

void CvCityJob::changeNumRequired(unsigned char ucChange) 
{
	setNumRequired(getNumRequired() + ucChange);
}

int CvCityJob::getNumUnits() const
{
	return m_units.getLength();
}

void CvCityJob::addUnit(CvUnit* pUnit)
{
	m_units.insertAtEnd(pUnit->getIDInfo());
	pUnit->setCityJobID(getID());

	CvCity* pCity = ::getCity(getCity());
	if (pCity != NULL) 
	{
		pUnit->setWorkingCity(pCity);
		pUnit->setHomeCity(pCity);
	}
	/*
	if (getNumUnits() >= getNumRequired()) 
	{
		setRequiredUnitClass(NO_UNITCLASS);
	}
	*/
}

void CvCityJob::removeUnit(CvUnit* pUnit, bool bRemoveCityID) 
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CLLNode<IDInfo>* pUnitNode = headUnitNode();

	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		if (pLoopUnit == NULL)
		{
			pLoopUnit = kPlayer.getEuropeUnitById(pUnitNode->m_data.iID);
		}

		if (pLoopUnit == pUnit)
		{
			if (bRemoveCityID) 
			{
				pUnit->setCityJobID(-1);
			}
			m_units.deleteNode(pUnitNode);
			break;
		}
		else
		{
			pUnitNode = nextUnitNode(pUnitNode);
		}
	}
}

CLLNode<IDInfo>* CvCityJob::nextUnitNode(CLLNode<IDInfo>* pNode) const
{
	return m_units.next(pNode);
}

CLLNode<IDInfo>* CvCityJob::headUnitNode() const
{
	return m_units.head();
}

const wchar* CvCityJob::getProfessionHelp() const
{
	ProfessionTypes eProfession = getProfession();
	const wchar* professionName = L"";
	switch(getJobType()) 
	{
	case AGRONOMIST_JOB:
	case CITIZEN_JOB:
		professionName = eProfession != NO_PROFESSION ? GC.getProfessionInfo(eProfession).getDescription() : L"";
		break;
	case MILITARY_JOB:
		professionName = L"Soldiers";
		break;
	case SCOUTS_JOB:
		professionName = L"Scout";
		break;
	case WORKERS_JOB:
		professionName = L"Workers";
		break;
	}
	return professionName;
}

void CvCityJob::read(FDataStreamBase* pStream)
{
	uint uiFlag=0;
	pStream->Read(&uiFlag);	// flags for expansion
	pStream->Read(&m_iID);
	pStream->Read(&m_ucNumRequired);
	pStream->Read((int*)&m_eOwner);
	pStream->Read((int*)&m_eJob);
	pStream->Read((int*)&m_eProfession);

	{
		m_aRequiredUnitClasses.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			UnitClassTypes eUnitClassType;
			byte iAmount;
			pStream->Read((int*)&eUnitClassType);
			pStream->Read(&iAmount);

			m_aRequiredUnitClasses.push_back(std::make_pair(eUnitClassType, iAmount));
		}
	}

	m_kCity.read(pStream);
	m_units.Read(pStream);
}

void CvCityJob::write(FDataStreamBase* pStream) const
{
	uint uiFlag=0;
	pStream->Write(uiFlag);		// flag for expansion
	pStream->Write(m_iID);
	pStream->Write(m_ucNumRequired);
	pStream->Write(m_eOwner);
	pStream->Write(m_eJob);
	pStream->Write(m_eProfession);

	{
		uint iSize = m_aRequiredUnitClasses.size();
		pStream->Write(iSize);
		std::vector< std::pair<UnitClassTypes, byte> >::const_iterator it;
		for (it = m_aRequiredUnitClasses.begin(); it != m_aRequiredUnitClasses.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	m_kCity.write(pStream);
	m_units.Write(pStream);
}
