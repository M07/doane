#include "CvGameCoreDLL.h"
#include "CyPlayer.h"
#include "CyUnit.h"
#include "CyCity.h"
#include "CyPlot.h"
#include "CySelectionGroup.h"
#include "CyArea.h"
#include "CyTradeRoute.h"
#include "CyAgreement.h"
//# include <boost/python/manage_new_object.hpp>
//# include <boost/python/return_value_policy.hpp>
//# include <boost/python/scope.hpp>
//
// published python interface for CyPlayer
//
void CyPlayerPythonInterface2(python::class_<CyPlayer>& x)
{
	OutputDebugString("Python Extension Module - CyPlayerPythonInterface2\n");
	// set the docstring of the current module scope
	python::scope().attr("__doc__") = "Civilization IV Player Class";
	x
		.def("AI_updateFoundValues", &CyPlayer::AI_updateFoundValues, "void (bool bStartingLoc)")
		.def("AI_foundValue", &CyPlayer::AI_foundValue, "int (int, int, int, bool)")
		.def("AI_demandRebukedWar", &CyPlayer::AI_demandRebukedWar, "bool (int /*PlayerTypes*/)")
		.def("AI_getAttitude", &CyPlayer::AI_getAttitude, "AttitudeTypes (int /*PlayerTypes*/) - Gets the attitude of the player towards the player passed in")
		.def("AI_unitValue", &CyPlayer::AI_unitValue, "int (int /*UnitTypes*/ eUnit, int /*UnitAITypes*/ eUnitAI, CyArea* pArea)")
		.def("AI_civicValue", &CyPlayer::AI_civicValue, "int (int /*CivicTypes*/ eCivic)")
		.def("AI_totalUnitAIs", &CyPlayer::AI_totalUnitAIs, "int (int /*UnitAITypes*/ eUnitAI)")
		.def("AI_totalAreaUnitAIs", &CyPlayer::AI_totalAreaUnitAIs, "int (CyArea* pArea, int /*UnitAITypes*/ eUnitAI)")
		.def("AI_totalWaterAreaUnitAIs", &CyPlayer::AI_totalWaterAreaUnitAIs, "int (CyArea* pArea, int /*UnitAITypes*/ eUnitAI)")
		.def("AI_getNumAIUnits", &CyPlayer::AI_getNumAIUnits, "int (UnitAIType) - Returns # of UnitAITypes the player current has of UnitAIType")
		.def("AI_getAttitudeExtra", &CyPlayer::AI_getAttitudeExtra, "int (int /*PlayerTypes*/ eIndex) - Returns the extra attitude for this player - usually scenario specific")
		.def("AI_setAttitudeExtra", &CyPlayer::AI_setAttitudeExtra, "void (int /*PlayerTypes*/ eIndex, int iNewValue) - Sets the extra attitude for this player - usually scenario specific")
		.def("AI_changeAttitudeExtra", &CyPlayer::AI_changeAttitudeExtra, "void (int /*PlayerTypes*/ eIndex, int iChange) - Changes the extra attitude for this player - usually scenario specific")
		.def("AI_getMemoryCount", &CyPlayer::AI_getMemoryCount, "int (/*PlayerTypes*/ eIndex1, /*MemoryTypes*/ eIndex2)")
		.def("AI_changeMemoryCount", &CyPlayer::AI_changeMemoryCount, "void (/*PlayerTypes*/ eIndex1, /*MemoryTypes*/ eIndex2, int iChange)")
		.def("AI_getExtraGoldTarget", &CyPlayer::AI_getExtraGoldTarget, "int ()")
		.def("AI_setExtraGoldTarget", &CyPlayer::AI_setExtraGoldTarget, "void (int)")
		.def("getScoreHistory", &CyPlayer::getScoreHistory, "int (int iTurn)")
		.def("getEconomyHistory", &CyPlayer::getEconomyHistory, "int (int iTurn)")
		.def("getIndustryHistory", &CyPlayer::getIndustryHistory, "int (int iTurn)")
		.def("getAgricultureHistory", &CyPlayer::getAgricultureHistory, "int (int iTurn)")
		.def("getLandMilitaryHistory", &CyPlayer::getLandMilitaryHistory, "int (int iTurn)")
		.def("getSeaMilitaryHistory", &CyPlayer::getSeaMilitaryHistory, "int (int iTurn)")
		.def("getNavalMerchantHistory", &CyPlayer::getNavalMerchantHistory, "int (int iTurn)")
		.def("getCultureHistory", &CyPlayer::getCultureHistory, "int (int iTurn)")
		.def("getScriptData", &CyPlayer::getScriptData, "str () - Get stored custom data (via pickle)")
		.def("setScriptData", &CyPlayer::setScriptData, "void (str) - Set stored custom data (via pickle)")
		.def("AI_maxGoldTrade", &CyPlayer::AI_maxGoldTrade, "int (int)")
		.def("forcePeace", &CyPlayer::forcePeace, "void (int)")
		.def("getHighestTradedYield", &CyPlayer::getHighestTradedYield, "YieldTypes ()")
		.def("getHighestStoredYieldCityId", &CyPlayer::getHighestStoredYieldCityId, "int (int /*YieldTypes*/)")
		.def("getAcceptSeedlingTurn", &CyPlayer::getAcceptSeedlingTurn, "int (int /*PlayerTypes*/ ePlayer)")
 		.def("getCrossesStored", &CyPlayer::getCrossesStored, "int ()")
		.def("getBellsStored", &CyPlayer::getBellsStored, "int ()")
		.def("getTaxRate", &CyPlayer::getTaxRate, "int ()")
		.def("changeTaxRate", &CyPlayer::changeTaxRate, "void (int)")
		.def("canTradeWithEurope", &CyPlayer::canTradeWithEurope, "bool ()")
		.def("getYieldSellPrice", &CyPlayer::getYieldSellPrice, "int (eYield)")
		.def("getYieldBuyPrice", &CyPlayer::getYieldBuyPrice, "int (eYield)")
		.def("getEuropeWarehouseYield", &CyPlayer::getEuropeWarehouseYield, "int (eYield)")
		.def("getEuropeBuyTradeYield", &CyPlayer::getEuropeBuyTradeYield, "int (eYield)")
		.def("getEuropeGoldBuyTradeYield", &CyPlayer::getEuropeGoldBuyTradeYield, "int (eYield)")
		.def("getEuropeSellTradeYield", &CyPlayer::getEuropeSellTradeYield, "int (eYield)")
		.def("getEuropeGoldSellTradeYield", &CyPlayer::getEuropeGoldSellTradeYield, "int (eYield)")
		.def("getEuropeTaxTradeYield", &CyPlayer::getEuropeTaxTradeYield, "int (eYield)")
		.def("getIATradeProposition", &CyPlayer::getIATradeProposition, "int (eYield)")
		.def("setIATradeProposition", &CyPlayer::setIATradeProposition, "void (eYield, iValue)")
		.def("getHumanTradeProposition", &CyPlayer::getHumanTradeProposition, "int (eYield)")
		.def("setHumanTradeProposition", &CyPlayer::setHumanTradeProposition, "void (eYield, iValue)")
		.def("getBuyPriceForYield", &CyPlayer::getBuyPriceForYield, "int (int /*YieldTypes*/ eYield, int iChange)")
		.def("getSellPriceForYield", &CyPlayer::getSellPriceForYield, "int (int /*YieldTypes*/ eYield, int iChange)")
		.def("sellYieldUnitToEurope", &CyPlayer::sellYieldUnitToEurope, "void (CyUnit* pUnit, int iAmount, int iCommission, int /*YieldTypes*/ eYield)")
		.def("buyYieldUnitFromEurope", &CyPlayer::buyYieldUnitFromEurope, "bool (int /*YieldTypes*/ eYield, int iAmount, CyUnit* pTransport)")
		.def("getEuropeUnitBuyPrice", &CyPlayer::getEuropeUnitBuyPrice, "int (int (UnitTypes))")
		.def("buyEuropeUnit", &CyPlayer::buyEuropeUnit, python::return_value_policy<python::manage_new_object>(), "CyUnit (int /*UnitTypes*/ eUnit)")
		.def("getYieldBoughtTotal", &CyPlayer::getYieldBoughtTotal, "int (int /*YieldTypes*/ eYield)")
		.def("getNumRevolutionEuropeUnits", &CyPlayer::getNumRevolutionEuropeUnits, "int ()")
		.def("getRevolutionEuropeUnit", &CyPlayer::getRevolutionEuropeUnit, "int (int iIndex)")
		.def("getRevolutionEuropeProfession", &CyPlayer::getRevolutionEuropeProfession, "int (int iIndex)")
		.def("isEurope", &CyPlayer::isEurope, "bool ()")
		.def("isInRevolution", &CyPlayer::isInRevolution, "bool ()")
		.def("getImmigrationRelationPoints", &CyPlayer::getImmigrationRelationPoints, "short ()")
		.def("getAgreementOfCity", &CyPlayer::getAgreementOfCity, "int (CyCity* pCity)")
		.def("getAgreement", &CyPlayer::getAgreement, python::return_value_policy<python::manage_new_object>(), "CyAgreement (int iId)")
		.def("firstAgreement", &CyPlayer::firstAgreement, "tuple(CyAgreement, int iterOut) () - gets the first Agreement")
		.def("nextAgreement", &CyPlayer::nextAgreement, "tuple(CyAgreement, int iterOut) (int iterIn) - gets the next Agreement")
		
		.def("getTradeRoute", &CyPlayer::getTradeRoute, python::return_value_policy<python::manage_new_object>(), "CyTradeRoute (int iIndex)")
		.def("firstTradeRoute", &CyPlayer::firstTradeRoute, "tuple(CyTradeRoute, int iterOut) () - gets the first TradeRoute")
		.def("nextTradeRoute", &CyPlayer::nextTradeRoute, "tuple(CyTradeRoute, int iterOut) (int iterIn) - gets the next TradeRoute")
		.def("getNumTradeRoutes", &CyPlayer::getNumTradeRoutes, "int ()")
		
		.def("canLoadYield", &CyPlayer::canLoadYield, "	bool canLoadYield(int /*PlayerTypes*/ eCityPlayer)")
		.def("canUnloadYield", &CyPlayer::canUnloadYield, "bool canUnloadYield(int /*PlayerTypes*/ eCityPlayer) ")
		.def("getYieldEquipmentAmount", &CyPlayer::getYieldEquipmentAmount, "int (int iProfession, int iYield)")
		.def("isProfessionValid", &CyPlayer::isProfessionValid, "int (int iProfession, int eUnitType)")
		.def("addRevolutionEuropeUnit", &CyPlayer::addRevolutionEuropeUnit, "void addRevolutionEuropeUnit(int /*UnitTypes*/ eUnit, int /*ProfessionTypes*/ eProfession)")
		.def("getNumTradeMessages", &CyPlayer::getNumTradeMessages, "int ()")
		.def("getTradeMessage", &CyPlayer::getTradeMessage, "string (int)")
		.def("getNumEuropeTradeMessages", &CyPlayer::getNumEuropeTradeMessages, "int ()")
		.def("getEuropeTradeMessage", &CyPlayer::getEuropeTradeMessage, "string (int)")
		.def("getNumEventEuropeMessages", &CyPlayer::getNumEventEuropeMessages, "int ()")
		.def("getEventEuropeMessage", &CyPlayer::getEventEuropeMessage, "string (int)")
		.def("getLocalPlayerActionPopupData3", &CyPlayer::getLocalPlayerActionPopupData3, "int ()")
		.def("getEuropeTradeAmount", &CyPlayer::getEuropeTradeAmount, "int (int /*YieldTypes*/ eYield, int /*PlayerTypes*/ eToPlayer)")
		.def("getEuropeTradeTotalAmount", &CyPlayer::getEuropeTradeTotalAmount, "int (YieldTypes eYield, PlayerTypes eToPlayer)")
		.def("isMarketTransactionBlocked",  &CyPlayer::isMarketTransactionBlocked, "bool (int /*YieldTypes*/ eYield, int /*PlayerTypes*/ eToPlayer)")
		.def("getEuropeNeeds",  &CyPlayer::getEuropeNeeds, "int (int /*YieldTypes*/ eYield)");
		;
}
