#pragma once

// player.h
#define NUM_TUTORIAL_DOANE 3
#define NUM_MAPS 40
#define NUM_HIST_TURN 15
#define NUM_RAND_VALUES 15

#ifndef CIV4_PLAYER_H
#define CIV4_PLAYER_H

#include "CvCityAI.h"
#include "CvUnitAI.h"
#include "CvSelectionGroupAI.h"
#include "LinkedList.h"
#include "CvIdVector.h"
#include "CvTalkingHeadMessage.h"
#include "CvSeaway.h"

class CvDiploParameters;
class CvPopupInfo;
class CvEventTriggerInfo;
class CvTradeRoute;
class CvRequirementsNewEra;
class CvPirateInfo;
class CvHospitals;
class CvAgreement;
class CvTempUnit;
class CvMarketTransaction;
class CvConvoy;

typedef std::list<CvTalkingHeadMessage> CvMessageQueue;
typedef std::list<CvPopupInfo*> CvPopupQueue;
typedef std::list<CvDiploParameters*> CvDiploQueue;
typedef stdext::hash_map<int, int> CvTurnScoreMap;
typedef stdext::hash_map<int, stdext::hash_map<int, int> > CvTurnImmigrationMap;
typedef stdext::hash_map<int, int> CvImmigrationIntInfoMap;
typedef stdext::hash_map<int, float> CvImmigrationFloatInfoMap;
typedef stdext::hash_map<EventTypes, EventTriggeredData> CvEventMap;
typedef std::vector< std::pair<UnitCombatTypes, PromotionTypes> > UnitCombatPromotionArray;
typedef std::vector< std::pair<UnitClassTypes, PromotionTypes> > UnitClassPromotionArray;
typedef std::vector< std::pair<CivilizationTypes, LeaderHeadTypes> > CivLeaderArray;

class CvPlayer
{
public:
	CvPlayer();
	virtual ~CvPlayer();

	DllExport void init(PlayerTypes eID);
	DllExport void setupGraphical();
	DllExport void reset(PlayerTypes eID = NO_PLAYER, bool bConstructorCall = false);

protected:

	void uninit();

public:

	void initFreeState();
	void initFreeUnits();
	void addFreeUnitAI(UnitAITypes eUnitAI, int iCount);
	CvUnit* addFreeUnit(UnitTypes eUnit, ProfessionTypes eProfession, UnitAITypes eUnitAI = NO_UNITAI);
	int startingPlotRange() const;
	int startingPlotDistanceFactor(CvPlot* pPlot, PlayerTypes ePlayer, int iRange) const;
	int findStartingArea() const;
	CvPlot* findStartingPlot(bool bRandomize = false);
	DllExport CvCity* initCity(int iX, int iY, bool bBumpUnits);
	void acquireCity(CvCity* pCity, bool bConquest, bool bTrade);
	void killCities();
	const CvWString getNewCityName() const;
	const CvWString& addNewCityName();
	void getCivilizationCityName(CvWString& szBuffer, CivilizationTypes eCivilization) const;
	bool isCityNameValid(const CvWString& szName, bool bTestDestroyed = true) const;
	DllExport CvUnit* initUnit(UnitTypes eUnit, ProfessionTypes eProfession, int iX, int iY, UnitAITypes eUnitAI = NO_UNITAI, DirectionTypes eFacingDirection = NO_DIRECTION, int iYieldStored = 0);
	DllExport CvUnit* initEuropeUnit(UnitTypes eUnit, UnitAITypes eUnitAI = NO_UNITAI, DirectionTypes eFacingDirection = NO_DIRECTION, bool bLikelyToLeave = true);
	void killUnits();
	DllExport CvSelectionGroup* cycleSelectionGroups(CvUnit* pUnit, bool bForward, bool* pbWrap);
	bool hasTrait(TraitTypes eTrait) const;
	int getTraitCount(TraitTypes eTrait) const;
	void changeTraitCount(TraitTypes eTrait, int iChange);
	int getMercantileFactor() const;
	DllExport bool isHuman() const;
	DllExport void updateHuman();
	void definePlayerRole();
	bool isNative() const;
	bool isEurope() const;
	bool isEuropean() const;
	bool isEuropeanAI() const;
	bool isAlwaysOpenBorders() const;
	DllExport const wchar* getName(uint uiForm = 0) const;
	DllExport const wchar* getNameKey() const;
	DllExport const wchar* getCivilizationDescription(uint uiForm = 0) const;
	DllExport const wchar* getCivilizationDescriptionKey() const;
	DllExport const wchar* getCivilizationShortDescription(uint uiForm = 0) const;
	DllExport const wchar* getCivilizationShortDescriptionKey() const;
	DllExport const wchar* getCivilizationAdjective(uint uiForm = 0) const;
	DllExport const wchar* getCivilizationAdjectiveKey() const;
	DllExport const char* getFlagDecal() const;
	DllExport bool isWhiteFlag() const;
	DllExport bool isInvertFlag() const;
	DllExport const CvWString getWorstEnemyName() const;
	DllExport ArtStyleTypes getArtStyleType() const;
	DllExport const TCHAR* getUnitButton(UnitTypes eUnit) const;
	void doTurn();
	void doTurnTradeRoutes();
	void doTurnUnits();
	void doEra();

	void doUnitsTravelTimer(); 
	void processReinforcement();
	bool shouldGiveBuilderUnits(int* pIterUnitIndex) const;
	bool hasBuilderUnits() const;
	bool receiveBuilderShip();
	bool receiveBuilderWagon();
	const CvRequirementsNewEra* getRequirementsNewEra(NewEraTypes eNewEra) const;

	void testHumanHasInfluenceToNative();
	// -----------------------------------------
	//
	//    START : DOANE Pirates
	//
	// -----------------------------------------
	void processPirates();
	bool processPirate(CvPirateInfo& kPirateInfo);
	void pirateUpgrade(int iLevel);
	int countNumPirates();
	bool hasEscortUnit();
	CvUnit* receiveUnitFromTheKing(UnitTypes eUnit, const CvString szDiploCommentTextKey);
	CvUnit* addPirate(int iLevel, bool bUpdate = false);
	CvUnit* addPirate(UnitTypes eUnit, bool bUpdate = false);
	CvSeaway* getBestSeawayForPirate() const;
	int getNewWorldYieldQuantity() const;
	// -----------------------------------------
	//
	//    ENd : DOANE Pirates
	//
	// -----------------------------------------

	int calculateProbaImmigration(UnitTypes eUnit, ProfessionTypes eProfession = NO_PROFESSION);
	void processEuropeImmigration();
	CvTempUnit* addImmigrantProposalInEurope(ImmigrationTypes eImmigrationType, UnitTypes eUnit);
	int getGlobalAverageImmigration();
	int getGlobalAverageImmigrationPercent();
	void refreshHistoryForImmigration();
	int getNumHistTurn();
	void refreshUnemploymentStatut();
	void refreshFoodStatut();
	void refreshEconomicStatut();
	void refreshReligionStatut();
	const CvWString weaknessAttraction();
	const CvWString strongPointAttraction();
	const CvWString generalAttraction();
	bool mustShowIconInfo(int iScreen);
	bool highlightNewErasButton();
	void setHighlightNewErasButton(bool bNewValue);

	void generateUsedShipInEurope();
	bool canBuyEuropeNewShip(UnitTypes eUnit) const;
	void verifyCivics();

	void updateYield();
	void updateCityPlotYield();
	void updateCitySight(bool bIncrement);

	void updateTimers();

	DllExport bool hasReadyUnit(bool bAny = false) const;
	DllExport bool hasAutoUnit() const;
	DllExport bool hasBusyUnit() const;
	int calculateScore(bool bFinal = false, bool bVictory = false) const;
	int getScoreTaxFactor() const;
	int findBestFoundValue() const;
	DllExport int upgradeAllPrice(UnitTypes eUpgradeUnit, UnitTypes eFromUnit);
	int countNumCoastalCities() const;
	int countNumCoastalCitiesByArea(CvArea* pArea) const;
	int countTotalCulture() const;
	int countTotalYieldStored(YieldTypes eYield) const;
	int countCityFeatures(FeatureTypes eFeature) const;
	int countNumBuildings(BuildingTypes eBuilding) const;
	DllExport bool canContact(PlayerTypes ePlayer) const;
	void contact(PlayerTypes ePlayer);
	DllExport void handleDiploEvent(DiploEventTypes eDiploEvent, PlayerTypes ePlayer, int iData1, int iData2);
	bool canTradeWith(PlayerTypes eWhoTo) const;
	bool canReceiveTradeCity(PlayerTypes eFromPlayer) const;
	DllExport bool canTradeItem(PlayerTypes eWhoTo, TradeData item, bool bTestDenial = false) const;
	DllExport DenialTypes getTradeDenial(PlayerTypes eWhoTo, TradeData item) const;
	bool isTradingWithTeam(TeamTypes eTeam, bool bIncludeCancelable) const;
	bool canStopTradingWithTeam(TeamTypes eTeam, bool bContinueNotTrading = false) const;
	void stopTradingWithTeam(TeamTypes eTeam);
	void killAllDeals();
	void findNewCapital();
	DllExport bool canRaze(CvCity* pCity, bool autoRaze) const;
	void raze(CvCity* pCity, bool autoRaze);
	void disband(CvCity* pCity);
	bool canReceiveGoody(CvPlot* pPlot, GoodyTypes eGoody, const CvUnit* pUnit) const;
	int receiveGoody(CvPlot* pPlot, GoodyTypes eGoody, CvUnit* pUnit);
	void receiveRandomGoody(CvPlot* pPlot, GoodyTypes eGoody, CvUnit* pUnit);
	void doGoody(CvPlot* pPlot, CvUnit* pUnit);
	DllExport bool canFound(int iX, int iY, bool bTestVisible = false) const;
	void found(int iX, int iY);
	DllExport bool canTrain(UnitTypes eUnit, bool bContinue = false, bool bTestVisible = false, bool bIgnoreCost = false) const;
	bool canConstruct(BuildingTypes eBuilding, bool bContinue = false, bool bTestVisible = false, bool bIgnoreCost = false) const;
	DllExport int getYieldProductionNeeded(UnitTypes eUnit, YieldTypes eYield) const;
	DllExport int getYieldProductionNeeded(BuildingTypes eBuilding, YieldTypes eYield) const;
	int getProductionModifier(UnitTypes eUnit) const;
	int getProductionModifier(BuildingTypes eBuilding) const;
	DllExport int getBuildingClassPrereqBuilding(BuildingTypes eBuilding, BuildingClassTypes ePrereqBuildingClass, int iExtra = 0) const;
	void removeBuildingClass(BuildingClassTypes eBuildingClass);
	void processTrait(TraitTypes eTrait, int iChange);
	int getBuildCost(const CvPlot* pPlot, BuildTypes eBuild) const;
	bool canBuild(const CvPlot* pPlot, BuildTypes eBuild, bool bTestEra = false, bool bTestVisible = false) const;
	RouteTypes getBestRoute(CvPlot* pPlot = NULL) const;
	int getImprovementUpgradeRate() const;
	int calculateTotalYield(YieldTypes eYield) const;
	int calculateTotalRawSellableYield() const;
	void europeGiveShip(bool bAccept);
	int getPlayerValue(YieldTypes eYield, int iAmount) const;
	int getPlayerValue(YieldTypes eYield) const;
	void setPlayerValue(YieldTypes eYield, int iValue);
	void changePlayerValue(YieldTypes eYield, int iChange);
	void processPlayerValue();
	void initYieldsPlayerValue();
	int getRelationTradeTo(PlayerTypes ePlayer) const;
	int getRelationNeighbor(PlayerTypes ePlayer);
 	bool isNearPlayer(PlayerTypes ePlayer) const;
 	int getRegularityTrade(PlayerTypes eIndex) const;
	void setRegularityTrade(PlayerTypes eIndex, int iValue);
	void changeRegularityTrade(PlayerTypes eIndex, int iChange);
	int getUnemploymentHistory(int eIndex) const;
	void setUnemploymentHistory(int eIndex, int iValue);
	int getAverageUnemploymentHistory();
	int getFoodHistory(int eIndex) const;
	void setFoodHistory(int eIndex, int iValue);
	int getAverageFoodHistory();
	int getEconomicHistory(int eIndex) const;
	void setEconomicHistory(int eIndex, int iValue);
	int getAverageEconomicHistory();
	int getReligionHistory(int eIndex) const;
	void setReligionHistory(int eIndex, int iValue);
	int getAverageReligionHistory() const; 

	unsigned char getRandomValues(int eIndex) const;
	void setRandomValues(int eIndex, unsigned char iValue);
	int getRandomValueBetweenRange(int minValue, int maxValue, int eIndex = 0) const;

	unsigned char getUnitGivedByKing(UnitTypes eIndex) const;
	void setUnitGivedByKing(UnitTypes eIndex, unsigned char iValue);
	void changeUnitGivedByKing(UnitTypes eIndex, unsigned char iValue);

	EconomicTrendTypes getEconomicTrend(YieldTypes eIndex) const;
	void setEconomicTrend(YieldTypes eIndex, EconomicTrendTypes eValue);
	unsigned char getEconomicTrendsRemainingTurns(YieldTypes eIndex) const;
	void setEconomicTrendsRemainingTurns(YieldTypes eIndex, unsigned char iValue);
	void changeEconomicTrendsRemainingTurns(YieldTypes eIndex, unsigned char iValue);

	bool getRegularityTradeInTurn(PlayerTypes eIndex) const;
	void setRegularityTradeInTurn(PlayerTypes eIndex, bool bValue);
	void europeGiveGalleon();
	int getRebelPercentForColonies() const;
	void doUnitsRebelSentiment();
	void calculateTotalYields(int aiYields[]) const;
	bool isCivic(CivicTypes eCivic) const;
	bool canDoCivics(CivicTypes eCivic) const;
	DllExport int greatGeneralThreshold() const;
	DllExport int greatAdmiralThreshold() const;
	int immigrationThreshold() const;
	int revolutionEuropeUnitThreshold() const;
	DllExport CvPlot* getStartingPlot() const;
	DllExport void setStartingPlot(CvPlot* pNewValue, bool bUpdateStartDist);
	DllExport int getTotalPopulation() const;
	int getAveragePopulation() const;
	void changeTotalPopulation(int iChange);
	long getRealPopulation() const;
	int getTotalLand() const;
	void changeTotalLand(int iChange);
	int getTotalLandScored() const;
	void changeTotalLandScored(int iChange);
	DllExport int getGold() const;
	DllExport void setGold(int iNewValue);
	DllExport void changeGold(int iChange);
	int getIncomeAndExpendituresRecord() const;
	void setIncomeAndExpendituresRecord(int iNewValue);
	
	unsigned short getWorkImprovementPercent() const;
	void setWorkImprovementPercent(unsigned short usNewValue);
	unsigned short getColonistIntoColoniesAmount() const;
	void setColonistIntoColoniesAmount(unsigned short usNewValue);
	unsigned short getLandForceInsideColoniesAmount() const;
	void setLandForceInsideColoniesAmount(unsigned short usNewValue);
	unsigned short getLandForceOutsideColoniesAmount() const;
	void setLandForceOutsideColoniesAmount(unsigned short usNewValue);
	unsigned short getUnemployedAmount() const;
	void setUnemployedAmount(unsigned short usNewValue);
	unsigned short getScoutAmount() const;
	void setScoutAmount(unsigned short usNewValue);
	unsigned short getAgronomistAmount() const;
	void setAgronomistAmount(unsigned short usNewValue);
	unsigned short getPioneerAmount() const;
	void setPioneerAmount(unsigned short usNewValue);
	unsigned short getMerchantShipAmount() const;
	void setMerchantShipAmount(unsigned short usNewValue);
	unsigned short getMilitaryShipAmount() const;
	void setMilitaryShipAmount(unsigned short usNewValue);

	int getLandForceInsideColoniesExpenditure() const;
	int getLandForceOutsideColoniesExpenditure() const;
	int getUnemployedExpenditure() const;
	int getScoutExpenditure() const;
	int getAgronomistExpenditure() const;
	int getPioneerExpenditure() const;
	int getMerchantShipExpenditure() const;
	int getMilitaryShipExpenditure() const;

	void processIncomeAndExpendituresRecord();
	void updateIncome();
	int getTotalExpenditures() const;
	void updateExpenditures();
	int getTotalIncome() const;
	int calculateColonistIntoColoniesAmount() const;
	int getColonistsIncome() const;
	int getStatemenIncomeIncrease() const;

	DllExport int getNewEra() const;
	NewEraTypes getNextNewEra() const;
	DllExport int getSelectQuantity() const;
	DllExport void setSelectQuantity(int iNewValue);
	DllExport int getEuropeWarehouseCapacity() const;
	DllExport void setEuropeWarehouseCapacity(int iNewValue);
	DllExport int getEuropeLoanPercent() const;
	DllExport void setEuropeLoanPercent(int iNewValue);
	DllExport void doEuropeLandPercent();
	DllExport int getEuropeLoan() const;
	DllExport void setEuropeLoan(int iNewValue);
	DllExport void changeEuropeLoan(int iChange);
	DllExport int getParentMood() const;
	DllExport void setParentMood(int iNewValue);
	DllExport void changeParentMood(int iChange);
	DllExport int getTotalImmigrant() const;
	DllExport void changeTotalImmigrant(int iNewValue);
	DllExport int getAcceptImmigrant() const;
	DllExport void changeAcceptImmigrant(int iNewValue);
	DllExport int getLeaveImmigrant() const;
	DllExport void changeLeaveImmigrant(int iNewValue);
	DllExport int getDistanceToEuropeIA() const;
	DllExport void setDistanceToEuropeIA(int iNewValue);
	DllExport int getRecruitPriceDiscount() const;
	DllExport void setRecruitPriceDiscount(int iNewValue);
	DllExport void changeRecruitPriceDiscount(int iChange);
	DllExport int getRecruitPriceDiscountCross() const;
	int getExtraTradeMultiplier(PlayerTypes eOtherPlayer) const;
	DllExport int getAdvancedStartPoints() const;
	DllExport void setAdvancedStartPoints(int iNewValue);
	DllExport void changeAdvancedStartPoints(int iChange);
	DllExport void doAdvancedStartAction(AdvancedStartActionTypes eAction, int iX, int iY, int iData, bool bAdd);
	DllExport int getAdvancedStartUnitCost(UnitTypes eUnit, bool bAdd, CvPlot* pPlot = NULL);
	DllExport int getAdvancedStartCityCost(bool bAdd, CvPlot* pPlot = NULL);
	DllExport int getAdvancedStartPopCost(bool bAdd, CvCity* pCity = NULL);
	DllExport int getAdvancedStartCultureCost(bool bAdd, CvCity* pCity = NULL);
	DllExport int getAdvancedStartBuildingCost(BuildingTypes eBuilding, bool bAdd, CvCity* pCity = NULL);
	DllExport int getAdvancedStartImprovementCost(ImprovementTypes eImprovement, bool bAdd, CvPlot* pPlot = NULL);
	DllExport int getAdvancedStartRouteCost(RouteTypes eRoute, bool bAdd, CvPlot* pPlot = NULL);
	DllExport int getAdvancedStartVisibilityCost(bool bAdd, CvPlot* pPlot = NULL);

	void createGreatGeneral(UnitTypes eGreatGeneralUnit, bool bIncrementExperience, int iX, int iY);
	int getGreatGeneralsCreated() const;
	void incrementGreatGeneralsCreated();
	int getGreatGeneralsThresholdModifier() const;
	void changeGreatGeneralsThresholdModifier(int iChange);
	int getGreatAdmiralsThresholdModifier() const;
	void changeGreatAdmiralsThresholdModifier(int iChange);
	int getGreatGeneralRateModifier() const;
	void changeGreatGeneralRateModifier(int iChange);
	int getDomesticGreatGeneralRateModifier() const;
	void changeDomesticGreatGeneralRateModifier(int iChange);
	int getImmigrationThresholdMultiplier() const;
	void setImmigrationThresholdMultiplier(int iChange);
	int getRevolutionEuropeUnitThresholdMultiplier() const;
	void setRevolutionEuropeUnitThresholdMultiplier(int iChange);
	int getKingNumUnitMultiplier() const;
	void setKingNumUnitMultiplier(int iChange);
	int getNativeAngerModifier() const;
	void changeNativeAngerModifier(int iChange);
	int getFreeExperience() const;
	void changeFreeExperience(int iChange);
	int getWorkerSpeedModifier() const;
	void changeWorkerSpeedModifier(int iChange);
	int getImprovementUpgradeRateModifier() const;
	void changeImprovementUpgradeRateModifier(int iChange);
	int getMilitaryProductionModifier() const;
	void changeMilitaryProductionModifier(int iChange);
	int getCityDefenseModifier() const;
	void changeCityDefenseModifier(int iChange);
	int getHighestUnitLevel() const;
	void setHighestUnitLevel(int iNewValue);
	int getExpInBorderModifier() const;
	void changeExpInBorderModifier(int iChange);
	int getLevelExperienceModifier() const;
	void changeLevelExperienceModifier(int iChange);
	DllExport CvCity* getCapitalCity() const;
	void setCapitalCity(CvCity* pNewCapitalCity);
	DllExport CvCity* getPrimaryCity() const;
	int getCitiesLost() const;
	void changeCitiesLost(int iChange);
	DllExport int getAssets() const;
	void changeAssets(int iChange);
	DllExport int getPower() const;
	void changePower(int iChange);
	DllExport int getPopScore() const;
	void changePopScore(int iChange);
	DllExport int getLandScore() const;
	void changeLandScore(int iChange);
	
	int getCombatLandExperience() const;
	void setCombatLandExperience(int iExperience);
	void changeCombatLandExperience(int iChange);
	int getCombatSeaExperience() const;
	void setCombatSeaExperience(int iExperience);
	void changeCombatSeaExperience(int iChange);
	
	DllExport bool isConnected() const;
	DllExport int getNetID() const;
	DllExport void setNetID(int iNetID);
	DllExport void sendReminder();

	int getCurrentTradeRouteId() const;
	void setCurrentTradeRouteId(int uiCurrentTradeRouteId, bool bUdateTradeRoutesScreen);
	
	uint getStartTime() const;
	DllExport void setStartTime(uint uiStartTime);
	DllExport uint getTotalTimePlayed() const;
	DllExport bool isAlive() const;
	bool isTerritorialInfluenceMode() const;
	void setTerritorialInfluenceMode(bool bTerritorialInfluence);
	DllExport bool isLaunchFirstPopup() const;
	DllExport void changeLaunchFirstPopup(bool bNewValue);
	DllExport bool isKingGiveGalleon() const;
	DllExport void setKingGiveGalleon(bool bNewValue);
	DllExport bool isKingRappelLoan() const;
	DllExport int getKingRappelLoan() const;
	DllExport void setKingRappelLoan(int iNewValue);
	DllExport bool wasAttackedByPirates() const;
	DllExport void setAttackedByPirates(bool bNewValue);
	DllExport bool isPenalitePlayerSave() const;
	DllExport void setPenalitePlayerSave(bool bNewValue);
	DllExport bool isPenalitePlayerLoad() const;
	DllExport void setPenalitePlayerLoad(bool bNewValue);

	DllExport bool isEverAlive() const;
	void setAlive(bool bNewValue);
	void verifyAlive();
	DllExport bool isTurnActive() const;
	DllExport void setTurnActive(bool bNewValue, bool bDoTurn = true);

	bool isAutoMoves() const;
	DllExport void setAutoMoves(bool bNewValue);
	DllExport void setTurnActiveForPbem(bool bActive);

	DllExport bool isPbemNewTurn() const;
	DllExport void setPbemNewTurn(bool bNew);

	bool isEndTurn() const;
	DllExport void setEndTurn(bool bNewValue);

	DllExport bool isTurnDone() const;
	bool isExtendedGame() const;
	DllExport void makeExtendedGame();
	bool isFoundedFirstCity() const;
	void setFoundedFirstCity(bool bNewValue);
	void calculDistanceCityEurope();
	bool isDisplaySeedling() const;
	void setDisplaySeedling(bool bNewValue);
	DllExport PlayerTypes getID() const;
	DllExport HandicapTypes getHandicapType() const;
	DllExport CivilizationTypes getCivilizationType() const;
	DllExport LeaderHeadTypes getLeaderType() const;
	LeaderHeadTypes getPersonalityType() const;
	void setPersonalityType(LeaderHeadTypes eNewValue);
	DllExport EraTypes getCurrentEra() const;
	void setCurrentEra(EraTypes eNewValue);
	DllExport PlayerTypes getParent() const;
	DllExport void setParent(PlayerTypes eParent);
	DllExport TeamTypes getTeam() const;
	void setTeam(TeamTypes eTeam);
	void updateTeamType();
	YieldTypes getImmigrationConversion() const;
	void setImmigrationConversion(YieldTypes eConversion);
	PlayerRoleTypes getPlayerRole() const;
	void setPlayerRole(PlayerRoleTypes ePlayerRoleType);
	DllExport PlayerColorTypes getPlayerColor() const;
	DllExport int getPlayerTextColorR() const;
	DllExport int getPlayerTextColorG() const;
	DllExport int getPlayerTextColorB() const;
	DllExport int getPlayerTextColorA() const;
	int getSeaPlotYield(YieldTypes eIndex) const;
	void changeSeaPlotYield(YieldTypes eIndex, int iChange);
	int getYieldRateModifier(YieldTypes eIndex) const;
	int getTaxYieldRateModifier(YieldTypes eIndex) const;
	void changeYieldRateModifier(YieldTypes eIndex, int iChange);
	int getCapitalYieldRateModifier(YieldTypes eIndex) const;
	void changeCapitalYieldRateModifier(YieldTypes eIndex, int iChange);
	int getBuildingRequiredYieldModifier(YieldTypes eIndex) const;
	void changeBuildingRequiredYieldModifier(YieldTypes eIndex, int iChange);
	int getCityExtraYield(YieldTypes eIndex) const;
	void updateCityExtraYield(YieldTypes eIndex);
	int getExtraYieldThreshold(YieldTypes eIndex) const;
	void updateExtraYieldThreshold(YieldTypes eIndex);
	int getYieldRate(YieldTypes eIndex) const;
	bool isYieldEuropeTradable(YieldTypes eIndex) const;
	void setYieldEuropeTradable(YieldTypes eIndex, bool bTradeable);
	void setYieldEuropeTradableAll();
	bool hasGeneratedPirates(int index) const;
	void setIfHasGeneratedPirates(int index, bool bNewValue);

	bool isLaunchedPopup(ButtonPopupTypes eButtonPopup) const;
	void setLaunchedPopup(ButtonPopupTypes eButtonPopup, bool bNewValue);
	void clearLaunchedPopup();
	
	bool isHasYieldUnknown(YieldTypes eIndex) const;
	void setHasYieldUnknown(YieldTypes eIndex, bool bValue);
	bool hasPinnedYieldInEurope(YieldTypes eIndex) const;
	void setHasPinnedYieldInEurope(YieldTypes eIndex, bool bValue);
	bool hasAutomaticTradeByYieldWithNative(YieldTypes eIndex) const;
	void setHasAutomaticTradeByYieldWithNative(YieldTypes eIndex, bool bValue);
	void initYieldUnknow();
	void updateYieldUnknowWithOtherEuropeans();
	BonusTypes getNextBetterEuropean() const;
	int getLevelReceiveSeedling() const;
	void changeLevelReceiveSeedling(int iChange);
	
	bool isFeatAccomplished(FeatTypes eIndex) const;
	void setFeatAccomplished(FeatTypes eIndex, bool bNewValue);
	DllExport bool shouldDisplayFeatPopup(FeatTypes eIndex) const;
	DllExport bool isOption(PlayerOptionTypes eIndex) const;
	DllExport void setOption(PlayerOptionTypes eIndex, bool bNewValue);
	bool isSecondaryOption(SecondaryPlayerOptionTypes eIndex) const;
	void setSecondaryOption(SecondaryPlayerOptionTypes eIndex, bool bNewValue);
	DllExport bool isHadTutorialDoane(int eIndex) const;
	DllExport void setHadTutorialDoane(int eIndex, bool bNewValue);
	int getNextUnitMap();
	void deleteUnitMap(int iNumMap);
	void processMapsTeam();
	DllExport bool isPlayable() const;
	DllExport void setPlayable(bool bNewValue);
	int getImprovementCount(ImprovementTypes eIndex) const;
	void changeImprovementCount(ImprovementTypes eIndex, int iChange);
	int getFreeBuildingCount(BuildingTypes eIndex) const;
	bool isBuildingFree(BuildingTypes eIndex) const;
	void changeFreeBuildingCount(BuildingTypes eIndex, int iChange);
	bool isHasFirstBuildingBuilt(BuildingTypes eIndex) const;
	void setHasFirstBuildingBuilt(BuildingTypes eIndex, bool bChange);
	int getUnitClassCount(UnitClassTypes eIndex) const;
	void changeUnitClassCount(UnitClassTypes eIndex, int iChange);
	int getEnrolUnitType(UnitClassTypes eIndex) const;
	void setEnrolUnitType(UnitClassTypes eIndex, int iValue);
	void changeEnrolUnitType(UnitClassTypes eIndex, bool bIncrease, bool bManual = true);
	int getUnitClassMaking(UnitClassTypes eIndex) const;
	void changeUnitClassMaking(UnitClassTypes eIndex, int iChange);
	int getUnitMarketPrice(UnitClassTypes eIndex) const;
	void changeUnitMarketPrice(UnitClassTypes eIndex, int iChange);
	int getUnitKilledCounter(UnitClassTypes eIndex) const;
	void changeUnitKilledCounter(UnitClassTypes eIndex, int iChange);
	void helpOftheKing(UnitClassTypes eIndex);
	int getUnitTurnRemaining(UnitClassTypes eIndex) const;
	void changeUnitTurnRemaining(UnitClassTypes eIndex, int iChange);
	void processNewShipBuyInEurope();
	void processUnitMarketPrice();
	void initYieldPrice();
	void processYieldPrice();
	void processYieldEconomicTrend(YieldTypes eYield);
	EconomicTrendTypes pickEconomicTrend(YieldTypes eYield);
	int getUnitClassCountPlusMaking(UnitClassTypes eIndex) const;
	int getUnitMoveChange(UnitClassTypes eIndex) const;
	DllExport void changeUnitMoveChange(UnitClassTypes eIndex, int iChange);
	int getUnitStrengthModifier(UnitClassTypes eIndex) const;
	DllExport void changeUnitStrengthModifier(UnitClassTypes eIndex, int iChange);
	int getProfessionCombatChange(ProfessionTypes eIndex) const;
	void changeProfessionCombatChange(ProfessionTypes eIndex, int iChange);
	int getProfessionMoveChange(ProfessionTypes eIndex) const;
	void changeProfessionMoveChange(ProfessionTypes eIndex, int iChange);
	int getBuildingClassCount(BuildingClassTypes eIndex) const;
	void changeBuildingClassCount(BuildingClassTypes eIndex, int iChange);
	int getBuildingClassMaking(BuildingClassTypes eIndex) const;
	void changeBuildingClassMaking(BuildingClassTypes eIndex, int iChange);
	int getBuildingClassCountPlusMaking(BuildingClassTypes eIndex) const;
	int getSpecialBuildingNotRequiredCount(SpecialBuildingTypes eIndex) const;
	bool isSpecialBuildingNotRequired(SpecialBuildingTypes eIndex) const;
	void changeSpecialBuildingNotRequiredCount(SpecialBuildingTypes eIndex, int iChange);
	DllExport CivicTypes getCivic(CivicOptionTypes eIndex) const;
	DllExport void setCivic(CivicOptionTypes eIndex, CivicTypes eNewValue);
	int getImprovementYieldChange(ImprovementTypes eIndex1, YieldTypes eIndex2) const;
	void changeImprovementYieldChange(ImprovementTypes eIndex1, YieldTypes eIndex2, int iChange);
	DllExport int getBuildingYieldChange(BuildingClassTypes eBuildingClass, YieldTypes eYield) const;
	void changeBuildingYieldChange(BuildingClassTypes eBuildingClass, YieldTypes eYield, int iChange);
	int getTaxYieldModifierCount(YieldTypes eYield) const;
	void changeTaxYieldModifierCount(YieldTypes eYield, int iChange) const;
	int getEuropeWarehouseYield(YieldTypes eYield) const;
	void changeEuropeWarehouseYield(YieldTypes eYield, int iChange);
	int getEuropeBuyTradeYield(YieldTypes eYield) const;
	void setEuropeBuyTradeYield(YieldTypes eYield, int iChange);
	void changeEuropeBuyTradeYield(YieldTypes eYield, int iValue);
	int getEuropeGoldBuyTradeYield(YieldTypes eYield) const;
	void setEuropeGoldBuyTradeYield(YieldTypes eYield, int iChange);
	void changeEuropeGoldBuyTradeYield(YieldTypes eYield, int iValue);
	int getEuropeSellTradeYield(YieldTypes eYield) const;
	void setEuropeSellTradeYield(YieldTypes eYield, int iValue);
	void changeEuropeSellTradeYield(YieldTypes eYield, int iChange);
	int getEuropeGoldSellTradeYield(YieldTypes eYield) const;
	void setEuropeGoldSellTradeYield(YieldTypes eYield, int iValue);
	void changeEuropeGoldSellTradeYield(YieldTypes eYield, int iChange);

	int getEuropeTaxTradeYield(YieldTypes eYield) const;
	void setEuropeTaxTradeYield(YieldTypes eYield, int iValue);
	
	unsigned char getEuropeNeeds(YieldTypes eYield) const;
	void setEuropeNeeds(YieldTypes eYield, unsigned char iValue);
	void changeEuropeNeeds(YieldTypes eYield, int iValue);
	void initEuropeNeeds();
	
	int getPricePercent(YieldTypes eYield) const;
	int getEuropeConsumption(YieldTypes eYield) const;
	int getEuropeQuantitySuppliedPercent(YieldTypes eYield) const;

	int getIATradeProposition(YieldTypes eYield) const;
	void setIATradeProposition(YieldTypes eYield, int iValue, bool bSync = true);
	int getHumanTradeProposition(YieldTypes eYield) const;
	void setHumanTradeProposition(YieldTypes eYield, int iValue, bool bSync = true);
	int whatKindOfTradeProposal();
	int getNativeGetUnknownYield(YieldTypes eYield) const;
	void setNativeGetUnknownYield(YieldTypes eYield, int iValue);
	int getNumUnknownYield() const;
	int getTotalTradeYields();
	void changeEuropeTaxTradeYield(YieldTypes eYield, int iChange);
	void resetEuropeTradeYield();
	int getBuyPriceForYield(YieldTypes eYield, int iQuantity = 1) const;
	int getSellPriceForYield(YieldTypes eYield, int iQuantity = 1) const;

	void updateGroupCycle(CvUnit* pUnit);
	void removeGroupCycle(int iID);

	CLLNode<int>* deleteGroupCycleNode(CLLNode<int>* pNode);
	CLLNode<int>* nextGroupCycleNode(CLLNode<int>* pNode) const;
	CLLNode<int>* previousGroupCycleNode(CLLNode<int>* pNode) const;
	CLLNode<int>* headGroupCycleNode() const;
	CLLNode<int>* tailGroupCycleNode() const;
	void addCityName(const CvWString& szName);
	int getNumCityNames() const;
	const CvWString& getCityName(int iIndex) const;

	// city iteration
	DllExport CvCity* firstCity(int *pIterIdx, bool bRev=false) const;
	DllExport CvCity* nextCity(int *pIterIdx, bool bRev=false) const;
	DllExport int getNumCities() const;
	DllExport CvCity* getCity(int iID) const;
	CvCity* addCity();
	void deleteCity(int iID);

	// -----------------------------------------
	//
	//    START : DOANE Seaways
	//
	// -----------------------------------------
	int getNumSeaways() const;
	CvSeaway* getSeaway(int iIndex) const;
	CLLNode<CvSeaway>* getNodeSeaway(int iIndex) const;
	CvSeaway* getSeawayByID(int iID) const;
	CLLNode<CvSeaway>* headSeawayNode() const;
	CLLNode<CvSeaway>* nextSeawayNode(CLLNode<CvSeaway>* pNode) const;
	void deleteSeaway(int iIndex);
	void deleteSeawayByID(int iID);
	CvSeaway* findSeaway(CvPlot* pPlot) const;
	int distanceWithTheClosestSeaway(CvPlot* pPlot) const;
	void addSeaway(CvPlot* pPlot, const CvWString& szName);
	CvSeaway* getSeawayNearCity(CvCity* pCity = NULL) const;
	void sendUpNodeSeaway(int iIndex);
	void sendDownNodeSeaway(int iIndex);
	
	// -----------------------------------------
	//
	//    END : DOANE Seaways
	//
	// -----------------------------------------

	// unit iteration
	DllExport CvUnit* firstUnit(int *pIterIdx) const;
	DllExport CvUnit* nextUnit(int *pIterIdx) const;
	DllExport int getNumUnits() const;
	DllExport CvUnit* getUnit(int iID) const;
	CvUnit* addUnit();
	void addExistingUnit(CvUnit *pUnit);
	void deleteUnit(int iID);
	CvUnit* getAndRemoveUnit(int iId);

	int getNumEuropeUnitsWithoutSailors() const;
	int getNumEuropeUnits() const;
	CvUnit* getEuropeUnit(int iIndex) const;
	DllExport CvUnit* getEuropeUnitById(int iId) const;
	void loadUnitFromEurope(CvUnit* pUnit, CvUnit* pTransport);
	void killUnitFromEurope(CvUnit* pUnit);
	void unloadUnitToEurope(CvUnit* pUnit);
	void transferUnitInEurope(CvUnit* pUnit, CvUnit* pTransport);
	void doREFReduction(int iGold);

	int countNumTravelUnits(UnitTravelStates eState, DomainTypes eDomain) const;
	int countNumDomainUnits(DomainTypes eDomain) const;

	// selection groups iteration
	DllExport CvSelectionGroup* firstSelectionGroup(int *pIterIdx, bool bRev=false) const;
	DllExport CvSelectionGroup* nextSelectionGroup(int *pIterIdx, bool bRev=false) const;
	int getNumSelectionGroups() const;
	CvSelectionGroup* getSelectionGroup(int iID) const;
	CvSelectionGroup* addSelectionGroup();
	void deleteSelectionGroup(int iID);
	// pending triggers iteration
	EventTriggeredData* firstEventTriggered(int *pIterIdx, bool bRev=false) const;
	EventTriggeredData* nextEventTriggered(int *pIterIdx, bool bRev=false) const;
	int getNumEventsTriggered() const;
	EventTriggeredData* getEventTriggered(int iID) const;
	EventTriggeredData* addEventTriggered();
	void deleteEventTriggered(int iID);
	EventTriggeredData* initTriggeredData(EventTriggerTypes eEventTrigger, bool bFire = false, int iCityId = -1, int iPlotX = INVALID_PLOT_COORD, int iPlotY = INVALID_PLOT_COORD, PlayerTypes eOtherPlayer = NO_PLAYER, int iOtherPlayerCityId = -1, int iUnitId = -1, BuildingTypes eBuilding = NO_BUILDING);
	int getEventTriggerWeight(EventTriggerTypes eTrigger) const;
	DllExport void addMessage(const CvTalkingHeadMessage& message);
	void showMissedMessages();
	void clearMessages();
	DllExport const CvMessageQueue& getGameMessages() const;
	DllExport void expireMessages();

	DllExport void addPopup(CvPopupInfo* pInfo, bool bFront = false);
	void clearPopups();
	DllExport CvPopupInfo* popFrontPopup();
	DllExport const CvPopupQueue& getPopups() const;
	DllExport void addDiplomacy(CvDiploParameters* pDiplo);
	void clearDiplomacy();
	DllExport const CvDiploQueue& getDiplomacy() const;
	DllExport CvDiploParameters* popFrontDiplomacy();

	int getScoreHistory(int iTurn) const;
	void updateScoreHistory(int iTurn, int iBestScore);

	int getEconomyHistory(int iTurn) const;
	void updateEconomyHistory();
	int getIndustryHistory(int iTurn) const;
	void updateIndustryHistory(int iTurn, int iBestIndustry);
	int getAgricultureHistory(int iTurn) const;
	void updateAgricultureHistory(int iTurn, int iBestAgriculture);
	
	int getLandMilitaryHistory(int iTurn) const;
	int getSeaMilitaryHistory(int iTurn) const;
	int getNavalMerchantHistory(int iTurn) const;
	void updateUnitsHistory();
	
	int getCultureHistory(int iTurn) const;
	void updateCultureHistory(int iTurn, int iBestCulture);

	int getTotalImmigration(UnitTypes eUnit) const;
	void incrementTotalImmigration(UnitTypes eUnit);
	int getTotalImmigrationAsked(UnitTypes eUnit) const;
	void incrementTotalImmigrationAsked(UnitTypes eUnit);
	int getTurnCountWithoutProposition(UnitTypes eUnit) const;
	void incrementTurnCountWithoutProposition(UnitTypes eUnit);
	void resetTurnCountWithoutProposition(UnitTypes eUnit);
	int getMaxTurnWithoutProposition(UnitTypes eUnit) const;
	void udpateMaxTurnCountWithoutPropositionIfNeeded(UnitTypes eUnit);
	float getCurrentImmigrationProbability(UnitTypes eUnit) const;
	void udpateCurrentImmigrationProbability(UnitTypes eUnit, float iProbability);
	void resetCurrentImmigrationProbability(UnitTypes eUnit);
	float getFirstUnitImmigrationProbability(UnitTypes eUnit) const;
	void udpateFirstUnitImmigrationProbabilityIfNeeded(UnitTypes eUnit);
	float getAverageImmigrationProbability(UnitTypes eUnit) const;
	void udpateAverageImmigrationProbabilityIfNeeded(UnitTypes eUnit);

	int getImmigrationHistory(int iTurn, UnitTypes eUnit) const;
	void doImmigrationHistory();
	void newImmigrant(UnitTypes eUnit);

	int addAgreement(const IDInfo& kSource, const IDInfo& kDestination, CvAgreement* pOriginalAgreement = NULL);
	bool removeAgreement(int iId, int iCase = 1);
	void removeAllAgreementOfTeam(TeamTypes eTradeTeam);
	void checkAgreementsDeadline();
	CvAgreement* getAgreement(int iId) const;
	int getNumAgreements() const;
	void getAgreements(std::vector<CvAgreement*>& aAgreements) const;
	void getAgreementsOfGroup(std::vector<CvAgreement*>& aAgreements, int iGroupID) const;
	int getAgreementOfCity(CvCity* pCity);
	CvAgreement* firstAgreement(int *pIterIdx) const;
	CvAgreement* nextAgreement(int *pIterIdx) const;
	void negociateAgreement(CvUnit* pUnit, CvCity* pCity, int iChoice, int iIncrease);
	int getNumWantedValue(int askedValue, YieldTypes eWantedYield, int iIncreasePercent) const;
	void endOfAgreement(int iAgreementID, int iPercent, int iCase);
	int getNumAgreement() const;
	void changeNumAgreement(int iChange);

	CvTempUnit* firstTempUnit(int *pIterIdx) const;
	CvTempUnit* nextTempUnit(int *pIterIdx) const;
	CvTempUnit* addTempUnit(UnitTypes eUnit);
	bool removeTempUnit(int iId);
	CvTempUnit* getTempUnit(int iId) const;
	int getNumTempUnits() const;
	void getTempUnits(std::vector<CvTempUnit*>& aTempUnits) const;
	
	void initHospitals();
	CvHospitals* getHospitals() const;
	void doHospitals();

	CvTradeRoute* addTradeRoute(const IDInfo& kSource, const IDInfo& kDestination);
	bool removeTradeRoute(int iId, bool bUdateTradeRoutesScreen = true);
	CvTradeRoute* getTradeRoute(int iId) const;
	int getNumTradeRoutes() const;
	void getTradeRoutes(std::vector<CvTradeRoute*>& aTradeRoutes) const;
	CvTradeRoute* firstTradeRoute(int *pIterIdx) const;
	CvTradeRoute* nextTradeRoute(int *pIterIdx) const;

	bool canLoadYield(PlayerTypes eCityPlayer) const;
	bool canUnloadYield(PlayerTypes eCityPlayer) const;

	// Script data needs to be a narrow string for pickling in Python
	std::string getScriptData() const;
	void setScriptData(std::string szNewValue);
	DllExport const CvString& getPbemEmailAddress() const;
	DllExport void setPbemEmailAddress(const char* szAddress);
	DllExport const CvString& getSmtpHost() const;
	DllExport void setSmtpHost(const char* szHost);
	const EventTriggeredData* getEventOccured(EventTypes eEvent) const;
	bool isTriggerFired(EventTriggerTypes eEventTrigger) const;
	void setEventOccured(EventTypes eEvent, const EventTriggeredData& kEventTriggered, bool bOthers = true);
	void resetEventOccured(EventTypes eEvent, bool bAnnounce = true);
	void setTriggerFired(const EventTriggeredData& kTriggeredData, bool bOthers = true, bool bAnnounce = true);
	void resetTriggerFired(EventTriggerTypes eEventTrigger);
	void trigger(EventTriggerTypes eEventTrigger);
	void trigger(const EventTriggeredData& kData);
	DllExport void applyEvent(EventTypes eEvent, int iTriggeredId, bool bUpdateTrigger = true);
	bool canDoEvent(EventTypes eEvent, const EventTriggeredData& kTriggeredData) const;
	int getEventCost(EventTypes eEvent, PlayerTypes eOtherPlayer, bool bRandom) const;
	bool canTrigger(EventTriggerTypes eTrigger, PlayerTypes ePlayer) const;
	const EventTriggeredData* getEventCountdown(EventTypes eEvent) const;
	void setEventCountdown(EventTypes eEvent, const EventTriggeredData& kEventTriggered);
	void resetEventCountdown(EventTypes eEvent);

	bool isFreePromotion(UnitCombatTypes eUnitCombat, PromotionTypes ePromotion) const;
	void setFreePromotion(UnitCombatTypes eUnitCombat, PromotionTypes ePromotion, bool bFree);
	bool isFreePromotion(UnitClassTypes eUnitCombat, PromotionTypes ePromotion) const;
	void setFreePromotion(UnitClassTypes eUnitCombat, PromotionTypes ePromotion, bool bFree);

	void invalidatePopulationRankCache();
	void invalidateYieldRankCache(YieldTypes eYield = NO_YIELD);

	PlayerTypes pickConqueredCityOwner(const CvCity& kCity) const;
	void forcePeace(PlayerTypes ePlayer);
	bool canSpiesEnterBorders(PlayerTypes ePlayer) const;
	int getNewCityProductionValue() const;

	int getGrowthThreshold(int iPopulation) const;

	void verifyUnitStacksValid();

	DllExport void buildTradeTable(PlayerTypes eOtherPlayer, CLinkList<TradeData>& ourList, const IDInfo& kTransport) const;
	DllExport bool getHeadingTradeString(PlayerTypes eOtherPlayer, TradeableItems eItem, CvWString& szString, CvString& szIcon) const;
	DllExport bool getItemTradeString(PlayerTypes eOtherPlayer, bool bOffer, bool bShowingCurrent, const TradeData& zTradeData, const IDInfo& kTransport, CvWString& szString, CvString& szIcon) const;
	DllExport void updateTradeList(PlayerTypes eOtherPlayer, CLinkList<TradeData>& ourInventory, const CLinkList<TradeData>& ourOffer, const CLinkList<TradeData>& theirOffer, const IDInfo& kTransport) const;
	DllExport int getMaxGoldTrade(PlayerTypes eOtherPlayer, const IDInfo& kTransport) const;

	UnitTypes getIdealUnitTypeForYield(YieldTypes eYield) const;

	CvCity* getPopulationUnitCity(int iUnitId) const;
	int getAcceptSeedlingTurn(PlayerTypes ePlayer) const;
	int getCrossesStored() const;
	void changeCrossesStored(int iChange);
	int getBellsStored() const;
	void changeBellsStored(int iChange);
	int getTaxRate() const;
	void setTaxRate(int iValue);
	void changeTaxRate(int iChange);
	int getLastTurnOfTaxesIncrease() const;
	void setLastTurnOfTaxesIncrease(int iValue);
	int getNativeCombatModifier() const;
	void setNativeCombatModifier(int iValue);
	void changeNativeCombatModifier(int iChange);
	int getDominateNativeBordersCount() const;
	void changeDominateNativeBordersCount(int iChange);
	int getRevolutionEuropeTradeCount() const;
	void changeRevolutionEuropeTradeCount(int iChange);
	bool canTradeWithEurope() const;
	void interceptEuropeUnits();

	void transfertEuropeToWarehouse(YieldTypes eYield, int iCargoValue);

	bool hasEnoughCitizenByColony(int iNumCities) const;
	bool hasEnoughDefendersByColony(int iNumDefenders) const;

	void kingGiveScout();
	bool haveAlreadySawSeeds() const;
	
	int getYieldSellPrice(YieldTypes eYield) const;
	int getYieldBuyPrice(YieldTypes eYield) const;
	int getYieldDecreasePriceEraChange(YieldTypes eYield) const;
	int getYieldRandomPercent(YieldTypes eYield) const;
	void setYieldRandomPercent(YieldTypes eYield, int iPrice);
	void sellYieldUnitToEurope(CvUnit* pUnit, int iAmount, int iCommission, YieldTypes eYield);
	bool buyYieldUnitFromEurope(YieldTypes eYield, int iAmount, CvUnit* pTransport);
	int getEuropeUnitBuyPrice(UnitTypes eUnit) const;
	CvUnit* buyEuropeUnit(UnitTypes eUnit, int iPriceModifier);
	void buyUnitsFromKing();
	int getYieldTradedTotal(YieldTypes eYield) const;
	void setYieldTradedTotal(YieldTypes eYield, int iValue);
	void changeYieldTradedTotal(YieldTypes eYield, int iChange);
	int getYieldBoughtTotal(YieldTypes eYield) const;
	void setYieldBoughtTotal(YieldTypes eYield, int iValue);
	void changeYieldBoughtTotal(YieldTypes eYield, int iChange);
	YieldTypes getHighestTradedYield() const;
	int getHighestStoredYieldCityId(YieldTypes eYield) const;

	DllExport void doAction(PlayerActionTypes eAction, int iData1, int iData2, int iData3);
	void ejectCitizen(CvCity* pCity);

	CvSelectionGroup* bestGroupForWaiting(CvSelectionGroup* pSourceGroup, CvSelectionGroup* pDestinationGroup);
	void kingHelpAIToDefendShips(UnitTypes eUnit, int iProbaWarShip);
	DllExport int getTradeYieldAmount(YieldTypes eYield) const;
	DllExport void setCityBillboardDirty(bool bNewValue);
	DllExport bool isInRevolution() const;
	bool checkIndependence() const;

	void applyMissionaryPoints(CvCity* pCity);
	int getMissionaryPoints(PlayerTypes ePlayer) const;
	void changeMissionaryPoints(PlayerTypes ePlayer, int iChange);
	int getMissionaryThresholdMultiplier(PlayerTypes ePlayer) const;
	void setMissionaryThresholdMultiplier(PlayerTypes ePlayer, int iValue);
	int missionaryThreshold(PlayerTypes ePlayer) const;
	void burnMissions(PlayerTypes ePlayer);
	bool canHaveMission(PlayerTypes ePlayer) const;
	void validateMissions();
	int getMissionaryRateModifier() const;
	void changeMissionaryRateModifier(int iChange);
	int getMissionarySuccessPercent() const;
	void setMissionarySuccessPercent(int iValue);

	int getAttackFocusEuropeanPlayer() const;
	void setAttackFocusEuropeanPlayer(int iValue);

	short getImmigrationRelationPoints() const;
	void changeImmigrationRelationPoints(short sValue);
	void processImmigrationRelationPoints();
	short getImmigrationRelationPointRequired(UnitTypes eUnit);

	int getRebelCombatPercent() const;

	int getProfessionEquipmentModifier(ProfessionTypes eProfession) const;
	void setProfessionEquipmentModifier(ProfessionTypes eProfession, int iValue);
	int getYieldEquipmentAmount(ProfessionTypes eProfession, YieldTypes eYield) const;
	bool isProfessionValid(ProfessionTypes eProfession, UnitTypes eUnit) const;
	void changeProfessionEurope(int iUnitId, ProfessionTypes eProfession, int iMode);

	int getNumRevolutionEuropeUnits() const;
	UnitTypes getRevolutionEuropeUnit(int i) const;
	ProfessionTypes getRevolutionEuropeProfession(int i) const;
	void addRevolutionEuropeUnit(UnitTypes eUnit, ProfessionTypes eProfession);
	void clearRevolutionEuropeUnits();

	UnitTypes pickBestImmigrant();
	UnitTypes pickFreeUnit();
	int getNumNeededUnitClass(UnitClassTypes eUnitClass) const;
	CvCity* getBestWorkingCity(UnitClassTypes eUnitClass, bool bForceResult = false) const;

	void buyLand(CvPlot* pPlot, bool bFree);

	int getNumTradeMessages() const;
	const wchar* getTradeMessage(int i) const;
	void addTradeMessage(CvWString& szString);

	int getNumEuropeTradeMessages() const;
	const wchar* getEuropeTradeMessage(int i) const;
	void addEuropeTradeMessage(CvWString& szString);

	int getNumEventEuropeMessages() const;
	const wchar* getEventEuropeMessage(int i) const;

	// -----------------------------------------
	//
	//    START : DOANE Player Action functions
	//
	// -----------------------------------------
	void destroyCity(CvCity* pCity);
	void giveExpToUnit(CvUnit* pUnit, int iExp);
	void repayLoan(int iGold);
	void arsenalManagement(CvCity* pCity, YieldTypes eYieldPicked);
	void buyMissingEquipement(CvUnit* pShipUnit);
	void unloadAllInEurope(CvUnit* pShipUnit);
	void resupplyBuilderPack(CvUnit* pUnit, bool bLoad);
	void processTradeProposition(CvUnit* pUnit, int iButtonClicked);
	void addSeawayFromUnit(CvUnit* pUnit);

	void addNewAgreement(CvUnit* pUnit, CvCity* pCitySource);
	void addNewAgreementPart1(CvUnit* pUnit, int id, int iChoice);
	void addNewAgreementPart2(CvUnit* pUnit, int iChoice, int iStartIncrease);
	void changeCityAgremment(CvAgreement* pAgreement, CvCity* pDestCity);
	void chooseAgreement(CvUnit* pUnit, int iAgreementID, bool bAssign);
	void chooseTradeRoute(CvUnit* pUnit, int iTradeRouteID, bool bAssign);

	void recruitCrew(CvTempUnit* pTempUnit, int iGold);
	void recruitImmigrant(CvTempUnit* pTempUnit, int iGold);
	void buyUsedShip(CvTempUnit* pTempUnit, int iGold);
	void sellShip(CvUnit* pUnit, int iGold);

	void loadCrew(CvUnit* pShipUnit, int iID, bool bEurope);
	void loadCrew(CvUnit* pShipUnit, CvUnit* pSailorUnit);
	void repairShip(CvUnit* pUnit, bool bIncludeGroup);
	void buyNewShipInEurope(UnitTypes eUnit, int iTurn, int iGold);
	void resupplyEuropeUnit(CvUnit* pUnit);
	void ejectUnitToGroup(CvUnit* pUnit, CvUnit* pCurrentUnit);

	void shipJoinGroup(CvUnit* pUnit, CvUnit* pHeadUnit);

	void transfertMap(CvUnit* pSender, CvUnit* pReceiver);
	void removeResource(CvUnit* pUnit);

	void robUnitCrew(CvUnit* pUnit, int iGold);
	void robUnitPassengers(CvUnit* pUnit, CvUnit* pAttackUnit, int iGold);
	void robUnitCargo(CvUnit* pUnit, YieldTypes eYield, int iQuantity);
	void robUnitEnd(CvUnit* pUnit, CvUnit* pAttackUnit);

	void manageHospitals(CvCity* pCity, int iAmount);
	void manageHospitalsBis(CvCity* pCity, int iConsumption, bool bAuto);
	
	void launchPlayerActionPopup(ButtonPopupTypes eButtonPopup, int iData1, int iData2);
	int getLocalPlayerActionPopupData3();
	void setLocalPlayerActionPopupData3(int iData3);
	
	void processPurchaseBid(CvUnit* pUnit, int iGold);
	void trainCrewInEurope(CvTempUnit* pTempUnit, int iMode);
	void enrolUnitType(UnitClassTypes eUnitClassType, bool bIncrease);
	void makeTheFirstSeaway(CvUnit* pUnit, CvCity* pCity, int iButtonClicked);
	void moveSeawayRank(int iIndex, int iState);

	void kingGiveAgronomist(BonusTypes eBonus);
	void changeDisplaySeedling();
	void swapSailors(CvUnit* pSourceUnit, CvUnit* pDestUnit);
	
	CvTradeRoute* createTradeRoute(int iSourceID, int iDestinationID);
	void changeTradeType(CvTradeRoute* pTradeRoute, TradeTypes eTrade, YieldTypes eYield);
	void setTradeRouteRadioOption(CvTradeRoute* pTradeRoute, TradeRouteRadioTypes eRadio, YieldTypes eYield);
	void setTradeRouteRadioOptionQuantity(CvTradeRoute* pTradeRoute, YieldTypes eYield, int iQuantity);
	void setTradeRouteEuropeanRadioOption(CvTradeRoute* pTradeRoute, TradeRouteRadioEuropeanTypes eRadio, YieldTypes eYield);
	void setTradeRouteRadioEuropeanOptionQuantity(YieldTypes eYield, TradeRouteRadioEuropeanTypes eRadio, int iQuantity);
	void setTradeRouteCheckBoxOption(CvTradeRoute* pTradeRoute, TradeRouteCheckBoxTypes eCheckBox, YieldTypes eYield, bool bActivate);
	void setTradeRouteCheckBoxOptionQuantity(TradeRouteCheckBoxTypes eCheckBox, YieldTypes eYield, int iQuantity);
	void setDestinationCityToUnit(CvUnit* pUnit, CvCity* pCity);
	void moveTransportToTransport(CvUnit* pSourceUnit, CvUnit* pDestinationUnit);
	void selectSeawayForGroup(CvSelectionGroup* pGroup, int iSeawayId);
	void setTradeRouteCheckBoxOptionGroup(CvSelectionGroup* pGroup, TradeRouteCheckBoxGroupsTypes eType, bool bNewValue);
	void setStartingSpecialist(UnitTypes eUnit, CvUnit* pTransport=NULL, CvUnit* pUnitToReplace=NULL);
	CvTempUnit* useRelationPointsForImmigration(UnitTypes eUnit);
	void changeMinGoldToConserveForTradeRoute(int tradeRouteId, int iNewValue);

	void permuteDebugMode();
	void sendPlayerOption(PlayerOptionTypes ePlayerOption, bool bNewValue);
	// -----------------------------------------
	//
	//    END : DOANE Player Action functions
	//
	// -----------------------------------------
	
	int getNumCityJobs() const;
	CvCityJob* getCityJob(int iID) const;
	CvCityJob* addCityJob();

	int getNumMarketTransactions() const;
	CvMarketTransaction* getMarketTransaction(int iID) const;
	CvMarketTransaction* getMarketTransaction(YieldTypes eYield, PlayerTypes eToPlayer) const;
	void initMarketTransactions();

	void haveNewEra(NewEraTypes eNewEra, bool hasReachedMaxTurnRequirementThanksToAnotherEuropean);
	void upgradeMilitaryProfessions();
	int getNumAgreementProposition(PlayerTypes eEuropenPlayerType) const;
	int getAgreementQuantity(int iProposition) const;
	int getAgreementMaxTurn(int iProposition) const;
	UnitTypes getCheapestShip() const;
	void processYieldTransactions(std::vector<PlayerTypes>& aliveEuropePlayer, YieldTypes eYield);
	void saveYieldTransactions(std::vector<PlayerTypes>& aliveEuropePlayer, YieldTypes eYield, int iExceed = -1, int iTradePercent = -1);
	void applyYieldsTransactions();
	int isMarketTransactionBlocked(YieldTypes eYield, PlayerTypes eToPlayer) const;
	int getEuropeTradeAmount(YieldTypes eYield, PlayerTypes eToPlayer) const;
	int getEuropeTradeTotalAmount(YieldTypes eYield, PlayerTypes eToPlayer) const;

	CvConvoy* addConvoy();
	bool removeConvoy(int iId);
	CvConvoy* getConvoy(int iId) const;
	int getNumConvoys() const;
	void getConvoys(std::vector<CvConvoy*>& aConvoys) const;
	CvConvoy* firstConvoy(int *pIterIdx) const;
	CvConvoy* nextConvoy(int *pIterIdx) const;

	virtual void AI_init() = 0;
	virtual void AI_reset() = 0;
	virtual void AI_doTurnPre() = 0;
	virtual void AI_doTurnPost() = 0;
	virtual void AI_doTurnUnitsPre() = 0;
	virtual void AI_doTurnUnitsPost() = 0;
	virtual void AI_updateFoundValues(bool bStartingLoc = false) = 0;
	virtual void AI_unitUpdate() = 0;
	virtual void AI_makeAssignWorkDirty() = 0;
	virtual void AI_assignWorkingPlots() = 0;
	virtual void AI_updateAssignWork() = 0;
	virtual void AI_makeProductionDirty() = 0;
	virtual void AI_conquerCity(CvCity* pCity) = 0;
	virtual int AI_foundValue(int iX, int iY, int iMinUnitRange = -1, bool bStartingLoc = false) = 0;
	virtual int AI_getPlotDanger(CvPlot* pPlot, int iRange = -1, bool bTestMoves = true, bool bOffensive = false) = 0;
	virtual bool AI_isWillingToTalk(PlayerTypes ePlayer) = 0;
	virtual bool AI_demandRebukedSneak(PlayerTypes ePlayer) = 0;
	virtual bool AI_demandRebukedWar(PlayerTypes ePlayer) = 0;
	virtual AttitudeTypes AI_getAttitude(PlayerTypes ePlayer, bool bForced = true) = 0;
	virtual int AI_dealVal(PlayerTypes ePlayer, const CLinkList<TradeData>* pList, bool bIgnoreAnnual = false, int iExtra = 0) = 0;
	virtual bool AI_considerOffer(PlayerTypes ePlayer, const CLinkList<TradeData>* pTheirList, const CLinkList<TradeData>* pOurList, int iChange = 1) = 0;
	virtual int AI_militaryHelp(PlayerTypes ePlayer, int& iNumUnits, UnitTypes& eUnit, ProfessionTypes& eProfession) = 0;
	virtual int AI_maxGoldTrade(PlayerTypes ePlayer) const = 0;
	virtual bool AI_counterPropose(PlayerTypes ePlayer, const CLinkList<TradeData>* pTheirList, const CLinkList<TradeData>* pOurList, CLinkList<TradeData>* pTheirInventory, CLinkList<TradeData>* pOurInventory, CLinkList<TradeData>* pTheirCounter, CLinkList<TradeData>* pOurCounter, const IDInfo& kTransport) = 0;
	virtual int AI_cityTradeVal(CvCity* pCity, PlayerTypes eOwner = NO_PLAYER) = 0;
	virtual DenialTypes AI_cityTrade(CvCity* pCity, PlayerTypes ePlayer) const = 0;
	virtual DenialTypes AI_stopTradingTrade(TeamTypes eTradeTeam, PlayerTypes ePlayer) const = 0;
	virtual DenialTypes AI_yieldTrade(YieldTypes eYield, const IDInfo& kTransport, PlayerTypes ePlayer) const = 0;	
	virtual int AI_unitValue(UnitTypes eUnit, UnitAITypes eUnitAI, CvArea* pArea) = 0;
	virtual int AI_totalUnitAIs(UnitAITypes eUnitAI) = 0;
	virtual int AI_totalAreaUnitAIs(CvArea* pArea, UnitAITypes eUnitAI) = 0;
	virtual int AI_totalWaterAreaUnitAIs(CvArea* pArea, UnitAITypes eUnitAI) = 0;
	virtual int AI_plotTargetMissionAIs(CvPlot* pPlot, MissionAITypes eMissionAI, CvSelectionGroup* pSkipSelectionGroup = NULL, int iRange = 0) = 0;
	virtual int AI_unitTargetMissionAIs(CvUnit* pUnit, MissionAITypes eMissionAI, CvSelectionGroup* pSkipSelectionGroup = NULL) = 0;
	virtual int AI_civicValue(CivicTypes eCivic) = 0;
	virtual int AI_getNumAIUnits(UnitAITypes eIndex) = 0;
	virtual void AI_changePeacetimeTradeValue(PlayerTypes eIndex, int iChange) = 0;
	virtual void AI_changePeacetimeGrantValue(PlayerTypes eIndex, int iChange) = 0;
	virtual int AI_getAttitudeExtra(PlayerTypes eIndex) = 0;
	virtual void AI_setAttitudeExtra(PlayerTypes eIndex, int iNewValue) = 0;
	virtual void AI_changeAttitudeExtra(PlayerTypes eIndex, int iChange) = 0;
	virtual void AI_setFirstContact(PlayerTypes eIndex, bool bNewValue) = 0;
	virtual int AI_getMemoryCount(PlayerTypes eIndex1, MemoryTypes eIndex2) const = 0;
	virtual void AI_changeMemoryCount(PlayerTypes eIndex1, MemoryTypes eIndex2, int iChange) = 0;
	virtual EventTypes AI_chooseEvent(int iTriggeredId) = 0;
	virtual void AI_doAdvancedStart(bool bNoExit = false) = 0;
	virtual int AI_getExtraGoldTarget() const = 0;
	virtual void AI_setExtraGoldTarget(int iNewValue) = 0;
	virtual void AI_chooseCivic(CivicOptionTypes eCivicOption) = 0;
	virtual bool AI_chooseGoody(GoodyTypes eGoody) = 0;
	virtual CvCity* AI_findBestCity() const = 0;
	virtual CvCity* AI_findBestPort() const = 0;

	bool checkPopulation() const;
	bool checkPower(bool bReset);

protected:

	int m_iStartingX;
	int m_iStartingY;
	int m_iTotalPopulation;
	int m_iTotalLand;
	int m_iTotalLandScored;
	int m_iGold;
	int m_iIncomeAndExpendituresRecord;
	int m_iNbCross;
	int m_iNewEra;
	int m_iSelectQuantity;	
	int m_iNumAgreement;
	int m_iLevelReceiveSeedling;
	int m_iEuropeWarehouseCapacity;
	int m_iLocalPlayerActionPopupData3;
	int m_iEuropeLoanPercent;
	int m_iEuropeLoan;
	int m_iParentMood;
	int m_iTotalImmigrant;
	int m_iAcceptImmigrant;
	int m_iLeaveImmigrant;
	int m_iDistanceToEuropeIA;
	int m_iAdvancedStartPoints;
	int m_iGreatGeneralsCreated;
	int m_iGreatGeneralsThresholdModifier;
	int m_iGreatAdmiralsThresholdModifier;
	int m_iGreatGeneralRateModifier;
	int m_iDomesticGreatGeneralRateModifier;
	int m_iImmigrationThresholdMultiplier;
	int m_iRevolutionEuropeUnitThresholdMultiplier;
	int m_iKingNumUnitMultiplier;
	int m_iNativeAngerModifier;
	int m_iFreeExperience;
	int m_iWorkerSpeedModifier;
	int m_iImprovementUpgradeRateModifier;
	int m_iMilitaryProductionModifier;
	int m_iCityDefenseModifier;
	int m_iHighestUnitLevel;
	int m_iExpInBorderModifier;
	int m_iLevelExperienceModifier;
	int m_iCapitalCityID;
	int m_iCitiesLost;
	int m_iAssets;
	int m_iPower;
	int m_iPopulationScore;
	int m_iLandScore;
	int m_iCombatLandExperience;
	int m_iCombatSeaExperience;
	int m_iCrossesStored;
	int m_iBellsStored;
	int m_iTaxRate;
	int m_iLastTurnOfTaxesIncrease;
	int m_iNativeCombatModifier;
	int m_iDominateNativeBordersCount;
	int m_iRevolutionEuropeTradeCount;
	int m_iMissionaryRateModifier;
	int m_iMissionarySuccessPercent;
	int m_iAttackFocusEuropeanPlayer;
	int m_iCurrentTradeRouteId;  // Don't save this

	short m_sImmigrationRelationPoints;
	
	unsigned short m_usWorkImprovementPercent;
	unsigned short m_usColonistIntoColoniesAmount;
	unsigned short m_usLandForceInsideColoniesAmount;
	unsigned short m_usLandForceOutsideColoniesAmount;
	unsigned short m_usUnemployedAmount;
	unsigned short m_usScoutAmount;
	unsigned short m_usAgronomistAmount;
	unsigned short m_usPioneerAmount;
	unsigned short m_usMerchantShipAmount;
	unsigned short m_usMilitaryShipAmount;

	uint m_uiStartTime;  // XXX save these?

	bool m_bAlive;
	bool m_bTerritorialInfluenceMode;
	bool m_bLaunchFirstPopup;
	bool m_bKingGiveGalleon;
	bool m_bHighlightNewErasButton;
	bool m_bKingRappelLoan;
	bool m_bAttackedByPirates;
	bool m_bPenalitePlayerSave;
	bool m_bPenalitePlayerLoad;
	bool m_bEverAlive;
	bool m_bTurnActive;
	bool m_bAutoMoves;
	bool m_bEndTurn;
	bool m_bPbemNewTurn;
	bool m_bExtendedGame;
	bool m_bFoundedFirstCity;
	bool m_bStrike;
	bool m_bDisplaySeedling;
	bool m_bHuman;

	PlayerTypes m_eID;
	LeaderHeadTypes m_ePersonalityType;
	EraTypes m_eCurrentEra;
	PlayerTypes m_eParent;
	TeamTypes m_eTeamType;
	YieldTypes m_eImmigrationConversion;
	PlayerRoleTypes m_ePlayerRole;

	int* m_aiSeaPlotYield;
	int* m_aiYieldRateModifier;
	int* m_aiCapitalYieldRateModifier;
	int* m_aiBuildingRequiredYieldModifier;
	int* m_aiCityExtraYield;
	int* m_aiExtraYieldThreshold;
	int* m_aiYieldRandomPercent;
	int* m_aiYieldTradedTotal;
	int* m_aiYieldBoughtTotal;
	int* m_aiTaxYieldModifierCount;

	int* m_aiEuropeWarehouseYield;
	int* m_aiEuropeSellTradeYield;
	int* m_aiEuropeGoldSellTradeYield;
	int* m_aiEuropeBuyTradeYield;
	int* m_aiEuropeGoldBuyTradeYield;
	int* m_aiEuropeTaxTradeYield;
	unsigned char* m_aucEuropeNeeds;

	int* m_aiNativeGetUnknownYield;
	int* m_aiIATradeProposition;
	int* m_aiHumanTradeProposition;
	int* m_aiPlayerValue;

	bool* m_abYieldEuropeTradable;
	bool* m_abYieldUnknown;
	bool* m_abPinnedYieldInEurope;
	bool* m_abAutomaticTradeByYieldWithNative;
	bool* m_abRegularityTradeInTurn;
	bool* m_abFeatAccomplished;
	bool* m_abOptions;
	bool* m_abSecondaryOptions;
	bool* m_abHadTutorialDoane;
	bool* m_abUnitUsedMap;
	bool* m_abLaunchedPopup;
	
	CvString m_szScriptData;

	int* m_paiImprovementCount;
	int* m_paiFreeBuildingCount;
	bool* m_pabFirstBuildingBuilt;
	bool* m_pabHasGeneratedPirates;
	int* m_paiUnitClassCount;
	int* m_paiEnrolUnitType;
	int* m_paiUnitClassMaking;
	int* m_paiUnitMarketPrice;
	int* m_paiUnitKilledCounter;
	int* m_paiUnitTurnRemaining;
	int* m_paiUnitMoveChange;
	int* m_paiUnitStrengthModifier;
	int* m_paiProfessionCombatChange;
	int* m_paiProfessionMoveChange;
	int* m_paiBuildingClassCount;
	int* m_paiBuildingClassMaking;
	int* m_paiSpecialBuildingNotRequiredCount;
	int* m_aiMissionaryPoints;
	int* m_aiMissionaryThresholdMultiplier;
	int* m_aiUnemploymentHistory;
	int* m_aiFoodHistory;
	int* m_aiEconomicHistory;
	int* m_aiRegularityTrade;
	int* m_aiReligionHistory;

	unsigned char* m_aucRandomValues;
	unsigned char* m_aucUnitGivedByKing;
	unsigned char* m_aucEconomicTrends;
	unsigned char* m_aucEconomicTrendsRemainingTurns;
	
	int* m_aiProfessionEquipmentModifier;
	int* m_aiTraitCount;

	std::vector<EventTriggerTypes> m_triggersFired;
	CivicTypes* m_paeCivics;
	int** m_ppiImprovementYieldChange;
	int** m_ppiBuildingYieldChange;
	CLinkList<int> m_groupCycle;
	std::vector<CvWString> m_aszCityNames;
	FFreeListTrashArray<CvCityAI> m_cities;
	CvIdVector<CvTradeRoute> m_tradeRoutes;
	CvIdVector<CvAgreement> m_agreements;
	CvIdVector<CvHospitals> m_hospitals;
	CvIdVector<CvTempUnit> m_tempUnits;
	CvIdVector<CvUnitAI> m_units;
	CvIdVector<CvCityJob> m_cityJobs;
	CvIdVector<CvMarketTransaction> m_marketTransactions;
	CvIdVector<CvConvoy> m_convoys;
	std::vector<CvUnit*> m_aEuropeUnits;
	
	mutable std::map< std::pair<YieldTypes, PlayerTypes>, CvMarketTransaction*> m_mappedMarketTransactions;
	mutable CLinkList<CvSeaway> m_seaways;
	
	FFreeListTrashArray<CvSelectionGroupAI> m_selectionGroups;
	FFreeListTrashArray<EventTriggeredData> m_eventsTriggered;
	CvEventMap m_mapEventsOccured;
	CvEventMap m_mapEventCountdown;
	UnitCombatPromotionArray m_aFreeUnitCombatPromotions;
	UnitClassPromotionArray m_aFreeUnitClassPromotions;

	std::vector< std::pair<UnitTypes, ProfessionTypes> > m_aEuropeRevolutionUnits;
	CvMessageQueue m_listGameMessages;
	CvPopupQueue m_listPopups;
	CvDiploQueue m_listDiplomacy;
	CvTurnScoreMap m_mapScoreHistory;
	CvTurnScoreMap m_mapEconomyHistory;
	CvTurnScoreMap m_mapCrossHistory;
	CvTurnScoreMap m_mapIndustryHistory;
	CvTurnScoreMap m_mapAgricultureHistory;
	CvTurnScoreMap m_mapLandMilitaryHistory;
	CvTurnScoreMap m_mapSeaMilitaryHistory;
	CvTurnScoreMap m_mapNavalMerchantHistory;
	CvTurnScoreMap m_mapCultureHistory;
	CvImmigrationIntInfoMap m_mapTotalImmigration;
	CvImmigrationIntInfoMap m_mapTotalImmigrationAsked;
	CvImmigrationIntInfoMap m_mapTurnCountWithoutProposition;
	CvImmigrationIntInfoMap m_mapMaxTurnWithoutProposition;
	CvImmigrationFloatInfoMap m_mapCurrentImmigrationProbability;
	CvImmigrationFloatInfoMap m_mapFirstUnitImmigrationProbability;
	CvImmigrationFloatInfoMap m_mapAverageImmigrationProbability;
	CvTurnImmigrationMap m_mapImmigrationHistory;

	std::vector<CvWString> m_aszTradeMessages;
	std::vector<CvWString> m_aszEuropeTradeMessages;
	std::vector<CvWString> m_aszEventEuropeMessages;


	void doTaxes();
	void doGold();
	void doBells();
	void doCrosses();
	void doWarnings();
	void doEvents();

	bool checkExpireEvent(EventTypes eEvent, const EventTriggeredData& kTriggeredData) const;
	void expireEvent(EventTypes eEvent, const EventTriggeredData& kTriggeredData, bool bFail);
	CvCity* pickTriggerCity(EventTriggerTypes eTrigger) const;
	CvUnit* pickTriggerUnit(EventTriggerTypes eTrigger, CvPlot* pPlot, bool bPickPlot) const;
	void freeEuropeUnits();

	void processCivics(CivicTypes eCivic, int iChange);

	// for serialization
	virtual void read(FDataStreamBase* pStream);
	virtual void write(FDataStreamBase* pStream);
	void doUpdateCacheOnTurn();

	virtual int AI_getYieldTradeValue(YieldTypes eYield, PlayerTypes ePlayer) const = 0;
};

#endif
