//------------------------------------------------------------------------------------------------
//
//  ***************** CIV4 GAME ENGINE   ********************
//
//! \file		CvAgreement.cpp
//! \author		M07
//! \brief		Implementation of agreements
//
//------------------------------------------------------------------------------------------------
//  Copyright (c) 2012 Fratelli Marconni, Inc. All rights reserved.
//------------------------------------------------------------------------------------------------

#include "CvGameCoreDLL.h"
#include "CvAgreement.h"
#include "CvGameAI.h"


//------------------------------------------------------------------------------------------------
// FUNCTION:    CvAgreement::CvAgreement
//! \brief      Default constructor.
//------------------------------------------------------------------------------------------------
CvAgreement::CvAgreement() :
	m_iId(ANY_AGREEMENT_ID)
{
	m_aiTradeYield = new int[NUM_YIELD_TYPES];
	m_aiOriginalAmount = new int[NUM_YIELD_TYPES];
	m_aiActualAmount = new int[NUM_YIELD_TYPES];

	reset(0, true);
}

CvAgreement::~CvAgreement()
{
	uninit();

	SAFE_DELETE_ARRAY(m_aiTradeYield);
	SAFE_DELETE_ARRAY(m_aiOriginalAmount);
	SAFE_DELETE_ARRAY(m_aiActualAmount);
}

void CvAgreement::init(int iID, const IDInfo& kSourceCity, const IDInfo& kDestinationCity)
{
	reset(iID);
	m_kSourceCity = kSourceCity;
	m_kDestinationCity = kDestinationCity;
	m_iTurnCreated = GC.getGameINLINE().getGameTurn();
}

void CvAgreement::uninit()
{
	m_aAssignedGroups.clear();
}
void CvAgreement::reset(int iID, bool bConstructorCall)
{
	m_iId = iID;
	m_iTurnCreated = 0;
	m_iTurnMax = -1;
	m_iLevel = 0;
	m_iFixedPrice = 0;
	m_iPrime = 0;

	m_kSourceCity.reset();
	m_kDestinationCity.reset();

	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		m_aiTradeYield[iI] = NO_TRADE;
		m_aiOriginalAmount[iI] = 0;
		m_aiActualAmount[iI] = 0;
	}
}
int CvAgreement::getID() const
{
	return m_iId;
}

void CvAgreement::setID(int iId)
{
	m_iId = iId;
}

PlayerTypes CvAgreement::getOwner() const
{
	return getSourceCity().eOwner;
}

int CvAgreement::getTurnCreated() const
{
	return m_iTurnCreated;
}

int CvAgreement::getTurnMax() const
{
	return m_iTurnMax;
}

void CvAgreement::setTurnMax(int iTurnMax)
{
	m_iTurnMax = iTurnMax;
}

int CvAgreement::getLevel() const
{
	return m_iLevel;
}

void CvAgreement::setLevel(int iLevel)
{
	m_iLevel = iLevel;
	setTurnMax(10*iLevel);
}

int CvAgreement::getFixedPrice() const
{
	return m_iFixedPrice;
}

void CvAgreement::setFixedPrice(int iValue)
{
	if (getFixedPrice() != iValue)
	{
		m_iFixedPrice = iValue;
	}
}

int CvAgreement::getPrime() const
{
	return m_iPrime;
}

void CvAgreement::setPrime(int iValue)
{
	if (getPrime() != iValue)
	{
		m_iPrime = iValue;
	}
}

const IDInfo& CvAgreement::getSourceCity() const
{
	return m_kSourceCity;
}

void CvAgreement::setSourceCity(const IDInfo& kCity)
{
	if (getSourceCity() != kCity)
	{
		m_kSourceCity = kCity;
	}
}

const wchar* CvAgreement::getSourceCityNameKey() const
{
	if (getSourceCity().iID == EUROPE_CITY_ID)
	{
		return L"TXT_KEY_CONCEPT_EUROPE";
	}

	CvCity* pCity = ::getCity(getSourceCity());
	FAssert(pCity != NULL);
	if (pCity != NULL)
	{
		return pCity->getNameKey();
	}

	return L"";
}

const IDInfo& CvAgreement::getDestinationCity() const
{
	return m_kDestinationCity;
}

void CvAgreement::setDestinationCity(const IDInfo& kCity)
{
	if (getDestinationCity() != kCity)
	{
		m_kDestinationCity = kCity;
	}
}

const wchar* CvAgreement::getDestinationCityNameKey() const
{
	if (getDestinationCity().iID == EUROPE_CITY_ID)
	{
		return L"TXT_KEY_CONCEPT_EUROPE";
	}

	CvCity* pCity = ::getCity(getDestinationCity());
	FAssert(pCity != NULL);
	if (pCity != NULL)
	{
		return pCity->getNameKey();
	}

	return L"";
}

TradeTypes CvAgreement::getTradeState(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return (TradeTypes)m_aiTradeYield[eYield];
}

void CvAgreement::setTradeState(YieldTypes eYield, TradeTypes iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getTradeState(eYield) != iValue)
	{
		m_aiTradeYield[eYield] = iValue;
	}
}

int CvAgreement::getOriginalAmount(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiOriginalAmount[eYield];
}

void CvAgreement::setOriginalAmount(YieldTypes eYield, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getOriginalAmount(eYield) != iValue)
	{
		m_aiOriginalAmount[eYield] = iValue;
	}
}

int CvAgreement::getActualAmount(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiActualAmount[eYield];
}

void CvAgreement::setActualAmount(YieldTypes eYield, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getActualAmount(eYield) != iValue)
	{
		m_aiActualAmount[eYield] = iValue;
	}
}

void CvAgreement::changeActualAmount(YieldTypes eYield, int iValue)
{
	setActualAmount(eYield, getActualAmount(eYield)+iValue);
}

bool CvAgreement::hasNoDeadline() const
{
	return getTurnMax() < 0;
}

bool CvAgreement::isDeadline() const
{
	if (hasNoDeadline())
	{
		return false;
	}

	return GC.getGameINLINE().getGameTurn() >= getTurnCreated() + getTurnMax();
}

int CvAgreement::getTurnLeft() const
{
	if (hasNoDeadline())
	{
		return -1;
	}
	return getTurnCreated() + getTurnMax() - GC.getGameINLINE().getGameTurn();
}

bool CvAgreement::isYieldFree(YieldTypes eYield) const
{
	return getTradeState(eYield) == NO_TRADE;
}

bool CvAgreement::isYieldOver(YieldTypes eYield) const
{
	if (isYieldFree(eYield))
	{
		return true;
	}
	return getActualAmount(eYield) == getOriginalAmount(eYield);
}

bool CvAgreement::isOver() const
{
	for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
	{
		YieldTypes eYield = (YieldTypes)iI;
		if (getTradeState(eYield) != NO_TRADE && !isYieldOver(eYield))
		{
			return false;
		}
	}
	return true;
}
void CvAgreement::addYieldTrade(YieldTypes eYield, TradeTypes eType, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);

	if (isYieldFree(eYield) && eType != NO_TRADE)
	{
		setTradeState(eYield, eType);
		setOriginalAmount(eYield, iValue);
		setActualAmount(eYield, 0);
	}
}

int CvAgreement::getYieldOfType(TradeTypes eType, YieldTypes* eYield) const
{
	int iCount = 0;
	for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
	{
		YieldTypes eLoopYield = (YieldTypes)iI;
		if (getTradeState(eLoopYield) == eType)
		{
			*eYield = eLoopYield;
			iCount++;
		}
	}
	return iCount;
}

YieldTypes CvAgreement::getImportYield() const
{
	YieldTypes eYield;
	if (getYieldOfType(TRADE_IMPORT, &eYield) == 1)
	{
		return eYield;
	}
	return NO_YIELD;
}

YieldTypes CvAgreement::getExportYield() const
{
	YieldTypes eYield;
	if (getYieldOfType(TRADE_EXPORT, &eYield) == 1)
	{
		return eYield;
	}
	return NO_YIELD;
}

int CvAgreement::getMaxLoadOfExportedYield() const
{
	int iActualAmountsImp = 0;
	int iOriginalAmountsImp = 0;
	int iActualAmountsExp = 0;
	int iOriginalAmountsExp = 0;
	for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
	{
		YieldTypes eYield = (YieldTypes)iI;
		if (getTradeState(eYield) == TRADE_IMPORT)
		{
			iActualAmountsImp += getActualAmount(eYield);
			iOriginalAmountsImp += getOriginalAmount(eYield);
		}
		if (getTradeState(eYield) == TRADE_EXPORT)
		{
			iActualAmountsExp += getActualAmount(eYield);
			iOriginalAmountsExp += getOriginalAmount(eYield);
		}
	}
	FAssertMsg(iOriginalAmountsImp > 0, "iOriginalAmountsImp expected to be > 0");
	FAssertMsg(iOriginalAmountsExp > 0, "iOriginalAmountsExp expected to be > 0");

	int iBonusPercent = 15;//To take a little advance
	int iPercent = iActualAmountsImp*100/iOriginalAmountsImp;

	iPercent = std::min(iPercent + iBonusPercent, 100);

	int iMaxLoad = iOriginalAmountsExp*iPercent/100;
	iMaxLoad -= iActualAmountsExp;
	return std::max(iMaxLoad, 0);

}

int CvAgreement::getNumAssignedGroups() const
{
	return m_aAssignedGroups.size();
}

void CvAgreement::assignGroup(int iId, bool bAssign)
{
	if (isAssignedGroup(iId) != bAssign)
	{
		if (bAssign)
		{
			m_aAssignedGroups.insert(iId);
		}
		else
		{
			m_aAssignedGroups.erase(iId);
		}
	}
}

bool CvAgreement::isAssignedGroup(int iId) const
{
	return (m_aAssignedGroups.find(iId) != m_aAssignedGroups.end());
}

int CvAgreement::getMissingYieldsPercent(TradeTypes eType) const
{
	int iActualAmountYields = 0, iOriginalAmountYields = 0;
	for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
	{
		YieldTypes eYield = (YieldTypes)iI;
		if (getTradeState(eYield) == eType)
		{
			iActualAmountYields += getActualAmount(eYield);
			iOriginalAmountYields += getOriginalAmount(eYield);
		}
	}
	if (iOriginalAmountYields == 0 || iActualAmountYields == iOriginalAmountYields)
	{
		return 0;
	}

	return std::max(1, 100 - iActualAmountYields*100/iOriginalAmountYields);
}

void CvAgreement::copyAgreement(CvAgreement* pAgreement)
{
	for (int iI = 0; iI < NUM_YIELD_TYPES; ++iI)
	{
		YieldTypes eYield = (YieldTypes)iI;
		if (pAgreement->getTradeState(eYield) != NO_TRADE )
		{
			setTradeState(eYield, pAgreement->getTradeState(eYield));
			setOriginalAmount(eYield, pAgreement->getOriginalAmount(eYield));
			setActualAmount(eYield, pAgreement->getActualAmount(eYield));
		}
	}
	setTurnMax(pAgreement->getTurnMax());
	setPrime(pAgreement->getPrime());
	setFixedPrice(pAgreement->getFixedPrice());
}

void CvAgreement::read(FDataStreamBase* pStream)
{
	reset();

	uint uiFlag = 0;
	pStream->Read(&uiFlag);	// flags for expansion

	pStream->Read(&m_iId);
	pStream->Read(&m_iTurnCreated);
	pStream->Read(&m_iTurnMax);
	pStream->Read(&m_iLevel);
	pStream->Read(&m_iFixedPrice);
	pStream->Read(&m_iPrime);
	m_kSourceCity.read(pStream);
	m_kDestinationCity.read(pStream);
	pStream->Read(NUM_YIELD_TYPES, m_aiTradeYield);
	pStream->Read(NUM_YIELD_TYPES, m_aiOriginalAmount);
	pStream->Read(NUM_YIELD_TYPES, m_aiActualAmount);

	m_aAssignedGroups.clear();
	int iNumAssignedGroups;
	std::vector<int> aAssignedGroups;
	pStream->Read(&iNumAssignedGroups);
	aAssignedGroups.resize(iNumAssignedGroups);
	if (iNumAssignedGroups > 0)
	{
		pStream->Read(iNumAssignedGroups, (int*)&aAssignedGroups[0]);
		m_aAssignedGroups.insert(aAssignedGroups.begin(), aAssignedGroups.end());
	}
}

void CvAgreement::write(FDataStreamBase* pStream)
{
	uint uiFlag = 0;
	pStream->Write(uiFlag);		// flag for expansion

	pStream->Write(m_iId);
	pStream->Write(m_iTurnCreated);
	pStream->Write(m_iTurnMax);
	pStream->Write(m_iLevel);
	pStream->Write(m_iFixedPrice);
	pStream->Write(m_iPrime);
	m_kSourceCity.write(pStream);
	m_kDestinationCity.write(pStream);
	pStream->Write(NUM_YIELD_TYPES, m_aiTradeYield);
	pStream->Write(NUM_YIELD_TYPES, m_aiOriginalAmount);
	pStream->Write(NUM_YIELD_TYPES, m_aiActualAmount);

	pStream->Write((int)m_aAssignedGroups.size());
	for (std::set<int>::iterator it = m_aAssignedGroups.begin(); it != m_aAssignedGroups.end(); ++it)
	{
		pStream->Write(*it);
	}
}
