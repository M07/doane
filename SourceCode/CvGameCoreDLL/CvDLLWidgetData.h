#pragma once

#ifndef _CVDLLWIDGETDATA_H_
#define _CVDLLWIDGETDATA_H_

//#include "CvEnums.h"

class CvDLLWidgetData
{

public:

	DllExport static CvDLLWidgetData& getInstance();
	DllExport static void freeInstance();

	DllExport void parseHelp(CvWStringBuffer &szBuffer, CvWidgetDataStruct &widgetDataStruct);

	DllExport bool executeAction( CvWidgetDataStruct &widgetDataStruct );
	DllExport bool executeAltAction( CvWidgetDataStruct &widgetDataStruct );
	DllExport bool executeDropOn(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData);
	DllExport bool executeDoubleClick(const CvWidgetDataStruct& destinationWidgetData);

	DllExport bool isLink(const CvWidgetDataStruct &widgetDataStruct) const;

protected:
	//	Actions to be executed
	void doPlotList(CvWidgetDataStruct &widgetDataStruct);
	void doLiberateCity();
	void doRenameCity();
	void doRenameUnit();
	void doCreateGroup();
	void doDeleteGroup();
	void doTrain(CvWidgetDataStruct &widgetDataStruct);
	void doConstruct(CvWidgetDataStruct &widgetDataStruct);
	void doHurry(CvWidgetDataStruct &widgetDataStruct);
	void doPlayerHurry(CvWidgetDataStruct &widgetDataStruct);
	void doAction(CvWidgetDataStruct &widgetDataStruct);
	void doContactCiv(CvWidgetDataStruct &widgetDataStruct);
	void doAutomateCitizens(CvWidgetDataStruct &widgetDataStruct);
	void doAutomateProduction();
	void doEmphasize(CvWidgetDataStruct &widgetDataStruct);
	void doChooseQuantityTradeInCity(CvWidgetDataStruct &widgetDataStruct);
	void doChooseQuantityTradeInUnit(CvWidgetDataStruct &widgetDataStruct);
	void doCancelCivics();
	void applyCityEdit();
	void doUnitModel();
	void doFlag();
	void doSelected(CvWidgetDataStruct &widgetDataStruct);
	void doPediaUnitJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaProfessionJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaBuildingJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaBack();
	void doPediaForward();
	void doPediaBonusJump(CvWidgetDataStruct &widgetDataStruct, bool bData2 = false);
	void doPediaTerrainJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaYieldJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaFeatureJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaMain(CvWidgetDataStruct &widgetDataStruct);
	void doPediaPromotionJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaImprovementJump(CvWidgetDataStruct &widgetDataStruct, bool bData2 = false);
	void doPediaCivicJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaCivilizationJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaLeaderJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaDescription(CvWidgetDataStruct &widgetDataStruct);
	void doGotoTurnEvent(CvWidgetDataStruct &widgetDataStruct);
	void doPediaConstructJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaTrainJump(CvWidgetDataStruct &widgetDataStruct);
	void doPediaBuildJump(CvWidgetDataStruct &widgetDataStruct);
	void doDealKill(CvWidgetDataStruct &widgetDataStruct);
	void doRefreshMilitaryAdvisor(CvWidgetDataStruct &widgetDataStruct);
	void doCityUnitAssignCitizen(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData);
	void doMoveShipCargo(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData);
	void doMoveShipToGroup(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData);
	void doMoveDockUnit(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData);
	void doMoveYieldImportExport(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData);
	void doUnitIntoCity(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData);
	void doMoveCargoToCity(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData);
	void doChooseTradeRoute(const CvWidgetDataStruct& widgetDataStruct);
	void doAssignTradeRoute(const CvWidgetDataStruct& widgetDataStruct);
	void doGoToCity(const CvWidgetDataStruct& widgetDataStruct);
	
	void doTradeRouteMoveTransportToTransport(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData);
	void doMoveCargoToTransport(const CvWidgetDataStruct& destinationWidgetData, const CvWidgetDataStruct& sourceWidgetData);
	void doCreateTradeRoute(const CvWidgetDataStruct& widgetDataStruct);
	void doYieldImportExport(const CvWidgetDataStruct& widgetDataStruct);
	void doControlHospital(const CvWidgetDataStruct& widgetDataStruct);
	void doYieldProduction(const CvWidgetDataStruct& widgetDataStruct);
	void doControlTavern(const CvWidgetDataStruct& widgetDataStruct);
	void doAgreementList(const CvWidgetDataStruct &widgetDataStruct);
	void doSeedlingList(const CvWidgetDataStruct &widgetDataStruct);
	void doDestroyCity(const CvWidgetDataStruct& widgetDataStruct);
	void doRecruitCrew(const CvWidgetDataStruct& widgetDataStruct);
	void doLoadAllTheYieldInGroup(const CvWidgetDataStruct& widgetDataStruct);
	void doEnrolUnitType(const CvWidgetDataStruct& widgetDataStruct, bool bIncrease);
	void doTipsEnrolUnitType(const CvWidgetDataStruct& widgetDataStruct);
	
	void doSendUpSeaway(const CvWidgetDataStruct& widgetDataStruct);
	void doSendDownSeaway(const CvWidgetDataStruct& widgetDataStruct);
	void doSelectDestinationColony(const CvWidgetDataStruct& widgetDataStruct);

	void doAddNewResourceIntoTradeRoute(const CvWidgetDataStruct& widgetDataStruct);
	
	void doProposalToImmigrant(const CvWidgetDataStruct& widgetDataStruct);
	void doArsenalManagement(const CvWidgetDataStruct& widgetDataStruct);
	void doBuyShipUsed(const CvWidgetDataStruct& widgetDataStruct);
	void doBuyNewShip(const CvWidgetDataStruct& widgetDataStruct);
	void doTransfertEuropeToWarehouse(const CvWidgetDataStruct& widgetDataStruct);
	void doUnloadNewCargo(const CvWidgetDataStruct& widgetDataStruct);
	void doUnloadYield(const CvWidgetDataStruct& widgetDataStruct);
	void doMenu();

	void doDoubleClickCitizen(const CvWidgetDataStruct& widgetDataStruct);
	void doDoubleClickDock(const CvWidgetDataStruct& widgetDataStruct);
	void doClickRenameShip(const CvWidgetDataStruct& widgetDataStruct);

	//	Help parsing
	void parsePlotListHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseLiberateCityHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseCityNameHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseTrainHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseConstructHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseHurryHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseActionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseCitizenHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseContactCivHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseNotorietyHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseChangeSeawayPositionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseScoreHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseImportExportHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseHospitalHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseYieldProductionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseTavernHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseDestructionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseArsenalManagementHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseBuyShipUsedHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseBuyNewShipHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseEjectCitizenHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseConvertHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseAutomateCitizensHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseAutomateProductionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseEmphasizeHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseChooseQuantityTradeHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseTradeItem(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseUnitModelHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseCityDefenseHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseFlagHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parsePopulationHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseRebelHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseGreatGeneralHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseSelectedHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseBuildingHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseSpecialBuildingHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseTerrainHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseYieldHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseFeatureHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseUnitHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseShipCargoUnitHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseGroupShipHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseOneShipHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseTradeRouteImpExpListHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseTradeRouteImpExpBoxHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseTradeRouteBestYieldHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseEuropeUnitHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseEuropeRecruitCrewHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseEuropeEnrolUnitTypeHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseTransfertEuropeToWarehouseHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseRenameShipHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseProfessionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parsePediaBack(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parsePediaForward(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseBonusHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parsePromotionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseUnitPromotionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseCityYieldHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseTradeYieldWithNatifHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseRobYieldHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseDestinationCityHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseSeedInfoHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseAgreementHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseTradeRoutesHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseChooseTradeRoute(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseAssignTradeRoute(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseReceiveMoveCargoToTransportHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseUnloadNewCargoHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseEventHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseImprovementHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseCivicHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseCivilizationHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseLeaderHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseDescriptionHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer, bool bMinimal);
	void parseCloseScreenHelp(CvWStringBuffer &szBuffer);
	void parseKillDealHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseProductionModHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseLeaderheadHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseLeaderheadKingHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);
	void parseLeaderLineHelp(CvWidgetDataStruct &widgetDataStruct, CvWStringBuffer &szBuffer);

	EmphasizeTypes getNextEmphasis(CvCity* pCity, YieldTypes eYield);
	void doAssignCitizenToPlot(CvCity* pCity, int iPlotIndex, int iUnitId);

	static CvDLLWidgetData* m_pInst;

};

#endif//_CVDLLWIDGETDATA_H_
