#pragma once

#ifndef CVAGREEMENT_H
#define CVAGREEMENT_H

#include "CvString.h"
#include "CvIdVector.h"

class CvMerchant;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  CLASS:      CvAgreement
//!  \brief		Agreement for trade
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class DllExport CvAgreement
{

public:
	CvAgreement();

	~CvAgreement();

	void init(int iID, const IDInfo& kSourceCity, const IDInfo& kDestinationCity);
	void uninit();
	void reset(int iID = 0, bool bConstructorCall = false);
	
	int getID() const;
	void setID(int iId);

	PlayerTypes getOwner() const;

	int getTurnMax() const;
	void setTurnMax(int iTurnMax);

	int getLevel() const;
	void setLevel(int iLevel);

	int getTurnCreated() const;

	int getFixedPrice() const;
	void setFixedPrice(int iValue);

	int getPrime() const;
	void setPrime(int iValue);

	const IDInfo& getSourceCity() const;
	void setSourceCity(const IDInfo& kCity);
	const wchar* getSourceCityNameKey() const;

	const IDInfo& getDestinationCity() const;
	void setDestinationCity(const IDInfo& kCity);
	const wchar* getDestinationCityNameKey() const;
	const wchar* getCityNameKey() const;

	TradeTypes getTradeState(YieldTypes eYield) const;
	void setTradeState(YieldTypes eYield, TradeTypes iValue);

	int getOriginalAmount(YieldTypes eYield) const;
	void setOriginalAmount(YieldTypes eYield, int iValue);

	int getActualAmount(YieldTypes eYield) const;
	void setActualAmount(YieldTypes eYield, int iValue);
	void changeActualAmount(YieldTypes eYield, int iValue);

	bool hasNoDeadline() const;
	bool isDeadline() const;
	int getTurnLeft() const;
	bool isYieldFree(YieldTypes eYield) const;
	bool isYieldOver(YieldTypes eYield) const;
	bool isOver() const;
	void addYieldTrade(YieldTypes eYield, TradeTypes eType, int iValue);
	int getYieldOfType(TradeTypes eType, YieldTypes* eYield) const;
	YieldTypes getImportYield() const;
	YieldTypes getExportYield() const;

	int getMaxLoadOfExportedYield() const;

	int getNumAssignedGroups() const;
	void assignGroup(int iId, bool bAssign);
	bool isAssignedGroup(int iId) const;

	int getMissingYieldsPercent(TradeTypes eType) const;

	void copyAgreement(CvAgreement* pAgreement);

	void read(FDataStreamBase* pStream);
	void write(FDataStreamBase* pStream);

	static const int EUROPE_CITY_ID = -1;
	static const int ANY_AGREEMENT_ID = -2;

protected:
	int m_iId;
	int m_iTurnMax;
	int m_iLevel;
	int m_iTurnCreated;
	int m_iFixedPrice;
	int m_iPrime;
	IDInfo m_kSourceCity;
	IDInfo m_kDestinationCity;

	int* m_aiTradeYield;
	int* m_aiOriginalAmount;
	int* m_aiActualAmount;

	std::set<int> m_aAssignedGroups;

	//CvMerchant* m_pMerchant;
};


#endif	// CVAGREEMENT_H
