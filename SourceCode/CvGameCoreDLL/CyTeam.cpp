//
// Python wrapper class for CvTeam
// updated 6-5
//
#include "CvGameCoreDLL.h"
#include "CyTeam.h"
#include "CyArea.h"
#include "CvTeam.h"
#include "CyUnit.h"
CyTeam::CyTeam() : m_pTeam(NULL)
{
}
CyTeam::CyTeam(CvTeam* pTeam) : m_pTeam(pTeam)
{
}
void CyTeam::addTeam(int /*TeamTypes*/ eTeam)
{
	if (m_pTeam)
		m_pTeam->addTeam((TeamTypes)eTeam);
}
bool CyTeam::canChangeWarPeace(int /*TeamTypes*/ eTeam)
{
	return m_pTeam ? m_pTeam->canChangeWarPeace((TeamTypes)eTeam) : false;
}
bool CyTeam::canDeclareWar(int /*TeamTypes*/ eTeam)
{
	return m_pTeam ? m_pTeam->canDeclareWar((TeamTypes)eTeam) : false;
}
void CyTeam::declareWar(int /*TeamTypes*/ eTeam, bool bNewDiplo, int /*WarPlanTypes*/ eWarPlan)
{
	if (m_pTeam)
		m_pTeam->declareWar((TeamTypes)eTeam, bNewDiplo, (WarPlanTypes)eWarPlan);
}
void CyTeam::makePeace(int /*TeamTypes*/ eTeam)
{
	if (m_pTeam)
		m_pTeam->makePeace((TeamTypes)eTeam);
}
bool CyTeam::canContact(int /*TeamTypes*/ eTeam)
{
	return m_pTeam ? m_pTeam->canContact((TeamTypes)eTeam) : false;
}
void CyTeam::meet(int /*TeamTypes*/ eTeam, bool bNewDiplo)
{
	if (m_pTeam)
		m_pTeam->meet((TeamTypes)eTeam, bNewDiplo);
}
void CyTeam::signOpenBorders(int /*TeamTypes*/ eTeam)
{
	if (m_pTeam)
		m_pTeam->signOpenBorders((TeamTypes)eTeam);
}
void CyTeam::signDefensivePact(int /*TeamTypes*/ eTeam)
{
	if (m_pTeam)
		m_pTeam->signDefensivePact((TeamTypes)eTeam);
}
int CyTeam::getAssets()
{
	return m_pTeam ? m_pTeam->getAssets() : -1;
}
int CyTeam::getPower()
{
	return m_pTeam ? m_pTeam->getPower() : -1;
}
int CyTeam::getDefensivePower()
{
	return m_pTeam ? m_pTeam->getDefensivePower() : -1;
}
int CyTeam::getAtWarCount()
{
	return m_pTeam ? m_pTeam->getAtWarCount() : -1;
}
int CyTeam::getWarPlanCount(int /*WarPlanTypes*/ eWarPlan)
{
	return m_pTeam ? m_pTeam->getWarPlanCount((WarPlanTypes) eWarPlan) : -1;
}
int CyTeam::getAnyWarPlanCount()
{
	return m_pTeam ? m_pTeam->getAnyWarPlanCount() : -1;
}
int CyTeam::getChosenWarCount()
{
	return m_pTeam ? m_pTeam->getChosenWarCount() : -1;
}
int CyTeam::getHasMetCivCount()
{
	return m_pTeam ? m_pTeam->getHasMetCivCount() : -1;
}
bool CyTeam::hasMetHuman()
{
	return m_pTeam ? m_pTeam->hasMetHuman() : false;
}
int CyTeam::getDefensivePactCount()
{
	return m_pTeam ? m_pTeam->getDefensivePactCount() : -1;
}
int CyTeam::getUnitClassMaking(int /*UnitClassTypes*/ eUnitClass)
{
	return m_pTeam ? m_pTeam->getUnitClassMaking((UnitClassTypes)eUnitClass) : -1;
}
int CyTeam::getUnitClassCountPlusMaking(int /*UnitClassTypes*/ eUnitClass)
{
	return m_pTeam ? m_pTeam->getUnitClassCountPlusMaking((UnitClassTypes)eUnitClass) : -1;
}
int CyTeam::getBuildingClassMaking(int /*BuildingClassTypes*/ eBuildingClass)
{
	return m_pTeam ? m_pTeam->getBuildingClassMaking((BuildingClassTypes)eBuildingClass) : -1;
}
int CyTeam::getBuildingClassCountPlusMaking(int /*BuildingClassTypes*/ eBuildingClass)
{
	return m_pTeam ? m_pTeam->getBuildingClassCountPlusMaking((BuildingClassTypes)eBuildingClass) : -1;
}
int CyTeam::countTotalCulture()
{
	return m_pTeam ? m_pTeam->countTotalCulture() : -1;
}
int CyTeam::countNumUnitsByArea(CyArea* pArea)
{
	return m_pTeam ? m_pTeam->countNumUnitsByArea(pArea->getArea()) : -1;
}
int CyTeam::countNumCitiesByArea(CyArea* pArea)
{
	return m_pTeam ? m_pTeam->countNumCitiesByArea(pArea->getArea()) : -1;
}
int CyTeam::countTotalPopulationByArea(CyArea* pArea)
{
	return m_pTeam ? m_pTeam->countTotalPopulationByArea(pArea->getArea()) : -1;
}
int CyTeam::countPowerByArea(CyArea* pArea)
{
	return m_pTeam ? m_pTeam->countPowerByArea(pArea->getArea()) : -1;
}
int CyTeam::countEnemyPowerByArea(CyArea* pArea)
{
	return m_pTeam ? m_pTeam->countEnemyPowerByArea(pArea->getArea()) : -1;
}
int CyTeam::countNumAIUnitsByArea(CyArea* pArea, int /*UnitAITypes*/ eUnitAI)
{
	return m_pTeam ? m_pTeam->countNumAIUnitsByArea(pArea->getArea(), (UnitAITypes) eUnitAI) : -1;
}
int CyTeam::countEnemyDangerByArea(CyArea* pArea)
{
	return m_pTeam ? m_pTeam->countEnemyDangerByArea(pArea->getArea()) : -1;
}
bool CyTeam::isHuman()
{
	return m_pTeam ? m_pTeam->isHuman() : false;
}
bool CyTeam::isNearTeam(int /*TeamTypes*/ eIndex)
{
	return m_pTeam ? m_pTeam->isNearTeam((TeamTypes)eIndex) : false;
}
bool CyTeam::hasNativePlayer()
{
	return m_pTeam ? m_pTeam->hasNativePlayer() : false;
}
int /*PlayerTypes*/ CyTeam::getLeaderID()
{
	return m_pTeam ? m_pTeam->getLeaderID() : -1;
}
int /*HandicapTypes*/ CyTeam::getHandicapType()
{
	return m_pTeam ? m_pTeam->getHandicapType() : -1;
}
std::wstring CyTeam::getName()
{
	return m_pTeam ? m_pTeam->getName() : L"";
}
int CyTeam::getNumMembers()
{
	return m_pTeam ? m_pTeam->getNumMembers() : -1;
}
bool CyTeam::isAlive()
{
	return m_pTeam ? m_pTeam->isAlive() : false;
}
bool CyTeam::isEverAlive()
{
	return m_pTeam ? m_pTeam->isEverAlive() : false;
}
int CyTeam::getNumCities()
{
	return m_pTeam ? m_pTeam->getNumCities() : -1;
}
int CyTeam::getTotalPopulation()
{
	return m_pTeam ? m_pTeam->getTotalPopulation() : -1;
}
int CyTeam::getTotalLand()
{
	return m_pTeam ? m_pTeam->getTotalLand() : -1;
}
int CyTeam::getMapTradingCount()
{
	return m_pTeam ? m_pTeam->getMapTradingCount() : -1;
}
bool CyTeam::isMapTrading()
{
	return m_pTeam ? m_pTeam->isMapTrading() : false;
}
void CyTeam::changeMapTradingCount(int iChange)
{
	if (m_pTeam)
		m_pTeam->changeMapTradingCount(iChange);
}
int CyTeam::getGoldTradingCount()
{
	return m_pTeam ? m_pTeam->getGoldTradingCount() : -1;
}
bool CyTeam::isGoldTrading()
{
	return m_pTeam ? m_pTeam->isGoldTrading() : false;
}
void CyTeam::changeGoldTradingCount(int iChange)
{
	if (m_pTeam)
		m_pTeam->changeGoldTradingCount(iChange);
}
int CyTeam::getOpenBordersTradingCount()
{
	return m_pTeam ? m_pTeam->getOpenBordersTradingCount() : -1;
}
bool CyTeam::isOpenBordersTrading()
{
	return m_pTeam ? m_pTeam->isOpenBordersTrading() : false;
}
void CyTeam::changeOpenBordersTradingCount(int iChange)
{
	if (m_pTeam)
		m_pTeam->changeOpenBordersTradingCount(iChange);
}
int CyTeam::getDefensivePactTradingCount()
{
	return m_pTeam ? m_pTeam->getDefensivePactTradingCount() : -1;
}
bool CyTeam::isDefensivePactTrading()
{
	return m_pTeam ? m_pTeam->isDefensivePactTrading() : false;
}
void CyTeam::changeDefensivePactTradingCount(int iChange)
{
	if (m_pTeam)
		m_pTeam->changeDefensivePactTradingCount(iChange);
}
int CyTeam::getPermanentAllianceTradingCount()
{
	return m_pTeam ? m_pTeam->getPermanentAllianceTradingCount() : -1;
}
bool CyTeam::isPermanentAllianceTrading()
{
	return m_pTeam ? m_pTeam->isPermanentAllianceTrading() : false;
}
void CyTeam::changePermanentAllianceTradingCount(int iChange)
{
	if (m_pTeam)
		m_pTeam->changePermanentAllianceTradingCount(iChange);
}
bool CyTeam::isMapCentering()
{
	return m_pTeam ? m_pTeam->isMapCentering() : false;
}
void CyTeam::setMapCentering(bool bNewValue)
{
	if (m_pTeam)
		m_pTeam->setMapCentering(bNewValue);
}
int CyTeam::getID()
{
	return m_pTeam ? m_pTeam->getID() : -1;
}
bool CyTeam::isHasMet(int /*TeamTypes*/ eIndex)
{
	return m_pTeam ? m_pTeam->isHasMet((TeamTypes)eIndex) : false;
}
bool CyTeam::isAtWar(int /*TeamTypes*/ iIndex)
{
	if (iIndex == NO_TEAM) return false;
	return m_pTeam ? m_pTeam->isAtWar((TeamTypes)iIndex) : false;
}
bool CyTeam::isPermanentWarPeace(int /*TeamTypes*/ eIndex)
{
	return m_pTeam ? m_pTeam->isPermanentWarPeace((TeamTypes)eIndex) : false;
}
void CyTeam::setPermanentWarPeace(int /*TeamTypes*/ eIndex, bool bNewValue)
{
	if (m_pTeam)
		m_pTeam->setPermanentWarPeace((TeamTypes)eIndex, bNewValue);
}
bool CyTeam::isOpenBorders(int /*TeamTypes*/ eIndex)
{
	return m_pTeam ? m_pTeam->isOpenBorders((TeamTypes)eIndex) : false;
}
bool CyTeam::isForcePeace(int /*TeamTypes*/ eIndex)
{
	return m_pTeam ? m_pTeam->isForcePeace((TeamTypes)eIndex) : false;
}
bool CyTeam::isDefensivePact(int /*TeamTypes*/ eIndex)
{
	return m_pTeam ? m_pTeam->isDefensivePact((TeamTypes)eIndex) : false;
}
int CyTeam::getUnitClassCount(int /*UnitClassTypes*/ eIndex)
{
	return m_pTeam ? m_pTeam->getUnitClassCount((UnitClassTypes)eIndex) : -1;
}
int CyTeam::getBuildingClassCount(int /*BuildingClassTypes*/ eIndex)
{
	return m_pTeam ? m_pTeam->getBuildingClassCount((BuildingClassTypes)eIndex) : -1;
}
bool CyTeam::AI_shareWar(int /*TeamTypes*/ eTeam)
{
	return m_pTeam ? m_pTeam->AI_shareWar((TeamTypes)eTeam) : false;
}
void CyTeam::AI_setWarPlan(int /*TeamTypes*/ eIndex, int /*WarPlanTypes*/ eNewValue)
{
	if (m_pTeam)
	{
		m_pTeam->AI_setWarPlan((TeamTypes)eIndex, (WarPlanTypes)eNewValue);
	}
}
int CyTeam::AI_getAtWarCounter(int /*TeamTypes*/ eTeam) const
{
	return m_pTeam ? m_pTeam->AI_getAtWarCounter((TeamTypes)eTeam) : -1;
}
int CyTeam::AI_getAtPeaceCounter(int /*TeamTypes*/ eTeam) const
{
	return m_pTeam ? m_pTeam->AI_getAtPeaceCounter((TeamTypes)eTeam) : -1;
}
int CyTeam::AI_getWarSuccess(int /*TeamTypes*/ eIndex) const
{
	return m_pTeam ? m_pTeam->AI_getWarSuccess((TeamTypes)eIndex) : -1;
}
int CyTeam::getRebelPercent() const
{
	return m_pTeam ? m_pTeam->getRebelPercent() : -1;
}
void CyTeam::doRevolution()
{
	if (m_pTeam)
	{
		m_pTeam->doRevolution();
	}
}
bool CyTeam::canDoRevolution() const
{
	return m_pTeam ? m_pTeam->canDoRevolution() : false;
}
bool CyTeam::isParentOf(int /*TeamTypes*/ eChildTeam)
{
	return m_pTeam ? m_pTeam->isParentOf((TeamTypes) eChildTeam) : false;
}

int CyTeam::getNewEra() const
{
	return m_pTeam ? m_pTeam->getNewEra() : 0;
}

void CyTeam::setNewEra(int /*NewEraTypes*/ eNewEra)
{
	if (m_pTeam) {
		m_pTeam->setNewEra((NewEraTypes)eNewEra);
	}
}

int CyTeam::getNextNewEra() const
{
	return m_pTeam ? m_pTeam->getNextNewEra() : 0;
}

int CyTeam::getIdealTurn(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getIdealTurn((NewEraTypes) eNewEra) : 0;
}

int CyTeam::getCurrentTurn() const
{
	return m_pTeam ? m_pTeam->getCurrentTurn() : 0;
}

int CyTeam::getRequiredMaxTurn(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getRequiredMaxTurn((NewEraTypes) eNewEra) : 0;
}

bool CyTeam::hasReachedMaxTurnRequirement(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->hasReachedMaxTurnRequirement((NewEraTypes) eNewEra) : false;
}

long CyTeam::getGold() const
{
	return m_pTeam ? m_pTeam->getGold() : 0;
}

long CyTeam::getRequiredGold(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getRequiredGold((NewEraTypes) eNewEra) : 0;
}

bool CyTeam::hasReachedGoldRequirement(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->hasReachedGoldRequirement((NewEraTypes) eNewEra) : false;
}

int CyTeam::getNewWorldYieldQuantity() const
{
	return m_pTeam ? m_pTeam->getNewWorldYieldQuantity() : 0;
}

int CyTeam::getRequiredRawMaterialsSold(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getRequiredRawMaterialsSold((NewEraTypes) eNewEra) : 0;
}

bool CyTeam::hasReachedRawMaterialsSoldRequirement(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->hasReachedRawMaterialsSoldRequirement((NewEraTypes) eNewEra) : false;
}

int CyTeam::getNumEuropeanCities() const
{
	return m_pTeam ? m_pTeam->getNumEuropeanCities() : 0;
}

int CyTeam::getRequiredNumCities(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getRequiredNumCities((NewEraTypes) eNewEra) : 0;
}

bool CyTeam::hasReachedNumCitiesRequirement(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->hasReachedNumCitiesRequirement((NewEraTypes) eNewEra) : false;
}

int CyTeam::getRequiredMinTurn(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getRequiredMinTurn((NewEraTypes) eNewEra) : 0;
}

bool CyTeam::hasReachedMinTurnRequirement(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->hasReachedMinTurnRequirement((NewEraTypes) eNewEra) : false;
}

int CyTeam::getRequiredRebelPercent(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getRequiredRebelPercent((NewEraTypes) eNewEra) : 0;
}

bool CyTeam::hasReacheRebelPercentRequirement(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->hasReacheRebelPercentRequirement((NewEraTypes) eNewEra) : false;
}

int CyTeam::getCrossesStored() const
{
	return m_pTeam ? m_pTeam->getCrossesStored() : 0;
}

int CyTeam::getRequiredCrossesStored(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getRequiredCrossesStored((NewEraTypes) eNewEra) : 0;
}

bool CyTeam::hasReachedCrossesStoredRequirement(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->hasReachedCrossesStoredRequirement((NewEraTypes) eNewEra) : false;
}

int CyTeam::getRequiredCitizenByColony(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getRequiredCitizenByColony((NewEraTypes) eNewEra) : 0;
}		

bool CyTeam::hasReachedCitizenByColonyRequirement(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->hasReachedCitizenByColonyRequirement((NewEraTypes) eNewEra) : false;
}

int CyTeam::getRequiredDefendersByColony(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getRequiredDefendersByColony((NewEraTypes) eNewEra) : 0;
}		

bool CyTeam::hasReachedDefendersByColonyRequirement(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->hasReachedDefendersByColonyRequirement((NewEraTypes) eNewEra) : false;
}

int CyTeam::getLandDiscovered() const
{
	return m_pTeam ? m_pTeam->getLandDiscovered() : 0;
}

int CyTeam::getRequiredLandDiscovered(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->getRequiredLandDiscovered((NewEraTypes) eNewEra) : 0;
}		

bool CyTeam::hasReachedLandDiscoveredRequirement(int /*NewEraTypes*/ eNewEra) const
{
	return m_pTeam ? m_pTeam->hasReachedLandDiscoveredRequirement((NewEraTypes) eNewEra) : false;
}
