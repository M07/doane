//------------------------------------------------------------------------------------------------
//
//  ***************** CIV4 GAME ENGINE   ********************
//
//! \file		CvTempUnit.cpp
//! \author		M07
//! \brief		Implementation of temporary units
//
//------------------------------------------------------------------------------------------------
//  Copyright (c) 2011 Fratelli Marconni, Inc. All rights reserved.
//------------------------------------------------------------------------------------------------

#include "CvGameCoreDLL.h"
#include "CvGameAI.h"
#include "CvPlayerAI.h"
#include "CvInfos.h"
#include "CvTempUnit.h"
#include "CvDLLInterfaceIFaceBase.h"
#include <numeric>

//------------------------------------------------------------------------------------------------
// FUNCTION:    CvTempUnit::CvTempUnit
//! \brief      Default constructor.
//------------------------------------------------------------------------------------------------

CvTempUnit::CvTempUnit() :
	m_iId(-1),
	m_eUnitType(NO_UNIT),
	m_eOwner(NO_PLAYER),
	m_eProfessionType(NO_PROFESSION)
{
	reset(0, NO_UNIT, NO_PLAYER, NO_PROFESSION, true);
}

CvTempUnit::~CvTempUnit()
{
	uninit();
}

void CvTempUnit::init(int iID, UnitTypes eUnit, PlayerTypes eOwner, ProfessionTypes eProfession)
{

	FAssert(NO_UNIT != eUnit);

	//--------------------------------
	// Init saved data
	reset(iID, eUnit, eOwner, eProfession);

	setGameTurnCreated(GC.getGameINLINE().getGameTurn());
}

void CvTempUnit::uninit()
{

}

// FUNCTION: reset()
// Initializes data members that are serialized.
void CvTempUnit::reset(int iID, UnitTypes eUnit, PlayerTypes eOwner, ProfessionTypes eProfession, bool bConstructorCall)
{
	//--------------------------------
	// Uninit class
	uninit();

	m_iId = iID;
	m_iTurnCreated = 0;
	m_iPrice = 0;
	m_iDamage = 0;
	m_iPromotionLevel = 0;

	m_bRequestTraining = false;

	m_eUnitType = eUnit;
	m_eOwner = eOwner;
	m_eProfessionType = eProfession;
	m_pUnitInfo = (NO_UNIT != m_eUnitType) ? &GC.getUnitInfo(m_eUnitType) : NULL;
	m_eImmigrationType = NO_IMMIGRATION;


	if (!bConstructorCall)
	{
		//nothing to do yet
	}
}

int CvTempUnit::getID() const
{
	return m_iId;
}

void CvTempUnit::setID(int iId)
{
	m_iId = iId;
}

int CvTempUnit::getGameTurnCreated() const
{
	return m_iTurnCreated;
}

void CvTempUnit::setGameTurnCreated(int iTurn)
{
	m_iTurnCreated = iTurn;
}

int CvTempUnit::getPrice() const
{
	return m_iPrice;
}

void CvTempUnit::setPrice(int iPrice)
{
	m_iPrice = iPrice;
}

int CvTempUnit::getDamage() const
{
	return m_iDamage;
}

void CvTempUnit::setDamage(int iDamage)
{
	m_iDamage = iDamage;
}

int CvTempUnit::getPromotionLevel() const
{
	return m_iPromotionLevel;
}

void CvTempUnit::setPromotionLevel(int iPromotionLevel)
{
	m_iPromotionLevel = iPromotionLevel;
}

bool CvTempUnit::requestTraining() const
{
	return m_bRequestTraining;
}

void CvTempUnit::setRequestTraining(bool bNewValue)
{
	m_bRequestTraining = bNewValue;
}

UnitTypes CvTempUnit::getUnitType() const
{
	return m_eUnitType;
}

CvUnitInfo &CvTempUnit::getUnitInfo() const
{
	return *m_pUnitInfo;
}

ProfessionTypes CvTempUnit::getProfession() const
{
	return m_eProfessionType;
}

void CvTempUnit::setProfession(ProfessionTypes eProfession)
{
	if (getProfession() != eProfession)
	{
		m_eProfessionType = eProfession;
	}
}

ImmigrationTypes CvTempUnit::getImmigrationType() const
{
	return m_eImmigrationType;
}

void CvTempUnit::setImmigrationType(ImmigrationTypes eType)
{
	if (eType != getImmigrationType())
	{
		m_eImmigrationType = eType;
	}
}

PlayerTypes CvTempUnit::getOwner() const
{
	return m_eOwner;
}

CivilizationTypes CvTempUnit::getCivilizationType() const
{
	return GET_PLAYER(getOwner()).getCivilizationType();
}

const CvArtInfoUnit* CvTempUnit::getArtInfo(int i) const
{
	return m_pUnitInfo->getUnitArtStylesArtInfo(i, getProfession(), (UnitArtStyleTypes) GC.getCivilizationInfo(getCivilizationType()).getUnitArtStyleType());
}

const TCHAR* CvTempUnit::getFullLengthIcon() const
{
	const CvArtInfoUnit* pArtInfo = getArtInfo(0);

	if (NULL != pArtInfo)
	{
		return pArtInfo->getFullLengthIcon();
	}

	return NULL;
}
const CvWString CvTempUnit::getName(uint uiForm) const
{
	return m_pUnitInfo->getDescription(uiForm);
}
const CvWString CvTempUnit::getNameAndProfession() const
{
	CvWString szText;

	if (NO_PROFESSION != getProfession())
	{
		szText.Format(L"%s (%s)", GC.getProfessionInfo(getProfession()).getDescription(), getName().GetCString());
	}
	else
	{
		szText = getName();
	}

	return szText;
}

CvUnit* CvTempUnit::doRecruitImmigrant()
{
	CvPlayerAI& kPlayer = GET_PLAYER(getOwner());
	UnitTypes eUnit = getUnitType();
	UnitClassTypes eUnitClass = (UnitClassTypes) GC.getUnitInfo(eUnit).getUnitClassType();
	if (eUnit == NO_UNIT) 
	{
		return NULL;
	}

	CvUnit* pNewUnit = kPlayer.initEuropeUnit(eUnit);
	if (pNewUnit != NULL) 
	{
		if (pNewUnit->getUnitInfo().getEnrolType() != 3)
		{
			pNewUnit->setLikelyToLeave(true);
		}
		pNewUnit->setEuropeRecruitPrice(getPrice());
		kPlayer.removeTempUnit(getID());
		kPlayer.changeEnrolUnitType(eUnitClass, false, false);

		if (pNewUnit->AI_shouldBeReservedForWarPlan()) 
		{
			pNewUnit->AI_setReservedForWarPlan(true);
			pNewUnit->setLikelyToLeave(false);
			pNewUnit->AI_setUnitAIType(kPlayer.AI_getUnitAIForCurrentWarPlan());
			kPlayer.AI_decreaseWarPlanUnitClass(eUnitClass);
		} 
		else 
		{
			CvCity* pCity = kPlayer.getBestWorkingCity(eUnitClass);
			if (pCity != NULL) 
			{	
				pCity->assignToCityJob(pNewUnit);
				pNewUnit->setWorkingCity(pCity);
				pNewUnit->setDestinationCity(pCity->getID());
			}
			if (kPlayer.isHuman())
			{
				gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
			}
		}
	}
	return pNewUnit;
}

void CvTempUnit::buyShip(int iPrice)
{
	CvPlayerAI& kPlayer = GET_PLAYER(getOwner());
	UnitTypes eUnit = getUnitType();

	CvPlot* pStartingPlot = kPlayer.getStartingPlot();
	CvUnit* pNewUnit = NULL;
	if (eUnit != NO_UNIT  && pStartingPlot != NULL)
	{
		pNewUnit = kPlayer.initUnit(eUnit, NO_PROFESSION, INVALID_PLOT_COORD, INVALID_PLOT_COORD);
	}
	if (pNewUnit != NULL)
	{
		kPlayer.changeGold(-iPrice);
		pNewUnit->setUnitTravelState(UNIT_TRAVEL_STATE_IN_EUROPE, false);
		pNewUnit->setDamage(getDamage());
		pNewUnit->setHasCrew(false);
		pNewUnit->addToMap(pStartingPlot->getX_INLINE(), pStartingPlot->getY_INLINE());
		kPlayer.removeTempUnit(getID());
		if (kPlayer.isHuman())
		{
			gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen2_DIRTY_BIT, true);
		}
		else
		{
			UnitClassTypes eUnitClass = (UnitClassTypes) GC.getUnitInfo(eUnit).getUnitClassType();
			if (kPlayer.AI_getWarPlanUnitClass(eUnitClass) > 0)
			{
				pNewUnit->AI_setReservedForWarPlan(true);
				kPlayer.AI_decreaseWarPlanUnitClass(eUnitClass);
			}
		}
	}
}

int CvTempUnit::getSailorPrice() const
{
	int iPrice = 0;
	int iRange = 0;
	int iPromoLevel = getPromotionLevel();
	switch(iPromoLevel)
	{
	case 1:
	case 2:
		iRange = 400;
		break;
	case 3:
		iRange = 600;
		break;
	case 4:
		iRange = 800;
		break;
	case 5:
		iRange = 1000;
		break;
	default:
		FAssertMsg(false, "getSailorPrice bad promotion level");
	}
	if (iRange > 0)
	{
		int iRandom = GC.getGameINLINE().getSorenRandNum(iRange, "getSailorPrice");
		iPrice = 400 + iPromoLevel*200 + iRandom;
	}
	return iPrice;
}

void CvTempUnit::generatePromotionLevel()
{
	int iRandom = GC.getGameINLINE().getSorenRandNum(100, "generatePromotionLevel");
	int iPromoLevel = 0;
	if (iRandom < 51)
	{
		iPromoLevel = 1;
	}
	else if (iRandom < 77)
	{
		iPromoLevel = 2;
	}
	else if (iRandom < 90)
	{
		iPromoLevel = 3;
	}
	else if (iRandom < 97)
	{
		iPromoLevel = 4;
	}
	else
	{
		iPromoLevel = 5;
	}
	FAssertMsg(iPromoLevel > 0, "iPromoLevel excepted to be > 0");
	setPromotionLevel(iPromoLevel);
}

void CvTempUnit::generatePrice()
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	int iPrice = 0;
	if (getProfession() == PROFESSION_SAILOR)
	{
		iPrice = getSailorPrice();
	}
	else if (m_pUnitInfo->isMechUnit())
	{
		iPrice = kPlayer.getEuropeUnitBuyPrice(getUnitType())*75/100;
		iPrice -= iPrice*getDamage()/250;

		PlayerTypes eEuropePlayer = kPlayer.getParent();
		if (eEuropePlayer != NO_PLAYER )
		{
			iPrice += iPrice*GET_PLAYER(eEuropePlayer).getUnitMarketPrice((UnitClassTypes) m_pUnitInfo->getUnitClassType())/100;
		}
	}
	setPrice(iPrice);
}

void CvTempUnit::doRecruitCrew(int iFormationType)
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	UnitTypes eUnit = getUnitType();
	CvUnit* pNewUnit = NULL;
	if (eUnit != NO_UNIT)
	{
		pNewUnit = kPlayer.initEuropeUnit(eUnit);
	}
	if (pNewUnit != NULL)
	{
		if (!requestTraining())
		{
			pNewUnit->setCanBeSailor(true);
			pNewUnit->setProfession(PROFESSION_SAILOR, true);			
			pNewUnit->addSailorPromotions(getPromotionLevel());
		}
		else
		{
			bool bMerchantSailor = (iFormationType == 1);
			int iCost = GC.getDefineINT(bMerchantSailor ? "MERCHANT_CREW_FORMATION_PRICE" : "MILITARY_CREW_FORMATION_PRICE");
			int iTurn = GC.getDefineINT(bMerchantSailor ? "MERCHANT_CREW_FORMATION_TURN" : "MILITARY_CREW_FORMATION_TURN");
			kPlayer.changeGold(-iCost);
			pNewUnit->setCrewFormationTurn(iTurn);
			pNewUnit->setLikelyToLeave(false);
			pNewUnit->setProfession(PROFESSION_SAILOR, true);
			pNewUnit->addSailorPromotions(0, 1, bMerchantSailor ? 0 : 1);
		}
	}
}

void CvTempUnit::read(FDataStreamBase* pStream)
{
	// Init data before load
	reset();

	uint uiFlag = 0;
	pStream->Read(&uiFlag);	// flags for expansion

	pStream->Read(&m_iId);
	pStream->Read(&m_iTurnCreated);
	pStream->Read(&m_iPrice);
	pStream->Read(&m_iDamage);
	pStream->Read(&m_iPromotionLevel);

	pStream->Read(&m_bRequestTraining);

	pStream->Read((int*)&m_eUnitType);
	FAssert(NO_UNIT != m_eUnitType);
	m_pUnitInfo = (NO_UNIT != m_eUnitType) ? &GC.getUnitInfo(m_eUnitType) : NULL;
	pStream->Read((int*)&m_eOwner);
	pStream->Read((int*)&m_eProfessionType);
	pStream->Read((int*)&m_eImmigrationType);

}

void CvTempUnit::write(FDataStreamBase* pStream)
{
	uint uiFlag = 0;
	pStream->Write(uiFlag);		// flag for expansion

	pStream->Write(m_iId);
	pStream->Write(m_iTurnCreated);
	pStream->Write(m_iPrice);
	pStream->Write(m_iDamage);
	pStream->Write(m_iPromotionLevel);

	pStream->Write(m_bRequestTraining);

	pStream->Write(m_eUnitType);
	pStream->Write(m_eOwner);
	pStream->Write(m_eProfessionType);
	pStream->Write(m_eImmigrationType);
}
/*
double CvTempUnit::Log2(double n)  
{  
// log(n)/log(2) is log2.  
return log( n ) / log( 2 );  
}
*/