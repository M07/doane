//------------------------------------------------------------------------------------------------
//
//  ***************** CIV4 GAME ENGINE   ********************
//
//! \file		CvHospitals.cpp
//! \author		M07
//! \brief		Implementation of hospitals
//
//------------------------------------------------------------------------------------------------
//  Copyright (c) 2012 Fratelli Marconni, Inc. All rights reserved.
//------------------------------------------------------------------------------------------------

#include "CvGameCoreDLL.h"
#include "CvPlayerAI.h"
#include "CvInfos.h"
#include "CvHospitals.h"

//------------------------------------------------------------------------------------------------
// FUNCTION:    CvHospitals::CvHospitals
//! \brief      Default constructor.
//------------------------------------------------------------------------------------------------
CvHospitals::CvHospitals() :
	m_iId(ANY_HOSPITALS_ID),
	m_iStock(0),
	m_iConsumption(0)
{
}

CvHospitals::~CvHospitals()
{
}

void  CvHospitals::init(PlayerTypes eOwner)
{
	m_eOwner = eOwner;
}

int CvHospitals::getID() const
{
	return m_iId;
}

void CvHospitals::setID(int iId)
{
	m_iId = iId;
}

PlayerTypes CvHospitals::getOwner() const
{
	return m_eOwner;
}

int CvHospitals::getStock() const
{
	return m_iStock;
}

void CvHospitals::setStock(int iNewValue)
{
	m_iStock = iNewValue;
}

void CvHospitals::changeStock(int iChange)
{
	setStock(getStock()+iChange);
}

int CvHospitals::getConsumption() const
{
	return m_iConsumption;
}

void CvHospitals::setConsumption(int iNewValue)
{
	m_iConsumption = iNewValue;
}

void CvHospitals::changeConsumption(int iChange)
{
	setConsumption(getConsumption()+iChange);
}

BuildingTypes CvHospitals::getBuilding() const
{
	BuildingClassTypes eBuildingClass = (BuildingClassTypes) GC.getDefineINT("HOSPITAL_BUILDINGCLASS");
	if (eBuildingClass != NO_BUILDINGCLASS)
	{
		return ((BuildingTypes)(GC.getCivilizationInfo(GET_PLAYER(getOwner()).getCivilizationType()).getCivilizationBuildings(eBuildingClass)));
	}
	return NO_BUILDING;
}

int CvHospitals::getTotalNumber() const
{
	int iCount = 0, iLoop;
	BuildingTypes eBuilding = getBuilding();
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvCity* pLoopCity;

	for (pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
	{
		if (pLoopCity->isHasRealBuilding(eBuilding))
		{
			iCount++;
		}
	}
	return iCount;
}

int CvHospitals::getHealRate() const
{
	return std::min(9+3*getConsumption()+getTotalNumber(), 30);
}

int CvHospitals::getFoodKeptRate() const
{
	return std::min(getConsumption()+getTotalNumber(), 10);
}

int CvHospitals::getBirthRate(int iFoodProduction) const
{
	return iFoodProduction ? (100 - getFoodKeptRate())/iFoodProduction : -1;
}

int CvHospitals::calculateClothNetYield() const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvCity* pLoopCity;
	int iTotal = 0, iLoop;

	for (pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
	{
		if (pLoopCity->isAutoHospital())
		{
			iTotal += pLoopCity->calculateNetYield(YIELD_CLOTH);
		}
	}

	return iTotal;
}

int CvHospitals::getNetClothYield(bool bAuto, int iStockCity, int iProduction) const
{
	int iLoss = 0;
	int iNumHospitals = getTotalNumber();
	int iConsumption = getConsumption();
	if (iNumHospitals > 0)
	{
		if (iConsumption > 0)
		{
			if (bAuto)
			{
				if ((iConsumption*iNumHospitals - iProduction) < 0)
				{
					return -1;
				}
				if ((iConsumption*iNumHospitals - iProduction) > 0)
				{
					iLoss = (getStock() + iStockCity)/(iConsumption*iNumHospitals - iProduction);
				}
			}
			else
			{
				iLoss = getStock()/(iConsumption*iNumHospitals);
			}
		}
		else
		{
			if ((iProduction) > 0)
			{
				return -1;
			}
		}
	}
	return iLoss;
}

int CvHospitals::getMaxStock() const
{
	return 100*getTotalNumber();
}

int CvHospitals::getMaxConsumption() const
{	
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	int iLoop, iTotalCloth = getStock();
	int iNumHospitals = getTotalNumber();
	FAssertMsg(iNumHospitals > 0, "No hospitals in cities" );

	CvCity* pLoopCity;
	for (pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
	{
		if (pLoopCity->isAutoHospital())
		{
			iTotalCloth += pLoopCity->getYieldStored(YIELD_CLOTH);
			iTotalCloth += pLoopCity->calculateNetYield(YIELD_CLOTH);
		}
	}
	return std::min(5, iTotalCloth/iNumHospitals);
}

void CvHospitals::transferToCity(CvCity* pCity, int iChange)
{
	changeStock(iChange);
	pCity->changeYieldStored(YIELD_CLOTH, -iChange);
	setConsumption(std::min(getMaxConsumption(), getTotalNumber()*getConsumption()));
}

void CvHospitals::doTurn()
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	BuildingTypes eBuilding = getBuilding();
	int iNumHospitals = getTotalNumber();
	int iConsumptionInTurn = iNumHospitals*getConsumption();
	int iMaxStock = getMaxStock();
	YieldTypes eYield = YIELD_CLOTH;

	FAssertMsg(eBuilding != NO_BUILDING, "No building for hospitals" );

	int iLoop;
	CvCity* pLoopCity;

	for (pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
	{
		if (pLoopCity->isHasRealBuilding(eBuilding))
		{
			if (pLoopCity->isAutoHospital())
			{
				int iYieldStored = pLoopCity->getYieldStored(eYield);
				if (iYieldStored > 0)
				{
					if (getStock() < iMaxStock)
					{
						int iTransfer = std::min(iYieldStored, 2*iConsumptionInTurn);//Useless to transfer all clothes
						iTransfer = std::min(iTransfer, iMaxStock-getStock());//Avoid to have more clothes than the max value
						changeStock(iTransfer);
						pLoopCity->changeYieldStored(eYield, -iTransfer);
					}
				}
			}
			pLoopCity->setFoodKept(getFoodKeptRate());
		}
		else
		{
			pLoopCity->setFoodKept(0);
		}
	}

	if (iNumHospitals == 0)
	{
		return;
	}

	setConsumption(std::min(getStock(), iConsumptionInTurn)/iNumHospitals);
	changeStock(-iConsumptionInTurn);
	setConsumption(std::min(getStock(), iConsumptionInTurn)/iNumHospitals);
}

void CvHospitals::read(FDataStreamBase* pStream)
{
	uint uiFlag = 0;
	pStream->Read(&uiFlag);	// flags for expansion

	pStream->Read(&m_iId);
	pStream->Read(&m_iStock);
	pStream->Read(&m_iConsumption);

	pStream->Read((int*)&m_eOwner);
}

void CvHospitals::write(FDataStreamBase* pStream)
{
	uint uiFlag = 0;
	pStream->Write(uiFlag);		// flag for expansion

	pStream->Write(m_iId);
	pStream->Write(m_iStock);
	pStream->Write(m_iConsumption);

	pStream->Write(m_eOwner);
}
