#include "CvGameCoreDLL.h"
#include "CyTradeRoute.h"
//
// published python interface for CyTradeRoute
//

void CyTradeRoutePythonInterface()
{
	OutputDebugString("Python Extension Module - CyTradeRoutePythonInterface\n");

	python::class_<CyTradeRoute>("CyTradeRoute")
		.def("getID", &CyTradeRoute::getID, "int ()")
		.def("getGoldAmountToAlwaysConserve", &CyTradeRoute::getGoldAmountToAlwaysConserve, "int ()")
		.def("getName", &CyTradeRoute::getName, "string ()")
		.def("setName", &CyTradeRoute::setName, "void (str)")
		.def("getSourceCity", &CyTradeRoute::getSourceCity, "IDInfo ()")
		.def("getDestinationCity", &CyTradeRoute::getDestinationCity, "IDInfo ()")
		.def("getSourceCityNameKey", &CyTradeRoute::getSourceCityNameKey, "wstring ()")
		.def("getSourceCityName", &CyTradeRoute::getSourceCityName, "wstring ()")
		.def("getDestinationCityNameKey", &CyTradeRoute::getDestinationCityNameKey, "wstring ()")
		.def("getDestinationCityName", &CyTradeRoute::getDestinationCityName, "wstring ()")
		.def("getOwner", &CyTradeRoute::getOwner, "PlayerTypes ()")
		.def("getTradeType", &CyTradeRoute::getTradeType, "TradeTypes (int)")
		.def("getRadioOption", &CyTradeRoute::getRadioOption, "OptionInfo (int, int)")
		.def("getEuropeanRadioOption", &CyTradeRoute::getEuropeanRadioOption, "AdvancedOptionInfo (int, int)")
		.def("getCheckBoxOption", &CyTradeRoute::getCheckBoxOption, "OptionInfo (int, int)")
		.def("getTransportUnitsCapacity", &CyTradeRoute::getTransportUnitsCapacity, "int ()")
		.def("getYieldValue", &CyTradeRoute::getYieldValue, "int (int)")
		.def("getLoadedCargoPercent", &CyTradeRoute::getLoadedCargoPercent, "int ()")
		.def("getLoadedCargoHistPercent", &CyTradeRoute::getLoadedCargoHistPercent, "int ()")
		.def("getTradeProfits", &CyTradeRoute::getTradeProfits, "int ()")
		.def("getTradeProfitsHist", &CyTradeRoute::getTradeProfitsHist, "int ()")
		.def("getTotalProfits", &CyTradeRoute::getTotalProfits, "unsigned int ()")
		.def("getTransportType", &CyTradeRoute::getTransportType, "TransportTypes ()")
		
	;
}
