// player.cpp

#include "CvGameCoreDLL.h"
#include "CvGlobals.h"
#include "CvArea.h"
#include "CvGameAI.h"
#include "CvMap.h"
#include "CvPlot.h"
#include "CvRandom.h"
#include "CvTeamAI.h"
#include "CvGameCoreUtils.h"
#include "CvPlayerAI.h"
#include "CvPlayer.h"
#include "CvMarketTransaction.h"
#include "CvArtFileMgr.h"
#include "CvDiploParameters.h"
#include "CvInitCore.h"
#include "CyArgsList.h"
#include "CvInfos.h"
#include "CvPopupInfo.h"
#include "CvDiploParameters.h"
#include "FProfiler.h"
#include "CvGameTextMgr.h"
#include "CyCity.h"
#include "CyPlot.h"
#include "CvTrade.h"
#include "CvCityJob.h"
#include "CvHospitals.h"
#include "CvTradeRoute.h"
#include "CvAgreement.h"
#include "CvConvoy.h"
#include "CvTempUnit.h"
#include <numeric>

#include "CvDLLInterfaceIFaceBase.h"
#include "CvDLLEntityIFaceBase.h"
#include "CvDLLEngineIFaceBase.h"
#include "CvDLLFAStarIFaceBase.h"
#include "CvDLLEventReporterIFaceBase.h"
#include "CvDLLPythonIFaceBase.h"
#include <stdio.h>
// Public Functions...

CvPlayer::CvPlayer()
{
	m_aiSeaPlotYield = new int[NUM_YIELD_TYPES];
	m_aiYieldRateModifier = new int[NUM_YIELD_TYPES];
	m_aiCapitalYieldRateModifier = new int[NUM_YIELD_TYPES];
	m_aiBuildingRequiredYieldModifier = new int[NUM_YIELD_TYPES];
	m_aiCityExtraYield = new int[NUM_YIELD_TYPES];
	m_aiExtraYieldThreshold = new int[NUM_YIELD_TYPES];
	m_aiYieldRandomPercent = new int[NUM_YIELD_TYPES];
	m_aiYieldTradedTotal = new int[NUM_YIELD_TYPES];
	m_aiYieldBoughtTotal = new int[NUM_YIELD_TYPES];
	m_aiTaxYieldModifierCount = new int[NUM_YIELD_TYPES];
	m_aiEuropeWarehouseYield = new int[NUM_YIELD_TYPES];
	m_aiEuropeSellTradeYield = new int[NUM_YIELD_TYPES];
	m_aiEuropeGoldSellTradeYield = new int[NUM_YIELD_TYPES];
	m_aiEuropeBuyTradeYield = new int[NUM_YIELD_TYPES];
	m_aiEuropeGoldBuyTradeYield = new int[NUM_YIELD_TYPES];
	m_aiEuropeTaxTradeYield = new int[NUM_YIELD_TYPES];
	m_aucEuropeNeeds = new unsigned char[NUM_YIELD_TYPES];
	m_aiNativeGetUnknownYield = new int[NUM_YIELD_TYPES];
	m_aiIATradeProposition = new int[NUM_YIELD_TYPES];
	m_aiHumanTradeProposition = new int[NUM_YIELD_TYPES];
	m_aiPlayerValue = new int[NUM_YIELD_TYPES];
	m_aiMissionaryPoints = new int[MAX_PLAYERS];
	m_aiMissionaryThresholdMultiplier = new int[MAX_PLAYERS];
	m_aiRegularityTrade = new int[MAX_PLAYERS];
	m_aiUnemploymentHistory = new int[NUM_HIST_TURN];
	m_aiFoodHistory = new int[NUM_HIST_TURN];
	m_aiEconomicHistory = new int[NUM_HIST_TURN];
	m_aiReligionHistory = new int[NUM_HIST_TURN];

	m_aucRandomValues = new unsigned char[NUM_RAND_VALUES];
	m_aucUnitGivedByKing = new unsigned char[NUM_UNIT_TYPES];
	m_aucEconomicTrends = new unsigned char[NUM_YIELD_TYPES];
	m_aucEconomicTrendsRemainingTurns = new unsigned char[NUM_YIELD_TYPES];

	m_abYieldEuropeTradable = new bool[NUM_YIELD_TYPES];
	m_abYieldUnknown = new bool[NUM_YIELD_TYPES];
	m_abPinnedYieldInEurope = new bool[NUM_YIELD_TYPES];
	m_abAutomaticTradeByYieldWithNative = new bool[NUM_YIELD_TYPES];
	m_abRegularityTradeInTurn = new bool[NUM_YIELD_TYPES];
	m_abFeatAccomplished = new bool[NUM_FEAT_TYPES];
	m_abOptions = new bool[NUM_PLAYEROPTION_TYPES];
	m_abSecondaryOptions = new bool[NUM_SECONDARYPLAYEROPTION_TYPES];
	m_abHadTutorialDoane = new bool[NUM_TUTORIAL_DOANE];
	m_abUnitUsedMap = new bool[NUM_MAPS];
	m_abLaunchedPopup = new bool[NUM_BUTTONPOPUP_TYPES];

	m_paiImprovementCount = NULL;
	m_paiFreeBuildingCount = NULL;
	m_pabFirstBuildingBuilt = NULL;
	m_pabHasGeneratedPirates = NULL;
	m_paiUnitClassCount = NULL;
	m_paiEnrolUnitType = NULL;
	m_paiUnitClassMaking = NULL;
	m_paiUnitMarketPrice = NULL;
	m_paiUnitKilledCounter = NULL;
	m_paiUnitTurnRemaining = NULL;
	m_paiUnitMoveChange = NULL;
	m_paiUnitStrengthModifier = NULL;
	m_paiProfessionCombatChange = NULL;
	m_paiProfessionMoveChange = NULL;
	m_paiBuildingClassCount = NULL;
	m_paiBuildingClassMaking = NULL;
	m_paiSpecialBuildingNotRequiredCount = NULL;
	m_aiProfessionEquipmentModifier = NULL;
	m_aiTraitCount = NULL;

	m_paeCivics = NULL;

	m_ppiImprovementYieldChange = NULL;
	m_ppiBuildingYieldChange = NULL;

	reset(NO_PLAYER, true);
}


CvPlayer::~CvPlayer()
{
	uninit();

	SAFE_DELETE_ARRAY(m_aiSeaPlotYield);
	SAFE_DELETE_ARRAY(m_aiYieldRateModifier);
	SAFE_DELETE_ARRAY(m_aiCapitalYieldRateModifier);
	SAFE_DELETE_ARRAY(m_aiBuildingRequiredYieldModifier);
	SAFE_DELETE_ARRAY(m_aiCityExtraYield);
	SAFE_DELETE_ARRAY(m_aiExtraYieldThreshold);
	SAFE_DELETE_ARRAY(m_aiYieldRandomPercent);
	SAFE_DELETE_ARRAY(m_aiYieldTradedTotal);
	SAFE_DELETE_ARRAY(m_aiYieldBoughtTotal);
	SAFE_DELETE_ARRAY(m_aiTaxYieldModifierCount);
	SAFE_DELETE_ARRAY(m_aiEuropeWarehouseYield);
	SAFE_DELETE_ARRAY(m_aiEuropeSellTradeYield);
	SAFE_DELETE_ARRAY(m_aiEuropeGoldSellTradeYield);
	SAFE_DELETE_ARRAY(m_aiEuropeBuyTradeYield);
	SAFE_DELETE_ARRAY(m_aiEuropeGoldBuyTradeYield);
	SAFE_DELETE_ARRAY(m_aiEuropeTaxTradeYield);
	SAFE_DELETE_ARRAY(m_aucEuropeNeeds);
	SAFE_DELETE_ARRAY(m_aiNativeGetUnknownYield);
	SAFE_DELETE_ARRAY(m_aiIATradeProposition);
	SAFE_DELETE_ARRAY(m_aiHumanTradeProposition);
	SAFE_DELETE_ARRAY(m_aiPlayerValue);
	SAFE_DELETE_ARRAY(m_aiMissionaryPoints);
	SAFE_DELETE_ARRAY(m_aiMissionaryThresholdMultiplier);
	SAFE_DELETE_ARRAY(m_aiRegularityTrade);
	SAFE_DELETE_ARRAY(m_aiFoodHistory);
	SAFE_DELETE_ARRAY(m_aiUnemploymentHistory);
	SAFE_DELETE_ARRAY(m_aiEconomicHistory);
	SAFE_DELETE_ARRAY(m_aiReligionHistory);
	SAFE_DELETE_ARRAY(m_aucRandomValues);
	SAFE_DELETE_ARRAY(m_aucUnitGivedByKing);
	SAFE_DELETE_ARRAY(m_aucEconomicTrends);
	SAFE_DELETE_ARRAY(m_aucEconomicTrendsRemainingTurns);
	SAFE_DELETE_ARRAY(m_abYieldEuropeTradable);
	SAFE_DELETE_ARRAY(m_abYieldUnknown);
	SAFE_DELETE_ARRAY(m_abPinnedYieldInEurope);
	SAFE_DELETE_ARRAY(m_abAutomaticTradeByYieldWithNative);
	SAFE_DELETE_ARRAY(m_abRegularityTradeInTurn);
	SAFE_DELETE_ARRAY(m_abFeatAccomplished);
	SAFE_DELETE_ARRAY(m_abOptions);
	SAFE_DELETE_ARRAY(m_abSecondaryOptions);
	SAFE_DELETE_ARRAY(m_abHadTutorialDoane);
	SAFE_DELETE_ARRAY(m_abUnitUsedMap);
	SAFE_DELETE_ARRAY(m_abLaunchedPopup);
}


void CvPlayer::init(PlayerTypes eID)
{
	//--------------------------------
	// Init saved data
	reset(eID);

	//assign europe civilization as parent
	for (int iParent = 0; iParent < MAX_PLAYERS; ++iParent)
	{
		CvPlayer& kParent = GET_PLAYER((PlayerTypes) iParent);
		if (kParent.getCivilizationType() != NO_CIVILIZATION)
		{
			if (GC.getCivilizationInfo(kParent.getCivilizationType()).getDerivativeCiv() == getCivilizationType())
			{
				setParent((PlayerTypes) iParent);
				break;
			}
		}
	}

	definePlayerRole();

	//--------------------------------
	// Init containers
	m_cities.init();

	m_tradeRoutes.reset();
	m_agreements.reset();
	m_hospitals.reset();
	m_tempUnits.reset();
	m_cityJobs.reset();
	m_marketTransactions.reset();
	m_convoys.reset();
	m_mappedMarketTransactions.clear();
	m_units.reset();
	freeEuropeUnits();

	m_selectionGroups.init();

	m_eventsTriggered.init();

	//--------------------------------
	// Init non-saved data
	setupGraphical();

	//--------------------------------
	// Init other game data
	FAssert(getTeam() != NO_TEAM);
	GET_TEAM(getTeam()).changeNumMembers(1);

	if ((GC.getInitCore().getSlotStatus(getID()) == SS_TAKEN) || (GC.getInitCore().getSlotStatus(getID()) == SS_COMPUTER))
	{
		setAlive(true);

		FAssertMsg((GC.getNumTraitInfos() > 0), "GC.getNumTraitInfos() is less than or equal to zero but is expected to be larger than zero in CvPlayer::init");
		for (int iI = 0; iI < GC.getNumTraitInfos(); iI++)
		{
			if (GC.getCivilizationInfo(getCivilizationType()).hasTrait(iI))
			{
				processTrait((TraitTypes) iI, 1);
			}

			if (GC.getLeaderHeadInfo(getLeaderType()).hasTrait(iI))
			{
				processTrait((TraitTypes) iI, 1);
			}
		}

		for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
		{
			updateExtraYieldThreshold((YieldTypes)iI);
			updateCityExtraYield((YieldTypes) iI);
		}

		for (int iI = 0; iI < GC.getNumCivicOptionInfos(); iI++)
		{
			setCivic(((CivicOptionTypes)iI), ((CivicTypes)(GC.getCivilizationInfo(getCivilizationType()).getCivilizationInitialCivics(iI))));
		}

		for (int iI = 0; iI < GC.getNumEventInfos(); iI++)
		{
			resetEventOccured((EventTypes)iI, false);
		}

		for (int iI = 0; iI < GC.getNumEventTriggerInfos(); iI++)
		{
			resetTriggerFired((EventTriggerTypes)iI);
		}
	}

	initYieldUnknow();
	AI_init();
	initYieldsPlayerValue();
	initEuropeNeeds();
}


void CvPlayer::uninit()
{
	int iI;

	SAFE_DELETE_ARRAY(m_paiImprovementCount);
	SAFE_DELETE_ARRAY(m_paiFreeBuildingCount);
	SAFE_DELETE_ARRAY(m_pabFirstBuildingBuilt);
	SAFE_DELETE_ARRAY(m_pabHasGeneratedPirates);
	SAFE_DELETE_ARRAY(m_paiUnitClassCount);
	SAFE_DELETE_ARRAY(m_paiEnrolUnitType);
	SAFE_DELETE_ARRAY(m_paiUnitClassMaking);
	SAFE_DELETE_ARRAY(m_paiUnitMarketPrice);
	SAFE_DELETE_ARRAY(m_paiUnitKilledCounter);
	SAFE_DELETE_ARRAY(m_paiUnitTurnRemaining);
	SAFE_DELETE_ARRAY(m_paiUnitMoveChange);
	SAFE_DELETE_ARRAY(m_paiUnitStrengthModifier);
	SAFE_DELETE_ARRAY(m_paiProfessionCombatChange);
	SAFE_DELETE_ARRAY(m_paiProfessionMoveChange);
	SAFE_DELETE_ARRAY(m_paiBuildingClassCount);
	SAFE_DELETE_ARRAY(m_paiBuildingClassMaking);
	SAFE_DELETE_ARRAY(m_paiSpecialBuildingNotRequiredCount);
	SAFE_DELETE_ARRAY(m_aiProfessionEquipmentModifier);
	SAFE_DELETE_ARRAY(m_aiTraitCount);

	SAFE_DELETE_ARRAY(m_paeCivics);

	m_triggersFired.clear();

	if (m_ppiImprovementYieldChange != NULL)
	{
		for (iI = 0; iI < GC.getNumImprovementInfos(); iI++)
		{
			SAFE_DELETE_ARRAY(m_ppiImprovementYieldChange[iI]);
		}
		SAFE_DELETE_ARRAY(m_ppiImprovementYieldChange);
	}
	if (m_ppiBuildingYieldChange != NULL)
	{
		for (iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
		{
			SAFE_DELETE_ARRAY(m_ppiBuildingYieldChange[iI]);
		}
		SAFE_DELETE_ARRAY(m_ppiBuildingYieldChange);
	}

	m_groupCycle.clear();
	m_aszCityNames.clear();

	m_cities.uninit();

	m_seaways.clear();

	m_tradeRoutes.reset();
	m_agreements.reset();
	m_hospitals.reset();
	m_tempUnits.reset();
	m_cityJobs.reset();
	m_marketTransactions.reset();
	m_convoys.reset();
	m_mappedMarketTransactions.clear();
	m_units.reset();
	freeEuropeUnits();

	m_selectionGroups.uninit();

	m_eventsTriggered.uninit();

	clearMessages();

	clearPopups();

	clearDiplomacy();
}


// FUNCTION: reset()
// Initializes data members that are serialized.
void CvPlayer::reset(PlayerTypes eID, bool bConstructorCall)
{
	int iI, iJ;

	//--------------------------------
	// Uninit class
	uninit();

	m_iStartingX = INVALID_PLOT_COORD;
	m_iStartingY = INVALID_PLOT_COORD;
	m_iTotalPopulation = 0;
	m_iTotalLand = 0;
	m_iTotalLandScored = 0;
	m_iGold = 0;
	m_iIncomeAndExpendituresRecord = 0;
	m_iNbCross = 0;
	m_iNewEra = -1;
	m_iSelectQuantity = 10;
	m_iNumAgreement = 0;
	m_iLevelReceiveSeedling = 0;
	m_iEuropeWarehouseCapacity = 250;
	m_iLocalPlayerActionPopupData3 = 0;
	m_iEuropeLoanPercent = 0;
	m_iEuropeLoan = 0;
	m_iParentMood = 0;
	m_iTotalImmigrant = 0;
	m_iAcceptImmigrant = 0;
	m_iLeaveImmigrant = 0;
	m_iDistanceToEuropeIA = 0;
	m_iAdvancedStartPoints = -1;
	m_iGreatGeneralsCreated = 0;
	m_iGreatGeneralsThresholdModifier = 0;
	m_iGreatAdmiralsThresholdModifier = 0;
	m_iGreatGeneralRateModifier = 0;
	m_iDomesticGreatGeneralRateModifier = 0;
	m_iImmigrationThresholdMultiplier = 100;
	m_iRevolutionEuropeUnitThresholdMultiplier = 100;
	m_iKingNumUnitMultiplier = 100;
	m_iNativeAngerModifier = 0;
	m_iFreeExperience = 0;
	m_iWorkerSpeedModifier = 0;
	m_iImprovementUpgradeRateModifier = 0;
	m_iMilitaryProductionModifier = 0;
	m_iCityDefenseModifier = 0;
	m_iHighestUnitLevel = 1;
	m_iExpInBorderModifier = 0;
	m_iLevelExperienceModifier = 0;
	m_iCapitalCityID = FFreeList::INVALID_INDEX;
	m_iCitiesLost = 0;
	m_iAssets = 0;
	m_iPower = 0;
	m_iPopulationScore = 0;
	m_iLandScore = 0;
	m_iCombatLandExperience = 0;
	m_iCombatSeaExperience = 0;
	m_iCrossesStored = 0;
	m_iBellsStored = 0;
	m_iTaxRate = 0;
	m_iLastTurnOfTaxesIncrease = 0;
	m_iNativeCombatModifier = 0;
	m_iDominateNativeBordersCount = 0;
	m_iRevolutionEuropeTradeCount = 0;
	m_iMissionaryRateModifier = 0;
	m_iMissionarySuccessPercent = 100;
	m_iAttackFocusEuropeanPlayer = -1;
	m_sImmigrationRelationPoints = 0;
	m_usWorkImprovementPercent = 0;

	m_usColonistIntoColoniesAmount = 0;
	m_usLandForceInsideColoniesAmount = 0;
	m_usLandForceOutsideColoniesAmount = 0;
	m_usUnemployedAmount = 0;
	m_usScoutAmount = 0;
	m_usAgronomistAmount = 0;
	m_usPioneerAmount = 0;
	m_usMerchantShipAmount = 0;
	m_usMilitaryShipAmount = 0;

	m_uiStartTime = 0;
	m_iCurrentTradeRouteId = -1;

	m_bAlive = false;
	m_bTerritorialInfluenceMode = false;
	m_bLaunchFirstPopup = false;
	m_bKingGiveGalleon = false;
	m_bHighlightNewErasButton = false;
	m_bAttackedByPirates = false;
	m_bPenalitePlayerSave = false;
	m_bPenalitePlayerLoad = false;
	m_bEverAlive = false;
	m_bTurnActive = false;
	m_bAutoMoves = false;
	m_bEndTurn = false;
	m_bPbemNewTurn = false;
	m_bExtendedGame = false;
	m_bFoundedFirstCity = false;
	m_bStrike = false;
	m_bDisplaySeedling = false;

	m_eID = eID;
	updateTeamType();
	updateHuman();

	if (m_eID != NO_PLAYER)
	{
		m_ePersonalityType = GC.getInitCore().getLeader(m_eID); //??? Is this repeated data???
	}
	else
	{
		m_ePersonalityType = NO_LEADER;
	}
	m_eCurrentEra = ((EraTypes)0);  //??? Is this repeated data???
	m_eParent = NO_PLAYER;
	m_eImmigrationConversion = YIELD_CROSSES;
	m_ePlayerRole = NO_PLAYER_ROLE;

	for (iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		m_aiSeaPlotYield[iI] = 0;
		m_aiYieldRateModifier[iI] = 0;
		m_aiCapitalYieldRateModifier[iI] = 0;
		m_aiBuildingRequiredYieldModifier[iI] = 0;
		m_aiCityExtraYield[iI] = 0;
		m_aiExtraYieldThreshold[iI] = 0;
		m_aiYieldRandomPercent[iI] = 0;
		m_aiYieldTradedTotal[iI] = 0;
		m_aiYieldBoughtTotal[iI] = 0;
		m_abYieldEuropeTradable[iI] = true;
		m_abYieldUnknown[iI] = false;
		m_abPinnedYieldInEurope[iI] = false;
		m_abAutomaticTradeByYieldWithNative[iI] = false;
		m_aiTaxYieldModifierCount[iI] = 0;
		m_aiEuropeWarehouseYield[iI] = 0;
		m_aiEuropeSellTradeYield[iI] = 0;
		m_aiEuropeGoldSellTradeYield[iI] = 0;
		m_aiEuropeBuyTradeYield[iI] = 0;
		m_aiEuropeGoldBuyTradeYield[iI] = 0;
		m_aiEuropeTaxTradeYield[iI] = 0;
		m_aucEuropeNeeds[iI] = 0;
		m_aiNativeGetUnknownYield[iI] = 0;
		m_aiIATradeProposition[iI] = 0;
		m_aiHumanTradeProposition[iI] = 0;
		m_aiPlayerValue[iI] = 0;
		m_aucEconomicTrends[iI] = NO_ECONOMIC_TREND;
		m_aucEconomicTrendsRemainingTurns[iI] = 0;
	}

	for (iI = 0; iI < MAX_PLAYERS; ++iI)
	{
		m_aiMissionaryPoints[iI] = 0;
		m_aiMissionaryThresholdMultiplier[iI] = 100;
		m_aiRegularityTrade[iI] = 0;
		m_abRegularityTradeInTurn[iI] = false;
	}
	for (iI = 0; iI < NUM_HIST_TURN; ++iI)
	{
		m_aiUnemploymentHistory[iI] = 0;
		m_aiFoodHistory[iI] = 0;
		m_aiEconomicHistory[iI] = 0;
		m_aiReligionHistory[iI] = 0;
	}
	for (iI = 0; iI < NUM_RAND_VALUES; iI++)
	{
		m_aucRandomValues[iI] = 0;
	}

	for (iI = 0; iI < NUM_UNIT_TYPES; iI++)
	{
		m_aucUnitGivedByKing[iI] = 0;
	}

	for (iI = 0; iI < NUM_FEAT_TYPES; iI++)
	{
		m_abFeatAccomplished[iI] = false;
	}

	for (iI = 0; iI < NUM_PLAYEROPTION_TYPES; iI++)
	{
		m_abOptions[iI] = false;
	}

	for (iI = 0; iI < NUM_SECONDARYPLAYEROPTION_TYPES; iI++)
	{
		m_abSecondaryOptions[iI] = false;

		if ((SecondaryPlayerOptionTypes)iI == SECONDARYPLAYEROPTION_KEPT_AUTO_IN_WARTIME)
		{
			m_abSecondaryOptions[iI] = true;
		}
	}

	for (iI = 0; iI < NUM_TUTORIAL_DOANE; iI++)
	{
		m_abHadTutorialDoane[iI] = false;
	}

	for (iI = 0; iI < NUM_MAPS; iI++)
	{
		m_abUnitUsedMap[iI] = false;
	}

	for (iI = 0; iI < NUM_BUTTONPOPUP_TYPES; iI++)
	{
		m_abLaunchedPopup[iI] = false;
	}

	m_szScriptData = "";

	if (!bConstructorCall)
	{
		FAssertMsg(0 < GC.getNumImprovementInfos(), "GC.getNumImprovementInfos() is not greater than zero but it is used to allocate memory in CvPlayer::reset");
		FAssertMsg(m_paiImprovementCount==NULL, "about to leak memory, CvPlayer::m_paiImprovementCount");
		m_paiImprovementCount = new int [GC.getNumImprovementInfos()];
		for (iI = 0; iI < GC.getNumImprovementInfos(); iI++)
		{
			m_paiImprovementCount[iI] = 0;
		}

		FAssertMsg(m_paiFreeBuildingCount==NULL, "about to leak memory, CvPlayer::m_paiFreeBuildingCount");
		m_paiFreeBuildingCount = new int [GC.getNumBuildingInfos()];
		for (iI = 0; iI < GC.getNumBuildingInfos(); iI++)
		{
			m_paiFreeBuildingCount[iI] = 0;
		}

		FAssertMsg(m_pabFirstBuildingBuilt==NULL, "about to leak memory, CvPlayer::m_paiFreeBuildingCount");
		m_pabFirstBuildingBuilt = new bool [GC.getNumBuildingInfos()];
		for (iI = 0; iI < GC.getNumBuildingInfos(); iI++)
		{
			m_pabFirstBuildingBuilt[iI] = false;
		}

		FAssertMsg(m_paiUnitClassCount==NULL, "about to leak memory, CvPlayer::m_paiUnitClassCount");
		m_paiUnitClassCount = new int [GC.getNumUnitClassInfos()];
		FAssertMsg(m_paiEnrolUnitType==NULL, "about to leak memory, CvPlayer::m_paiUnitClassCount");
		m_paiEnrolUnitType = new int [GC.getNumUnitClassInfos()];
		FAssertMsg(m_paiUnitClassMaking==NULL, "about to leak memory, CvPlayer::m_paiUnitClassMaking");
		m_paiUnitClassMaking = new int [GC.getNumUnitClassInfos()];
		FAssertMsg(m_paiUnitMarketPrice==NULL, "about to leak memory, CvPlayer::m_paiUnitMarketPrice");
		m_paiUnitMarketPrice = new int [GC.getNumUnitClassInfos()];
		FAssertMsg(m_paiUnitKilledCounter==NULL, "about to leak memory, CvPlayer::m_paiUnitKilledCounter");
		m_paiUnitKilledCounter = new int [GC.getNumUnitClassInfos()];		
		FAssertMsg(m_paiUnitTurnRemaining==NULL, "about to leak memory, CvPlayer::m_paiUnitTurnRemaining");
		m_paiUnitTurnRemaining = new int [GC.getNumUnitClassInfos()];
		FAssertMsg(m_paiUnitMoveChange==NULL, "about to leak memory, CvPlayer::m_paiUnitMoveChange");
		m_paiUnitMoveChange = new int [GC.getNumUnitClassInfos()];
		FAssertMsg(m_paiUnitStrengthModifier==NULL, "about to leak memory, CvPlayer::m_paiUnitStrengthModifier");
		m_paiUnitStrengthModifier = new int [GC.getNumUnitClassInfos()];

		FAssertMsg(m_pabHasGeneratedPirates==NULL, "about to leak memory, CvPlayer::m_pabHasGeneratedPirates");
		m_pabHasGeneratedPirates = new bool [GC.getNumPirateInfos()];
		for (iI = 0; iI < GC.getNumPirateInfos(); ++iI)
		{
			m_pabHasGeneratedPirates[iI] = false;
		}



		for (iI = 0; iI < GC.getNumUnitClassInfos(); iI++)
		{
			m_paiUnitClassCount[iI] = 0;
			m_paiEnrolUnitType[iI] = 0;
			m_paiUnitClassMaking[iI] = 0;
			m_paiUnitMarketPrice[iI] = 0;
			m_paiUnitKilledCounter[iI] = 0;
			m_paiUnitTurnRemaining[iI] = 0;
			m_paiUnitMoveChange[iI] = 0;
			m_paiUnitStrengthModifier[iI] = 0;
		}

		FAssertMsg(m_paiProfessionCombatChange==NULL, "about to leak memory, CvPlayer::m_paiProfessionMoveChange");
		m_paiProfessionCombatChange = new int [GC.getNumProfessionInfos()];
		FAssertMsg(m_paiProfessionMoveChange==NULL, "about to leak memory, CvPlayer::m_paiProfessionMoveChange");
		m_paiProfessionMoveChange = new int [GC.getNumProfessionInfos()];
		for (iI = 0; iI < GC.getNumProfessionInfos(); iI++)
		{
			m_paiProfessionCombatChange[iI] = 0;
			m_paiProfessionMoveChange[iI] = 0;
		}

		FAssertMsg(m_paiBuildingClassCount==NULL, "about to leak memory, CvPlayer::m_paiBuildingClassCount");
		m_paiBuildingClassCount = new int [GC.getNumBuildingClassInfos()];
		FAssertMsg(m_paiBuildingClassMaking==NULL, "about to leak memory, CvPlayer::m_paiBuildingClassMaking");
		m_paiBuildingClassMaking = new int [GC.getNumBuildingClassInfos()];
		for (iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
		{
			m_paiBuildingClassCount[iI] = 0;
			m_paiBuildingClassMaking[iI] = 0;
		}

		FAssertMsg(m_paiSpecialBuildingNotRequiredCount==NULL, "about to leak memory, CvPlayer::m_paiSpecialBuildingNotRequiredCount");
		m_paiSpecialBuildingNotRequiredCount = new int [GC.getNumSpecialBuildingInfos()];
		for (iI = 0; iI < GC.getNumSpecialBuildingInfos(); iI++)
		{
			m_paiSpecialBuildingNotRequiredCount[iI] = 0;
		}

		FAssertMsg(m_paeCivics==NULL, "about to leak memory, CvPlayer::m_paeCivics");
		m_paeCivics = new CivicTypes [GC.getNumCivicOptionInfos()];
		for (iI = 0; iI < GC.getNumCivicOptionInfos(); iI++)
		{
			m_paeCivics[iI] = NO_CIVIC;
		}

		FAssertMsg(m_aiProfessionEquipmentModifier==NULL, "about to leak memory, CvPlayer::m_aiProfessionEquipmentModifier");
		m_aiProfessionEquipmentModifier = new int[GC.getNumProfessionInfos()];
		for (iI = 0; iI < GC.getNumProfessionInfos(); iI++)
		{
			m_aiProfessionEquipmentModifier[iI] = 0;
		}

		FAssertMsg(m_aiTraitCount==NULL, "about to leak memory, CvPlayer::m_aiTraitCount");
		m_aiTraitCount = new int[GC.getNumTraitInfos()];
		for (iI = 0; iI < GC.getNumTraitInfos(); iI++)
		{
			m_aiTraitCount[iI] = 0;
		}

		FAssertMsg(m_ppiImprovementYieldChange==NULL, "about to leak memory, CvPlayer::m_ppiImprovementYieldChange");
		m_ppiImprovementYieldChange = new int*[GC.getNumImprovementInfos()];
		for (iI = 0; iI < GC.getNumImprovementInfos(); iI++)
		{
			m_ppiImprovementYieldChange[iI] = new int[NUM_YIELD_TYPES];
			for (iJ = 0; iJ < NUM_YIELD_TYPES; iJ++)
			{
				m_ppiImprovementYieldChange[iI][iJ] = 0;
			}
		}

		FAssertMsg(m_ppiBuildingYieldChange==NULL, "about to leak memory, CvPlayer::m_ppiBuildingYieldChange");
		m_ppiBuildingYieldChange = new int*[GC.getNumBuildingClassInfos()];
		for (iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
		{
			m_ppiBuildingYieldChange[iI] = new int[NUM_YIELD_TYPES];
			for (iJ = 0; iJ < NUM_YIELD_TYPES; iJ++)
			{
				m_ppiBuildingYieldChange[iI][iJ] = 0;
			}
		}

		m_mapEventsOccured.clear();
		m_mapEventCountdown.clear();
		m_aFreeUnitCombatPromotions.clear();
		m_aFreeUnitClassPromotions.clear();
		m_aEuropeRevolutionUnits.clear();
		m_triggersFired.clear();
	}

	m_cities.removeAll();

	m_tradeRoutes.reset();
	m_agreements.reset();
	m_hospitals.reset();
	m_tempUnits.reset();
	m_cityJobs.reset();
	m_marketTransactions.reset();
	m_convoys.reset();
	m_mappedMarketTransactions.clear();
	m_units.reset();
	freeEuropeUnits();

	m_selectionGroups.removeAll();

	m_eventsTriggered.removeAll();
	m_aszTradeMessages.clear();
	m_aszEuropeTradeMessages.clear();
	m_aszEventEuropeMessages.clear();
	m_mapTotalImmigration.clear();
	m_mapTotalImmigrationAsked.clear();
	m_mapTurnCountWithoutProposition.clear();
	m_mapMaxTurnWithoutProposition.clear();
	m_mapCurrentImmigrationProbability.clear();
	m_mapFirstUnitImmigrationProbability.clear();
	m_mapAverageImmigrationProbability.clear();

	if (!bConstructorCall)
	{
		AI_reset();
	}
}


//////////////////////////////////////
// graphical only setup
//////////////////////////////////////
void CvPlayer::setupGraphical()
{
	if (!GC.IsGraphicsInitialized())
		return;

	CvCity* pLoopCity;
	CvUnit* pLoopUnit;

	// Setup m_cities
	int iLoop;
	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		pLoopCity->setupGraphical();
	}

	// Setup m_units
	for (pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		if (pLoopUnit->plot() != NULL)
		{
			pLoopUnit->setupGraphical();
		}
	}
}


void CvPlayer::initFreeState()
{
	setGold(0);
	changeGold(GC.getHandicapInfo(getHandicapType()).getStartingGold());
	changeGold(GC.getEraInfo(GC.getGameINLINE().getStartEra()).getStartingGold());
}


void CvPlayer::initFreeUnits()
{
	UnitTypes eLoopUnit;
	int iFreeCount;

	if ((GC.getGameINLINE().isOption(GAMEOPTION_ADVANCED_START) || GC.getCivilizationInfo(getCivilizationType()).getAdvancedStartPoints() > 0) && !GC.getCivilizationInfo(getCivilizationType()).isEurope())
	{
		int iPoints = GC.getCivilizationInfo(getCivilizationType()).getAdvancedStartPoints();

		if (NO_WORLDSIZE != GC.getInitCore().getWorldSize())
		{
			iPoints *= GC.getWorldInfo(GC.getInitCore().getWorldSize()).getAdvancedStartPointsMod();
			iPoints /= 100;
		}

		if (NO_GAMESPEED != GC.getInitCore().getGameSpeed())
		{
			iPoints *= GC.getGameSpeedInfo(GC.getInitCore().getGameSpeed()).getGrowthPercent();
			iPoints /= 100;
		}

		if (GC.getGameINLINE().isOption(GAMEOPTION_ADVANCED_START))
		{
			iPoints += GC.getGameINLINE().getNumAdvancedStartPoints();
		}

		iPoints *= GC.getHandicapInfo(getHandicapType()).getAdvancedStartPointsMod();
		iPoints /= 100;

		if (!isHuman())
		{
			iPoints *= GC.getHandicapInfo(getHandicapType()).getAIAdvancedStartPercent();
			iPoints /= 100;
		}

		setAdvancedStartPoints(iPoints);

		// Starting visibility
		CvPlot* pStartingPlot = getStartingPlot();
		if (NULL != pStartingPlot)
		{
			for (int iPlotLoop = 0; iPlotLoop < GC.getMapINLINE().numPlots(); ++iPlotLoop)
			{
				CvPlot* pPlot = GC.getMapINLINE().plotByIndex(iPlotLoop);

				if (plotDistance(pPlot->getX_INLINE(), pPlot->getY_INLINE(), pStartingPlot->getX_INLINE(), pStartingPlot->getY_INLINE()) <= GC.getDefineINT("ADVANCED_START_SIGHT_RANGE"))
				{
					pPlot->setRevealed(getTeam(), true, false, NO_TEAM);
				}
			}
		}

		if (!GC.getGameINLINE().isOption(GAMEOPTION_ADVANCED_START) && !isHuman() && (getAdvancedStartPoints() >= 0))
		{
			AI_doAdvancedStart();
		}
	}
	else
	{
		CvCivilizationInfo& kCivilizationInfo = GC.getCivilizationInfo(getCivilizationType());
		bool bFoundPack = false;
		for (int iI = 0; iI < kCivilizationInfo.getNumCivilizationFreeUnits(); iI++)
		{
			int iLoopUnitClass = kCivilizationInfo.getCivilizationFreeUnitsClass(iI);
			ProfessionTypes eLoopUnitProfession = (ProfessionTypes) kCivilizationInfo.getCivilizationFreeUnitsProfession(iI);
			eLoopUnit = (UnitTypes)kCivilizationInfo.getCivilizationUnits(iLoopUnitClass);

			if (eLoopUnit != NO_UNIT)
			{
				//add revolution units to children
				if (GC.getCivilizationInfo(getCivilizationType()).isEurope())
				{
					for (int iPlayer = 0; iPlayer < MAX_PLAYERS; iPlayer++)
					{
						CvPlayer& kChild = GET_PLAYER((PlayerTypes) iPlayer);
						if (kChild.isAlive() && (kChild.getParent() == getID()))
						{
							kChild.addRevolutionEuropeUnit(eLoopUnit, eLoopUnitProfession);
						}
					}
				}
				else
				{ //Europeans units
					CvUnit* pUnit = addFreeUnit(eLoopUnit, eLoopUnitProfession);

					if (pUnit->getMaxMunition() != -1)
					{
						pUnit->setMunition(pUnit->getMaxMunition());
					}

					if (pUnit->getUnitInfo().isMechUnit() && !bFoundPack)
					{
						pUnit->setHaveFoundPack(true);
						bFoundPack = true;
					}
				}
			}
		}

		if (isEuropeanAI())
		{
			setStartingSpecialist(pickFreeUnit());
		}

		iFreeCount = GC.getEraInfo(GC.getGameINLINE().getStartEra()).getStartingDefenseUnits();
		iFreeCount += GC.getHandicapInfo(getHandicapType()).getStartingDefenseUnits();

		if (!isHuman())
		{
			iFreeCount += GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIStartingDefenseUnits();
		}

		if (iFreeCount > 0)
		{
			addFreeUnitAI(UNITAI_DEFENSIVE, iFreeCount);
		}

		iFreeCount = GC.getEraInfo(GC.getGameINLINE().getStartEra()).getStartingWorkerUnits();
		iFreeCount += GC.getHandicapInfo(getHandicapType()).getStartingWorkerUnits();

		if (!isHuman())
		{
			iFreeCount += GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIStartingWorkerUnits();
		}

		if (iFreeCount > 0)
		{
			addFreeUnitAI(UNITAI_SETTLER, iFreeCount);
		}

		iFreeCount = GC.getEraInfo(GC.getGameINLINE().getStartEra()).getStartingExploreUnits();
		iFreeCount += GC.getHandicapInfo(getHandicapType()).getStartingExploreUnits();

		if (!isHuman())
		{
			iFreeCount += GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIStartingExploreUnits();
		}

		if (iFreeCount > 0)
		{
			addFreeUnitAI(UNITAI_SCOUT, iFreeCount);
		}
	}
}

void CvPlayer::addFreeUnitAI(UnitAITypes eUnitAI, int iCount)
{
	UnitTypes eBestUnit = NO_UNIT;
	ProfessionTypes eBestProfession = NO_PROFESSION;
	int iBestValue = 0;

	for (int iI = 0; iI < GC.getNumUnitClassInfos(); iI++)
	{
		UnitTypes eLoopUnit = (UnitTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(iI);

		if (eLoopUnit != NO_UNIT)
		{
			if (canTrain(eLoopUnit))
			{
				int iValue = AI_unitValue(eLoopUnit, eUnitAI, NULL);

				if (iValue > iBestValue)
				{
					eBestUnit = eLoopUnit;
					iBestValue = iValue;
					eBestProfession = (ProfessionTypes) GC.getUnitInfo(eLoopUnit).getDefaultProfession();
				}
			}
		}
	}

	if (eBestUnit != NO_UNIT)
	{
		for (int iI = 0; iI < iCount; iI++)
		{
			addFreeUnit(eBestUnit, eBestProfession, eUnitAI);
		}
	}
}


CvUnit* CvPlayer::addFreeUnit(UnitTypes eUnit, ProfessionTypes eProfession, UnitAITypes eUnitAI)
{
	CvSeaway* pSeaway = getSeawayNearCity();
	if (pSeaway != NULL)
	{
		CvPlot* pPlot = pSeaway->plot();
		if (pPlot != NULL)
		{
			CvUnit* pUnit = initUnit(eUnit, eProfession, pPlot->getX_INLINE(), pPlot->getY_INLINE(), eUnitAI);
			return pUnit;
		}
	}
	CvPlot* pStartingPlot = getStartingPlot();
	if (pStartingPlot != NULL)
	{
		CvUnit* pUnit = initUnit(eUnit, eProfession, pStartingPlot->getX_INLINE(), pStartingPlot->getY_INLINE(), eUnitAI);
		return pUnit;
	}

	return NULL;
}


int CvPlayer::startingPlotRange() const
{
	return 25;
	/*
	int iRange;

	//iRange = (GC.getMapINLINE().maxStepDistance() + 40);

	iRange = GC.getMapINLINE().getGridHeight() / GC.getGameINLINE().countCivPlayersAlive() + 1;

	iRange *= GC.getDefineINT("STARTING_DISTANCE_PERCENT");
	iRange /= 100;

	iRange *= (GC.getMapINLINE().getLandPlots() / (GC.getWorldInfo(GC.getMapINLINE().getWorldSize()).getTargetNumCities() * GC.getGameINLINE().countCivPlayersAlive()));
	iRange /= NUM_CITY_PLOTS;

	iRange += std::min(((GC.getMapINLINE().getNumAreas() + 1) / 2), GC.getGameINLINE().countCivPlayersAlive());

	long lResult=0;
	if (gDLL->getPythonIFace()->callFunction(gDLL->getPythonIFace()->getMapScriptModule(), "minStartingDistanceModifier", NULL, &lResult))
	{
	iRange *= std::max<int>(0, (lResult + 100));
	iRange /= 100;
	}
	return std::max(iRange, GC.getDefineINT("MIN_CIV_STARTING_DISTANCE"));
	*/
}



int CvPlayer::startingPlotDistanceFactor(CvPlot* pPlot, PlayerTypes ePlayer, int iRange) const
{
	PROFILE_FUNC();

	FAssert(ePlayer != getID());

	CvPlot* pStartingPlot;

	int iValue = 1000;

	pStartingPlot = getStartingPlot();

	if (pStartingPlot != NULL)
	{
		if (GC.getGameINLINE().isTeamGame())
		{
			if (GET_PLAYER(ePlayer).getTeam() == getTeam())
			{
				iRange *= GC.getDefineINT("OWN_TEAM_STARTING_MODIFIER");
				iRange /= 100;
			}
			else
			{
				iRange *= GC.getDefineINT("RIVAL_TEAM_STARTING_MODIFIER");
				iRange /= 100;
			}
		}

		int iDistance = stepDistance(pPlot->getX_INLINE(), pPlot->getY_INLINE(), pStartingPlot->getX_INLINE(), pStartingPlot->getY_INLINE());
		if (pStartingPlot->getArea() != pPlot->getArea())
		{
			iDistance *= 4;
			iDistance /= 3;
		}

		iValue *= iDistance;
		iValue /= iRange ;

	}

	return std::max(1, iValue);

}


// Returns the id of the best area, or -1 if it doesn't matter:
int CvPlayer::findStartingArea() const
{
	PROFILE_FUNC();

	long result = -1;
	if (gDLL->getPythonIFace()->pythonFindStartingArea(getID(), &result) && !gDLL->getPythonIFace()->pythonUsingDefaultImpl()) // Python override
	{
		if (result == -1 || GC.getMapINLINE().getArea(result) != NULL)
		{
			return result;
		}
		else
		{
			FAssertMsg(false, "python findStartingArea() must return -1 or the ID of a valid area");
		}
	}

	//not useful for water starts
	if (GC.getCivilizationInfo(getCivilizationType()).isWaterStart())
	{
		return -1;
	}

	int iBestValue = 0;
	int iBestArea = -1;
	int iValue;
	int iLoop = 0;

	CvArea *pLoopArea = NULL;

	// find best land area
	for (pLoopArea = GC.getMapINLINE().firstArea(&iLoop); pLoopArea != NULL; pLoopArea = GC.getMapINLINE().nextArea(&iLoop))
	{
		if (!(pLoopArea->isWater()))
		{
			// iNumPlayersOnArea is the number of players starting on the area, plus this player
			int iNumPlayersOnArea = (pLoopArea->getNumStartingPlots() + 1);
			int iTileValue = ((pLoopArea->calculateTotalBestNatureYield() + (pLoopArea->countCoastalLand() * 2) + pLoopArea->getNumRiverEdges() + (pLoopArea->getNumTiles())) + 1);
			iValue = iTileValue / iNumPlayersOnArea;

			iValue *= std::min(NUM_CITY_PLOTS + 1, pLoopArea->getNumTiles() + 1);
			iValue /= (NUM_CITY_PLOTS + 1);

			if (iNumPlayersOnArea <= 2)
			{
				iValue *= 4;
				iValue /= 3;
			}

			if (iValue > iBestValue)
			{
				iBestValue = iValue;
				iBestArea = pLoopArea->getID();
			}
		}
	}

	return iBestArea;
}


CvPlot* CvPlayer::findStartingPlot(bool bRandomize)
{
	PROFILE_FUNC();

	long result = -1;
	if (gDLL->getPythonIFace()->pythonFindStartingPlot(getID(), &result) && !gDLL->getPythonIFace()->pythonUsingDefaultImpl()) // Python override
	{
		CvPlot *pPlot = GC.getMapINLINE().plotByIndexINLINE(result);
		if (pPlot != NULL)
		{
			return pPlot;
		}
		else
		{
			FAssertMsg(false, "python findStartingPlot() returned an invalid plot index!");
		}
	}

	CvPlot* pLoopPlot;
	int iBestArea = -1;
	int iI;

	bool bNew = false;
	if (getStartingPlot() != NULL)
	{
		iBestArea = getStartingPlot()->getArea();
		setStartingPlot(NULL, true);
		bNew = true;
	}

	AI_updateFoundValues(true);//this sets all plots found values to -1

	if (!bNew)
	{
		iBestArea = findStartingArea();
	}

	//flood fill land distances O(numPlots)
	std::vector<short> aLandDistances(GC.getMapINLINE().numPlotsINLINE(), MAX_SHORT);

	{
		PROFILE("CvPlayer::findStartingPlot::landDistances");

		//initialize
		std::deque<int> aFillQueue;
		for (int i=0;i<GC.getMapINLINE().numPlotsINLINE();i++)
		{
			CvPlot* pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(i);
			if (!pLoopPlot->isWater())
			{
				aLandDistances[i] = 0;
				aFillQueue.push_back(i);
			}
		}

		//fill
		while(!aFillQueue.empty())
		{
			int iLoopIndex = aFillQueue.front();
			aFillQueue.pop_front();
			CvPlot* pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iLoopIndex);
			int iLoopDistance = aLandDistances[iLoopIndex] + 1;
			for (int iDirection=0;iDirection<NUM_DIRECTION_TYPES;iDirection++)
			{
				CvPlot* pAdjacentPlot = plotDirection(pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE(), (DirectionTypes) iDirection);
				if (pAdjacentPlot != NULL)
				{
					int iAdjacentIndex = GC.getMapINLINE().plotNumINLINE(pAdjacentPlot->getX_INLINE(), pAdjacentPlot->getY_INLINE());
					if (iLoopDistance < aLandDistances[iAdjacentIndex])
					{
						aLandDistances[iAdjacentIndex] = iLoopDistance;
						aFillQueue.push_back(iAdjacentIndex);
					}
				}
			}
		}
	}

	bool bStartOnWater = GC.getCivilizationInfo(getCivilizationType()).isWaterStart();
	std::deque<bool> abEuropeStart;
	if (bStartOnWater)
	{
		for (int i = 0; i < GC.getNumEuropeInfos(); ++i)
		{
			abEuropeStart.push_back(GC.getEuropeInfo((EuropeTypes) i).isStart());
			gDLL->getPythonIFace()->pythonIsEuropeStart((EuropeTypes) i, &abEuropeStart[i]);
		}
	}

	for (int iPass = 0; iPass < 2; iPass++)
	{
		int iBestValue = 0;
		int iBestIndex = -1;

		std::vector<int> aiWeights(GC.getMapINLINE().numPlotsINLINE(), 0);
		for (iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); iI++)
		{
			int iValue = 0;
			pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iI);

			if ((iBestArea == -1) || (pLoopPlot->getArea() == iBestArea) || (iPass > 0))
			{
				if (bStartOnWater)
				{
					EuropeTypes eEurope = pLoopPlot->getEurope();
					if (eEurope != NO_EUROPE && abEuropeStart[eEurope])
					{
						iValue = 1;
						switch ((CardinalDirectionTypes)GC.getEuropeInfo(eEurope).getCardinalDirection())
						{
						case CARDINALDIRECTION_EAST:
						case CARDINALDIRECTION_WEST:
							iValue *= 1 + std::min(::plotDistance(pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE(), pLoopPlot->getX_INLINE(), 0), startingPlotRange());
							iValue *= 1 + std::min(::plotDistance(pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE(), pLoopPlot->getX_INLINE(), GC.getMapINLINE().getGridHeightINLINE() - 1), startingPlotRange() / 2);
							break;
						case CARDINALDIRECTION_NORTH:
						case CARDINALDIRECTION_SOUTH:
							iValue *= 1 + std::min(::plotDistance(pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE(), 0, pLoopPlot->getY_INLINE()), startingPlotRange());
							iValue *= 1 + std::min(::plotDistance(pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE(), GC.getMapINLINE().getGridWidthINLINE() - 1, pLoopPlot->getY_INLINE()), startingPlotRange() / 2);
							break;
						default:
							break;
						}
						int iMinPlotDistance = startingPlotRange();
						for (int iPlayer = 0; iPlayer < MAX_PLAYERS; iPlayer++)
						{
							CvPlayer& otherPlayer = GET_PLAYER((PlayerTypes) iPlayer);
							if ((iPlayer != getID()) && otherPlayer.isAlive())
							{
								CvPlot* pOtherPlot = otherPlayer.getStartingPlot();
								if (pOtherPlot != NULL)
								{
									if (GC.getCivilizationInfo(otherPlayer.getCivilizationType()).isWaterStart())
									{
										int iPlotDistance = plotDistance(pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE(), pOtherPlot->getX_INLINE(), pOtherPlot->getY_INLINE());
										if (iPlotDistance < iMinPlotDistance)
										{
											iMinPlotDistance = iPlotDistance;
										}
									}
								}
							}
						}
						iValue *= 1 + iMinPlotDistance;

						iValue /= (aLandDistances[iI] + 1);
					}
				}
				else
				{
					//the distance factor is now done inside foundValue
					iValue = pLoopPlot->getFoundValue(getID());
				}

				if (iValue > iBestValue)
				{
					iBestValue = iValue;
					iBestIndex = iI;
				}
			}

			FAssert(iValue >= 0);
			aiWeights[iI] = std::max(0, iValue);
		}

		FAssert(std::accumulate(aiWeights.begin(), aiWeights.end(), 0) > 0);

		if (bRandomize)
		{
			iBestIndex = GC.getGameINLINE().getSorenRand().pickValue(aiWeights, "Randomizing start");
		}

		return GC.getMapINLINE().plotByIndexINLINE(iBestIndex);

		FAssertMsg(iPass != 0, "CvPlayer::findStartingPlot - could not find starting plot in first pass.");
	}

	FAssertMsg(false, "Could not find starting plot.");
	return NULL;
}


CvCity* CvPlayer::initCity(int iX, int iY, bool bBumpUnits)
{
	PROFILE_FUNC();

	CvCity* pCity = addCity();

	FAssertMsg(pCity != NULL, "City is not assigned a valid value");
	FAssertMsg(!(GC.getMapINLINE().plotINLINE(iX, iY)->isCity()), "No city is expected at this plot when initializing new city");

	pCity->init(pCity->getID(), getID(), iX, iY, bBumpUnits);

	return pCity;
}

void CvPlayer::acquireCity(CvCity* pOldCity, bool bConquest, bool bTrade)
{
	CLLNode<IDInfo>* pUnitNode;
	CvCity* pNewCity;
	CvUnit* pLoopUnit;
	CvPlot* pCityPlot;
	CvPlot* pLoopPlot;
	bool* pabHasRealBuilding;
	int* paiBuildingOriginalOwner;
	int* paiBuildingOriginalTime;
	CvWString szBuffer;
	CvWString szName;
	bool abEverOwned[MAX_PLAYERS];
	int aiCulture[MAX_PLAYERS];
	PlayerTypes eOldOwner;
	PlayerTypes eOriginalOwner;
	PlayerTypes eHighestCulturePlayer;
	BuildingTypes eBuilding;
	bool bForceUnowned;
	bool bRecapture;
	bool bRaze;
	bool bGift;
	int iRange;
	int iGameTurnFounded;
	int iPopulation;
	int iHighestPopulation;
	int iOccupationTimer;
	int iTeamCulturePercent;
	int iDamage;
	int iDX, iDY;
	int iI;
	CLinkList<IDInfo> oldUnits;

	pCityPlot = pOldCity->plot();

	pUnitNode = pCityPlot->headUnitNode();

	while (pUnitNode != NULL)
	{
		oldUnits.insertAtEnd(pUnitNode->m_data);
		pUnitNode = pCityPlot->nextUnitNode(pUnitNode);
	}

	pUnitNode = oldUnits.head();

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = oldUnits.next(pUnitNode);

		if (pLoopUnit && pLoopUnit->getTeam() != getTeam())
		{
			if (pLoopUnit->getDomainType() == DOMAIN_IMMOBILE)
			{
				pLoopUnit->kill(false);
			}
		}
	}

	if (bConquest)
	{
		iRange = pOldCity->getCultureLevel();

		for (iDX = -(iRange); iDX <= iRange; iDX++)
		{
			for (iDY = -(iRange); iDY <= iRange; iDY++)
			{
				if (pOldCity->cultureDistance(iDX, iDY) <= iRange)
				{
					pLoopPlot = plotXY(pOldCity->getX_INLINE(),pOldCity-> getY_INLINE(), iDX, iDY);

					if (pLoopPlot != NULL)
					{
						if (pLoopPlot->getOwnerINLINE() == pOldCity->getOwnerINLINE())
						{
							if (pLoopPlot->getNumCultureRangeCities(pOldCity->getOwnerINLINE()) == 1)
							{
								bForceUnowned = false;

								for (iI = 0; iI < MAX_PLAYERS; iI++)
								{
									if (GET_PLAYER((PlayerTypes)iI).isAlive())
									{
										if ((GET_PLAYER((PlayerTypes)iI).getTeam() != getTeam()) && (GET_PLAYER((PlayerTypes)iI).getTeam() != pOldCity->getTeam()))
										{
											if (pLoopPlot->getNumCultureRangeCities((PlayerTypes)iI) > 0)
											{
												bForceUnowned = true;
												break;
											}
										}
									}
								}

								if (bForceUnowned)
								{
									pLoopPlot->setForceUnownedTimer(2); // XXX
								}
							}
						}
					}
				}
			}
		}
	}

	bool isOriginalOwner = pOldCity->getOriginalOwner() == getID();

	if (pOldCity->getOriginalOwner() == pOldCity->getOwnerINLINE())
	{
		GET_PLAYER(pOldCity->getOriginalOwner()).changeCitiesLost(1);
	}
	else if (isOriginalOwner)
	{
		GET_PLAYER(pOldCity->getOriginalOwner()).changeCitiesLost(-1);
	}

	if (bConquest)
	{
		szBuffer = gDLL->getText("TXT_KEY_MISC_CAPTURED_CITY", pOldCity->getNameKey()).GetCString();
		gDLL->getInterfaceIFace()->addMessage(getID(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_CITYCAPTURE", MESSAGE_TYPE_MAJOR_EVENT, ARTFILEMGR.getInterfaceArtInfo("WORLDBUILDER_CITY_EDIT")->getPath(), (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pOldCity->getX_INLINE(), pOldCity->getY_INLINE(), true, true);

		szName.Format(L"%s (%s)", pOldCity->getName().GetCString(), GET_PLAYER(pOldCity->getOwnerINLINE()).getName());

		for (iI = 0; iI < MAX_PLAYERS; iI++)
		{
			if (GET_PLAYER((PlayerTypes)iI).isAlive())
			{
				if (iI != getID())
				{
					if (pOldCity->isRevealed(GET_PLAYER((PlayerTypes)iI).getTeam(), false))
					{
						szBuffer = gDLL->getText("TXT_KEY_MISC_CITY_CAPTURED_BY", szName.GetCString(), getCivilizationDescriptionKey());
						gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_CITYCAPTURED", MESSAGE_TYPE_MAJOR_EVENT, ARTFILEMGR.getInterfaceArtInfo("WORLDBUILDER_CITY_EDIT")->getPath(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pOldCity->getX_INLINE(), pOldCity->getY_INLINE(), true, true);
					}
				}
			}
		}

		szBuffer = gDLL->getText("TXT_KEY_MISC_CITY_WAS_CAPTURED_BY", szName.GetCString(), getCivilizationDescriptionKey());
		GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getID(), szBuffer, pOldCity->getX_INLINE(), pOldCity->getY_INLINE(), (ColorTypes)GC.getInfoTypeForString("COLOR_WARNING_TEXT"));
	}

	pabHasRealBuilding = new bool[GC.getNumBuildingInfos()];
	paiBuildingOriginalOwner = new int[GC.getNumBuildingInfos()];
	paiBuildingOriginalTime = new int[GC.getNumBuildingInfos()];

	eOldOwner = pOldCity->getOwnerINLINE();
	eOriginalOwner = pOldCity->getOriginalOwner();
	PlayerTypes eOldPreviousOwner = pOldCity->getPreviousOwner();
	eHighestCulturePlayer = pOldCity->findHighestCulture();
	iGameTurnFounded = pOldCity->getGameTurnFounded();
	iPopulation = pOldCity->getPopulation();
	iHighestPopulation = pOldCity->getHighestPopulation();
	iOccupationTimer = pOldCity->getOccupationTimer();
	szName = pOldCity->getNameKey();
	iDamage = pOldCity->getDefenseDamage();
	int iOldCityId = pOldCity->getID();
	UnitClassTypes eTeachUnitClass = pOldCity->getTeachUnitClass();
	int iEducationThresholdMultiplier = pOldCity->getEducationThresholdMultiplier();

	for (iI = 0; iI < MAX_PLAYERS; iI++)
	{
		abEverOwned[iI] = pOldCity->isEverOwned((PlayerTypes)iI);
		aiCulture[iI] = pOldCity->getCulture((PlayerTypes)iI);
	}

	abEverOwned[getID()] = true;

	for (iI = 0; iI < GC.getNumBuildingInfos(); iI++)
	{
		pabHasRealBuilding[iI] = pOldCity->isHasRealBuilding((BuildingTypes)iI);
		paiBuildingOriginalOwner[iI] = pOldCity->getBuildingOriginalOwner((BuildingTypes)iI);
		paiBuildingOriginalTime[iI] = pOldCity->getBuildingOriginalTime((BuildingTypes)iI);
	}

	std::vector<BuildingYieldChange> aBuildingYieldChange;
	for (iI = 0; iI < GC.getNumBuildingClassInfos(); ++iI)
	{
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
		{
			BuildingYieldChange kChange;
			kChange.eBuildingClass = (BuildingClassTypes)iI;
			kChange.eYield = (YieldTypes)iYield;
			kChange.iChange = pOldCity->getBuildingYieldChange((BuildingClassTypes)iI, (YieldTypes)iYield);
			if (0 != kChange.iChange)
			{
				aBuildingYieldChange.push_back(kChange);
			}
		}
	}

	int aiYieldsStored[NUM_YIELD_TYPES];
	for (iI = 0; iI < NUM_YIELD_TYPES; ++iI)
	{
		aiYieldsStored[iI] = pOldCity->getYieldStored((YieldTypes) iI);
	}


	//capture all population units
	std::vector<CvUnit *> aOldPopulationUnits;
	for (int i = 0; i < iPopulation; ++i)
	{
		CvUnit* pUnit = pOldCity->getPopulationUnitByIndex(0);
		bool bRemoved = pOldCity->removePopulationUnit(pUnit, false, NO_PROFESSION);
		FAssert(bRemoved);
		aOldPopulationUnits.push_back(pUnit);
		GET_PLAYER(pOldCity->getOwnerINLINE()).getAndRemoveUnit(pUnit->getID());
	}
	FAssert(pOldCity->getPopulation() == 0);

	bRecapture = ((eHighestCulturePlayer != NO_PLAYER) ? (GET_PLAYER(eHighestCulturePlayer).getTeam() == getTeam()) : false);

	bool bNative(false);
	//Check if the old city owner is native:
	bNative = pOldCity->isNative();

	pOldCity->kill();

	//acquire old population units
	//DOANE Natives escape from razed city
	/*	Note:
	Natives have now a random percentage of receiving free units which escaped from razed city.
	The Probability of receiving a free unit is related with a global define set in XML.
	Here the escaped units are deleted from the population captured list, where a counter stores
	the num of free units. Note this has only to be done when conquering and not recapturing the city.
	This counter is then used to init the new units in the nearest native city.
	*/

	int iEscapeProbability = (int)GC.getDefineUCHAR("NATIVES_ESCAPE_FROM_CONQUEST_CITY_PROBABILITY");
	unsigned char ucCount(0); //Count units escaped for future use
	std::vector<CvUnit*> aNewPopulationUnits;

	if (bNative && bConquest && !bRecapture && iEscapeProbability) //Can native units escape from city?
	{
		bool bProb(false);
		for (int i=0;i<(int)aOldPopulationUnits.size();i++)
		{
			CvUnit* pOldUnit = aOldPopulationUnits[i];
			UnitTypes eNewUnitType = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(pOldUnit->getUnitClassType());

			if (eNewUnitType != NO_UNIT)
			{
				//Do probability check
				bProb = (GC.getGameINLINE().getSorenRandNum(100, "Natives escape from razed city") < iEscapeProbability);
				if (bProb) //Unit flees!
				{
					pOldUnit->updateOwnerCache(-1);
					SAFE_DELETE(pOldUnit);
					ucCount++; //Increase counter
				}
				else //acquire old population units
				{
					CvUnit* pNewUnit = initUnit(eNewUnitType, NO_PROFESSION, pCityPlot->getX_INLINE(), pCityPlot->getY_INLINE(), pOldUnit->AI_getUnitAIType());
					pNewUnit->convert(pOldUnit, true); //kills old unit
					pNewUnit->setRebelSentiment(0);
					aNewPopulationUnits.push_back(pNewUnit);
				}
			}
			else //NO_UNIT
			{
				pOldUnit->updateOwnerCache(-1);
				SAFE_DELETE(pOldUnit);
			}
		}
	}
	else //Works by default for non native players (as vanilla)
	{
		//acquire old population units
		for (int i=0;i<(int)aOldPopulationUnits.size();i++)
		{
			CvUnit* pOldUnit = aOldPopulationUnits[i];
			UnitTypes eNewUnitType = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(pOldUnit->getUnitClassType());
			if (eNewUnitType != NO_UNIT)
			{
				CvUnit* pNewUnit = initUnit(eNewUnitType, NO_PROFESSION, pCityPlot->getX_INLINE(), pCityPlot->getY_INLINE(), pOldUnit->AI_getUnitAIType());
				pNewUnit->convert(pOldUnit, true); //kills old unit
				pNewUnit->setRebelSentiment(0);
				aNewPopulationUnits.push_back(pNewUnit);
			}
			else
			{
				pOldUnit->updateOwnerCache(-1);
				SAFE_DELETE(pOldUnit);
			}
		}
	}
	//END DOANE

	for (iDX = -1; iDX <= 1; iDX++)
	{
		for (iDY = -1; iDY <= 1; iDY++)
		{
			pLoopPlot	= plotXY(pCityPlot->getX_INLINE(), pCityPlot->getY_INLINE(), iDX, iDY);

			if (pLoopPlot != NULL)
			{
				pLoopPlot->setCulture(eOldOwner, 0, false);
			}
		}
	}

	pNewCity = initCity(pCityPlot->getX_INLINE(), pCityPlot->getY_INLINE(), !bConquest);

	FAssertMsg(pNewCity != NULL, "NewCity is not assigned a valid value");

	pNewCity->setPreviousOwner(eOldOwner);
	pNewCity->setOriginalOwner(eOriginalOwner);
	pNewCity->setGameTurnFounded(iGameTurnFounded);
	pNewCity->setTeachUnitClass(eTeachUnitClass);
	pNewCity->setEducationThresholdMultiplier(iEducationThresholdMultiplier);

	for (uint i = 0; i < aNewPopulationUnits.size(); i++)
	{
		pNewCity->addPopulationUnit(aNewPopulationUnits[i], NO_PROFESSION);
	}

	pNewCity->setHighestPopulation(iHighestPopulation);
	pNewCity->setName(szName);
	pNewCity->setNeverLost(false);
	pNewCity->changeDefenseDamage(iDamage);

	for (iI = 0; iI < MAX_PLAYERS; iI++)
	{
		pNewCity->setEverOwned(((PlayerTypes)iI), abEverOwned[iI]);
		pNewCity->setCulture(((PlayerTypes)iI), aiCulture[iI], false);
	}

	for (iI = 0; iI < GC.getNumBuildingInfos(); iI++)
	{
		bool bSetHasBuilding = false;

		if (pabHasRealBuilding[iI])
		{
			BuildingClassTypes eBuildingClass = (BuildingClassTypes)GC.getBuildingInfo((BuildingTypes)iI).getBuildingClassType();
			eBuilding = (BuildingTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationBuildings(eBuildingClass);

			if (eBuilding != NO_BUILDING)
			{
				if (bTrade || !(GC.getBuildingInfo((BuildingTypes)iI).isNeverCapture()))
				{
					if (pNewCity->isValidBuildingLocation(eBuilding))
					{
						if (!bConquest || bRecapture || pNewCity->isHasRealBuilding(eBuilding) || GC.getGameINLINE().getSorenRandNum(100, "Capture Probability") < GC.getBuildingInfo((BuildingTypes)iI).getConquestProbability())
						{
							bSetHasBuilding = true;
						}
					}
				}

				pNewCity->setHasRealBuildingTimed(eBuilding, bSetHasBuilding, false, ((PlayerTypes)(paiBuildingOriginalOwner[iI])), paiBuildingOriginalTime[iI]);
			}
		}
	}

	for (iI = 0; iI < NUM_YIELD_TYPES; ++iI)
	{
		pNewCity->setYieldStored((YieldTypes) iI, aiYieldsStored[iI]);
	}

	for (std::vector<BuildingYieldChange>::iterator it = aBuildingYieldChange.begin(); it != aBuildingYieldChange.end(); ++it)
	{
		pNewCity->setBuildingYieldChange((*it).eBuildingClass, (*it).eYield, (*it).iChange);
	}

	if (bTrade)
	{
		if (!bRecapture)
		{
			pNewCity->changeOccupationTimer(iOccupationTimer);
		}
	}

	if (bConquest)
	{
		if (!isOriginalOwner || iOccupationTimer == 0) 
		{
			iTeamCulturePercent = pNewCity->calculateTeamCulturePercent(getTeam());

			if (iTeamCulturePercent < GC.getDefineINT("OCCUPATION_CULTURE_PERCENT_THRESHOLD"))
			{
				pNewCity->changeOccupationTimer(((GC.getDefineINT("BASE_OCCUPATION_TURNS") + ((pNewCity->getPopulation() * GC.getDefineINT("OCCUPATION_TURNS_POPULATION_PERCENT")) / 100)) * (100 - iTeamCulturePercent)) / 100);
			}
		}

		GC.getMapINLINE().verifyUnitValidPlot();
	}

	pCityPlot->setRevealed(GET_PLAYER(eOldOwner).getTeam(), true, false, NO_TEAM);

	gDLL->getEventReporterIFace()->cityAcquired(eOldOwner, getID(), pNewCity, bConquest, bTrade);

	SAFE_DELETE_ARRAY(pabHasRealBuilding);
	SAFE_DELETE_ARRAY(paiBuildingOriginalOwner);
	SAFE_DELETE_ARRAY(paiBuildingOriginalTime);

	if (bConquest)
	{
		//DOANE Python Callbacks | Speed Improvement
		long lResult=1; //We want this code to be running as default
		if (GC.getUSE_CAN_RAZE_CITY_CALLBACK())
		{
			CyCity* pyCity = new CyCity(pNewCity);
			CyArgsList argsList;
			argsList.add(getID());	// Player ID
			argsList.add(gDLL->getPythonIFace()->makePythonObject(pyCity));	// pass in city class
			lResult=0; //If using python then it starts as false and "canRazeCity" checks if it should be true, like in vanilla.
			gDLL->getPythonIFace()->callFunction(PYGameModule, "canRazeCity", argsList.makeFunctionArgs(), &lResult); //Returns true as default
			delete pyCity;	// python fxn must not hold on to this pointer
		}
		//END DOANE
		if (lResult == 1)
		{
			//auto raze based on game rules
			if (pNewCity->isAutoRaze())
			{
				pNewCity->doTask(TASK_RAZE);
			}
			else if (!isHuman())
			{
				AI_conquerCity(pNewCity); // could delete the pointer...
			}
			else
			{
				//popup raze option
				eHighestCulturePlayer = pNewCity->getLiberationPlayer(true);
				bRaze = canRaze(pNewCity, false);
				bGift = ((eHighestCulturePlayer != NO_PLAYER)
					&& (eHighestCulturePlayer != getID())
					&& ((getTeam() == GET_PLAYER(eHighestCulturePlayer).getTeam())
					|| GET_TEAM(getTeam()).isOpenBorders(GET_PLAYER(eHighestCulturePlayer).getTeam())));

				if (bRaze || bGift)
				{
					if (GC.getGameINLINE().getActivePlayer() == getID())
					{
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_RAZECITY);
						pInfo->setData1(pNewCity->getID());
						pInfo->setData2(eHighestCulturePlayer);
						gDLL->getInterfaceIFace()->addPopup(pInfo, getID());
					}
				}
				else
				{
					pNewCity->chooseProduction();
					gDLL->getEventReporterIFace()->cityAcquiredAndKept(GC.getGameINLINE().getActivePlayer(), pNewCity);
				}
			}
		}
	}
	else
	{
		if (pNewCity->getPopulation() == 0)
		{
			pNewCity->doTask(TASK_RAZE);
		}
		else if (!bTrade)
		{
			FAssertMsg(false, "No more culture flips");
			gDLL->getEventReporterIFace()->cityAcquiredAndKept(GC.getGameINLINE().getActivePlayer(), pNewCity);
		}
	}

	// Forcing events that deal with the old city not to expire just because we conquered that city
	for (CvEventMap::iterator it = m_mapEventsOccured.begin(); it != m_mapEventsOccured.end(); ++it)
	{
		EventTriggeredData &triggerData = (*it).second;
		if ((triggerData.m_eOtherPlayer == eOldOwner) && (triggerData.m_iOtherPlayerCityId == iOldCityId))
		{
			triggerData.m_iOtherPlayerCityId = -1;
		}
	}
}

void CvPlayer::killCities()
{
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		pLoopCity->kill();
	}
}


const CvWString CvPlayer::getNewCityName() const
{
	for (int i = 0; i < getNumCityNames(); ++i)
	{
		if (isCityNameValid(getCityName(i), true))
		{
			return getCityName(i);
		}
	}

	CvWString szName;
	getCivilizationCityName(szName, getCivilizationType());
	if (!szName.empty())
	{
		return szName;
	}

	// Pick a name from another random civ
	std::vector<CivilizationTypes> aePriorityCivs;
	std::vector<CivilizationTypes> aeOtherCivs;
	int iRandOffset = GC.getGameINLINE().getSorenRandNum(GC.getNumCivilizationInfos(), "Place Units (Player)");
	for (int iI = 0; iI < GC.getNumCivilizationInfos(); iI++)
	{
		CivilizationTypes eLoopCiv = (CivilizationTypes) ((iI + iRandOffset) % GC.getNumCivilizationInfos());
		if (eLoopCiv != getCivilizationType())
		{
			if (GC.getCivilizationInfo(eLoopCiv).isNative() == isNative())
			{
				aePriorityCivs.push_back(eLoopCiv);
			}
			else
			{
				aeOtherCivs.push_back(eLoopCiv);
			}
		}
	}

	for (uint i = 0; i < aePriorityCivs.size(); ++i)
	{
		getCivilizationCityName(szName, aePriorityCivs[i]);
		if (!szName.empty())
		{
			return szName;
		}
	}

	for (uint i = 0; i < aeOtherCivs.size(); ++i)
	{
		getCivilizationCityName(szName, aeOtherCivs[i]);
		if (!szName.empty())
		{
			return szName;
		}
	}

	return L"TXT_KEY_CITY";
}

const CvWString& CvPlayer::addNewCityName()
{
	addCityName(getNewCityName());
	return getCityName(getNumCityNames() - 1);
}


void CvPlayer::getCivilizationCityName(CvWString& szBuffer, CivilizationTypes eCivilization) const
{
	for (int iI = 0; iI < GC.getCivilizationInfo(eCivilization).getNumCityNames(); iI++)
	{
		szBuffer = CvWString(GC.getCivilizationInfo(eCivilization).getCityNames(iI));
		if (isCityNameValid(szBuffer, true))
		{
			return;
		}
	}

	szBuffer.clear();
}


bool CvPlayer::isCityNameValid(const CvWString& szName, bool bTestDestroyed) const
{
	CvCity* pLoopCity;
	int iLoop;

	if (bTestDestroyed)
	{
		if (GC.getGameINLINE().isDestroyedCityName(szName))
		{
			return false;
		}

		for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes)iPlayer);
			for (pLoopCity = kLoopPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kLoopPlayer.nextCity(&iLoop))
			{
				if (pLoopCity->getName() == gDLL->getText(szName))
				{
					return false;
				}
			}
		}
	}
	else
	{
		for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
		{
			if (pLoopCity->getName() == gDLL->getText(szName))
			{
				return false;
			}
		}
	}

	return true;
}

CvUnit* CvPlayer::initUnit(UnitTypes eUnit, ProfessionTypes eProfession, int iX, int iY, UnitAITypes eUnitAI, DirectionTypes eFacingDirection, int iYieldStored)
{
	PROFILE_FUNC();

	FAssertMsg(eUnit != NO_UNIT, "Unit is not assigned a valid value");

	CvUnit* pUnit = addUnit();
	FAssertMsg(pUnit != NULL, "Unit is not assigned a valid value");
	if (NULL != pUnit)
	{
		if (eUnitAI == NO_UNITAI && eProfession != NO_PROFESSION)
		{
			eUnitAI = (UnitAITypes) GC.getProfessionInfo(eProfession).getDefaultUnitAIType();
		}

		if (eUnitAI == NO_UNITAI)
		{
			eUnitAI = (UnitAITypes) GC.getUnitInfo(eUnit).getDefaultUnitAIType();
		}

		pUnit->init(pUnit->getID(), eUnit, eProfession, eUnitAI, getID(), iX, iY, eFacingDirection, iYieldStored);

		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			if (gDLL->getInterfaceIFace()->getHeadSelectedUnit() == NULL)
			{
				gDLL->getInterfaceIFace()->changeCycleSelectionCounter(1);
			}
		}
	}

	return pUnit;
}

CvUnit* CvPlayer::initEuropeUnit(UnitTypes eUnit, UnitAITypes eUnitAI, DirectionTypes eFacingDirection, bool bLikelyToLeave)
{
	ProfessionTypes eProfession = (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession();
	if (eUnit == UNIT_ARTILLERYMAN_EXPERT)
	{
		eProfession = (ProfessionTypes) GC.getCivilizationInfo(getCivilizationType()).getDefaultProfession();
	}
	CvUnit* pUnit = initUnit(eUnit, eProfession, INVALID_PLOT_COORD, INVALID_PLOT_COORD, eUnitAI, eFacingDirection);
	
	if (pUnit != NULL)
	{
		unloadUnitToEurope(pUnit);
		if (!bLikelyToLeave)
		{
			pUnit->setLikelyToLeave(bLikelyToLeave);
		}
	}
	return pUnit;
}

void CvPlayer::killUnits()
{
	while (!m_units.empty())
	{
		m_units.begin()->second->kill(false);
	}
}


// XXX should pUnit be a CvSelectionGroup???
// Returns the next unit in the cycle...
CvSelectionGroup* CvPlayer::cycleSelectionGroups(CvUnit* pUnit, bool bForward, bool* pbWrap)
{
	CLLNode<int>* pSelectionGroupNode;
	CLLNode<int>* pFirstSelectionGroupNode;
	CvSelectionGroup* pLoopSelectionGroup;

	if (pbWrap != NULL)
	{
		*pbWrap = false;
	}

	pSelectionGroupNode = headGroupCycleNode();

	if (pUnit != NULL)
	{
		while (pSelectionGroupNode != NULL)
		{
			if (getSelectionGroup(pSelectionGroupNode->m_data) == pUnit->getGroup())
			{
				if (bForward)
				{
					pSelectionGroupNode = nextGroupCycleNode(pSelectionGroupNode);
				}
				else
				{
					pSelectionGroupNode = previousGroupCycleNode(pSelectionGroupNode);
				}
				break;
			}

			pSelectionGroupNode = nextGroupCycleNode(pSelectionGroupNode);
		}
	}

	if (pSelectionGroupNode == NULL)
	{
		if (bForward)
		{
			pSelectionGroupNode = headGroupCycleNode();
		}
		else
		{
			pSelectionGroupNode = tailGroupCycleNode();
		}

		if (pbWrap != NULL)
		{
			*pbWrap = true;
		}
	}

	if (pSelectionGroupNode != NULL)
	{
		pFirstSelectionGroupNode = pSelectionGroupNode;

		while (true)
		{
			pLoopSelectionGroup = getSelectionGroup(pSelectionGroupNode->m_data);
			FAssertMsg(pLoopSelectionGroup != NULL, "LoopSelectionGroup is not assigned a valid value");

			if (pLoopSelectionGroup->readyToSelect())
			{
				if (pUnit && pLoopSelectionGroup == pUnit->getGroup())
				{
					if (pbWrap != NULL)
					{
						*pbWrap = true;
					}
				}

				return pLoopSelectionGroup;
			}

			if (bForward)
			{
				pSelectionGroupNode = nextGroupCycleNode(pSelectionGroupNode);

				if (pSelectionGroupNode == NULL)
				{
					pSelectionGroupNode = headGroupCycleNode();

					if (pbWrap != NULL)
					{
						*pbWrap = true;
					}
				}
			}
			else
			{
				pSelectionGroupNode = previousGroupCycleNode(pSelectionGroupNode);

				if (pSelectionGroupNode == NULL)
				{
					pSelectionGroupNode = tailGroupCycleNode();

					if (pbWrap != NULL)
					{
						*pbWrap = true;
					}
				}
			}

			if (pSelectionGroupNode == pFirstSelectionGroupNode)
			{
				break;
			}
		}
	}

	return NULL;
}


bool CvPlayer::hasTrait(TraitTypes eTrait) const
{
	return (getTraitCount(eTrait) > 0);
}

int CvPlayer::getTraitCount(TraitTypes eTrait) const
{
	FAssert(eTrait >= 0 && eTrait < GC.getNumTraitInfos());
	return m_aiTraitCount[eTrait];
}

void CvPlayer::changeTraitCount(TraitTypes eTrait, int iChange)
{
	FAssert(eTrait >= 0 && eTrait < GC.getNumTraitInfos());
	m_aiTraitCount[eTrait] += iChange;
	FAssert(getTraitCount(eTrait) >= 0);
}


int CvPlayer::getMercantileFactor() const
{
	int iMercantileFactor = 100;
	for (int iTrait=0;iTrait<GC.getNumTraitInfos();iTrait++)
	{
		TraitTypes eTrait = (TraitTypes) iTrait;
		if (hasTrait(eTrait))
		{
			iMercantileFactor *= (100 + GC.getTraitInfo(eTrait).getMercantileFactor());
			iMercantileFactor /= 100;
		}
	}

	return iMercantileFactor - 100;
}

bool CvPlayer::isHuman() const
{
	return m_bHuman;
}

void CvPlayer::updateHuman()
{
	if (getID() == NO_PLAYER)
	{
		m_bHuman = false;
	}
	else
	{
		bool bWasHuman = isHuman();
		m_bHuman = GC.getInitCore().getHuman(getID());
		if (bWasHuman && !isHuman() && isTurnActive())
		{
			setEndTurn(true);
		}
	}
}

void CvPlayer::definePlayerRole()
{
	PlayerRoleTypes ePlayerRole = NO_PLAYER_ROLE;
	CivilizationTypes eCivilizationType = getCivilizationType();
	if (eCivilizationType != NO_CIVILIZATION)
	{
		if (GC.getCivilizationInfo(eCivilizationType).isNative())
		{
			ePlayerRole = PLAYER_ROLE_NATIVE;
		}
		else if (GC.getCivilizationInfo(getCivilizationType()).getDerivativeCiv() != NO_CIVILIZATION)
		{
			ePlayerRole = PLAYER_ROLE_KING;
		}
		else
		{
			ePlayerRole = PLAYER_ROLE_EUROPEAN;
		}
	}

	setPlayerRole(ePlayerRole);
}

bool CvPlayer::isNative() const
{
	return getPlayerRole() == PLAYER_ROLE_NATIVE;
}

bool CvPlayer::isEurope() const
{
	return getPlayerRole() == PLAYER_ROLE_KING;
}

bool CvPlayer::isEuropean() const 
{
	return getPlayerRole() == PLAYER_ROLE_EUROPEAN;
}

bool CvPlayer::isEuropeanAI() const 
{
	return isEuropean() && !isHuman();
}

bool CvPlayer::isAlwaysOpenBorders() const
{
	if (getCivilizationType() == NO_CIVILIZATION)
	{
		return false;
	}

	return GC.getCivilizationInfo(getCivilizationType()).isOpenBorders();
}

const wchar* CvPlayer::getName(uint uiForm) const
{
	if (isEmpty(gDLL->getPlayerName(getID(), uiForm)) || (GC.getGameINLINE().isMPOption(MPOPTION_ANONYMOUS) && isAlive() && GC.getGameINLINE().getGameState() == GAMESTATE_ON))
	{
		return GC.getLeaderHeadInfo(getLeaderType()).getDescription(uiForm);
	}
	else
	{
		return gDLL->getPlayerName(getID(), uiForm);
	}
}


const wchar* CvPlayer::getNameKey() const
{
	if (isEmpty(gDLL->getPlayerNameKey(getID())) || (GC.getGameINLINE().isMPOption(MPOPTION_ANONYMOUS) && isAlive()))
	{
		return GC.getLeaderHeadInfo(getLeaderType()).getTextKeyWide();
	}
	else
	{
		return gDLL->getPlayerNameKey(getID());
	}
}


const wchar* CvPlayer::getCivilizationDescription(uint uiForm) const
{
	if (isEmpty(gDLL->getCivDescription(getID(), uiForm)))
	{
		return GC.getCivilizationInfo(getCivilizationType()).getDescription(uiForm);
	}
	else
	{
		return gDLL->getCivDescription(getID(), uiForm);
	}
}


const wchar* CvPlayer::getCivilizationDescriptionKey() const
{
	if (isEmpty(gDLL->getCivDescriptionKey(getID())))
	{
		return GC.getCivilizationInfo(getCivilizationType()).getTextKeyWide();
	}
	else
	{
		return gDLL->getCivDescriptionKey(getID());
	}
}


const wchar* CvPlayer::getCivilizationShortDescription(uint uiForm) const
{
	if (isEmpty(gDLL->getCivShortDesc(getID(), uiForm)))
	{
		return GC.getCivilizationInfo(getCivilizationType()).getShortDescription(uiForm);
	}
	else
	{
		return gDLL->getCivShortDesc(getID(), uiForm);
	}
}


const wchar* CvPlayer::getCivilizationShortDescriptionKey() const
{
	if (isEmpty(gDLL->getCivShortDescKey(getID())))
	{
		return GC.getCivilizationInfo(getCivilizationType()).getShortDescriptionKey();
	}
	else
	{
		return gDLL->getCivShortDescKey(getID());
	}
}


const wchar* CvPlayer::getCivilizationAdjective(uint uiForm) const
{
	if (isEmpty(gDLL->getCivAdjective(getID(), uiForm)))
	{
		return GC.getCivilizationInfo(getCivilizationType()).getAdjective(uiForm);
	}
	else
	{
		return gDLL->getCivAdjective(getID(), uiForm);
	}
}

const wchar* CvPlayer::getCivilizationAdjectiveKey() const
{
	if (isEmpty(gDLL->getCivAdjectiveKey(getID())))
	{
		return GC.getCivilizationInfo(getCivilizationType()).getAdjectiveKey();
	}
	else
	{
		return gDLL->getCivAdjectiveKey(getID());
	}
}


const char* CvPlayer::getFlagDecal() const
{
	return GC.getCivilizationInfo(getCivilizationType()).getFlagTexture();
}

bool CvPlayer::isWhiteFlag() const
{
	return GC.getCivilizationInfo(getCivilizationType()).getArtInfo()->isWhiteFlag();
}

bool CvPlayer::isInvertFlag() const
{
	return GC.getCivilizationInfo(getCivilizationType()).getArtInfo()->isInvertFlag();
}

const CvWString CvPlayer::getWorstEnemyName() const
{
	TeamTypes eWorstEnemy;

	eWorstEnemy = GET_TEAM(getTeam()).AI_getWorstEnemy();

	if (eWorstEnemy != NO_TEAM)
	{
		return GET_TEAM(eWorstEnemy).getName();
	}

	return "";
}

ArtStyleTypes CvPlayer::getArtStyleType() const
{
	if (GC.getInitCore().getArtStyle(getID()) == NO_ARTSTYLE)
	{
		return ((ArtStyleTypes)(GC.getCivilizationInfo(getCivilizationType()).getArtStyleType()));
	}
	else
	{
		return GC.getInitCore().getArtStyle(getID());
	}
}

const TCHAR* CvPlayer::getUnitButton(UnitTypes eUnit) const
{
	if (getCivilizationType() != NO_CIVILIZATION)
	{
		return GC.getUnitInfo(eUnit).getUnitArtStylesArtInfo(0, NO_PROFESSION, (UnitArtStyleTypes) GC.getCivilizationInfo(getCivilizationType()).getUnitArtStyleType())->getButton();
	}
	else
	{
		return GC.getUnitInfo(eUnit).getArtInfo(0, NO_PROFESSION)->getButton();
	}
}

void CvPlayer::doTurn()
{
	PROFILE_FUNC();

	CvCity* pLoopCity;
	int iLoop;

	FAssertMsg(isAlive(), "isAlive is expected to be true");
	FAssertMsg(!hasBusyUnit() || GC.getGameINLINE().isMPOption(MPOPTION_SIMULTANEOUS_TURNS)  || GC.getGameINLINE().isSimultaneousTeamTurns(), "End of turn with busy units in a sequential-turn game");

	gDLL->getEventReporterIFace()->beginPlayerTurn( GC.getGameINLINE().getGameTurn(),  getID());

	doEra();
	doUpdateCacheOnTurn();
	processMapsTeam();

	GC.getGameINLINE().verifyDeals();

	AI_doTurnPre();

	AI_assignWorkingPlots();

	resetEuropeTradeYield();

	doTaxes();

	doGold();

	doBells();

	doCrosses();

	processPirates();

	testHumanHasInfluenceToNative();

	refreshHistoryForImmigration();

	kingGiveScout();//TODO Check all these things in a central function

	generateUsedShipInEurope();

	doTurnTradeRoutes();

	processNewShipBuyInEurope();

	clearLaunchedPopup();

	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		pLoopCity->doTurn();
	}

	verifyCivics();

	doEvents();

	interceptEuropeUnits();

	doHospitals();

	doUnitsRebelSentiment();

	doEuropeLandPercent();

	processUnitMarketPrice();

	processYieldPrice();

	checkAgreementsDeadline();

	updateYieldUnknowWithOtherEuropeans();

	setPenalitePlayerSave(false);
	setPenalitePlayerLoad(false);

	updateEconomyHistory();
	updateIndustryHistory(GC.getGameINLINE().getGameTurn(), calculateTotalRawSellableYield());
	updateAgricultureHistory(GC.getGameINLINE().getGameTurn(), calculateTotalYield(YIELD_FOOD));
	updateUnitsHistory();
	updateCultureHistory(GC.getGameINLINE().getGameTurn(), countTotalCulture());
	doImmigrationHistory();
	expireMessages();  // turn log
	m_aszTradeMessages.clear();
	m_aszEuropeTradeMessages.clear();
	m_aszEventEuropeMessages.clear();

	gDLL->getInterfaceIFace()->setDirty(CityInfo_DIRTY_BIT, true);

	doUnitsTravelTimer();

	AI_doTurnPost();

	processEuropeImmigration();
	processImmigrationRelationPoints();
	processIncomeAndExpendituresRecord();

	gDLL->getEventReporterIFace()->endPlayerTurn( GC.getGameINLINE().getGameTurn(), getID());

	FAssert(checkPower(false));
	FAssert(checkPopulation());
}

void CvPlayer::doTurnTradeRoutes()
{
	int iLoop;

	for (CvTradeRoute* pTradeRoute = firstTradeRoute(&iLoop); pTradeRoute != NULL; pTradeRoute = nextTradeRoute(&iLoop))
	{
		pTradeRoute->doTurn();
	}
}


void CvPlayer::doTurnUnits()
{
	PROFILE_FUNC();

	CvSelectionGroup* pLoopSelectionGroup;
	int iLoop;

	AI_doTurnUnitsPre();

	for (pLoopSelectionGroup = firstSelectionGroup(&iLoop); pLoopSelectionGroup != NULL; pLoopSelectionGroup = nextSelectionGroup(&iLoop))
	{
		pLoopSelectionGroup->doDelayedDeath();
	}

	for (pLoopSelectionGroup = firstSelectionGroup(&iLoop); pLoopSelectionGroup != NULL; pLoopSelectionGroup = nextSelectionGroup(&iLoop))
	{
		pLoopSelectionGroup->doTurn();
	}

	for (int i = 0; i < getNumEuropeUnits(); ++i)
	{
		CvUnit* pUnit = getEuropeUnit(i);
		if (pUnit != NULL)
		{
			if (!pUnit->isLikelyToLeave() && pUnit->isHurt())
			{
				pUnit->doHeal();
			}
		}
	}

	if (getParent() != NO_PLAYER)
	{
		CvPlayer& kEurope = GET_PLAYER(getParent());
		if (kEurope.isAlive() && kEurope.isEurope() && !::atWar(getTeam(), kEurope.getTeam()))
		{
			processReinforcement();

			doEuropeLandPercent();
		}
	}

	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getFAStarIFace()->ForceReset(&GC.getInterfacePathFinder());

		gDLL->getInterfaceIFace()->setDirty(Waypoints_DIRTY_BIT, true);
		gDLL->getInterfaceIFace()->setDirty(SelectionButtons_DIRTY_BIT, true);
	}

	gDLL->getInterfaceIFace()->setDirty(UnitInfo_DIRTY_BIT, true);

	AI_doTurnUnitsPost();
}

void CvPlayer::doUnitsTravelTimer() 
{
	int iLoop;

	for (CvSelectionGroup* pLoopSelectionGroup = firstSelectionGroup(&iLoop); pLoopSelectionGroup != NULL; pLoopSelectionGroup = nextSelectionGroup(&iLoop))
	{
		CLLNode<IDInfo>* pUnitNode;
		CvUnit* pLoopUnit;
		CvUnit* pHeadUnit;

		if (pLoopSelectionGroup->getNumUnits() > 0)
		{
			if (pLoopSelectionGroup->isAutomated())
			{
				pHeadUnit = pLoopSelectionGroup->getHeadTransportSeaUnit();
				if (pHeadUnit != NULL && pHeadUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
				{
					int iSeawayId = pLoopSelectionGroup->AI_getSeawayId();
					if (iSeawayId != -1)
					{
						CvSeaway* pSeaway = getSeawayByID(iSeawayId);
						if (pSeaway != NULL)
						{
							CvPlot* pSeawayPlot = pSeaway->plot();
							if (pSeawayPlot != pLoopSelectionGroup->plot())
							{
								pHeadUnit->moveToSeaway(pSeaway);
							}
						}
					}
					if (pLoopSelectionGroup->getAutomateType() != AUTOMATE_TRANSPORT_ROUTES || (pHeadUnit->getConvoy() != NULL && !pLoopSelectionGroup->isWaiting()))
					{
						//In case of trade route automation, we cross ocean only if the group has a trade route
						pHeadUnit->crossOcean(UNIT_TRAVEL_STATE_FROM_EUROPE);
					}
				}
			}
			// do unit's turns (checking for damage)
			pUnitNode = pLoopSelectionGroup->headUnitNode();
			while (pUnitNode != NULL)
			{
				pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pLoopSelectionGroup->nextUnitNode(pUnitNode);

				pLoopUnit->doUnitTravelTimer();
			}
		}
	}
}

/** Call to help european player via king gift */
void CvPlayer::processReinforcement()
{
	if (getNewEra() > 0)
	{
		return;
	}
	if (getParent() == NO_PLAYER)
	{
		return;
	}
	CvPlayer& kEurope = GET_PLAYER(getParent());

	int iTurn = GC.getGameINLINE().getGameTurn();
	int iMaxturn = GC.getGameINLINE().getMaxTurns();
	int iUnitIndex;

	if (shouldGiveBuilderUnits(&iUnitIndex))
	{
		int iLoop;
		CvCity* pFirstCity = firstCity(&iLoop);
		// Should not give wagon builder on little islands
		bool bShouldGiveShip = pFirstCity == NULL || pFirstCity->area() == NULL || pFirstCity->area()->getNumUnownedTiles() < 30;
		if (iUnitIndex == 1) 
		{
			if (bShouldGiveShip || getRandomValues(0) < 75)
			{
				receiveBuilderShip();
				if (getID() == GC.getGameINLINE().getActivePlayer())
				{
					CvDiploParameters* pDiplo = new CvDiploParameters(kEurope.getID());
					pDiplo->setDiploComment((DiploCommentTypes) GC.getInfoTypeForString("AI_DIPLO_KING_GIFT_FIRST_MERCHANT_SHIP"));
					pDiplo->setAIContact(true);
					gDLL->beginDiplomacy(pDiplo, getID());
				}
			}
			else
			{
				receiveBuilderWagon();
				if (getID() == GC.getGameINLINE().getActivePlayer())
				{
					CvDiploParameters* pDiplo = new CvDiploParameters(kEurope.getID());
					pDiplo->setDiploComment((DiploCommentTypes) GC.getInfoTypeForString("AI_DIPLO_KING_GIFT_FIRST_BUILDER_WAGON"));
					pDiplo->addDiploCommentVariable(pFirstCity->getName().GetCString());
					pDiplo->setAIContact(true);
					gDLL->beginDiplomacy(pDiplo, getID());
				}
			}
		} 
		else if (iUnitIndex == 2)
		{
			if (bShouldGiveShip || getRandomValues(1) < 50)
			{
				receiveBuilderShip();
				if (getID() == GC.getGameINLINE().getActivePlayer())
				{
					CvDiploParameters* pDiplo = new CvDiploParameters(kEurope.getID());
					pDiplo->setDiploComment((DiploCommentTypes) GC.getInfoTypeForString("AI_DIPLO_KING_GIFT_SECOND_MERCHANT_SHIP"));
					pDiplo->setAIContact(true);
					gDLL->beginDiplomacy(pDiplo, getID());
				}
			}
			else
			{
				receiveBuilderWagon();
				if (getID() == GC.getGameINLINE().getActivePlayer())
				{
					CvDiploParameters* pDiplo = new CvDiploParameters(kEurope.getID());
					pDiplo->setDiploComment((DiploCommentTypes) GC.getInfoTypeForString("AI_DIPLO_KING_GIFT_SECOND_BUILDER_WAGON"));
					pDiplo->setAIContact(true);
					gDLL->beginDiplomacy(pDiplo, getID());
				}
			}
		}
	}
}

bool CvPlayer::shouldGiveBuilderUnits(int* pIterUnitIndex) const
{
	int iTurn = GC.getGameINLINE().getGameTurn();
	bool bIsHuman = isHuman();
	const int iMinTurn = 22;
	int iRandomOffsetTurn = bIsHuman ? getRandomValues(0) % 5 : getRandomValues(0) % 3;
	int iSecondRandomOffsetTurn = getRandomValues(1) % 3 + 2;
	int iNewEra = getNewEra();

	// We initiailize the default value
	*pIterUnitIndex = 0;

	if (getNumCities() == 0)
	{
		return false;
	}

	// If turn less than min allowed turn or if not in a first era 
	if (iTurn < (iMinTurn + iRandomOffsetTurn) || iNewEra > 1)
	{
		return false;
	}

	// If equals to the dedicated turn
	if (iTurn == (iMinTurn + iRandomOffsetTurn))
	{
		*pIterUnitIndex = 1;
		return true;
	}

	// For AI, do not give builder unit if it will receive one soon
	if (iTurn < (iMinTurn + iRandomOffsetTurn) && getNumCities() >= 1)
	{
		return false;
	}
	
	NewEraTypes eNewEra = getNextNewEra();
	CvTeam& kTeam = GET_TEAM(getTeam());
	int iRequiredNumCities = kTeam.getRequiredNumCities(eNewEra) / kTeam.getAliveEuropeanPlayers();
	if (iRequiredNumCities > 2) 
	{
		// If equals to the dedicated turn
		if (iTurn == (iMinTurn + iRandomOffsetTurn + iSecondRandomOffsetTurn))
		{
			*pIterUnitIndex = 2;
			return true;
		}

		// For AI, do not give builder unit if it will receive one soon
		if (iTurn < (iMinTurn + iRandomOffsetTurn + iSecondRandomOffsetTurn) && getNumCities() >= 2)
		{
			return false;
		}
	}

	//For AI every 5 turns
	if (isEuropeanAI() && iTurn % 5 == 0)
	{
		//IF has enough cities for the next era condition
		if (kTeam.hasReachedNumCitiesRequirement(eNewEra))
		{
			return false;
		}
		// if AI does not have ships with builder
		if (!hasBuilderUnits())
		{
			return true;
		}
	}
	return false;
}

bool CvPlayer::receiveBuilderShip()
{
	UnitTypes eCheapestShip = getCheapestShip();
	CvCivilizationInfo& kCivilizationInfo = GC.getCivilizationInfo(getCivilizationType());
	//We create ship first
	CvUnit* pTransportUnit = addFreeUnit(eCheapestShip, NO_PROFESSION);
	if (pTransportUnit == NULL)
	{
		return false;
	}

	pTransportUnit->setHaveFoundPack(true);
	// and add units into them after
	for (int iI = 0; iI < kCivilizationInfo.getNumCivilizationFreeUnits(); iI++)
	{
		int iLoopUnitClass = kCivilizationInfo.getCivilizationFreeUnitsClass(iI);
		ProfessionTypes eLoopUnitProfession = (ProfessionTypes) kCivilizationInfo.getCivilizationFreeUnitsProfession(iI);
		UnitTypes eLoopUnit = (UnitTypes)kCivilizationInfo.getCivilizationUnits(iLoopUnitClass);
		if (eLoopUnit != NO_UNIT && !GC.getUnitInfo(eLoopUnit).isMechUnit())
		{
			CvUnit* pUnit = addFreeUnit(eLoopUnit, eLoopUnitProfession);
			if (pTransportUnit != NULL)
			{
				pUnit->setTransportUnit(pTransportUnit);
			}
		}
	}

	return true;
}

bool CvPlayer::receiveBuilderWagon()
{
	int iLoop;
	CvCity* pLoopCity = firstCity(&iLoop);
	if (pLoopCity == NULL)
	{
		return false;
	}
	CvUnit* pUnit = initUnit(UNIT_BUILDER_WAGON, NO_PROFESSION, pLoopCity->getX_INLINE(), pLoopCity->getY_INLINE());
	if (pUnit == NULL) 
	{
		return false;
	}
	CvCivilizationInfo& kCivilizationInfo = GC.getCivilizationInfo(getCivilizationType());
	for (int iI = 0; iI < kCivilizationInfo.getNumCivilizationFreeUnits(); iI++)
	{
		int iLoopUnitClass = kCivilizationInfo.getCivilizationFreeUnitsClass(iI);
		ProfessionTypes eLoopUnitProfession = (ProfessionTypes) kCivilizationInfo.getCivilizationFreeUnitsProfession(iI);
		UnitTypes eLoopUnit = (UnitTypes)kCivilizationInfo.getCivilizationUnits(iLoopUnitClass);
		if (eLoopUnit != NO_UNIT && !GC.getUnitInfo(eLoopUnit).isMechUnit())
		{
			initUnit(eLoopUnit, eLoopUnitProfession, pLoopCity->getX_INLINE(), pLoopCity->getY_INLINE());
		}
	}

	return true;
}

bool CvPlayer::hasBuilderUnits() const
{
	int iLoop;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		if (pLoopUnit->isHaveFoundPack())
		{
			return true;
		}
		if (pLoopUnit->getUnitInfo().isFound())
		{
			return true;
		}
	}
	return false;
}

void CvPlayer::testHumanHasInfluenceToNative()
{
	if (!isHuman() || !isOption(PLAYEROPTION_TUTORIAL_DOANE) || isHadTutorialDoane(1))
	{
		return;
	}

	for (int iI = 0; iI < MAX_PLAYERS; iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER((PlayerTypes)iI);
		if (kPlayer.isAlive() && kPlayer.isNative())
		{
			int iLoop;
			for (CvCity* pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
			{
				CvPlot* pPlot = pLoopCity->plot();
				if (pPlot->calculateCulturePercent(getID()) > 20)
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_TUTORIAL_DOANE_1);
					gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
					setHadTutorialDoane(1, true);
				}
			}
		}
	}
}

// -----------------------------------------
//
//    START : DOANE Pirates
//
// -----------------------------------------

void CvPlayer::processPirates()
{
	if (getParent() == NO_PLAYER)
	{
		return;
	}
	if (isInRevolution())
	{
		return;
	}
	if (GC.getHandicapInfo(getHandicapType()).getPiratesPercent() == 0)
	{
		return;
	}

	for (int iI = 0; iI < GC.getNumPirateInfos(); ++iI)
	{
		if (!hasGeneratedPirates(iI))
		{
			CvPirateInfo& kPirateInfo = GC.getPirateInfo((PirateInfoTypes)iI);
			if (processPirate(kPirateInfo))
			{
				setIfHasGeneratedPirates(iI, true);
				break;// I don't want two generation of pirates during the same turn for one player
			}
		}
	}
}

bool CvPlayer::processPirate(CvPirateInfo& kPirateInfo) 
{
	if (getNewEra() != kPirateInfo.getNewEra())
	{
		return false;
	}

	HandicapTypes eHandicapType = getHandicapType();
	if (kPirateInfo.getMinHandicapType() > eHandicapType)
	{
		return false;
	}
	if (eHandicapType > kPirateInfo.getMaxHandicapType())
	{
		return false;
	}

	int iCurrentResourcesSoldInEurope = getNewWorldYieldQuantity();
	int iRequiredResourcesSoldInEurope = getRandomValueBetweenRange(kPirateInfo.getMinResourcesSoldInEurope(), kPirateInfo.getMaxResourcesSoldInEurope());
	iRequiredResourcesSoldInEurope += iRequiredResourcesSoldInEurope * GC.getHandicapInfo(eHandicapType).getResourcesSoldInEuropeModifierPercent() / 100;
	if (iCurrentResourcesSoldInEurope < iRequiredResourcesSoldInEurope)
	{
		return false;
	}

	int iNumPiratesToCreate = getRandomValueBetweenRange(kPirateInfo.getMinCreated(), kPirateInfo.getMaxCreated());

	bool hasCreatedPirates = false;
	for (int iI = 0; iI < iNumPiratesToCreate; ++iI)
	{
		CvUnit* pPirate = addPirate((UnitTypes) kPirateInfo.getUnitType());
		if (pPirate != NULL)
		{
			hasCreatedPirates = true;
		}
	}

	bool shouldReceiveUnitFromKing = hasCreatedPirates || kPirateInfo.getMaxCreated() == 0;
	if (shouldReceiveUnitFromKing && kPirateInfo.hasKingAlert())
	{
		uint iRandValue = GC.getGameINLINE().getSorenRandNum(100, "pirate alert info probability");
		if (iRandValue < kPirateInfo.getHelpProbabilty())
		{
			CvString commentTextKey = wasAttackedByPirates() ? kPirateInfo.getDiploCommentTypeAttackedByPiratesTextKey() : kPirateInfo.getDiploCommentTextKey();
			CvUnit* pUnit = receiveUnitFromTheKing((UnitTypes) kPirateInfo.getUnitTypeOfferedByKing(), commentTextKey);
			if (pUnit != NULL)
			{
				pUnit->addSailorPromotionsWithCoef(kPirateInfo.getNavigationPromotionCoefficient(), kPirateInfo.getMilitaryPromotionCoefficient());
			}
		}
	}

	return true;
}

void CvPlayer::pirateUpgrade(int iLevel)
{
	if (getParent() == NO_PLAYER)
	{
		return;
	}
	UnitTypes ePirateUnit = NO_UNIT;

	for (int iI = 0;iI < GC.getNumUnitInfos();iI++)
	{
		UnitTypes eUnit = (UnitTypes) iI;
		if (GC.getUnitInfo(eUnit).getPirateLevel() == iLevel)
		{
			ePirateUnit = eUnit;
			break;
		}
	}
	if (ePirateUnit == NO_UNIT)
	{
		return;
	}
	CvPlayer& kKingPlayer = GET_PLAYER(getParent());
	int iLoop;
	for (CvUnit* pLoopUnit = kKingPlayer.firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = kKingPlayer.nextUnit(&iLoop))
	{
		int iPirateLevel = pLoopUnit->getUnitInfo().getPirateLevel();
		if (iPirateLevel > 0)
		{
			//Les unités pirates
			if (iPirateLevel < iLevel)
			{
				if ( GC.getGameINLINE().getSorenRandNum(100, "pirateUpgrade")%3 == 0)
				{
					pLoopUnit->upgradePirate();
				}
			}
		}
	}
}

int CvPlayer::countNumPirates()
{
	if (getParent() == NO_PLAYER)
	{
		return 0;
	}
	int iTotal = 0;
	int iLoop;
	CvPlayer& kKingPlayer = GET_PLAYER(getParent());
	for (CvUnit* pLoopUnit = kKingPlayer.firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = kKingPlayer.nextUnit(&iLoop))
	{
		if (pLoopUnit->getUnitInfo().getPirateLevel()>0)
		{
			//Les unités pirates
			iTotal++;
		}
	}
	return iTotal;
}

bool CvPlayer::hasEscortUnit()
{
	int iLoop;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		if (pLoopUnit->getUnitType() == UNIT_FRIGATE)
		{
			return true;
		}
		if (pLoopUnit->getUnitType() == UNIT_MILITARY_ESCORT)
		{
			return true;
		}
	}

	for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
	{
		CvUnit* pLoopUnit = m_aEuropeUnits[i];
		if (pLoopUnit->getUnitType() == UNIT_FRIGATE)
		{
			return true;
		}
		if (pLoopUnit->getUnitType() == UNIT_MILITARY_ESCORT)
		{
			return true;
		}
	}
	return false;
}

CvUnit* CvPlayer::receiveUnitFromTheKing(UnitTypes eUnit, const CvString szDiploCommentTextKey)
{
	PlayerTypes eEuropePlayer = getParent();
	if (eUnit == NO_UNIT || eEuropePlayer == NO_PLAYER)
	{
		return NULL;
	}
	CvPlayer& kEurope = GET_PLAYER(eEuropePlayer);
	CvDiploParameters* pDiplo = new CvDiploParameters(kEurope.getID());
	CvUnit* pUnit = buyEuropeUnit(eUnit, 0);
	if (pUnit == NULL)
	{
		return NULL;
	}
	pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLO_KING_GIFT_WAR_SHIP"));
	pDiplo->addDiploCommentVariable(gDLL->getText(szDiploCommentTextKey, pUnit->getUnitInfo().getDescription()));
	pDiplo->setAIContact(true);
	gDLL->beginDiplomacy(pDiplo, getID());

	return pUnit;
}

CvUnit* CvPlayer::addPirate(UnitTypes ePirateUnit, bool bUpdate)
{
	if (getParent() == NO_PLAYER || ePirateUnit == NO_UNIT)
	{
		return NULL;
	}
	CvPlayer& kKingPlayer = GET_PLAYER(getParent());
	CvUnit* pPirateUnit = NULL;

	int iHandicapPercent = GC.getHandicapInfo(getHandicapType()).getPiratesPercent();
	int iRand = GC.getGameINLINE().getSorenRandNum(100, "iPirate percent handicap");
	if (bUpdate || iRand < iHandicapPercent)
	{

		CvSeaway* pBestSeaway = getBestSeawayForPirate();
		CvPlot* pBestPlot = getStartingPlot();
		if (pBestSeaway != NULL)
		{
			pBestPlot = pBestSeaway->plot();
		}
		if (pBestPlot != NULL)
		{
			pPirateUnit = kKingPlayer.initUnit(ePirateUnit, NO_PROFESSION, pBestPlot->getX_INLINE(), pBestPlot->getY_INLINE(), UNITAI_PIRATE_SEA);
		}
		if (pPirateUnit != NULL)
		{
			pPirateUnit->setTargetPlayer(getID());
			pPirateUnit->setTempInvisible(true);
			int iNumNames = GC.getUnitInfo(ePirateUnit).getNumUnitNames();
			int iOffset = GC.getGameINLINE().getSorenRandNum(iNumNames, "Unit name selection");
			bool bMissingNameForPirates = true;
			for (int iI = 0; iI < iNumNames; iI++)
			{
				int iIndex = (iI + iOffset) % iNumNames;
				CvWString szName = gDLL->getText(GC.getUnitInfo(ePirateUnit).getUnitNames(iIndex));
				if (!GC.getGameINLINE().isPirateBorn(szName))
				{
					pPirateUnit->setName(szName);
					GC.getGameINLINE().addPirateBornName(szName);
					bMissingNameForPirates = false;
					break;
				}
			}
			if (bMissingNameForPirates)
			{
				CvString szError;
				szError.Format("WARNING one pirate added without unique name for him");
				gDLL->logMsg("DoaNE.log", szError);
			}
		}
	}

	return pPirateUnit;
}

CvUnit* CvPlayer::addPirate(int iLevel, bool bUpdate)
{
	UnitTypes ePirateUnit = NO_UNIT;
	for (int iI = 0;iI < GC.getNumUnitInfos();iI++)
	{
		UnitTypes eUnit = (UnitTypes) iI;
		if (GC.getUnitInfo(eUnit).getPirateLevel() > 0)
		{
			if (GC.getUnitInfo(eUnit).getPirateLevel() == iLevel)
			{
				ePirateUnit = eUnit;
			}
		}
	}
	return addPirate(ePirateUnit, bUpdate);
}

CvSeaway* CvPlayer::getBestSeawayForPirate() const
{
	CLLNode<CvSeaway>* pSeawayNode;
	CvSeaway* pBestSeaway = NULL;
	int iBestValue = 0;
	int iRange = 5, iValue;

	pSeawayNode = headSeawayNode();

	while (pSeawayNode != NULL)
	{
		CvPlot* pSeawayPlot = pSeawayNode->m_data.plot();
		iValue = 0;
		for (int iX = -iRange; iX <= iRange; iX++)
		{
			for (int iY = -iRange; iY <= iRange; iY++)
			{
				if (iX != 0 || iY != 0)
				{
					CvPlot* pLoopPlot = plotXY(pSeawayPlot->getX_INLINE(), pSeawayPlot->getY_INLINE(), iX, iY);
					if (pLoopPlot != NULL && pLoopPlot->isWater())
					{
						iValue += pLoopPlot->getCrumbs();
					}
				}
			}
		}
		if (iBestValue < iValue)
		{
			iBestValue = iValue;
			pBestSeaway = &(pSeawayNode->m_data);
		}
		pSeawayNode = nextSeawayNode(pSeawayNode);
	}
	return pBestSeaway;
}

int CvPlayer::getNewWorldYieldQuantity() const
{	
	if (getParent() == NO_PLAYER)
	{
		return 0;
	}
	int iTotal = 0;
	CvPlayer& kKingPlayer = GET_PLAYER(getParent());

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		switch(eYield)
		{
		case YIELD_FOOD:
		case YIELD_SILVER:
		case YIELD_COTTON:
		case YIELD_FUR:
		case YIELD_SUGAR:
		case YIELD_TOBACCO:
		case YIELD_INDIGO:
		case YIELD_COFFEE:
		case YIELD_HEMP:
		case YIELD_ARACHIDE:
		case YIELD_CACAO:
		case YIELD_CLOTH:
		case YIELD_COATS:
		case YIELD_RUM:
		case YIELD_CIGARS:
		case YIELD_TOOLS:
		case YIELD_SWORDS:
		case YIELD_MUSKETS:
		case YIELD_CANNON:
		case YIELD_HORSES:
			iTotal += kKingPlayer.getYieldBoughtTotal(eYield);
			break;
		}
	}

	return iTotal;
}
// -----------------------------------------
//
//    END : DOANE Pirates
//
// -----------------------------------------

int CvPlayer::calculateProbaImmigration(UnitTypes eUnit, ProfessionTypes eProfession)
{
	if (getParent() == NO_PLAYER)
	{
		return 0;
	}

	if (eUnit == NO_UNIT)
	{
		return 0;
	}

	int iProba = GC.getUnitInfo(eUnit).getProbaImmigration();
	CvPlayer& kKingPlayer = GET_PLAYER(getParent());

	iProba = iProba*getGlobalAverageImmigrationPercent()/100;
	iProba -= iProba*kKingPlayer.getUnitMarketPrice((UnitClassTypes)GC.getUnitInfo(eUnit).getUnitClassType())/100;
	iProba += GC.getDefineINT("GENERAL_IMMIGRATION_PERCENT_MODIFY"); //TODO cache

	iProba = iProba*GC.getHandicapInfo(getHandicapType()).getAIImmigration()/100;

	if (eProfession == PROFESSION_SAILOR)
	{
		iProba *= 9;
		iProba /= 40;
	}
	return range(iProba, 0, 100);
}

void CvPlayer::processEuropeImmigration()
{
	if (isNative() || isEurope() || isInRevolution())
	{
		return;
	}

	CvPlayer& kKingPlayer = GET_PLAYER(getParent());
	int iAverageImmigration = getGlobalAverageImmigrationPercent()*GC.getHandicapInfo(getHandicapType()).getAIImmigration()/100;;
	int iTurn = GC.getGameINLINE().getGameTurn();
	int iLoop;

	//DOANE Remove immigrant european units available
	int iDeleteImmig = GC.getGameINLINE().getSorenRandNum(100, "iDeleteImmig");
	int iMinDeleteImmig = GC.getMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_AVAILABLE(); //Turns an inmigrant  will be available to offer a contract before being removed
	for (CvTempUnit* pTempUnit = firstTempUnit(&iLoop); pTempUnit != NULL; pTempUnit = nextTempUnit(&iLoop))
	{
		if ((iTurn - pTempUnit->getGameTurnCreated()) >= iMinDeleteImmig)
		{ //Don't delete until it reaches the minimum turn
			if (iDeleteImmig > 100 - (iTurn - pTempUnit->getGameTurnCreated() - iMinDeleteImmig)*35)
			{ //Count turns from the limit iMinDeleteImmig
				removeTempUnit(pTempUnit->getID());
			}
		}
	}
	//END DOANE Remove immigrant european units available

	//DOANE Remove units from the docks
	int iDeleteNewUnit = GC.getGameINLINE().getSorenRandNum(100, "iDeleteNewUnit");
	int iMinDeleteNewUnit = GC.getMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_DOCK(); //TODO Cache //Turns you can safely have an inmigrant waiting in European docks without being removed
	for (int i = 0; i < getNumEuropeUnits(); ++i)
	{
		CvUnit* pUnit = getEuropeUnit(i);
		if (pUnit != NULL)
		{
			if (pUnit->isLikelyToLeave() && isHuman())
			{
				if ((iTurn - pUnit->getGameTurnCreated()) >= iMinDeleteNewUnit)
				{
					//Don't delete until it reaches the minimum turn
					if (iDeleteNewUnit > 200 - (iTurn - pUnit->getGameTurnCreated() - iMinDeleteNewUnit)*25)
					{
						//Count turns from the limit iMinDeleteNewUnit
						changeLeaveImmigrant(1);
						changeAcceptImmigrant(-1);
						iDeleteNewUnit = GC.getGameINLINE().getSorenRandNum(100, "iDeleteNewUnit");
						if (pUnit->getEuropeRecruitPrice() > 0)
						{
							int iGold = pUnit->getEuropeRecruitPrice() * 90 / 100;
							addTradeMessage(gDLL->getText("EUROPE_SCREEN_IMMIGRANT_LEAVE", pUnit->getUnitInfo().getDescription(), iGold));
							changeGold(iGold);
						}
						else
						{
							addTradeMessage(gDLL->getText("EUROPE_SCREEN_IMMIGRANT_LEAVE_FREE", pUnit->getUnitInfo().getDescription()));
						}
						pUnit->setWorkingCity(NULL);
						pUnit->removeFromMap();
						pUnit->updateOwnerCache(-1);
						killUnitFromEurope(pUnit);
					}
				}
			}
			else
			{
				pUnit->processCrewFormationTurn();
			}
		}
	}
	//END DOANE Remove units from the docks
	if (isHuman())
	{
		//DOANE Remove units from the cargo:
		// Additional to MIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_DOCK, so you can leave the unit some turns in the dock and then load it to a ship and therefore add some more time before being removed....
		int iMinDeleteCargoUnit = GC.getMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_CARGO(); //Turns you can safely have an inmigrant loaded in one of your ships as cargo without being removed.
		for (CvUnit* pUnit = firstUnit(&iLoop); pUnit != NULL; pUnit = nextUnit(&iLoop))
		{
			if (pUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
			{
				if (pUnit->hasCargo())
				{
					for (CLLNode<IDInfo>* pUnitNode = pUnit->plot()->headUnitNode(); pUnitNode != NULL; pUnitNode = pUnit->plot()->nextUnitNode(pUnitNode))
					{
						CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
						if (pLoopUnit->getTransportUnit() == pUnit && pLoopUnit->isLikelyToLeave())
						{
							if ((iTurn - pUnit->getGameTurnCreated()) >= iMinDeleteCargoUnit)
							{ 
								//Don't delete until it reaches the minimum turn
								if (iDeleteNewUnit > 200 - (iTurn - pLoopUnit->getGameTurnCreated() - iMinDeleteCargoUnit - iMinDeleteNewUnit)*25) //Count turns from the limit (iMinDeleteNewUnit + iMinDeleteCargoUnit)
								{
									pLoopUnit->kill(true);
									changeLeaveImmigrant(1);
									if (isHuman())
									{
										changeAcceptImmigrant(-1);
									}
									iDeleteNewUnit = GC.getGameINLINE().getSorenRandNum(100, "iDeleteNewUnit");
									if (pLoopUnit->getEuropeRecruitPrice() > 0)
									{
										int iGold = pLoopUnit->getEuropeRecruitPrice()*90/100;
										addTradeMessage(gDLL->getText("EUROPE_SCREEN_IMMIGRANT_LEAVE", pLoopUnit->getUnitInfo().getDescription(), iGold));
										changeGold(iGold);
									}
									else
									{
										addTradeMessage(gDLL->getText("EUROPE_SCREEN_IMMIGRANT_LEAVE_FREE", pLoopUnit->getUnitInfo().getDescription()));
									}
									pLoopUnit->kill(true);
								}
							}
						}
					}
				}
			}
		}
		//END DOANE Remove units from the cargo	
	}

	//DOANE Add Crew with promotions
	int iCrewProba = calculateProbaImmigration(UNIT_COLONIST, PROFESSION_SAILOR);
	int iRandomCrewProba = GC.getGameINLINE().getSorenRandNum(100, "random iCrewProba");

	if (iRandomCrewProba < iCrewProba)
	{
		UnitTypes eSailorUnit = NO_UNIT;
		int iRandomUnitType = GC.getGameINLINE().getSorenRandNum(100, "random eUnit");
		if (iRandomUnitType < 80)
		{
			eSailorUnit = UNIT_CRIMINAL;
		}
		else
		{
			eSailorUnit = UNIT_COLONIST;
		}

		if (eSailorUnit != NO_UNIT)
		{
			CvTempUnit* pTempUnit = addTempUnit(eSailorUnit);
			pTempUnit->setProfession(PROFESSION_SAILOR);
			if (pTempUnit != NULL)
			{
				pTempUnit->generatePromotionLevel();
				pTempUnit->generatePrice();
			}
		}
	}
	//END DOANE

	//DOANE Add sailor needed training
	int iTrainingCrewProba = iCrewProba * 3;
	int iRandomTrainingCrewProba = GC.getGameINLINE().getSorenRandNum(100, "random iRandomTrainingCrewProba");

	if (iRandomTrainingCrewProba < iTrainingCrewProba)
	{
		UnitTypes eSailorUnit = NO_UNIT;
		int iRandomUnitType = GC.getGameINLINE().getSorenRandNum(100, "random eUnit");
		if (iRandomUnitType < 80)
		{
			eSailorUnit = UNIT_CRIMINAL;
		}
		else
		{
			eSailorUnit = UNIT_COLONIST;
		}
		if (eSailorUnit != NO_UNIT)
		{
			CvTempUnit* pTempUnit = addTempUnit(eSailorUnit);
			pTempUnit->setProfession(PROFESSION_SAILOR);
			if (pTempUnit != NULL)
			{
				pTempUnit->setRequestTraining(true);
			}
		}
	}
	//END DOANE

	/*	
	*	DOANE Natural Immigration in Europe : 
	*	Probability of appareance: The probability of appareance of an immigrant varies with time. It has a fixed value at the start (iProba1ImmigNaturStart)
	*	and then increases or decreases with time according to the turn number and a modifier (iProba1ImmigNaturPercent).
	*	When the game reaches a specific turn (iTurnImmigNaturChange) this behaviour is changed and the probability becomes a fixed value (iProba1ImmigNaturLate)
	*/

	std::vector<bool> aUnitProposedToCome(GC.getNumUnitInfos(), false);
	int iProba1ImmigNaturStart = GC.getPROBABILITY_NATURAL_IMMIGRATION_START(); //Probability Limit at the start
	int iProba1ImmigNaturLate = GC.getPROBABILITY_NATURAL_IMMIGRATION_LATE(); //Probability Limit at late game
	int iProba1ImmigNaturPercent = GC.getPROBABILITY_NATURAL_IMMIGRATION_PERCENT(); //Modifer applied to the probability, multiplied to game turn
	int iTurnImmigNaturChange = GC.getTURN_NATURAL_IMMIGRATION_CHANGE(); //Turn in wich the probabilty use the late limit instead of the start limit

	int iProba1 = iTurn < iTurnImmigNaturChange ? iProba1ImmigNaturStart + iTurn*iProba1ImmigNaturPercent/100 : iProba1ImmigNaturLate;
	int iRandImmig = GC.getGameINLINE().getSorenRandNum(100, "iRandImmig");
	if (iRandImmig < iProba1)
	{
		iRandImmig = GC.getGameINLINE().getSorenRandNum(100, "iRandImmig");
		if (iRandImmig < iAverageImmigration)
		{
			UnitTypes eBestUnit = pickBestImmigrant();
			if (addImmigrantProposalInEurope(IMMIGRATION_NATURAL, eBestUnit))
			{
				aUnitProposedToCome[eBestUnit] = true;
			}
		}
	}
	//END DOANE Natural Inmigration in Europe

	for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass)
	{
		UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(iUnitClass);
		if (NO_UNIT != eUnit)
		{
			if (getEnrolUnitType((UnitClassTypes) iUnitClass) != 0)
			{
				int iProba = calculateProbaImmigration(eUnit);

				iRandImmig = GC.getGameINLINE().getSorenRandNum(100, "iRandImmig");
				if (iRandImmig < iProba)
				{
					if (addImmigrantProposalInEurope(IMMIGRATION_CONTRACT, eUnit))
					{
						aUnitProposedToCome[eUnit] = true;
					}
				}
				float fProba = (float) (100.0f - iProba) / 100.0f;// We have to take the opposite 
				udpateCurrentImmigrationProbability(eUnit, fProba);
				if (aUnitProposedToCome[eUnit])
				{
					udpateMaxTurnCountWithoutPropositionIfNeeded(eUnit);
					udpateFirstUnitImmigrationProbabilityIfNeeded(eUnit);
					udpateAverageImmigrationProbabilityIfNeeded(eUnit);
					resetCurrentImmigrationProbability(eUnit);
				}
				else
				{
					incrementTurnCountWithoutProposition(eUnit);
				}
				incrementTotalImmigrationAsked(eUnit);
			}
		}
	}
}

CvTempUnit* CvPlayer::addImmigrantProposalInEurope(ImmigrationTypes eImmigrationType, UnitTypes eUnit)
{
	CvTempUnit* pTempUnit = addTempUnit(eUnit);
	if (pTempUnit == NULL)
	{
		return pTempUnit;
	}
	int iCost = 0;

	pTempUnit->setImmigrationType(eImmigrationType);
	if (eImmigrationType == IMMIGRATION_CONTRACT)
	{
		iCost = getEuropeUnitBuyPrice(eUnit);
	}

	if (iCost > 0)
	{
		CvPlayer& kKingPlayer = GET_PLAYER(getParent());
		iCost += iCost * kKingPlayer.getUnitMarketPrice((UnitClassTypes) GC.getUnitInfo(eUnit).getUnitClassType()) / 100;
		iCost += iCost * (GC.getGameINLINE().getSorenRandNum(20, "iRandPrice") - 10) / 100;
	}
	pTempUnit->setPrice(iCost);
	incrementTotalImmigration(eUnit);

	return pTempUnit;
}

int CvPlayer::getGlobalAverageImmigration()
{
	int iAverage = 0;
	int iTotal = getTotalImmigrant();
	int iAccept = getAcceptImmigrant();
	int iRefuse = getLeaveImmigrant();
	int iValueAcceptation = range(iTotal > 0 ? 4*iAccept/iTotal - 2: 0, -2, 2);
	int iValueLeave = range(iAccept - 3*iRefuse, -2, 2);

	iAverage += getAverageReligionHistory();
	iAverage += getAverageEconomicHistory();
	iAverage += getAverageFoodHistory();
	iAverage += iValueAcceptation;
	iAverage += iValueLeave;
	iAverage += getAverageUnemploymentHistory();	
	iAverage += getNewEra()*2;

	return iAverage;
}

int CvPlayer::getGlobalAverageImmigrationPercent()
{
	int iAverage = getGlobalAverageImmigration();
	if (iAverage > 0)
	{
		iAverage /= 2;// We make harder to have immigrants
	}
	iAverage = std::max((10+iAverage)*5, 0);//j'en fais un pourcentage

	if (getNumCities() == 0)
	{
		return 0;
	}

	return iAverage;
}

void CvPlayer::refreshHistoryForImmigration()
{
	if (isNative() || isEurope())
	{
		return;
	}
	refreshUnemploymentStatut();
	refreshFoodStatut();
	refreshReligionStatut();
	refreshEconomicStatut();
}

int CvPlayer::getNumHistTurn() 
{
	return NUM_HIST_TURN;
}

void CvPlayer::refreshUnemploymentStatut()
{
	/*TODO What about checking how many weapons the nation has?
	Players (AI & Human) usually stack units to create soldiers later, these should not be considered as unemployed...
	It could be done this way: consider a colonist as employed per 40 muskets or swords.
	If the player has 200 weapons then 5 colonists are allowed to be unemployed without being considered here, in other words, without increasing iUnemployed variable.
	*/
	int iTurn = GC.getGameINLINE().getGameTurn();
	int eIndex = iTurn%NUM_HIST_TURN;
	int iValue = 0, iChomage;
	int iNumPopulation = 0;
	int iUnemployed = 0;

	int iLoop;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		if (pLoopUnit->getUnitInfo().canBeUnemployed())
		{
			if (iTurn - pLoopUnit->getGameTurnCreated() > 6)
			{
				iNumPopulation++;

				if (pLoopUnit->getProfession() == PROFESSION_COLONIST && pLoopUnit->getUnitInfo().canBeUnemployed())
				{
					iUnemployed++;
				}
			}
		}
	}

	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		iNumPopulation += pLoopCity->getPopulation() - 1;
	}

	for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
	{
		CvUnit* pLoopUnit = m_aEuropeUnits[i];

		if (pLoopUnit->getUnitInfo().canBeUnemployed())
		{
			if (iTurn - pLoopUnit->getGameTurnCreated() > 6)
			{
				iNumPopulation++;
				if (pLoopUnit->getProfession() == PROFESSION_COLONIST && pLoopUnit->getUnitInfo().canBeUnemployed())
				{
					iUnemployed++;
				}
			}
		}
	}

	iChomage = iNumPopulation > 0 ? 100 * iUnemployed / iNumPopulation : 0;
	iValue = iChomage < 10 ? -80 * iChomage / 100 : -8;

	setUnemploymentHistory(eIndex, iValue);
}
void CvPlayer::refreshFoodStatut()
{
	/*TODO This should include food availability in the city and therefore how easy is to obtain food in adjacent plots for ex.
	There are similar checks done by the AI in other files, those functions could be used instead.
	*/
	int iTurn = GC.getGameINLINE().getGameTurn();
	int eIndex = iTurn%NUM_HIST_TURN;
	int iValue = 0, iFood;
	int iNumCities = getNumCities();
	if (iNumCities > 0)
	{
		int iLoop;
		for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
		{
			iFood = pLoopCity->getFood();
			iValue += range(iFood, 0, 50)*4/50 -2;//Je cherche tjrs de la mettre dans [-2; 2]
		}
		iValue /= iNumCities;
	}
	setFoodHistory(eIndex, iValue);
}
void CvPlayer::refreshEconomicStatut()
{
	/*TODO Same than Food Hustory... here is not considered how the Colonial Economy is working at all but only a fixed value, gold.
	Richer nations are not necessarily those with more gold, but those which are able to produce more gold, yields, etc.
	This should include production, yields, percentage of units bought and sold, etc.
	There are similar checks done by the AI in other files, those functions could be used instead.
	*/
	int iTurn = GC.getGameINLINE().getGameTurn();
	int eIndex = iTurn % NUM_HIST_TURN;
	int iValue = 0, iCoeff = 1;
	int iGold = getGold();
	for (int iI = 0; iI < MAX_PLAYERS; iI++) 
	{
		if (GET_PLAYER((PlayerTypes)iI).isAlive()) 
		{
			if (atWar(getTeam(), GET_PLAYER((PlayerTypes)iI).getTeam())) 
			{
				iCoeff += 1;
			}
		}
	}

	iValue = range(iGold, 0, 5000*iCoeff) * 4 / (5000*iCoeff) - 2;//Je mets l'or comme une var compris entre [-2; 2]
	if (getEuropeLoan() > iGold) 
	{
		iValue -= 1;
	}

	iValue = range(iValue, -2, 2);

	setEconomicHistory(eIndex, iValue);
}
void CvPlayer::refreshReligionStatut()
{
	int iTurn = GC.getGameINLINE().getGameTurn();
	int eIndex = iTurn%NUM_HIST_TURN;
	int iValue = 0;
	int iNumCities = getNumCities();
	if (iNumCities > 0)
	{
		int iLoop;
		for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop)) 
		{
			for (int i = 0; i < pLoopCity->getPopulation(); ++i)
			{
				CvUnit* pUnit = pLoopCity->getPopulationUnitByIndex(i);
				if (pUnit != NULL) 
				{
					ProfessionTypes eProfession = pUnit->getProfession();
					if (eProfession != NO_PROFESSION) 
					{
						CvProfessionInfo& kProfessionInfo = GC.getProfessionInfo(eProfession);
						YieldTypes eYieldProduced = (YieldTypes) kProfessionInfo.getYieldProduced();
						if (eYieldProduced == YIELD_CROSSES) 
						{
							iValue += 3;
							if (GC.getUnitInfo(pUnit->getUnitType()).getYieldModifier(YIELD_CROSSES) > 0) 
							{
								iValue += 1;//Si c'est un curé professionnel on donne un bonus
							}
						}
					}
				}
			}
			iValue -= 2;
		}
		iValue /= iNumCities;//On fait la moyenne entre les villes
	}
	iValue = range(iValue, -2, 2);

	setReligionHistory(eIndex, iValue);
}

const CvWString CvPlayer::weaknessAttraction()
{
	CvWString szBuffer;

	int iAverage = 0;
	int iTotal = getTotalImmigrant();
	int iAccept = getAcceptImmigrant();
	int iRefuse = getLeaveImmigrant();
	int iValueAcceptation = range(iTotal > 0 ? 4*iAccept/iTotal - 2: 0, -2, 2);
	int iValueLeave = range(iAccept - 3*iRefuse, -2, 2);
	int iBadValue = 0;
	int iTemp;
	szBuffer = gDLL->getText("TXT_KEY_MAIN_MENU_NONE");

	iTemp = getAverageEconomicHistory();
	if (iTemp < iBadValue)
	{
		szBuffer = gDLL->getText("TXT_KEY_CONCEPT_ECONOMY");
		iBadValue = iTemp;
	}

	iTemp = getAverageFoodHistory();
	if (iTemp < iBadValue)
	{
		szBuffer = gDLL->getText("TXT_KEY_CONCEPT_FOOD");
		iBadValue = iTemp;
	}

	iTemp = getAverageUnemploymentHistory();
	if (iTemp < iBadValue)
	{
		szBuffer = gDLL->getText("TXT_KEY_CONCEPT_WORK");
		iBadValue = iTemp;
	}

	iTemp = getAverageReligionHistory();
	if (iTemp < iBadValue)
	{
		szBuffer = gDLL->getText("TXT_KEY_CONCEPT_RELIGION");
		iBadValue = iTemp;
	}

	iTemp = iValueLeave + iValueAcceptation;
	if (iTemp < iBadValue)
	{
		szBuffer = gDLL->getText("TXT_KEY_CONCEPT_REPUTATION");
		iBadValue = iTemp;
	}

	return szBuffer;
}
const CvWString CvPlayer::strongPointAttraction()
{
	CvWString szBuffer;

	int iAverage = 0;
	int iTotal = getTotalImmigrant();
	int iAccept = getAcceptImmigrant();
	int iRefuse = getLeaveImmigrant();
	int iValueAcceptation = range(iTotal > 0 ? 4*iAccept/iTotal - 2: 0, -2, 2);
	int iValueLeave = range(iAccept - 3*iRefuse, -2, 2);
	int iGoodValue = 0;
	int iTemp;
	szBuffer = gDLL->getText("TXT_KEY_MAIN_MENU_NONE");
	iTemp = getAverageEconomicHistory();
	if (iTemp > iGoodValue)
	{
		szBuffer = gDLL->getText("TXT_KEY_CONCEPT_ECONOMY");
		iGoodValue = iTemp;
	}

	iTemp = getAverageFoodHistory();
	if (iTemp > iGoodValue)
	{
		szBuffer = gDLL->getText("TXT_KEY_CONCEPT_FOOD");
		iGoodValue = iTemp;
	}

	iTemp = getAverageReligionHistory();
	if (iTemp > iGoodValue)
	{
		szBuffer = gDLL->getText("TXT_KEY_CONCEPT_RELIGION");
		iGoodValue = iTemp;
	}

	iTemp = iValueLeave + iValueAcceptation;
	if (iTemp > iGoodValue)
	{
		szBuffer = gDLL->getText("TXT_KEY_CONCEPT_REPUTATION");
		iGoodValue = iTemp;
	}

	return szBuffer;
}
const CvWString CvPlayer::generalAttraction()
{
	int iPercent = getGlobalAverageImmigrationPercent();

	if (iPercent >= 80)
	{
		return gDLL->getText("TXT_KEY_NOTE_EVALUATION_VERY_GOOD");
	}

	if (iPercent >= 60)
	{
		return gDLL->getText("TXT_KEY_NOTE_EVALUATION_GOOD");
	}

	if (iPercent >= 40)
	{
		return gDLL->getText("TXT_KEY_NOTE_EVALUATION_AVERAGE");
	}

	if (iPercent >= 20)
	{
		return gDLL->getText("TXT_KEY_NOTE_EVALUATION_LOW");
	}

	if (iPercent >= 0)
	{
		return gDLL->getText("TXT_KEY_NOTE_EVALUATION_VERY_LOW");
	}

	// Should never go here
	return gDLL->getText("TXT_KEY_MAIN_MENU_ERROR");
}

bool CvPlayer::mustShowIconInfo(int iScreen)
{
	if (!isHuman() || getParent() == NO_PLAYER)
	{
		return false;
	}
	int iLoop;
	int iGameTurn = GC.getGameINLINE().getGameTurn();

	switch(iScreen)
	{
	case 0:
		for (CvUnit* pUnit = firstUnit(&iLoop); pUnit != NULL; pUnit = nextUnit(&iLoop)) 
		{
			if (pUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE && !pUnit->isInEuropeDrydock() && pUnit->getShipSellPrice() == 0) 
			{
				if (!pUnit->isAutomated()) 
				{
					return true;
				}
			}
		}
		break;
	case 1:
		for (CvTempUnit* pTempUnit = firstTempUnit(&iLoop); pTempUnit != NULL; pTempUnit = nextTempUnit(&iLoop))
		{
			if (pTempUnit->getProfession() == PROFESSION_SAILOR && !pTempUnit->requestTraining())
			{
				return true;
			}
		}
		for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
		{
			if (pLoopUnit->isHasPurchaseBid())
			{
				return true;
			}
		}
		break;
	case 2:
		for (CvUnit* pUnit = firstUnit(&iLoop); pUnit != NULL; pUnit = nextUnit(&iLoop))
		{
			if (pUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE && pUnit->getOnlyNewCargo() > 0 && !pUnit->isAutomated())
			{
				return true;
			}
		}
		break;
	case 3:
		for (CvTempUnit* pTempUnit = firstTempUnit(&iLoop); pTempUnit != NULL; pTempUnit = nextTempUnit(&iLoop))
		{
			if (!pTempUnit->getUnitInfo().isMechUnit() && pTempUnit->getProfession() != PROFESSION_SAILOR)
			{
				return true;
			}
		}
		for (int i = 0; i < getNumEuropeUnits(); ++i)
		{
			CvUnit* pUnit = getEuropeUnit(i);
			if (pUnit != NULL && pUnit->isLikelyToLeave() && pUnit->getDestinationCity() == -1) 
			{
				if (iGameTurn - pUnit->getGameTurnCreated() > 2) 
				{
					return true;
				}
			}
		}
		break;
	}
	return false;
}

bool CvPlayer::highlightNewErasButton()
{
	return m_bHighlightNewErasButton;
}

void CvPlayer::setHighlightNewErasButton(bool bNewValue)
{
	if (highlightNewErasButton() != bNewValue)
	{
		m_bHighlightNewErasButton = bNewValue;
	}
}

//DOANE generateUsedShipInEurope
void CvPlayer::generateUsedShipInEurope()
{
	if (isInRevolution() || !isEuropean()) 
	{
		return;
	}

	int iVarDeleteUnit = GC.getGameINLINE().getSorenRandNum(100, "randomiVarDeleteUnit");
	int iTurn = GC.getGameINLINE().getGameTurn();
	int iNumUsedShip = 0;
	int iLoop;
	for (CvTempUnit* pTempUnit = firstTempUnit(&iLoop); pTempUnit != NULL; pTempUnit = nextTempUnit(&iLoop))
	{
		if (pTempUnit->getUnitInfo().isMechUnit()) 
		{
			iNumUsedShip++;
			if (iVarDeleteUnit > 50 - (iTurn-pTempUnit->getGameTurnCreated())*25) 
			{
				removeTempUnit(pTempUnit->getID());
				iNumUsedShip--;
			}
		}
	}

	int iGold = getGold();
	UnitTypes eUnit = NO_UNIT;

	CvPlot* pStartingPlot = getStartingPlot();
	if (pStartingPlot == NULL) 
	{
		return;
	}

	for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass) 
	{
		UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(iUnitClass);
		if (canBuyEuropeNewShip(eUnit)) 
		{
			CvUnitInfo& kUnit = GC.getUnitInfo(eUnit);
			if (kUnit.isOnlyDefensive() && kUnit.getCargoSpace() > 0)
			{
				int iPrice = getEuropeUnitBuyPrice(eUnit);
				if (iPrice*3/4 < iGold) 
				{
					// Player should have at least enough money if we propose this kind of ship 
					int iProba = 100/(kUnit.getCargoSpace() + 1);
					int iGenerateUnitProba = GC.getGameINLINE().getSorenRandNum(100, "generate used ships creation probability");

					CvTempUnit* pTempUnit = NULL;
					if (iGenerateUnitProba < iProba - iNumUsedShip*3) 
					{
						pTempUnit = addTempUnit(eUnit);
						if (pTempUnit != NULL)
						{
							int iPercentDamage = GC.getGameINLINE().getSorenRandNum(95, "generate damage percent for used ships");
							pTempUnit->setDamage(iPercentDamage);
							pTempUnit->generatePrice();
						}
					}
				}
			}
		}
	}
}

bool CvPlayer::canBuyEuropeNewShip(UnitTypes eUnit) const
{
	if (eUnit == NO_UNIT)
	{
		return false;
	}

	CvUnitInfo& kUnit = GC.getUnitInfo(eUnit);
	if (!kUnit.isMechUnit())
	{
		return false;
	}

	int iNewEra = getNewEra();
	if (iNewEra < kUnit.getAllowEra())
	{
		return false;
	}

	if (getEuropeUnitBuyPrice(eUnit) <= 0)
	{
		return false;
	}
	return true;
}
//END DOANE Europe Buy Ship

void CvPlayer::verifyCivics()
{
	for (int iI = 0; iI < GC.getNumCivicOptionInfos(); iI++)
	{
		if (!canDoCivics(getCivic((CivicOptionTypes)iI)))
		{
			for (int iJ = 0; iJ < GC.getNumCivicInfos(); iJ++)
			{
				if (GC.getCivicInfo((CivicTypes)iJ).getCivicOptionType() == iI)
				{
					if (canDoCivics((CivicTypes)iJ))
					{
						setCivic(((CivicOptionTypes)iI), ((CivicTypes)iJ));
						break;
					}
				}
			}
		}
	}
}

void CvPlayer::updateYield()
{
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		pLoopCity->updateYield();
	}
}

void CvPlayer::updateCityPlotYield()
{
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		pLoopCity->plot()->updateYield(true);
	}
}


void CvPlayer::updateCitySight(bool bIncrement)
{
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		pLoopCity->plot()->updateSight(bIncrement);
	}
}

void CvPlayer::updateTimers()
{
	CvSelectionGroup* pLoopSelectionGroup;
	int iLoop;

	for (pLoopSelectionGroup = firstSelectionGroup(&iLoop); pLoopSelectionGroup; pLoopSelectionGroup = nextSelectionGroup(&iLoop))
	{
		pLoopSelectionGroup->updateTimers(); // could destroy the selection group...
	}

	// if a unit was busy, perhaps it was not quite deleted yet, give it one more try
	if (getNumSelectionGroups() > getNumUnits())
	{
		for (pLoopSelectionGroup = firstSelectionGroup(&iLoop); pLoopSelectionGroup; pLoopSelectionGroup = nextSelectionGroup(&iLoop))
		{
			pLoopSelectionGroup->doDelayedDeath(); // could destroy the selection group...
		}
	}

	FAssertMsg(getNumSelectionGroups() <= getNumUnits(), "The number of Units is expected not to exceed the number of Selection Groups");
}


bool CvPlayer::hasReadyUnit(bool bAny) const
{
	PROFILE_FUNC();

	CvSelectionGroup* pLoopSelectionGroup;
	int iLoop;

	for (pLoopSelectionGroup = firstSelectionGroup(&iLoop); pLoopSelectionGroup; pLoopSelectionGroup = nextSelectionGroup(&iLoop))
	{
		if (pLoopSelectionGroup->readyToMove(bAny))
		{
			return true;
		}
	}

	return false;
}


bool CvPlayer::hasAutoUnit() const
{
	PROFILE_FUNC();

	CvSelectionGroup* pLoopSelectionGroup;
	int iLoop;

	for (pLoopSelectionGroup = firstSelectionGroup(&iLoop); pLoopSelectionGroup; pLoopSelectionGroup = nextSelectionGroup(&iLoop))
	{
		if (pLoopSelectionGroup->readyToAuto())
		{
			return true;
		}
	}

	return false;
}


bool CvPlayer::hasBusyUnit() const
{
	PROFILE_FUNC();

	CvSelectionGroup* pLoopSelectionGroup;
	int iLoop;

	for (pLoopSelectionGroup = firstSelectionGroup(&iLoop); pLoopSelectionGroup; pLoopSelectionGroup = nextSelectionGroup(&iLoop))
	{
		if (pLoopSelectionGroup->isBusy())
		{
			if (pLoopSelectionGroup->getNumUnits() == 0)
			{
				pLoopSelectionGroup->kill();
				return false;
			}

			return true;
		}
	}

	return false;
}


int CvPlayer::calculateScore(bool bFinal, bool bVictory) const
{
	PROFILE_FUNC();

	if (!isAlive())
	{
		return 0;
	}

	if (GET_TEAM(getTeam()).getNumMembers() == 0)
	{
		return 0;
	}

	if (isNative())
	{
		return 0;
	}
	if (isEurope())
	{
		return 0;
	}
	int iTurn = GC.getGameINLINE().getGameTurn();
	if (iTurn == 0)
	{
		return 3;
	}
	int iScore = getEconomyHistory(iTurn - 1);

	return iScore;
}

int CvPlayer::getScoreTaxFactor() const
{
	return std::max(0, 100 - GC.getDefineINT("SCORE_TAX_FACTOR") * getTaxRate() / 100);
}

int CvPlayer::findBestFoundValue() const
{
	CvArea* pLoopArea;
	int iValue;
	int iBestValue;
	int iLoop;

	iBestValue = 0;

	for (pLoopArea = GC.getMapINLINE().firstArea(&iLoop); pLoopArea != NULL; pLoopArea = GC.getMapINLINE().nextArea(&iLoop))
	{
		iValue = pLoopArea->getBestFoundValue(getID());

		if (iValue > iBestValue)
		{
			iBestValue = iValue;
		}
	}

	return iBestValue;
}


int CvPlayer::upgradeAllPrice(UnitTypes eUpgradeUnit, UnitTypes eFromUnit)
{
	CvUnit* pLoopUnit;
	int iPrice;
	int iLoop;

	iPrice = 0;

	// Loop through units and determine the total power of this player's military
	for (pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		if (pLoopUnit->getUnitType() == eFromUnit)
		{
			if (pLoopUnit->canUpgrade(eUpgradeUnit, true))
			{
				iPrice += pLoopUnit->upgradePrice(eUpgradeUnit);
			}
		}
	}

	return iPrice;
}


int CvPlayer::countNumCoastalCities() const
{
	CvCity* pLoopCity;
	int iCount;
	int iLoop;

	iCount = 0;

	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		if (pLoopCity->isCoastal(GC.getMIN_WATER_SIZE_FOR_OCEAN()))
		{
			iCount++;
		}
	}

	return iCount;
}


int CvPlayer::countNumCoastalCitiesByArea(CvArea* pArea) const
{
	CvCity* pLoopCity;
	int iCount;
	int iLoop;

	iCount = 0;

	int iAreaID = pArea->getID();
	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		if (pLoopCity->isCoastal(GC.getMIN_WATER_SIZE_FOR_OCEAN()))
		{
			if ((pLoopCity->getArea() == iAreaID) || pLoopCity->plot()->isAdjacentToArea(iAreaID))
			{
				iCount++;
			}
		}
	}

	return iCount;
}


int CvPlayer::countTotalCulture() const
{
	int iLoop;
	int iCount = 0;

	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		iCount += pLoopCity->getCulture(getID());
	}

	return (iCount);
}

int CvPlayer::countTotalYieldStored(YieldTypes eYield) const
{
	int iLoop;
	int iCount = 0;

	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		iCount += pLoopCity->getYieldStored(eYield);
	}

	return (iCount);
}

int CvPlayer::countCityFeatures(FeatureTypes eFeature) const
{
	PROFILE_FUNC();

	CvCity* pLoopCity;
	CvPlot* pLoopPlot;
	int iCount;
	int iLoop;
	int iI;

	iCount = 0;

	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		for (iI = 0; iI < NUM_CITY_PLOTS; iI++)
		{
			pLoopPlot = plotCity(pLoopCity->getX_INLINE(), pLoopCity->getY_INLINE(), iI);

			if (pLoopPlot != NULL)
			{
				if (pLoopPlot->getFeatureType() == eFeature)
				{
					iCount++;
				}
			}
		}
	}

	return iCount;
}


int CvPlayer::countNumBuildings(BuildingTypes eBuilding) const
{
	PROFILE_FUNC();

	CvCity* pLoopCity;
	int iCount;
	int iLoop;

	iCount = 0;

	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		if (pLoopCity->isHasConceptualBuilding(eBuilding))
		{
			++iCount;
		}
	}

	return iCount;
}


bool CvPlayer::canContact(PlayerTypes ePlayer) const
{
	if (ePlayer == getID())
	{
		return false;
	}

	if (!isAlive() || !(GET_PLAYER(ePlayer).isAlive()))
	{
		return false;
	}
	if (getTeam() != GET_PLAYER(ePlayer).getTeam())
	{
		if (!(GET_TEAM(getTeam()).isHasMet(GET_PLAYER(ePlayer).getTeam())))
		{
			return false;
		}

		if (atWar(getTeam(), GET_PLAYER(ePlayer).getTeam()))
		{
			if (!(GET_TEAM(getTeam()).canChangeWarPeace(GET_PLAYER(ePlayer).getTeam())))
			{
				return false;
			}

			if (isHuman() || GET_PLAYER(ePlayer).isHuman())
			{
				if (GC.getGameINLINE().isOption(GAMEOPTION_ALWAYS_WAR))
				{
					return false;
				}
			}

			if (GET_TEAM(getTeam()).isParentOf(GET_PLAYER(ePlayer).getTeam()) || GET_TEAM(GET_PLAYER(ePlayer).getTeam()).isParentOf(getTeam()))
			{
				return false;
			}
		}

		if (GET_TEAM(GET_PLAYER(ePlayer).getTeam()).isParentOf(getTeam()))
		{
			if (getParent() != ePlayer)
			{
				return false;
			}
		}

		if (GET_TEAM(getTeam()).isParentOf(GET_PLAYER(ePlayer).getTeam()))
		{
			if (GET_PLAYER(ePlayer).getParent() != getID())
			{
				return false;
			}
		}

		/*if (isEurope() && GET_PLAYER(ePlayer).getParent() != getID())
		{
		return false;
		}*/

		/*if (GET_PLAYER(ePlayer).isEurope() && getParent() != ePlayer)
		{
		return false;
		}*/
	}

	return true;
}

void CvPlayer::contact(PlayerTypes ePlayer)
{
	CvDiploParameters* pDiplo;

	if (!canContact(ePlayer) || isTurnDone())
	{
		return;
	}

	if (GET_PLAYER(ePlayer).isHuman())
	{
		if (GC.getGameINLINE().isPbem() || GC.getGameINLINE().isHotSeat() || (GC.getGameINLINE().isPitboss() && !gDLL->isConnected(GET_PLAYER(ePlayer).getNetID())))
		{
			if (gDLL->isMPDiplomacy())
			{
				gDLL->beginMPDiplomacy(ePlayer, false, false);
			}
		}
		else
		{
			if (gDLL->getInterfaceIFace()->isFlashing(ePlayer))
			{
				if (!gDLL->getInterfaceIFace()->isDiplomacyLocked())
				{
					gDLL->getInterfaceIFace()->setDiplomacyLocked(true);
					gDLL->sendContactCiv(NETCONTACT_RESPONSE, ePlayer, gDLL->getInterfaceIFace()->getDiplomacyTransportId(ePlayer));
				}
			}
			else
			{
				gDLL->sendContactCiv(NETCONTACT_INITIAL, ePlayer, -1);
			}
		}
	}
	else
	{
		pDiplo = new CvDiploParameters(ePlayer);
		FAssert(pDiplo != NULL);
		if (gDLL->ctrlKey() && getParent() != ePlayer)
		{
			pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_TRADING"));
		}
		else if (getParent() != ePlayer && GET_PLAYER(ePlayer).isEurope())
		{
			if (!atWar(getTeam(), GET_PLAYER(ePlayer).getTeam()))
			{
				pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_HELP_EUROPEAN_FRIEND"));
				pDiplo->addDiploCommentVariable(getCivilizationShortDescription());
				pDiplo->addDiploCommentVariable(getCivilizationAdjective());
				pDiplo->addDiploCommentVariable(GET_PLAYER(ePlayer).getCivilizationAdjective());
			}
		}
		gDLL->beginDiplomacy(pDiplo, getID());
	}
}



void CvPlayer::handleDiploEvent(DiploEventTypes eDiploEvent, PlayerTypes ePlayer, int iData1, int iData2)
{
	CvCity* pCity;
	int iI;

	//FAssertMsg(ePlayer != getID(), "shouldn't call this function on ourselves");
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	switch (eDiploEvent)
	{
	case DIPLOEVENT_CONTACT:
		AI_setFirstContact(ePlayer, true);
		kPlayer.AI_setFirstContact(getID(), true);
		break;

	case DIPLOEVENT_AI_CONTACT:
		break;

	case DIPLOEVENT_FAILED_CONTACT:
		AI_setFirstContact(ePlayer, true);
		kPlayer.AI_setFirstContact(getID(), true);
		break;

	case DIPLOEVENT_GIVE_HELP:
		AI_changeMemoryCount(ePlayer, MEMORY_GIVE_HELP, 1);
		forcePeace(ePlayer);
		break;

	case DIPLOEVENT_REFUSED_HELP:
		AI_changeMemoryCount(ePlayer, MEMORY_REFUSED_HELP, 1);
		break;

	case DIPLOEVENT_ACCEPT_DEMAND:
		AI_changeMemoryCount(ePlayer, MEMORY_ACCEPT_DEMAND, 1);
		forcePeace(ePlayer);
		break;

	case DIPLOEVENT_REJECTED_DEMAND:
		FAssertMsg(kPlayer.getTeam() != getTeam(), "shouldn't call this function on our own team");

		AI_changeMemoryCount(ePlayer, MEMORY_REJECTED_DEMAND, 1);

		if (AI_demandRebukedSneak(ePlayer))
		{
			GET_TEAM(getTeam()).AI_setWarPlan(kPlayer.getTeam(), WARPLAN_PREPARING_LIMITED);
		}
		break;

	case DIPLOEVENT_DEMAND_WAR:
		FAssertMsg(kPlayer.getTeam() != getTeam(), "shouldn't call this function on our own team");

		GET_TEAM(getTeam()).declareWar(kPlayer.getTeam(), false, WARPLAN_LIMITED);
		break;

	case DIPLOEVENT_JOIN_WAR:
		AI_changeMemoryCount(ePlayer, MEMORY_ACCEPTED_JOIN_WAR, 1);
		GET_TEAM(kPlayer.getTeam()).declareWar(((TeamTypes)iData1), false, WARPLAN_DOGPILE);

		for (iI = 0; iI < MAX_PLAYERS; iI++)
		{
			if (GET_PLAYER((PlayerTypes)iI).isAlive())
			{
				if (GET_PLAYER((PlayerTypes)iI).getTeam() == ((TeamTypes)iData1))
				{
					GET_PLAYER((PlayerTypes)iI).AI_changeMemoryCount(getID(), MEMORY_HIRED_WAR_ALLY, 1);
				}
			}
		}
		break;

	case DIPLOEVENT_NO_JOIN_WAR:
		AI_changeMemoryCount(ePlayer, MEMORY_DENIED_JOIN_WAR, 1);
		break;

	case DIPLOEVENT_STOP_TRADING:
		AI_changeMemoryCount(ePlayer, MEMORY_ACCEPTED_STOP_TRADING, 1);
		kPlayer.stopTradingWithTeam((TeamTypes)iData1);

		for (iI = 0; iI < MAX_PLAYERS; iI++)
		{
			if (GET_PLAYER((PlayerTypes)iI).isAlive())
			{
				if (GET_PLAYER((PlayerTypes)iI).getTeam() == ((TeamTypes)iData1))
				{
					GET_PLAYER((PlayerTypes)iI).AI_changeMemoryCount(getID(), MEMORY_HIRED_TRADE_EMBARGO, 1);
				}
			}
		}
		break;

	case DIPLOEVENT_NO_STOP_TRADING:
		AI_changeMemoryCount(ePlayer, MEMORY_DENIED_STOP_TRADING, 1);
		break;

	case DIPLOEVENT_ASK_HELP:
		AI_changeMemoryCount(ePlayer, MEMORY_MADE_DEMAND_RECENT, 1);
		break;

	case DIPLOEVENT_MADE_DEMAND:
		if (AI_getMemoryCount(ePlayer, MEMORY_MADE_DEMAND) < 10)
		{
			AI_changeMemoryCount(ePlayer, MEMORY_MADE_DEMAND, 1);
		}
		AI_changeMemoryCount(ePlayer, MEMORY_MADE_DEMAND_RECENT, 1);
		break;

	case DIPLOEVENT_ACCEPT_TAX_RATE:
		kPlayer.changeTaxRate(iData1);
		if (AI_getMemoryCount(ePlayer, MEMORY_REFUSED_TAX) - 1 >= 0)
		{
			kPlayer.changeParentMood(1);
			AI_changeMemoryCount(ePlayer, MEMORY_REFUSED_TAX, -1);
		}
		break;

	case DIPLOEVENT_REFUSE_TAX_RATE:
		{
			AI_changeMemoryCount(ePlayer, MEMORY_REFUSED_TAX, 3);
			kPlayer.changeParentMood(-3);
			YieldTypes eYield = (YieldTypes) iData1;
			kPlayer.setYieldEuropeTradable(eYield, false);
			kPlayer.setYieldTradedTotal(eYield, 0);
			CvCity* pCity = kPlayer.getCity(iData2);
			if (pCity != NULL)
			{
				pCity->setYieldStored(eYield, 0);
				CvWString szMessage = gDLL->getText("TXT_KEY_BOSTON_TEA_PARTY", kPlayer.getCivilizationAdjectiveKey(), pCity->getNameKey(), GC.getYieldInfo(eYield).getTextKeyWide());
				gDLL->getInterfaceIFace()->addMessage(ePlayer, true, GC.getEVENT_MESSAGE_TIME(), szMessage, "AS2D_CITY_REVOLT", MESSAGE_TYPE_MAJOR_EVENT, ARTFILEMGR.getInterfaceArtInfo("WORLDBUILDER_CITY_EDIT")->getPath(), (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"), pCity->getX_INLINE(), pCity->getY_INLINE(), true, true);
			}
		}
		break;

	case DIPLOEVENT_ACCEPT_KING_GOLD:
		kPlayer.changeGold(-iData1);
		kPlayer.doREFReduction(iData1);
		if (AI_getMemoryCount(ePlayer, MEMORY_REFUSED_TAX) - 1 >= 0)
		{
			kPlayer.changeParentMood(1);
			AI_changeMemoryCount(ePlayer, MEMORY_REFUSED_TAX, -1);
		}
		break;

	case DIPLOEVENT_REFUSE_KING_GOLD:
		{
			AI_changeMemoryCount(ePlayer, MEMORY_REFUSED_TAX, 3);
			kPlayer.changeParentMood(-3);
		}
		break;

	case DIPLOEVENT_LIVE_AMONG_NATIVES:
		{
			CvUnit* pUnit = kPlayer.getUnit(iData1);
			if (pUnit != NULL)
			{
				pUnit->doLiveAmongNatives();
			}
		}
		break;

	case DIPLOEVENT_TRANSPORT_TREASURE:
		{
			CvUnit* pUnit = kPlayer.getUnit(iData1);
			if (pUnit != NULL)
			{
				pUnit->doKingTransport();
			}
		}
		break;

	case DIPLOEVENT_FOUND_CITY:
		{
			CvUnit* pUnit = kPlayer.getUnit(iData1);
			if (pUnit != NULL)
			{
				pUnit->doFound(iData2);
			}
		}
		break;

	case DIPLOEVENT_FOUND_CITY_CHECK_NATIVES:
		{
			CvUnit* pUnit = kPlayer.getUnit(iData1);
			if (pUnit != NULL)
			{
				pUnit->doFoundCheckNatives();
			}
		}
		break;

	case DIPLOEVENT_ACCEPT_UNIT_SHIP:
		{
			kPlayer.europeGiveShip(iData1);
		}
		break;

	case DIPLOEVENT_ACCEPT_UNIT_SHIP_NO_ATTACK:
		{
			kPlayer.europeGiveShip(iData1);
		}
		break;

	case DIPLOEVENT_REPAY_LOAN:
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_REPAY_LOAN);
			gDLL->getInterfaceIFace()->addPopup(pInfo, kPlayer.getID(), true, true);
		}
		break;
	case DIPLOEVENT_KING_GIFT_GALEON_SHIP:
		{
			kPlayer.europeGiveGalleon();
		}
		break;
	case DIPLOEVENT_DISPLAY_SEEDLING_DISCOVERY:
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SEEDLING_DISCOVERY, iData1);
			gDLL->getInterfaceIFace()->addPopup(pInfo, kPlayer.getID());
		}
		break;
	case DIPLOEVENT_TARGET_CITY:
		pCity = GET_PLAYER((PlayerTypes)iData1).getCity(iData2);
		if (pCity != NULL)
		{
			pCity->area()->setTargetCity(getID(), pCity);
		}
		break;

	default:
		FAssert(false);
		break;
	}
}


bool CvPlayer::canTradeWith(PlayerTypes eWhoTo) const
{
	if (getParent() != eWhoTo)
	{
		CvPlayer& kWhoTo = GET_PLAYER(eWhoTo);

		if (atWar(getTeam(), kWhoTo.getTeam()))
		{
			return true;
		}

		if (GET_TEAM(getTeam()).isGoldTrading() || GET_TEAM(kWhoTo.getTeam()).isGoldTrading())
		{
			return true;
		}

		if (GET_TEAM(getTeam()).isMapTrading() || GET_TEAM(kWhoTo.getTeam()).isMapTrading())
		{
			return true;
		}

		if (GET_TEAM(getTeam()).isOpenBordersTrading() || GET_TEAM(kWhoTo.getTeam()).isOpenBordersTrading())
		{
			return true;
		}

		if (GET_TEAM(getTeam()).isDefensivePactTrading() || GET_TEAM(kWhoTo.getTeam()).isDefensivePactTrading())
		{
			return true;
		}

		if (GET_TEAM(getTeam()).isPermanentAllianceTrading() || GET_TEAM(kWhoTo.getTeam()).isPermanentAllianceTrading())
		{
			return true;
		}
	}

	return false;
}

bool CvPlayer::canReceiveTradeCity(PlayerTypes eFromPlayer) const
{
	if (GC.getGameINLINE().isOption(GAMEOPTION_ONE_CITY_CHALLENGE) && isHuman())
	{
		return false;
	}

	if (isNative() && !GET_PLAYER(eFromPlayer).isNative())
	{
		return false;
	}

	return true;
}

bool CvPlayer::canTradeItem(PlayerTypes eWhoTo, TradeData item, bool bTestDenial) const
{
	CvPlayerAI& kWhoTo = GET_PLAYER(eWhoTo);

	if (isEurope() || kWhoTo.isEurope())
	{
		return false;
	}

	if (bTestDenial)
	{
		if (getTradeDenial(eWhoTo, item) != NO_DENIAL)
		{
			return false;
		}
	}

	CvTeam& kTeam = GET_TEAM(getTeam());
	CvTeam& kWhoToTeam = GET_TEAM(kWhoTo.getTeam());

	switch (item.m_eItemType)
	{
	case TRADE_CITIES:
		{
			CvCity* pCityTraded = getCity(item.m_iData1);
			if (NULL != pCityTraded)
			{
				if (pCityTraded->canTradeAway(eWhoTo))
				{
					return true;
				}
			}
		}
		break;

	case TRADE_GOLD:
		if (kTeam.isGoldTrading() || kWhoToTeam.isGoldTrading())
		{
			if (!isNative() && !kWhoTo.isNative())
			{
				int iGold = getMaxGoldTrade(eWhoTo, item.m_kTransport);
				if (iGold > 0 && iGold >= item.m_iData1)
				{
					return true;
				}
			}
		}
		break;

	case TRADE_YIELD:
		if ((getParent() == NO_PLAYER || kWhoTo.getParent() == NO_PLAYER) && !isNative())
		{
			if (getTeam() != kWhoTo.getTeam())
			{
				YieldTypes eYield = (YieldTypes) item.m_iData1;
				if (kWhoTo.AI_getYieldTradeValue(eYield, kWhoTo.getID())) 
				{
					return true;
				}
			}
		}
		break;

	case TRADE_MAPS:
		/*if (!isNative() && !kWhoTo.isNative())
		{
		if (getTeam() != kWhoTo.getTeam())
		{
		if (GET_TEAM(getTeam()).isMapTrading() || GET_TEAM(kWhoTo.getTeam()).isMapTrading())
		{
		return true;
		}
		}
		}*/
		break;

	case TRADE_PEACE:
		if (!kTeam.isHuman())
		{
			if (kTeam.isHasMet((TeamTypes)(item.m_iData1)) && kWhoToTeam.isHasMet((TeamTypes)(item.m_iData1)))
			{
				if (atWar(getTeam(), ((TeamTypes)(item.m_iData1))))
				{
					return true;
				}
			}
		}
		break;

	case TRADE_WAR:
		if (!kTeam.isHuman())
		{
			CvTeam& kTeamToAttack = GET_TEAM((TeamTypes) item.m_iData1);
			if (kTeam.isHasMet(kTeamToAttack.getID()) && kWhoToTeam.isHasMet(kTeamToAttack.getID()))
			{
				if (kTeam.canDeclareWar(kTeamToAttack.getID()))
				{
					return true;
				}
			}
		}
		break;

	case TRADE_EMBARGO:
		if (!kTeam.isHuman())
		{
			if (kTeam.isHasMet((TeamTypes)(item.m_iData1)) && kWhoToTeam.isHasMet((TeamTypes)(item.m_iData1)))
			{
				if (canStopTradingWithTeam((TeamTypes)(item.m_iData1)))
				{
					return true;
				}
			}
		}
		break;

	case TRADE_OPEN_BORDERS:
		if (getTeam() != kWhoTo.getTeam())
		{
			if (!atWar(getTeam(), kWhoTo.getTeam()))
			{
				if (!kTeam.isOpenBorders(kWhoTo.getTeam()) && !isAlwaysOpenBorders() && !kWhoTo.isAlwaysOpenBorders())
				{
					if (kTeam.isOpenBordersTrading() || kWhoToTeam.isOpenBordersTrading())
					{
						return true;
					}
				}
			}
		}
		break;

	case TRADE_SHARE_VISION:
		if (getTeam() != kWhoTo.getTeam())
		{
			if (!atWar(getTeam(), kWhoTo.getTeam()))
			{
				if (!kTeam.isSharingVision(kWhoTo.getTeam()))
				{
					if (kWhoTo.isHuman() && kTeam.isHuman())
					{
						return true;
					}

					if (kTeam.AI_shareWar(kWhoTo.getTeam()))
					{
						return true;
					}
				}
			}
		}
		break;

	case TRADE_DEFENSIVE_PACT:
		if (getParent() == NO_PLAYER || kWhoTo.getParent() == NO_PLAYER)
		{
			if (getTeam() != kWhoTo.getTeam())
			{
				if (!atWar(getTeam(), kWhoTo.getTeam()))
				{
					if (!kTeam.isDefensivePact(kWhoTo.getTeam()))
					{
						if (kTeam.isDefensivePactTrading() || kWhoToTeam.isDefensivePactTrading())
						{
							if (kTeam.getAtWarCount() == 0 && kWhoToTeam.getAtWarCount() == 0)
							{
								if (kTeam.canSignDefensivePact(kWhoTo.getTeam()))
								{
									return true;
								}
							}
						}
					}
				}
			}
		}
		break;

	case TRADE_PERMANENT_ALLIANCE:
		if (getTeam() != kWhoTo.getTeam())
		{
			if (!atWar(getTeam(), kWhoTo.getTeam()))
			{
				if (kTeam.isPermanentAllianceTrading() || kWhoToTeam.isPermanentAllianceTrading())
				{
					if (kTeam.getNumMembers() == 1 && kWhoToTeam.getNumMembers() == 1)
					{
						return true;
					}
				}
			}
		}
		break;

	case TRADE_PEACE_TREATY:
		return true;
		break;
	}

	return false;
}


DenialTypes CvPlayer::getTradeDenial(PlayerTypes eWhoTo, TradeData item) const
{
	CvCity* pCity;
	CvPlayer& kWhoTo = GET_PLAYER(eWhoTo);

	switch (item.m_eItemType)
	{
	case TRADE_CITIES:
		pCity = getCity(item.m_iData1);
		if (pCity != NULL)
		{
			return AI_cityTrade(pCity, eWhoTo);
		}
		break;

	case TRADE_GOLD:
		break;

	case TRADE_YIELD:
		return getTradeYieldAmount((YieldTypes) item.m_iData1) > 0 ? NO_DENIAL : DENIAL_UNKNOWN;

	case TRADE_MAPS:
		return GET_TEAM(getTeam()).AI_mapTrade(kWhoTo.getTeam());

	case TRADE_PEACE:
		return GET_TEAM(getTeam()).AI_makePeaceTrade(((TeamTypes)(item.m_iData1)), kWhoTo.getTeam());

	case TRADE_WAR:
		return GET_TEAM(getTeam()).AI_declareWarTrade(((TeamTypes)(item.m_iData1)), kWhoTo.getTeam());

	case TRADE_EMBARGO:
		return AI_stopTradingTrade(((TeamTypes)(item.m_iData1)), eWhoTo);

	case TRADE_OPEN_BORDERS:
		return GET_TEAM(getTeam()).AI_openBordersTrade(kWhoTo.getTeam());

	case TRADE_SHARE_VISION:
		return GET_TEAM(getTeam()).AI_sharingVisionTrade(kWhoTo.getTeam());

	case TRADE_DEFENSIVE_PACT:
		return GET_TEAM(getTeam()).AI_defensivePactTrade(kWhoTo.getTeam());

	case TRADE_PERMANENT_ALLIANCE:
		return GET_TEAM(getTeam()).AI_permanentAllianceTrade(kWhoTo.getTeam());

	case TRADE_PEACE_TREATY:
		break;
	}

	return NO_DENIAL;
}

bool CvPlayer::isTradingWithTeam(TeamTypes eTeam, bool bIncludeCancelable) const
{
	int iLoop;

	if (eTeam == getTeam())
	{
		return false;
	}

	for (CvDeal* pLoopDeal = GC.getGameINLINE().firstDeal(&iLoop); pLoopDeal != NULL; pLoopDeal = GC.getGameINLINE().nextDeal(&iLoop))
	{
		if (bIncludeCancelable || pLoopDeal->isCancelable(getID()))
		{
			if (!pLoopDeal->isPeaceDeal())
			{
				if ((pLoopDeal->getFirstPlayer() == getID()) && (GET_PLAYER(pLoopDeal->getSecondPlayer()).getTeam() == eTeam))
				{
					if (pLoopDeal->getLengthFirstTrades() > 0)
					{
						return true;
					}
				}

				if ((pLoopDeal->getSecondPlayer() == getID()) && (GET_PLAYER(pLoopDeal->getFirstPlayer()).getTeam() == eTeam))
				{
					if (pLoopDeal->getLengthSecondTrades() > 0)
					{
						return true;
					}
				}
			}
		}
	}

	return false;
}


bool CvPlayer::canStopTradingWithTeam(TeamTypes eTeam, bool bContinueNotTrading) const
{
	if (eTeam == getTeam())
	{
		return false;
	}

	if (!isTradingWithTeam(eTeam, false))
	{
		if (bContinueNotTrading && !isTradingWithTeam(eTeam, true))
		{
			return true;
		}

		return false;
	}

	return true;
}


void CvPlayer::stopTradingWithTeam(TeamTypes eTeam)
{
	CvDeal* pLoopDeal;
	int iLoop;

	FAssert(eTeam != getTeam());

	for (pLoopDeal = GC.getGameINLINE().firstDeal(&iLoop); pLoopDeal != NULL; pLoopDeal = GC.getGameINLINE().nextDeal(&iLoop))
	{
		if (pLoopDeal->isCancelable(getID()) && !(pLoopDeal->isPeaceDeal()))
		{
			if (((pLoopDeal->getFirstPlayer() == getID()) && (GET_PLAYER(pLoopDeal->getSecondPlayer()).getTeam() == eTeam)) ||
				((pLoopDeal->getSecondPlayer() == getID()) && (GET_PLAYER(pLoopDeal->getFirstPlayer()).getTeam() == eTeam)))
			{
				pLoopDeal->kill(true, getTeam());
			}
		}
	}

	CvTeam& kOtherTeam = GET_TEAM(eTeam);
	for (int iI = 0; iI < kOtherTeam.getPlayerCount(); iI++)
	{
		CvPlayerAI& kPlayer = GET_PLAYER(kOtherTeam.getPlayer(iI));
		if (kPlayer.isAlive())
		{
			kPlayer.AI_changeMemoryCount(getID(), MEMORY_STOPPED_TRADING, 1);
			kPlayer.AI_changeMemoryCount(getID(), MEMORY_STOPPED_TRADING_RECENT, 1);
		}
	}
}


void CvPlayer::killAllDeals()
{
	CvDeal* pLoopDeal;
	int iLoop;

	for (pLoopDeal = GC.getGameINLINE().firstDeal(&iLoop); pLoopDeal != NULL; pLoopDeal = GC.getGameINLINE().nextDeal(&iLoop))
	{
		if ((pLoopDeal->getFirstPlayer() == getID()) || (pLoopDeal->getSecondPlayer() == getID()))
		{
			pLoopDeal->kill(true, getTeam());
		}
	}
}


void CvPlayer::findNewCapital()
{
	CvCity* pOldCapital;
	CvCity* pLoopCity;
	CvCity* pBestCity;
	BuildingTypes eCapitalBuilding;
	int iValue;
	int iBestValue;
	int iLoop;

	BuildingClassTypes eCapitalBuildingClass = (BuildingClassTypes) GC.getDefineINT("CAPITAL_BUILDINGCLASS");
	if (eCapitalBuildingClass == NO_BUILDINGCLASS)
	{
		return;
	}

	eCapitalBuilding = ((BuildingTypes)(GC.getCivilizationInfo(getCivilizationType()).getCivilizationBuildings(eCapitalBuildingClass)));
	if (eCapitalBuilding == NO_BUILDING)
	{
		return;
	}

	pOldCapital = getCapitalCity();

	iBestValue = 0;
	pBestCity = NULL;

	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		if (pLoopCity != pOldCapital)
		{
			if (!pLoopCity->isHasRealBuilding(eCapitalBuilding))
			{
				iValue = (pLoopCity->getPopulation() * 4);

				iValue += pLoopCity->getYieldRate(YIELD_FOOD);
				iValue += pLoopCity->getCultureLevel();

				iValue *= (pLoopCity->calculateCulturePercent(getID()) + 100);
				iValue /= 100;

				if (iValue > iBestValue)
				{
					iBestValue = iValue;
					pBestCity = pLoopCity;
				}
			}
		}
	}

	if (pBestCity != NULL)
	{
		if (pOldCapital != NULL)
		{
			if ( pOldCapital->isHasRealBuilding(eCapitalBuilding) )
			{
				pOldCapital->setHasRealBuilding(eCapitalBuilding, false);
			}
		}
		FAssert(!(pBestCity->isHasRealBuilding(eCapitalBuilding)));
		pBestCity->setHasRealBuilding(eCapitalBuilding, true);
	}
}

bool CvPlayer::canRaze(CvCity* pCity, bool autoRaze) const
{
	if (!pCity->isAutoRaze() && !autoRaze)
	{
		if (GC.getGameINLINE().isOption(GAMEOPTION_NO_CITY_RAZING))
		{
			return false;
		}

		if (pCity->getOwnerINLINE() != getID())
		{
			return false;
		}

		if (pCity->calculateTeamCulturePercent(getTeam()) >= GC.getDefineINT("RAZING_CULTURAL_PERCENT_THRESHOLD"))
		{
			return false;
		}
	}

	CyCity* pyCity = new CyCity(pCity);
	CyArgsList argsList;
	argsList.add(getID());	// Player ID
	argsList.add(gDLL->getPythonIFace()->makePythonObject(pyCity));	// pass in city class
	long lResult=0;
	gDLL->getPythonIFace()->callFunction(PYGameModule, "canRazeCity", argsList.makeFunctionArgs(), &lResult);
	delete pyCity;	// python fxn must not hold on to this pointer
	if (lResult == 0)
	{
		return (false);
	}

	return true;
}


void CvPlayer::raze(CvCity* pCity, bool autoRaze)
{
	if (!canRaze(pCity, autoRaze))
	{
		return;
	}

	FAssert(pCity->getOwnerINLINE() == getID());

	PlayerTypes eHighestCulturePlayer = pCity->findHighestCulture();

	if (eHighestCulturePlayer != NO_PLAYER)
	{
		if (GET_PLAYER(eHighestCulturePlayer).getTeam() != getTeam())
		{
			GET_PLAYER(eHighestCulturePlayer).AI_changeMemoryCount(getID(), MEMORY_RAZED_CITY, 1);
			AI_changeMemoryCount(eHighestCulturePlayer, MEMORY_REVENGE_TAKEN, 1);
			if (isNative())
			{
				GET_TEAM(getTeam()).AI_changeDamages(pCity->getTeam(), -AI_cityTradeVal(pCity, eHighestCulturePlayer));
			}
		}
	}

	CvWString szBuffer = gDLL->getText("TXT_KEY_MISC_DESTROYED_CITY", pCity->getNameKey());
	gDLL->getInterfaceIFace()->addMessage(getID(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_CITYRAZE", MESSAGE_TYPE_MAJOR_EVENT, ARTFILEMGR.getInterfaceArtInfo("WORLDBUILDER_CITY_EDIT")->getPath(), (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pCity->getX_INLINE(), pCity->getY_INLINE(), true, true);

	for (int iI = 0; iI < MAX_PLAYERS; iI++)
	{
		CvPlayer& pLoopPlayer = GET_PLAYER((PlayerTypes)iI);
		if (pLoopPlayer.isAlive())
		{
			if (pLoopPlayer.getID() != getID())
			{
				if (pCity->isRevealed(pLoopPlayer.getTeam(), false))
				{
					szBuffer = gDLL->getText("TXT_KEY_MISC_CITY_HAS_BEEN_RAZED_BY", pCity->getNameKey(), getCivilizationDescriptionKey());
					gDLL->getInterfaceIFace()->addMessage(pLoopPlayer.getID(), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_CITYRAZED", MESSAGE_TYPE_MAJOR_EVENT, ARTFILEMGR.getInterfaceArtInfo("WORLDBUILDER_CITY_EDIT")->getPath(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pCity->getX_INLINE(), pCity->getY_INLINE(), true, true);
				}

				if (pLoopPlayer.isEuropeanAI())
				{
					pCity->preventTerritorialInfluenceArround(pLoopPlayer.getID(), false);
				}
			}
		}
	}

	szBuffer = gDLL->getText("TXT_KEY_MISC_CITY_RAZED_BY", pCity->getNameKey(), getCivilizationDescriptionKey());
	GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getID(), szBuffer, pCity->getX_INLINE(), pCity->getY_INLINE(), (ColorTypes)GC.getInfoTypeForString("COLOR_WARNING_TEXT"));

	// Report this event
	gDLL->getEventReporterIFace()->cityRazed(pCity, getID());

	if (pCity->getPreviousOwner() != NO_PLAYER && !isNative())
	{
		if (pCity->getHighestPopulation() > 1)
		{
			UnitClassTypes eUnitClass = (UnitClassTypes) GC.getCivilizationInfo(GET_PLAYER(pCity->getPreviousOwner()).getCivilizationType()).getCapturedCityUnitClass();

			if (eUnitClass != NO_UNITCLASS)
			{
				UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClass);

				if (eUnit != NO_UNIT)
				{
					for (int i = 0; i < pCity->getPopulation(); ++i)
					{
						initUnit(eUnit, (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession(), pCity->getX_INLINE(), pCity->getY_INLINE());
					}
				}
			}
		}

		int iTreasure = GC.getDefineINT("SETTLEMENT_TREASURE_YIELD") * pCity->getHighestPopulation();
		iTreasure = iTreasure * GC.getCivilizationInfo(GET_PLAYER(pCity->getPreviousOwner()).getCivilizationType()).getTreasure() / 100;
		iTreasure = iTreasure * GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent() / 100;
		iTreasure = iTreasure / 2 + GC.getGameINLINE().getSorenRandNum(iTreasure / 2, "Treasure Gold 1") + GC.getGameINLINE().getSorenRandNum(iTreasure / 2, "Treasure Gold 2");

		int iTreasureMod = 100;
		for (int iTrait = 0; iTrait < GC.getNumTraitInfos(); ++iTrait)
		{
			CvTraitInfo& kTrait = GC.getTraitInfo((TraitTypes) iTrait);
			if (GET_PLAYER(pCity->getPreviousOwner()).hasTrait((TraitTypes) iTrait))
			{
				iTreasureMod += kTrait.getTreasureModifier();
			}
		}
		iTreasure *= iTreasureMod;
		iTreasure /= 100;

		if (iTreasure != 0)
		{
			UnitClassTypes eUnitClass = (UnitClassTypes) GC.getDefineINT("TREASURE_UNITCLASS");

			if (eUnitClass != NO_UNITCLASS)
			{
				UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClass);

				if (eUnit != NO_UNIT)
				{
					FAssert(GC.getUnitInfo(eUnit).isTreasure());
					CvUnit* pTreasure = initUnit(eUnit, (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession(), pCity->getX_INLINE(), pCity->getY_INLINE(), NO_UNITAI, NO_DIRECTION, iTreasure);
				}
			}
		}
	}

	disband(pCity);
}


void CvPlayer::disband(CvCity* pCity)
{
	if (getNumCities() == 1)
	{
		setFoundedFirstCity(false);
	}

	GC.getGameINLINE().addDestroyedCityName(pCity->getNameKey());

	pCity->kill();
}


bool CvPlayer::canReceiveGoody(CvPlot* pPlot, GoodyTypes eGoody, const CvUnit* pUnit) const
{
	CvGoodyInfo& kGoody = GC.getGoodyInfo(eGoody);

	if (kGoody.isWar())
	{
		if (!pPlot->isOwned())
		{
			return false;
		}

		if (!GET_TEAM(pPlot->getTeam()).canDeclareWar(getTeam()))
		{
			return false;
		}
	}
	if (pPlot->getPlotCity() == NULL && kGoody.getCityGoodyWeight() > 0)
	{
		return false;
	}

	//DOANE
	if (kGoody.getExperience() > 0)
	{	
		return false;
	}

	if (kGoody.getExploAndNavigationExperience() > 0)
	{	//Only scouts
		if ((pUnit == NULL) || !pUnit->canAcquirePromotionAny() || pUnit->getProfession() != PROFESSION_SCOUT)
		{
			return false;
		}
	}
	//END DOANE

	if (kGoody.getDamagePrereq() > 0)
	{
		if ((pUnit == NULL) || (pUnit->getDamage() < ((pUnit->maxHitPoints() * kGoody.getDamagePrereq()) / 100)))
		{
			return false;
		}
	}
	if (kGoody.isBad())
	{
		if ((pUnit == NULL) || pUnit->isNoBadGoodies())
		{
			return false;
		}
	}

	if (kGoody.getUnitClassType() != NO_UNITCLASS)
	{
		UnitTypes eUnit = ((UnitTypes)(GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(kGoody.getUnitClassType())));

		if (eUnit == NO_UNIT)
		{
			return false;
		}

		if ((GC.getUnitInfo(eUnit).getCombat() > 0) && !(GC.getUnitInfo(eUnit).isOnlyDefensive()))
		{
			if (GC.getGameINLINE().isGameMultiPlayer())
			{
				return false;
			}
		}

		if (GC.getGameINLINE().isOption(GAMEOPTION_ONE_CITY_CHALLENGE) && isHuman())
		{
			if (GC.getUnitInfo(eUnit).isFound())
			{
				return false;
			}
		}
	}

	if (kGoody.getTeachUnitClassType() != NO_UNITCLASS)
	{
		if (pUnit == NULL)
		{
			return false;
		}

		UnitTypes eUnit = (UnitTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(kGoody.getTeachUnitClassType());

		if (eUnit == NO_UNIT)
		{
			return false;
		}

		if (pUnit->getUnitInfo().getLearnTime() < 0)
		{
			return false;
		}

		if (pUnit->getProfession() != GC.getUnitInfo(eUnit).getDefaultProfession())
		{
			return false;
		}
	}

	for (int i = 0; i < GC.getNumGoodyInfos(); ++i)
	{
		if (kGoody.getGoodyWeight(i) > 0)
		{
			for (int j = 0; j < GC.getNumGoodyInfos(); ++j)
			{
				if (GC.getGoodyInfo((GoodyTypes) i).getGoodyWeight(j) > 0)
				{
					return false;
				}
			}

			if (!canReceiveGoody(pPlot, (GoodyTypes) i, pUnit))
			{
				return false;
			}
		}
	}

	return true;
}

void CvPlayer::receiveRandomGoody(CvPlot* pPlot, GoodyTypes eGoody, CvUnit* pUnit)
{
	int iTotalWeight = 0;
	GoodyTypes eBestGoody = NO_GOODY;
	for (int i = 0; i < GC.getNumGoodyInfos(); ++i)
	{
		iTotalWeight += GC.getGoodyInfo(eGoody).getGoodyWeight(i);
	}

	int iPick = GC.getGameINLINE().getSorenRandNum(iTotalWeight, "goody result");

	iTotalWeight = 0;
	for (int i = 0; i < GC.getNumGoodyInfos(); ++i)
	{
		iTotalWeight += GC.getGoodyInfo(eGoody).getGoodyWeight(i);
		if (iTotalWeight > iPick)
		{
			receiveGoody(pPlot, (GoodyTypes) i, pUnit);
			break;
		}
	}
}

int CvPlayer::receiveGoody(CvPlot* pPlot, GoodyTypes eGoody, CvUnit* pUnit)
{
	int iReturnValue = -1;

	FAssertMsg(canReceiveGoody(pPlot, eGoody, pUnit), "Instance is expected to be able to recieve goody");

	CvGoodyInfo& kGoody = GC.getGoodyInfo(eGoody);

	CvWString szBuffer = gDLL->getObjectText(kGoody.getAnnounceTextKey(), 0);

	int iGold = kGoody.getGold() + GC.getGameINLINE().getSorenRandNum(kGoody.getGoldRand1(), "Goody Gold 1") + GC.getGameINLINE().getSorenRandNum(kGoody.getGoldRand2(), "Goody Gold 2");

	if (iGold != 0)
	{
		//store gold in treasure unit instead
		UnitClassTypes eUnitClass = (UnitClassTypes) kGoody.getUnitClassType();
		bool bTreasure = false;
		if (eUnitClass != NO_UNITCLASS)
		{
			UnitTypes eTreasure = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClass);
			if ((eTreasure != NO_UNIT) && GC.getUnitInfo(eTreasure).isTreasure())
			{
				bTreasure = true;
			}
		}
	}

	if (!szBuffer.empty())
	{
		gDLL->getInterfaceIFace()->addMessage(getID(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, kGoody.getSound(), MESSAGE_TYPE_MINOR_EVENT, ARTFILEMGR.getImprovementArtInfo("ART_DEF_IMPROVEMENT_GOODY_HUT")->getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
	}

	int iRange = kGoody.getMapRange();
	if ((pUnit != NULL) && pUnit->isNoBadGoodies())
	{
		iRange = iRange * GC.getDefineINT("NO_BAD_GOODIES_GOLD_PERCENT") / 100; //TODO Cache
	}

	if (iRange > 0)
	{
		int iOffset = kGoody.getMapOffset();

		CvPlot* pBestPlot = NULL;
		if (iOffset > 0)
		{
			int iBestValue = 0;

			for (int iDX = -(iOffset); iDX <= iOffset; iDX++)
			{
				for (int iDY = -(iOffset); iDY <= iOffset; iDY++)
				{
					CvPlot* pLoopPlot = plotXY(pPlot->getX_INLINE(), pPlot->getY_INLINE(), iDX, iDY);

					if (pLoopPlot != NULL)
					{
						if (!(pLoopPlot->isRevealed(getTeam(), false)))
						{
							int iValue = (1 + GC.getGameINLINE().getSorenRandNum(10000, "Goody Map"));

							iValue *= plotDistance(pPlot->getX_INLINE(), pPlot->getY_INLINE(), pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE());

							if (iValue > iBestValue)
							{
								iBestValue = iValue;
								pBestPlot = pLoopPlot;
							}
						}
					}
				}
			}
		}

		if (pBestPlot == NULL)
		{
			pBestPlot = pPlot;
		}

		for (int iDX = -(iRange); iDX <= iRange; iDX++)
		{
			for (int iDY = -(iRange); iDY <= iRange; iDY++)
			{
				CvPlot* pLoopPlot = plotXY(pBestPlot->getX_INLINE(), pBestPlot->getY_INLINE(), iDX, iDY);

				if (pLoopPlot != NULL)
				{
					if (plotDistance(pBestPlot->getX_INLINE(), pBestPlot->getY_INLINE(), pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE()) <= iRange)
					{
						if (GC.getGameINLINE().getSorenRandNum(100, "Goody Map") < kGoody.getMapProb())
						{
							//DOANE Maps: Give revealed map to unit. This is forced, no need for scout or ship
							if (pUnit != NULL)
							{
								int iNumMap = -1;
								if (NULL != pUnit && !pLoopPlot->isDefinitelyRevealed(getTeam()))
								{
									//Forces goodie to give the map (temporarily) to any unit, so maps are not lost when given to a soldier for ex.
									iNumMap = pUnit->getNumMapForced(false);
									if (iNumMap >= 0) 
									{
										pLoopPlot->setRevealedMap(getTeam(), iNumMap, true);
										pUnit->incrementCountDiscoveredMapTiles();
									}
								}
								pLoopPlot->setRevealed(getTeam(), true, false, NO_TEAM);
							}
							//DOANE MAPS: Give map to the city. Just reveal definitely...
							else
							{
								pLoopPlot->setRevealed(getTeam(), true, false, NO_TEAM);
								pLoopPlot->setDefinitelyRevealed(getTeam(), true);
							}
							//END DOANE
						}
					}
				}
			}
		}
	}

	if (pUnit != NULL)
	{
		int iExploAndNavigationExperience = kGoody.getExploAndNavigationExperience();
		//DOANE Goody experience for scouts: give a different experience type if the unit is a scout
		if (iExploAndNavigationExperience > 0) //Only Scouts
		{
			if (pUnit->getProfession() == PROFESSION_SCOUT)
			{
				pUnit->changeExperienceExploAndNavigation(iExploAndNavigationExperience);
			}
		}
		//END DOANE
	}

	if (pUnit != NULL)
	{
		pUnit->changeDamage(-(kGoody.getHealing()));
	}

	if (kGoody.getUnitClassType() != NO_UNITCLASS)
	{
		UnitTypes eUnit = (UnitTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(kGoody.getUnitClassType());

		if (eUnit != NO_UNIT)
		{
			CvUnit* pGoodyUnit = initUnit(eUnit, (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession(), pPlot->getX_INLINE(), pPlot->getY_INLINE());

			//treasure
			if ((pGoodyUnit != NULL) && (iGold != 0) && pGoodyUnit->getUnitInfo().isTreasure())
			{
				pGoodyUnit->setYieldStored(iGold);
			}
		}
	}

	for (int i = 0; i < GC.getNumGoodyInfos(); ++i)
	{
		if (kGoody.getGoodyWeight(i) > 0)
		{
			if (isHuman())
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_GOODY, eGoody, GC.getMapINLINE().plotNumINLINE(pPlot->getX_INLINE(), pPlot->getY_INLINE()), pUnit ? pUnit->getID() : -1);
				gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
			}
			else
			{
				if (AI_chooseGoody(eGoody))
				{
					receiveRandomGoody(pPlot, eGoody, pUnit);
				}
			}
			break;
		}
	}
	iReturnValue = iGold;

	return iReturnValue;
}

void CvPlayer::doGoody(CvPlot* pPlot, CvUnit* pUnit)
{
	// Python override
	if (gDLL->getPythonIFace()->pythonDoGoody(getID(), pPlot, pUnit))
	{
		return;
	}
	else
	{
		FAssertMsg(pPlot->isGoody(), "pPlot->isGoody is expected to be true");

		if (!isNative())
		{
			pPlot->removeGoody();

			std::vector<int> aGoodyFactors(GC.getNumGoodyInfos(), 1);
			for (int iTrait = 0; iTrait < GC.getNumTraitInfos(); ++iTrait)
			{
				CvTraitInfo& kTraitInfo = GC.getTraitInfo((TraitTypes) iTrait);
				if (hasTrait((TraitTypes) iTrait))
				{
					for (int iGoody = 0; iGoody < GC.getNumGoodyInfos(); ++iGoody)
					{
						aGoodyFactors[iGoody] *= kTraitInfo.getGoodyFactor(iGoody);
					}
				}
			}

			int iBestValue = -1;
			GoodyTypes eBestGoody = NO_GOODY;
			for (int iI = 0; iI < GC.getHandicapInfo(getHandicapType()).getNumGoodies(); iI++)
			{
				GoodyTypes eGoody = (GoodyTypes) GC.getHandicapInfo(getHandicapType()).getGoodies(iI);

				FAssert(eGoody >= 0);
				FAssert(eGoody < GC.getNumGoodyInfos());

				if (canReceiveGoody(pPlot, eGoody, pUnit))
				{
					for (int iJ = 0; iJ < aGoodyFactors[eGoody]; ++iJ)
					{
						int iRandValue = GC.getGameINLINE().getSorenRandNum(1000, "Goodies");
						if (iRandValue > iBestValue)
						{
							iBestValue = iRandValue;
							eBestGoody = eGoody;
						}
					}
				}
			}

			int iRandValue = GC.getGameINLINE().getSorenRandNum(100, "Ruins Goodies");
			if (iRandValue < 50)
			{
				eBestGoody = NO_GOODY;
				gDLL->getInterfaceIFace()->addMessage(getID(), false, GC.getEVENT_MESSAGE_TIME(), gDLL->getText("TXT_KEY_NO_GOODY_ON_FEATURE"), "AS2D_PILLAGED", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"));
			}

			if (eBestGoody != NO_GOODY)
			{
				receiveGoody(pPlot, eBestGoody, pUnit);

				// Python Event
				gDLL->getEventReporterIFace()->goodyReceived(getID(), pPlot, pUnit, eBestGoody);
			}
		}
	}
}

bool CvPlayer::canFound(int iX, int iY, bool bTestVisible) const
{
	CvPlot* pPlot;
	CvPlot* pLoopPlot;
	bool bValid;
	int iRange;
	int iDX, iDY;

	pPlot = GC.getMapINLINE().plotINLINE(iX, iY);

	if (GC.getGameINLINE().isFinalInitialized())
	{
		if (GC.getGameINLINE().isOption(GAMEOPTION_ONE_CITY_CHALLENGE) && isHuman())
		{
			if (getNumCities() > 0)
			{
				return false;
			}
		}
	}

	if (pPlot->isImpassable())
	{
		return false;
	}

	if (pPlot->isPeak())
	{
		return false;
	}

	if (pPlot->getFeatureType() != NO_FEATURE)
	{
		if (GC.getFeatureInfo(pPlot->getFeatureType()).isNoCity())
		{
			return false;
		}
	}

	if (pPlot->isCity())
	{
		return false;
	}

	if (pPlot->isOwned() && (pPlot->getOwnerINLINE() != getID()))
	{
		if (!GET_PLAYER(pPlot->getOwnerINLINE()).isNative())
		{
			return false;
		}
	}

	bValid = false;

	if (!bValid)
	{
		if (GC.getTerrainInfo(pPlot->getTerrainType()).isFound())
		{
			bValid = true;
		}
	}

	if (!bValid)
	{
		if (GC.getTerrainInfo(pPlot->getTerrainType()).isFoundCoast())
		{
			if (pPlot->isCoastalLand())
			{
				bValid = true;
			}
		}
	}

	long lResult = 0;
	if (GC.getUSE_CAN_FOUND_CITIES_ON_WATER_CALLBACK())
	{
		CyArgsList argsList2;
		argsList2.add(iX);
		argsList2.add(iY);
		lResult=0;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "canFoundCitiesOnWater", argsList2.makeFunctionArgs(), &lResult);
	}

	if (lResult == 1)
	{
		bValid = true;
	}
	else
	{
		if (pPlot->isWater())
		{
			return false;
		}
	}

	if (!bValid)
	{
		return false;
	}

	if (!bTestVisible)
	{
		iRange = GC.getMIN_CITY_RANGE();

		for (iDX = -iRange; iDX <= iRange; iDX++)
		{
			for (iDY = -iRange; iDY <= iRange; iDY++)
			{
				pLoopPlot = plotXY(pPlot->getX_INLINE(), pPlot->getY_INLINE(), iDX, iDY);

				if (pLoopPlot != NULL && pLoopPlot->isCity())
				{
					if (pLoopPlot->area() == pPlot->area())
					{
						if (!GET_PLAYER(pLoopPlot->getOwnerINLINE()).isNative())
						{
							return false;
						}
					}
				}
			}
		}
	}

	return true;
}


void CvPlayer::found(int iX, int iY)
{
	if (!canFound(iX, iY))
	{
		return;
	}

	CvCity* pCity = initCity(iX, iY, true);
	FAssertMsg(pCity != NULL, "City is not assigned a valid value");

	for (int iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
	{
		BuildingTypes eLoopBuilding = ((BuildingTypes)(GC.getCivilizationInfo(getCivilizationType()).getCivilizationBuildings(iI)));

		if (eLoopBuilding != NO_BUILDING)
		{
			if (GC.getBuildingInfo(eLoopBuilding).getFreeStartEra() != NO_ERA)
			{
				if (GC.getGameINLINE().getStartEra() >= GC.getBuildingInfo(eLoopBuilding).getFreeStartEra())
				{
					if (pCity->canConstruct(eLoopBuilding))
					{
						pCity->setHasRealBuilding(eLoopBuilding, true);
					}
				}
			}
		}
	}

	if (getAdvancedStartPoints() < 0)
	{
		if (isHuman())
		{
			pCity->chooseProduction();
		}
		else
		{
			pCity->doFoundMessage();
		}
	}

	gDLL->getEventReporterIFace()->cityBuilt(pCity);
}


bool CvPlayer::canTrain(UnitTypes eUnit, bool bContinue, bool bTestVisible, bool bIgnoreCost) const
{
	PROFILE_FUNC();

	UnitClassTypes eUnitClass;
	eUnitClass = ((UnitClassTypes)(GC.getUnitInfo(eUnit).getUnitClassType()));

	FAssert(GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClass) == eUnit);
	if (GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClass) != eUnit)
	{
		return false;
	}

	if (!isEurope())
	{
		switch(eUnit)
		{
		case UNIT_FRIGATE:
		case UNIT_SHIP_OF_THE_LINE:
		case UNIT_MAN_O_WAR:
			return false;
		}
	}

	if (!bIgnoreCost)
	{
		bool bCanBuild = false;
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
		{
			if (GC.getUnitInfo(eUnit).getYieldCost(iYield) > 0)
			{
				bCanBuild = true;
				break;
			}
		}

		if (!bCanBuild)
		{
			return false;
		}
	}

	if (GC.getGameINLINE().isOption(GAMEOPTION_ONE_CITY_CHALLENGE) && isHuman())
	{
		if (GC.getUnitInfo(eUnit).isFound())
		{
			return false;
		}
	}

	//if (!bTestVisible)
	{
		if (GC.getUnitInfo(eUnit).getSpecialUnitType() != NO_SPECIALUNIT)
		{
			if (!(GC.getGameINLINE().isSpecialUnitValid((SpecialUnitTypes)(GC.getUnitInfo(eUnit).getSpecialUnitType()))))
			{
				return false;
			}
		}
	}
	if (!isHuman() && !isNative() && !isEurope())
	{
		int iNumMaxIA = GC.getUnitInfo(eUnit).getNumMaxIA();
		if (iNumMaxIA >= 0)
		{
			int iNum = 0;
			int iLoop;
			for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
			{
				if (pLoopUnit->getUnitType() == eUnit)
				{
					iNum++;
				}
			}
			if (iNum >= iNumMaxIA)
			{
				return false;
			}
		}
	}

	return true;
}


bool CvPlayer::canConstruct(BuildingTypes eBuilding, bool bContinue, bool bTestVisible, bool bIgnoreCost) const
{
	BuildingClassTypes eBuildingClass;
	CvTeamAI& currentTeam = GET_TEAM(getTeam());

	eBuildingClass = ((BuildingClassTypes)(GC.getBuildingInfo(eBuilding).getBuildingClassType()));

	FAssert(GC.getCivilizationInfo(getCivilizationType()).getCivilizationBuildings(eBuildingClass) == eBuilding);
	if (GC.getCivilizationInfo(getCivilizationType()).getCivilizationBuildings(eBuildingClass) != eBuilding)
	{
		return false;
	}

	if (!bIgnoreCost)
	{
		bool bCanBuild = false;
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
		{
			if (GC.getBuildingInfo(eBuilding).getYieldCost(iYield) > 0)
			{
				bCanBuild = true;
				break;
			}
		}

		if (!bCanBuild && GC.getBuildingInfo(eBuilding).getRefBuilding() != 8)//DO NOTHING
		{
			return false;
		}
	}
	if (GC.getGameINLINE().countCivTeamsEverAlive() < GC.getBuildingInfo(eBuilding).getNumTeamsPrereq())
	{
		return false;
	}

	if (GC.getBuildingInfo(eBuilding).getVictoryPrereq() != NO_VICTORY)
	{
		if (!(GC.getGameINLINE().isVictoryValid((VictoryTypes)(GC.getBuildingInfo(eBuilding).getVictoryPrereq()))))
		{
			return false;
		}
	}

	if (GC.getBuildingInfo(eBuilding).getMaxStartEra() != NO_ERA)
	{
		if (GC.getGameINLINE().getStartEra() > GC.getBuildingInfo(eBuilding).getMaxStartEra())
		{
			return false;
		}
	}

	//if (!bTestVisible)
	{
		if (GC.getBuildingInfo(eBuilding).getSpecialBuildingType() != NO_SPECIALBUILDING)
		{
			if (!(GC.getGameINLINE().isSpecialBuildingValid((SpecialBuildingTypes)(GC.getBuildingInfo(eBuilding).getSpecialBuildingType()))))
			{
				return false;
			}
		}

		if (getNumCities() < GC.getBuildingInfo(eBuilding).getNumCitiesPrereq())
		{
			return false;
		}

		if (getHighestUnitLevel() < GC.getBuildingInfo(eBuilding).getUnitLevelPrereq())
		{
			return false;
		}
		for (int iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
		{
			if (getBuildingClassCount((BuildingClassTypes)iI) < getBuildingClassPrereqBuilding(eBuilding, ((BuildingClassTypes)iI), ((bContinue) ? 0 : getBuildingClassMaking(eBuildingClass))))
			{
				return false;
			}
		}
	}

	return true;
}

int CvPlayer::getYieldProductionNeeded(UnitTypes eUnit, YieldTypes eYield) const
{
	UnitClassTypes eUnitClass = (UnitClassTypes)GC.getUnitInfo(eUnit).getUnitClassType();
	FAssert(NO_UNITCLASS != eUnitClass);

	int iProductionNeeded = GC.getUnitInfo(eUnit).getYieldCost(eYield);

	iProductionNeeded *= GC.getDefineINT("UNIT_PRODUCTION_PERCENT");
	iProductionNeeded /= 100;

	iProductionNeeded *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getTrainPercent();
	iProductionNeeded /= 100;

	iProductionNeeded *= GC.getEraInfo(GC.getGameINLINE().getStartEra()).getTrainPercent();
	iProductionNeeded /= 100;

	if (!isHuman() && !isNative())
	{
		iProductionNeeded *= GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAITrainPercent();
		iProductionNeeded /= 100;

		iProductionNeeded *= std::max(0, ((GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIPerEraModifier() * getCurrentEra()) + 100));
		iProductionNeeded /= 100;
	}

	// Python cost modifier
	if (GC.getUSE_GET_UNIT_COST_MOD_CALLBACK())
	{
		CyArgsList argsList;
		argsList.add(getID());	// Player ID
		argsList.add((int)eUnit);
		argsList.add((int)eYield);
		long lResult=0;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "getUnitCostMod", argsList.makeFunctionArgs(), &lResult);

		if (lResult > 1)
		{
			iProductionNeeded *= lResult;
			iProductionNeeded /= 100;
		}
	}

	return iProductionNeeded;
}


int CvPlayer::getYieldProductionNeeded(BuildingTypes eBuilding, YieldTypes eYield) const
{
	int iProductionNeeded = GC.getBuildingInfo(eBuilding).getYieldCost(eYield);

	iProductionNeeded *= 100 + getBuildingRequiredYieldModifier(eYield);
	iProductionNeeded /= 100;

	if ( GC.getBuildingInfo(eBuilding).getEraAllowed() < getNewEra())
	{
		iProductionNeeded *= 100 - GC.getAVANCED_NEW_ERA_BOOST_FOR_CONSTRUCTION();
		iProductionNeeded /= 100;
	}

	return iProductionNeeded;
}


int CvPlayer::getProductionModifier(UnitTypes eUnit) const
{
	int iMultiplier = 0;

	if (GC.getUnitInfo(eUnit).isMilitaryProduction())
	{
		iMultiplier += getMilitaryProductionModifier();
	}

	for (int iI = 0; iI < GC.getNumTraitInfos(); iI++)
	{
		if (hasTrait((TraitTypes)iI))
		{
			iMultiplier += GC.getUnitInfo(eUnit).getProductionTraits(iI);

			if (GC.getUnitInfo(eUnit).getSpecialUnitType() != NO_SPECIALUNIT)
			{
				iMultiplier += GC.getSpecialUnitInfo((SpecialUnitTypes) GC.getUnitInfo(eUnit).getSpecialUnitType()).getProductionTraits(iI);
			}
		}
	}

	return iMultiplier;
}

int CvPlayer::getProductionModifier(BuildingTypes eBuilding) const
{
	int iMultiplier = 0;
	for (int iI = 0; iI < GC.getNumTraitInfos(); iI++)
	{
		if (hasTrait((TraitTypes)iI))
		{
			iMultiplier += GC.getBuildingInfo(eBuilding).getProductionTraits(iI);

			if (GC.getBuildingInfo(eBuilding).getSpecialBuildingType() != NO_SPECIALBUILDING)
			{
				iMultiplier += GC.getSpecialBuildingInfo((SpecialBuildingTypes) GC.getBuildingInfo(eBuilding).getSpecialBuildingType()).getProductionTraits(iI);
			}
		}
	}

	BuildingClassTypes eBuildingClass = (BuildingClassTypes) GC.getBuildingInfo(eBuilding).getBuildingClassType();
	for (int iI = 0; iI < GC.getNumTraitInfos(); iI++)
	{
		TraitTypes eTrait = (TraitTypes) iI;
		if (hasTrait(eTrait))
		{
			iMultiplier += GC.getTraitInfo(eTrait).getBuildingProductionModifier(eBuildingClass);
		}
	}

	return iMultiplier;
}

int CvPlayer::getBuildingClassPrereqBuilding(BuildingTypes eBuilding, BuildingClassTypes ePrereqBuildingClass, int iExtra) const
{
	CvBuildingInfo& kBuilding = GC.getBuildingInfo(eBuilding);

	int iPrereqs = kBuilding.getPrereqNumOfBuildingClass(ePrereqBuildingClass);

	// dont bother with the rest of the calcs if we have no prereqs
	if (iPrereqs < 1)
	{
		return 0;
	}

	BuildingClassTypes eBuildingClass = (BuildingClassTypes)kBuilding.getBuildingClassType();

	iPrereqs *= std::max(0, (GC.getWorldInfo(GC.getMapINLINE().getWorldSize()).getBuildingClassPrereqModifier() + 100));
	iPrereqs /= 100;

	iPrereqs *= (getBuildingClassCount((BuildingClassTypes)(GC.getBuildingInfo(eBuilding).getBuildingClassType())) + iExtra + 1);

	if (GC.getGameINLINE().isOption(GAMEOPTION_ONE_CITY_CHALLENGE) && isHuman())
	{
		iPrereqs = std::min(1, iPrereqs);
	}

	return iPrereqs;
}


void CvPlayer::removeBuildingClass(BuildingClassTypes eBuildingClass)
{
	CvCity* pLoopCity;
	BuildingTypes eBuilding;
	int iLoop;

	eBuilding = ((BuildingTypes)(GC.getCivilizationInfo(getCivilizationType()).getCivilizationBuildings(eBuildingClass)));

	if (eBuilding != NO_BUILDING)
	{
		for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
		{
			if (pLoopCity->isHasRealBuilding(eBuilding))
			{
				pLoopCity->setHasRealBuilding(eBuilding, false);
				break;
			}
		}
	}
}

void CvPlayer::processTrait(TraitTypes eTrait, int iChange)
{
	CvTraitInfo& kTrait = GC.getTraitInfo(eTrait);

	changeTraitCount(eTrait, iChange);

	changeLevelExperienceModifier(kTrait.getLevelExperienceModifier() * iChange);
	changeGreatGeneralRateModifier(kTrait.getGreatGeneralRateModifier() * iChange);
	changeDomesticGreatGeneralRateModifier(kTrait.getDomesticGreatGeneralRateModifier() * iChange);

	changeNativeAngerModifier(kTrait.getNativeAngerModifier() * iChange);
	changeNativeCombatModifier(kTrait.getNativeCombatModifier() * iChange);
	changeMissionaryRateModifier(kTrait.getMissionaryModifier() * iChange);

	for (int iProfession = 0; iProfession < GC.getNumProfessionInfos(); ++iProfession)
	{
		ProfessionTypes eProfession = (ProfessionTypes) iProfession;
		int iNewValue = getProfessionEquipmentModifier(eProfession) + kTrait.getProfessionEquipmentModifier(eProfession) * iChange;
		setProfessionEquipmentModifier(eProfession, iNewValue);
	}

	int iLoop;
	std::vector<CvUnit*> apUnits;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		apUnits.push_back(pLoopUnit);
	}
	for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
	{
		apUnits.push_back(m_aEuropeUnits[i]);
	}

	for (uint i = 0; i < apUnits.size(); ++i)
	{
		CvUnit* pLoopUnit = apUnits[i];
		for (int iJ = 0; iJ < GC.getNumPromotionInfos(); iJ++)
		{
			if (kTrait.isFreePromotion(iJ))
			{
				if ((pLoopUnit->getUnitCombatType() != NO_UNITCOMBAT) && kTrait.isFreePromotionUnitCombat(pLoopUnit->getUnitCombatType()))
				{
					pLoopUnit->changeFreePromotionCount(((PromotionTypes)iJ), iChange);
				}
			}
		}
	}

	bool bUpdatePlotYields = false;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;

		changeYieldRateModifier(eYield, iChange * kTrait.getYieldModifier(iYield));
		changeBuildingRequiredYieldModifier(eYield, kTrait.getBuildingRequiredYieldModifier(eYield) * iChange);

		if (kTrait.isTaxYieldModifier(iYield))
		{
			changeTaxYieldModifierCount(eYield, iChange);
		}

		for (int iBuildingClass = 0; iBuildingClass < GC.getNumBuildingClassInfos(); ++iBuildingClass)
		{
			changeBuildingYieldChange((BuildingClassTypes) iBuildingClass, eYield, iChange * kTrait.getBuildingYieldChange(iBuildingClass, iYield));
		}

		updateExtraYieldThreshold(eYield);

		if (kTrait.getCityExtraYield(iYield) != 0 || kTrait.getExtraYieldThreshold(iYield) != 0)
		{
			bUpdatePlotYields = true;
		}
	}

	if (bUpdatePlotYields)
	{
		updateYield();
	}

	for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass)
	{
		changeUnitMoveChange((UnitClassTypes) iUnitClass, iChange * kTrait.getUnitMoveChange(iUnitClass));
		changeUnitStrengthModifier((UnitClassTypes) iUnitClass, iChange * kTrait.getUnitStrengthModifier(iUnitClass));
	}

	for (int iProfession = 0; iProfession < GC.getNumProfessionInfos(); ++iProfession)
	{
		changeProfessionMoveChange((ProfessionTypes) iProfession, iChange * kTrait.getProfessionMoveChange(iProfession));
	}

	for (int iBuildingClass = 0; iBuildingClass < GC.getNumBuildingClassInfos(); ++iBuildingClass)
	{
		if (kTrait.isFreeBuildingClass(iBuildingClass))
		{
			BuildingTypes eFreeBuilding = (BuildingTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationBuildings(iBuildingClass);
			if (eFreeBuilding != NO_BUILDING)
			{
				changeFreeBuildingCount(eFreeBuilding, iChange);
			}
		}
	}

	if (kTrait.getNativeAttitudeChange() != 0)
	{
		for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes) iPlayer);

			if (kLoopPlayer.isAlive() && kLoopPlayer.isNative())
			{
				kLoopPlayer.AI_changeAttitudeExtra(getID(), kTrait.getNativeAttitudeChange());

				if (iChange > 0 && kTrait.getNativeAttitudeChange() > 0)
				{
					GET_TEAM(getTeam()).makePeace(kLoopPlayer.getTeam());
				}
			}
		}
	}

	changeCityDefenseModifier(iChange * kTrait.getCityDefense());
}

bool CvPlayer::canBuild(const CvPlot* pPlot, BuildTypes eBuild, bool bTestEra, bool bTestVisible) const
{
	PROFILE_FUNC();

	if (!(pPlot->canBuild(eBuild, getID(), bTestVisible)))
	{
		return false;
	}
	if (!bTestVisible)
	{
		if (std::max(0, getGold()) < getBuildCost(pPlot, eBuild))
		{
			return false;
		}
	}
	BonusTypes eBonus = (BonusTypes) GC.getBuildInfo(eBuild).getBonusType();
	if (eBonus != NO_BONUS && !GET_TEAM(getTeam()).hasSeedling(eBonus) && isHuman())
	{
		return false;
	}

	return true;
}


// Returns the cost
int CvPlayer::getBuildCost(const CvPlot* pPlot, BuildTypes eBuild) const
{
	FAssert(eBuild >= 0 && eBuild < GC.getNumBuildInfos());

	if (pPlot->getBuildProgress(eBuild) > 0)
	{
		return 0;
	}
	int iCost = GC.getBuildInfo(eBuild).getCost();

	iCost += GC.getBuildInfo(eBuild).getCostPerTurn()*pPlot->getBuildTime(eBuild)/100;

	return std::max(0, iCost);
}

RouteTypes CvPlayer::getBestRoute(CvPlot* pPlot) const
{
	PROFILE_FUNC();

	RouteTypes eRoute;
	RouteTypes eBestRoute;
	int iValue;
	int iBestValue;
	int iI;

	iBestValue = 0;
	eBestRoute = NO_ROUTE;

	for (iI = 0; iI < GC.getNumBuildInfos(); iI++)
	{
		eRoute = ((RouteTypes)(GC.getBuildInfo((BuildTypes)iI).getRoute()));

		if (eRoute != NO_ROUTE)
		{
			if (pPlot != NULL)
			{
				if ((pPlot->getRouteType() == eRoute) || canBuild(pPlot, ((BuildTypes)iI)))
				{
					iValue = GC.getRouteInfo(eRoute).getValue();
					if (iValue > iBestValue)
					{
						iBestValue = iValue;
						eBestRoute = eRoute;
					}
				}
			}
		}
	}

	return eBestRoute;
}


int CvPlayer::getImprovementUpgradeRate() const
{
	int iRate;

	iRate = 1; // XXX

	iRate *= std::max(0, (getImprovementUpgradeRateModifier() + 100));
	iRate /= 100;

	return iRate;
}


int CvPlayer::calculateTotalYield(YieldTypes eYield) const
{
	CvCity* pLoopCity;
	int iTotal = 0;
	int iLoop = 0;

	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		iTotal += pLoopCity->calculateNetYield(eYield);
	}

	return iTotal;
}

int CvPlayer::calculateTotalRawSellableYield() const
{
	if (getParent() == NO_PLAYER)
	{
		return 0;
	}
	CvPlayerAI& kPlayer = GET_PLAYER(getID());
	CvCity* pLoopCity;
	int iTotal = 0;
	int iLoop = 0;
	//int iTotalPrice = 0;

	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		iTotal += pLoopCity->calculateTotalRawSellableYield();
	}

	return iTotal;
}

void CvPlayer::europeGiveShip(bool bAccept)
{
	if (isHuman())
	{
		if (bAccept)
		{
			CvUnit* pUnit = buyEuropeUnit(UNIT_MILITARY_ESCORT, 0);
			if (pUnit != NULL)
			{
				pUnit->setHasCrew(false);
			}
		}
		else
		{
			int iPrice = getEuropeUnitBuyPrice(UNIT_MILITARY_ESCORT);
			iPrice -= iPrice*(10+getRandomValues(0)%30)/100;
			changeEuropeLoan(-iPrice);
		}
		doEuropeLandPercent();
	}
}

int CvPlayer::getPlayerValue(YieldTypes eYield, int iAmount) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return iAmount*(100+getPlayerValue(eYield))/100;
}
int CvPlayer::getPlayerValue(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiPlayerValue[eYield];
}

void CvPlayer::setPlayerValue(YieldTypes eYield, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getPlayerValue(eYield) != iValue)
	{
		m_aiPlayerValue[eYield] = iValue;
	}
}
void CvPlayer::changePlayerValue(YieldTypes eYield, int iChange)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);

	setPlayerValue(eYield, std::max(getPlayerValue(eYield)+iChange, 0));
}
void CvPlayer::processPlayerValue()
{
	int iValue;
	int iTurn = GC.getGameINLINE().getGameTurn();
	if (isHuman() || isEurope())
	{
		return;
	}
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes)iYield;
		CvYieldInfo& kYieldInfo = GC.getYieldInfo(eYield);
		iValue = isNative() ? kYieldInfo.getNativeValue(): kYieldInfo.getEuropeanValue();
		if (getPlayerValue(eYield) < iValue && iTurn%2)
		{
			changePlayerValue(eYield, 1);
		}
	}
}
void CvPlayer::initYieldsPlayerValue()
{
	int iNatVal = 0;
	int	iEurVal = 0;
	int iValue;
	if (isHuman() || isEurope())
	{
		return;
	}
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes)iYield;
		CvYieldInfo& kYieldInfo = GC.getYieldInfo(eYield);
		iValue = isNative() ? kYieldInfo.getNativeValue(): kYieldInfo.getEuropeanValue();

		setPlayerValue(eYield, iValue);
	}
}
int CvPlayer::getRelationTradeTo(PlayerTypes ePlayer) const
{
	int iResult = 0;
	iResult += range(AI_getMemoryCount(ePlayer, MEMORY_AI_GIFT_TRADE)*4/100, -4, 4);
	iResult += AI_getMemoryCount(ePlayer, MEMORY_AI_REGULARITY_TRADE);
	iResult += std::min(AI_getMemoryCount(ePlayer, MEMORY_AI_AGREEMENT), 3);
	return iResult;
}
int CvPlayer::getRelationNeighbor(PlayerTypes ePlayer)
{
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	if (kPlayer.isEurope() || isEurope())
	{
		return 0;
	}
	if (kPlayer.isNative())
	{
		return 0;
	}
	if (isNearPlayer(ePlayer))
	{
		return isNative() ? 5 : -2;
	}
	return 0;
}

bool CvPlayer::isNearPlayer(PlayerTypes ePlayer) const
{
	int iLoop;
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvPlot* pLoopPlot;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		pLoopPlot = pLoopCity->plot();
		if (pLoopPlot->isNearPlayer(ePlayer, 3))
		{
			return true;
		}
	}
	for (CvCity* pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
	{
		pLoopPlot = pLoopCity->plot();
		if (pLoopPlot->isNearPlayer(getID(), 3))
		{
			return true;
		}
	}
	return false;
}

int CvPlayer::getRegularityTrade(PlayerTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_PLAYERS, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiRegularityTrade[eIndex];
}

void CvPlayer::setRegularityTrade(PlayerTypes eIndex, int iValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_PLAYERS, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_aiRegularityTrade[eIndex] = iValue;
}
void CvPlayer::changeRegularityTrade(PlayerTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_PLAYERS, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_aiRegularityTrade[eIndex] = (m_aiRegularityTrade[eIndex] + iChange);
}

int CvPlayer::getUnemploymentHistory(int eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_HIST_TURN, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiUnemploymentHistory[eIndex];
}

void CvPlayer::setUnemploymentHistory(int eIndex, int iValue)
{	//DOANE fix assignment operator
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_HIST_TURN, "eIndex is expected to be within maximum bounds (invalid Index)");
	if (iValue != getUnemploymentHistory(eIndex))
	{
		m_aiUnemploymentHistory[eIndex] = iValue;
	}
}

int CvPlayer::getAverageUnemploymentHistory()
{
	int iNumHistTurn = NUM_HIST_TURN;
	int iTurn = GC.getGameINLINE().getGameTurn();
	int iVar = 0;
	if (iTurn < 100)
	{
		return 0;
	}
	for (int iI = 0; iI < iNumHistTurn; iI++)
	{
		iVar += getUnemploymentHistory(iI);
	}
	iVar /= iNumHistTurn;

	return iVar;
}
int CvPlayer::getFoodHistory(int eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_HIST_TURN, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiFoodHistory[eIndex];
}

void CvPlayer::setFoodHistory(int eIndex, int iValue)
{	//DOANE fix assignment operator
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_HIST_TURN, "eIndex is expected to be within maximum bounds (invalid Index)");
	if (iValue != getFoodHistory(eIndex))
	{
		m_aiFoodHistory[eIndex] = iValue;
	}
}

int CvPlayer::getAverageFoodHistory()
{
	FAssertMsg(getNumHistTurn() > 0, "getNumHistTurn is expected to be positive (invalid getNumHistTurn)");

	int iNumHistTurn = getNumHistTurn();
	int iTurn = GC.getGameINLINE().getGameTurn();
	int iVar = 0;
	if (iTurn < iNumHistTurn)
	{
		return 0;
	}
	for (int iI = 0; iI < iNumHistTurn; iI++)
	{
		iVar += getFoodHistory(iI);
	}
	iVar /= iNumHistTurn;

	return iVar;
}
int CvPlayer::getEconomicHistory(int eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_HIST_TURN, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiEconomicHistory[eIndex];
}

void CvPlayer::setEconomicHistory(int eIndex, int iValue)
{	//DOANE fix assignment operator
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_HIST_TURN, "eIndex is expected to be within maximum bounds (invalid Index)");
	if (iValue != getEconomicHistory(eIndex))
	{
		m_aiEconomicHistory[eIndex] = iValue;
	}
}

int CvPlayer::getAverageEconomicHistory()
{
	int iNumHistTurn = NUM_HIST_TURN;
	int iTurn = GC.getGameINLINE().getGameTurn();
	int iVar = 0;
	if (iTurn < iNumHistTurn)
	{
		return 0;
	}
	for (int iI = 0; iI < iNumHistTurn; iI++)
	{
		iVar += getEconomicHistory(iI);
	}
	iVar /= iNumHistTurn;
	return iVar;
}
int CvPlayer::getReligionHistory(int eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_HIST_TURN, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiReligionHistory[eIndex];
}

void CvPlayer::setReligionHistory(int eIndex, int iValue)
{	//DOANE fix assignment operator
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_HIST_TURN, "eIndex is expected to be within maximum bounds (invalid Index)");
	if (iValue != getReligionHistory(eIndex))
	{
		m_aiReligionHistory[eIndex] = iValue;
	}
}

int CvPlayer::getAverageReligionHistory() const
{
	int iNumHistTurn = NUM_HIST_TURN;
	int iTurn = GC.getGameINLINE().getGameTurn();
	int iVar = 0;
	if (iTurn < iNumHistTurn)
	{
		return 0;
	}
	for (int iI = 0; iI < iNumHistTurn; iI++)
	{
		iVar += getReligionHistory(iI);
	}
	iVar /= iNumHistTurn;

	return iVar;
}

unsigned char CvPlayer::getRandomValues(int eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_RAND_VALUES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aucRandomValues[eIndex];
}

void CvPlayer::setRandomValues(int eIndex, unsigned char iValue)
{	//DOANE fix assignment operator
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_RAND_VALUES, "eIndex is expected to be within maximum bounds (invalid Index)");
	if (iValue != getRandomValues(eIndex))
	{
		m_aucRandomValues[eIndex] = iValue;
	}
}

int CvPlayer::getRandomValueBetweenRange(int minValue, int maxValue, int eIndex) const 
{
	double offset = (maxValue - minValue)*getRandomValues(eIndex) / 100.0;
	return minValue + (int)std::floor(offset + 0.5);//Allow to do a ceil
}

unsigned char CvPlayer::getUnitGivedByKing(UnitTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_UNIT_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aucUnitGivedByKing[eIndex];
}

void CvPlayer::setUnitGivedByKing(UnitTypes eIndex, unsigned char iValue)
{	
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_UNIT_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	if (iValue != getUnitGivedByKing(eIndex))
	{
		m_aucUnitGivedByKing[eIndex] = iValue;
	}
}

void CvPlayer::changeUnitGivedByKing(UnitTypes eIndex, unsigned char iValue)
{	
	setUnitGivedByKing(eIndex, getUnitGivedByKing(eIndex) + iValue);
}

EconomicTrendTypes CvPlayer::getEconomicTrend(YieldTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return (EconomicTrendTypes) m_aucEconomicTrends[eIndex];
}

void CvPlayer::setEconomicTrend(YieldTypes eIndex, EconomicTrendTypes eValue)
{	
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	if (eValue != getEconomicTrend(eIndex))
	{
		m_aucEconomicTrends[eIndex] = (unsigned char) eValue;
	}
}

unsigned char CvPlayer::getEconomicTrendsRemainingTurns(YieldTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aucEconomicTrendsRemainingTurns[eIndex];
}

void CvPlayer::setEconomicTrendsRemainingTurns(YieldTypes eIndex, unsigned char iValue)
{	
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	if (iValue != getEconomicTrendsRemainingTurns(eIndex))
	{
		m_aucEconomicTrendsRemainingTurns[eIndex] = iValue;
	}
}

void CvPlayer::changeEconomicTrendsRemainingTurns(YieldTypes eIndex, unsigned char iValue)
{	
	setEconomicTrendsRemainingTurns(eIndex, getEconomicTrendsRemainingTurns(eIndex) + iValue);
}

bool CvPlayer::getRegularityTradeInTurn(PlayerTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_PLAYERS, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_abRegularityTradeInTurn[eIndex];
}

void CvPlayer::setRegularityTradeInTurn(PlayerTypes eIndex, bool bValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_PLAYERS, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_abRegularityTradeInTurn[eIndex] = bValue;
}
void CvPlayer::europeGiveGalleon()
{
	if (isHuman())
	{
		int iPrice = getEuropeUnitBuyPrice(UNIT_GALLEON);
		if (getParentMood() >= -1)
		{
			iPrice = iPrice/2;
		}
		else
		{
			iPrice = iPrice*3/4;
		}
		changeEuropeLoan(iPrice);
		buyEuropeUnit(UNIT_GALLEON, 0);
	}
}

int CvPlayer::getRebelPercentForColonies() const
{
	int iLoop;
	int iRebelSentiment = 0;
	int iNumCities = getNumCities();

	if (iNumCities == 0)
	{
		return 0;
	}

	for (CvCity* pCity = firstCity(&iLoop); pCity != NULL; pCity = nextCity(&iLoop))
	{
		iRebelSentiment += pCity->getRebelPercent();
	}

	return iRebelSentiment / iNumCities;
}

void CvPlayer::doUnitsRebelSentiment()
{
	int iLoop;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		int iRebelUnit = pLoopUnit->getRebelSentiment();
		if (pLoopUnit->getUnitInfo().isFound())
		{
			CvPlot* pPlot = pLoopUnit->plot();
			if (pPlot != NULL)
			{
				CvCity* pCity = pPlot->getPlotCity();
				if ( pCity != NULL )
				{
					if ( pCity->getOwnerINLINE() == getID())
					{
						int iRebelCity = pCity->getRebelPercent();
						int iNbCitizen = std::max(1, pCity->getPopulation());
						if ( iRebelUnit < iRebelCity)
						{
							int iChange = std::max(0, std::min(iNbCitizen*iRebelCity/100, 20));
							if ( iRebelUnit + iChange > iRebelCity )
							{
								pLoopUnit->setRebelSentiment(iRebelCity);
							}
							else
							{
								pLoopUnit->changeRebelSentiment(iChange);
							}
						}
					}
				}
				else
				{
					int iTurn = GC.getGameINLINE().getGameTurn();
					TeamTypes eTeamUnit = pLoopUnit->getTeam();
					TeamTypes eTeamPlot = pPlot->getTeam();
					if (eTeamUnit != NO_TEAM && (eTeamPlot != NO_TEAM && GET_TEAM(eTeamUnit).isFriendlyTerritory(eTeamPlot)))
					{
						int iTeamRebel = GET_TEAM(eTeamUnit).getRebelPercent();
						int iChange = std::max(0, std::min(2*iTeamRebel/40, 4));
						if ( iRebelUnit + iChange > iTeamRebel )
						{
							pLoopUnit->setRebelSentiment(iTeamRebel);
						}
						else
						{
							pLoopUnit->changeRebelSentiment(iChange);
						}
					}
				}
			}
		}
	}
}

void CvPlayer::calculateTotalYields(int aiYields[]) const
{
	CvCity* pLoopCity;
	int iTotal = 0;
	int iLoop = 0;

	for (int i=0;i<NUM_YIELD_TYPES;i++)
	{
		aiYields[i] = 0;
	}

	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		int aiTempYields[NUM_YIELD_TYPES];
		pLoopCity->calculateNetYields(aiTempYields);
		for (int i=0;i<NUM_YIELD_TYPES;i++)
		{
			aiYields[i] += aiTempYields[i];
		}
	}
}

bool CvPlayer::isCivic(CivicTypes eCivic) const
{
	int iI;
	for (iI = 0; iI < GC.getNumCivicOptionInfos(); iI++)
	{
		if (getCivic((CivicOptionTypes)iI) == eCivic)
		{
			return true;
		}
	}
	return false;
}

bool CvPlayer::canDoCivics(CivicTypes eCivic) const
{
	PROFILE_FUNC();

	if (eCivic == NO_CIVIC)
	{
		return true;
	}

	if (GC.getUSE_CAN_DO_CIVIC_CALLBACK())
	{
		CyArgsList argsList;
		argsList.add(getID());
		argsList.add(eCivic);
		long lResult=0;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "canDoCivic", argsList.makeFunctionArgs(), &lResult);
		if (lResult == 1)
		{
			return true;
		}
	}

	if (GC.getUSE_CANNOT_DO_CIVIC_CALLBACK())
	{
		CyArgsList argsList2; // XXX
		argsList2.add(getID());
		argsList2.add(eCivic);
		long lResult=0;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "cannotDoCivic", argsList2.makeFunctionArgs(), &lResult);
		if (lResult == 1)
		{
			return false;
		}
	}

	return true;
}

int CvPlayer::greatGeneralThreshold() const
{
	int iThreshold = ((GC.getDefineINT("GREAT_GENERALS_THRESHOLD") * std::max(0, (getGreatGeneralsThresholdModifier() + 100))) / 100);

	iThreshold *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGreatGeneralPercent();
	iThreshold /= 100;
	iThreshold *= GC.getEraInfo(GC.getGameINLINE().getStartEra()).getGreatGeneralPercent();
	iThreshold /= 100;

	return std::max(1, iThreshold);
}

int CvPlayer::greatAdmiralThreshold() const
{
	int iThreshold = ((GC.getDefineINT("GREAT_GENERALS_THRESHOLD") * std::max(0, (getGreatAdmiralsThresholdModifier() + 100))) / 100);

	iThreshold *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGreatGeneralPercent();
	iThreshold /= 100;
	iThreshold *= GC.getEraInfo(GC.getGameINLINE().getStartEra()).getGreatGeneralPercent();
	iThreshold /= 100;

	return std::max(1, iThreshold);
}

int CvPlayer::immigrationThreshold() const
{
	int iThreshold = ((GC.getDefineINT("IMMIGRATION_THRESHOLD") * std::max(0, (getImmigrationThresholdMultiplier()))) / 100);

	for (int iTrait = 0; iTrait < GC.getNumTraitInfos(); ++iTrait)
	{
		TraitTypes eTrait = (TraitTypes) iTrait;
		if (hasTrait(eTrait))
		{
			iThreshold *= 100 + GC.getTraitInfo(eTrait).getImmigrationThresholdModifier();
			iThreshold /= 100;
		}
	}

	iThreshold *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent();
	iThreshold /= 100;

	iThreshold *= GC.getEraInfo(GC.getGameINLINE().getStartEra()).getGrowthPercent();
	iThreshold /= 100;

	iThreshold *= 3;
	if (!isHuman())
	{
		iThreshold *= GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIGrowthPercent();
		iThreshold /= 100;
	}

	return std::max(1, iThreshold);
}

int CvPlayer::revolutionEuropeUnitThreshold() const
{
	int iThreshold = ((GC.getDefineINT("REVOLUTION_EUROPE_UNIT_THRESHOLD") * std::max(0, (getRevolutionEuropeUnitThresholdMultiplier()))) / 100);

	iThreshold *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getTrainPercent();
	iThreshold /= 100;

	iThreshold *= GC.getEraInfo(GC.getGameINLINE().getStartEra()).getTrainPercent();
	iThreshold /= 100;

	iThreshold *= GC.getHandicapInfo(getHandicapType()).getAITrainPercent();
	iThreshold /= 100;

	if (!isHuman())
	{
		iThreshold *= GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIKingUnitThresholdPercent();
		iThreshold /= 100;
	}

	return std::max(1, iThreshold);
}

CvPlot* CvPlayer::getStartingPlot() const
{
	return GC.getMapINLINE().plotSorenINLINE(m_iStartingX, m_iStartingY);
}


void CvPlayer::setStartingPlot(CvPlot* pNewValue, bool bUpdateStartDist)
{
	CvPlot* pOldStartingPlot;

	pOldStartingPlot = getStartingPlot();

	if (pOldStartingPlot != pNewValue)
	{
		if (pOldStartingPlot != NULL)
		{
			pOldStartingPlot->area()->changeNumStartingPlots(-1);

			if (bUpdateStartDist)
			{
				GC.getMapINLINE().updateMinOriginalStartDist(pOldStartingPlot->area());
			}
		}

		if (pNewValue == NULL)
		{
			m_iStartingX = INVALID_PLOT_COORD;
			m_iStartingY = INVALID_PLOT_COORD;
		}
		else
		{
			FAssertMsg(!isEuropean() || pNewValue->getEurope() != NO_EUROPE, "StartingPlot have to be Europe");
			m_iStartingX = pNewValue->getX_INLINE();
			m_iStartingY = pNewValue->getY_INLINE();

			getStartingPlot()->area()->changeNumStartingPlots(1);

			if (bUpdateStartDist)
			{
				GC.getMapINLINE().updateMinOriginalStartDist(getStartingPlot()->area());
			}
		}
	}
}


int CvPlayer::getTotalPopulation() const
{
	return m_iTotalPopulation;
}


int CvPlayer::getAveragePopulation() const
{
	if (getNumCities() == 0)
	{
		return 0;
	}

	return ((getTotalPopulation() / getNumCities()) + 1);
}


void CvPlayer::changeTotalPopulation(int iChange)
{
	changePopScore(-getTotalPopulation());

	m_iTotalPopulation += iChange;
	FAssert(getTotalPopulation() >= 0);

	changePopScore(getTotalPopulation());
}


long CvPlayer::getRealPopulation() const
{
	__int64 iTotalPopulation = 0;
	int iLoop = 0;

	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		iTotalPopulation += pLoopCity->getRealPopulation();
	}

	if (iTotalPopulation > MAX_INT)
	{
		iTotalPopulation = MAX_INT;
	}

	return ((long)(iTotalPopulation));
}


int CvPlayer::getTotalLand() const
{
	return m_iTotalLand;
}


void CvPlayer::changeTotalLand(int iChange)
{
	m_iTotalLand = (m_iTotalLand + iChange);
	FAssert(getTotalLand() >= 0);
}


int CvPlayer::getTotalLandScored() const
{
	return m_iTotalLandScored;
}


void CvPlayer::changeTotalLandScored(int iChange)
{
	if (iChange != 0)
	{
		changeLandScore(-getTotalLandScored());

		m_iTotalLandScored = (m_iTotalLandScored + iChange);
		FAssert(getTotalLandScored() >= 0);

		changeLandScore(getTotalLandScored());
	}
}


int CvPlayer::getGold() const
{
	return m_iGold;
}


void CvPlayer::setGold(int iNewValue)
{
	if (getGold() != iNewValue)
	{
		m_iGold = iNewValue;

		FAssert(getGold() >= 0);

		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(MiscButtons_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(SelectionButtons_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(GameData_DIRTY_BIT, true);
		}
	}
}

void CvPlayer::changeGold(int iChange)
{
	setGold(getGold() + iChange);
	if (iChange > 0)
	{
		GET_PLAYER(getID()).AI_changeTotalIncome(iChange);
	}
}

int CvPlayer::getIncomeAndExpendituresRecord() const
{
	return m_iIncomeAndExpendituresRecord;
}

void CvPlayer::setIncomeAndExpendituresRecord(int iNewValue)
{
	if (getIncomeAndExpendituresRecord() != iNewValue)
	{
		m_iIncomeAndExpendituresRecord = iNewValue;
	}
}	

unsigned short CvPlayer::getWorkImprovementPercent() const
{
	return m_usWorkImprovementPercent;
}

void CvPlayer::setWorkImprovementPercent(unsigned short usNewValue)
{
	if (getWorkImprovementPercent() != usNewValue)
	{
		m_usWorkImprovementPercent = usNewValue;
	}
}

unsigned short CvPlayer::getColonistIntoColoniesAmount() const
{
	return m_usColonistIntoColoniesAmount;
}

void CvPlayer::setColonistIntoColoniesAmount(unsigned short usNewValue)
{
	if (getColonistIntoColoniesAmount() != usNewValue)
	{
		m_usColonistIntoColoniesAmount = usNewValue;
	}
}

unsigned short CvPlayer::getLandForceInsideColoniesAmount() const
{
	return m_usLandForceInsideColoniesAmount;
}

void CvPlayer::setLandForceInsideColoniesAmount(unsigned short usNewValue)
{
	if (getLandForceInsideColoniesAmount() != usNewValue)
	{
		m_usLandForceInsideColoniesAmount = usNewValue;
	}
}

unsigned short CvPlayer::getLandForceOutsideColoniesAmount() const
{
	return m_usLandForceOutsideColoniesAmount;
}

void CvPlayer::setLandForceOutsideColoniesAmount(unsigned short usNewValue)
{
	if (getLandForceOutsideColoniesAmount() != usNewValue)
	{
		m_usLandForceOutsideColoniesAmount = usNewValue;
	}
}

unsigned short CvPlayer::getUnemployedAmount() const
{
	return m_usUnemployedAmount;
}

void CvPlayer::setUnemployedAmount(unsigned short usNewValue)
{
	if (getUnemployedAmount() != usNewValue)
	{
		m_usUnemployedAmount = usNewValue;
	}
}

unsigned short CvPlayer::getScoutAmount() const
{
	return m_usScoutAmount;
}

void CvPlayer::setScoutAmount(unsigned short usNewValue)
{
	if (getScoutAmount() != usNewValue)
	{
		m_usScoutAmount = usNewValue;
	}
}

unsigned short CvPlayer::getAgronomistAmount() const
{
	return m_usAgronomistAmount;
}

void CvPlayer::setAgronomistAmount(unsigned short usNewValue)
{
	if (getAgronomistAmount() != usNewValue)
	{
		m_usAgronomistAmount = usNewValue;
	}
}

unsigned short CvPlayer::getPioneerAmount() const
{
	return m_usPioneerAmount;
}

void CvPlayer::setPioneerAmount(unsigned short usNewValue)
{
	if (getPioneerAmount() != usNewValue)
	{
		m_usPioneerAmount = usNewValue;
	}
}

unsigned short CvPlayer::getMerchantShipAmount() const
{
	return m_usMerchantShipAmount;
}

void CvPlayer::setMerchantShipAmount(unsigned short usNewValue)
{
	if (getMerchantShipAmount() != usNewValue)
	{
		m_usMerchantShipAmount = usNewValue;
	}
}

unsigned short CvPlayer::getMilitaryShipAmount() const
{
	return m_usMilitaryShipAmount;
}

void CvPlayer::setMilitaryShipAmount(unsigned short usNewValue)
{
	if (getMilitaryShipAmount() != usNewValue)
	{
		m_usMilitaryShipAmount = usNewValue;
	}
}

int CvPlayer::getLandForceInsideColoniesExpenditure() const
{
	int iExpenditureByLandForceInsideColonies = GC.getEXPENDITURE_BY_LAND_FORCE_INSIDE_COLONIES();

	return getLandForceInsideColoniesAmount() * iExpenditureByLandForceInsideColonies;
}

int CvPlayer::getLandForceOutsideColoniesExpenditure() const
{
	int iExpenditureByLandForceOutsideColonies = GC.getEXPENDITURE_BY_LAND_FORCE_OUTSIDE_COLONIES();

	return getLandForceOutsideColoniesAmount() * iExpenditureByLandForceOutsideColonies;
}

int CvPlayer::getUnemployedExpenditure() const
{
	int iExpenditureByUnemployed = GC.getEXPENDITURE_BY_UNEMPLOYED();

	return getUnemployedAmount() * iExpenditureByUnemployed;
}

int CvPlayer::getScoutExpenditure() const
{
	int iExpenditureByScout = GC.getEXPENDITURE_BY_SCOUT();

	return getScoutAmount() * iExpenditureByScout;
}

int CvPlayer::getAgronomistExpenditure() const
{
	int iExpenditureByAgronomist = GC.getEXPENDITURE_BY_AGRONOMIST();

	return getAgronomistAmount() * iExpenditureByAgronomist;
}

int CvPlayer::getPioneerExpenditure() const
{
	int iExpenditureByPioneer = GC.getEXPENDITURE_BY_PIONEER();

	return getPioneerAmount() * iExpenditureByPioneer;
}

int CvPlayer::getMerchantShipExpenditure() const
{
	int iExpenditureByMerchantShip = GC.getEXPENDITURE_BY_MERCHANT_SHIP();

	return getMerchantShipAmount() * iExpenditureByMerchantShip;
}

int CvPlayer::getMilitaryShipExpenditure() const
{
	int iExpenditureByMilitaryShip = GC.getEXPENDITURE_BY_MILITARY_SHIP();

	return getMilitaryShipAmount() * iExpenditureByMilitaryShip;
}

void CvPlayer::processIncomeAndExpendituresRecord()
{
	if (!isEuropean() || getNumCities() == 0) 
	{
		return;
	}

	int iIncomeAndExpendituresRecord = getIncomeAndExpendituresRecord();
	int iGold = getGold();

	if (iIncomeAndExpendituresRecord < 0 && iGold + iIncomeAndExpendituresRecord < 0) 
	{
		// We don't have enough money so we record this value in the player's debt.
		changeEuropeLoan(-iIncomeAndExpendituresRecord);
	}
	else 
	{
		// If we have a positive amount of gold after the transaction we can directly apply the amount to the player gold.
		changeGold(iIncomeAndExpendituresRecord);
	}

	// We are updating income from the current turn
	updateIncome();

	// We are updating expenditures from the current turn
	updateExpenditures();

	int iNewIncomeAndExpendituresRecord = getTotalIncome() - getTotalExpenditures();

	setIncomeAndExpendituresRecord(iNewIncomeAndExpendituresRecord);
}

int CvPlayer::getTotalExpenditures() const
{
	int iTotalExpenditures = 0;

	iTotalExpenditures += getLandForceInsideColoniesExpenditure();

	iTotalExpenditures += getLandForceOutsideColoniesExpenditure();

	iTotalExpenditures += getUnemployedExpenditure();

	iTotalExpenditures += getScoutExpenditure();

	iTotalExpenditures += getAgronomistExpenditure();

	iTotalExpenditures += getPioneerExpenditure();

	iTotalExpenditures += getMerchantShipExpenditure();

	iTotalExpenditures += getMilitaryShipExpenditure();

	FAssertMsg( iTotalExpenditures >= 0, "iTotalTotalExpenditures should always be positive or nul" );

	return iTotalExpenditures;
}

void CvPlayer::updateExpenditures()
{
	int iLandForceInsideColoniesAmount = 0;
	int iLandForceOutsideColoniesAmount = 0;
	int iUnemployedAmount = 0;
	int iScoutAmount = 0;
	int iAgronomistAmount = 0;
	int iPioneerAmount = 0;
	int iMerchantShipAmount = 0;
	int iMilitaryShipAmount = 0;

	int iGameTurn = GC.getGameINLINE().getGameTurn();

	int iLoop, iNumWartShip = 0;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop)) 
	{
		if (pLoopUnit->isMilitary()) 
		{
			if (pLoopUnit->isInsideNationCity())
			{
				iLandForceInsideColoniesAmount++;
			}
			else 
			{
				iLandForceOutsideColoniesAmount++;
			}
		}

		if (pLoopUnit->isUnemployed(iGameTurn)) 
		{
			iUnemployedAmount++;
		}

		if (pLoopUnit->getProfession() == PROFESSION_SCOUT)
		{
			iScoutAmount++;
		}

		if (pLoopUnit->getUnitType() == UNIT_AGRONOME)
		{
			iAgronomistAmount++;
		}

		if (pLoopUnit->getProfession() == PROFESSION_PIONEER)
		{
			iPioneerAmount++;
		}

		if (pLoopUnit->getUnitInfo().isMechUnit() && pLoopUnit->hasCrew())
		{
			if (pLoopUnit->isOnlyDefensive())
			{
				iMerchantShipAmount++;
			}
			else
			{
				iMilitaryShipAmount++;
			}
		}
	}

	for (uint i = 0; i < m_aEuropeUnits.size(); ++i) 
	{
		CvUnit* pLoopUnit = m_aEuropeUnits[i];

		if (pLoopUnit->isMilitary()) 
		{
			iLandForceInsideColoniesAmount++;
		}

		if (pLoopUnit->isUnemployed(iGameTurn)) 
		{
			iUnemployedAmount++;
		}

		if (pLoopUnit->getProfession() == PROFESSION_SCOUT)
		{
			iScoutAmount++;
		}

		if (pLoopUnit->getUnitType() == UNIT_AGRONOME)
		{
			iAgronomistAmount++;
		}

		if (pLoopUnit->getProfession() == PROFESSION_PIONEER)
		{
			iPioneerAmount++;
		}
	}

	setLandForceInsideColoniesAmount(iLandForceInsideColoniesAmount);
	setLandForceOutsideColoniesAmount(iLandForceOutsideColoniesAmount);
	setUnemployedAmount(iUnemployedAmount);
	setScoutAmount(iScoutAmount);
	setAgronomistAmount(iAgronomistAmount);
	setPioneerAmount(iPioneerAmount);
	setMerchantShipAmount(iMerchantShipAmount);
	setMilitaryShipAmount(iMilitaryShipAmount);
}


int CvPlayer::getTotalIncome() const
{
	int iTotalIncome = 0;

	iTotalIncome += getColonistsIncome();

	iTotalIncome += getStatemenIncomeIncrease();

	FAssertMsg( iTotalIncome >= 0, "iTotalTotalExpenditures should always be positive or nul" );

	return iTotalIncome;
}

void CvPlayer::updateIncome()
{
	setColonistIntoColoniesAmount(calculateColonistIntoColoniesAmount());

	setWorkImprovementPercent(getRebelPercentForColonies());
}

int CvPlayer::calculateColonistIntoColoniesAmount() const
{
	int iLoop;
	int iNumPopulation = 0;

	for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop)) 
	{
		if (!pLoopCity->isDisorder()) 
		{
			iNumPopulation += pLoopCity->getPopulation();
		}
	}

	return iNumPopulation;
}

int CvPlayer::getColonistsIncome() const
{
	int iIncomeByPopulation = GC.getHandicapInfo(getHandicapType()).getIncomeByPopulation();

	return getColonistIntoColoniesAmount() * iIncomeByPopulation;
}

int CvPlayer::getStatemenIncomeIncrease() const
{
	return getColonistsIncome() * getWorkImprovementPercent() / 100;
}

int CvPlayer::getNewEra() const
{
	return GET_TEAM(getTeam()).getNewEra();
}

NewEraTypes CvPlayer::getNextNewEra() const
{
	return GET_TEAM(getTeam()).getNextNewEra();
}

int CvPlayer::getSelectQuantity() const
{
	return m_iSelectQuantity;
}
void CvPlayer::setSelectQuantity(int iNewValue)
{
	if (getSelectQuantity() != iNewValue)
	{
		m_iSelectQuantity = iNewValue;
	}
}

int CvPlayer::getEuropeWarehouseCapacity() const
{
	return m_iEuropeWarehouseCapacity;
}
void CvPlayer::setEuropeWarehouseCapacity(int iNewValue)
{
	if (getEuropeWarehouseCapacity() != iNewValue)
	{
		m_iEuropeWarehouseCapacity = iNewValue;
	}
}

int CvPlayer::getEuropeLoanPercent() const
{
	return m_iEuropeLoanPercent;
}

void CvPlayer::setEuropeLoanPercent(int iNewValue)
{
	if (getEuropeLoanPercent() != iNewValue)
	{
		m_iEuropeLoanPercent = iNewValue;
	}
}

void CvPlayer::doEuropeLandPercent()
{
	if (isHuman())
	{
		if (getEuropeLoan() > 0)
		{
			int iGold = getGold();
			int iTaxe;

			iTaxe = 5 + std::min(20, std::max(20*iGold/6000, 0));
			setEuropeLoanPercent(iTaxe);
		}
		else
		{
			setEuropeLoanPercent(0);
		}
	}
}

int CvPlayer::getEuropeLoan() const
{
	return m_iEuropeLoan;
}
void CvPlayer::setEuropeLoan(int iNewValue)
{
	if (getEuropeLoan() != iNewValue)
	{
		m_iEuropeLoan = iNewValue;
	}
}
void CvPlayer::changeEuropeLoan(int iChange)
{
	setEuropeLoan(getEuropeLoan() + iChange);
}
int CvPlayer::getParentMood() const
{
	return m_iParentMood;
}
void CvPlayer::setParentMood(int iNewValue)
{
	if (getParentMood() != iNewValue)
	{
		m_iParentMood = iNewValue;
	}
}
void CvPlayer::changeParentMood(int iChange)
{
	setParentMood(getParentMood() + iChange);
}
int CvPlayer::getTotalImmigrant() const
{
	return m_iTotalImmigrant;
}
void CvPlayer::changeTotalImmigrant(int iNewValue)
{
	m_iTotalImmigrant += iNewValue;
}
int CvPlayer::getAcceptImmigrant() const
{
	return m_iAcceptImmigrant;
}
void CvPlayer::changeAcceptImmigrant(int iNewValue)
{
	m_iAcceptImmigrant += iNewValue;
}
int CvPlayer::getLeaveImmigrant() const
{
	return m_iLeaveImmigrant;
}
void CvPlayer::changeLeaveImmigrant(int iNewValue)
{
	m_iLeaveImmigrant += iNewValue;
}

int CvPlayer::getDistanceToEuropeIA() const
{
	return m_iDistanceToEuropeIA;
}
void CvPlayer::setDistanceToEuropeIA(int iNewValue)
{
	if (getDistanceToEuropeIA() != iNewValue)
	{
		m_iDistanceToEuropeIA = iNewValue;
	}
}

// -----------------------------------------
//
//    START : DOANE Player Action functions
//
// -----------------------------------------
void CvPlayer::destroyCity(CvCity* pCity)
{
	FAssert(pCity != NULL);
	ejectCitizen(pCity);
	raze(pCity, true);
}

void CvPlayer::giveExpToUnit(CvUnit* pUnit, int iExp)
{
	FAssert(iExp != -1);
	pUnit->setExperience(iExp);
}

void CvPlayer::repayLoan(int iGold)
{
	FAssert(iGold >= 0);
	int iDiscount = iGold*2/10;//20% of discount if we pay Loan

	changeEuropeLoan(-iGold);
	changeGold(-(iGold - iDiscount));
}

void CvPlayer::arsenalManagement(CvCity* pCity, YieldTypes eYield)
{
	FAssert(pCity != NULL);

	pCity->setYieldProductionPercent(YIELD_SWORDS, 0);
	pCity->setYieldProductionPercent(YIELD_MUSKETS, 0);
	pCity->setYieldProductionPercent(YIELD_AMMUNITION, 0);
	pCity->setYieldProductionPercent(YIELD_CANNON, 0);


	pCity->setYieldProductionPercent(eYield, 100);

	if (getID() == GC.getGameINLINE().getActivePlayer()) 
	{
		gDLL->getInterfaceIFace()->setDirty(ResourceTable_DIRTY_BIT, true);
	}
}

void CvPlayer::buyMissingEquipement(CvUnit* pShipUnit)
{
	int iPrice = pShipUnit->getMissingEquipementPrice();
	if (getGold() >= iPrice) 
	{
		changeGold(-iPrice);
		pShipUnit->resupplyGroupShips(100, 100, true, false);
	}
}

void CvPlayer::unloadAllInEurope(CvUnit* pShipUnit)
{
	pShipUnit->unloadAll();
	pShipUnit->resupplyGroupShips(0, 0, false, false);
}

void CvPlayer::resupplyBuilderPack(CvUnit* pUnit, bool bLoad)
{
	FAssert(pUnit != NULL);
	bool isInEurope = (pUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);
	//Get the yield cost for the builder pack by looking for builder wagon costs
	short iTools = GC.getBuilderPackToolCost(); //Delayed cache
	short iLumber = GC.getBuilderPackLumberCost(); //Delayed cache
	unsigned char iModifier = GC.getBUILDER_PACK_YIELD_COST_PERCENT(); //From 0 to 255%
	if (pUnit == NULL)
	{
		return;
	}

	//Apply the global modifier to the costs:
	iTools *= iModifier;
	iTools /= 100;

	iLumber *= iModifier;
	iLumber /= 100;

	if (isInEurope)
	{
		bool hasEnoughYieldsToWarehouse = getEuropeWarehouseYield(YIELD_TOOLS) >= iTools && getEuropeWarehouseYield(YIELD_LUMBER) >= iLumber;
		int iToolCostPrice = getBuyPriceForYield(YIELD_TOOLS, iTools);
		int iLumberCostPrice = getBuyPriceForYield(YIELD_LUMBER, iLumber);
		int iBuilderBackCost = iToolCostPrice + iLumberCostPrice;
		if (bLoad)
		{
			if (hasEnoughYieldsToWarehouse)
			{
				changeEuropeWarehouseYield(YIELD_TOOLS, -iTools);
				changeEuropeWarehouseYield(YIELD_LUMBER, -iLumber);
			}
			else
			{
				changeGold(-iBuilderBackCost);
			}
		}
		else if (pUnit->isHaveFoundPack())
		{
			changeEuropeWarehouseYield(YIELD_TOOLS, iTools);
			changeEuropeWarehouseYield(YIELD_LUMBER, iLumber);
		}
		pUnit->setHaveFoundPack(bLoad);

		gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
	}
	CvPlot* pPlot = pUnit->plot();
	if (pPlot == NULL)
	{
		return;
	}
	CvCity* pCity = pPlot->getPlotCity();
	if (pCity != NULL)
	{
		if (bLoad)
		{ //Load a new builder pack into the ship
			pCity->changeYieldStored(YIELD_TOOLS, -iTools);
			pCity->changeYieldStored(YIELD_LUMBER, -iLumber);
			pUnit->setHaveFoundPack(true);
		}
		else  { //Unload the builder pack from a ship
			pCity->changeYieldStored(YIELD_TOOLS, iTools);
			pCity->changeYieldStored(YIELD_LUMBER, iLumber);
			pUnit->setHaveFoundPack(false);
		}
	}
	pUnit->reloadEntity();
}

void CvPlayer::processTradeProposition(CvUnit* pUnit, int iButtonClicked)
{
	GAMETRADE.processProposal(pUnit, iButtonClicked);
}
void CvPlayer::addSeawayFromUnit(CvUnit* pUnit)
{
	if (pUnit != NULL)
	{
		addSeaway(pUnit->plot(), L"");//Only other players have to create a seaway (Multi pb)
	}
}
void CvPlayer::addNewAgreement(CvUnit* pUnit, CvCity* pCitySource)
{
	FAssert(pUnit != NULL);
	FAssert(pCitySource != NULL);
	if (pUnit == NULL)
	{
		return;
	}
	CvCity* pCityDest = pUnit->plot()->getPlotCity();
	FAssert(pCityDest != NULL);

	int iID = addAgreement(pCitySource->getIDInfo(), pCityDest->getIDInfo());
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_NEW_AGREEMENT_PART1, pUnit->getID(), iID);
		gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
	}
}
void CvPlayer::addNewAgreementPart1(CvUnit* pUnit, int id, int iChoice)
{
	FAssert(pUnit != NULL);
	CvCity* pCity =  pUnit->plot()->getPlotCity();
	FAssert(pCity != NULL);

	CvPlayer& kOtherPlayer = GET_PLAYER(pCity->getOwner());
	CvAgreement* pAgreement = getAgreement(id);

	int iQuantity = (40+5*(kOtherPlayer.getRandomValues(0)%4))*iChoice;

	pAgreement->addYieldTrade(pCity->getNativeYieldProduce(), TRADE_IMPORT, iQuantity);
	pAgreement->setLevel(iChoice);
}
void CvPlayer::addNewAgreementPart2(CvUnit* pUnit, int iChoice, int iStartIncrease)
{
	FAssert(pUnit != NULL);
	CvCity* pCity =  pUnit->plot()->getPlotCity();
	FAssert(pCity != NULL);

	negociateAgreement(pUnit, pCity, iChoice, iStartIncrease);
}
void CvPlayer::changeCityAgremment(CvAgreement* pAgreement, CvCity* pDestCity)
{
	pAgreement->setSourceCity(pDestCity->getIDInfo());
}
void CvPlayer::chooseAgreement(CvUnit* pUnit, int iAgreementID, bool bAssign)
{
	if (pUnit == NULL)
	{
		return;
	}
	CvSelectionGroup* pGroup = pUnit->getGroup();
	pGroup->assignAgreement(iAgreementID, bAssign);

	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_AGREEMENTS, pUnit->getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
	}
}
void CvPlayer::chooseTradeRoute(CvUnit* pUnit, int iTradeRouteID, bool bAssign)
{
	if (pUnit == NULL)
	{
		return;
	}

	CvSelectionGroup* pGroup = pUnit->getGroup();
	CvTradeRoute* pTrade = getTradeRoute(iTradeRouteID);
	if (pTrade != NULL && pGroup != NULL)
	{
		pGroup->assignTradeRoute(iTradeRouteID, bAssign);
		const IDInfo kEurope(getID(), CvTradeRoute::EUROPE_CITY_ID);

		if (pTrade->getSourceCity() == kEurope || pTrade->getDestinationCity() == kEurope)
		{
			CvCity* pCity = ::getCity(pTrade->getSourceCity() == kEurope ? pTrade->getDestinationCity() : pTrade->getSourceCity());
			if (pCity != NULL)
			{
				CvSeaway* pSeaway = getSeawayNearCity(pCity);
				if (pSeaway != NULL)
				{
					pGroup->AI_setSeawayId(pSeaway->getID());
				}
			}
		}
	}


	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		if (gDLL->getInterfaceIFace()->isCityScreenUp())
		{
			gDLL->getInterfaceIFace()->setDirty(CityScreen_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(Domestic_Advisor_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(ResourceTable_DIRTY_BIT, true);
		}
		else
		{
			CyArgsList argsList;
			argsList.add(pGroup->getID());
			gDLL->getPythonIFace()->callFunction(PYScreensModule, "setLastGroupAssigned", argsList.makeFunctionArgs());
			gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
		}
	}
}

void CvPlayer::recruitCrew(CvTempUnit* pTempUnit, int iGold)
{
	if (pTempUnit == NULL)
	{
		return;
	}
	if (iGold >= 0)
	{
		pTempUnit->doRecruitCrew();
		changeGold(-iGold);
	}
	removeTempUnit(pTempUnit->getID());

	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen_DIRTY_BIT, true);
	}
}
void CvPlayer::recruitImmigrant(CvTempUnit* pTempUnit, int iGold)
{
	if (pTempUnit == NULL)
	{
		return;
	}
	if (iGold >= 0)
	{		
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			addTradeMessage(gDLL->getText(iGold > 0 ? "EUROPE_SCREEN_BUY_IMMIGRANT": "EUROPE_SCREEN_BUY_FREE_IMMIGRANT", pTempUnit->getUnitInfo().getDescription(), iGold));
		}
		pTempUnit->doRecruitImmigrant();
		changeAcceptImmigrant(1);
		if (iGold > 0)
		{
			changeGold(-iGold);
		}
	}
	else
	{
		removeTempUnit(pTempUnit->getID());
	}
	changeTotalImmigrant(1);
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{		
		gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
	}
}
void CvPlayer::buyUsedShip(CvTempUnit* pTempUnit, int iGold)
{
	if (pTempUnit == NULL)
	{
		return;
	}
	if (pTempUnit != NULL)
	{
		if (iGold >= 0)
		{
			pTempUnit->buyShip(iGold);
		}
		else
		{
			removeTempUnit(pTempUnit->getID());
		}
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen_DIRTY_BIT, true);
		}
	}
}
void CvPlayer::sellShip(CvUnit* pUnit, int iGold)
{
	if (pUnit == NULL)
	{
		return;
	}
	bool bHasPurchaseBid = (iGold <= pUnit->getSellPrice()/2 && iGold > 0);
	pUnit->setShipSellPrice(iGold);

	pUnit->setHasPurchaseBid(bHasPurchaseBid);
	pUnit->joinGroup(NULL);
	pUnit->getGroup()->setAutomateType(NO_AUTOMATE);
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen2_DIRTY_BIT, true);
	}
}

void CvPlayer::loadCrew(CvUnit* pShipUnit, int iID, bool bEurope)
{
	loadCrew(pShipUnit, bEurope ? getEuropeUnitById(iID) : getUnit(iID)); //Load crew
}


void CvPlayer::loadCrew(CvUnit* pShipUnit, CvUnit* pSailorUnit)
{
	int iExperience = pSailorUnit->getExperience();
	int iLevel = pSailorUnit->getLevel();
	int numPromotionInfos = GC.getNumPromotionInfos();

	pShipUnit->setLevel(std::max(0,iLevel));
	pShipUnit->setExperience(std::max(0,iExperience));

	for (int iPromotionIndex = 0; iPromotionIndex < numPromotionInfos; iPromotionIndex++)
	{
		PromotionTypes ePromotion = (PromotionTypes)iPromotionIndex;
		if (!GC.getPromotionInfo(ePromotion).isGraphicalOnly() && pSailorUnit->isHasPromotion(ePromotion))
		{
			pShipUnit->setHasRealPromotion(ePromotion, true);
		}
	}

	//Switch Exp  and lvl nav
	pShipUnit->setExperienceExploAndNavigation(pSailorUnit->getExperienceExploAndNavigation());

	pShipUnit->setLevelExploAndNavigation(pSailorUnit->getLevelExploAndNavigation());
	//end

	pShipUnit->setUnitSailorType(pSailorUnit->getUnitType());
	pShipUnit->setHasCrew(true);
	pShipUnit->setNumMap(pSailorUnit->getNumMap());

	if (pShipUnit->getUnitTravelState() != UNIT_TRAVEL_STATE_IN_EUROPE)
	{
		pShipUnit->finishMoves();
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->insertIntoSelectionList(pShipUnit, true, false);
		}
		pSailorUnit->kill(false);
		pShipUnit->testPromotionReady();
	}
	else
	{
		pSailorUnit->removeFromMap();
		pSailorUnit->updateOwnerCache(-1);
		killUnitFromEurope(pSailorUnit);

		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen_DIRTY_BIT, true);
		}
	}
	FAssert(checkPopulation());
}

void CvPlayer::repairShip(CvUnit* pUnit, bool bIncludeGroup)
{
	FAssert(pUnit != NULL);

	int iCost = pUnit->getReparationCostInEurope(bIncludeGroup);
	changeGold(-iCost);

	std::vector<CvUnit*> apShipsToHeal;
	CvSelectionGroup* pGroup = pUnit->getGroup();
	CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();

	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pGroup->nextUnitNode(pUnitNode);

		if (bIncludeGroup || pLoopUnit->getID() == pUnit->getID())
		{
			if (pLoopUnit->isHurt() && !pLoopUnit->isInEuropeDrydock())
			{
				apShipsToHeal.push_back(pLoopUnit);
			}
		}
	}

	for (uint index = 0; index < apShipsToHeal.size(); ++index)
	{
		CvUnit* pLoopUnit = apShipsToHeal[index];
		pLoopUnit->setInEuropeDrydock(true); //Being repaired...
		pLoopUnit->joinGroup(NULL); //Exit from any group
	}

	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
		gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen_DIRTY_BIT, true);
	}
}

void CvPlayer::buyNewShipInEurope(UnitTypes eUnit, int iTurn, int iGold)
{
	FAssert(eUnit != NO_UNIT);
	UnitClassTypes eUnitClassType = (UnitClassTypes)GC.getUnitInfo(eUnit).getUnitClassType();

	changeUnitTurnRemaining(eUnitClassType, iTurn);
	changeGold(-iGold);

	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen2_DIRTY_BIT, true);
	}
}

void CvPlayer::resupplyEuropeUnit(CvUnit* pUnit)
{
	FAssert(pUnit != NULL);
	ProfessionTypes eProfession = pUnit->getProfession();
	if (NO_PROFESSION != eProfession)
	{
		CvProfessionInfo& kProfessionInfo = GC.getProfessionInfo(eProfession);
		int iMunition = pUnit->getMunition();
		int iMunitionInEurope = getEuropeWarehouseYield(YIELD_AMMUNITION);

		int iTransfertMunititon = std::min(kProfessionInfo.getMaxMunition()-iMunition, iMunitionInEurope);
		pUnit->changeMunition(iTransfertMunititon);
		changeEuropeWarehouseYield(YIELD_AMMUNITION, -iTransfertMunititon);
	}
}
void CvPlayer::shipJoinGroup(CvUnit* pUnit, CvUnit* pHeadUnit)
{
	FAssert(pUnit != NULL);
	FAssert(pHeadUnit != NULL);
	if (pUnit->getUnitTravelState() != pHeadUnit->getUnitTravelState())
	{
		return;
	}
	CvSelectionGroup* pGroup = pHeadUnit->getGroup();
	if (pGroup != NULL)
	{
		CvPlot* pPlot = pHeadUnit->plot();
		if (pPlot != NULL && !pUnit->atPlot(pPlot))
		{
			pUnit->setXY(pPlot->getX(), pPlot->getY());
		}
		pUnit->joinGroup(pGroup);
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
		}
	}
}
void CvPlayer::ejectUnitToGroup(CvUnit* pUnit, CvUnit* pCurrentUnit)
{
	FAssert(pUnit != NULL);
	FAssert(pCurrentUnit != NULL);
	CvSelectionGroup* pGroup = pUnit->getGroup();
	CvSelectionGroup* pCurrentGroup = pCurrentUnit->getGroup();
	if (pCurrentGroup != NULL)
	{
		if (pCurrentUnit == pUnit)
		{
			pCurrentUnit->joinGroup(NULL);
		}
		else
		{
			pCurrentUnit->joinGroup(pGroup);
		}
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
		}
	}
}
void CvPlayer::transfertMap(CvUnit* pSender, CvUnit* pReceiver)
{
	FAssert(pSender != NULL && pReceiver != NULL);
	pSender->transfertMapTo(pReceiver);
}
void CvPlayer::removeResource(CvUnit* pUnit)
{
	FAssert(pUnit != NULL);
	CvPlot* pPlot = pUnit->plot();

	pPlot->setBonusType(NO_BONUS); //Delete bonus
	pPlot->setImprovementLevel(0); //Reset improvement levels
	pUnit->finishMoves(); //Finish moves
	pUnit->reloadEntity();
}

void CvPlayer::robUnitCrew(CvUnit* pUnit, int iGold)
{
	FAssert(pUnit != NULL);
	if (iGold >= 0)
	{
		changeGold(-iGold);
	}
	else
	{
		pUnit->removeCapturedCrew();
	}
}

void CvPlayer::robUnitPassengers(CvUnit* pUnit, CvUnit* pAttackUnit, int iGold)
{
	FAssert(pUnit != NULL);
	if (pAttackUnit == NULL)
	{
		if (iGold >= 0)
		{
			changeGold(-iGold);
			pUnit->setOriginalOwner(NO_PLAYER);
		}
		else
		{
			if (pUnit->getOriginalOwner() == getID() && pUnit->getProfession() == PROFESSION_PRISONER)
			{
				pUnit->setProfession(PROFESSION_COLONIST, true);
				pUnit->setOriginalOwner(NO_PLAYER);
			}
			else
			{
				pUnit->setProfession(PROFESSION_PRISONER, true);
			}
			if (pUnit->getTransportUnit() != NULL)
			{
				pUnit->getTransportUnit()->reloadEntity();
			}
		}
	}
	else
	{
		pUnit->setXY(pAttackUnit->getX(), pAttackUnit->getY());
		pUnit->setTransportUnit(pAttackUnit);
		if (iGold >= 0)
		{
			changeGold(-iGold);
			pUnit->setOriginalOwner(NO_PLAYER);
			pUnit->setProposePrice(0);
		}
		else
		{ //Unload unit to rob ship
			if (pUnit->getOriginalOwner() == NO_PLAYER && pUnit->getProfession() != PROFESSION_PRISONER)
			{
				pUnit->setOriginalOwner(getID());
			}
		}
	}
}

void CvPlayer::robUnitCargo(CvUnit* pUnit, YieldTypes eYield, int iQuantity)
{
	FAssert(pUnit != NULL);
	FAssert(eYield != NO_YIELD);

	pUnit->changeNewCargoYield(eYield, iQuantity);
}

void CvPlayer::robUnitEnd(CvUnit* pUnit, CvUnit* pAttackUnit)
{
	FAssert(pUnit != NULL);
	CvUnit* pAttUnit = pUnit->getCombatUnit();
	PlayerTypes eOriginalOwner = pUnit->getOriginalOwner();
	if (pAttUnit != NULL)
	{
		pUnit->reloadEntity();
		CvPlot* pPlot = pUnit->plot();
		if (pAttackUnit != NULL)
		{
			if (eOriginalOwner != NO_PLAYER)
			{
				CvUnit* pNewUnit = GET_PLAYER(eOriginalOwner).initUnit(pUnit->getUnitType(), pUnit->getProfession(), pUnit->getX_INLINE(), pUnit->getY_INLINE(), pUnit->AI_getUnitAIType(), pUnit->getFacingDirection(false), pUnit->getYieldStored());
				if (pNewUnit != NULL)
				{
					CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
					while(pUnitNode != NULL)
					{
						CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
						pUnitNode = pPlot->nextUnitNode(pUnitNode);
						CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
						if (pTranportUnit == pUnit && pCargoUnit->getProfession() == PROFESSION_PRISONER)
						{
							if (pCargoUnit->getOriginalOwner() == eOriginalOwner)
							{
								pCargoUnit->setProfession(PROFESSION_COLONIST, true);
								pCargoUnit->setOriginalOwner(NO_PLAYER);
							}
						}
					}
					pUnit->setOriginalOwner(NO_PLAYER);
					pUnit->setCombatUnit(NULL);
					pNewUnit->convert(pUnit, true);
					pNewUnit->setTempInvisible(true);
					CvWString szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_ACCOSTED", pNewUnit->getNameOrProfessionKey(), pAttUnit->getNameOrProfessionKey());
					gDLL->getInterfaceIFace()->addMessage(eOriginalOwner, true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
					GET_PLAYER(eOriginalOwner).kingHelpAIToDefendShips(NO_UNIT, 40);
				}
			}
		}
		else
		{
			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->insertIntoSelectionList(pUnit, true, false);
			}
			pUnit->setCombatUnit(NULL);
			pUnit->setTempInvisible(false);
			if (eOriginalOwner != NO_PLAYER)
			{
				CvWString szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_CAPTURED_UNIT_NO_YIELD", pAttUnit->getNameOrProfessionKey(), pUnit->getNameOrProfessionKey());
				gDLL->getInterfaceIFace()->addMessage(eOriginalOwner, true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
				GET_PLAYER(eOriginalOwner).kingHelpAIToDefendShips(pUnit->getUnitType(), 100);
			}
		}
		pAttUnit->setTempInvisibleAllPlot(false);
	}
}
void CvPlayer::manageHospitals(CvCity* pCity, int iAmount)
{
	CvHospitals* pHospitals = getHospitals();
	pHospitals->transferToCity(pCity, iAmount);
}
void CvPlayer::manageHospitalsBis(CvCity* pCity, int iConsumption, bool bAuto)
{
	CvHospitals* pHospitals = getHospitals();
	bool bChange = (bAuto !=  pCity->isAutoHospital() || iConsumption != 0);
	pHospitals->changeConsumption(iConsumption);
	pCity->setAutoHospital(bAuto);
	if (bChange && getID() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_HOSPITAL, pCity->getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
	}
}

void CvPlayer::launchPlayerActionPopup(ButtonPopupTypes eButtonPopup, int iData1, int iData2)
{
	FAssert(eButtonPopup >= 0 && eButtonPopup < NUM_BUTTONPOPUP_TYPES);
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(eButtonPopup, iData1, iData2, getLocalPlayerActionPopupData3());
		gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
	}
}

int CvPlayer::getLocalPlayerActionPopupData3()
{
	return m_iLocalPlayerActionPopupData3;
}
void CvPlayer::setLocalPlayerActionPopupData3(int iData3)
{
	if (getLocalPlayerActionPopupData3() != iData3)
	{
		m_iLocalPlayerActionPopupData3 = iData3;
	}
}

void CvPlayer::processPurchaseBid(CvUnit* pUnit, int iGold)
{
	if (iGold >= 0)
	{
		pUnit->sellUnit(iGold);
	}
	else
	{
		pUnit->setHasPurchaseBid(false);
	}
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen_DIRTY_BIT, true);
	}
}

void CvPlayer::trainCrewInEurope(CvTempUnit* pTempUnit, int iMode)
{
	if (iMode > 0)
	{
		pTempUnit->doRecruitCrew(iMode);
	}
	removeTempUnit(pTempUnit->getID());

	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen_DIRTY_BIT, true);
	}
}

void CvPlayer::enrolUnitType(UnitClassTypes eUnitClassType, bool bIncrease)
{
	changeEnrolUnitType(eUnitClassType, bIncrease);
	gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
}

void CvPlayer::makeTheFirstSeaway(CvUnit* pUnit, CvCity* pCity, int iButtonClicked)
{
	FAssert(pUnit != NULL);
	CvPlot* pPlot = pUnit->plot();
	CvWString szBuffer = gDLL->getText("TXT_KEY_MY_FIRST_SEAWAY");
	if (iButtonClicked == 1)
	{
		szBuffer = gDLL->getText("TXT_KEY_NEW_SEAWAY_NEAR_COLONY", pCity->getNameKey());
	}
	if (iButtonClicked == 3)
	{
		pPlot = pCity->getNearestOceanPlot(false);
		szBuffer = gDLL->getText("TXT_KEY_NEW_SEAWAY_NEAR_COLONY", pCity->getNameKey());
	}
	addSeaway(pPlot, szBuffer);
	gDLL->sendDoCommand(pUnit->getID(), COMMAND_WAKE, 1, -1, false);
}

void CvPlayer::moveSeawayRank(int iIndex, int iState)
{
	switch(iState) 
	{
	case 1:
		sendUpNodeSeaway(iIndex);
		break;
	case 2:
		sendDownNodeSeaway(iIndex);
		break;
	default:
		FAssert(false);
		return;
	}
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
		gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
	}
}


void CvPlayer::kingGiveAgronomist(BonusTypes eBonus)
{	
	CvWString szText;
	CvUnit* pUnit = NULL;
	UnitTypes eUnit = UNIT_AGRONOME;
	CvCity* pIdealCity = NULL;
	int iBestValue = 0; 
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		int iValue = pLoopCity->getBonusLandsValue(eBonus);
		if (iValue > iBestValue)
		{
			iBestValue = iValue;
			pIdealCity = pLoopCity;
		}
	}
	if (pIdealCity !=  NULL)
	{
		if (getUnitGivedByKing(eUnit) == 0)
		{	
			if (GC.getHandicapInfo(getHandicapType()).hasFreeUnitsInCity())
			{

				pUnit = initUnit(eUnit, (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession(), pIdealCity->getX_INLINE(), pIdealCity->getY_INLINE());

				//Initialize text
				szText = gDLL->getText("AI_DIPLO_KING_GIVE_AGRONOMIST_IN_CITY", pIdealCity->getName().GetCString());				
			}
			else
			{
				pUnit = initEuropeUnit(eUnit, NO_UNITAI, NO_DIRECTION, false);

				//Initialize text
				szText = gDLL->getText("AI_DIPLO_KING_GIVE_AGRONOMIST_IN_EUROPE");
			}
			if (pUnit != NULL)
			{
				changeUnitGivedByKing(eUnit, 1);
				if (isHuman())
				{
					CvDiploParameters* pDiplo = new CvDiploParameters(getParent());
					pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLO_KING_GIVE_AGRONOMIST"));
					pDiplo->addDiploCommentVariable(GC.getBonusInfo(eBonus).getDescription());
					pDiplo->addDiploCommentVariable(szText);
					pDiplo->setAIContact(true);
					gDLL->beginDiplomacy(pDiplo, getID());
				}
				else
				{
					pUnit->setDestinationCity(pIdealCity->getID());
				}
			}
			// We give to all our teammates an agronomist in order to be fair.
			CvTeam& kNewTeam = GET_TEAM(getTeam());
			for (int iI = 0; iI < kNewTeam.getPlayerCount(); iI++)
			{
				CvPlayer& kLoopPlayer = GET_PLAYER(kNewTeam.getPlayer(iI));
				if (kLoopPlayer.getID() != getID() && kLoopPlayer.isAlive())
				{
					CvUnit* pUnit = kLoopPlayer.initEuropeUnit(eUnit, NO_UNITAI, NO_DIRECTION, false);
					kLoopPlayer.changeUnitGivedByKing(eUnit, 1);
					if (kLoopPlayer.isHuman())
					{
						CvDiploParameters* pDiplo = new CvDiploParameters(kLoopPlayer.getParent());
						pDiplo->setDiploComment((DiploCommentTypes) GC.getInfoTypeForString("AI_DIPLO_KING_GIVE_AGRONOMIST_THANKS_TO_TEAMMATE"));
						pDiplo->addDiploCommentVariable(getName());
						pDiplo->setAIContact(true);
						gDLL->beginDiplomacy(pDiplo, kLoopPlayer.getID());
					}
					else
					{
						CvCity* pLoopCity = kLoopPlayer.firstCity(&iLoop);
						if (pLoopCity != NULL) {
							pUnit->setDestinationCity(pIdealCity->getID());
						}
					}
				}
			}
		}
		else if (isHadTutorialDoane(1) && isHuman())
		{
			CvDiploParameters* pDiplo = new CvDiploParameters(getParent());
			pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLO_KING_ADVICE_FOR_AGRONOMISTS"));
			pDiplo->addDiploCommentVariable(GC.getBonusInfo(eBonus).getDescription());
			pDiplo->addDiploCommentVariable(pIdealCity->getName().GetCString());
			pDiplo->setAIContact(true);
			gDLL->beginDiplomacy(pDiplo, getID());
		}
	}
}

void CvPlayer::changeDisplaySeedling()
{
	setDisplaySeedling(!isDisplaySeedling());
	gDLL->getInterfaceIFace()->setDirty(GlobeInfo_DIRTY_BIT, true);
}

void CvPlayer::swapSailors(CvUnit* pSourceUnit, CvUnit* pDestUnit)
{
	FAssert(pSourceUnit != NULL);
	FAssert(pDestUnit != NULL);

	if (pSourceUnit != NULL && pDestUnit != NULL)
	{
		pSourceUnit->swapCrew(pDestUnit);
	}
}
CvTradeRoute* CvPlayer::createTradeRoute(int iSourceID, int iDestinationID)
{
	FAssert(iDestinationID != iSourceID);

	const IDInfo kSource(getID(), iSourceID);
	const IDInfo kDestination(getID(), iDestinationID);
	return addTradeRoute(kSource, kDestination);
}
void CvPlayer::changeTradeType(CvTradeRoute* pTradeRoute, TradeTypes eTrade, YieldTypes eYield)
{
	FAssert(pTradeRoute != NULL);
	pTradeRoute->setTradeType(eYield, eTrade);
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		CyArgsList argsList;
		argsList.add(eYield);
		argsList.add(eTrade);
		gDLL->getPythonIFace()->callFunction(PYScreensModule, "setSelectedYield", argsList.makeFunctionArgs());
	}
}
void CvPlayer::setTradeRouteRadioOption(CvTradeRoute* pTradeRoute, TradeRouteRadioTypes eRadio, YieldTypes eYield)
{
	FAssert(pTradeRoute != NULL);
	pTradeRoute->setRadioOption(eYield, eRadio, true);
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);

		if (pTradeRoute->isRadioOptionWithQuantity(eRadio))
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_TRADE_ROUTE_RADIO_QUANTITY, pTradeRoute->getID(), eYield, eRadio);
			gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
		}
	}
}
void CvPlayer::setTradeRouteRadioOptionQuantity(CvTradeRoute* pTradeRoute, YieldTypes eYield, int iQuantity)
{
	FAssert(pTradeRoute != NULL);
	pTradeRoute->setRadioOptionQuantity(eYield, iQuantity);
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
	}
}
void CvPlayer::setTradeRouteEuropeanRadioOption(CvTradeRoute* pTradeRoute, TradeRouteRadioEuropeanTypes eRadio, YieldTypes eYield)
{
	FAssert(pTradeRoute != NULL);
	pTradeRoute->setEuropeanRadioOption(eYield, eRadio, true);
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);

		switch(eRadio)
		{
		case BUY_QUANTITY: 
		case BUY_UNTIL_QUANTITY_REACHED: 
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_TRADE_ROUTE_RADIO_EUROPEAN_QUANTITY, pTradeRoute->getID(), eYield, eRadio);
				gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
			}
			break;
		}
	}
}
void CvPlayer::setTradeRouteRadioEuropeanOptionQuantity(YieldTypes eYield, TradeRouteRadioEuropeanTypes eRadio, int iQuantity)
{
	CvTradeRoute* pTradeRoute = getTradeRoute(getCurrentTradeRouteId());
	FAssert(pTradeRoute != NULL);
	if (pTradeRoute != NULL)
	{
		pTradeRoute->setEuropeanRadioOptionQuantity(eYield, iQuantity);
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
		}
	}
}
void CvPlayer::setTradeRouteCheckBoxOption(CvTradeRoute* pTradeRoute, TradeRouteCheckBoxTypes eCheckBox, YieldTypes eYield, bool bActivate)
{
	FAssert(pTradeRoute != NULL);
	pTradeRoute->setCheckBoxOption(eYield, eCheckBox, bActivate);
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);

		if (bActivate && pTradeRoute->isCheckBoxOptionWithQuantity(eCheckBox))
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_TRADE_ROUTE_CHECK_BOX_QUANTITY, pTradeRoute->getID(), eYield, eCheckBox);
			gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
		}
	}
}

void CvPlayer::setTradeRouteCheckBoxOptionQuantity(TradeRouteCheckBoxTypes eCheckBox, YieldTypes eYield, int iQuantity)
{
	CvTradeRoute* pTradeRoute = getTradeRoute(getCurrentTradeRouteId());
	FAssert(pTradeRoute != NULL);
	if (pTradeRoute != NULL)
	{
		pTradeRoute->setCheckBoxOptionQuantity(eYield, eCheckBox, iQuantity);
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
		}
	}
}

void CvPlayer::setDestinationCityToUnit(CvUnit* pUnit, CvCity* pCity)
{
	FAssert(pUnit != NULL);

	if (pUnit != NULL)
	{
		pUnit->setDestinationCity(pCity != NULL ? pCity->getID() : -1);
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
		}
	}
}
void CvPlayer::moveTransportToTransport(CvUnit* pSourceUnit, CvUnit* pDestinationUnit)
{
	FAssert(pSourceUnit != NULL);
	FAssert(pDestinationUnit != NULL);

	if (pSourceUnit != NULL && pDestinationUnit != NULL)
	{
		CvSelectionGroup* pSourceGroup = pSourceUnit->getGroup();
		CvSelectionGroup* pDestinationGroup = pDestinationUnit->getGroup();
		if (pSourceGroup != NULL && pDestinationGroup != pSourceGroup)
		{		
			if (pSourceUnit->getUnitInfo().isMechUnit())
			{
				CvPlot* pSourcePlot = pSourceUnit->plot();
				CvPlot* pDestinationPlot = pDestinationUnit->plot();
				if (pSourcePlot != NULL && pDestinationPlot != NULL) 
				{
					if (pSourcePlot != pDestinationPlot) 
					{
						if (pSourceUnit->getUnitTravelState() == pDestinationUnit->getUnitTravelState() && pSourceUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
						{
							pSourceGroup->setXY(pDestinationPlot->getX(), pDestinationPlot->getY());
							pSourcePlot = pSourceUnit->plot();
						}
					}

					if (pSourcePlot == pDestinationPlot && pSourceUnit->getUnitTravelState() == pDestinationUnit->getUnitTravelState() && pSourceUnit->getUnitTravelTimer() == pDestinationUnit->getUnitTravelTimer()) //Create canDirectlyMerge() function
					{ 
						pSourceGroup->mergeIntoGroup(pDestinationGroup);

						if (getID() == GC.getGameINLINE().getActivePlayer()) 
						{
							gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
							gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
						}
					} 
					else if (pSourceGroup->isAutomated() && pDestinationGroup->isAutomated())
					{
						//We have to precise which group will wait
						if (getID() == GC.getGameINLINE().getActivePlayer())
						{
							gDLL->getInterfaceIFace()->clearSelectionList();
						}
						CvSelectionGroup* bestGroup = bestGroupForWaiting(pSourceGroup, pDestinationGroup);
						if (bestGroup != NULL)
						{
							CvSelectionGroup* pOtherGroup = bestGroup == pSourceGroup ? pDestinationGroup : pSourceGroup;
							bestGroup->AI_setGroupIdForMerging(pOtherGroup->getID());
							bestGroup->AI_setShouldWaitForMerging(true);
							if (pOtherGroup->AI_getGroupIdForMerging() == -1)
							{
								pOtherGroup->AI_setGroupIdForMerging(bestGroup->getID());
							}
						} 
						if (getID() == GC.getGameINLINE().getActivePlayer())
						{
							gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
						}
					}
				}
			}
		}		
	}
}

void CvPlayer::selectSeawayForGroup(CvSelectionGroup* pGroup, int iSeawayId)
{
	FAssert(pGroup != NULL);
	pGroup->AI_setSeawayId(iSeawayId);
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
	}
}

void CvPlayer::setTradeRouteCheckBoxOptionGroup(CvSelectionGroup* pGroup, TradeRouteCheckBoxGroupsTypes eType, bool bNewValue)
{
	FAssert(pGroup != NULL);
	switch(eType)
	{
	case STOP_CONVOY:
		pGroup->AI_setShouldStopConvoy(bNewValue);
		break;
	case RESUPPLY_AUTO:
		//pGroup->AI_setShouldResupplyAuto(bNewValue);
		break;
	case AUTO_HEAL:
		pGroup->AI_setShouldAutoHeal(bNewValue);
		break;
	}
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
	}
}

void CvPlayer::setStartingSpecialist(UnitTypes eUnit, CvUnit* pTransport, CvUnit* pUnitToReplace)
{
	FAssert(eUnit != NO_UNIT);
	int iLoop;
	if (pTransport == NULL) 
	{
		pTransport = firstUnit(&iLoop);
	}

	if (pTransport == NULL)
	{
		return;
	}

	CvPlot* pPlot = pTransport->plot();
	if (pPlot == NULL) 
	{
		return;
	}

	if (pUnitToReplace != NULL)
	{
		ProfessionTypes eProfession = pUnitToReplace->getProfession();
		pUnitToReplace->kill(false);

		CvUnit* pNewUnit = initUnit(eUnit, eProfession, INVALID_PLOT_COORD, INVALID_PLOT_COORD);

		pNewUnit->addToMap(pPlot->getX_INLINE(), pPlot->getY_INLINE());
		pNewUnit->setTransportUnit(pTransport);

		if (getID() == GC.getGameINLINE().getActivePlayer()) 
		{
			gDLL->getInterfaceIFace()->setDirty(DawnOfMan_DIRTY_BIT, true);
		}
	}
}

CvTempUnit* CvPlayer::useRelationPointsForImmigration(UnitTypes eUnit)
{
	short sImmigrationRelationPointRequired = getImmigrationRelationPointRequired(eUnit);
	changeImmigrationRelationPoints(-sImmigrationRelationPointRequired);
	CvTempUnit* pTempUnit = addImmigrantProposalInEurope(IMMIGRATION_CONTRACT, eUnit);

	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
	}
	return pTempUnit;
}
void CvPlayer::changeMinGoldToConserveForTradeRoute(int tradeRouteId, int iNewValue)
{
	CvTradeRoute* pTradeRoute = getTradeRoute(tradeRouteId);
	if (pTradeRoute == NULL) 
	{
		return;
	}

	pTradeRoute->setGoldAmountToAlwaysConserve(iNewValue);
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
	}
}

void CvPlayer::permuteDebugMode()
{
	gDLL->setChtLvl(!gDLL->getChtLvl());
	GC.getGameINLINE().toggleDebugMode();
}

void CvPlayer::sendPlayerOption(PlayerOptionTypes ePlayerOption, bool bNewValue)
{	
	gDLL->sendPlayerOption(ePlayerOption, bNewValue);
}

// -----------------------------------------
//
//    END : DOANE Player Action functions
//
// -----------------------------------------
CvSelectionGroup* CvPlayer::bestGroupForWaiting(CvSelectionGroup* pSourceGroup, CvSelectionGroup* pDestinationGroup)
{
	bool bSourceShouldWaitForMerging = pSourceGroup->AI_getGroupIdForMerging() >= 0;
	bool bDestinationShouldWaitForMerging = pDestinationGroup->AI_getGroupIdForMerging() >= 0;

	if (bSourceShouldWaitForMerging && bDestinationShouldWaitForMerging)
	{
		return NULL;
	}
	else if (bSourceShouldWaitForMerging) 
	{
		return pDestinationGroup;
	}
	else if (bDestinationShouldWaitForMerging) 
	{
		return pSourceGroup;
	}

	int iSourceTurnLeft = pSourceGroup->getTurnLeftToDestination();
	int iDestinationTurnLeft = pDestinationGroup->getTurnLeftToDestination();

	if (pSourceGroup->getDestination() == pDestinationGroup->getDestination()) 
	{
		return iSourceTurnLeft > iDestinationTurnLeft ? pDestinationGroup : pSourceGroup;
	} 
	else 
	{
		return iSourceTurnLeft < iDestinationTurnLeft ? pDestinationGroup : pSourceGroup;
	}
}

void CvPlayer::kingHelpAIToDefendShips(UnitTypes eUnit, int iProbaWarShip)
{
	if (getParent() == NO_PLAYER || isHuman() || getNewEra() == -1) 
	{
		return;
	}
	int iLoop, iNumWartShip = 0;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop)) 
	{
		if (pLoopUnit->getUnitInfo().isMechUnit() && pLoopUnit->getUnitInfo().getMaxCannon() > 0) 
		{
			iNumWartShip++;
		}
	}

	bool bValid = true;
	if (eUnit != NO_UNIT)
	{
		CvUnitInfo& kUnitInfo = GC.getUnitInfo(eUnit);
		if (!kUnitInfo.isMechUnit())
		{
			return;
		}

		for (uint i = 0; i < m_aEuropeUnits.size(); ++i) 
		{
			CvUnit* pLoopUnit = m_aEuropeUnits[i];
			if (pLoopUnit->getUnitInfo().isMechUnit() && pLoopUnit->getUnitInfo().getMaxCannon() > 0) 
			{
				iNumWartShip++;
			}
		}

		if (kUnitInfo.getMaxCannon() > 0)
		{
			CvUnit* pUnit = buyEuropeUnit(eUnit, 0);
			if (pUnit == NULL) 
			{
				return;
			}

			bValid = false;//We don't buy a new war ship because the attacked ship was a war ship.
			UnitTypes eCrewUnit = NO_UNIT;
			int iRandomUnitType = GC.getGameINLINE().getSorenRandNum(100, "random eCrewUnit");
			if (iRandomUnitType < 80) 
			{
				eCrewUnit = UNIT_CRIMINAL;
			}
			else if (iRandomUnitType >= 95) 
			{
				eCrewUnit = UNIT_SAILOR_EXPERT;
			}
			else 
			{
				eCrewUnit = UNIT_COLONIST;
			}

			if (eCrewUnit != NO_UNIT) 
			{
				pUnit->setUnitSailorType(eCrewUnit, true);
				pUnit->addSailorPromotionsWithCoef(120, 140);
			}
		} 
		else 
		{
			CvUnit* pUnit = buyEuropeUnit(eUnit, 0);
			if (pUnit == NULL) 
			{
				return;
			}
			pUnit->addSailorPromotionsWithCoef(100, 0);
		}
	}

	if (bValid)
	{
		if (GC.getGameINLINE().getSorenRandNum(100, "random iProbaBuyWarShip") < iProbaWarShip) 
		{
			if (iNumWartShip < getNewEra()+2) 
			{
				UnitTypes eWarShip = NO_UNIT;
				switch(getNewEra()) 
				{
				case 0:
					eWarShip = UNIT_MILITARY_ESCORT;
					break;
				case 1:
					eWarShip = UNIT_MILITARY_ESCORT;
					if (GC.getGameINLINE().getSorenRandNum(100, "random frigate") < 60 - 5*iNumWartShip) 
					{
						eWarShip = UNIT_FRIGATE;
					}
					break;
				case 2:
					eWarShip = UNIT_SHIP_OF_THE_LINE;
					break;
				}

				if (eWarShip != NO_UNIT) 
				{
					CvUnit* pWarUnit = buyEuropeUnit(eWarShip, 0);
					if (pWarUnit != NULL) 
					{
						UnitTypes eCrewUnit = NO_UNIT;
						int iRandomUnitType = GC.getGameINLINE().getSorenRandNum(100, "random eCrewUnit");
						if (iRandomUnitType < 80) 
						{
							eCrewUnit = UNIT_CRIMINAL;
						} 
						else if (iRandomUnitType >= 95) 
						{
							eCrewUnit = UNIT_SAILOR_EXPERT;
						}
						else 
						{
							eCrewUnit = UNIT_COLONIST;
						}

						if (eCrewUnit != NO_UNIT) 
						{
							pWarUnit->setUnitSailorType(eCrewUnit, true);
							pWarUnit->addSailorPromotionsWithCoef(120, 140);
						}
					}
				}
			}
		}
	}
}

int CvPlayer::getRecruitPriceDiscount() const
{
	return m_iNbCross;
}
void CvPlayer::setRecruitPriceDiscount(int iNewValue)
{
	if (getRecruitPriceDiscount() != iNewValue)
	{
		m_iNbCross = iNewValue;
	}
}
void CvPlayer::changeRecruitPriceDiscount(int iChange)
{
	setRecruitPriceDiscount(getRecruitPriceDiscount() + iChange);
}
int CvPlayer::getRecruitPriceDiscountCross() const
{
	int iPrice = 0;
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	if (getRecruitPriceDiscount() < 0)
	{
		kPlayer.setRecruitPriceDiscount(600);
	}
	if (getRecruitPriceDiscount() <= 200)
	{
		iPrice = getRecruitPriceDiscount()/20;
	}
	else
	{
		int iLoop;
		bool bPretre = false;
		for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
		{
			int iCityYieldProduction = 0;
			for (int i = 0; i < pLoopCity->getPopulation(); ++i)
			{
				CvUnit* pUnit = pLoopCity->getPopulationUnitByIndex(i);
				if (NULL != pUnit)
				{
					ProfessionTypes eProfession = pUnit->getProfession();
					if (NO_PROFESSION != eProfession)
					{
						CvProfessionInfo& kProfessionInfo = GC.getProfessionInfo(eProfession);
						YieldTypes eYieldProduced = (YieldTypes) kProfessionInfo.getYieldProduced();
						if (eYieldProduced == YIELD_CROSSES)
						{
							bPretre = true;
						}
					}
				}
			}
		}
		if (bPretre)
		{
			if (getRecruitPriceDiscount() >= 600)
			{
				kPlayer.setRecruitPriceDiscount(600);
				iPrice = 30;
			}
			else
			{
				iPrice = getRecruitPriceDiscount()/20;
			}
		}
		else
		{
			kPlayer.changeRecruitPriceDiscount(-(getRecruitPriceDiscount()*15)/100);
			iPrice = -(getRecruitPriceDiscount())*15/2000;
		}
	}
	return iPrice;
}
int CvPlayer::getExtraTradeMultiplier(PlayerTypes eOtherPlayer) const
{
	int iMultiplier = 100;
	if (!isHuman() && !GET_PLAYER(eOtherPlayer).isHuman())
	{
		iMultiplier += GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIExtraTradePercent();
	}
	return iMultiplier;
}

int CvPlayer::getAdvancedStartPoints() const
{
	return m_iAdvancedStartPoints;
}


void CvPlayer::setAdvancedStartPoints(int iNewValue)
{
	if (getAdvancedStartPoints() != iNewValue)
	{
		m_iAdvancedStartPoints = iNewValue;

		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(MiscButtons_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(SelectionButtons_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(GameData_DIRTY_BIT, true);
		}
	}
}

void CvPlayer::changeAdvancedStartPoints(int iChange)
{
	setAdvancedStartPoints(getAdvancedStartPoints() + iChange);
}

int CvPlayer::getGreatGeneralsCreated() const
{
	return m_iGreatGeneralsCreated;
}

void CvPlayer::incrementGreatGeneralsCreated()
{
	m_iGreatGeneralsCreated++;
}

int CvPlayer::getGreatGeneralsThresholdModifier() const
{
	return m_iGreatGeneralsThresholdModifier;
}

void CvPlayer::changeGreatGeneralsThresholdModifier(int iChange)
{
	m_iGreatGeneralsThresholdModifier += iChange;
}

int CvPlayer::getGreatAdmiralsThresholdModifier() const
{
	return m_iGreatAdmiralsThresholdModifier;
}

void CvPlayer::changeGreatAdmiralsThresholdModifier(int iChange)
{
	m_iGreatAdmiralsThresholdModifier += iChange;
}

int CvPlayer::getGreatGeneralRateModifier() const
{
	return m_iGreatGeneralRateModifier;
}

void CvPlayer::changeGreatGeneralRateModifier(int iChange)
{
	m_iGreatGeneralRateModifier += iChange;
}


int CvPlayer::getDomesticGreatGeneralRateModifier() const
{
	return (GC.getDefineINT("COMBAT_EXPERIENCE_IN_BORDERS_PERCENT") + m_iDomesticGreatGeneralRateModifier);
}


void CvPlayer::changeDomesticGreatGeneralRateModifier(int iChange)
{
	m_iDomesticGreatGeneralRateModifier += iChange;
}


int CvPlayer::getImmigrationThresholdMultiplier() const
{
	return m_iImmigrationThresholdMultiplier;
}


void CvPlayer::setImmigrationThresholdMultiplier(int iValue)
{
	m_iImmigrationThresholdMultiplier = iValue;
}

int CvPlayer::getRevolutionEuropeUnitThresholdMultiplier() const
{
	return m_iRevolutionEuropeUnitThresholdMultiplier;
}

void CvPlayer::setRevolutionEuropeUnitThresholdMultiplier(int iValue)
{
	m_iRevolutionEuropeUnitThresholdMultiplier = iValue;
}

int CvPlayer::getKingNumUnitMultiplier() const
{
	return m_iKingNumUnitMultiplier;
}

void CvPlayer::setKingNumUnitMultiplier(int iValue)
{
	m_iKingNumUnitMultiplier = iValue;
}

int CvPlayer::getNativeAngerModifier() const
{
	return m_iNativeAngerModifier;
}

void CvPlayer::changeNativeAngerModifier(int iChange)
{
	m_iNativeAngerModifier += iChange;
}

int CvPlayer::getFreeExperience() const
{
	return m_iFreeExperience;
}


void CvPlayer::changeFreeExperience(int iChange)
{
	m_iFreeExperience += iChange;
}

int CvPlayer::getWorkerSpeedModifier() const
{
	return m_iWorkerSpeedModifier;
}


void CvPlayer::changeWorkerSpeedModifier(int iChange)
{
	m_iWorkerSpeedModifier += iChange;
}


int CvPlayer::getImprovementUpgradeRateModifier() const
{
	return m_iImprovementUpgradeRateModifier;
}


void CvPlayer::changeImprovementUpgradeRateModifier(int iChange)
{
	m_iImprovementUpgradeRateModifier += iChange;
}


int CvPlayer::getMilitaryProductionModifier() const
{
	return m_iMilitaryProductionModifier;
}


void CvPlayer::changeMilitaryProductionModifier(int iChange)
{
	m_iMilitaryProductionModifier += iChange;
}

int CvPlayer::getCityDefenseModifier() const
{
	return m_iCityDefenseModifier;
}

void CvPlayer::changeCityDefenseModifier(int iChange)
{
	if (iChange != 0)
	{
		m_iCityDefenseModifier += iChange;
		setCityBillboardDirty(true);
	}
}

int CvPlayer::getHighestUnitLevel()	const
{
	return m_iHighestUnitLevel;
}


void CvPlayer::setHighestUnitLevel(int iNewValue)
{
	m_iHighestUnitLevel = iNewValue;
	FAssert(getHighestUnitLevel() >= 0);
}

int CvPlayer::getExpInBorderModifier() const
{
	return m_iExpInBorderModifier;
}

void CvPlayer::changeExpInBorderModifier(int iChange)
{
	if (iChange != 0)
	{
		m_iExpInBorderModifier += iChange;
		FAssert(getExpInBorderModifier() >= 0);
	}
}

int CvPlayer::getLevelExperienceModifier() const
{
	return m_iLevelExperienceModifier;
}

void CvPlayer::changeLevelExperienceModifier(int iChange)
{
	m_iLevelExperienceModifier += iChange;
}

CvCity* CvPlayer::getCapitalCity() const
{
	return getCity(m_iCapitalCityID);
}

CvCity* CvPlayer::getPrimaryCity() const
{
	CvCity* pCity = getCapitalCity();

	if (pCity == NULL)
	{
		int iIndex;
		pCity = firstCity(&iIndex);
	}

	return pCity;
}

void CvPlayer::setCapitalCity(CvCity* pNewCapitalCity)
{
	CvCity* pOldCapitalCity = getCapitalCity();
	BuildingTypes nBuilding;
	BuildingTypes pBuilding;

	if (pOldCapitalCity != pNewCapitalCity)
	{
		if (pNewCapitalCity != NULL)
		{
			m_iCapitalCityID = pNewCapitalCity->getID();
		}
		else
		{
			m_iCapitalCityID = FFreeList::INVALID_INDEX;
		}

		if (pOldCapitalCity != NULL)
		{
			nBuilding = (BuildingTypes) -1;
			for (int iJ = 0; iJ < GC.getNumBuildingClassInfos(); iJ++)
			{
				pBuilding = (BuildingTypes) iJ;
				if (GC.getBuildingInfo(pBuilding).isCapital())
				{
					nBuilding = pBuilding;
				}
			}
			pOldCapitalCity->setHasRealBuilding(nBuilding, false);
			pOldCapitalCity->setBillboardDirty(true);
		}
		if (pNewCapitalCity != NULL)
		{
			pNewCapitalCity->setBillboardDirty(true);
		}
	}
}

int CvPlayer::getCitiesLost() const
{
	return m_iCitiesLost;
}


void CvPlayer::changeCitiesLost(int iChange)
{
	m_iCitiesLost += iChange;
}

int CvPlayer::getAssets() const
{
	return m_iAssets;
}

void CvPlayer::changeAssets(int iChange)
{
	m_iAssets += iChange;
	FAssert(getAssets() >= 0);
}

int CvPlayer::getPower() const
{
	return m_iPower;
}

void CvPlayer::changePower(int iChange)
{
	m_iPower += iChange;
	FAssert(getPower() >= 0);
}


int CvPlayer::getPopScore() const
{
	return (m_iPopulationScore / std::max(1, GET_TEAM(getTeam()).getNumMembers()));
}

void CvPlayer::changePopScore(int iChange)
{
	if (iChange != 0)
	{
		m_iPopulationScore += iChange;
		FAssert(getPopScore() >= 0);

		GC.getGameINLINE().setScoreDirty(true);
	}
}


int CvPlayer::getLandScore() const
{
	return (m_iLandScore  / std::max(1, GET_TEAM(getTeam()).getNumMembers()));
}


void CvPlayer::changeLandScore(int iChange)
{
	if (iChange != 0)
	{
		m_iLandScore += iChange;
		FAssert(getLandScore() >= 0);

		GC.getGameINLINE().setScoreDirty(true);
	}
}

int CvPlayer::getCombatLandExperience() const
{
	return m_iCombatLandExperience;
}

void CvPlayer::setCombatLandExperience(int iExperience)
{
	FAssert(iExperience >= 0);

	if (iExperience != getCombatLandExperience())
	{
		m_iCombatLandExperience = iExperience;

		int iExperienceThreshold = greatGeneralThreshold();
		if (getCombatLandExperience() >= iExperienceThreshold && iExperienceThreshold > 0)
		{
			int iLoop;
			CvCity* pBestCity = firstCity(&iLoop);

			if (pBestCity)
			{
				int iRandOffset = GC.getGameINLINE().getSorenRandNum(GC.getNumUnitClassInfos(), "Warlord Unit Generation");

				for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass)
				{
					UnitTypes eLoopUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits((iUnitClass+ iRandOffset) % GC.getNumUnitClassInfos());
					if (eLoopUnit != NO_UNIT && eLoopUnit != UNIT_GREAT_ADMIRAL)
					{
						if (GC.getUnitInfo(eLoopUnit).getLeaderExperience() > 0 || GC.getUnitInfo(eLoopUnit).getLeaderPromotion() != NO_PROMOTION)
						{
							pBestCity->createGreatGeneral(eLoopUnit, true);
							setCombatLandExperience(getCombatLandExperience() - iExperienceThreshold);
							break;
						}
					}
				}
			}
		}
	}
}

void CvPlayer::changeCombatLandExperience(int iChange)
{
	setCombatLandExperience(getCombatLandExperience() + iChange);
}

int CvPlayer::getCombatSeaExperience() const
{
	return m_iCombatSeaExperience;
}

void CvPlayer::setCombatSeaExperience(int iExperience)
{
	FAssert(iExperience >= 0);

	if (iExperience != getCombatSeaExperience())
	{
		m_iCombatSeaExperience = iExperience;

		int iExperienceThreshold = greatAdmiralThreshold();
		if (getCombatSeaExperience() >= iExperienceThreshold && iExperienceThreshold > 0)
		{
			int iLoop;
			CvCity* pBestCity = firstCity(&iLoop);

			if (pBestCity)
			{
				pBestCity->createGreatGeneral(UNIT_GREAT_ADMIRAL, true);
				setCombatSeaExperience(getCombatSeaExperience() - iExperienceThreshold);
			}
		}
	}
}

void CvPlayer::changeCombatSeaExperience(int iChange)
{
	setCombatSeaExperience(getCombatSeaExperience() + iChange);
}


bool CvPlayer::isConnected() const
{
	return gDLL->isConnected( getNetID() );
}

int CvPlayer::getNetID() const
{
	return GC.getInitCore().getNetID(getID());
}


void CvPlayer::setNetID(int iNetID)
{
	GC.getInitCore().setNetID(getID(), iNetID);
}

void CvPlayer::sendReminder()
{
	CvWString szYearStr;

	// Only perform this step if we have a valid email address on record,
	// and we have provided information about how to send emails
	if ( !getPbemEmailAddress().empty() &&
		!gDLL->GetPitbossSmtpHost().empty() )
	{
		GAMETEXT.setTimeStr(szYearStr, GC.getGameINLINE().getGameTurn(), true);

		// Generate our arguments
		CyArgsList argsList;
		argsList.add(getPbemEmailAddress());
		argsList.add(gDLL->GetPitbossSmtpHost());
		argsList.add(gDLL->GetPitbossSmtpLogin());
		argsList.add(gDLL->GetPitbossSmtpPassword());
		argsList.add(GC.getGameINLINE().getName());
		argsList.add(GC.getGameINLINE().isMPOption(MPOPTION_TURN_TIMER));
		argsList.add(GC.getGameINLINE().getPitbossTurnTime());
		argsList.add(gDLL->GetPitbossEmail());
		argsList.add(szYearStr);

		// Now send our email via Python
		long iResult;
		bool bOK = gDLL->getPythonIFace()->callFunction(PYPitBossModule, "sendEmail", argsList.makeFunctionArgs(), &iResult);

		FAssertMsg( bOK, "Pitboss Python call to onSendEmail failed!" );
		FAssertMsg( iResult == 0, "Pitboss Python fn onSendEmail encountered an error" );
	}
}

int CvPlayer::getCurrentTradeRouteId() const
{
	return m_iCurrentTradeRouteId;
}


void CvPlayer::setCurrentTradeRouteId(int iCurrentTradeRouteId, bool bUdateTradeRoutesScreen)
{
	m_iCurrentTradeRouteId = iCurrentTradeRouteId;
	if (bUdateTradeRoutesScreen && getID() == GC.getGameINLINE().getActivePlayer())
	{
		CyArgsList argsList;
		argsList.add(iCurrentTradeRouteId);
		gDLL->getPythonIFace()->callFunction(PYScreensModule, "setCurrentTradeRouteId", argsList.makeFunctionArgs());
	}
}


uint CvPlayer::getStartTime() const
{
	return m_uiStartTime;
}


void CvPlayer::setStartTime(uint uiStartTime)
{
	m_uiStartTime = uiStartTime;
}


uint CvPlayer::getTotalTimePlayed() const
{
	return ((timeGetTime() - m_uiStartTime)/1000);
}


bool CvPlayer::isAlive() const
{
	return m_bAlive;
}

bool CvPlayer::isTerritorialInfluenceMode() const
{
	return m_bTerritorialInfluenceMode;
}

void CvPlayer::setTerritorialInfluenceMode(bool bTerritorialInfluence)
{
	if (!gDLL->getInterfaceIFace()->isBareMapMode() || !bTerritorialInfluence)
	{
		gDLL->getInterfaceIFace()->toggleBareMapMode();
	}
	if (bTerritorialInfluence)
	{
		gDLL->getInterfaceIFace()->clearSelectionList();
	}
	m_bTerritorialInfluenceMode = bTerritorialInfluence;
}

bool CvPlayer::isLaunchFirstPopup() const
{
	return m_bLaunchFirstPopup;
}

void CvPlayer::changeLaunchFirstPopup(bool bChange)
{
	m_bLaunchFirstPopup = bChange;
}

bool CvPlayer::isKingGiveGalleon() const
{
	return m_bKingGiveGalleon;
}

void CvPlayer::setKingGiveGalleon(bool bChange)
{
	m_bKingGiveGalleon = bChange;
}

bool CvPlayer::wasAttackedByPirates() const
{
	return m_bAttackedByPirates;
}

void CvPlayer::setAttackedByPirates(bool bChange)
{
	m_bAttackedByPirates = bChange;
}

bool CvPlayer::isPenalitePlayerSave() const
{
	return m_bPenalitePlayerSave;
}

void CvPlayer::setPenalitePlayerSave(bool bChange)
{
	m_bPenalitePlayerSave = bChange;
}

bool CvPlayer::isPenalitePlayerLoad() const
{
	return m_bPenalitePlayerLoad;
}

void CvPlayer::setPenalitePlayerLoad(bool bChange)
{
	m_bPenalitePlayerLoad = bChange;
}

bool CvPlayer::isEverAlive() const
{
	return m_bEverAlive;
}


void CvPlayer::setAlive(bool bNewValue)
{
	CvWString szBuffer;
	int iI;

	if (isAlive() != bNewValue)
	{
		m_bAlive = bNewValue;

		GET_TEAM(getTeam()).changeAliveCount((isAlive()) ? 1 : -1);

		if (isEuropean())
		{
			GET_TEAM(getTeam()).changeAliveEuropeanPlayers((isAlive()) ? 1 : -1);
		}

		// Report event to Python
		gDLL->getEventReporterIFace()->setPlayerAlive(getID(), bNewValue);

		if (isAlive())
		{
			if (!isEverAlive())
			{
				m_bEverAlive = true;

				GET_TEAM(getTeam()).changeEverAliveCount(1);
			}

			if (getNumCities() == 0)
			{
				setFoundedFirstCity(false);
			}

			if (GC.getGameINLINE().isMPOption(MPOPTION_SIMULTANEOUS_TURNS) || (GC.getGameINLINE().getNumGameTurnActive() == 0) || (GC.getGameINLINE().isSimultaneousTeamTurns() && GET_TEAM(getTeam()).isTurnActive()))
			{
				setTurnActive(true);
			}

			gDLL->openSlot(getID());
		}
		else
		{
			killUnits();
			killCities();
			killAllDeals();

			setTurnActive(false);

			gDLL->endMPDiplomacy();
			gDLL->endDiplomacy();

			if (!isHuman())
			{
				gDLL->closeSlot(getID());
			}

			if (!GC.getGameINLINE().isGameMultiPlayer())
			{
				if (GC.getGameINLINE().getActivePlayer() == getID() && GC.getGameINLINE().getAIAutoPlay() <= 0 && !gDLL->GetAutorun())
				{
					GC.getGameINLINE().setGameState(GAMESTATE_OVER);
				}
			}

			if (GC.getGameINLINE().getElapsedGameTurns() > 0)
			{
				szBuffer = gDLL->getText("TXT_KEY_MISC_CIV_DESTROYED", getCivilizationAdjectiveKey());

				for (iI = 0; iI < MAX_PLAYERS; iI++)
				{
					if (GET_PLAYER((PlayerTypes)iI).isAlive())
					{
						gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_CIVDESTROYED", MESSAGE_TYPE_MAJOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WARNING_TEXT"));
					}
				}

				GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getID(), szBuffer, -1, -1, (ColorTypes)GC.getInfoTypeForString("COLOR_WARNING_TEXT"));
			}
		}

		GC.getGameINLINE().setScoreDirty(true);
	}
}


void CvPlayer::verifyAlive()
{
	bool bKill;

	if (isAlive())
	{
		bKill = false;

		if (!bKill)
		{
			if (getNumCities() == 0 && !isEurope() && getAdvancedStartPoints() < 0)
			{
				if (isNative() || getNumUnits() == 0)
				{
					bKill = true;
				}
			}
		}

		if (bKill)
		{
			//don't kill colonist players
			if (getParent() != NO_PLAYER)
			{
				CvPlayer& kEurope = GET_PLAYER(getParent());
				if (kEurope.isAlive() && kEurope.isEurope() && !::atWar(getTeam(), kEurope.getTeam()) && (GC.getGameINLINE().getAIAutoPlay() == 0 || GC.getGameINLINE().getActivePlayer() != getID()))
				{
					initFreeUnits();
					if (isTurnActive())
					{
						setEndTurn(true);
					}

					//change taxrate
					int iOldTaxRate = getTaxRate();
					int iNewTaxRate = std::min(99, iOldTaxRate + 1 + GC.getGameINLINE().getSorenRandNum(GC.getDefineINT("TAX_RATE_MAX_INCREASE"), "Tax Rate Increase"));
					int iChange = iNewTaxRate - iOldTaxRate;
					changeTaxRate(iChange);

					if (isHuman())
					{
						CvDiploParameters* pDiplo = new CvDiploParameters(kEurope.getID());
						pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_KING_REVIVE"));
						pDiplo->addDiploCommentVariable(iNewTaxRate);
						pDiplo->setAIContact(true);
						gDLL->beginDiplomacy(pDiplo, getID());
					}

					bKill = false;
				}
			}
		}

		if (!bKill)
		{
			if (!isNative() && !isEurope() && !isHuman())
			{
				/*if (GC.getGameINLINE().getMaxCityElimination() > 0)
				{*/
				if (getNumCities() == 0 && getCitiesLost() >= 2/*GC.getGameINLINE().getMaxCityElimination()*/)
				{
					bKill = true;
				}
				//}
			}
		}

		if (bKill)
		{
			setAlive(false);
		}
	}
	else
	{
		if ((getNumCities() > 0) || (getNumUnits() > 0))
		{
			setAlive(true);
		}
	}
}

bool CvPlayer::isTurnActive() const
{
	return m_bTurnActive;
}

void CvPlayer::setTurnActiveForPbem(bool bActive)
{
	FAssertMsg(GC.getGameINLINE().isPbem(), "You are using setTurnActiveForPbem. Are you sure you know what you're doing?");

	// does nothing more than to set the member variable before saving the game
	// the rest of the turn will be performed upon loading the game
	// This allows the player to browse the game in paused mode after he has generated the save
	if (isTurnActive() != bActive)
	{
		m_bTurnActive = bActive;
		GC.getGameINLINE().changeNumGameTurnActive(isTurnActive() ? 1 : -1);
	}
}


void CvPlayer::setTurnActive(bool bNewValue, bool bDoTurn)
{
	if (isTurnActive() != bNewValue)
	{
		m_bTurnActive = bNewValue;

		if (isTurnActive())
		{
			if (GC.getLogging())
			{
				if (gDLL->getChtLvl() > 0)
				{
					TCHAR szOut[1024];
					sprintf(szOut, "Player %d Turn ON\n", getID());
					gDLL->messageControlLog(szOut);
				}
			}

			FAssertMsg(isAlive(), "isAlive is expected to be true");

			setEndTurn(false);
			GC.getGameINLINE().resetTurnTimer();

			// If we are the Pitboss, send this player an email
			if ( gDLL->IsPitbossHost() )
			{
				// If this guy is not currently connected, try sending him an email
				if ( isHuman() && !isConnected() )
				{
					sendReminder();
				}
			}

			if ((GC.getGameINLINE().isHotSeat() || GC.getGameINLINE().isPbem()) && isHuman() && bDoTurn)
			{
				gDLL->getInterfaceIFace()->clearEventMessages();
				gDLL->getEngineIFace()->setResourceLayer(false);

				GC.getGameINLINE().setActivePlayer(getID());
			}

			GC.getGameINLINE().changeNumGameTurnActive(1);

			if (bDoTurn)
			{
				if (isAlive() && !isHuman() && (getAdvancedStartPoints() >= 0))
				{
					AI_doAdvancedStart();
				}

				if (GC.getGameINLINE().getElapsedGameTurns() > 0)
				{
					if (isAlive())
					{
						if (GC.getGameINLINE().isMPOption(MPOPTION_SIMULTANEOUS_TURNS))
						{
							doTurn();
						}

						doTurnUnits();
					}
				}

				if ((getID() == GC.getGameINLINE().getActivePlayer()) && (GC.getGameINLINE().getElapsedGameTurns() > 0))
				{
					if (GC.getGameINLINE().isNetworkMultiPlayer())
					{
						gDLL->getInterfaceIFace()->addMessage(getID(), true, GC.getEVENT_MESSAGE_TIME(), gDLL->getText("TXT_KEY_MISC_TURN_BEGINS").GetCString(), "AS2D_NEWTURN", MESSAGE_TYPE_DISPLAY_ONLY);
					}
					else
					{
						gDLL->getInterfaceIFace()->playGeneralSound("AS2D_NEWTURN");
					}
				}

				doWarnings();
			}

			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				if (gDLL->getInterfaceIFace()->getLengthSelectionList() == 0)
				{
					gDLL->getInterfaceIFace()->setCycleSelectionCounter(1);
				}

				gDLL->getInterfaceIFace()->setDirty(SelectionCamera_DIRTY_BIT, true);
			}
		}
		else
		{
			if (GC.getLogging())
			{
				if (gDLL->getChtLvl() > 0)
				{
					TCHAR szOut[1024];
					sprintf(szOut, "Player %d Turn OFF\n", getID());
					gDLL->messageControlLog(szOut);
				}
			}

			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->setForcePopup(false);
				gDLL->getInterfaceIFace()->clearQueuedPopups();
				gDLL->getInterfaceIFace()->flushTalkingHeadMessages();
			}

			GC.getGameINLINE().changeNumGameTurnActive(-1);

			if (bDoTurn)
			{
				if (!GC.getGameINLINE().isMPOption(MPOPTION_SIMULTANEOUS_TURNS))
				{
					if (isAlive())
					{
						doTurn();
					}

					if ((GC.getGameINLINE().isPbem() || GC.getGameINLINE().isHotSeat()) && isHuman() && GC.getGameINLINE().countHumanPlayersAlive() > 1)
					{
						GC.getGameINLINE().setHotPbemBetweenTurns(true);
					}

					if (GC.getGameINLINE().isSimultaneousTeamTurns())
					{
						if (!GET_TEAM(getTeam()).isTurnActive())
						{
							for (int iI = (getTeam() + 1); iI < MAX_TEAMS; iI++)
							{
								if (GET_TEAM((TeamTypes)iI).isAlive())
								{
									GET_TEAM((TeamTypes)iI).setTurnActive(true);
									break;
								}
							}
						}
					}
					else
					{
						for (int iI = (getID() + 1); iI < MAX_PLAYERS; iI++)
						{
							if (GET_PLAYER((PlayerTypes)iI).isAlive())
							{
								if (GC.getGameINLINE().isPbem() && GET_PLAYER((PlayerTypes)iI).isHuman())
								{
									if (!GC.getGameINLINE().getPbemTurnSent())
									{
										gDLL->sendPbemTurn((PlayerTypes)iI);
									}
								}
								else
								{
									GET_PLAYER((PlayerTypes)iI).setTurnActive(true);
								}
								break;
							}
						}
					}
				}
			}
		}

		gDLL->getInterfaceIFace()->updateCursorType();

		gDLL->getInterfaceIFace()->setDirty(Score_DIRTY_BIT, true);
	}
}


bool CvPlayer::isAutoMoves() const
{
	return m_bAutoMoves;
}


void CvPlayer::setAutoMoves(bool bNewValue)
{
	if (isAutoMoves() != bNewValue)
	{
		m_bAutoMoves = bNewValue;

		if (!isAutoMoves())
		{
			if (isEndTurn() || !isHuman())
			{
				setTurnActive(false);
			}
			else
			{
				if (getID() == GC.getGameINLINE().getActivePlayer())
				{
					gDLL->getInterfaceIFace()->setCycleSelectionCounter(1);
				}
			}
		}
	}
}


bool CvPlayer::isEndTurn() const
{
	return m_bEndTurn;
}


void CvPlayer::setEndTurn(bool bNewValue)
{
	if (isTerritorialInfluenceMode())
	{
		return;
	}
	if (isEndTurn() != bNewValue)
	{
		FAssertMsg(isTurnActive(), "isTurnActive is expected to be true");

		m_bEndTurn = bNewValue;

		if (isEndTurn())
		{
			setAutoMoves(true);
		}
	}
}

bool CvPlayer::isTurnDone() const
{
	// if this returns true, popups and diplomacy will wait to appear until next turn
	if (!GC.getGameINLINE().isPbem() && !GC.getGameINLINE().isHotSeat())
	{
		return false;
	}
	if (!isHuman() )
	{
		return true;
	}
	if (!isEndTurn())
	{
		return false;
	}
	return (!isAutoMoves());
}

bool CvPlayer::isExtendedGame() const
{
	return m_bExtendedGame;
}


void CvPlayer::makeExtendedGame()
{
	m_bExtendedGame = true;
}


bool CvPlayer::isFoundedFirstCity() const
{
	return m_bFoundedFirstCity;
}


void CvPlayer::setFoundedFirstCity(bool bNewValue)
{
	if (isFoundedFirstCity() != bNewValue)
	{
		m_bFoundedFirstCity = bNewValue;
		if (bNewValue && !isHuman() && !isNative() && !isEurope())
		{
			calculDistanceCityEurope();
		}
	}
}

void CvPlayer::calculDistanceCityEurope()
{
	int iLoop;
	CvCity* pLoopCity = firstCity(&iLoop);
	if (pLoopCity != NULL)
	{
		int iDX = 0;
		int iDistance = 8;
		while(iDX < 30)
		{
			for (int iDY = -iDistance; iDY <= iDistance; iDY++)
			{
				CvPlot* pPlot = plotXY(pLoopCity->getX_INLINE(), pLoopCity->getY_INLINE(), iDX, iDY);
				if (pPlot != NULL)
				{
					if (pPlot->isEurope())
					{
						setDistanceToEuropeIA(std::max(7, iDX + 1));
						return;
					}
				}
			}
			iDX++;
		}
	}
}

bool CvPlayer::isDisplaySeedling() const
{
	return m_bDisplaySeedling;
}


void CvPlayer::setDisplaySeedling(bool bNewValue)
{
	if (isDisplaySeedling() != bNewValue)
	{
		m_bDisplaySeedling = bNewValue;
	}
}

PlayerTypes CvPlayer::getID() const
{
	return m_eID;
}


HandicapTypes CvPlayer::getHandicapType() const
{
	return GC.getInitCore().getHandicap(getID());
}


CivilizationTypes CvPlayer::getCivilizationType() const
{
	return GC.getInitCore().getCiv(getID());
}


LeaderHeadTypes CvPlayer::getLeaderType() const
{
	return GC.getInitCore().getLeader(getID());
}


LeaderHeadTypes CvPlayer::getPersonalityType() const
{
	return m_ePersonalityType;
}


void CvPlayer::setPersonalityType(LeaderHeadTypes eNewValue)
{
	m_ePersonalityType = eNewValue;
}


EraTypes CvPlayer::getCurrentEra() const
{
	return m_eCurrentEra;
}



void CvPlayer::setCurrentEra(EraTypes eNewValue)
{
	CvCity* pLoopCity;
	CvPlot* pLoopPlot;
	int iLoop;
	int iI;

	if (getCurrentEra() != eNewValue)
	{
		EraTypes eOldEra = m_eCurrentEra;
		m_eCurrentEra = eNewValue;

		if (GC.getGameINLINE().getActiveTeam() != NO_TEAM)
		{
			for (iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); iI++)
			{
				pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iI);
				if (pLoopPlot->getRevealedImprovementType(GC.getGameINLINE().getActiveTeam(), true) != NO_IMPROVEMENT)
				{
					if ((pLoopPlot->getOwnerINLINE() == getID()) || (!(pLoopPlot->isOwned()) && (getID() == GC.getGameINLINE().getActivePlayer())))
					{
						pLoopPlot->setLayoutDirty(true);
					}
				}
			}
		}

		// dirty all of this player's cities...
		for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
		{
			if (pLoopCity->getOwnerINLINE() == getID())
			{
				pLoopCity->setLayoutDirty(true);
			}
		}

		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(Soundtrack_DIRTY_BIT, true);
		}
	}
}


PlayerTypes CvPlayer::getParent() const
{
	return m_eParent;
}

void CvPlayer::setParent(PlayerTypes eParent)
{
	m_eParent = eParent;
}

TeamTypes CvPlayer::getTeam() const
{
	return m_eTeamType;
}

void CvPlayer::updateTeamType()
{
	if (getID() == NO_PLAYER)
	{
		m_eTeamType = NO_TEAM;
	}
	else
	{
		m_eTeamType = GC.getInitCore().getTeam(getID());
		GET_TEAM(getTeam()).addPlayer(getID());
	}
}

void CvPlayer::setTeam(TeamTypes eTeam)
{
	FAssert(eTeam != NO_TEAM);
	FAssert(getTeam() != NO_TEAM);
	CvTeam& kTeam = GET_TEAM(getTeam());

	kTeam.changeNumMembers(-1);
	if (isAlive())
	{
		kTeam.changeAliveCount(-1);
	}
	if (isEverAlive())
	{
		kTeam.changeEverAliveCount(-1);
	}
	kTeam.changeNumCities(-(getNumCities()));
	if (isEuropean()) 
	{
		kTeam.changeNumEuropeanCities(-(getNumCities()));
	}

	kTeam.changeTotalLand(-(getTotalLand()));

	GC.getInitCore().setTeam(getID(), eTeam);
	CvTeam& kNewTeam = GET_TEAM(getTeam());

	kNewTeam.changeNumMembers(1);
	if (isAlive())
	{
		kNewTeam.changeAliveCount(1);
	}
	if (isEverAlive())
	{
		kNewTeam.changeEverAliveCount(1);
	}
	kNewTeam.changeNumCities(getNumCities());
	if (isEuropean()) 
	{
		kNewTeam.changeNumEuropeanCities(getNumCities());
	}

	kNewTeam.changeTotalLand(getTotalLand());
}

YieldTypes CvPlayer::getImmigrationConversion() const
{
	return m_eImmigrationConversion;
}

void CvPlayer::setImmigrationConversion(YieldTypes eConversion)
{
	m_eImmigrationConversion = eConversion;
}

PlayerRoleTypes CvPlayer::getPlayerRole() const
{
	return m_ePlayerRole;
}

void CvPlayer::setPlayerRole(PlayerRoleTypes ePlayerRoleType)
{
	m_ePlayerRole = ePlayerRoleType;
}

PlayerColorTypes CvPlayer::getPlayerColor() const
{
	return GC.getInitCore().getColor(getID());
}

int CvPlayer::getPlayerTextColorR() const
{
	FAssertMsg(getPlayerColor() != NO_PLAYERCOLOR, "getPlayerColor() is not expected to be equal with NO_PLAYERCOLOR");
	return ((int)(GC.getColorInfo((ColorTypes) GC.getPlayerColorInfo(getPlayerColor()).getTextColorType()).getColor().r * 255));
}

int CvPlayer::getPlayerTextColorG() const
{
	FAssertMsg(getPlayerColor() != NO_PLAYERCOLOR, "getPlayerColor() is not expected to be equal with NO_PLAYERCOLOR");
	return ((int)(GC.getColorInfo((ColorTypes) GC.getPlayerColorInfo(getPlayerColor()).getTextColorType()).getColor().g * 255));
}

int CvPlayer::getPlayerTextColorB() const
{
	FAssertMsg(getPlayerColor() != NO_PLAYERCOLOR, "getPlayerColor() is not expected to be equal with NO_PLAYERCOLOR");
	return ((int)(GC.getColorInfo((ColorTypes) GC.getPlayerColorInfo(getPlayerColor()).getTextColorType()).getColor().b * 255));
}

int CvPlayer::getPlayerTextColorA() const
{
	FAssertMsg(getPlayerColor() != NO_PLAYERCOLOR, "getPlayerColor() is not expected to be equal with NO_PLAYERCOLOR");
	return ((int)(GC.getColorInfo((ColorTypes) GC.getPlayerColorInfo(getPlayerColor()).getTextColorType()).getColor().a * 255));
}

int CvPlayer::getSeaPlotYield(YieldTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiSeaPlotYield[eIndex];
}

void CvPlayer::changeSeaPlotYield(YieldTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		m_aiSeaPlotYield[eIndex] = (m_aiSeaPlotYield[eIndex] + iChange);

		updateYield();
	}
}

int CvPlayer::getYieldRateModifier(YieldTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiYieldRateModifier[eIndex];
}

int CvPlayer::getTaxYieldRateModifier(YieldTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (getTaxYieldModifierCount(eIndex) > 0)
	{
		return getTaxRate();
	}

	return 0;
}

void CvPlayer::changeYieldRateModifier(YieldTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		m_aiYieldRateModifier[eIndex] += iChange;

		invalidateYieldRankCache(eIndex);

		AI_makeAssignWorkDirty();

		if (getTeam() == GC.getGameINLINE().getActiveTeam())
		{
			gDLL->getInterfaceIFace()->setDirty(CityInfo_DIRTY_BIT, true);
		}
	}
}


int CvPlayer::getCapitalYieldRateModifier(YieldTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiCapitalYieldRateModifier[eIndex];
}


void CvPlayer::changeCapitalYieldRateModifier(YieldTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		m_aiCapitalYieldRateModifier[eIndex] += iChange;

		invalidateYieldRankCache(eIndex);

		CvCity* pCapitalCity = getCapitalCity();
		if (pCapitalCity != NULL)
		{
			pCapitalCity->AI_setAssignWorkDirty(true);
			if (pCapitalCity->getTeam() == GC.getGameINLINE().getActiveTeam())
			{
				pCapitalCity->setBillboardDirty(true);
			}
		}
	}
}

int CvPlayer::getBuildingRequiredYieldModifier(YieldTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiBuildingRequiredYieldModifier[eIndex];
}


void CvPlayer::changeBuildingRequiredYieldModifier(YieldTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		m_aiBuildingRequiredYieldModifier[eIndex] += iChange;
	}
}

int CvPlayer::getCityExtraYield(YieldTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiCityExtraYield[eIndex];
}

void CvPlayer::updateCityExtraYield(YieldTypes eIndex)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");

	int iBestValue = 0;
	for (int iI = 0; iI < GC.getNumTraitInfos(); iI++)
	{
		if (hasTrait((TraitTypes)iI))
		{
			iBestValue += GC.getTraitInfo((TraitTypes) iI).getCityExtraYield(eIndex);
		}
	}

	if (getCityExtraYield(eIndex) != iBestValue)
	{
		m_aiCityExtraYield[eIndex] = iBestValue;
		FAssert(getCityExtraYield(eIndex) >= 0);

		updateYield();
	}
}


int CvPlayer::getExtraYieldThreshold(YieldTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiExtraYieldThreshold[eIndex];
}


void CvPlayer::updateExtraYieldThreshold(YieldTypes eIndex)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");

	int iBestValue = 0;

	for (int iI = 0; iI < GC.getNumTraitInfos(); iI++)
	{
		if (hasTrait((TraitTypes)iI))
		{
			if (GC.getTraitInfo((TraitTypes) iI).getExtraYieldThreshold(eIndex) > 0)
			{
				if ((iBestValue == 0) || (GC.getTraitInfo((TraitTypes) iI).getExtraYieldThreshold(eIndex) < iBestValue))
				{
					iBestValue = GC.getTraitInfo((TraitTypes) iI).getExtraYieldThreshold(eIndex);
				}
			}
		}
	}

	if (getExtraYieldThreshold(eIndex) != iBestValue)
	{
		m_aiExtraYieldThreshold[eIndex] = iBestValue;
		FAssert(getExtraYieldThreshold(eIndex) >= 0);

		updateYield();
	}
}


int CvPlayer::getYieldRate(YieldTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_YIELD_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (getNumCities() == 0)
	{
		return 0;
	}

	int iTotalRate = GC.getCivilizationInfo(getCivilizationType()).getFreeYields(eIndex);
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		iTotalRate += pLoopCity->calculateNetYield(eIndex);
	}

	return iTotalRate;
}

bool CvPlayer::isYieldEuropeTradable(YieldTypes eYield) const
{
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);

	if (getParent() == NO_PLAYER)
	{
		return false;
	}

	if (!GC.getYieldInfo(eYield).isCargo())
	{
		return false;
	}

	if (!canTradeWithEurope())
	{
		return false;
	}

	if (GET_PLAYER(getParent()).getYieldBuyPrice(eYield) <= 0)
	{
		return false;
	}

	return m_abYieldEuropeTradable[eYield];
}

void CvPlayer::setYieldEuropeTradable(YieldTypes eYield, bool bTradeable)
{
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);

	bool bOldTradeable = isYieldEuropeTradable(eYield);

	m_abYieldEuropeTradable[eYield] = bTradeable;
}

void CvPlayer::setYieldEuropeTradableAll()
{
	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		setYieldEuropeTradable((YieldTypes)iI, true);
	}
}

bool CvPlayer::hasGeneratedPirates(int index) const
{
	FAssert(index >= 0 && index < GC.getNumPirateInfos());
	return m_pabHasGeneratedPirates[index];
}

void CvPlayer::setIfHasGeneratedPirates(int index, bool bNewValue)
{
	FAssert(index >= 0 && index < GC.getNumPirateInfos());
	m_pabHasGeneratedPirates[index] = bNewValue;
}
bool CvPlayer::isLaunchedPopup(ButtonPopupTypes eButtonPopup) const
{
	FAssert(eButtonPopup >= 0 && eButtonPopup < NUM_BUTTONPOPUP_TYPES);
	return m_abLaunchedPopup[eButtonPopup];
}


void CvPlayer::setLaunchedPopup(ButtonPopupTypes eButtonPopup, bool bNewValue)
{
	FAssert(eButtonPopup >= 0 && eButtonPopup < NUM_BUTTONPOPUP_TYPES);
	if (CvDLLButtonPopup::launchOnlyOnceByTurnPopup(eButtonPopup) || CvDLLButtonPopup::launchOnlyOncePopup(eButtonPopup))
	{
		m_abLaunchedPopup[eButtonPopup] = bNewValue;
	}
}

void CvPlayer::clearLaunchedPopup()
{
	for (int iI = 0; iI < NUM_BUTTONPOPUP_TYPES; ++iI)
	{
		ButtonPopupTypes eButtonPopup = (ButtonPopupTypes) iI;
		if (isLaunchedPopup(eButtonPopup))
		{
			if (CvDLLButtonPopup::launchOnlyOnceByTurnPopup(eButtonPopup))
			{
				setLaunchedPopup(eButtonPopup, false);
			}
		}
	}
}

bool CvPlayer::isHasYieldUnknown(YieldTypes eYield) const
{
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);
	return m_abYieldUnknown[eYield];
}


void CvPlayer::setHasYieldUnknown(YieldTypes eYield, bool bNewValue)
{
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);
	m_abYieldUnknown[eYield] = bNewValue;
	if (eYield == YIELD_TOBACCO && !bNewValue)
	{
		setHasYieldUnknown(YIELD_CIGARS, false);
	}
}

bool CvPlayer::hasPinnedYieldInEurope(YieldTypes eYield) const
{
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);
	return m_abPinnedYieldInEurope[eYield];
}


void CvPlayer::setHasPinnedYieldInEurope(YieldTypes eYield, bool bNewValue)
{
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);
	m_abPinnedYieldInEurope[eYield] = bNewValue;
}


bool CvPlayer::hasAutomaticTradeByYieldWithNative(YieldTypes eYield) const
{
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);
	return m_abAutomaticTradeByYieldWithNative[eYield];
}

void CvPlayer::setHasAutomaticTradeByYieldWithNative(YieldTypes eYield, bool bNewValue)
{
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);
	m_abAutomaticTradeByYieldWithNative[eYield] = bNewValue;
}

void CvPlayer::initYieldUnknow()
{
	if (isNative())
	{
		return;
	}
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (GC.getYieldInfo(eYield).isUnknown())
		{
			setHasYieldUnknown(eYield, true);
		}
	}
}

void CvPlayer::updateYieldUnknowWithOtherEuropeans()
{
	if (getParent() == NO_PLAYER)
	{
		return;
	}
	CvPlayer& kEurope = GET_PLAYER(getParent());
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (GC.getYieldInfo(eYield).isUnknown() && !isHasYieldUnknown(eYield)/* && kEurope.getEuropeWarehouseYield(eYield) > 0*/)
		{
			for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
			{
				CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes) iPlayer);
				if (kLoopPlayer.isAlive() && kLoopPlayer.getParent() != NO_PLAYER && kLoopPlayer.isHasYieldUnknown(eYield))
				{
					if (kLoopPlayer.isHuman())
					{
						gDLL->getInterfaceIFace()->addMessage(kLoopPlayer.getID(), false, GC.getEVENT_MESSAGE_TIME(), gDLL->getText("TXT_KEY_EUROPEAN_DISCOVERED_NEW_RESSOURCE", getCivilizationShortDescriptionKey(), GC.getYieldInfo(eYield).getDescription()), "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MAJOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"));
					}
					kLoopPlayer.setHasYieldUnknown(eYield, false);
				}
			}
		}
	}
}

BonusTypes CvPlayer::getNextBetterEuropean() const
{
	BonusTypes eBestBonus = NO_BONUS;
	int iBestValue = 0, iTempValue;
	CvTeam& kTeam = GET_TEAM(getTeam());
	for (int iI = 0; iI < GC.getNumBonusInfos(); ++iI)
	{
		BonusTypes eBonus = (BonusTypes)iI;
		if (GC.getBonusInfo(eBonus).isSeed() && (GC.getBonusInfo(eBonus).getRefBonus() != 1))
		{
			YieldTypes eBonusYield = NO_YIELD;
			for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
			{
				YieldTypes eYield = (YieldTypes)iYield;
				if (GC.getBonusInfo(eBonus).getYieldChange(eYield) > 0)
				{
					if (eBonusYield != NO_YIELD)
					{
						//Only bonuses with one ressource
						eBonusYield = NO_YIELD;
						break;
					}
					eBonusYield = eYield;
				}
			}
			if (eBonusYield != NO_YIELD && !GC.getYieldInfo(eBonusYield).isNewWorld()) 	{
				if (!kTeam.hasSeedling(eBonus))
				{
					iTempValue = GC.getGameINLINE().getSorenRandNum(50, "getNextBetterEuropean") + 1;//Silly system with a random choice
					if (iTempValue > iBestValue)
					{
						iBestValue = iTempValue;
						eBestBonus = eBonus;
					}
				}
			}
		}
	}
	return eBestBonus;
}

int CvPlayer::getLevelReceiveSeedling() const
{
	return m_iLevelReceiveSeedling;
}

void CvPlayer::changeLevelReceiveSeedling(int iChange)
{
	m_iLevelReceiveSeedling += iChange;
}


bool CvPlayer::isFeatAccomplished(FeatTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_FEAT_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_abFeatAccomplished[eIndex];
}


void CvPlayer::setFeatAccomplished(FeatTypes eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_FEAT_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_abFeatAccomplished[eIndex] = bNewValue;
}

bool CvPlayer::shouldDisplayFeatPopup(FeatTypes eIndex) const
{
	if (isFeatAccomplished(eIndex))
	{
		return false;
	}

	if (GC.getGameINLINE().isNetworkMultiPlayer())
	{
		return false;
	}

	if (!isOption(PLAYEROPTION_ADVISOR_POPUPS))
	{
		return false;
	}

	if (!isHuman())
	{
		return false;
	}

	if (GC.getGameINLINE().getElapsedGameTurns() == 0)
	{
		return false;
	}

	return true;
}

bool CvPlayer::isOption(PlayerOptionTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_PLAYEROPTION_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_abOptions[eIndex];
}

void CvPlayer::setOption(PlayerOptionTypes eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_PLAYEROPTION_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_abOptions[eIndex] = bNewValue;
}

bool CvPlayer::isSecondaryOption(SecondaryPlayerOptionTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_SECONDARYPLAYEROPTION_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_abSecondaryOptions[eIndex];
}

void CvPlayer::setSecondaryOption(SecondaryPlayerOptionTypes eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_SECONDARYPLAYEROPTION_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_abSecondaryOptions[eIndex] = bNewValue;
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		switch(eIndex)
		{
		case SECONDARYPLAYEROPTION_SHOW_SAILORS_IN_IMMIGRATION:
			gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
			break;
		case SECONDARYPLAYEROPTION_KEPT_AUTO_IN_WARTIME:
			gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
			break;
		}
	}
}

bool CvPlayer::isHadTutorialDoane(int eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_TUTORIAL_DOANE, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_abHadTutorialDoane[eIndex];
}

void CvPlayer::setHadTutorialDoane(int eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_TUTORIAL_DOANE, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_abHadTutorialDoane[eIndex] = bNewValue;
}

int CvPlayer::getNextUnitMap()
{
	for (int iI = 0; iI < NUM_MAPS; iI++)
	{
		if (!m_abUnitUsedMap[iI])
		{
			m_abUnitUsedMap[iI] = true;
			return iI;
		}
	}
	return -1;
}
void CvPlayer::deleteUnitMap(int iNumMap)
{
	m_abUnitUsedMap[iNumMap] = false;
}
void CvPlayer::processMapsTeam()
{
	TeamTypes eTeam = getTeam();
	if (eTeam == NO_TEAM || !isHuman())
	{
		return;
	}
	int iMapWidth = GC.getMapINLINE().getGridWidthINLINE();
	int iMapHeight = GC.getMapINLINE().getGridHeightINLINE();
	CvPlot* pPlot = NULL;
	//
	// Start : Init starting plot
	//
	int iTurn = GC.getGameINLINE().getGameTurn();
	if (iTurn == 0)
	{
		CvPlot* pStartingPlot = getStartingPlot();
		if (eTeam != NO_TEAM && pStartingPlot != NULL)
		{
			int iDistance = 1;
			for (int iDX = -iDistance; iDX <= iDistance; iDX++)
			{
				for (int iDY = -iDistance; iDY <= iDistance; iDY++)
				{
					CvPlot* pPlot1 = plotXY(pStartingPlot->getX_INLINE(), pStartingPlot->getY_INLINE(), iDX, iDY);
					if (pPlot1 != NULL)
					{
						pPlot1->setDefinitelyRevealed(eTeam, true);
					}
				}
			}
		}
	}
	//
	// END : Init starting plot
	//
	for (int iX = 0; iX < iMapWidth; iX++)
	{
		for (int iY = 0; iY < iMapHeight; iY++)
		{
			pPlot = GC.getMapINLINE().plot(iX, iY);
			if (pPlot != NULL && !pPlot->isDefinitelyRevealed(eTeam) && !pPlot->isVisible(eTeam, false) && pPlot->isRevealed(eTeam, false))
			{
				pPlot->setRevealed(eTeam, false, false, NO_TEAM);
			}
		}
	}
}

bool CvPlayer::isPlayable() const
{
	return GC.getInitCore().getPlayableCiv(getID());
}

void CvPlayer::setPlayable(bool bNewValue)
{
	GC.getInitCore().setPlayableCiv(getID(), bNewValue);
}

int CvPlayer::getImprovementCount(ImprovementTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumImprovementInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiImprovementCount[eIndex];
}


void CvPlayer::changeImprovementCount(ImprovementTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumImprovementInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_paiImprovementCount[eIndex] = (m_paiImprovementCount[eIndex] + iChange);
	FAssert(getImprovementCount(eIndex) >= 0);
}


int CvPlayer::getFreeBuildingCount(BuildingTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumBuildingInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiFreeBuildingCount[eIndex];
}


bool CvPlayer::isBuildingFree(BuildingTypes eIndex)	const
{
	return (getFreeBuildingCount(eIndex) > 0);
}


void CvPlayer::changeFreeBuildingCount(BuildingTypes eIndex, int iChange)
{
	CvCity* pLoopCity;
	int iOldFreeBuildingCount;
	int iLoop;

	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumBuildingInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		iOldFreeBuildingCount = getFreeBuildingCount(eIndex);

		m_paiFreeBuildingCount[eIndex] = (m_paiFreeBuildingCount[eIndex] + iChange);
		FAssert(getFreeBuildingCount(eIndex) >= 0);

		if (iOldFreeBuildingCount == 0)
		{
			FAssertMsg(getFreeBuildingCount(eIndex) > 0, "getFreeBuildingCount(eIndex) is expected to be greater than 0");

			for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
			{
				pLoopCity->setHasFreeBuilding(eIndex, true);
			}
		}
		else if (getFreeBuildingCount(eIndex) == 0)
		{
			FAssertMsg(iOldFreeBuildingCount > 0, "iOldFreeBuildingCount is expected to be greater than 0");

			for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
			{
				pLoopCity->setHasFreeBuilding(eIndex, false);
			}
		}
	}
}

bool CvPlayer::isHasFirstBuildingBuilt(BuildingTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumBuildingInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_pabFirstBuildingBuilt[eIndex];
}

void CvPlayer::setHasFirstBuildingBuilt(BuildingTypes eIndex, bool bChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumBuildingInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");

	if (isHasFirstBuildingBuilt(eIndex) != bChange)
	{
		m_pabFirstBuildingBuilt[eIndex] = bChange;
	}
}

int CvPlayer::getUnitClassCount(UnitClassTypes eIndex) const
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	return m_paiUnitClassCount[eIndex];
}

void CvPlayer::changeUnitClassCount(UnitClassTypes eIndex, int iChange)
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	m_paiUnitClassCount[eIndex] += iChange;
	FAssert(getUnitClassCount(eIndex) >= 0);
}

int CvPlayer::getEnrolUnitType(UnitClassTypes eIndex) const
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	return m_paiEnrolUnitType[eIndex];
}

void CvPlayer::setEnrolUnitType(UnitClassTypes eIndex, int iValue)
{
	if (getEnrolUnitType(eIndex) != iValue)
	{
		m_paiEnrolUnitType[eIndex] = iValue;
	}
}
void CvPlayer::changeEnrolUnitType(UnitClassTypes eIndex, bool bIncrease, bool bManual)
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	int iMaxValue = GC.getMAX_ENROL_UNIT();
	int iCurrentValue = getEnrolUnitType(eIndex);
	if (bIncrease) 
	{
		m_paiEnrolUnitType[eIndex] = (iCurrentValue < iMaxValue) ? iCurrentValue + 1 : -1;
	} 
	else if (bManual) 
	{
		m_paiEnrolUnitType[eIndex] = (iCurrentValue > -1) ? iCurrentValue - 1 : iMaxValue; // It is cyclical when we decrease manually
	} 
	else if (iCurrentValue > 0) 
	{
		m_paiEnrolUnitType[eIndex] = iCurrentValue - 1;
	}
}

int CvPlayer::getUnitClassMaking(UnitClassTypes eIndex) const
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	return m_paiUnitClassMaking[eIndex];
}

void CvPlayer::changeUnitClassMaking(UnitClassTypes eIndex, int iChange)
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());

	if (iChange != 0)
	{
		m_paiUnitClassMaking[eIndex] += iChange;
		FAssert(getUnitClassMaking(eIndex) >= 0);

		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(Help_DIRTY_BIT, true);
		}
	}
}

int CvPlayer::getUnitMarketPrice(UnitClassTypes eIndex) const
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	return m_paiUnitMarketPrice[eIndex];
}

void CvPlayer::changeUnitMarketPrice(UnitClassTypes eIndex, int iChange)
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());

	if (iChange != 0)
	{
		m_paiUnitMarketPrice[eIndex] += iChange;
	}
}

int CvPlayer::getUnitKilledCounter(UnitClassTypes eIndex) const
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	return m_paiUnitKilledCounter[eIndex];
}

void CvPlayer::changeUnitKilledCounter(UnitClassTypes eIndex, int iChange)
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());

	if (iChange != 0)
	{
		m_paiUnitKilledCounter[eIndex] += iChange;
	}
}

void CvPlayer::helpOftheKing(UnitClassTypes eUnitClassType)
{
	// If it is not an european, it will not have king so no help from king :)
	if (!isEuropean())
	{
		return;
	}

	// If less than two units has been kill, we don't help him
	if (getUnitKilledCounter(eUnitClassType) < 2) 
	{
		return;
	}

	// If the player has more than two units, we don't help him too
	int iUnitCount = getUnitClassCount(eUnitClassType) - 1;
	if (iUnitCount >= 2) 
	{
		return;
	}

	UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClassType);

	// For the moment we only help the player for agronomist
	if (eUnit != UNIT_AGRONOME) 
	{
		return;
	}

	int iUnitGivedByTheKing = 2 - iUnitCount + GC.getGameINLINE().getSorenRandNum(2, "iUnitGivedByTheKing agronomist");
	iUnitGivedByTheKing = std::min(iUnitGivedByTheKing, 2);

	// We buy agronomist in Europe
	for (int i = 0; i < iUnitGivedByTheKing; ++i) 
	{
		CvUnit* pUnit = buyEuropeUnit(eUnit, 0);
		changeEuropeLoan(getEuropeUnitBuyPrice(eUnit));
	}

	// We display the message to the player
	if (iUnitGivedByTheKing > 0 && isHuman()) 
	{
		CvDiploParameters* pDiplo = new CvDiploParameters(getParent());
		pDiplo->setDiploComment((DiploCommentTypes) GC.getInfoTypeForString("AI_DIPLO_KING_SEND_AGRONOMISTS_IF_SOME_WERE_KILLED"));
		pDiplo->addDiploCommentVariable(GC.getUnitInfo(eUnit).getDescription());
		pDiplo->setAIContact(true);
		gDLL->beginDiplomacy(pDiplo, getID());
	}
}

int CvPlayer::getUnitTurnRemaining(UnitClassTypes eIndex) const
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	return m_paiUnitTurnRemaining[eIndex];
}

void CvPlayer::changeUnitTurnRemaining(UnitClassTypes eIndex, int iChange)
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());

	if (iChange != 0)
	{
		m_paiUnitTurnRemaining[eIndex] += iChange;
		FAssert(getUnitTurnRemaining(eIndex) >= 0);
	}
}
void CvPlayer::processNewShipBuyInEurope()
{
	for (int i = 0; i<GC.getNumUnitClassInfos(); i++)
	{
		UnitClassTypes eUnitClass = (UnitClassTypes) i;
		if (getUnitTurnRemaining(eUnitClass) > 0)
		{
			if (getUnitTurnRemaining(eUnitClass) == 1)
			{
				CvPlot* pStartingPlot = getStartingPlot();
				if (pStartingPlot != NULL)
				{
					UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClass);
					if (NO_UNIT != eUnit)
					{
						CvUnit* pUnit = initUnit(eUnit, NO_PROFESSION, INVALID_PLOT_COORD, INVALID_PLOT_COORD);
						if (pUnit != NULL)
						{
							pUnit->setUnitTravelState(UNIT_TRAVEL_STATE_IN_EUROPE, false);
							pUnit->setHasCrew(false);
							pUnit->addToMap(pStartingPlot->getX_INLINE(), pStartingPlot->getY_INLINE());
							CvPlayerAI& kPlayer = GET_PLAYER(getID());
							if (kPlayer.AI_getWarPlanUnitClass(eUnitClass) > 0)
							{
								pUnit->AI_setReservedForWarPlan(true);
								kPlayer.AI_decreaseWarPlanUnitClass(eUnitClass);
							}
						}
					}
				}
			}
			changeUnitTurnRemaining((UnitClassTypes)i, -1);
		}
	}
}

void CvPlayer::processUnitMarketPrice()
{
	if (isEurope())
	{
		int iRandom, iRandom2, iRandom3;
		for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass)
		{
			iRandom = GC.getGameINLINE().getSorenRandNum(100, "random 1 processUnitMarketPrice");
			if (iRandom%2 == 0)
			{
				iRandom2 = GC.getGameINLINE().getSorenRandNum(100, "random 2 processUnitMarketPrice") - 50;
				if (getUnitMarketPrice((UnitClassTypes)iUnitClass) < iRandom2)
				{
					iRandom3 = 1 + GC.getGameINLINE().getSorenRandNum(100, "random 3 processUnitMarketPrice")%3;
					changeUnitMarketPrice((UnitClassTypes)iUnitClass, iRandom3);
				}
				else
				{
					iRandom3 = 1 + GC.getGameINLINE().getSorenRandNum(100, "random 3 processUnitMarketPrice")%3;
					changeUnitMarketPrice((UnitClassTypes)iUnitClass, -iRandom3);
				}
			}
		}
	}
}
void CvPlayer::initYieldPrice()
{
	int iRandom, iEuropeStock;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		iRandom = GC.getGameINLINE().getSorenRandNum(1000, "random  Process")%40 - 20;
		iEuropeStock = GC.getYieldInfo(eYield).getEuropeStock();
		if (iEuropeStock)
		{
			changeEuropeWarehouseYield(eYield, iEuropeStock + iEuropeStock*iRandom/100);
		}
	}
}

void CvPlayer::processYieldPrice()
{
	if (!isEurope())
	{
		return;
	}

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		CvYieldInfo kYield = GC.getYieldInfo(eYield);
		
		int iEuropeWarehouseStock = getEuropeWarehouseYield(eYield);
		int iTargetStock = kYield.getEuropeStock();

		// We want to fluctuate market price depends on stock and needs
		if (kYield.isRawMaterial())
		{
			processYieldEconomicTrend(eYield);
		}
		else
		{
			int iNewValue = getYieldRandomPercent(eYield) - 4 + GC.getGameINLINE().getSorenRandNum(9, "Update YieldRandomPercent");
			iNewValue = range(iNewValue, 80, 120);
			setYieldRandomPercent(eYield, iNewValue);
		}
	}
}

void CvPlayer::processYieldEconomicTrend(YieldTypes eYield)
{
	if (getEconomicTrendsRemainingTurns(eYield) > 0)
	{
		changeEconomicTrendsRemainingTurns(eYield, -1);

		EconomicTrendTypes eEconomicTrend = getEconomicTrend(eYield);
		if (NO_ECONOMIC_TREND == eEconomicTrend)
		{
			return;
		}
				
		CvEconomicTrendInfo& kInfo = GC.getEconomicTrendInfo(eEconomicTrend);
		int iChangeProbabilty = kInfo.getChangeProbability();
		int iRandomValue = GC.getGameINLINE().getSorenRandNum(100, "ChangeProbabilty Needs for Yield");
		if (iRandomValue <= iChangeProbabilty)
		{
			int iChangeDiff = kInfo.getMaxChangeNeedsPerTurn() - kInfo.getMinChangeNeedsPerTurn();
			int iChangeRandomValue = GC.getGameINLINE().getSorenRandNum(iChangeDiff, "ChangeNeedsPerTurn for Yield");
			int iChangeValue = kInfo.getMinChangeNeedsPerTurn() + iChangeRandomValue;
			unsigned char iNewValue = range(getEuropeNeeds(eYield) + iChangeValue, 0, 100);
			setEuropeNeeds(eYield, iNewValue);
		}

		return;
	}

	EconomicTrendTypes eNewEconomicTrend = pickEconomicTrend(eYield);
	if (NO_ECONOMIC_TREND == eNewEconomicTrend)
	{
		return;
	}
	
	CvEconomicTrendInfo& kInfo = GC.getEconomicTrendInfo(eNewEconomicTrend);
	int iNewTurnDuration = kInfo.getMinTurnDuration() + GC.getGameINLINE().getSorenRandNum(kInfo.getMaxTurnDuration() - kInfo.getMinTurnDuration(), "New remaining turn for Economic Trend");
	
	setEconomicTrend(eYield, eNewEconomicTrend);
	setEconomicTrendsRemainingTurns(eYield, iNewTurnDuration);
}

EconomicTrendTypes CvPlayer::pickEconomicTrend(YieldTypes eYield)
{
	std::vector<int> aiWeights(GC.getNumEconomicTrendInfos(), 0);
	bool hasSetWeight = false;
	for (int iEconomicTrend = 0; iEconomicTrend < GC.getNumEconomicTrendInfos(); ++iEconomicTrend)
	{
		EconomicTrendTypes eEconomicTrend = (EconomicTrendTypes) iEconomicTrend;
		if (NO_ECONOMIC_TREND != eEconomicTrend)
		{
			CvEconomicTrendInfo& kInfo = GC.getEconomicTrendInfo(eEconomicTrend);
			int iWeight = kInfo.getWeight();

			int iEuropeNeeds = getEuropeNeeds(eYield);
			if (kInfo.getMinEuropeNeeds() <= iEuropeNeeds && iEuropeNeeds <= kInfo.getMaxEuropeNeeds())
			{
				aiWeights[eEconomicTrend] = iWeight;
				hasSetWeight = true;
			}
		}
	}

	if (!hasSetWeight)
	{
		return NO_ECONOMIC_TREND;
	}

	return (EconomicTrendTypes) GC.getGameINLINE().getSorenRand().pickValue(aiWeights, "pick Economic Trend");
}

int CvPlayer::getUnitClassCountPlusMaking(UnitClassTypes eIndex) const
{
	return (getUnitClassCount(eIndex) + getUnitClassMaking(eIndex));
}

int CvPlayer::getUnitMoveChange(UnitClassTypes eIndex) const
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	return m_paiUnitMoveChange[eIndex];
}

void CvPlayer::changeUnitMoveChange(UnitClassTypes eIndex, int iChange)
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	if (iChange != 0)
	{
		m_paiUnitMoveChange[eIndex] += iChange;
	}
}

int CvPlayer::getUnitStrengthModifier(UnitClassTypes eIndex) const
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	return m_paiUnitStrengthModifier[eIndex];
}

void CvPlayer::changeUnitStrengthModifier(UnitClassTypes eIndex, int iChange)
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumUnitClassInfos());
	if (iChange != 0)
	{
		m_paiUnitStrengthModifier[eIndex] += iChange;
	}
}

int CvPlayer::getProfessionMoveChange(ProfessionTypes eIndex) const
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumProfessionInfos());
	return m_paiProfessionMoveChange[eIndex];
}

void CvPlayer::changeProfessionMoveChange(ProfessionTypes eIndex, int iChange)
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumProfessionInfos());
	if (iChange != 0)
	{
		m_paiProfessionMoveChange[eIndex] += iChange;
	}
}

int CvPlayer::getProfessionCombatChange(ProfessionTypes eIndex) const
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumProfessionInfos());
	return m_paiProfessionCombatChange[eIndex];
}

void CvPlayer::changeProfessionCombatChange(ProfessionTypes eIndex, int iChange)
{
	FAssert(eIndex >= 0 && eIndex < GC.getNumProfessionInfos());
	if (iChange != 0)
	{
		int iLoop;

		for (CvCity* pCity = firstCity(&iLoop); pCity != NULL; pCity = nextCity(&iLoop))
		{
			pCity->AI_setWorkforceHack(true);
		}

		for (CvUnit* pUnit = firstUnit(&iLoop); pUnit != NULL; pUnit = nextUnit(&iLoop))
		{
			pUnit->processProfession(pUnit->getProfession(), -1, false);
		}

		for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
		{
			m_aEuropeUnits[i]->processProfession(m_aEuropeUnits[i]->getProfession(), -1, false);
		}

		for (CvCity* pCity = firstCity(&iLoop); pCity != NULL; pCity = nextCity(&iLoop))
		{
			for (int i = 0; i < pCity->getPopulation(); ++i)
			{
				CvUnit* pUnit = pCity->getPopulationUnitByIndex(i);
				if (NULL != pUnit)
				{
					pUnit->processProfession(pUnit->getProfession(), -1, false);
				}
			}
		}

		m_paiProfessionCombatChange[eIndex] += iChange;

		for (CvUnit* pUnit = firstUnit(&iLoop); pUnit != NULL; pUnit = nextUnit(&iLoop))
		{
			pUnit->processProfession(pUnit->getProfession(), 1, false);
		}

		for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
		{
			m_aEuropeUnits[i]->processProfession(m_aEuropeUnits[i]->getProfession(), 1, false);
		}

		for (CvCity* pCity = firstCity(&iLoop); pCity != NULL; pCity = nextCity(&iLoop))
		{
			for (int i = 0; i < pCity->getPopulation(); ++i)
			{
				CvUnit* pUnit = pCity->getPopulationUnitByIndex(i);
				if (NULL != pUnit)
				{
					pUnit->processProfession(pUnit->getProfession(), 1, false);
				}
			}
		}

		for (CvCity* pCity = firstCity(&iLoop); pCity != NULL; pCity = nextCity(&iLoop))
		{
			pCity->AI_setWorkforceHack(false);
		}
	}
}

int CvPlayer::getBuildingClassCount(BuildingClassTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumBuildingClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiBuildingClassCount[eIndex];
}

void CvPlayer::changeBuildingClassCount(BuildingClassTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumBuildingClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_paiBuildingClassCount[eIndex] = (m_paiBuildingClassCount[eIndex] + iChange);
	FAssert(getBuildingClassCount(eIndex) >= 0);
}


int CvPlayer::getBuildingClassMaking(BuildingClassTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumBuildingClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiBuildingClassMaking[eIndex];
}


void CvPlayer::changeBuildingClassMaking(BuildingClassTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumBuildingClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		m_paiBuildingClassMaking[eIndex] = (m_paiBuildingClassMaking[eIndex] + iChange);
		FAssert(getBuildingClassMaking(eIndex) >= 0);

		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(Help_DIRTY_BIT, true);
		}
	}
}


int CvPlayer::getBuildingClassCountPlusMaking(BuildingClassTypes eIndex) const
{
	return (getBuildingClassCount(eIndex) + getBuildingClassMaking(eIndex));
}

int CvPlayer::getSpecialBuildingNotRequiredCount(SpecialBuildingTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumSpecialBuildingInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiSpecialBuildingNotRequiredCount[eIndex];
}


bool CvPlayer::isSpecialBuildingNotRequired(SpecialBuildingTypes eIndex) const
{
	return (getSpecialBuildingNotRequiredCount(eIndex) > 0);
}

void CvPlayer::changeSpecialBuildingNotRequiredCount(SpecialBuildingTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumSpecialBuildingInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_paiSpecialBuildingNotRequiredCount[eIndex] = (m_paiSpecialBuildingNotRequiredCount[eIndex] + iChange);
	FAssert(getSpecialBuildingNotRequiredCount(eIndex) >= 0);
}


CivicTypes CvPlayer::getCivic(CivicOptionTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumCivicOptionInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paeCivics[eIndex];
}

void CvPlayer::setCivic(CivicOptionTypes eIndex, CivicTypes eNewValue)
{
	CivicTypes eOldCivic = getCivic(eIndex);

	if (eOldCivic != eNewValue)
	{
		m_paeCivics[eIndex] = eNewValue;

		if (eOldCivic != NO_CIVIC)
		{
			processCivics(eOldCivic, -1);
		}

		if (getCivic(eIndex) != NO_CIVIC)
		{
			processCivics(getCivic(eIndex), 1);
		}

		GC.getGameINLINE().AI_makeAssignWorkDirty();

		if (GC.getGameINLINE().isFinalInitialized())
		{
			if (gDLL->isDiplomacy() && (gDLL->getDiplomacyPlayer() == getID()))
			{
				gDLL->updateDiplomacyAttitude(true);
			}

			if (getCivic(eIndex) != NO_CIVIC)
			{
				if (getCivic(eIndex) != GC.getCivilizationInfo(getCivilizationType()).getCivilizationInitialCivics(eIndex))
				{
					for (int iI = 0; iI < MAX_PLAYERS; iI++)
					{
						if (GET_PLAYER((PlayerTypes)iI).isAlive())
						{
							if (GET_TEAM(getTeam()).isHasMet(GET_PLAYER((PlayerTypes)iI).getTeam()))
							{
								CvWString szBuffer = gDLL->getText("TXT_KEY_MISC_PLAYER_ADOPTED_CIVIC", getNameKey(), GC.getCivicInfo(getCivic(eIndex)).getTextKeyWide());
								gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MAJOR_EVENT);
							}
						}
					}

					CvWString szBuffer = gDLL->getText("TXT_KEY_MISC_PLAYER_ADOPTED_CIVIC", getNameKey(), GC.getCivicInfo(getCivic(eIndex)).getTextKeyWide());
					GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getID(), szBuffer);
				}
			}
		}
	}
}


int CvPlayer::getImprovementYieldChange(ImprovementTypes eIndex1, YieldTypes eIndex2) const
{
	FAssertMsg(eIndex1 >= 0, "eIndex1 is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex1 < GC.getNumImprovementInfos(), "eIndex1 is expected to be within maximum bounds (invalid Index)");
	FAssertMsg(eIndex2 >= 0, "eIndex2 is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex2 < NUM_YIELD_TYPES, "eIndex2 is expected to be within maximum bounds (invalid Index)");
	return m_ppiImprovementYieldChange[eIndex1][eIndex2];
}


void CvPlayer::changeImprovementYieldChange(ImprovementTypes eIndex1, YieldTypes eIndex2, int iChange)
{
	FAssertMsg(eIndex1 >= 0, "eIndex1 is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex1 < GC.getNumImprovementInfos(), "eIndex1 is expected to be within maximum bounds (invalid Index)");
	FAssertMsg(eIndex2 >= 0, "eIndex2 is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex2 < NUM_YIELD_TYPES, "eIndex2 is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		m_ppiImprovementYieldChange[eIndex1][eIndex2] += iChange;
		FAssert(getImprovementYieldChange(eIndex1, eIndex2) >= 0);

		updateYield();
	}
}

int CvPlayer::getBuildingYieldChange(BuildingClassTypes eBuildingClass, YieldTypes eYield) const
{
	FAssert(eBuildingClass >= 0);
	FAssert(eBuildingClass < GC.getNumBuildingClassInfos());
	FAssert(eYield >= 0);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_ppiBuildingYieldChange[eBuildingClass][eYield];
}

void CvPlayer::changeBuildingYieldChange(BuildingClassTypes eBuildingClass, YieldTypes eYield, int iChange)
{
	FAssert(eBuildingClass >= 0);
	FAssert(eBuildingClass < GC.getNumBuildingClassInfos());
	FAssert(eYield >= 0);
	FAssert(eYield < NUM_YIELD_TYPES);

	if (iChange != 0)
	{
		m_ppiBuildingYieldChange[eBuildingClass][eYield] += iChange;
		FAssert(getBuildingYieldChange(eBuildingClass, eYield) >= 0);

		updateYield();
	}
}

int CvPlayer::getTaxYieldModifierCount(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiTaxYieldModifierCount[eYield];
}

void CvPlayer::changeTaxYieldModifierCount(YieldTypes eYield, int iChange) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	m_aiTaxYieldModifierCount[eYield] += iChange;
}
int CvPlayer::getEuropeWarehouseYield(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiEuropeWarehouseYield[eYield];
}

void CvPlayer::changeEuropeWarehouseYield(YieldTypes eYield, int iChange)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	m_aiEuropeWarehouseYield[eYield] += iChange;
}
int CvPlayer::getEuropeBuyTradeYield(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiEuropeBuyTradeYield[eYield];
}

void CvPlayer::setEuropeBuyTradeYield(YieldTypes eYield, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getEuropeBuyTradeYield(eYield) != iValue)
	{
		m_aiEuropeBuyTradeYield[eYield] = iValue;
	}
}
void CvPlayer::changeEuropeBuyTradeYield(YieldTypes eYield, int iChange)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	setEuropeBuyTradeYield(eYield, getEuropeBuyTradeYield(eYield) + iChange);
}
int CvPlayer::getEuropeGoldBuyTradeYield(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiEuropeGoldBuyTradeYield[eYield];
}

void CvPlayer::setEuropeGoldBuyTradeYield(YieldTypes eYield, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getEuropeGoldBuyTradeYield(eYield) != iValue)
	{
		m_aiEuropeGoldBuyTradeYield[eYield] = iValue;
	}
}
void CvPlayer::changeEuropeGoldBuyTradeYield(YieldTypes eYield, int iChange)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	setEuropeGoldBuyTradeYield(eYield, getEuropeGoldBuyTradeYield(eYield) + iChange);
}
int CvPlayer::getEuropeSellTradeYield(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiEuropeSellTradeYield[eYield];
}

void CvPlayer::setEuropeSellTradeYield(YieldTypes eYield, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getEuropeSellTradeYield(eYield) != iValue)
	{
		m_aiEuropeSellTradeYield[eYield] = iValue;
	}
}

void CvPlayer::changeEuropeSellTradeYield(YieldTypes eYield, int iChange)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	setEuropeSellTradeYield(eYield, getEuropeSellTradeYield(eYield) + iChange);
}
int CvPlayer::getEuropeGoldSellTradeYield(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiEuropeGoldSellTradeYield[eYield];
}

void CvPlayer::setEuropeGoldSellTradeYield(YieldTypes eYield, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getEuropeGoldSellTradeYield(eYield) != iValue)
	{
		m_aiEuropeGoldSellTradeYield[eYield] = iValue;
	}
}

void CvPlayer::changeEuropeGoldSellTradeYield(YieldTypes eYield, int iChange)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	setEuropeGoldSellTradeYield(eYield, getEuropeGoldSellTradeYield(eYield) + iChange);
}
int CvPlayer::getEuropeTaxTradeYield(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiEuropeTaxTradeYield[eYield];
}

void CvPlayer::setEuropeTaxTradeYield(YieldTypes eYield, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getEuropeTaxTradeYield(eYield) != iValue)
	{
		m_aiEuropeTaxTradeYield[eYield] = iValue;
	}
}

unsigned char CvPlayer::getEuropeNeeds(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	// Europe Needs will automatically increase with time.
	return GC.getGameINLINE().getGameTurn() % 12 + m_aucEuropeNeeds[eYield];
}

void CvPlayer::setEuropeNeeds(YieldTypes eYield, unsigned char iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getEuropeNeeds(eYield) != iValue)
	{
		m_aucEuropeNeeds[eYield] = iValue;
	}
}

void CvPlayer::initEuropeNeeds()
{
	if (!isEurope())
	{
		return;
	}

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		CvYieldInfo& kYield = GC.getYieldInfo(eYield);
		
		if (kYield.isRawMaterial())
		{
			setEuropeNeeds(eYield, 30 + GC.getGameINLINE().getSorenRandNum(50, "init initEuropeNeeds"));
		}
	}
}

int CvPlayer::getEuropeConsumption(YieldTypes eYield) const
{
	int iMinEuropeConsumption = 10; // TODO put it in Global Var
	int iMaxEuropeConsumption = 100; // TODO put it in Global Var
	return iMinEuropeConsumption + getEuropeNeeds(eYield) * (iMaxEuropeConsumption - iMinEuropeConsumption) / 100;
}

void CvPlayer::changeEuropeNeeds(YieldTypes eYield, int iValue)
{
	setEuropeNeeds(eYield, getEuropeNeeds(eYield) + iValue);
}

int CvPlayer::getPricePercent(YieldTypes eYield) const
{
	return (getEuropeNeeds(eYield) * getEuropeQuantitySuppliedPercent(eYield)) / 100;
}

int CvPlayer::getEuropeQuantitySuppliedPercent(YieldTypes eYield) const
{
	int iConsumption = getEuropeConsumption(eYield);
	int iQuantitySupplied = getEuropeWarehouseYield(eYield);
	int iMinCoeffMultiAllowed = 5; // TODO put it in Global Var
	int iMaxCoeffMultiAllowed = 10; // TODO put it in Global Var

	if (iQuantitySupplied > iMinCoeffMultiAllowed * iConsumption)
	{
		return std::max(0, (100 - iQuantitySupplied * 100 / (iConsumption * iMaxCoeffMultiAllowed)));
	}

	if (iQuantitySupplied >= iConsumption)
	{
		return 100;
	}

	return 125 - iQuantitySupplied * 100 / iConsumption;
}


int CvPlayer::getIATradeProposition(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiIATradeProposition[eYield];
}

void CvPlayer::setIATradeProposition(YieldTypes eYield, int iValue, bool bSync)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (bSync)
	{
		gDLL->sendPlayerAction(getID(), PLAYER_ACTION_TRADE_PROPOSITION, eYield, iValue, 0);//Synchronize
	}
	if (getIATradeProposition(eYield) != iValue)
	{
		m_aiIATradeProposition[eYield] = iValue;
	}
}

int CvPlayer::getHumanTradeProposition(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiHumanTradeProposition[eYield];
}

void CvPlayer::setHumanTradeProposition(YieldTypes eYield, int iValue, bool bSync)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (bSync)
	{
		gDLL->sendPlayerAction(getID(), PLAYER_ACTION_TRADE_PROPOSITION, eYield, iValue, 1);//Synchronize
	}

	if (getHumanTradeProposition(eYield) != iValue)
	{
		m_aiHumanTradeProposition[eYield] = iValue;
	}
}

int CvPlayer::whatKindOfTradeProposal()
{
	bool bTestDon = false, bTestReq = false;
	int iResult = -1;
	/*retourne:
	0 -> Une donnation du joueur humain
	1 -> Une requete du joueur humain
	2 -> Un echange
	*/
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (!bTestDon && getHumanTradeProposition(eYield) > 0)
		{
			bTestDon = true;
		}
		if (!bTestReq && getIATradeProposition(eYield) > 0)
		{
			bTestReq = true;
		}
	}
	if (bTestReq && bTestDon)
	{
		iResult = 2;
	}
	else
	{
		if (bTestReq)
		{
			iResult = 1;
		}
		if (bTestDon)
		{
			iResult = 0;
		}
	}
	return iResult;
}
int CvPlayer::getNativeGetUnknownYield(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiNativeGetUnknownYield[eYield];
}

void CvPlayer::setNativeGetUnknownYield(YieldTypes eYield, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getNativeGetUnknownYield(eYield) != iValue)
	{
		m_aiNativeGetUnknownYield[eYield] = iValue;
	}
}
int CvPlayer::getNumUnknownYield() const
{
	int iResult = 0;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		CvYieldInfo& kYield = GC.getYieldInfo(eYield);
		if (kYield.isUnknown() && getNativeGetUnknownYield(eYield))
		{
			iResult++;
		}
	}
	return iResult;
}

int CvPlayer::getTotalTradeYields()
{
	int iResult = 0;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;

		if (eYield != YIELD_AMMUNITION)
		{
			iResult += getYieldTradedTotal(eYield);
		}
	}
	return iResult;
}

void CvPlayer::changeEuropeTaxTradeYield(YieldTypes eYield, int iChange)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	setEuropeTaxTradeYield(eYield, getEuropeTaxTradeYield(eYield) + iChange);
}

void CvPlayer::resetEuropeTradeYield()
{
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		setEuropeSellTradeYield(eYield, 0);
		setEuropeBuyTradeYield(eYield, 0);
		setEuropeTaxTradeYield(eYield, 0);
		setEuropeGoldSellTradeYield(eYield, 0);
		setEuropeGoldBuyTradeYield(eYield, 0);
	}
}

int CvPlayer::getBuyPriceForYield(YieldTypes eYield, int iQuantity) const
{
	PlayerTypes eEuropePlayer = getParent();
	CvPlayer& pEuropePlayer = GET_PLAYER(eEuropePlayer != NO_PLAYER ? eEuropePlayer : getID());
	return pEuropePlayer.getYieldSellPrice(eYield) * iQuantity;
}

int CvPlayer::getSellPriceForYield(YieldTypes eYield, int iQuantity) const
{
	PlayerTypes eEuropePlayer = getParent();
	CvPlayer& pEuropePlayer = GET_PLAYER(eEuropePlayer != NO_PLAYER ? eEuropePlayer : getID());
	return pEuropePlayer.getYieldBuyPrice(eYield) * iQuantity;
}

// XXX should pUnit be a CvSelectionGroup???
void CvPlayer::updateGroupCycle(CvUnit* pUnit)
{
	CLLNode<IDInfo>* pUnitNode;
	CLLNode<int>* pSelectionGroupNode;
	CLLNode<int>* pBestSelectionGroupNode;
	CvSelectionGroup* pLoopSelectionGroup;
	CvUnit* pHeadUnit;
	CvUnit* pBeforeUnit;
	CvUnit* pAfterUnit;
	CvUnit* pLoopUnit;
	CvPlot* pPlot;
	int iValue;
	int iBestValue;

	if (!(pUnit->isOnMap()))
	{
		return;
	}

	FAssertMsg(pUnit->getGroup() != NULL, "Unit->getGroup() is not assigned a valid value");

	removeGroupCycle(pUnit->getGroupID());

	pPlot = pUnit->plot();

	pBeforeUnit = NULL;
	pAfterUnit = NULL;

	pUnitNode = pPlot->headUnitNode();

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);

		if (pLoopUnit->isGroupHead())
		{
			if (pLoopUnit != pUnit)
			{
				if (!isBeforeUnitCycle(pLoopUnit, pUnit))
				{
					pBeforeUnit = pLoopUnit;
					break;
				}
				else
				{
					pAfterUnit = pLoopUnit;
				}
			}
		}
	}

	pSelectionGroupNode = headGroupCycleNode();

	iBestValue = MAX_INT;
	pBestSelectionGroupNode = NULL;

	while (pSelectionGroupNode != NULL)
	{
		pLoopSelectionGroup = getSelectionGroup(pSelectionGroupNode->m_data);
		FAssertMsg(pLoopSelectionGroup != NULL, "LoopSelectionGroup is not assigned a valid value");

		pHeadUnit = pLoopSelectionGroup->getHeadUnit();

		if (pHeadUnit != NULL)
		{
			if (pBeforeUnit != NULL)
			{
				if (pBeforeUnit == pHeadUnit)
				{
					pBestSelectionGroupNode = pSelectionGroupNode;
					break;
				}
			}
			else if (pAfterUnit != NULL)
			{
				if (pAfterUnit == pHeadUnit)
				{
					pBestSelectionGroupNode = nextGroupCycleNode(pSelectionGroupNode);
					break;
				}
			}
			else
			{
				iValue = plotDistance(pUnit->getX_INLINE(), pUnit->getY_INLINE(), pHeadUnit->getX_INLINE(), pHeadUnit->getY_INLINE());

				if (iValue < iBestValue)
				{
					iBestValue = iValue;
					pBestSelectionGroupNode = pSelectionGroupNode;
				}
			}
		}

		pSelectionGroupNode = nextGroupCycleNode(pSelectionGroupNode);
	}

	if (pBestSelectionGroupNode != NULL)
	{
		m_groupCycle.insertBefore(pUnit->getGroupID(), pBestSelectionGroupNode);
	}
	else
	{
		m_groupCycle.insertAtEnd(pUnit->getGroupID());
	}
}


void CvPlayer::removeGroupCycle(int iID)
{
	CLLNode<int>* pSelectionGroupNode;

	pSelectionGroupNode = headGroupCycleNode();

	while (pSelectionGroupNode != NULL)
	{
		if (pSelectionGroupNode->m_data == iID)
		{
			pSelectionGroupNode = deleteGroupCycleNode(pSelectionGroupNode);
			break;
		}
		else
		{
			pSelectionGroupNode = nextGroupCycleNode(pSelectionGroupNode);
		}
	}
}

CLLNode<int>* CvPlayer::deleteGroupCycleNode(CLLNode<int>* pNode)
{
	return m_groupCycle.deleteNode(pNode);
}

CLLNode<int>* CvPlayer::nextGroupCycleNode(CLLNode<int>* pNode) const
{
	return m_groupCycle.next(pNode);
}

CLLNode<int>* CvPlayer::previousGroupCycleNode(CLLNode<int>* pNode) const
{
	return m_groupCycle.prev(pNode);
}

CLLNode<int>* CvPlayer::headGroupCycleNode() const
{
	return m_groupCycle.head();
}

CLLNode<int>* CvPlayer::tailGroupCycleNode() const
{
	return m_groupCycle.tail();
}
void CvPlayer::addCityName(const CvWString& szName)
{
	m_aszCityNames.push_back(szName);
}

int CvPlayer::getNumCityNames() const
{
	return m_aszCityNames.size();
}

const CvWString& CvPlayer::getCityName(int iIndex) const
{
	return m_aszCityNames[iIndex];
}

CvCity* CvPlayer::firstCity(int *pIterIdx, bool bRev) const
{
	return !bRev ? m_cities.beginIter(pIterIdx) : m_cities.endIter(pIterIdx);
}

CvCity* CvPlayer::nextCity(int *pIterIdx, bool bRev) const
{
	return !bRev ? m_cities.nextIter(pIterIdx) : m_cities.prevIter(pIterIdx);
}


int CvPlayer::getNumCities() const
{
	return m_cities.getCount();
}


CvCity* CvPlayer::getCity(int iID) const
{
	return(m_cities.getAt(iID));
}


CvCity* CvPlayer::addCity()
{
	return(m_cities.add());
}


void CvPlayer::deleteCity(int iID)
{
	m_cities.removeAt(iID);
}

// -----------------------------------------
//
//    START : DOANE Seaway functions
//
// -----------------------------------------

int CvPlayer::getNumSeaways() const
{
	return m_seaways.getLength();
}

CvSeaway* CvPlayer::getSeaway(int iIndex) const
{
	CLLNode<CvSeaway>* pSeawayNode;

	pSeawayNode = getNodeSeaway(iIndex);

	return (pSeawayNode != NULL) ?  &(pSeawayNode->m_data) : NULL;
}

CLLNode<CvSeaway>* CvPlayer::getNodeSeaway(int iIndex) const
{
	return m_seaways.nodeNum(iIndex);
}


CvSeaway* CvPlayer::getSeawayByID(int iID) const
{
	CLLNode<CvSeaway>* pSeawayNode = headSeawayNode();

	while (pSeawayNode != NULL)
	{
		if (pSeawayNode->m_data.getID() == iID)
		{
			return &(pSeawayNode->m_data);
		}
		pSeawayNode = nextSeawayNode(pSeawayNode);
	}
	return NULL;
}

CLLNode<CvSeaway>* CvPlayer::headSeawayNode() const
{
	return m_seaways.head();
}

CLLNode<CvSeaway>* CvPlayer::nextSeawayNode(CLLNode<CvSeaway>* pNode) const
{
	return m_seaways.next(pNode);
}

void CvPlayer::deleteSeaway(int iIndex)
{
	CLLNode<CvSeaway>* pSeawayNode = getNodeSeaway(iIndex);
	if (pSeawayNode != NULL)
	{
		m_seaways.deleteNode(pSeawayNode);
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
			GC.getGameINLINE().updateColoredPlots();
		}
	}
}

void CvPlayer::deleteSeawayByID(int iID)
{
	CLLNode<CvSeaway>* pSeawayNode = headSeawayNode();

	while (pSeawayNode != NULL)
	{
		if (pSeawayNode->m_data.getID() == iID)
		{
			m_seaways.deleteNode(pSeawayNode);
			GC.getGameINLINE().updateColoredPlots();
		}
		pSeawayNode = nextSeawayNode(pSeawayNode);
	}
	if (getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
		gDLL->getInterfaceIFace()->setDirty(TradeRoutes_DIRTY_BIT, true);
		GC.getGameINLINE().updateColoredPlots();
	}
}

CvSeaway* CvPlayer::findSeaway(CvPlot* pPlot) const
{
	CLLNode<CvSeaway>* pSeawayNode = headSeawayNode();

	while (pSeawayNode != NULL)
	{
		if (pSeawayNode->m_data.isSamePlot(pPlot))
		{
			return &(pSeawayNode->m_data);
		}
		pSeawayNode = nextSeawayNode(pSeawayNode);
	}
	return NULL;
}

int CvPlayer::distanceWithTheClosestSeaway(CvPlot* pPlot) const
{
	int iDistance = MAX_INT;
	CLLNode<CvSeaway>* pSeawayNode = headSeawayNode();

	while (pSeawayNode != NULL)
	{
		int iCurrentDistance = stepDistance(pPlot->getX_INLINE(), pPlot->getY_INLINE(), pSeawayNode->m_data.getX(), pSeawayNode->m_data.getY());
		if (iCurrentDistance < iDistance)
		{
			iDistance = iCurrentDistance;
		}
		pSeawayNode = nextSeawayNode(pSeawayNode);
	}
	return iDistance;
}

void CvPlayer::addSeaway(CvPlot* pPlot, const CvWString& szName)
{
	if (pPlot != NULL && findSeaway(pPlot) == NULL)
	{
		CvSeaway pSeaway;
		pSeaway.init(szName, pPlot->getX(), pPlot->getY());
		m_seaways.insertAtEnd(pSeaway);
		gDLL->getEngineIFace()->addColoredPlot(pPlot->getX_INLINE(), pPlot->getY_INLINE(), GC.getColorInfo((ColorTypes)GC.getInfoTypeForString("COLOR_GREEN")).getColor(), PLOT_STYLE_CIRCLE, PLOT_LANDSCAPE_LAYER_BASE);

		CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
		while (pUnitNode != NULL)
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);

			if (pLoopUnit->IsSelected())
			{
				pLoopUnit->reloadEntity();
			}
		}
	}
}

CvSeaway* CvPlayer::getSeawayNearCity(CvCity* pCity) const
{
	CvSeaway* pBestSeaway = NULL;
	int iIndex, iDistance, iPosXCity, iPosYCity;
	int iMinDist = MAX_INT;

	if (pCity == NULL)
	{
		pCity = firstCity(&iIndex);
	}

	if (pCity == NULL)
	{
		return NULL;
	}
	iPosXCity = pCity->getX_INLINE();
	iPosYCity = pCity->getY_INLINE();

	CLLNode<CvSeaway>* pSeawayNode;

	pSeawayNode = headSeawayNode();

	while (pSeawayNode != NULL)
	{
		iDistance = stepDistance(pSeawayNode->m_data.getX(), pSeawayNode->m_data.getY(), iPosXCity, iPosYCity);
		if (iDistance < iMinDist)
		{
			iMinDist = iDistance;
			pBestSeaway = &(pSeawayNode->m_data);
		}
		pSeawayNode = nextSeawayNode(pSeawayNode);
	}
	return pBestSeaway;
}

void CvPlayer::sendUpNodeSeaway(int iIndex)
{
	CLLNode<CvSeaway>* pSeawayNode = getNodeSeaway(iIndex);
	if (pSeawayNode != NULL)
	{
		m_seaways.moveUp(pSeawayNode);
	}
}
void CvPlayer::sendDownNodeSeaway(int iIndex)
{
	CLLNode<CvSeaway>* pSeawayNode = getNodeSeaway(iIndex);
	if (pSeawayNode != NULL)
	{
		m_seaways.moveDown(pSeawayNode);
	}
}
// -----------------------------------------
//
//    END : DOANE Seaway functions
//
// -----------------------------------------

CvUnit* CvPlayer::firstUnit(int *pIterIdx) const
{
	if (m_units.empty())
	{
		return NULL;
	}

	CvIdVector<CvUnitAI>::const_iterator it = m_units.begin();
	CvUnit* pUnit = it->second;

	++it;
	if (it == m_units.end())
	{
		*pIterIdx = -1;
	}
	else
	{
		*pIterIdx = it->first;
	}

	return pUnit;
}


CvUnit* CvPlayer::nextUnit(int *pIterIdx) const
{
	CvIdVector<CvUnitAI>::const_iterator it = m_units.find(*pIterIdx);

	if (it == m_units.end())
	{
		return NULL;
	}

	CvUnit* pUnit = it->second;

	++it;
	if (it == m_units.end())
	{
		*pIterIdx = -1;
	}
	else
	{
		*pIterIdx = it->first;
	}

	return pUnit;
}


int CvPlayer::getNumUnits() const
{
	return (int)(m_units.size());
}


CvUnit* CvPlayer::getUnit(int iID) const
{
	return (m_units.getById(iID));
}


CvUnit* CvPlayer::addUnit()
{
	return (m_units.addNew());
}

void CvPlayer::addExistingUnit(CvUnit *pUnit)
{
	CvUnitAI* pUnitAI = dynamic_cast<CvUnitAI*>(pUnit);
	FAssert(pUnitAI != NULL);
	m_units[pUnitAI->getID()] = pUnitAI;
}

void CvPlayer::deleteUnit(int iID)
{
	m_units.removeById(iID);
}

CvUnit* CvPlayer::getAndRemoveUnit(int iId)
{
	CvUnit* pUnit = m_units.getById(iId);
	if (pUnit != NULL)
	{
		pUnit->removeFromMap();
		m_units.erase(pUnit->getID());
	}

	return pUnit;
}

int CvPlayer::getNumEuropeUnitsWithoutSailors() const
{
	int iCount = 0;
	for (int i = 0; i < getNumEuropeUnits(); ++i)
	{
		CvUnit* pUnit = getEuropeUnit(i);
		if (!pUnit->canBeSailor())
		{
			iCount++;
		}
	}
	return iCount;
}

int CvPlayer::getNumEuropeUnits() const
{
	return m_aEuropeUnits.size();
}

CvUnit* CvPlayer::getEuropeUnit(int iIndex) const
{
	FAssert(iIndex >= 0);
	FAssert(iIndex < (int)m_aEuropeUnits.size());
	return m_aEuropeUnits[iIndex];
}

CvUnit* CvPlayer::getEuropeUnitById(int iId) const
{
	for (int i = 0; i < getNumEuropeUnits(); ++i)
	{
		CvUnit* pUnit = getEuropeUnit(i);
		if (pUnit->getID() == iId)
		{
			return pUnit;
		}
	}

	return NULL;
}

void CvPlayer::loadUnitFromEurope(CvUnit* pUnit, CvUnit* pTransport)
{
	FAssert(NULL != pUnit && NULL != pTransport);
	if (NULL != pUnit && NULL != pTransport)
	{
		if (pUnit->canLoadUnit(pTransport, pTransport->plot(), false))
		{
			//transfer to player
			std::vector<CvUnit*>::iterator it = std::remove(m_aEuropeUnits.begin(), m_aEuropeUnits.end(), pUnit);
			if (it == m_aEuropeUnits.end())
			{
				FAssertMsg(false, "Could not find europe unit.");
			}
			else
			{
				m_aEuropeUnits.erase(it);
				addExistingUnit(pUnit);
				FAssert(pUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);
				pUnit->addToMap(pTransport->getX_INLINE(), pTransport->getY_INLINE());
				pUnit->setTransportUnit(pTransport);
				gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
				gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
			}
		}
	}
}
void CvPlayer::killUnitFromEurope(CvUnit* pUnit)
{
	FAssert(NULL != pUnit);

	if (NULL != pUnit)
	{
		CvCityJob* pJob = pUnit->getCityJob();
		if (pJob != NULL)
		{
			pJob->removeUnit(pUnit);
		}
		std::vector<CvUnit*>::iterator it = std::remove(m_aEuropeUnits.begin(), m_aEuropeUnits.end(), pUnit);
		if (it == m_aEuropeUnits.end())
		{
			FAssertMsg(false, "Could not find europe unit.");
		}
		else
		{
			m_aEuropeUnits.erase(it);
		}
	}
}

void CvPlayer::unloadUnitToEurope(CvUnit* pUnit)
{
	FAssert(pUnit != NULL);
	if (NULL != pUnit)
	{
		FAssertMsg(pUnit->getYield() == NO_YIELD, "Yields are meant to be sold not unloaded");
		FAssertMsg(pUnit->getYieldStored() == 0, "Yields and Treasures are meant to be sold, not unloaded");
		UnitAITypes eUnitAI = pUnit->AI_getUnitAIType();
		CvUnit* pTransferUnit = getAndRemoveUnit(pUnit->getID());
		FAssert(pTransferUnit == pUnit);
		m_aEuropeUnits.push_back(pTransferUnit);
		pTransferUnit->AI_setUnitAIType(eUnitAI);
		pTransferUnit->setUnitTravelState(UNIT_TRAVEL_STATE_IN_EUROPE, false);
		gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
		gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
	}
}

void CvPlayer::transferUnitInEurope(CvUnit* pUnit, CvUnit* pTransport)
{
	FAssert(NULL != pUnit && NULL != pTransport);
	if (NULL != pUnit && NULL != pTransport)
	{
		if (pUnit->canLoadUnit(pTransport, pTransport->plot(), false))
		{
			pUnit->setTransportUnit(pTransport);
			gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
		}
		else
		{
			if (pUnit->getUnitInfo().isMechUnit() && pTransport->getUnitInfo().isMechUnit())
			{
				CvSelectionGroup* pGroup = pTransport->getGroup();
				if (pGroup == NULL)
				{
					pTransport->joinGroup(NULL);
					pGroup = pTransport->getGroup();
				}
				if (pGroup != NULL)
				{
					CvPlot* pPlot = pTransport->plot();
					if (pPlot != NULL)
					{
						if (!pUnit->at(pPlot->getX(), pPlot->getY()))
						{
							pUnit->setXY(pPlot->getX(), pPlot->getY());
						}
					}
					else
					{
						pUnit->setXY(INVALID_PLOT_COORD, INVALID_PLOT_COORD);
					}

					pUnit->joinGroup(pGroup);
					gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
				}
			}
		}
	}
}

int CvPlayer::countNumTravelUnits(UnitTravelStates eState, DomainTypes eDomain) const
{
	int iCount = 0;
	int iLoop;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		if (eDomain == NO_DOMAIN || pLoopUnit->getDomainType() == eDomain)
		{
			if (pLoopUnit->getUnitTravelState() == eState)
			{
				if (!pLoopUnit->isInEuropeDrydock())
				{
					++iCount;
				}
			}
		}
	}

	return iCount;
}

int CvPlayer::countNumDomainUnits(DomainTypes eDomain) const
{
	int iCount = 0;
	int iLoop;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		if (eDomain == NO_DOMAIN || pLoopUnit->getDomainType() == eDomain)
		{
			++iCount;
		}
	}
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		for (int i = 0; i < pLoopCity->getPopulation(); ++i)
		{
			if (eDomain == NO_DOMAIN || pLoopCity->getPopulationUnitByIndex(i)->getDomainType() == eDomain)
			{
				++iCount;
			}
		}
	}
	for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
	{
		if (eDomain == NO_DOMAIN || m_aEuropeUnits[i]->getDomainType() == eDomain)
		{
			++iCount;
		}
	}

	return iCount;
}

CvSelectionGroup* CvPlayer::firstSelectionGroup(int *pIterIdx, bool bRev) const
{
	return !bRev ? m_selectionGroups.beginIter(pIterIdx) : m_selectionGroups.endIter(pIterIdx);
}


CvSelectionGroup* CvPlayer::nextSelectionGroup(int *pIterIdx, bool bRev) const
{
	return !bRev ? m_selectionGroups.nextIter(pIterIdx) : m_selectionGroups.prevIter(pIterIdx);
}


int CvPlayer::getNumSelectionGroups() const
{
	return m_selectionGroups.getCount();
}


CvSelectionGroup* CvPlayer::getSelectionGroup(int iID) const
{
	return ((CvSelectionGroup *)(m_selectionGroups.getAt(iID)));
}

CvSelectionGroup* CvPlayer::addSelectionGroup()
{
	return ((CvSelectionGroup *)(m_selectionGroups.add()));
}

void CvPlayer::deleteSelectionGroup(int iID)
{
	bool bRemoved = m_selectionGroups.removeAt(iID);

	FAssertMsg(bRemoved, "could not find group, delete failed");
}

EventTriggeredData* CvPlayer::firstEventTriggered(int *pIterIdx, bool bRev) const
{
	return !bRev ? m_eventsTriggered.beginIter(pIterIdx) : m_eventsTriggered.endIter(pIterIdx);
}

EventTriggeredData* CvPlayer::nextEventTriggered(int *pIterIdx, bool bRev) const
{
	return !bRev ? m_eventsTriggered.nextIter(pIterIdx) : m_eventsTriggered.prevIter(pIterIdx);
}

int CvPlayer::getNumEventsTriggered() const
{
	return m_eventsTriggered.getCount();
}

EventTriggeredData* CvPlayer::getEventTriggered(int iID) const
{
	return ((EventTriggeredData*)(m_eventsTriggered.getAt(iID)));
}

EventTriggeredData* CvPlayer::addEventTriggered()
{
	return ((EventTriggeredData*)(m_eventsTriggered.add()));
}

void CvPlayer::deleteEventTriggered(int iID)
{
	m_eventsTriggered.removeAt(iID);
}


void CvPlayer::addMessage(const CvTalkingHeadMessage& message)
{
	m_listGameMessages.push_back(message);
}


void CvPlayer::clearMessages()
{
	m_listGameMessages.clear();
}


const CvMessageQueue& CvPlayer::getGameMessages() const
{
	return (m_listGameMessages);
}


void CvPlayer::expireMessages()
{
	CvMessageQueue::iterator it = m_listGameMessages.begin();
	bool bFoundExpired = false;
	while(it != m_listGameMessages.end())
	{
		CvTalkingHeadMessage& message = *it;
		if (GC.getGameINLINE().getGameTurn() >= message.getExpireTurn())
		{
			it = m_listGameMessages.erase(it);
			bFoundExpired = true;
		}
		else
		{
			++it;
		}
	}
	if (bFoundExpired)
	{
		gDLL->getInterfaceIFace()->dirtyTurnLog(getID());
	}
}

void CvPlayer::addPopup(CvPopupInfo* pInfo, bool bFront)
{
	if (isHuman())
	{
		if (bFront)
		{
			m_listPopups.push_front(pInfo);
		}
		else
		{
			m_listPopups.push_back(pInfo);
		}
	}
	else
	{
		FAssertMsg(false, "Adding popup to AI");
		SAFE_DELETE(pInfo);
	}
}


void CvPlayer::clearPopups()
{
	CvPopupQueue::iterator it;
	for (it = m_listPopups.begin(); it != m_listPopups.end(); ++it)
	{
		CvPopupInfo* pInfo = *it;
		if (NULL != pInfo)
		{
			delete pInfo;
		}
	}
	m_listPopups.clear();
}


CvPopupInfo* CvPlayer::popFrontPopup()
{
	CvPopupInfo* pInfo = NULL;
	if (!m_listPopups.empty())
	{
		pInfo = m_listPopups.front();
		m_listPopups.pop_front();
	}
	return pInfo;
}


const CvPopupQueue& CvPlayer::getPopups() const
{
	return (m_listPopups);
}


void CvPlayer::addDiplomacy(CvDiploParameters* pDiplo)
{
	if (NULL != pDiplo)
	{
		m_listDiplomacy.push_back(pDiplo);
	}
}


void CvPlayer::clearDiplomacy()
{
	CvDiploQueue::iterator it;
	for (it = m_listDiplomacy.begin(); it != m_listDiplomacy.end(); ++it)
	{
		CvDiploParameters* pDiplo = *it;
		if (NULL != pDiplo)
		{
			delete pDiplo;
		}
	}
	m_listDiplomacy.clear();
}


const CvDiploQueue& CvPlayer::getDiplomacy() const
{
	return (m_listDiplomacy);
}


CvDiploParameters* CvPlayer::popFrontDiplomacy()
{
	CvDiploParameters* pDiplo = NULL;
	if (!m_listDiplomacy.empty())
	{
		pDiplo = m_listDiplomacy.front();
		m_listDiplomacy.pop_front();
	}
	return pDiplo;
}


int CvPlayer::getScoreHistory(int iTurn) const
{
	CvTurnScoreMap::const_iterator it = m_mapScoreHistory.find(iTurn);
	if (it != m_mapScoreHistory.end())
	{
		return (*it).second;
	}
	return 0;
}

void CvPlayer::updateScoreHistory(int iTurn, int iBestScore)
{
	m_mapScoreHistory[iTurn] = iBestScore;
}

int CvPlayer::getEconomyHistory(int iTurn) const
{
	CvTurnScoreMap::const_iterator it = m_mapEconomyHistory.find(iTurn);
	if (it != m_mapEconomyHistory.end())
	{
		return (*it).second;
	}
	return 0;
}

void CvPlayer::updateEconomyHistory()
{
	int iTurn = GC.getGameINLINE().getGameTurn(),
		iLoop;
	long	iBestEconomy = 0,
		iGoldScore = 0,
		iShipsScore = 0,
		iBuildingScore = 0,
		iCitiesPopulation = 0;


	// Gold Score part
	iGoldScore = getGold() / 300;

	//Ships Score part
	PlayerTypes eEuropePlayer = getParent();
	if (eEuropePlayer != NO_PLAYER)
	{
		int iTotalCannon = 0;
		for (CvUnit* pLoopUnit = firstUnit(&iLoop); NULL != pLoopUnit; pLoopUnit = nextUnit(&iLoop))
		{
			if (pLoopUnit->getUnitInfo().isMechUnit())
			{
				iShipsScore += pLoopUnit->getUnitInfo().getEuropeCost();
				int iNumCannon = pLoopUnit->getNbCannon();
				if (iNumCannon > 0)
				{
					iNumCannon += iNumCannon;
				} 
			}
		}
		iShipsScore += getSellPriceForYield(YIELD_CANNON, iTotalCannon);
		for (int i = 0; i < GC.getNumUnitClassInfos(); i++)
		{
			UnitClassTypes eUnitClassType = (UnitClassTypes) i;
			if (getUnitTurnRemaining(eUnitClassType) > 0)
			{
				UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClassType);
				if (eUnit != NO_UNIT)
				{
					iShipsScore += GC.getUnitInfo(eUnit).getEuropeCost();
				}
			}
		}
	}
	iShipsScore /= 300;

	//Building Score
	int iTotalHammers = 0;
	int iScoreByHammer = 2;
	for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop)) 
	{
		if (!pLoopCity->isDisorder()) 
		{
			for (int i = 0; i < GC.getNumBuildingInfos(); ++i)
			{
				BuildingTypes eBuilding = (BuildingTypes) i;
				if (pLoopCity->isHasBuilding(eBuilding)) 
				{
					iTotalHammers += GC.getBuildingInfo(eBuilding).getYieldCost(YIELD_HAMMERS);
				}
			}
		}
	}
	iBuildingScore = iTotalHammers*iScoreByHammer;

	//City Population Score
	int iNumPopulation = 0;
	int iScoreByPopulation = 20;
	for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop)) 
	{
		if (!pLoopCity->isDisorder()) 
		{
			iNumPopulation += pLoopCity->getPopulation();
		}
	}
	iCitiesPopulation = iScoreByPopulation*iNumPopulation;

	//Best Econony Total
	iBestEconomy = iGoldScore + iShipsScore + iBuildingScore + iCitiesPopulation;

	m_mapEconomyHistory[iTurn] = (int) iBestEconomy;
}

int CvPlayer::getIndustryHistory(int iTurn) const
{
	CvTurnScoreMap::const_iterator it = m_mapIndustryHistory.find(iTurn);
	if (it != m_mapIndustryHistory.end())
	{
		return (*it).second;
	}
	return 0;
}

void CvPlayer::updateIndustryHistory(int iTurn, int iBestIndustry)
{
	m_mapIndustryHistory[iTurn] = iBestIndustry;
}

int CvPlayer::getAgricultureHistory(int iTurn) const
{
	CvTurnScoreMap::const_iterator it = m_mapAgricultureHistory.find(iTurn);
	if (it != m_mapAgricultureHistory.end())
	{
		return (*it).second;
	}
	return 0;
}

void CvPlayer::updateAgricultureHistory(int iTurn, int iBestAgriculture)
{
	m_mapAgricultureHistory[iTurn] = iBestAgriculture;
}

int CvPlayer::getLandMilitaryHistory(int iTurn) const
{
	CvTurnScoreMap::const_iterator it = m_mapLandMilitaryHistory.find(iTurn);
	if (it != m_mapLandMilitaryHistory.end())
	{
		return (*it).second;
	}
	return 0;
}

int CvPlayer::getSeaMilitaryHistory(int iTurn) const
{
	CvTurnScoreMap::const_iterator it = m_mapSeaMilitaryHistory.find(iTurn);
	if (it != m_mapSeaMilitaryHistory.end())
	{
		return (*it).second;
	}
	return 0;
}

int CvPlayer::getNavalMerchantHistory(int iTurn) const
{
	CvTurnScoreMap::const_iterator it = m_mapNavalMerchantHistory.find(iTurn);
	if (it != m_mapNavalMerchantHistory.end())
	{
		return (*it).second;
	}
	return 0;
}


void CvPlayer::updateUnitsHistory()
{
	int iTurn = GC.getGameINLINE().getGameTurn(),
		iLoop,
		iArtilleryValue = 0,
		iBestCapacity = 0,
		iTotalExp = 0,
		iNumUnits = 0;

	int	iLandForce = 0,
		iNavalForce = 0,
		iMerchantCapacity = 0;

	for (CvUnit* pLoopUnit = firstUnit(&iLoop); NULL != pLoopUnit; pLoopUnit = nextUnit(&iLoop))
	{
		if (!pLoopUnit->getUnitInfo().isMechUnit())
		{
			if (pLoopUnit->baseOriginCombatStr() > 0)
			{
				iLandForce += pLoopUnit->baseOriginCombatStr();
			}
			else if (pLoopUnit->isArtilleryMen())
			{
				iLandForce += iArtilleryValue;
			}
		}
		else
		{
			if (pLoopUnit->isOnlyDefensive())
			{
				int iSpace = pLoopUnit->getUnitInfo().getCargoSpace();
				if (iSpace > 0)
				{
					iMerchantCapacity += iSpace/2;
					iTotalExp += pLoopUnit->getExperienceExploAndNavigation();
					iNumUnits++;
				}	
			}
			else
			{
				int iBaseCombat = pLoopUnit->baseOriginCombatStr();
				int iAgeUnit = iTurn - pLoopUnit->getGameTurnCreated();
				int iProgressPercent = iAgeUnit >= 10 ? 100 : iAgeUnit*100/10; 
				iNavalForce += (iBaseCombat/2)*iProgressPercent/100;
				int iExpPercent = std::min(100, pLoopUnit->getExperience() * 100 / 5) +  pLoopUnit->getExperienceExploAndNavigation() * 100 / 300;
				iExpPercent /= 2;
				iNavalForce += (iBaseCombat/2)*iExpPercent/100;
			}
		}
	}

	int iExpPercent = iNumUnits == 0 ? 0 : (iTotalExp/iNumUnits)*100/300;
	iMerchantCapacity += iMerchantCapacity*iExpPercent/100;

	m_mapNavalMerchantHistory[iTurn] = iMerchantCapacity;

	m_mapLandMilitaryHistory[iTurn] = iLandForce;

	m_mapSeaMilitaryHistory[iTurn] = iNavalForce;
}

int CvPlayer::getCultureHistory(int iTurn) const
{
	CvTurnScoreMap::const_iterator it = m_mapCultureHistory.find(iTurn);
	if (it != m_mapCultureHistory.end())
	{
		return (*it).second;
	}
	return 0;
}

void CvPlayer::updateCultureHistory(int iTurn, int iBestCulture)
{
	m_mapCultureHistory[iTurn] = iBestCulture;
}

int CvPlayer::getTotalImmigration(UnitTypes eUnit) const
{
	CvImmigrationIntInfoMap::const_iterator it = m_mapTotalImmigration.find(eUnit);
	if (it != m_mapTotalImmigration.end())
	{
		return (*it).second;
	}
	return 0;
}

void CvPlayer::incrementTotalImmigration(UnitTypes eUnit)
{
	m_mapTotalImmigration[eUnit]++;
}

int CvPlayer::getTotalImmigrationAsked(UnitTypes eUnit) const
{
	CvImmigrationIntInfoMap::const_iterator it = m_mapTotalImmigrationAsked.find(eUnit);
	if (it != m_mapTotalImmigrationAsked.end())
	{
		return (*it).second;
	}
	return 0;
}

void CvPlayer::incrementTotalImmigrationAsked(UnitTypes eUnit)
{
	m_mapTotalImmigrationAsked[eUnit]++;
}

int CvPlayer::getTurnCountWithoutProposition(UnitTypes eUnit) const
{
	CvImmigrationIntInfoMap::const_iterator it = m_mapTurnCountWithoutProposition.find(eUnit);
	if (it != m_mapTurnCountWithoutProposition.end())
	{
		return (*it).second;
	}
	return 0;
}

void CvPlayer::incrementTurnCountWithoutProposition(UnitTypes eUnit)
{
	m_mapTurnCountWithoutProposition[eUnit]++;
}

void CvPlayer::resetTurnCountWithoutProposition(UnitTypes eUnit)
{
	m_mapTurnCountWithoutProposition[eUnit] = 0;
}

int CvPlayer::getMaxTurnWithoutProposition(UnitTypes eUnit) const
{
	CvImmigrationIntInfoMap::const_iterator it = m_mapMaxTurnWithoutProposition.find(eUnit);
	if (it != m_mapMaxTurnWithoutProposition.end())
	{
		return (*it).second;
	}
	return 0;
}

void CvPlayer::udpateMaxTurnCountWithoutPropositionIfNeeded(UnitTypes eUnit)
{
	int iCurrentValue = getTurnCountWithoutProposition(eUnit);
	if (iCurrentValue > getMaxTurnWithoutProposition(eUnit))
	{
		m_mapMaxTurnWithoutProposition[eUnit] = iCurrentValue;
	}
	resetTurnCountWithoutProposition(eUnit);
}

float CvPlayer::getCurrentImmigrationProbability(UnitTypes eUnit) const
{
	CvImmigrationFloatInfoMap::const_iterator it = m_mapCurrentImmigrationProbability.find(eUnit);
	if (it != m_mapCurrentImmigrationProbability.end())
	{
		return (*it).second;
	}
	return 0.0;
}

void CvPlayer::udpateCurrentImmigrationProbability(UnitTypes eUnit, float iProbability)
{
	if (m_mapCurrentImmigrationProbability[eUnit] == 0)
	{
		resetCurrentImmigrationProbability(eUnit);
	}
	m_mapCurrentImmigrationProbability[eUnit] *= iProbability;
}

void CvPlayer::resetCurrentImmigrationProbability(UnitTypes eUnit)
{
	m_mapCurrentImmigrationProbability[eUnit] = 1;
}

float CvPlayer::getFirstUnitImmigrationProbability(UnitTypes eUnit) const
{
	CvImmigrationFloatInfoMap::const_iterator it = m_mapFirstUnitImmigrationProbability.find(eUnit);
	if (it != m_mapFirstUnitImmigrationProbability.end())
	{
		return (*it).second;
	}
	return 0.0;
}

void CvPlayer::udpateFirstUnitImmigrationProbabilityIfNeeded(UnitTypes eUnit)
{
	if (getTotalImmigration(eUnit) == 1)
	{
		m_mapFirstUnitImmigrationProbability[eUnit] = getCurrentImmigrationProbability(eUnit);
	}
}

float CvPlayer::getAverageImmigrationProbability(UnitTypes eUnit) const
{
	CvImmigrationFloatInfoMap::const_iterator it = m_mapAverageImmigrationProbability.find(eUnit);
	if (it != m_mapAverageImmigrationProbability.end())
	{
		return (*it).second;
	}
	return 0.0;
}

void CvPlayer::udpateAverageImmigrationProbabilityIfNeeded(UnitTypes eUnit)
{
	int iTotalImmigration = getTotalImmigration(eUnit);
	if (iTotalImmigration > 0)
	{
		m_mapAverageImmigrationProbability[eUnit] = (m_mapAverageImmigrationProbability[eUnit] * (iTotalImmigration - 1) + getCurrentImmigrationProbability(eUnit)) / iTotalImmigration;
	}
}


int CvPlayer::getImmigrationHistory(int iTurn, UnitTypes eUnit) const
{
	CvTurnImmigrationMap::const_iterator it = m_mapImmigrationHistory.find(iTurn);
	if (it != m_mapImmigrationHistory.end())
	{
		stdext::hash_map<int, int> mUnitTypes = (*it).second;
		stdext::hash_map<int, int>::const_iterator itUnitType = mUnitTypes.find(eUnit);
		if (itUnitType != mUnitTypes.end())
		{
			return (*itUnitType).second;
		}
	}
	return 0;
}

void CvPlayer::doImmigrationHistory()
{
	if (!isEuropean()) 
	{
		return;
	}

	int iTurn = GC.getGameINLINE().getGameTurn();
	for (int iI = 0; iI < GC.getNumUnitInfos(); iI++)
	{
		UnitTypes eUnit = (UnitTypes) iI;
		if (GC.getUnitInfo(eUnit).getProbaImmigration() > 0) 
		{
			m_mapImmigrationHistory[iTurn][eUnit] = getTotalImmigration(eUnit);
		}
	}
}

//Start Agreement part
int CvPlayer::addAgreement(const IDInfo& kSource, const IDInfo& kDestination, CvAgreement* pOriginalAgreement)
{
	CvCity* pSourceCity = ::getCity(kSource);
	FAssert(pSourceCity != NULL);
	if (pSourceCity == NULL)
	{
		return -1;
	}
	CvCity* pDestinationCity = ::getCity(kDestination);
	FAssert(pDestinationCity != NULL || (kDestination.eOwner == getID() && kDestination.iID == CvAgreement::EUROPE_CITY_ID));

	if (kSource == kDestination)
	{
		return -1;
	}

	CvAgreement* pAgreement = m_agreements.addNew();
	pAgreement->init(pAgreement->getID(), kSource, kDestination);

	if (pOriginalAgreement != NULL)
	{
		pAgreement->copyAgreement(pOriginalAgreement);
	}

	return pAgreement->getID();
}

bool CvPlayer::removeAgreement(int iId, int iCase)
{
	CvAgreement* pAgreement = getAgreement(iId);
	FAssert(pAgreement != NULL);
	if (pAgreement == NULL) 
	{
		return false;
	}
	CvCity* pSouceCity = ::getCity(pAgreement->getSourceCity());
	CvCity* pDestCity = ::getCity(pAgreement->getDestinationCity());


	if (pSouceCity == NULL || (iCase != 1 && pDestCity == NULL))
	{
		return m_agreements.removeById(iId);
	}

	
	YieldTypes eWantedYield = pDestCity != NULL ? pDestCity->getWantedYield() : NO_YIELD;

	bool bHasSentPopup = false;
	int iLoop;
	int iNewId = -1;
	for (CvSelectionGroup* pLoopSelectionGroup = firstSelectionGroup(&iLoop); pLoopSelectionGroup; pLoopSelectionGroup = nextSelectionGroup(&iLoop))
	{
		if (pAgreement->isAssignedGroup(pLoopSelectionGroup->getID())) 
		{
			pLoopSelectionGroup->assignAgreement(iId, false);
			// We stop the contract
			if (iCase == 1) 
			{
				if (pLoopSelectionGroup->plot() != pSouceCity->plot() && !pLoopSelectionGroup->isAssignedAgreements()) 
				{
					//A vérifier lorsqu'une unité est en Europe
					pLoopSelectionGroup->pushMission(MISSION_MOVE_TO, pSouceCity->getX_INLINE(), pSouceCity->getY_INLINE(), MOVE_NO_ENEMY_TERRITORY, false, false, MISSIONAI_TRANSPORT, pSouceCity->plot());
				}
			}
			else
			{
				if (eWantedYield != NO_YIELD && hasAutomaticTradeByYieldWithNative(eWantedYield)) 
				{
					if (iNewId == -1) 
					{
						CvPlayer& kOtherPlayer = GET_PLAYER(pDestCity->getOwner());
						iNewId = addAgreement(pSouceCity->getIDInfo(), pDestCity->getIDInfo());
						CvAgreement* pNewAgreement = getAgreement(iNewId);

						int iNumAgreementProposition = kOtherPlayer.getNumAgreementProposition(getID());

						int iTurns = kOtherPlayer.getAgreementMaxTurn(iNumAgreementProposition);
						int iQuantity = kOtherPlayer.getAgreementQuantity(iNumAgreementProposition);
						int iIncrease = 65; // Trade benefice in percent for the Native. (Default is 70)
						pNewAgreement->setTurnMax(iTurns);

						pNewAgreement->addYieldTrade(pAgreement->getImportYield(), TRADE_IMPORT, iQuantity);
						pNewAgreement->setLevel(iNumAgreementProposition);

						int iNumWantedYield = kOtherPlayer.getNumWantedValue(iQuantity, eWantedYield, iIncrease);
						pNewAgreement->addYieldTrade(eWantedYield, TRADE_EXPORT, iNumWantedYield);
						if (getID() == GC.getGameINLINE().getActivePlayer()) 
						{
							CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_AUTOMATIC_RENEW_AGREEMENT, iNewId, -1, true);
							gDLL->getInterfaceIFace()->addPopup(pInfo, getID());
						}
					}

					if (pLoopSelectionGroup->canAssignAgreement(iNewId))
					{
						pLoopSelectionGroup->assignAgreement(iNewId, true);
					}
				}
				// We renew the contract
				else if (!bHasSentPopup) 
				{
					CvUnit* pHeadUnit = pLoopSelectionGroup->getHeadUnit();
					if (pLoopSelectionGroup->plot() == pDestCity->plot() && !pLoopSelectionGroup->isAssignedAgreements()) 
					{					
						if (getID() == GC.getGameINLINE().getActivePlayer()) 
						{
							CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_NEW_AGREEMENT, pHeadUnit->getID(), pSouceCity->getID(), true);
							gDLL->getInterfaceIFace()->addPopup(pInfo, getID());
						}
						bHasSentPopup = true;
					}
				}
			}
		}
	}

	return m_agreements.removeById(iId);
}

void CvPlayer::removeAllAgreementOfTeam(TeamTypes eTeam)
{
	int iNumAgreements = getNumAgreements();
	if (iNumAgreements == 0)
	{
		return;
	}
	std::vector<int> listToRemove;
	int iLoop;
	for (CvAgreement* pAgreement = firstAgreement(&iLoop); pAgreement != NULL; pAgreement = nextAgreement(&iLoop))
	{
		PlayerTypes eCityPlayer = pAgreement->getDestinationCity().eOwner;
		if (eCityPlayer != NO_PLAYER)
		{
			CvPlayer& kPlayer = GET_PLAYER(eCityPlayer);
			if (kPlayer.getTeam() == eTeam)
			{
				listToRemove.push_back(pAgreement->getID());
			}
		}
	}
	for (std::vector<int>::iterator it = listToRemove.begin(); it != listToRemove.end(); ++it)
	{
		removeAgreement(*it);
	}
}


void CvPlayer::checkAgreementsDeadline()
{
	bool bContinue = true;
	int iCount = 0;
	CvIdVector<CvAgreement>::const_iterator it;
	while(bContinue)
	{
		bContinue = false;
		iCount++;
		for (it = m_agreements.begin(); it != m_agreements.end(); ++it)
		{
			CvAgreement* pAgreement = it->second;
			if (pAgreement != NULL)
			{
				if (pAgreement->isOver())
				{
					if (isHuman())
					{
						//Le commercant nous transporte les marchandises
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_END_OF_AGREEMENT, pAgreement->getID(), -1, -1);
						gDLL->getInterfaceIFace()->addPopup(pInfo, getID());
					}
				}
				else if (pAgreement->isDeadline())
				{
					CvCity* pCity = ::getCity(pAgreement->getDestinationCity());
					if (isHuman())
					{
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_END_OF_AGREEMENT, pAgreement->getID(), pAgreement->getMissingYieldsPercent(TRADE_EXPORT), -1);
						gDLL->getInterfaceIFace()->addPopup(pInfo, getID());
					}
					else
					{
						removeAgreement(pAgreement->getID());
						bContinue = true;//After removed we redo the check
						break;
					}
					if (pCity != NULL && GET_PLAYER(pCity->getOwner()).isNative())
					{
						pCity->randomWantedYield();
					}
				}
			}
		}
		if (iCount > 100)
		{
			CvString szError;
			szError.Format("checkAgreementsDeadline WARNING More than 100 agremments or infinity loop");
			gDLL->logMsg("DoaNE.log", szError);
			break;
		}
	}
}

CvAgreement* CvPlayer::getAgreement(int iId) const
{
	if (getParent() != NO_PLAYER)
	{
		CvAgreement* pAgreement = m_agreements.getById(iId);
		FAssert(pAgreement == NULL || pAgreement->getSourceCity().eOwner == getID());
		return pAgreement;
	}
	else
	{
		//On récupère tous les accords des "fils"
		for (int iPlayer=0;iPlayer<MAX_PLAYERS;iPlayer++)
		{
			CvPlayer& kChild = GET_PLAYER((PlayerTypes) iPlayer);
			if (kChild.isAlive() && (kChild.getParent() == getID()))
			{
				CvAgreement* pAgreement = kChild.getAgreement(iId);
				if (pAgreement != NULL && pAgreement->getDestinationCity().iID == CvTradeRoute::EUROPE_CITY_ID)
				{
					return pAgreement;
				}
			}
		}
	}
	return NULL;
}

int CvPlayer::getNumAgreements() const
{
	return m_agreements.size();
}

void CvPlayer::getAgreements(std::vector<CvAgreement*>& aAgreements) const
{
	if (getParent() != NO_PLAYER)
	{
		CvIdVector<CvAgreement>::const_iterator it;
		for (it = m_agreements.begin(); it != m_agreements.end(); ++it)
		{
			aAgreements.push_back(it->second);
		}
	}
	else
	{
		//On récupère tous les accords des "fils"
		for (int iPlayer=0;iPlayer<MAX_PLAYERS;iPlayer++)
		{
			CvPlayer& kChild = GET_PLAYER((PlayerTypes) iPlayer);
			if (kChild.isAlive() && (kChild.getParent() == getID()))
			{
				std::vector<CvAgreement*> aiTempAgreements;
				kChild.getAgreements(aiTempAgreements);
				for (uint i = 0; i < aiTempAgreements.size(); ++i)
				{
					aAgreements.push_back(aiTempAgreements[i]);
				}
			}
		}
	}
}

void CvPlayer::getAgreementsOfGroup(std::vector<CvAgreement*>& aAgreements, int iGroupID) const
{
	if (getParent() != NO_PLAYER)
	{
		CvIdVector<CvAgreement>::const_iterator it;
		for (it = m_agreements.begin(); it != m_agreements.end(); ++it)
		{
			CvAgreement* pAgreement = it->second;
			if (pAgreement->isAssignedGroup(iGroupID))
			{
				aAgreements.push_back(it->second);
			}
		}
	}
	else
	{
		//On récupère tous les accords des "fils"
		for (int iPlayer=0;iPlayer<MAX_PLAYERS;iPlayer++)
		{
			CvPlayer& kChild = GET_PLAYER((PlayerTypes) iPlayer);
			if (kChild.isAlive() && (kChild.getParent() == getID()))
			{
				std::vector<CvAgreement*> aiTempAgreements;
				kChild.getAgreements(aiTempAgreements);
				for (uint i = 0; i < aiTempAgreements.size(); ++i)
				{
					CvAgreement* pAgreement = aiTempAgreements[i];
					if (pAgreement->isAssignedGroup(iGroupID))
					{
						aAgreements.push_back(pAgreement);
					}
				}
			}
		}
	}
}

int CvPlayer::getAgreementOfCity(CvCity* pCity)
{
	if (pCity == NULL)
	{
		return -1;
	}
	IDInfo& kDestination = pCity->getIDInfo();

	CvIdVector<CvAgreement>::const_iterator it;
	for (it = m_agreements.begin(); it != m_agreements.end(); ++it)
	{
		CvAgreement* pAgreement = it->second;
		if (pAgreement->getDestinationCity() == kDestination)
		{
			return pAgreement->getID();
		}
	}
	return -1;
}

CvAgreement* CvPlayer::firstAgreement(int *pIterIdx) const
{
	if (m_agreements.empty())
	{
		return NULL;
	}

	CvIdVector<CvAgreement>::const_iterator it = m_agreements.begin();
	CvAgreement* pAgreement = it->second;

	++it;
	if (it == m_agreements.end())
	{
		*pIterIdx = -1;
	}
	else
	{
		*pIterIdx = it->first;
	}

	return pAgreement;
}


CvAgreement* CvPlayer::nextAgreement(int *pIterIdx) const
{
	CvIdVector<CvAgreement>::const_iterator it = m_agreements.find(*pIterIdx);

	if (it == m_agreements.end())
	{
		return NULL;
	}

	CvAgreement* pAgreement = it->second;

	++it;
	if (it == m_agreements.end())
	{
		*pIterIdx = -1;
	}
	else
	{
		*pIterIdx = it->first;
	}

	return pAgreement;
}
void CvPlayer::negociateAgreement(CvUnit* pUnit, CvCity* pCity, int iChoice, int iStartIncrease)
{
	CvAgreement* pAgreement = getAgreement(getAgreementOfCity(pCity));
	CvPlayer& kOtherPlayer = GET_PLAYER(pCity->getOwner());
	bool bErase = true, bRefresh = true;
	int iIncrease = iStartIncrease;
	switch(iChoice)
	{
	case 0:
		{
			pCity->changeDissatisfiedCptTo(getID(), 1);
			int iValue = GC.getGameINLINE().getSorenRandNum(5, "iRand negociateAgreement")+1;
			if (iValue < pCity->getDissatisfiedCptTo(getID()))
			{
				pCity->setBanTurnTo(getID(), iValue);
				pCity->setDissatisfiedCptTo(getID(), 0);

				if (getID() == GC.getGameINLINE().getActivePlayer())
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_STOP_TRADE, pUnit->getID(), -1, -1);
					gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
				}
			}
		}
		break;
	case 1:
		{
			YieldTypes eWantedYield = pCity->getWantedYield();
			YieldTypes eSourceYield = pCity->getNativeYieldProduce();
			int iNumSourceYield = pAgreement->getOriginalAmount(eSourceYield);
			int iValue = kOtherPlayer.getPlayerValue(eSourceYield, iNumSourceYield);
			int iNumWantedYield = kOtherPlayer.getNumWantedValue(iValue, eWantedYield, iIncrease);
			pAgreement->addYieldTrade(eWantedYield, TRADE_EXPORT, iNumWantedYield);
			CvSelectionGroup* pGroup = pUnit->getGroup();
			if (pGroup->canAssignAgreement(pAgreement->getID()))
			{
				pUnit->finishMoves();
				pGroup->assignAgreement(pAgreement->getID(), true);
			}
			bErase = false;
		}
		break;
	case 2:
		{
			int iValue = GC.getGameINLINE().getSorenRandNum(100, "iRand negociateAgreement");
			iIncrease -= (15 + iValue%15);
			if (iValue >= 50 + getMercantileFactor() || iIncrease <= 0)
			{
				iIncrease = -iStartIncrease;//In This case we failed our negociation
			}
			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_NEW_AGREEMENT_PART2, pUnit->getID(), pAgreement->getID(), iIncrease);
				gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
			}
			bErase = false;
			bRefresh = false;
		}
		break;
	case 3:
		{
			int iIncrease = -iStartIncrease;
			iIncrease = std::min(iIncrease+iIncrease*7/100, 60);
			pCity->changeDissatisfiedCptTo(getID(), 1);
			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_NEW_AGREEMENT_PART2, pUnit->getID(), pAgreement->getID(), iIncrease, 0, true);
				gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
			}
			bErase = false;
			bRefresh = false;
		}
		break;
	}
	if (bErase)
	{
		removeAgreement(pAgreement->getID());
	}
	if (bRefresh)
	{
		gDLL->getInterfaceIFace()->setDirty(NativeYieldTradeResetTrade_DIRTY_BIT, true);
	}
}

int CvPlayer::getNumWantedValue(int askedValue, YieldTypes eWantedYield, int iIncreasePercent) const
{
	int result = askedValue*80/(100 + getPlayerValue(eWantedYield));

	return std::max(4, result + result * iIncreasePercent /100);
}

void CvPlayer::endOfAgreement(int iAgreementID, int iPercent, int iCase)
{
	CvAgreement* pAgreement = getAgreement(iAgreementID);
	CvPlayer& kOther = GET_PLAYER(pAgreement->getDestinationCity().eOwner);
	CvTeamAI& kTeam2 = GET_TEAM(kOther.getTeam());
	CvCity* pCity = kOther.getCity(pAgreement->getDestinationCity().iID);
	int iRelationTrade = 100 - (kOther.getRelationTradeTo(getID()) + 15)*100/30; // Entre -15 et 15
	int iRelationGlobal = (kTeam2.AI_getAttitudeVal(getTeam()) + 15)*100/30;
	iRelationGlobal = std::min(100, std::max(0, iRelationGlobal));//0 bad 100 good
	//Les cas où le contract ne s'est pas bien déroulé
	if (iPercent > 66) 
	{
		if (iRelationGlobal < 40)
		{
			kTeam2.declareWar(getTeam(), true, NO_WARPLAN);
		}

		int iMem = kOther.AI_getMemoryCount(getID(), MEMORY_AI_AGREEMENT);
		int iVal = iMem - 3 < -3 ? iMem + 3 : 3;
		kOther.AI_changeMemoryCount(getID(), MEMORY_AI_AGREEMENT, -iVal);

		int iBanTurn = 20 + 40*iRelationTrade/100;
		pCity->changeBanTurnTo(getID(), iBanTurn);
	}
	else if (iPercent > 33) 
	{
		if (iRelationGlobal < 20) 
		{
			kTeam2.declareWar(getTeam(), true, NO_WARPLAN);
		}

		int iMem = kOther.AI_getMemoryCount(getID(), MEMORY_AI_AGREEMENT);
		int iVal = iMem - 2 < -3 ? iMem + 3 : 2;
		kOther.AI_changeMemoryCount(getID(), MEMORY_AI_AGREEMENT, -iVal);

		int iBanTurn = 10 + 30*iRelationTrade/100;
		pCity->changeBanTurnTo(getID(), iBanTurn);
	} 
	else if (iPercent > 0) 
	{
		int iMem = kOther.AI_getMemoryCount(getID(), MEMORY_AI_AGREEMENT);
		int iVal = iMem - 1 < -3 ? iMem + 3 : 1;
		kOther.AI_changeMemoryCount(getID(), MEMORY_AI_AGREEMENT, -iVal);

		int iBanTurn = 0 + 20*iRelationTrade/100;
		pCity->changeBanTurnTo(getID(), iBanTurn);
	} 
	else if (iPercent == -1) 
	{
		int iMem = kOther.AI_getMemoryCount(getID(), MEMORY_AI_AGREEMENT);
		if (iMem + 1 <= 10) 
		{
			int iVal = -GC.getGameINLINE().getSorenRandNum(2, "AI_AGREEMENT val");
			iVal += pAgreement->getLevel();
			kOther.AI_changeMemoryCount(getID(), MEMORY_AI_AGREEMENT, iVal);			
		}

		changeNumAgreement(1);
		changeGold(pAgreement->getPrime());
	}

	removeAgreement(iAgreementID, iCase);
}

int CvPlayer::getNumAgreement() const
{
	return m_iNumAgreement;
}
void CvPlayer::changeNumAgreement(int iNewValue)
{
	m_iNumAgreement += iNewValue;
}

//End Agreement part

//Start temps units

CvTempUnit* CvPlayer::firstTempUnit(int *pIterIdx) const
{
	if (m_tempUnits.empty())
	{
		return NULL;
	}

	CvIdVector<CvTempUnit>::const_iterator it = m_tempUnits.begin();
	CvTempUnit* pTempUnit = it->second;

	++it;
	if (it == m_tempUnits.end())
	{
		*pIterIdx = -1;
	}
	else
	{
		*pIterIdx = it->first;
	}

	return pTempUnit;
}


CvTempUnit* CvPlayer::nextTempUnit(int *pIterIdx) const
{
	CvIdVector<CvTempUnit>::const_iterator it = m_tempUnits.find(*pIterIdx);

	if (it == m_tempUnits.end())
	{
		return NULL;
	}

	CvTempUnit* pTempUnit = it->second;

	++it;
	if (it == m_tempUnits.end())
	{
		*pIterIdx = -1;
	}
	else
	{
		*pIterIdx = it->first;
	}

	return pTempUnit;
}

CvTempUnit* CvPlayer::addTempUnit(UnitTypes eUnit)
{
	ProfessionTypes eProfession = (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession();
	CvTempUnit* pTempUnit = m_tempUnits.addNew();
	pTempUnit->init(pTempUnit->getID(), eUnit, getID(), eProfession);

	return pTempUnit;
}

bool CvPlayer::removeTempUnit(int iId)
{
	CvTempUnit* pTempUnit = getTempUnit(iId);
	FAssert(pTempUnit != NULL);
	if (pTempUnit == NULL)
	{
		return false;
	}

	if (!m_tempUnits.removeById(iId))
	{
		return false;
	}

	return true;
}


CvTempUnit* CvPlayer::getTempUnit(int iId) const
{
	CvTempUnit* pTempUnit = m_tempUnits.getById(iId);

	return pTempUnit;
}

int CvPlayer::getNumTempUnits() const
{
	return m_tempUnits.size();
}

void CvPlayer::getTempUnits(std::vector<CvTempUnit*>& aTempUnits) const
{
	CvIdVector<CvTempUnit>::const_iterator it;
	for (it = m_tempUnits.begin(); it != m_tempUnits.end(); ++it)
	{
		aTempUnits.push_back(it->second);
	}
}
//End Temp Unit

//Start Hospitals
void CvPlayer::initHospitals()
{
	if (!isEuropean())
	{
		return;
	}

	CvHospitals* pHospitals = m_hospitals.addNew();
	pHospitals->init(getID());
}
CvHospitals* CvPlayer::getHospitals() const
{
	CvHospitals* pHospitals = NULL;
	CvIdVector<CvHospitals>::const_iterator it = m_hospitals.begin();
	if (it != m_hospitals.end())
	{
		pHospitals = it->second;
	}
	return pHospitals;
}
void CvPlayer::doHospitals()
{
	CvHospitals* pHospitals = getHospitals();
	if (pHospitals != NULL)
	{
		pHospitals->doTurn();
	}
}
//End Hospitals

//----------------------
//	START Trade Routes
//----------------------
CvTradeRoute* CvPlayer::addTradeRoute(const IDInfo& kSource, const IDInfo& kDestination)
{
	CvCity* pSourceCity = ::getCity(kSource);
	FAssert(pSourceCity != NULL || (kSource.eOwner == getID() && kSource.iID == CvTradeRoute::EUROPE_CITY_ID));

	CvCity* pDestinationCity = ::getCity(kDestination);
	FAssert(pDestinationCity != NULL || (kDestination.eOwner == getID() && kDestination.iID == CvTradeRoute::EUROPE_CITY_ID));

	if (kSource == kDestination)
	{
		return NULL;
	}

	CvTradeRoute* pTradeRoute = m_tradeRoutes.addNew();
	pTradeRoute->init(pTradeRoute->getID(), kSource, kDestination);

	setCurrentTradeRouteId(pTradeRoute->getID(), true);

	return pTradeRoute;
}

bool CvPlayer::removeTradeRoute(int iId, bool bUdateTradeRoutesScreen)
{
	/*CvTradeRoute* pTradeRoute = getTradeRoute(iId);
	FAssert(pTradeRoute != NULL);
	if (pTradeRoute == NULL)
	{
		return false;
	}
	const IDInfo kEurope(getID(), CvTradeRoute::EUROPE_CITY_ID);
	bool isEuropeTradeRoute = pTradeRoute->getSourceCity() == kEurope || pTradeRoute->getDestinationCity() == kEurope;

	CLLNode<int>* pSelectionGroupNode;
	CvSelectionGroup* pLoopSelectionGroup;

	pSelectionGroupNode = headGroupCycleNode();

	while (pSelectionGroupNode != NULL)
	{
		pLoopSelectionGroup = getSelectionGroup(pSelectionGroupNode->m_data);
		if (pLoopSelectionGroup->isAssignedTradeRoute(iId))
		{
			pLoopSelectionGroup->assignTradeRoute(iId, false);
			if (isEuropeTradeRoute)
			{
				pLoopSelectionGroup->setAutomateType(AUTOMATE_SAIL);
			}
		}
		pSelectionGroupNode = nextGroupCycleNode(pSelectionGroupNode);
	}

	bool bResult = m_tradeRoutes.removeById(iId);

	setCurrentTradeRouteId(-1, bUdateTradeRoutesScreen);
	if (bUdateTradeRoutesScreen && bResult && getID() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(TradeRoutesResetSelection_DIRTY_BIT, true);
	}
	return bResult;*/
	return false;
}

CvTradeRoute* CvPlayer::getTradeRoute(int iId) const
{
	if (iId == -1)
	{
		return NULL;
	}

	CvTradeRoute* pTradeRoute = m_tradeRoutes.getById(iId);
	FAssert(pTradeRoute != NULL && pTradeRoute->getSourceCity().eOwner == getID());
	return pTradeRoute;
}

int CvPlayer::getNumTradeRoutes() const
{
	return m_tradeRoutes.size();
}

void CvPlayer::getTradeRoutes(std::vector<CvTradeRoute*>& aTradeRoutes) const
{
	CvIdVector<CvTradeRoute>::const_iterator it;
	for (it = m_tradeRoutes.begin(); it != m_tradeRoutes.end(); ++it)
	{
		aTradeRoutes.push_back(it->second);
	}
}

CvTradeRoute* CvPlayer::firstTradeRoute(int *pIterIdx) const
{
	if (m_tradeRoutes.empty())
	{
		return NULL;
	}

	CvIdVector<CvTradeRoute>::const_iterator it = m_tradeRoutes.begin();
	CvTradeRoute* pTradeRoute = it->second;

	++it;
	if (it == m_tradeRoutes.end())
	{
		*pIterIdx = -1;
	}
	else
	{
		*pIterIdx = it->first;
	}

	return pTradeRoute;
}


CvTradeRoute* CvPlayer::nextTradeRoute(int *pIterIdx) const
{
	CvIdVector<CvTradeRoute>::const_iterator it = m_tradeRoutes.find(*pIterIdx);

	if (it == m_tradeRoutes.end())
	{
		return NULL;
	}

	CvTradeRoute* pTradeRoute = it->second;

	++it;
	if (it == m_tradeRoutes.end())
	{
		*pIterIdx = -1;
	}
	else
	{
		*pIterIdx = it->first;
	}

	return pTradeRoute;
}
//-------------------
//	End trade routes
//-------------------

bool CvPlayer::canLoadYield(PlayerTypes eCityPlayer) const
{
	if (eCityPlayer != getID())
	{
		return false;
	}

	return true;
}

bool CvPlayer::canUnloadYield(PlayerTypes eCityPlayer) const
{
	if (GET_PLAYER(eCityPlayer).getTeam() != getTeam())
	{
		return false;
	}

	return true;
}

std::string CvPlayer::getScriptData() const
{
	return m_szScriptData;
}

void CvPlayer::setScriptData(std::string szNewValue)
{
	m_szScriptData = szNewValue;
}

const CvString& CvPlayer::getPbemEmailAddress() const
{
	return GC.getInitCore().getEmail(getID());
}

void CvPlayer::setPbemEmailAddress(const char* szAddress)
{
	GC.getInitCore().setEmail(getID(), szAddress);
}

const CvString& CvPlayer::getSmtpHost() const
{
	return GC.getInitCore().getSmtpHost(getID());
}

void CvPlayer::setSmtpHost(const char* szHost)
{
	GC.getInitCore().setSmtpHost(getID(), szHost);
}

// Protected Functions...

void CvPlayer::doTaxes()
{
	//We don't proceed the increasing of taxes
	if (getParent() == NO_PLAYER || isInRevolution())
	{
		return;
	}
	NewEraTypes eNewEra = (NewEraTypes)getNewEra();
	if (eNewEra == NO_NEW_ERA)
	{
		return;
	}
	CvNewEraInfo& kNewEra = GC.getNewEraInfo(eNewEra);
	int iOldTaxRate = getTaxRate();

	CvHandicapInfo& kHandicapInfo = GC.getHandicapInfo(getHandicapType());
	int iTurn = GC.getGameINLINE().getGameTurn();
	int iLastTurnOfTaxesIncrease = getLastTurnOfTaxesIncrease();
	if (kHandicapInfo.getMinTurnsBeforeTaxesIncrease() >= iTurn - iLastTurnOfTaxesIncrease)
	{
		return;//Player already had taxes increase recently
	}

	int iAverageTurnOfTaxesIncrease = kHandicapInfo.getAverageTurnsForTaxesIncrease();
	int iIndex = (iAverageTurnOfTaxesIncrease + iLastTurnOfTaxesIncrease) % NUM_RAND_VALUES;
	int iRandomVar = getRandomValues(iIndex) - 50; // [-50..50]
	int iNextTaxesIncreaseInTurn = iAverageTurnOfTaxesIncrease + iAverageTurnOfTaxesIncrease*iRandomVar/100;

	if (iTurn == iNextTaxesIncreaseInTurn + iLastTurnOfTaxesIncrease)
	{
		//We will increase taxes of the player
		setLastTurnOfTaxesIncrease(iTurn);
		int iMaxTaxRate = kNewEra.getTaxRateCeiling() + kHandicapInfo.getTaxRateCeilingIncrease();
		if (iMaxTaxRate == iOldTaxRate)
		{
			return;
		}
		int iChangeTaxeRate = std::min(iMaxTaxRate-iOldTaxRate, (int)kHandicapInfo.getMaxTaxRateIncrease());
		int iKingMinTaxesPercent = kHandicapInfo.getKingMinTaxesPercent();
		iChangeTaxeRate = iChangeTaxeRate * (iKingMinTaxesPercent + GC.getGameINLINE().getSorenRandNum(100-iKingMinTaxesPercent, "Increase tax rate")) / 100;
		if (iChangeTaxeRate > 0)
		{
			changeTaxRate(iChangeTaxeRate);
			if (isHuman())
			{
				CvDiploParameters* pDiplo = new CvDiploParameters(getParent());
				pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_KISS_PINKY"));
				pDiplo->addDiploCommentVariable(iOldTaxRate);
				pDiplo->addDiploCommentVariable(getTaxRate());
				pDiplo->setAIContact(true);
				gDLL->beginDiplomacy(pDiplo, getID());
			}
		}
	}
}

void CvPlayer::doGold()
{
	//DOANE Python Callbacks | Speed Improvement
	if (GC.getUSE_ON_DO_GOLD_CALLBACK())
	{
		CyArgsList argsList;
		argsList.add(getID());
		long lResult=0;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "doGold", argsList.makeFunctionArgs(), &lResult); //Returns false as default
		if (lResult == 1) // Python override
		{
			return;
		}
	}
	//END DOANE

	int iGoldChange = 0;

	if (!isHuman() && getGold() < GC.getHandicapInfo(getHandicapType()).getAIMoneyTree())
	{
		iGoldChange = 500 + GC.getGameINLINE().getSorenRandNum(50, "iGoldChange") * 10;

	}

	changeGold(iGoldChange);
}

void CvPlayer::doBells()
{
	if (getParent() == NO_PLAYER)
	{
		return;
	}

	int iBellsRate = getYieldRate(YIELD_BELLS);
	if (iBellsRate == 0)
	{
		return;
	}

	//update revolution unit bells
	if (!isInRevolution())
	{
		changeBellsStored(iBellsRate);
		if (getBellsStored() >= revolutionEuropeUnitThreshold() && iBellsRate > GC.getCivilizationInfo(getCivilizationType()).getFreeYields(YIELD_BELLS))
		{
			changeBellsStored(-revolutionEuropeUnitThreshold());
			setRevolutionEuropeUnitThresholdMultiplier((getRevolutionEuropeUnitThresholdMultiplier() * (100 + GC.getDefineINT("REVOLUTION_EUROPE_UNIT_THRESHOLD_INCREASE"))) / 100);
			setKingNumUnitMultiplier(getKingNumUnitMultiplier() * GC.getHandicapInfo(getHandicapType()).getKingNumUnitMultiplier() / 100);

			if (NO_PLAYER != getParent())
			{
				CvPlayer& kParent = GET_PLAYER(getParent());
				FAssert(kParent.isEurope());

				CvCivilizationInfo& kCivilizationInfo = GC.getCivilizationInfo(kParent.getCivilizationType());
				int iNumFreeUnits = kCivilizationInfo.getNumCivilizationFreeUnits();
				std::vector<int> aiUnitWeights(iNumFreeUnits, 100);
				for (int i = 0; i < iNumFreeUnits; ++i)
				{
					int iUnitClass = kCivilizationInfo.getCivilizationFreeUnitsClass(i);
					UnitTypes eUnit = (UnitTypes) kCivilizationInfo.getCivilizationUnits(iUnitClass);
					if (eUnit == NO_UNIT)
					{
						aiUnitWeights[i] = 0;
					}
					else
					{
						if (GC.getUnitInfo(eUnit).getDomainType() == DOMAIN_SEA)
						{
							aiUnitWeights[i] += std::max(-100, GC.getDefineINT("REVOLUTION_EUROPE_UNIT_SHIP_MODIFIER"));
						}
					}
				}

				if (iNumFreeUnits > 0)
				{
					int iNumUnits = std::max(1, GC.getDefineINT("KING_INITIAL_UNIT_INCREASE") * getKingNumUnitMultiplier() / 100);

					if (iNumUnits > GC.getHandicapInfo(getHandicapType()).getKingUnitIncreaseCap())
					{
						if (isHuman())
						{
							iNumUnits = GC.getHandicapInfo(getHandicapType()).getKingUnitIncreaseCap();
						}
						else
						{
							iNumUnits = GC.getHandicapInfo(getHandicapType()).getKingUnitIncreaseCap() / 4;
						}
					}

					for (int i = 0; i < iNumUnits; ++i)
					{
						int iIndex = GC.getGameINLINE().getSorenRand().pickValue(aiUnitWeights, "Pick Expeditionary force unit");
						int iUnitClass = kCivilizationInfo.getCivilizationFreeUnitsClass(iIndex);
						ProfessionTypes eUnitProfession = (ProfessionTypes) kCivilizationInfo.getCivilizationFreeUnitsProfession(iIndex);
						UnitTypes eUnit = (UnitTypes)kCivilizationInfo.getCivilizationUnits(iUnitClass);
						FAssert(eUnit != NO_UNIT);
						addRevolutionEuropeUnit(eUnit, eUnitProfession);
					}
					
					VictoryTypes eVictory = GC.getGameINLINE().getVictoryTarget();
					if (eVictory != NO_VICTORY)
					{	
						CvVictoryInfo& kVictoryInfo = GC.getVictoryInfo(eVictory);
						if (kVictoryInfo.isRevolution()) 
						{
							CvWString szBuffer = gDLL->getText("TXT_KEY_NEW_EUROPE_ARMY_NEW", kParent.getCivilizationShortDescriptionKey(), getCivilizationShortDescriptionKey(), iNumUnits, kParent.getCivilizationAdjectiveKey());
							gDLL->getInterfaceIFace()->addMessage(getID(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_UNIT_GREATPEOPLE", GC.getGameINLINE().isDebugMode() ? MESSAGE_TYPE_MAJOR_EVENT : MESSAGE_TYPE_INFO, GC.getLeaderHeadInfo(kParent.getLeaderType()).getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_UNIT_TEXT"));
						}
					}
				}
			}
		}
	}
}

void CvPlayer::doCrosses()
{
	if (getParent() == NO_PLAYER)
	{
		return;
	}

	int iCrossRate = getYieldRate(YIELD_CROSSES);

	if (getImmigrationConversion() == YIELD_CROSSES)
	{
		changeCrossesStored(iCrossRate);
		changeRecruitPriceDiscount(iCrossRate);
	}
}

void CvPlayer::doAdvancedStartAction(AdvancedStartActionTypes eAction, int iX, int iY, int iData, bool bAdd)
{
	if (getAdvancedStartPoints() < 0)
	{
		return;
	}

	CvPlot* pPlot = GC.getMap().plot(iX, iY);

	if (0 == getNumCities())
	{
		switch (eAction)
		{
		case ADVANCEDSTARTACTION_EXIT:
			//Try to build this player's empire
			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->setBusy(true);
			}
			AI_doAdvancedStart(true);
			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->setBusy(false);
			}
			break;
		case ADVANCEDSTARTACTION_AUTOMATE:
		case ADVANCEDSTARTACTION_CITY:
			break;
		default:
			// The first action must be to place a city
			// so players can lose by spending everything
			return;
		}
	}

	switch (eAction)
	{
	case ADVANCEDSTARTACTION_EXIT:
		changeGold(getAdvancedStartPoints());
		setAdvancedStartPoints(-1);
		if (GC.getGameINLINE().getActivePlayer() == getID())
		{
			gDLL->getInterfaceIFace()->setInAdvancedStart(false);
		}

		if (isHuman())
		{
			int iLoop;
			for (CvCity* pCity = firstCity(&iLoop); NULL != pCity; pCity = nextCity(&iLoop))
			{
				pCity->chooseProduction();
			}
		}
		break;
	case ADVANCEDSTARTACTION_AUTOMATE:
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setBusy(true);
		}
		AI_doAdvancedStart(true);
		if (getID() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setBusy(false);
		}
		break;
	case ADVANCEDSTARTACTION_UNIT:
		{
			if (pPlot == NULL)
				return;

			UnitTypes eUnit = (UnitTypes) iData;
			int iCost = getAdvancedStartUnitCost(eUnit, bAdd, pPlot);

			if (bAdd && iCost < 0)
			{
				return;
			}

			// Add unit to the map
			if (bAdd)
			{
				if (getAdvancedStartPoints() >= iCost)
				{
					CvUnit* pUnit = initUnit(eUnit, (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession(), iX, iY);
					if (NULL != pUnit)
					{
						pUnit->finishMoves();
						changeAdvancedStartPoints(-iCost);
					}
				}
			}

			// Remove unit from the map
			else
			{
				// If cost is -1 we already know this unit isn't present
				if (iCost != -1)
				{
					CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
					while (pUnitNode != NULL)
					{
						CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
						pUnitNode = pPlot->nextUnitNode(pUnitNode);

						if (pLoopUnit->getUnitType() == eUnit)
						{
							pLoopUnit->kill(false);
							changeAdvancedStartPoints(iCost);
							return;
						}
					}
				}

				// Proper unit not found above, delete first found
				CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
				if (pUnitNode != NULL)
				{
					CvUnit* pUnit = ::getUnit(pUnitNode->m_data);

					iCost = getAdvancedStartUnitCost(pUnit->getUnitType(), false);
					FAssertMsg(iCost != -1, "If this is -1 then that means it's going to try to delete a unit which shouldn't exist");
					pUnit->kill(false);
					changeAdvancedStartPoints(iCost);
				}
			}

			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->setDirty(Advanced_Start_DIRTY_BIT, true);
			}
		}
		break;
	case ADVANCEDSTARTACTION_CITY:
		{
			if (pPlot == NULL)
				return;

			int iCost = getAdvancedStartCityCost(bAdd, pPlot);

			if (iCost < 0)
			{
				return;
			}

			// Add City to the map
			if (bAdd)
			{
				if (0 == getNumCities())
				{
					PlayerTypes eClosestPlayer = NO_PLAYER;
					int iMinDistance = MAX_INT;
					for (int iPlayer = 0; iPlayer < MAX_PLAYERS; iPlayer++)
					{
						CvPlayer& kPlayer = GET_PLAYER((PlayerTypes)iPlayer);
						if (kPlayer.isAlive())
						{
							if (kPlayer.getTeam() == getTeam())
							{
								if (0 == kPlayer.getNumCities())
								{
									FAssert(kPlayer.getStartingPlot() != NULL);
									int iDistance = plotDistance(iX, iY, kPlayer.getStartingPlot()->getX_INLINE(), kPlayer.getStartingPlot()->getY_INLINE());
									if (iDistance < iMinDistance)
									{
										eClosestPlayer = kPlayer.getID();
										iMinDistance = iDistance;
									}
								}
							}
						}
					}
					FAssertMsg(eClosestPlayer != NO_PLAYER, "Self at a minimum should always be valid");
					if (eClosestPlayer != getID())
					{
						CvPlot* pTempPlot = GET_PLAYER(eClosestPlayer).getStartingPlot();
						GET_PLAYER(eClosestPlayer).setStartingPlot(getStartingPlot(), false);
						setStartingPlot(pTempPlot, false);
					}
				}
				if (getAdvancedStartPoints() >= iCost || 0 == getNumCities())
				{
					found(iX, iY);
					changeAdvancedStartPoints(-iCost);
					GC.getGameINLINE().updateColoredPlots();
					CvCity* pCity = pPlot->getPlotCity();
					if (pCity != NULL)
					{
						if (pCity->getPopulation() == 0)
						{
							pCity->setPopulation(1);
						}

						if (pCity->getPopulation() > 1)
						{
							pCity->setFood(pCity->growthThreshold() / 2);
						}
					}
				}
			}

			// Remove City from the map
			else
			{
				pPlot->setRouteType(NO_ROUTE);
				pPlot->getPlotCity()->kill();
				pPlot->setImprovementType(NO_IMPROVEMENT);
				changeAdvancedStartPoints(iCost);
			}

			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->setDirty(Advanced_Start_DIRTY_BIT, true);
			}
		}
		break;
	case ADVANCEDSTARTACTION_POP:
		{
			if (pPlot == NULL)
				return;

			CvCity* pCity = pPlot->getPlotCity();

			if (pCity != NULL)
			{
				int iCost = getAdvancedStartPopCost(bAdd, pCity);

				if (iCost < 0)
				{
					return;
				}

				// Add Pop to the City
				if (bAdd)
				{
					if (getAdvancedStartPoints() >= iCost)
					{
						pCity->changePopulation(1);
						changeAdvancedStartPoints(-iCost);
						if (pCity->getPopulation() > 1)
						{
							pCity->setFood(pCity->growthThreshold() / 2);
							//pCity->setFoodKept((pCity->getFood() * (pCity->getMaxFoodKeptPercent())) / 100);
						}
					}
				}

				// Remove Pop from the city
				else
				{
					pCity->changePopulation(-1);
					changeAdvancedStartPoints(iCost);
					if (pCity->getPopulation() == 1)
					{
						pCity->setFood(0);
						pCity->setFoodKept(0);
					}
				}
			}
		}
		break;
	case ADVANCEDSTARTACTION_CULTURE:
		{
			if (pPlot == NULL)
				return;

			CvCity* pCity = pPlot->getPlotCity();

			if (pCity != NULL)
			{
				int iCost = getAdvancedStartCultureCost(bAdd, pCity);

				if (iCost < 0)
				{
					return;
				}

				// Add Culture to the City
				if (bAdd)
				{
					if (getAdvancedStartPoints() >= iCost)
					{
						pCity->setCulture(getID(), pCity->getCultureThreshold(), true);
						changeAdvancedStartPoints(-iCost);
					}
				}

				// Remove Culture from the city
				else
				{
					CultureLevelTypes eLevel = (CultureLevelTypes)std::max(0, pCity->getCultureLevel() - 1);
					pCity->setCulture(getID(), CvCity::getCultureThreshold(eLevel), true);
					changeAdvancedStartPoints(iCost);
				}
			}
		}
		break;
	case ADVANCEDSTARTACTION_BUILDING:
		{
			if (pPlot == NULL)
				return;

			CvCity* pCity = pPlot->getPlotCity();

			if (pCity != NULL)
			{
				BuildingTypes eBuilding = (BuildingTypes) iData;
				int iCost = getAdvancedStartBuildingCost(eBuilding, bAdd, pCity);

				if (iCost < 0)
				{
					return;
				}

				// Add Building to the City
				if (bAdd)
				{
					if (getAdvancedStartPoints() >= iCost)
					{
						pCity->setHasRealBuilding(eBuilding, true);
						changeAdvancedStartPoints(-iCost);
						/*if (GC.getBuildingInfo(eBuilding).getFoodKept() != 0)
						{
						pCity->setFoodKept((pCity->getFood() * pCity->getMaxFoodKeptPercent()) / 100);
						}*/
					}
				}

				// Remove Building from the map
				else
				{
					pCity->setHasRealBuilding(eBuilding, false);
					changeAdvancedStartPoints(iCost);
					/*if (GC.getBuildingInfo(eBuilding).getFoodKept() != 0)
					{
					pCity->setFoodKept((pCity->getFood() * pCity->getMaxFoodKeptPercent()) / 100);
					}*/
				}
			}

			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->setDirty(Advanced_Start_DIRTY_BIT, true);
			}
		}
		break;
	case ADVANCEDSTARTACTION_ROUTE:
		{
			if (pPlot == NULL)
				return;

			RouteTypes eRoute = (RouteTypes) iData;
			int iCost = getAdvancedStartRouteCost(eRoute, bAdd, pPlot);

			if (bAdd && iCost < 0)
			{
				return;
			}

			// Add Route to the plot
			if (bAdd)
			{
				if (getAdvancedStartPoints() >= iCost)
				{
					pPlot->setRouteType(eRoute);
					changeAdvancedStartPoints(-iCost);
				}
			}

			// Remove Route from the Plot
			else
			{
				if (pPlot->getRouteType() != eRoute)
				{
					eRoute = pPlot->getRouteType();
					iCost = getAdvancedStartRouteCost(eRoute, bAdd);
				}

				if (iCost < 0)
				{
					return;
				}

				pPlot->setRouteType(NO_ROUTE);
				changeAdvancedStartPoints(iCost);
			}

			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->setDirty(Advanced_Start_DIRTY_BIT, true);
			}
		}
		break;
	case ADVANCEDSTARTACTION_IMPROVEMENT:
		{
			if (pPlot == NULL)
				return;

			ImprovementTypes eImprovement = (ImprovementTypes) iData;
			int iCost = getAdvancedStartImprovementCost(eImprovement, bAdd, pPlot);

			if (bAdd && iCost < 0)
			{
				return;
			}

			// Add Improvement to the plot
			if (bAdd)
			{
				if (getAdvancedStartPoints() >= iCost)
				{
					if (pPlot->getFeatureType() != NO_FEATURE)
					{
						for (int iI = 0; iI < GC.getNumBuildInfos(); ++iI)
						{
							ImprovementTypes eLoopImprovement = ((ImprovementTypes)(GC.getBuildInfo((BuildTypes)iI).getImprovement()));

							if (eImprovement == eLoopImprovement)
							{
								if (GC.getBuildInfo((BuildTypes)iI).isFeatureRemove(pPlot->getFeatureType()) && canBuild(pPlot, (BuildTypes)iI))
								{
									pPlot->setFeatureType(NO_FEATURE);
									break;
								}
							}
						}
					}
					pPlot->setImprovementType(eImprovement);
					changeAdvancedStartPoints(-iCost);
				}
			}
			// Remove Improvement from the Plot
			else
			{
				if (pPlot->getImprovementType() != eImprovement)
				{
					eImprovement = pPlot->getImprovementType();
					iCost = getAdvancedStartImprovementCost(eImprovement, bAdd, pPlot);
				}
				if (iCost < 0)
				{
					return;
				}
				pPlot->setImprovementType(NO_IMPROVEMENT);
				changeAdvancedStartPoints(iCost);
			}

			if (getID() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->setDirty(Advanced_Start_DIRTY_BIT, true);
			}
		}
		break;
	case ADVANCEDSTARTACTION_VISIBILITY:
		{
			if (pPlot == NULL)
				return;

			int iCost = getAdvancedStartVisibilityCost(bAdd, pPlot);

			if (iCost < 0)
			{
				return;
			}

			// Add Visibility to the plot
			if (bAdd)
			{
				if (getAdvancedStartPoints() >= iCost)
				{
					pPlot->setRevealed(getTeam(), true, true, NO_TEAM);
					changeAdvancedStartPoints(-iCost);
				}
			}

			// Remove Visibility from the Plot
			else
			{
				pPlot->setRevealed(getTeam(), false, true, NO_TEAM);
				changeAdvancedStartPoints(iCost);
			}
		}
		break;
	default:
		FAssert(false);
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Adding or removing a unit
/////////////////////////////////////////////////////////////////////////////////////////////

int CvPlayer::getAdvancedStartUnitCost(UnitTypes eUnit, bool bAdd, CvPlot* pPlot)
{
	int iLoop;
	int iNumUnitType = 0;

	if (0 == getNumCities())
	{
		return -1;
	}

	int iCost = 0;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		iCost += getYieldProductionNeeded(eUnit, (YieldTypes) iYield);
	}

	iCost *= GC.getUnitInfo(eUnit).getAdvancedStartCost();
	if (iCost < 0)
	{
		return -1;
	}
	iCost /= 100;

	if (NULL == pPlot)
	{
		if (bAdd)
		{
			bool bValid = false;
			int iLoop;
			for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop))
			{
				if (pLoopCity->canTrain(eUnit))
				{
					bValid = true;
					break;
				}
			}

			if (!bValid)
			{
				return -1;
			}
		}
	}
	else
	{
		CvCity* pCity = NULL;

		if (0 == GC.getDefineINT("ADVANCED_START_ALLOW_UNITS_OUTSIDE_CITIES"))
		{
			pCity = pPlot->getPlotCity();

			if (NULL == pCity || pCity->getOwnerINLINE() != getID())
			{
				return -1;
			}

			iCost *= 100;
			iCost /= std::max(1, 100 + pCity->getProductionModifier(eUnit));
		}
		else
		{
			if (pPlot->getOwnerINLINE() != getID())
			{
				return -1;
			}

			iCost *= 100;
			iCost /= std::max(1, 100 + getProductionModifier(eUnit));
		}


		if (bAdd)
		{
			int iMaxUnitsPerCity = GC.getDefineINT("ADVANCED_START_MAX_UNITS_PER_CITY");
			if (iMaxUnitsPerCity >= 0)
			{
				if (getNumUnits() >= iMaxUnitsPerCity * getNumCities())
				{
					return -1;
				}
			}

			if (NULL != pCity)
			{
				if (!pCity->canTrain(eUnit))
				{
					return -1;
				}
			}
			else
			{
				if (!pPlot->canTrain(eUnit, false, false))
				{
					return -1;
				}

				if (pPlot->isImpassable() && !GC.getUnitInfo(eUnit).isCanMoveImpassable())
				{
					return -1;
				}

				if (pPlot->getFeatureType() != NO_FEATURE)
				{
					if (GC.getUnitInfo(eUnit).getFeatureImpassable(pPlot->getFeatureType()))
					{
						return -1;
					}
				}
				else
				{
					if (GC.getUnitInfo(eUnit).getTerrainImpassable(pPlot->getTerrainType()))
					{
						return -1;
					}
				}
			}
		}
		// Must be this unit at plot in order to remove
		else
		{
			bool bUnitFound = false;

			CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
			while (pUnitNode != NULL)
			{
				CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pPlot->nextUnitNode(pUnitNode);

				if (pLoopUnit->getUnitType() == eUnit)
				{
					bUnitFound = true;
				}
			}

			if (!bUnitFound)
			{
				return -1;
			}
		}
	}

	// Increase cost if the XML defines that additional units will cost more
	if (0 != GC.getUnitInfo(eUnit).getAdvancedStartCostIncrease())
	{
		for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
		{
			if (pLoopUnit->getUnitType() == eUnit)
			{
				++iNumUnitType;
			}
		}

		if (!bAdd)
		{
			--iNumUnitType;
		}

		if (iNumUnitType > 0)
		{
			iCost *= 100 + GC.getUnitInfo(eUnit).getAdvancedStartCostIncrease() * iNumUnitType;
			iCost /= 100;
		}
	}

	return iCost;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Adding or removing a City
/////////////////////////////////////////////////////////////////////////////////////////////

int CvPlayer::getAdvancedStartCityCost(bool bAdd, CvPlot* pPlot)
{
	int iNumCities = getNumCities();

	int iCost = getNewCityProductionValue();

	if (iCost < 0)
	{
		return -1;
	}

	// Valid plot?
	if (pPlot != NULL)
	{
		// Need valid plot to found on if adding
		if (bAdd)
		{
			if (!canFound(pPlot->getX(), pPlot->getY(), false))
			{
				return -1;
			}
		}
		// Need your own city present to remove
		else
		{
			if (pPlot->isCity())
			{
				if (pPlot->getPlotCity()->getOwnerINLINE() != getID())
				{
					return -1;
				}
			}
			else
			{
				return -1;
			}
		}

		// Is there a distance limit on how far a city can be placed from a player's start/another city?
		if (GC.getDefineINT("ADVANCED_START_CITY_PLACEMENT_MAX_RANGE") > 0)
		{
			PlayerTypes eClosestPlayer = NO_PLAYER;
			int iClosestDistance = MAX_INT;

			for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
			{
				CvPlayer& kPlayer = GET_PLAYER((PlayerTypes)iPlayer);

				if (kPlayer.isAlive())
				{
					CvPlot* pStartingPlot = kPlayer.getStartingPlot();

					if (NULL != pStartingPlot)
					{
						int iDistance = ::plotDistance(pPlot->getX_INLINE(), pPlot->getY_INLINE(), pStartingPlot->getX_INLINE(), pStartingPlot->getY_INLINE());
						if (iDistance <= GC.getDefineINT("ADVANCED_START_CITY_PLACEMENT_MAX_RANGE"))
						{
							if (iDistance < iClosestDistance || (iDistance == iClosestDistance && getTeam() != kPlayer.getTeam()))
							{
								iClosestDistance = iDistance;
								eClosestPlayer = kPlayer.getID();
							}
						}
					}
				}
			}

			if (NO_PLAYER == eClosestPlayer || GET_PLAYER(eClosestPlayer).getTeam() != getTeam())
			{
				return -1;
			}
			//Only allow founding a city at someone elses start point if
			//We have no cities and they have no cities.
			if ((getID() != eClosestPlayer) && ((getNumCities() > 0) || (GET_PLAYER(eClosestPlayer).getNumCities() > 0)))
			{
				return -1;
			}
		}
	}

	// Increase cost if the XML defines that additional units will cost more
	if (0 != GC.getDefineINT("ADVANCED_START_CITY_COST_INCREASE"))
	{
		if (!bAdd)
		{
			--iNumCities;
		}

		if (iNumCities > 0)
		{
			iCost *= 100 + GC.getDefineINT("ADVANCED_START_CITY_COST_INCREASE") * iNumCities;
			iCost /= 100;
		}
	}

	return iCost;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Adding or removing Population
/////////////////////////////////////////////////////////////////////////////////////////////

int CvPlayer::getAdvancedStartPopCost(bool bAdd, CvCity* pCity)
{
	if (0 == getNumCities())
	{
		return -1;
	}

	int iCost = (getGrowthThreshold(1) * GC.getDefineINT("ADVANCED_START_POPULATION_COST")) / 100;

	if (NULL != pCity)
	{
		if (pCity->getOwnerINLINE() != getID())
		{
			return -1;
		}

		int iPopulation = pCity->getPopulation();

		// Need to have Population to remove it
		if (!bAdd)
		{
			--iPopulation;

			if (iPopulation < GC.getDefineINT("INITIAL_CITY_POPULATION") + GC.getEraInfo(GC.getGameINLINE().getStartEra()).getFreePopulation())
			{
				return -1;
			}
		}

		iCost = (getGrowthThreshold(iPopulation) * GC.getDefineINT("ADVANCED_START_POPULATION_COST")) / 100;

		// Increase cost if the XML defines that additional Pop will cost more
		if (0 != GC.getDefineINT("ADVANCED_START_POPULATION_COST_INCREASE"))
		{
			--iPopulation;

			if (iPopulation > 0)
			{
				iCost *= 100 + GC.getDefineINT("ADVANCED_START_POPULATION_COST_INCREASE") * iPopulation;
				iCost /= 100;
			}
		}
	}

	return iCost;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Adding or removing Culture
/////////////////////////////////////////////////////////////////////////////////////////////

int CvPlayer::getAdvancedStartCultureCost(bool bAdd, CvCity* pCity)
{
	if (0 == getNumCities())
	{
		return -1;
	}

	int iCost = GC.getDefineINT("ADVANCED_START_CULTURE_COST");
	if (iCost < 0)
	{
		return -1;
	}

	if (NULL != pCity)
	{
		if (pCity->getOwnerINLINE() != getID())
		{
			return -1;
		}

		// Need to have enough culture to remove it
		if (!bAdd)
		{
			if (pCity->getCultureLevel() <= 0)
			{
				return -1;
			}
		}

		int iCulture;
		if (bAdd)
		{
			iCulture = CvCity::getCultureThreshold((CultureLevelTypes)(pCity->getCultureLevel() + 1)) - pCity->getCulture(getID());
		}
		else
		{
			iCulture = pCity->getCulture(getID()) - CvCity::getCultureThreshold((CultureLevelTypes)(pCity->getCultureLevel() - 1));
		}

		iCost *= iCulture;
		iCost *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent();
		iCost /= 10000;
	}

	return iCost;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Adding or removing a Building from a city
/////////////////////////////////////////////////////////////////////////////////////////////

int CvPlayer::getAdvancedStartBuildingCost(BuildingTypes eBuilding, bool bAdd, CvCity* pCity)
{
	CvBuildingInfo& kBuilding = GC.getBuildingInfo(eBuilding);
	if (0 == getNumCities())
	{
		return -1;
	}

	int iNumBuildingType = 0;

	int iCost = 0;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		iCost += getYieldProductionNeeded(eBuilding, (YieldTypes) iYield);
	}

	iCost *= kBuilding.getAdvancedStartCost();
	if (iCost < 0)
	{
		return -1;
	}
	iCost /= 100;

	if (kBuilding.getFreeStartEra() != NO_ERA && GC.getGameINLINE().getStartEra() >=  kBuilding.getFreeStartEra())
	{
		// you get this building for free
		return -1;
	}

	if (NULL == pCity)
	{
		if (bAdd)
		{
			bool bValid = false;
			int iLoop;
			for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop))
			{
				if (pLoopCity->canConstruct(eBuilding))
				{
					bValid = true;
					break;
				}
			}

			if (!bValid)
			{
				return -1;
			}
		}
	}
	if (NULL != pCity)
	{
		if (pCity->getOwnerINLINE() != getID())
		{
			return -1;
		}

		iCost *= 100;
		iCost /= std::max(1, 100 + pCity->getProductionModifier(eBuilding));

		if (bAdd)
		{
			if (!pCity->canConstruct(eBuilding, true, false, false))
			{
				return -1;
			}
		}
		else
		{
			if (!pCity->isHasRealBuilding(eBuilding))
			{
				return -1;
			}

			// Check other buildings in this city and make sure none of them require this one
			for (int iBuildingLoop = 0; iBuildingLoop < GC.getNumBuildingInfos(); iBuildingLoop++)
			{
				BuildingTypes eBuildingLoop = (BuildingTypes) iBuildingLoop;
				if (pCity->isHasRealBuilding(eBuildingLoop))
				{
					if (GC.getBuildingInfo(eBuildingLoop).isBuildingClassNeededInCity(kBuilding.getBuildingClassType()))
					{
						return -1;
					}
				}
			}
		}
	}

	// Increase cost if the XML defines that additional Buildings will cost more
	if (0 != GC.getBuildingInfo(eBuilding).getAdvancedStartCostIncrease())
	{
		iNumBuildingType = countNumBuildings(eBuilding);

		if (!bAdd)
		{
			--iNumBuildingType;
		}

		if (iNumBuildingType > 0)
		{
			iCost *= 100 + GC.getBuildingInfo(eBuilding).getAdvancedStartCostIncrease() * std::max(0, iNumBuildingType - getNumCities());
			iCost /= 100;
		}
	}

	return iCost;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Adding or removing Route
/////////////////////////////////////////////////////////////////////////////////////////////

int CvPlayer::getAdvancedStartRouteCost(RouteTypes eRoute, bool bAdd, CvPlot* pPlot)
{
	if (0 == getNumCities())
	{
		return -1;
	}

	if (eRoute == NO_ROUTE)
	{
		return -1;
	}

	int iNumRoutes = 0;

	int iCost = GC.getRouteInfo(eRoute).getAdvancedStartCost();

	// This denotes cities may not be purchased through Advanced Start
	if (iCost < 0)
	{
		return -1;
	}

	iCost *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent();
	iCost /= 100;

	// No invalid plots!
	if (pPlot != NULL)
	{
		if (pPlot->isCity())
		{
			return -1;
		}

		if (bAdd)
		{
			if (pPlot->isImpassable() || pPlot->isWater())
			{
				return -1;
			}
			// Can't place twice
			if (pPlot->getRouteType() == eRoute)
			{
				return -1;
			}
		}
		else
		{
			// Need Route to remove it
			if (pPlot->getRouteType() != eRoute)
			{
				return -1;
			}
		}

		// Must be owned by me
		if (pPlot->getOwner() != getID())
		{
			return -1;
		}
	}
	// Increase cost if the XML defines that additional units will cost more
	if (0 != GC.getRouteInfo(eRoute).getAdvancedStartCostIncrease())
	{
		int iPlotLoop = 0;
		CvPlot* pPlot;

		for (iPlotLoop = 0; iPlotLoop < GC.getMapINLINE().numPlots(); iPlotLoop++)
		{
			pPlot = GC.getMapINLINE().plotByIndex(iPlotLoop);

			if (pPlot->getRouteType() == eRoute)
			{
				++iNumRoutes;
			}
		}

		if (!bAdd)
		{
			--iNumRoutes;
		}

		if (iNumRoutes > 0)
		{
			iCost *= 100 + GC.getRouteInfo(eRoute).getAdvancedStartCostIncrease() * iNumRoutes;
			iCost /= 100;
		}
	}

	return iCost;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Adding or removing Improvement
/////////////////////////////////////////////////////////////////////////////////////////////

int CvPlayer::getAdvancedStartImprovementCost(ImprovementTypes eImprovement, bool bAdd, CvPlot* pPlot)
{
	if (eImprovement == NO_IMPROVEMENT)
	{
		return -1;
	}

	if (0 == getNumCities())
	{
		return -1;
	}

	int iNumImprovements = 0;
	int iCost = GC.getImprovementInfo(eImprovement).getAdvancedStartCost();

	// This denotes cities may not be purchased through Advanced Start
	if (iCost < 0)
	{
		return -1;
	}

	iCost *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent();
	iCost /= 100;

	// Can this Improvement be on our plot?
	if (pPlot != NULL)
	{
		if (bAdd)
		{
			// Valid Plot
			if (!pPlot->canHaveImprovement(eImprovement, getTeam(), false))
			{
				return -1;
			}

			bool bValid = false;

			for (int iI = 0; iI < GC.getNumBuildInfos(); ++iI)
			{
				CvBuildInfo& kBuild = GC.getBuildInfo((BuildTypes)iI);
				ImprovementTypes eLoopImprovement = ((ImprovementTypes)(kBuild.getImprovement()));
				if (eImprovement == eLoopImprovement && canBuild(pPlot, (BuildTypes)iI))
				{
					bValid = true;
					FeatureTypes eFeature = pPlot->getFeatureType();
					if (NO_FEATURE != eFeature && kBuild.isFeatureRemove(eFeature))
					{
						iCost += GC.getFeatureInfo(eFeature).getAdvancedStartRemoveCost();
					}
					break;
				}
			}
			if (!bValid)
			{
				return -1;
			}
			// Can't place twice
			if (pPlot->getImprovementType() == eImprovement)
			{
				return -1;
			}
		}
		else
		{
			// Need this improvement in order to remove it
			if (pPlot->getImprovementType() != eImprovement)
			{
				return -1;
			}
		}
		// Must be owned by me
		if (pPlot->getOwner() != getID())
		{
			return -1;
		}
	}
	// Increase cost if the XML defines that additional units will cost more
	if (0 != GC.getImprovementInfo(eImprovement).getAdvancedStartCostIncrease())
	{
		int iPlotLoop = 0;
		CvPlot* pPlot;
		for (iPlotLoop = 0; iPlotLoop < GC.getMapINLINE().numPlots(); iPlotLoop++)
		{
			pPlot = GC.getMapINLINE().plotByIndex(iPlotLoop);
			if (pPlot->getImprovementType() == eImprovement)
			{
				++iNumImprovements;
			}
		}
		if (!bAdd)
		{
			--iNumImprovements;
		}
		if (iNumImprovements > 0)
		{
			iCost *= 100 + GC.getImprovementInfo(eImprovement).getAdvancedStartCostIncrease() * iNumImprovements;
			iCost /= 100;
		}
	}

	return iCost;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Adding or removing Visibility
/////////////////////////////////////////////////////////////////////////////////////////////

int CvPlayer::getAdvancedStartVisibilityCost(bool bAdd, CvPlot* pPlot)
{
	if (0 == getNumCities())
	{
		return -1;
	}

	int iNumVisiblePlots = 0;
	int iCost = GC.getDefineINT("ADVANCED_START_VISIBILITY_COST");

	// This denotes Visibility may not be purchased through Advanced Start
	if (iCost == -1)
	{
		return -1;
	}

	// Valid Plot?
	if (pPlot != NULL)
	{
		if (bAdd)
		{
			if (pPlot->isRevealed(getTeam(), false))
			{
				return -1;
			}
			if (!pPlot->isAdjacentRevealed(getTeam()))
			{
				return -1;
			}
		}
		else
		{
			if (!pPlot->isRevealed(getTeam(), false))
			{
				return -1;
			}
		}
	}

	// Increase cost if the XML defines that additional units will cost more
	if (0 != GC.getDefineINT("ADVANCED_START_VISIBILITY_COST_INCREASE"))
	{
		int iPlotLoop = 0;
		CvPlot* pPlot;

		for (iPlotLoop = 0; iPlotLoop < GC.getMapINLINE().numPlots(); iPlotLoop++)
		{
			pPlot = GC.getMapINLINE().plotByIndex(iPlotLoop);

			if (pPlot->isRevealed(getTeam(), false))
			{
				++iNumVisiblePlots;
			}
		}

		if (!bAdd)
		{
			--iNumVisiblePlots;
		}

		if (iNumVisiblePlots > 0)
		{
			iCost *= 100 + GC.getDefineINT("ADVANCED_START_VISIBILITY_COST_INCREASE") * iNumVisiblePlots;
			iCost /= 100;
		}
	}

	return iCost;
}

void CvPlayer::doWarnings()
{
	//update enemy unit in your territory glow
	int iLoop;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		//update glow
		gDLL->getEntityIFace()->updateEnemyGlow(pLoopUnit->getUnitEntity());
	}

	//update enemy units close to your territory
	int iMaxCount = range(((getNumCities() + 4) / 7), 2, 5);
	for (int iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); iI++)
	{
		if (iMaxCount == 0)
		{
			break;
		}

		CvPlot* pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iI);

		if (pLoopPlot->isAdjacentPlayer(getID()))
		{
			if (!(pLoopPlot->isCity()))
			{
				if (pLoopPlot->isVisible(getTeam(), false))
				{
					CvUnit *pUnit = pLoopPlot->getVisibleEnemyDefender(getID());
					if (pUnit != NULL)
					{
						CvCity* pNearestCity = GC.getMapINLINE().findCity(pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE(), getID(), NO_TEAM, !(pLoopPlot->isWater()));
						if (pNearestCity != NULL)
						{
							CvWString szBuffer = gDLL->getText("TXT_KEY_MISC_ENEMY_TROOPS_SPOTTED", pNearestCity->getNameKey());
							gDLL->getInterfaceIFace()->addMessage(getID(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_ENEMY_TROOPS", MESSAGE_TYPE_INFO, pUnit->getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE(), true, true);
							iMaxCount--;
							if (pLoopPlot->isWater() && !pUnit->isAlwaysHostile(NULL))
							{
								unsigned char ucNavalDisturbation = pUnit->isOnlyDefensive() ? GC.getNAVAL_TRANSPORT_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY() : GC.getNAVAL_MILITARY_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY();
								pNearestCity->AI_changeNavalDisturbationCount(ucNavalDisturbation);
							}
						}
					}
				}
			}
		}
	}
}


void CvPlayer::processCivics(CivicTypes eCivic, int iChange)
{
	CvCivicInfo& kCivicInfo = GC.getCivicInfo(eCivic);

	changeGreatGeneralRateModifier(kCivicInfo.getGreatGeneralRateModifier() * iChange);
	changeDomesticGreatGeneralRateModifier(kCivicInfo.getDomesticGreatGeneralRateModifier() * iChange);
	changeFreeExperience(kCivicInfo.getFreeExperience() * iChange);
	changeWorkerSpeedModifier(kCivicInfo.getWorkerSpeedModifier() * iChange);
	changeImprovementUpgradeRateModifier(kCivicInfo.getImprovementUpgradeRateModifier() * iChange);
	changeMilitaryProductionModifier(kCivicInfo.getMilitaryProductionModifier() * iChange);
	changeExpInBorderModifier(kCivicInfo.getExpInBorderModifier() * iChange);
	changeNativeCombatModifier(kCivicInfo.getNativeCombatModifier() * iChange);
	changeDominateNativeBordersCount(kCivicInfo.isDominateNativeBorders() ? iChange : 0);
	changeRevolutionEuropeTradeCount(kCivicInfo.isRevolutionEuropeTrade() ? iChange : 0);

	if (kCivicInfo.getImmigrationConversion() != NO_YIELD)
	{
		if (iChange > 0)
		{
			setImmigrationConversion((YieldTypes) kCivicInfo.getImmigrationConversion());
		}
		else
		{
			setImmigrationConversion(YIELD_CROSSES);
		}
	}

	if (kCivicInfo.getNativeAttitudeChange() != 0)
	{
		for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes) iPlayer);

			if (kLoopPlayer.isAlive() && kLoopPlayer.isNative())
			{
				kLoopPlayer.AI_changeAttitudeExtra(getID(), kCivicInfo.getNativeAttitudeChange() * iChange);

				if (iChange > 0 && kCivicInfo.getNativeAttitudeChange() > 0)
				{
					GET_TEAM(getTeam()).makePeace(kLoopPlayer.getTeam());
				}
			}
		}
	}

	for (int iProfession = 0; iProfession < GC.getNumProfessionInfos(); ++iProfession)
	{
		changeProfessionCombatChange((ProfessionTypes) iProfession, kCivicInfo.getProfessionCombatChange(iProfession) * iChange);
	}

	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		changeYieldRateModifier(((YieldTypes)iI), (kCivicInfo.getYieldModifier(iI) * iChange));
		changeCapitalYieldRateModifier(((YieldTypes)iI), (kCivicInfo.getCapitalYieldModifier(iI) * iChange));
	}

	for (int iI = 0; iI < GC.getNumSpecialBuildingInfos(); iI++)
	{
		changeSpecialBuildingNotRequiredCount(((SpecialBuildingTypes)iI), ((kCivicInfo.isSpecialBuildingNotRequired(iI)) ? iChange : 0));
	}

	for (int iI = 0; iI < GC.getNumImprovementInfos(); iI++)
	{
		for (int iJ = 0; iJ < NUM_YIELD_TYPES; iJ++)
		{
			changeImprovementYieldChange(((ImprovementTypes)iI), ((YieldTypes)iJ), (kCivicInfo.getImprovementYieldChanges(iI, iJ) * iChange));
		}
	}

	for (int i = 0; i < kCivicInfo.getNumFreeUnitClasses(); ++i)
	{
		UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(kCivicInfo.getFreeUnitClass(i));
		if (eUnit != NO_UNIT)
		{
			int iLoop;
			for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
			{
				if (iChange > 0)
				{
					int iTargetPercent = pLoopCity->getRebelPercent();
					for (int j = 0; j < iChange; ++j)
					{
						CvUnit* pUnit = initUnit(eUnit, (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession(), pLoopCity->getX_INLINE(), pLoopCity->getY_INLINE());
						if (pUnit != NULL)
						{
							pLoopCity->addPopulationUnit(pUnit, NO_PROFESSION);
						}
					}
					if (pLoopCity->getRebelPercent() < iTargetPercent)
					{
						pLoopCity->setRebelSentiment(iTargetPercent * pLoopCity->getPopulation());
					}
				}
				else if (iChange < 0)
				{
					int iRemoved = 0;

					// remove proper unit from city
					for (int j = 0; j < -iChange; ++j)
					{
						CvUnit* pUnit = pLoopCity->removeUnitType(eUnit, (ProfessionTypes) GC.getCivilizationInfo(getCivilizationType()).getDefaultProfession());
						if (pUnit != NULL)
						{
							pUnit->kill(false);
							++iRemoved;
						}
					}

					// remove proper unit from field
					for (int j = iRemoved; j < -iChange; ++j)
					{
						int iUnitLoop;
						for (CvUnit* pUnit = firstUnit(&iUnitLoop); pUnit != NULL; pUnit = nextUnit(&iLoop))
						{
							if (pUnit->getUnitType() == eUnit)
							{
								pUnit->kill(false);
								++iRemoved;
								break;
							}
						}
					}

					// remove any city population
					int iRemaining = -iChange - iRemoved;
					if (iRemaining > 0 && pLoopCity->getPopulation() > iRemaining)
					{
						pLoopCity->changePopulation(-iRemaining);
						iRemoved += iRemaining;
					}

					// remove any unit from field
					for (int j = iRemoved; j < -iChange; ++j)
					{
						int iUnitLoop;
						for (CvUnit* pUnit = firstUnit(&iUnitLoop); pUnit != NULL; pUnit = nextUnit(&iLoop))
						{
							pUnit->kill(false);
							++iRemoved;
							break;
						}
					}
				}
			}
		}
	}
}

void CvPlayer::showMissedMessages()
{
	CvMessageQueue::iterator it = m_listGameMessages.begin();
	while (it != m_listGameMessages.end())
	{
		CvTalkingHeadMessage& msg = *it;
		if (!msg.getShown())
		{
			msg.setShown(true);
			gDLL->getInterfaceIFace()->showMessage(msg);
		}
		++it;
	}
}

bool CvPlayer::isPbemNewTurn() const
{
	return m_bPbemNewTurn;
}

void CvPlayer::setPbemNewTurn(bool bNew)
{
	m_bPbemNewTurn = bNew;
}



//
// read object from a stream
// used during load
//
void CvPlayer::read(FDataStreamBase* pStream)
{
	int iI;

	// Init data before load
	reset();

	uint uiFlag=0;
	pStream->Read(&uiFlag);	// flags for expansion

	pStream->Read(&m_iStartingX);
	pStream->Read(&m_iStartingY);
	pStream->Read(&m_iTotalPopulation);
	pStream->Read(&m_iTotalLand);
	pStream->Read(&m_iTotalLandScored);
	pStream->Read(&m_iGold);
	pStream->Read(&m_iIncomeAndExpendituresRecord);
	pStream->Read(&m_iNbCross);
	pStream->Read(&m_iNewEra);
	pStream->Read(&m_iSelectQuantity);
	pStream->Read(&m_iNumAgreement);
	pStream->Read(&m_iLevelReceiveSeedling);
	pStream->Read(&m_iEuropeWarehouseCapacity);
	pStream->Read(&m_iEuropeLoanPercent);
	pStream->Read(&m_iEuropeLoan);
	pStream->Read(&m_iParentMood);
	pStream->Read(&m_iTotalImmigrant);
	pStream->Read(&m_iAcceptImmigrant);
	pStream->Read(&m_iLeaveImmigrant);
	pStream->Read(&m_iDistanceToEuropeIA);
	pStream->Read(&m_iAdvancedStartPoints);
	pStream->Read(&m_iGreatGeneralsCreated);
	pStream->Read(&m_iGreatGeneralsThresholdModifier);
	pStream->Read(&m_iGreatAdmiralsThresholdModifier);
	pStream->Read(&m_iGreatGeneralRateModifier);
	pStream->Read(&m_iDomesticGreatGeneralRateModifier);
	pStream->Read(&m_iImmigrationThresholdMultiplier);
	pStream->Read(&m_iRevolutionEuropeUnitThresholdMultiplier);
	if (uiFlag > 1)
	{
		pStream->Read(&m_iKingNumUnitMultiplier);
	}
	if (uiFlag <= 2)
	{
		int iEducationThresholdMultiplier;
		pStream->Read(&iEducationThresholdMultiplier);
	}
	pStream->Read(&m_iNativeAngerModifier);
	pStream->Read(&m_iFreeExperience);
	pStream->Read(&m_iWorkerSpeedModifier);
	pStream->Read(&m_iImprovementUpgradeRateModifier);
	pStream->Read(&m_iMilitaryProductionModifier);
	pStream->Read(&m_iCityDefenseModifier);
	pStream->Read(&m_iHighestUnitLevel);
	pStream->Read(&m_iExpInBorderModifier);
	pStream->Read(&m_iLevelExperienceModifier);
	pStream->Read(&m_iCapitalCityID);
	pStream->Read(&m_iCitiesLost);
	pStream->Read(&m_iAssets);
	pStream->Read(&m_iPower);
	pStream->Read(&m_iPopulationScore);
	pStream->Read(&m_iLandScore);
	pStream->Read(&m_iCombatLandExperience);
	pStream->Read(&m_iCombatSeaExperience);
	if (uiFlag > 1)
	{
		pStream->Read(&m_iMissionarySuccessPercent);
	}
	pStream->Read(&m_iAttackFocusEuropeanPlayer);
	pStream->Read(&m_sImmigrationRelationPoints);	

	pStream->Read(&m_usWorkImprovementPercent);
	pStream->Read(&m_usColonistIntoColoniesAmount);
	pStream->Read(&m_usLandForceInsideColoniesAmount);
	pStream->Read(&m_usLandForceOutsideColoniesAmount);	
	pStream->Read(&m_usUnemployedAmount);
	pStream->Read(&m_usScoutAmount);
	pStream->Read(&m_usAgronomistAmount);
	pStream->Read(&m_usPioneerAmount);
	pStream->Read(&m_usMerchantShipAmount);
	pStream->Read(&m_usMilitaryShipAmount);

	pStream->Read(&m_bAlive);
	pStream->Read(&m_bTerritorialInfluenceMode);
	pStream->Read(&m_bLaunchFirstPopup);
	pStream->Read(&m_bKingGiveGalleon);
	pStream->Read(&m_bHighlightNewErasButton);
	pStream->Read(&m_bAttackedByPirates);
	pStream->Read(&m_bPenalitePlayerSave);
	pStream->Read(&m_bPenalitePlayerLoad);
	pStream->Read(&m_bEverAlive);
	pStream->Read(&m_bTurnActive);
	pStream->Read(&m_bAutoMoves);
	pStream->Read(&m_bEndTurn);
	pStream->Read(&m_bPbemNewTurn);
	pStream->Read(&m_bExtendedGame);
	pStream->Read(&m_bFoundedFirstCity);
	pStream->Read(&m_bStrike);
	pStream->Read(&m_bDisplaySeedling);

	pStream->Read((int*)&m_eID);
	pStream->Read((int*)&m_ePersonalityType);
	pStream->Read((int*)&m_eCurrentEra);
	pStream->Read((int*)&m_eParent);
	updateTeamType(); //m_eTeamType not saved
	updateHuman();
	pStream->Read((int*)&m_eImmigrationConversion);
	pStream->Read((int*)&m_ePlayerRole);

	pStream->Read(NUM_YIELD_TYPES, m_aiSeaPlotYield);
	pStream->Read(NUM_YIELD_TYPES, m_aiYieldRateModifier);
	pStream->Read(NUM_YIELD_TYPES, m_aiCapitalYieldRateModifier);
	pStream->Read(NUM_YIELD_TYPES, m_aiBuildingRequiredYieldModifier);
	pStream->Read(NUM_YIELD_TYPES, m_aiCityExtraYield);
	pStream->Read(NUM_YIELD_TYPES, m_aiExtraYieldThreshold);
	pStream->Read(NUM_YIELD_TYPES, m_aiYieldRandomPercent);
	pStream->Read(NUM_YIELD_TYPES, m_aiYieldTradedTotal);
	pStream->Read(NUM_YIELD_TYPES, m_aiYieldBoughtTotal);
	pStream->Read(NUM_YIELD_TYPES, m_aiTaxYieldModifierCount);
	pStream->Read(NUM_YIELD_TYPES, m_aiEuropeWarehouseYield);
	pStream->Read(NUM_YIELD_TYPES, m_aiEuropeSellTradeYield);
	pStream->Read(NUM_YIELD_TYPES, m_aiEuropeGoldSellTradeYield);
	pStream->Read(NUM_YIELD_TYPES, m_aiEuropeBuyTradeYield);
	pStream->Read(NUM_YIELD_TYPES, m_aiEuropeGoldBuyTradeYield);
	pStream->Read(NUM_YIELD_TYPES, m_aiEuropeTaxTradeYield);
	pStream->Read(NUM_YIELD_TYPES, m_aucEuropeNeeds);
	pStream->Read(NUM_YIELD_TYPES, m_aiNativeGetUnknownYield);
	pStream->Read(NUM_YIELD_TYPES, m_aiIATradeProposition);
	pStream->Read(NUM_YIELD_TYPES, m_aiHumanTradeProposition);
	pStream->Read(NUM_YIELD_TYPES, m_aiPlayerValue);
	if (uiFlag > 1)
	{
		pStream->Read(MAX_PLAYERS, m_aiMissionaryPoints);
		pStream->Read(MAX_PLAYERS, m_aiMissionaryThresholdMultiplier);
	}
	pStream->Read(MAX_PLAYERS, m_aiRegularityTrade);
	pStream->Read(NUM_HIST_TURN, m_aiUnemploymentHistory);
	pStream->Read(NUM_HIST_TURN, m_aiFoodHistory);
	pStream->Read(NUM_HIST_TURN, m_aiEconomicHistory);
	pStream->Read(NUM_HIST_TURN, m_aiReligionHistory);

	pStream->Read(NUM_RAND_VALUES, m_aucRandomValues);
	pStream->Read(NUM_UNIT_TYPES, m_aucUnitGivedByKing);
	pStream->Read(NUM_YIELD_TYPES, m_aucEconomicTrends);
	pStream->Read(NUM_YIELD_TYPES, m_aucEconomicTrendsRemainingTurns);

	pStream->Read(NUM_YIELD_TYPES, m_abYieldEuropeTradable);
	pStream->Read(NUM_YIELD_TYPES, m_abYieldUnknown);
	pStream->Read(NUM_YIELD_TYPES, m_abPinnedYieldInEurope);
	pStream->Read(NUM_YIELD_TYPES, m_abAutomaticTradeByYieldWithNative);	
	pStream->Read(NUM_YIELD_TYPES, m_abRegularityTradeInTurn);
	pStream->Read(NUM_FEAT_TYPES, m_abFeatAccomplished);
	pStream->Read(NUM_PLAYEROPTION_TYPES, m_abOptions);
	pStream->Read(NUM_SECONDARYPLAYEROPTION_TYPES, m_abSecondaryOptions);
	pStream->Read(NUM_TUTORIAL_DOANE, m_abHadTutorialDoane);
	pStream->Read(NUM_MAPS, m_abUnitUsedMap);
	pStream->Read(NUM_BUTTONPOPUP_TYPES, m_abLaunchedPopup);

	pStream->ReadString(m_szScriptData);

	FAssertMsg((0 < GC.getNumBonusInfos()), "GC.getNumBonusInfos() is not greater than zero but it is expected to be in CvPlayer::read");
	pStream->Read(GC.getNumImprovementInfos(), m_paiImprovementCount);
	pStream->Read(GC.getNumBuildingInfos(), m_paiFreeBuildingCount);
	pStream->Read(GC.getNumBuildingInfos(), m_pabFirstBuildingBuilt);
	pStream->Read(GC.getNumPirateInfos(), m_pabHasGeneratedPirates);
	pStream->Read(GC.getNumUnitClassInfos(), m_paiUnitClassCount);
	pStream->Read(GC.getNumUnitClassInfos(), m_paiEnrolUnitType);
	pStream->Read(GC.getNumUnitClassInfos(), m_paiUnitClassMaking);
	pStream->Read(GC.getNumUnitClassInfos(), m_paiUnitMarketPrice);
	pStream->Read(GC.getNumUnitClassInfos(), m_paiUnitKilledCounter);
	pStream->Read(GC.getNumUnitClassInfos(), m_paiUnitTurnRemaining);
	pStream->Read(GC.getNumUnitClassInfos(), m_paiUnitMoveChange);
	pStream->Read(GC.getNumUnitClassInfos(), m_paiUnitStrengthModifier);
	pStream->Read(GC.getNumProfessionInfos(), m_paiProfessionCombatChange);
	pStream->Read(GC.getNumProfessionInfos(), m_paiProfessionMoveChange);
	pStream->Read(GC.getNumBuildingClassInfos(), m_paiBuildingClassCount);
	pStream->Read(GC.getNumBuildingClassInfos(), m_paiBuildingClassMaking);
	pStream->Read(GC.getNumSpecialBuildingInfos(), m_paiSpecialBuildingNotRequiredCount);
	if (uiFlag <= 1)
	{
		std::vector<int> aiMissionaryPoints(GC.getNumCivilizationInfos());
		pStream->Read(GC.getNumCivilizationInfos(), &aiMissionaryPoints[0]);
		pStream->Read(GC.getNumCivilizationInfos(), &aiMissionaryPoints[0]);
	}
	pStream->Read(GC.getNumProfessionInfos(), m_aiProfessionEquipmentModifier);
	pStream->Read(GC.getNumTraitInfos(), m_aiTraitCount);

	for (iI=0;iI<GC.getNumCivicOptionInfos();iI++)
	{
		pStream->Read((int*)&m_paeCivics[iI]);
	}

	for (iI=0;iI<GC.getNumImprovementInfos();iI++)
	{
		pStream->Read(NUM_YIELD_TYPES, m_ppiImprovementYieldChange[iI]);
	}

	for (iI=0;iI<GC.getNumBuildingClassInfos();iI++)
	{
		pStream->Read(NUM_YIELD_TYPES, m_ppiBuildingYieldChange[iI]);
	}

	m_groupCycle.Read(pStream);
	{
		CvWString szBuffer;
		uint iSize;
		pStream->Read(&iSize);
		m_aszCityNames.resize(iSize);
		for (uint i = 0; i < iSize; i++)
		{
			pStream->ReadString(szBuffer);
			m_aszCityNames[i] = szBuffer;
		}
	}

	ReadStreamableFFreeListTrashArray(m_cities, pStream);

	ReadStreamableLinkList(m_seaways, pStream);

	m_tradeRoutes.Read(pStream);
	m_agreements.Read(pStream);
	m_hospitals.Read(pStream);
	m_tempUnits.Read(pStream);
	m_units.Read(pStream);
	m_cityJobs.Read(pStream);
	m_marketTransactions.Read(pStream);
	m_convoys.Read(pStream);

	freeEuropeUnits();
	int iNumEuropeUnits;
	pStream->Read(&iNumEuropeUnits);
	for (int i=0;i<iNumEuropeUnits;i++)
	{
		CvUnitAI *pUnit = new CvUnitAI();
		pUnit->read(pStream);
		m_aEuropeUnits.push_back(pUnit);
	}

	ReadStreamableFFreeListTrashArray(m_selectionGroups, pStream);
	ReadStreamableFFreeListTrashArray(m_eventsTriggered, pStream);

	{
		CvMessageQueue::_Alloc::size_type iSize;
		pStream->Read(&iSize);
		for (CvMessageQueue::_Alloc::size_type i = 0; i < iSize; i++)
		{
			CvTalkingHeadMessage message;
			message.read(*pStream);
			m_listGameMessages.push_back(message);
		}
	}
	{
		clearPopups();
		CvPopupQueue::_Alloc::size_type iSize;
		pStream->Read(&iSize);
		for (CvPopupQueue::_Alloc::size_type i = 0; i < iSize; i++)
		{
			CvPopupInfo* pInfo = new CvPopupInfo();
			if (NULL != pInfo)
			{
				pInfo->read(*pStream);
				m_listPopups.push_back(pInfo);
			}
		}
	}

	{
		clearDiplomacy();
		CvDiploQueue::_Alloc::size_type iSize;
		pStream->Read(&iSize);
		for (CvDiploQueue::_Alloc::size_type i = 0; i < iSize; i++)
		{
			CvDiploParameters* pDiplo = new CvDiploParameters(NO_PLAYER);
			if (NULL != pDiplo)
			{
				pDiplo->read(*pStream);
				m_listDiplomacy.push_back(pDiplo);
			}
		}
	}

	{
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iTurn;
			int iScore;
			pStream->Read(&iTurn);
			pStream->Read(&iScore);
			m_mapScoreHistory[iTurn] = iScore;
		}
	}

	{
		m_mapEconomyHistory.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iTurn;
			int iScore;
			pStream->Read(&iTurn);
			pStream->Read(&iScore);
			m_mapEconomyHistory[iTurn] = iScore;
		}
	}

	{
		m_mapCrossHistory.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iTurn;
			int iScore;
			pStream->Read(&iTurn);
			pStream->Read(&iScore);
			m_mapCrossHistory[iTurn] = iScore;
		}
	}

	{
		m_mapIndustryHistory.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iTurn;
			int iScore;
			pStream->Read(&iTurn);
			pStream->Read(&iScore);
			m_mapIndustryHistory[iTurn] = iScore;
		}
	}

	{
		m_mapAgricultureHistory.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iTurn;
			int iScore;
			pStream->Read(&iTurn);
			pStream->Read(&iScore);
			m_mapAgricultureHistory[iTurn] = iScore;
		}
	}

	{
		m_mapLandMilitaryHistory.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iTurn;
			int iScore;
			pStream->Read(&iTurn);
			pStream->Read(&iScore);
			m_mapLandMilitaryHistory[iTurn] = iScore;
		}
	}

	{
		m_mapSeaMilitaryHistory.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iTurn;
			int iScore;
			pStream->Read(&iTurn);
			pStream->Read(&iScore);
			m_mapSeaMilitaryHistory[iTurn] = iScore;
		}
	}

	{
		m_mapNavalMerchantHistory.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iTurn;
			int iScore;
			pStream->Read(&iTurn);
			pStream->Read(&iScore);
			m_mapNavalMerchantHistory[iTurn] = iScore;
		}
	}

	{
		m_mapCultureHistory.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iTurn;
			int iScore;
			pStream->Read(&iTurn);
			pStream->Read(&iScore);
			m_mapCultureHistory[iTurn] = iScore;
		}
	}
	{
		m_mapTotalImmigration.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iUnitType;
			int iScore;
			pStream->Read(&iUnitType);
			pStream->Read(&iScore);
			m_mapTotalImmigration[iUnitType] = iScore;
		}
	}
	{
		m_mapTotalImmigrationAsked.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iUnitType;
			int iScore;
			pStream->Read(&iUnitType);
			pStream->Read(&iScore);
			m_mapTotalImmigrationAsked[iUnitType] = iScore;
		}
	}
	{
		m_mapTurnCountWithoutProposition.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iUnitType;
			int iScore;
			pStream->Read(&iUnitType);
			pStream->Read(&iScore);
			m_mapTurnCountWithoutProposition[iUnitType] = iScore;
		}
	}
	{
		m_mapMaxTurnWithoutProposition.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iUnitType;
			int iScore;
			pStream->Read(&iUnitType);
			pStream->Read(&iScore);
			m_mapMaxTurnWithoutProposition[iUnitType] = iScore;
		}
	}
	{
		m_mapCurrentImmigrationProbability.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iUnitType;
			float fScore;
			pStream->Read(&iUnitType);
			pStream->Read(&fScore);
			m_mapCurrentImmigrationProbability[iUnitType] = fScore;
		}
	}
	{
		m_mapFirstUnitImmigrationProbability.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iUnitType;
			float fScore;
			pStream->Read(&iUnitType);
			pStream->Read(&fScore);
			m_mapFirstUnitImmigrationProbability[iUnitType] = fScore;
		}
	}
	{
		m_mapAverageImmigrationProbability.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iUnitType;
			float fScore;
			pStream->Read(&iUnitType);
			pStream->Read(&fScore);
			m_mapAverageImmigrationProbability[iUnitType] = fScore;
		}
	}

	{
		m_mapImmigrationHistory.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iTurn;
			pStream->Read(&iTurn);

			uint iSizeUnits;
			pStream->Read(&iSizeUnits);
			for (uint j = 0; j < iSizeUnits; j++)
			{
				uint iUnitType;
				int iValue;
				pStream->Read(&iUnitType);
				pStream->Read(&iValue);
				m_mapImmigrationHistory[iTurn][iUnitType] = iValue;
			}
		}
	}




	{
		m_mapEventsOccured.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			EventTriggeredData kData;
			EventTypes eEvent;
			pStream->Read((int*)&eEvent);
			kData.read(pStream);
			m_mapEventsOccured[eEvent] = kData;
		}
	}

	{
		m_mapEventCountdown.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			EventTriggeredData kData;
			EventTypes eEvent;
			pStream->Read((int*)&eEvent);
			kData.read(pStream);
			m_mapEventCountdown[eEvent] = kData;
		}
	}

	{
		m_aFreeUnitCombatPromotions.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iUnitCombat;
			int iPromotion;
			pStream->Read(&iUnitCombat);
			pStream->Read(&iPromotion);
			m_aFreeUnitCombatPromotions.push_back(std::make_pair((UnitCombatTypes)iUnitCombat, (PromotionTypes)iPromotion));
		}
	}

	{
		m_aFreeUnitClassPromotions.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			int iUnitClass;
			int iPromotion;
			pStream->Read(&iUnitClass);
			pStream->Read(&iPromotion);
			m_aFreeUnitClassPromotions.push_back(std::make_pair((UnitClassTypes)iUnitClass, (PromotionTypes)iPromotion));
		}
	}

	{
		m_aEuropeRevolutionUnits.clear();
		uint iSize;
		pStream->Read(&iSize);
		for (uint i = 0; i < iSize; i++)
		{
			ProfessionTypes eProfession;
			UnitTypes eUnit;
			pStream->Read((int*)&eUnit);
			pStream->Read((int*)&eProfession);
			m_aEuropeRevolutionUnits.push_back(std::make_pair(eUnit, eProfession));
		}
	}

	m_triggersFired.clear();
	uint iSize;
	pStream->Read(&iSize);
	for (uint i = 0; i < iSize; i++)
	{
		int iTrigger;
		pStream->Read(&iTrigger);
		m_triggersFired.push_back((EventTriggerTypes)iTrigger);
	}

	// Get the NetID from the initialization structure
	setNetID(gDLL->getAssignedNetworkID(getID()));

	pStream->Read(&m_iCrossesStored);
	pStream->Read(&m_iBellsStored);
	pStream->Read(&m_iTaxRate);
	pStream->Read(&m_iLastTurnOfTaxesIncrease);
	pStream->Read(&m_iNativeCombatModifier);
	pStream->Read(&m_iDominateNativeBordersCount);
	pStream->Read(&m_iRevolutionEuropeTradeCount);
	pStream->Read(&m_iMissionaryRateModifier);
}

//
// save object to a stream
// used during save
//
void CvPlayer::write(FDataStreamBase* pStream)
{
	int iI;

	uint uiFlag = 3;
	pStream->Write(uiFlag);		// flag for expansion

	pStream->Write(m_iStartingX);
	pStream->Write(m_iStartingY);
	pStream->Write(m_iTotalPopulation);
	pStream->Write(m_iTotalLand);
	pStream->Write(m_iTotalLandScored);
	pStream->Write(m_iGold);
	pStream->Write(m_iIncomeAndExpendituresRecord);
	pStream->Write(m_iNbCross);
	pStream->Write(m_iNewEra);
	pStream->Write(m_iSelectQuantity);
	pStream->Write(m_iNumAgreement);
	pStream->Write(m_iLevelReceiveSeedling);
	pStream->Write(m_iEuropeWarehouseCapacity);
	pStream->Write(m_iEuropeLoanPercent);
	pStream->Write(m_iEuropeLoan);
	pStream->Write(m_iParentMood);
	pStream->Write(m_iTotalImmigrant);
	pStream->Write(m_iAcceptImmigrant);
	pStream->Write(m_iLeaveImmigrant);
	pStream->Write(m_iDistanceToEuropeIA);
	pStream->Write(m_iAdvancedStartPoints);
	pStream->Write(m_iGreatGeneralsCreated);
	pStream->Write(m_iGreatGeneralsThresholdModifier);
	pStream->Write(m_iGreatAdmiralsThresholdModifier);
	pStream->Write(m_iGreatGeneralRateModifier);
	pStream->Write(m_iDomesticGreatGeneralRateModifier);
	pStream->Write(m_iImmigrationThresholdMultiplier);
	pStream->Write(m_iRevolutionEuropeUnitThresholdMultiplier);
	pStream->Write(m_iKingNumUnitMultiplier);
	pStream->Write(m_iNativeAngerModifier);
	pStream->Write(m_iFreeExperience);
	pStream->Write(m_iWorkerSpeedModifier);
	pStream->Write(m_iImprovementUpgradeRateModifier);
	pStream->Write(m_iMilitaryProductionModifier);
	pStream->Write(m_iCityDefenseModifier);
	pStream->Write(m_iHighestUnitLevel);
	pStream->Write(m_iExpInBorderModifier);
	pStream->Write(m_iLevelExperienceModifier);
	pStream->Write(m_iCapitalCityID);
	pStream->Write(m_iCitiesLost);
	pStream->Write(m_iAssets);
	pStream->Write(m_iPower);
	pStream->Write(m_iPopulationScore);
	pStream->Write(m_iLandScore);
	pStream->Write(m_iCombatLandExperience);
	pStream->Write(m_iCombatSeaExperience);
	pStream->Write(m_iMissionarySuccessPercent);
	pStream->Write(m_iAttackFocusEuropeanPlayer);
	pStream->Write(m_sImmigrationRelationPoints);

	pStream->Write(m_usWorkImprovementPercent);
	pStream->Write(m_usColonistIntoColoniesAmount);
	pStream->Write(m_usLandForceInsideColoniesAmount);
	pStream->Write(m_usLandForceOutsideColoniesAmount);
	pStream->Write(m_usUnemployedAmount);
	pStream->Write(m_usScoutAmount);
	pStream->Write(m_usAgronomistAmount);
	pStream->Write(m_usPioneerAmount);
	pStream->Write(m_usMerchantShipAmount);
	pStream->Write(m_usMilitaryShipAmount);

	pStream->Write(m_bAlive);
	pStream->Write(m_bTerritorialInfluenceMode);
	pStream->Write(m_bLaunchFirstPopup);
	pStream->Write(m_bKingGiveGalleon);
	pStream->Write(m_bHighlightNewErasButton);
	pStream->Write(m_bAttackedByPirates);
	pStream->Write(m_bPenalitePlayerSave);
	pStream->Write(m_bPenalitePlayerLoad);
	pStream->Write(m_bEverAlive);
	pStream->Write(m_bTurnActive);
	pStream->Write(m_bAutoMoves);
	pStream->Write(m_bEndTurn);
	pStream->Write(m_bPbemNewTurn && GC.getGameINLINE().isPbem());
	pStream->Write(m_bExtendedGame);
	pStream->Write(m_bFoundedFirstCity);
	pStream->Write(m_bStrike);
	pStream->Write(m_bDisplaySeedling);

	pStream->Write(m_eID);
	pStream->Write(m_ePersonalityType);
	pStream->Write(m_eCurrentEra);
	pStream->Write(m_eParent);
	//m_eTeamType not saved
	pStream->Write(m_eImmigrationConversion);
	pStream->Write(m_ePlayerRole);

	pStream->Write(NUM_YIELD_TYPES, m_aiSeaPlotYield);
	pStream->Write(NUM_YIELD_TYPES, m_aiYieldRateModifier);
	pStream->Write(NUM_YIELD_TYPES, m_aiCapitalYieldRateModifier);
	pStream->Write(NUM_YIELD_TYPES, m_aiBuildingRequiredYieldModifier);
	pStream->Write(NUM_YIELD_TYPES, m_aiCityExtraYield);
	pStream->Write(NUM_YIELD_TYPES, m_aiExtraYieldThreshold);
	pStream->Write(NUM_YIELD_TYPES, m_aiYieldRandomPercent);
	pStream->Write(NUM_YIELD_TYPES, m_aiYieldTradedTotal);
	pStream->Write(NUM_YIELD_TYPES, m_aiYieldBoughtTotal);
	pStream->Write(NUM_YIELD_TYPES, m_aiTaxYieldModifierCount);
	pStream->Write(NUM_YIELD_TYPES, m_aiEuropeWarehouseYield);
	pStream->Write(NUM_YIELD_TYPES, m_aiEuropeSellTradeYield);
	pStream->Write(NUM_YIELD_TYPES, m_aiEuropeGoldSellTradeYield);
	pStream->Write(NUM_YIELD_TYPES, m_aiEuropeBuyTradeYield);
	pStream->Write(NUM_YIELD_TYPES, m_aiEuropeGoldBuyTradeYield);
	pStream->Write(NUM_YIELD_TYPES, m_aiEuropeTaxTradeYield);
	pStream->Write(NUM_YIELD_TYPES, m_aucEuropeNeeds);
	pStream->Write(NUM_YIELD_TYPES, m_aiNativeGetUnknownYield);
	pStream->Write(NUM_YIELD_TYPES, m_aiIATradeProposition);
	pStream->Write(NUM_YIELD_TYPES, m_aiHumanTradeProposition);
	pStream->Write(NUM_YIELD_TYPES, m_aiPlayerValue);
	pStream->Write(MAX_PLAYERS, m_aiMissionaryPoints);
	pStream->Write(MAX_PLAYERS, m_aiMissionaryThresholdMultiplier);
	pStream->Write(MAX_PLAYERS, m_aiRegularityTrade);
	pStream->Write(NUM_HIST_TURN, m_aiUnemploymentHistory);
	pStream->Write(NUM_HIST_TURN, m_aiFoodHistory);
	pStream->Write(NUM_HIST_TURN, m_aiEconomicHistory);
	pStream->Write(NUM_HIST_TURN, m_aiReligionHistory);

	pStream->Write(NUM_RAND_VALUES, m_aucRandomValues);
	pStream->Write(NUM_UNIT_TYPES, m_aucUnitGivedByKing);
	pStream->Write(NUM_YIELD_TYPES, m_aucEconomicTrends);
	pStream->Write(NUM_YIELD_TYPES, m_aucEconomicTrendsRemainingTurns);

	pStream->Write(NUM_YIELD_TYPES, m_abYieldEuropeTradable);
	pStream->Write(NUM_YIELD_TYPES, m_abYieldUnknown);
	pStream->Write(NUM_YIELD_TYPES, m_abPinnedYieldInEurope);
	pStream->Write(NUM_YIELD_TYPES, m_abAutomaticTradeByYieldWithNative);	
	pStream->Write(NUM_YIELD_TYPES, m_abRegularityTradeInTurn);
	pStream->Write(NUM_FEAT_TYPES, m_abFeatAccomplished);
	pStream->Write(NUM_PLAYEROPTION_TYPES, m_abOptions);
	pStream->Write(NUM_SECONDARYPLAYEROPTION_TYPES, m_abSecondaryOptions);
	pStream->Write(NUM_TUTORIAL_DOANE, m_abHadTutorialDoane);
	pStream->Write(NUM_MAPS, m_abUnitUsedMap);
	pStream->Write(NUM_BUTTONPOPUP_TYPES, m_abLaunchedPopup);

	pStream->WriteString(m_szScriptData);

	FAssertMsg((0 < GC.getNumBonusInfos()), "GC.getNumBonusInfos() is not greater than zero but an array is being allocated in CvPlayer::write");
	pStream->Write(GC.getNumImprovementInfos(), m_paiImprovementCount);
	pStream->Write(GC.getNumBuildingInfos(), m_paiFreeBuildingCount);
	pStream->Write(GC.getNumBuildingInfos(), m_pabFirstBuildingBuilt);
	pStream->Write(GC.getNumPirateInfos(), m_pabHasGeneratedPirates);
	pStream->Write(GC.getNumUnitClassInfos(), m_paiUnitClassCount);
	pStream->Write(GC.getNumUnitClassInfos(), m_paiEnrolUnitType);
	pStream->Write(GC.getNumUnitClassInfos(), m_paiUnitClassMaking);
	pStream->Write(GC.getNumUnitClassInfos(), m_paiUnitMarketPrice);
	pStream->Write(GC.getNumUnitClassInfos(), m_paiUnitKilledCounter);
	pStream->Write(GC.getNumUnitClassInfos(), m_paiUnitTurnRemaining);
	pStream->Write(GC.getNumUnitClassInfos(), m_paiUnitMoveChange);
	pStream->Write(GC.getNumUnitClassInfos(), m_paiUnitStrengthModifier);
	pStream->Write(GC.getNumProfessionInfos(), m_paiProfessionCombatChange);
	pStream->Write(GC.getNumProfessionInfos(), m_paiProfessionMoveChange);
	pStream->Write(GC.getNumBuildingClassInfos(), m_paiBuildingClassCount);
	pStream->Write(GC.getNumBuildingClassInfos(), m_paiBuildingClassMaking);
	pStream->Write(GC.getNumSpecialBuildingInfos(), m_paiSpecialBuildingNotRequiredCount);
	pStream->Write(GC.getNumProfessionInfos(), m_aiProfessionEquipmentModifier);
	pStream->Write(GC.getNumTraitInfos(), m_aiTraitCount);

	for (iI=0;iI<GC.getNumCivicOptionInfos();iI++)
	{
		pStream->Write(m_paeCivics[iI]);
	}

	for (iI=0;iI<GC.getNumImprovementInfos();iI++)
	{
		pStream->Write(NUM_YIELD_TYPES, m_ppiImprovementYieldChange[iI]);
	}

	for (iI=0;iI<GC.getNumBuildingClassInfos();iI++)
	{
		pStream->Write(NUM_YIELD_TYPES, m_ppiBuildingYieldChange[iI]);
	}

	m_groupCycle.Write(pStream);
	{
		uint iSize = m_aszCityNames.size();
		pStream->Write(iSize);
		for (uint i = 0; i < iSize; ++i)
		{
			pStream->WriteString(m_aszCityNames[i]);
		}
	}

	WriteStreamableFFreeListTrashArray(m_cities, pStream);
	WriteStreamableLinkList(m_seaways, pStream);

	m_tradeRoutes.Write(pStream);
	m_agreements.Write(pStream);
	m_hospitals.Write(pStream);
	m_tempUnits.Write(pStream);
	m_units.Write(pStream);
	m_cityJobs.Write(pStream);
	m_marketTransactions.Write(pStream);
	m_convoys.Write(pStream);

	pStream->Write((int)m_aEuropeUnits.size());
	for (int i=0;i<(int)m_aEuropeUnits.size();i++)
	{
		m_aEuropeUnits[i]->write(pStream);
	}

	WriteStreamableFFreeListTrashArray(m_selectionGroups, pStream);
	WriteStreamableFFreeListTrashArray(m_eventsTriggered, pStream);

	{
		CvMessageQueue::_Alloc::size_type iSize = m_listGameMessages.size();
		pStream->Write(iSize);
		CvMessageQueue::iterator it;
		for (it = m_listGameMessages.begin(); it != m_listGameMessages.end(); ++it)
		{
			CvTalkingHeadMessage& message = *it;
			message.write(*pStream);
		}
	}

	{
		CvPopupQueue currentPopups;
		if (GC.getGameINLINE().isNetworkMultiPlayer())
		{
			// don't save open popups in MP to avoid having different state on different machines
			currentPopups.clear();
		}
		else
		{
			gDLL->getInterfaceIFace()->getDisplayedButtonPopups(currentPopups);
		}
		CvPopupQueue::_Alloc::size_type iSize = m_listPopups.size() + currentPopups.size();
		pStream->Write(iSize);
		CvPopupQueue::iterator it;
		for (it = currentPopups.begin(); it != currentPopups.end(); ++it)
		{
			CvPopupInfo* pInfo = *it;
			if (NULL != pInfo)
			{
				pInfo->write(*pStream);
			}
		}
		for (it = m_listPopups.begin(); it != m_listPopups.end(); ++it)
		{
			CvPopupInfo* pInfo = *it;
			if (NULL != pInfo)
			{
				pInfo->write(*pStream);
			}
		}
	}

	{
		CvDiploQueue::_Alloc::size_type iSize = m_listDiplomacy.size();
		pStream->Write(iSize);
		CvDiploQueue::iterator it;
		for (it = m_listDiplomacy.begin(); it != m_listDiplomacy.end(); ++it)
		{
			CvDiploParameters* pDiplo = *it;
			if (NULL != pDiplo)
			{
				pDiplo->write(*pStream);
			}
		}
	}

	{
		uint iSize = m_mapScoreHistory.size();
		pStream->Write(iSize);
		CvTurnScoreMap::iterator it;
		for (it = m_mapScoreHistory.begin(); it != m_mapScoreHistory.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_mapEconomyHistory.size();
		pStream->Write(iSize);
		CvTurnScoreMap::iterator it;
		for (it = m_mapEconomyHistory.begin(); it != m_mapEconomyHistory.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_mapCrossHistory.size();
		pStream->Write(iSize);
		CvTurnScoreMap::iterator it;
		for (it = m_mapCrossHistory.begin(); it != m_mapCrossHistory.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_mapIndustryHistory.size();
		pStream->Write(iSize);
		CvTurnScoreMap::iterator it;
		for (it = m_mapIndustryHistory.begin(); it != m_mapIndustryHistory.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_mapAgricultureHistory.size();
		pStream->Write(iSize);
		CvTurnScoreMap::iterator it;
		for (it = m_mapAgricultureHistory.begin(); it != m_mapAgricultureHistory.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_mapLandMilitaryHistory.size();
		pStream->Write(iSize);
		CvTurnScoreMap::iterator it;
		for (it = m_mapLandMilitaryHistory.begin(); it != m_mapLandMilitaryHistory.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}


	{
		uint iSize = m_mapSeaMilitaryHistory.size();
		pStream->Write(iSize);
		CvTurnScoreMap::iterator it;
		for (it = m_mapSeaMilitaryHistory.begin(); it != m_mapSeaMilitaryHistory.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}


	{
		uint iSize = m_mapNavalMerchantHistory.size();
		pStream->Write(iSize);
		CvTurnScoreMap::iterator it;
		for (it = m_mapNavalMerchantHistory.begin(); it != m_mapNavalMerchantHistory.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_mapCultureHistory.size();
		pStream->Write(iSize);
		CvTurnScoreMap::iterator it;
		for (it = m_mapCultureHistory.begin(); it != m_mapCultureHistory.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_mapTotalImmigration.size();
		pStream->Write(iSize);
		CvImmigrationIntInfoMap::iterator it;
		for (it = m_mapTotalImmigration.begin(); it != m_mapTotalImmigration.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_mapTotalImmigrationAsked.size();
		pStream->Write(iSize);
		CvImmigrationIntInfoMap::iterator it;
		for (it = m_mapTotalImmigrationAsked.begin(); it != m_mapTotalImmigrationAsked.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}
	{
		uint iSize = m_mapTurnCountWithoutProposition.size();
		pStream->Write(iSize);
		CvImmigrationIntInfoMap::iterator it;
		for (it = m_mapTurnCountWithoutProposition.begin(); it != m_mapTurnCountWithoutProposition.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}
	{
		uint iSize = m_mapMaxTurnWithoutProposition.size();
		pStream->Write(iSize);
		CvImmigrationIntInfoMap::iterator it;
		for (it = m_mapMaxTurnWithoutProposition.begin(); it != m_mapMaxTurnWithoutProposition.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}
	{
		uint iSize = m_mapCurrentImmigrationProbability.size();
		pStream->Write(iSize);
		CvImmigrationFloatInfoMap::iterator it;
		for (it = m_mapCurrentImmigrationProbability.begin(); it != m_mapCurrentImmigrationProbability.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}
	{
		uint iSize = m_mapFirstUnitImmigrationProbability.size();
		pStream->Write(iSize);
		CvImmigrationFloatInfoMap::iterator it;
		for (it = m_mapFirstUnitImmigrationProbability.begin(); it != m_mapFirstUnitImmigrationProbability.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}
	{
		uint iSize = m_mapAverageImmigrationProbability.size();
		pStream->Write(iSize);
		CvImmigrationFloatInfoMap::iterator it;
		for (it = m_mapAverageImmigrationProbability.begin(); it != m_mapAverageImmigrationProbability.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_mapImmigrationHistory.size();
		pStream->Write(iSize);
		CvTurnImmigrationMap::iterator it;

		for (it = m_mapImmigrationHistory.begin(); it != m_mapImmigrationHistory.end(); ++it)
		{
			pStream->Write((*it).first);//turn

			stdext::hash_map<int, int> tempUnits = (*it).second;
			iSize = tempUnits.size();
			pStream->Write(iSize);
			stdext::hash_map<int, int>::iterator unitIt;
			for (unitIt = tempUnits.begin(); unitIt != tempUnits.end(); ++unitIt)
			{
				pStream->Write((*unitIt).first);
				pStream->Write((*unitIt).second);
			}
		}
	}

	{
		uint iSize = m_mapEventsOccured.size();
		pStream->Write(iSize);
		CvEventMap::iterator it;
		for (it = m_mapEventsOccured.begin(); it != m_mapEventsOccured.end(); ++it)
		{
			pStream->Write((*it).first);
			(*it).second.write(pStream);
		}
	}

	{
		uint iSize = m_mapEventCountdown.size();
		pStream->Write(iSize);
		CvEventMap::iterator it;
		for (it = m_mapEventCountdown.begin(); it != m_mapEventCountdown.end(); ++it)
		{
			pStream->Write((*it).first);
			(*it).second.write(pStream);
		}
	}

	{
		uint iSize = m_aFreeUnitCombatPromotions.size();
		pStream->Write(iSize);
		UnitCombatPromotionArray::iterator it;
		for (it = m_aFreeUnitCombatPromotions.begin(); it != m_aFreeUnitCombatPromotions.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_aFreeUnitClassPromotions.size();
		pStream->Write(iSize);
		UnitClassPromotionArray::iterator it;
		for (it = m_aFreeUnitClassPromotions.begin(); it != m_aFreeUnitClassPromotions.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_aEuropeRevolutionUnits.size();
		pStream->Write(iSize);
		std::vector< std::pair<UnitTypes, ProfessionTypes> >::iterator it;
		for (it = m_aEuropeRevolutionUnits.begin(); it != m_aEuropeRevolutionUnits.end(); ++it)
		{
			pStream->Write((*it).first);
			pStream->Write((*it).second);
		}
	}

	{
		uint iSize = m_triggersFired.size();
		pStream->Write(iSize);
		std::vector<EventTriggerTypes>::iterator it;
		for (it = m_triggersFired.begin(); it != m_triggersFired.end(); ++it)
		{
			pStream->Write((*it));
		}
	}

	pStream->Write(m_iCrossesStored);
	pStream->Write(m_iBellsStored);
	pStream->Write(m_iTaxRate);
	pStream->Write(m_iLastTurnOfTaxesIncrease);
	pStream->Write(m_iNativeCombatModifier);
	pStream->Write(m_iDominateNativeBordersCount);
	pStream->Write(m_iRevolutionEuropeTradeCount);
	pStream->Write(m_iMissionaryRateModifier);
}

void CvPlayer::createGreatGeneral(UnitTypes eGreatGeneralUnit, bool bIncrementExperience, int iX, int iY)
{
	CvUnit* pGreatUnit = initUnit(eGreatGeneralUnit, (ProfessionTypes) GC.getUnitInfo(eGreatGeneralUnit).getDefaultProfession(), iX, iY);
	if (NULL == pGreatUnit)
	{
		FAssert(false);
		return;
	}

	if (bIncrementExperience)
	{
		incrementGreatGeneralsCreated();
		if (eGreatGeneralUnit == UNIT_GREAT_ADMIRAL)
		{
			changeGreatAdmiralsThresholdModifier(GC.getDefineINT("GREAT_GENERALS_THRESHOLD_INCREASE"));
		}
		else
		{
			changeGreatGeneralsThresholdModifier(GC.getDefineINT("GREAT_GENERALS_THRESHOLD_INCREASE"));
		}

		for (int iI = 0; iI < MAX_PLAYERS; iI++)
		{
			if (GET_PLAYER((PlayerTypes)iI).getTeam() == getTeam())
			{
				if (eGreatGeneralUnit == UNIT_GREAT_ADMIRAL)
				{
					GET_PLAYER((PlayerTypes)iI).changeGreatAdmiralsThresholdModifier(GC.getDefineINT("GREAT_GENERALS_THRESHOLD_INCREASE_TEAM"));
				}
				else
				{
					GET_PLAYER((PlayerTypes)iI).changeGreatGeneralsThresholdModifier(GC.getDefineINT("GREAT_GENERALS_THRESHOLD_INCREASE_TEAM"));
				}
			}
		}
	}


	CvPlot* pPlot = GC.getMapINLINE().plot(iX, iY);
	CvCity* pCity = pPlot->getPlotCity();
	CvWString szReplayMessage;

	if (pPlot)
	{
		if (pCity)
		{
			CvWString szCity;
			szCity.Format(L"%s (%s)", pCity->getName().GetCString(), GET_PLAYER(pCity->getOwnerINLINE()).getName());
			szReplayMessage = gDLL->getText("TXT_KEY_MISC_GP_BORN", pGreatUnit->getName().GetCString(), szCity.GetCString());
		}
		else
		{
			szReplayMessage = gDLL->getText("TXT_KEY_MISC_GP_BORN_FIELD", pGreatUnit->getName().GetCString());
		}
		GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getID(), szReplayMessage, iX, iY, (ColorTypes)GC.getInfoTypeForString("COLOR_UNIT_TEXT"));
	}

	for (int iI = 0; iI < MAX_PLAYERS; iI++)
	{
		if (GET_PLAYER((PlayerTypes)iI).isAlive())
		{
			if (pPlot->isRevealed(GET_PLAYER((PlayerTypes)iI).getTeam(), false))
			{
				gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), false, GC.getEVENT_MESSAGE_TIME(), szReplayMessage, "AS2D_UNIT_GREATPEOPLE", MESSAGE_TYPE_MAJOR_EVENT, pGreatUnit->getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_UNIT_TEXT"), iX, iY, true, true);
			}
			else
			{
				CvWString szMessage = gDLL->getText("TXT_KEY_MISC_GP_BORN_SOMEWHERE", pGreatUnit->getName().GetCString());
				gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), false, GC.getEVENT_MESSAGE_TIME(), szMessage, "AS2D_UNIT_GREATPEOPLE", MESSAGE_TYPE_MAJOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_UNIT_TEXT"));
			}
		}
	}
}


const EventTriggeredData* CvPlayer::getEventOccured(EventTypes eEvent) const
{
	FAssert(eEvent >= 0 && eEvent < GC.getNumEventInfos());

	CvEventMap::const_iterator it = m_mapEventsOccured.find(eEvent);

	if (it == m_mapEventsOccured.end())
	{
		return NULL;
	}

	return &((*it).second);
}

bool CvPlayer::isTriggerFired(EventTriggerTypes eEventTrigger) const
{
	return (std::find(m_triggersFired.begin(), m_triggersFired.end(), eEventTrigger) != m_triggersFired.end());
}

void CvPlayer::resetEventOccured(EventTypes eEvent, bool bAnnounce)
{
	FAssert(eEvent >= 0 && eEvent < GC.getNumEventInfos());

	CvEventMap::iterator it = m_mapEventsOccured.find(eEvent);

	if (it != m_mapEventsOccured.end())
	{
		expireEvent((*it).first, (*it).second, bAnnounce);
		m_mapEventsOccured.erase(it);
	}
}

void CvPlayer::setEventOccured(EventTypes eEvent, const EventTriggeredData& kEventTriggered, bool bOthers)
{
	FAssert(eEvent >= 0 && eEvent < GC.getNumEventInfos());

	m_mapEventsOccured[eEvent] = kEventTriggered;

	if (GC.getEventInfo(eEvent).isQuest())
	{
		CvWStringBuffer szMessageBuffer;
		szMessageBuffer.append(GC.getEventInfo(eEvent).getDescription());
		GAMETEXT.setEventHelp(szMessageBuffer, eEvent, kEventTriggered.getID(), getID());
		gDLL->getInterfaceIFace()->addQuestMessage(getID(), szMessageBuffer.getCString(), kEventTriggered.getID());
	}

	if (bOthers)
	{
		if (GC.getEventInfo(eEvent).isGlobal())
		{
			for (int i = 0; i < MAX_PLAYERS; i++)
			{
				if (i != getID())
				{
					GET_PLAYER((PlayerTypes)i).setEventOccured(eEvent, kEventTriggered, false);
				}
			}
		}
		else if (GC.getEventInfo(eEvent).isTeam())
		{
			for (int i = 0; i < MAX_PLAYERS; i++)
			{
				if (i != getID() && getTeam() == GET_PLAYER((PlayerTypes)i).getTeam())
				{
					GET_PLAYER((PlayerTypes)i).setEventOccured(eEvent, kEventTriggered, false);
				}
			}
		}
	}
}


const EventTriggeredData* CvPlayer::getEventCountdown(EventTypes eEvent) const
{
	FAssert(eEvent >= 0 && eEvent < GC.getNumEventInfos());

	CvEventMap::const_iterator it = m_mapEventCountdown.find(eEvent);

	if (it == m_mapEventCountdown.end())
	{
		return NULL;
	}

	return &((*it).second);
}

void CvPlayer::setEventCountdown(EventTypes eEvent, const EventTriggeredData& kEventTriggered)
{
	FAssert(eEvent >= 0 && eEvent < GC.getNumEventInfos());

	m_mapEventCountdown[eEvent] = kEventTriggered;
}

void CvPlayer::resetEventCountdown(EventTypes eEvent)
{
	FAssert(eEvent >= 0 && eEvent < GC.getNumEventInfos());

	CvEventMap::iterator it = m_mapEventCountdown.find(eEvent);

	if (it != m_mapEventCountdown.end())
	{
		m_mapEventCountdown.erase(it);
	}
}


void CvPlayer::resetTriggerFired(EventTriggerTypes eTrigger)
{
	std::vector<EventTriggerTypes>::iterator it = std::find(m_triggersFired.begin(), m_triggersFired.end(), eTrigger);

	if (it != m_triggersFired.end())
	{
		m_triggersFired.erase(it);
	}
}

void CvPlayer::setTriggerFired(const EventTriggeredData& kTriggeredData, bool bOthers, bool bAnnounce)
{
	FAssert(kTriggeredData.m_eTrigger >= 0 && kTriggeredData.m_eTrigger < GC.getNumEventTriggerInfos());

	CvEventTriggerInfo& kTrigger = GC.getEventTriggerInfo(kTriggeredData.m_eTrigger);

	if (!isTriggerFired(kTriggeredData.m_eTrigger))
	{
		m_triggersFired.push_back(kTriggeredData.m_eTrigger);

		if (bOthers)
		{
			if (kTrigger.isGlobal())
			{
				for (int i = 0; i < MAX_PLAYERS; i++)
				{
					if (i != getID())
					{
						GET_PLAYER((PlayerTypes)i).setTriggerFired(kTriggeredData, false, false);
					}
				}
			}
			else if (kTrigger.isTeam())
			{
				for (int i = 0; i < MAX_PLAYERS; i++)
				{
					if (i != getID() && getTeam() == GET_PLAYER((PlayerTypes)i).getTeam())
					{
						GET_PLAYER((PlayerTypes)i).setTriggerFired(kTriggeredData, false, false);
					}
				}
			}
		}
	}

	if (!isEmpty(kTrigger.getPythonCallback()))
	{
		long lResult;

		CyArgsList argsList;
		argsList.add(gDLL->getPythonIFace()->makePythonObject(&kTriggeredData));

		gDLL->getPythonIFace()->callFunction(PYRandomEventModule, kTrigger.getPythonCallback(), argsList.makeFunctionArgs(), &lResult);
	}

	if (bAnnounce && !kTrigger.isTutorial())
	{
		CvPlot* pPlot = GC.getMapINLINE().plot(kTriggeredData.m_iPlotX, kTriggeredData.m_iPlotY);

		if (!kTriggeredData.m_szGlobalText.empty())
		{
			for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
			{
				CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes)iPlayer);

				if (kLoopPlayer.isAlive())
				{
					if (GET_TEAM(kLoopPlayer.getTeam()).isHasMet(getTeam()) && (NO_PLAYER == kTriggeredData.m_eOtherPlayer || GET_TEAM(GET_PLAYER(kTriggeredData.m_eOtherPlayer).getTeam()).isHasMet(getTeam())))
					{
						bool bShowPlot = kTrigger.isShowPlot();

						if (bShowPlot)
						{
							if (kLoopPlayer.getTeam() != getTeam())
							{
								if (NULL == pPlot || !pPlot->isRevealed(kLoopPlayer.getTeam(), false))
								{
									bShowPlot = false;
								}
							}
						}

						if (bShowPlot)
						{
							gDLL->getInterfaceIFace()->addMessage((PlayerTypes)iPlayer, false, GC.getEVENT_MESSAGE_TIME(), kTriggeredData.m_szGlobalText, "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MINOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"), kTriggeredData.m_iPlotX, kTriggeredData.m_iPlotY, true, true);
						}
						else
						{
							gDLL->getInterfaceIFace()->addMessage((PlayerTypes)iPlayer, false, GC.getEVENT_MESSAGE_TIME(), kTriggeredData.m_szGlobalText, "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MINOR_EVENT);
						}
					}
				}
			}

			GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getID(), kTriggeredData.m_szGlobalText, kTriggeredData.m_iPlotX, kTriggeredData.m_iPlotY, (ColorTypes)GC.getInfoTypeForString("COLOR_HIGHLIGHT_TEXT"));
		}
		else if (!kTriggeredData.m_szText.empty())
		{
			if (kTrigger.isShowPlot() && NULL != pPlot && pPlot->isRevealed(getTeam(), false))
			{
				gDLL->getInterfaceIFace()->addMessage(getID(), false, GC.getEVENT_MESSAGE_TIME(), kTriggeredData.m_szText, "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MINOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"), kTriggeredData.m_iPlotX, kTriggeredData.m_iPlotY, true, true);
			}
			else
			{
				gDLL->getInterfaceIFace()->addMessage(getID(), false, GC.getEVENT_MESSAGE_TIME(), kTriggeredData.m_szText, "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MINOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"));
			}
		}
	}

	if (kTrigger.isTutorial())
	{
		gDLL->getInterfaceIFace()->addTutorialMessage(getID(), kTriggeredData.m_szText);
	}
}

EventTriggeredData* CvPlayer::initTriggeredData(EventTriggerTypes eEventTrigger, bool bFire, int iCityId, int iPlotX, int iPlotY, PlayerTypes eOtherPlayer, int iOtherPlayerCityId, int iUnitId, BuildingTypes eBuilding)
{

	CvEventTriggerInfo& kTrigger = GC.getEventTriggerInfo(eEventTrigger);

	CvCity* pCity = getCity(iCityId);
	CvCity* pOtherPlayerCity = NULL;
	if (NO_PLAYER != eOtherPlayer)
	{
		pOtherPlayerCity = GET_PLAYER(eOtherPlayer).getCity(iOtherPlayerCityId);
	}
	CvPlot* pPlot = GC.getMapINLINE().plot(iPlotX, iPlotY);
	CvUnit* pUnit = getUnit(iUnitId);

	std::vector<CvPlot*> apPlots;
	bool bPickPlot = ::isPlotEventTrigger(eEventTrigger);
	if (kTrigger.isPickCity())
	{
		if (NULL == pCity)
		{
			pCity = pickTriggerCity(eEventTrigger);
		}

		if (NULL != pCity)
		{
			if (bPickPlot)
			{
				for (int iPlot = 0; iPlot < NUM_CITY_PLOTS; ++iPlot)
				{
					if (CITY_HOME_PLOT != iPlot)
					{
						CvPlot* pLoopPlot = pCity->getCityIndexPlot(iPlot);

						if (NULL != pLoopPlot)
						{
							if (pLoopPlot->canTrigger(eEventTrigger, getID()))
							{
								apPlots.push_back(pLoopPlot);
							}
						}
					}
				}
			}
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		if (kTrigger.getNumBuildings() > 0 && kTrigger.getNumBuildingsRequired() > 0)
		{
			int iFoundValid = 0;

			for (int i = 0; i < kTrigger.getNumBuildingsRequired(); ++i)
			{
				if (kTrigger.getBuildingRequired(i) != NO_BUILDINGCLASS)
				{
					iFoundValid += getBuildingClassCount((BuildingClassTypes)kTrigger.getBuildingRequired(i));
				}
			}

			if (iFoundValid < kTrigger.getNumBuildings())
			{
				return NULL;
			}
		}

		if (kTrigger.getMinPopulation() > 0)
		{
			if (getTotalPopulation() < kTrigger.getMinPopulation())
			{
				return NULL;
			}
		}

		if (kTrigger.getMaxPopulation() > 0)
		{
			if (getTotalPopulation() > kTrigger.getMaxPopulation())
			{
				return NULL;
			}
		}

		if (bPickPlot)
		{
			for (int iPlot = 0; iPlot < GC.getMapINLINE().numPlotsINLINE(); ++iPlot)
			{
				CvPlot* pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iPlot);

				if (pLoopPlot->canTrigger(eEventTrigger, getID()))
				{
					apPlots.push_back(pLoopPlot);
				}
			}
		}
	}

	if (NULL == pPlot)
	{
		if (apPlots.size() > 0)
		{
			int iChosen = GC.getGameINLINE().getSorenRandNum(apPlots.size(), "Event pick plot");
			pPlot = apPlots[iChosen];

			if (NULL == pCity)
			{
				pCity = GC.getMapINLINE().findCity(pPlot->getX_INLINE(), pPlot->getY_INLINE(), getID(), NO_TEAM, false);
			}
		}
		else
		{
			if (bPickPlot)
			{
				return NULL;
			}

			if (NULL != pCity)
			{
				pPlot = pCity->plot();
			}
		}
	}

	if (kTrigger.getNumBuildings() > 0)
	{
		if (NULL != pCity && NO_BUILDING == eBuilding)
		{
			std::vector<BuildingTypes> aeBuildings;
			for (int i = 0; i < kTrigger.getNumBuildingsRequired(); ++i)
			{
				if (kTrigger.getBuildingRequired(i) != NO_BUILDINGCLASS)
				{
					BuildingTypes eTestBuilding = (BuildingTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationBuildings(kTrigger.getBuildingRequired(i));
					if (NO_BUILDING != eTestBuilding && pCity->isHasRealBuilding(eTestBuilding))
					{
						aeBuildings.push_back(eTestBuilding);
					}
				}
			}

			if (aeBuildings.size() > 0)
			{
				int iChosen = GC.getGameINLINE().getSorenRandNum(aeBuildings.size(), "Event pick building");
				eBuilding = aeBuildings[iChosen];
			}
			else
			{
				return NULL;
			}
		}
	}

	if (NULL == pUnit)
	{
		pUnit = pickTriggerUnit(eEventTrigger, pPlot, bPickPlot);
	}

	if (NULL == pUnit && kTrigger.getNumUnits() > 0)
	{
		return NULL;
	}

	if (NULL == pPlot && NULL != pUnit)
	{
		pPlot = pUnit->plot();
	}

	if (NULL == pPlot && bPickPlot)
	{
		return NULL;
	}

	if (kTrigger.getNumUnitsGlobal() > 0)
	{
		int iNumUnits = 0;
		for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes)iPlayer);

			if (kLoopPlayer.isAlive())
			{
				int iLoop;
				for (CvUnit* pLoopUnit = kLoopPlayer.firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = kLoopPlayer.nextUnit(&iLoop))
				{
					if (MIN_INT != pLoopUnit->getTriggerValue(eEventTrigger, pPlot, true))
					{
						++iNumUnits;
					}
				}
			}
		}

		if (iNumUnits < kTrigger.getNumUnitsGlobal())
		{
			return NULL;
		}
	}

	if (kTrigger.getNumBuildingsGlobal() > 0)
	{
		int iNumBuildings = 0;
		for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes)iPlayer);

			if (kLoopPlayer.isAlive())
			{
				for (int i = 0; i < kTrigger.getNumBuildingsRequired(); ++i)
				{
					if (kTrigger.getBuildingRequired(i) != NO_BUILDINGCLASS)
					{
						iNumBuildings += getBuildingClassCount((BuildingClassTypes)kTrigger.getBuildingRequired(i));
					}
				}
			}
		}

		if (iNumBuildings < kTrigger.getNumBuildingsGlobal())
		{
			return NULL;
		}
	}

	if (kTrigger.isPickPlayer())
	{
		std::vector<PlayerTypes> aePlayers;
		std::vector<CvCity*> apCities;

		if (NO_PLAYER == eOtherPlayer)
		{
			for (int i = 0; i < MAX_PLAYERS; i++)
			{
				if (GET_PLAYER((PlayerTypes)i).canTrigger(eEventTrigger, getID()))
				{
					if (kTrigger.isPickOtherPlayerCity())
					{
						CvCity* pBestCity = NULL;

						if (NULL != pCity)
						{
							pBestCity = GC.getMapINLINE().findCity(pCity->getX_INLINE(), pCity->getY_INLINE(), (PlayerTypes)i);
						}
						else
						{
							pBestCity = GET_PLAYER((PlayerTypes)i).pickTriggerCity(eEventTrigger);
						}

						if (NULL != pBestCity)
						{
							apCities.push_back(pBestCity);
							aePlayers.push_back((PlayerTypes)i);
						}
					}
					else
					{
						apCities.push_back(NULL);
						aePlayers.push_back((PlayerTypes)i);
					}
				}
			}

			if (aePlayers.size() > 0)
			{
				int iChosen = GC.getGameINLINE().getSorenRandNum(aePlayers.size(), "Event pick player");
				eOtherPlayer = aePlayers[iChosen];
				pOtherPlayerCity = apCities[iChosen];
			}
			else
			{
				return NULL;
			}
		}
	}

	EventTriggeredData* pTriggerData = addEventTriggered();

	if (NULL != pTriggerData)
	{
		pTriggerData->m_eTrigger = eEventTrigger;
		pTriggerData->m_ePlayer = getID();
		pTriggerData->m_iTurn = GC.getGameINLINE().getGameTurn();
		pTriggerData->m_iCityId = (NULL != pCity) ? pCity->getID() : -1;
		pTriggerData->m_iPlotX = (NULL != pPlot) ? pPlot->getX_INLINE() : INVALID_PLOT_COORD;
		pTriggerData->m_iPlotY = (NULL != pPlot) ? pPlot->getY_INLINE() : INVALID_PLOT_COORD;
		pTriggerData->m_eOtherPlayer = eOtherPlayer;
		pTriggerData->m_iOtherPlayerCityId = (NULL != pOtherPlayerCity) ? pOtherPlayerCity->getID() : -1;
		pTriggerData->m_iUnitId = (NULL != pUnit) ? pUnit->getID() : -1;
		pTriggerData->m_eBuilding = eBuilding;
	}
	else
	{
		return NULL;
	}

	if (!isEmpty(kTrigger.getPythonCanDo()))
	{
		long lResult;

		CyArgsList argsList;
		argsList.add(gDLL->getPythonIFace()->makePythonObject(pTriggerData));

		gDLL->getPythonIFace()->callFunction(PYRandomEventModule, kTrigger.getPythonCanDo(), argsList.makeFunctionArgs(), &lResult);

		if (0 == lResult)
		{
			deleteEventTriggered(pTriggerData->getID());
			return NULL;
		}

		// python may change pTriggerData
		pCity = getCity(pTriggerData->m_iCityId);
		pPlot = GC.getMapINLINE().plot(pTriggerData->m_iPlotX, pTriggerData->m_iPlotY);
		pUnit = getUnit(pTriggerData->m_iUnitId);
		eOtherPlayer = pTriggerData->m_eOtherPlayer;
		if (NO_PLAYER != eOtherPlayer)
		{
			pOtherPlayerCity = GET_PLAYER(eOtherPlayer).getCity(pTriggerData->m_iOtherPlayerCityId);
		}
		eBuilding = pTriggerData->m_eBuilding;
	}

	std::vector<CvWString> aszTexts;
	for (int i = 0; i < kTrigger.getNumTexts(); ++i)
	{
		if (NO_ERA == kTrigger.getTextEra(i) || kTrigger.getTextEra(i) == getCurrentEra())
		{
			aszTexts.push_back(kTrigger.getText(i));
		}
	}

	if (aszTexts.size() > 0)
	{
		int iText = GC.getGameINLINE().getSorenRandNum(aszTexts.size(), "Event Text choice");
		pTriggerData->m_szText = gDLL->getText(aszTexts[iText].GetCString(),
			eOtherPlayer != NO_PLAYER ? GET_PLAYER(eOtherPlayer).getCivilizationAdjectiveKey() : L"",
			NULL != pCity ? pCity->getNameKey() : L"",
			NULL != pUnit ? pUnit->getNameOrProfessionKey() : L"",
			L"",
			NO_BUILDING != eBuilding ? GC.getBuildingInfo(eBuilding).getTextKeyWide() : L"",
			NULL != pOtherPlayerCity ? pOtherPlayerCity->getNameKey() : L"",
			NULL != pPlot && NO_TERRAIN != pPlot->getTerrainType() ? GC.getTerrainInfo(pPlot->getTerrainType()).getTextKeyWide() : L"",
			NULL != pPlot && NO_IMPROVEMENT != pPlot->getImprovementType() ? GC.getImprovementInfo(pPlot->getImprovementType()).getTextKeyWide() : L"",
			NULL != pPlot && NO_BONUS != pPlot->getBonusType() ? GC.getBonusInfo(pPlot->getBonusType()).getTextKeyWide() : L"",
			NULL != pPlot && NO_ROUTE != pPlot->getRouteType() ? GC.getRouteInfo(pPlot->getRouteType()).getTextKeyWide() : L""
			);

	}
	else
	{
		pTriggerData->m_szText = L"";
	}

	if (kTrigger.getNumWorldNews() > 0)
	{
		int iText = GC.getGameINLINE().getSorenRandNum(kTrigger.getNumWorldNews(), "Trigger World News choice");

		pTriggerData->m_szGlobalText = gDLL->getText(kTrigger.getWorldNews(iText).GetCString(),
			getCivilizationAdjectiveKey(),
			NULL != pCity ? pCity->getNameKey() : L"",
			L"",
			eOtherPlayer != NO_PLAYER ? GET_PLAYER(eOtherPlayer).getCivilizationAdjectiveKey() : L"",
			NULL != pOtherPlayerCity ? pOtherPlayerCity->getNameKey() : L""
			);
	}
	else
	{
		pTriggerData->m_szGlobalText.clear();
	}

	if (bFire)
	{
		trigger(*pTriggerData);
	}

	return pTriggerData;
}


bool CvPlayer::canDoEvent(EventTypes eEvent, const EventTriggeredData& kTriggeredData) const
{
	if (eEvent == NO_EVENT)
	{
		FAssert(false);
		return false;
	}

	CvEventInfo& kEvent = GC.getEventInfo(eEvent);

	int iGold = std::min(getEventCost(eEvent, kTriggeredData.m_eOtherPlayer, false), getEventCost(eEvent, kTriggeredData.m_eOtherPlayer, true));

	if (iGold != 0)
	{
		if (iGold > 0 && NO_PLAYER != kTriggeredData.m_eOtherPlayer && kEvent.isGoldToPlayer())
		{
			if (GET_PLAYER(kTriggeredData.m_eOtherPlayer).getGold() < iGold)
			{
				return false;
			}
		}
		else if (iGold < 0)
		{
			if (getGold() < -iGold)
			{
				return false;
			}
		}
	}

	if (kEvent.getUnitClass() != NO_UNITCLASS)
	{
		UnitTypes eUnit = (UnitTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(kEvent.getUnitClass());
		if (eUnit == NO_UNIT)
		{
			return false;
		}
	}

	if (kEvent.isCityEffect())
	{
		CvCity* pCity =	getCity(kTriggeredData.m_iCityId);
		if (NULL == pCity || !pCity->canApplyEvent(eEvent, kTriggeredData))
		{
			return false;
		}
	}
	else if (kEvent.isOtherPlayerCityEffect())
	{
		if (NO_PLAYER == kTriggeredData.m_eOtherPlayer)
		{
			return false;
		}

		CvCity* pCity = GET_PLAYER(kTriggeredData.m_eOtherPlayer).getCity(kTriggeredData.m_iOtherPlayerCityId);
		if (NULL == pCity || !pCity->canApplyEvent(eEvent, kTriggeredData))
		{
			return false;
		}
	}

	if (::isPlotEventTrigger(kTriggeredData.m_eTrigger))
	{
		CvPlot* pPlot = GC.getMapINLINE().plotINLINE(kTriggeredData.m_iPlotX, kTriggeredData.m_iPlotY);
		if (NULL != pPlot)
		{
			if (!pPlot->canApplyEvent(eEvent))
			{
				return false;
			}
		}
	}

	CvUnit* pUnit = getUnit(kTriggeredData.m_iUnitId);
	if (NULL != pUnit)
	{
		if (!pUnit->canApplyEvent(eEvent))
		{
			return false;
		}
	}

	if (NO_BONUS != kEvent.getBonusRevealed())
	{
		if (GET_TEAM(getTeam()).isForceRevealedBonus((BonusTypes)kEvent.getBonusRevealed()))
		{
			return false;
		}
	}

	if (0 != kEvent.getAttitudeModifier())
	{
		if (NO_PLAYER == kTriggeredData.m_eOtherPlayer)
		{
			return false;
		}

		if (GET_PLAYER(kTriggeredData.m_eOtherPlayer).getTeam() == getTeam())
		{
			return false;
		}

		if (GET_PLAYER(kTriggeredData.m_eOtherPlayer).isHuman())
		{
			if (0 == kEvent.getOurAttitudeModifier())
			{
				return false;
			}
		}
	}

	if (0 != kEvent.getTheirEnemyAttitudeModifier())
	{
		if (NO_PLAYER == kTriggeredData.m_eOtherPlayer)
		{
			return false;
		}

		TeamTypes eWorstEnemy = GET_TEAM(GET_PLAYER(kTriggeredData.m_eOtherPlayer).getTeam()).AI_getWorstEnemy();
		if (NO_TEAM == eWorstEnemy || eWorstEnemy == getTeam())
		{
			return false;
		}

		if (!GET_TEAM(eWorstEnemy).isAlive())
		{
			return false;
		}

		if (eWorstEnemy == getTeam())
		{
			return false;
		}
	}

	if (kEvent.isDeclareWar())
	{
		if (NO_PLAYER == kTriggeredData.m_eOtherPlayer)
		{
			return false;
		}

		if (!GET_TEAM(GET_PLAYER(kTriggeredData.m_eOtherPlayer).getTeam()).canDeclareWar(getTeam()) || !GET_TEAM(getTeam()).canDeclareWar(GET_PLAYER(kTriggeredData.m_eOtherPlayer).getTeam()))
		{
			return false;
		}
	}

	if (kEvent.isQuest())
	{
		for (int iTrigger = 0; iTrigger < GC.getNumEventTriggerInfos(); ++iTrigger)
		{
			CvEventTriggerInfo& kTrigger = GC.getEventTriggerInfo((EventTriggerTypes)iTrigger);
			if (!kTrigger.isRecurring())
			{
				for (int i = 0; i < kTrigger.getNumPrereqEvents(); ++i)
				{
					if (kTrigger.getPrereqEvent(i) == eEvent)
					{
						if (isTriggerFired((EventTriggerTypes)iTrigger))
						{
							return false;
						}
					}
				}
			}
		}
	}

	if (!isEmpty(kEvent.getPythonCanDo()))
	{
		long lResult;

		CyArgsList argsList;
		argsList.add(eEvent);
		argsList.add(gDLL->getPythonIFace()->makePythonObject(&kTriggeredData));

		gDLL->getPythonIFace()->callFunction(PYRandomEventModule, kEvent.getPythonCanDo(), argsList.makeFunctionArgs(), &lResult);

		if (0 == lResult)
		{
			return false;
		}
	}

	return true;
}


void CvPlayer::applyEvent(EventTypes eEvent, int iEventTriggeredId, bool bUpdateTrigger)
{
	FAssert(eEvent != NO_EVENT);

	EventTriggeredData* pTriggeredData = getEventTriggered(iEventTriggeredId);

	if (NULL == pTriggeredData)
	{
		deleteEventTriggered(iEventTriggeredId);
		return;
	}

	if (bUpdateTrigger)
	{
		setTriggerFired(*pTriggeredData, true);
	}

	if (!canDoEvent(eEvent, *pTriggeredData))
	{
		if (bUpdateTrigger)
		{
			deleteEventTriggered(iEventTriggeredId);
		}
		return;
	}

	setEventOccured(eEvent, *pTriggeredData);

	CvEventInfo& kEvent = GC.getEventInfo(eEvent);
	CvCity* pCity =	getCity(pTriggeredData->m_iCityId);
	CvCity* pOtherPlayerCity = NULL;

	if (NO_PLAYER != pTriggeredData->m_eOtherPlayer)
	{
		pOtherPlayerCity = GET_PLAYER(pTriggeredData->m_eOtherPlayer).getCity(pTriggeredData->m_iOtherPlayerCityId);
	}

	int iGold = getEventCost(eEvent, pTriggeredData->m_eOtherPlayer, false);
	int iRandomGold = getEventCost(eEvent, pTriggeredData->m_eOtherPlayer, true);

	iGold += GC.getGameINLINE().getSorenRandNum(iRandomGold - iGold + 1, "Event random gold");

	if (iGold != 0)
	{
		changeGold(iGold);

		if (NO_PLAYER != pTriggeredData->m_eOtherPlayer && kEvent.isGoldToPlayer())
		{
			GET_PLAYER(pTriggeredData->m_eOtherPlayer).changeGold(-iGold);
		}
	}

	if (kEvent.isDeclareWar())
	{
		if (NO_PLAYER != pTriggeredData->m_eOtherPlayer)
		{
			GET_TEAM(GET_PLAYER(pTriggeredData->m_eOtherPlayer).getTeam()).declareWar(getTeam(), false, WARPLAN_LIMITED);
		}
	}

	bool bClear = false;
	for (int iEvent = 0; iEvent < GC.getNumEventInfos(); ++iEvent)
	{
		if (kEvent.getClearEventChance(iEvent) > 0)
		{
			bClear = GC.getGameINLINE().getSorenRandNum(100, "Event Clear") < kEvent.getClearEventChance(iEvent);
			if (bClear)
			{
				if (kEvent.isGlobal())
				{
					for (int j = 0; j < MAX_PLAYERS; j++)
					{
						GET_PLAYER((PlayerTypes)j).resetEventOccured((EventTypes)iEvent, j != getID());
					}
				}
				else if (kEvent.isTeam())
				{
					for (int j = 0; j < MAX_PLAYERS; j++)
					{
						if (getTeam() == GET_PLAYER((PlayerTypes)j).getTeam())
						{
							GET_PLAYER((PlayerTypes)j).resetEventOccured((EventTypes)iEvent, j != getID());
						}
					}
				}
				else
				{
					resetEventOccured((EventTypes)iEvent, false);
				}
			}
		}
	}

	if (NULL != pCity && kEvent.isCityEffect())
	{
		pCity->applyEvent(eEvent, *pTriggeredData, bClear);
	}
	else if (NULL != pOtherPlayerCity && kEvent.isOtherPlayerCityEffect())
	{
		pOtherPlayerCity->applyEvent(eEvent, *pTriggeredData, bClear);
	}

	if (!kEvent.isCityEffect() && !kEvent.isOtherPlayerCityEffect())
	{
		if (kEvent.getNumBuildingYieldChanges() > 0)
		{
			int iLoop;
			for (int iBuildingClass = 0; iBuildingClass < GC.getNumBuildingClassInfos(); ++iBuildingClass)
			{
				for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
				{
					for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop))
					{
						pLoopCity->changeBuildingYieldChange((BuildingClassTypes)iBuildingClass, (YieldTypes)iYield, kEvent.getBuildingYieldChange(iBuildingClass, iYield));
					}
				}
			}
		}

		if (kEvent.getMaxPillage() > 0)
		{
			FAssert(kEvent.getMaxPillage() >= kEvent.getMinPillage());
			int iNumPillage = kEvent.getMinPillage() + GC.getGameINLINE().getSorenRandNum(kEvent.getMaxPillage() - kEvent.getMinPillage(), "Pick number of event pillaged plots");

			int iNumPillaged = 0;
			for (int i = 0; i < iNumPillage; ++i)
			{
				int iRandOffset = GC.getGameINLINE().getSorenRandNum(GC.getMapINLINE().numPlotsINLINE(), "Pick event pillage plot (any city)");
				for (int j = 0; j < GC.getMapINLINE().numPlotsINLINE(); ++j)
				{
					int iPlot = (j + iRandOffset) % GC.getMapINLINE().numPlotsINLINE();
					CvPlot* pPlot = GC.getMapINLINE().plotByIndexINLINE(iPlot);
					if (NULL != pPlot && pPlot->getOwnerINLINE() == getID() && pPlot->isCity())
					{
						if (NO_IMPROVEMENT != pPlot->getImprovementType() && !GC.getImprovementInfo(pPlot->getImprovementType()).isPermanent())
						{
							CvWString szBuffer = gDLL->getText("TXT_KEY_EVENT_CITY_IMPROVEMENT_DESTROYED", GC.getImprovementInfo(pPlot->getImprovementType()).getTextKeyWide());
							gDLL->getInterfaceIFace()->addMessage(getID(), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_PILLAGED", MESSAGE_TYPE_INFO, GC.getImprovementInfo(pPlot->getImprovementType()).getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE(), true, true);
							pPlot->setImprovementType(NO_IMPROVEMENT);
							++iNumPillaged;
							break;
						}
					}
				}
			}

			if (NO_PLAYER != pTriggeredData->m_eOtherPlayer)
			{
				CvWString szBuffer = gDLL->getText("TXT_KEY_EVENT_NUM_CITY_IMPROVEMENTS_DESTROYED", iNumPillaged, getCivilizationAdjectiveKey());
				gDLL->getInterfaceIFace()->addMessage(pTriggeredData->m_eOtherPlayer, false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_PILLAGED", MESSAGE_TYPE_INFO);
			}
		}

		if (kEvent.getFood() != 0)
		{
			int iLoop;

			for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop))
			{
				pLoopCity->changeFood(kEvent.getFood());
			}
		}

		if (kEvent.getFoodPercent() != 0)
		{
			int iLoop;

			for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop))
			{
				pLoopCity->changeFood((pLoopCity->getFood() * kEvent.getFoodPercent()) / 100);
			}
		}

		if (kEvent.getPopulationChange() != 0)
		{
			int iLoop;

			for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop))
			{
				if (pLoopCity->getPopulation() + kEvent.getPopulationChange() > 0)
				{
					pLoopCity->changePopulation(kEvent.getPopulationChange());
				}
			}
		}

		if (kEvent.getCulture() != 0)
		{
			int iLoop;

			for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop))
			{
				if (pLoopCity->getCulture(pLoopCity->getOwnerINLINE()) + kEvent.getCulture() > 0)
				{
					pLoopCity->changeCulture(pLoopCity->getOwnerINLINE(), kEvent.getCulture(), true);
				}
			}
		}

		if (kEvent.getUnitClass() != NO_UNITCLASS)
		{
			UnitTypes eUnit = (UnitTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(kEvent.getUnitClass());
			if (eUnit != NO_UNIT)
			{
				CvCity* pUnitCity = pCity;

				if (NULL == pUnitCity)
				{
					pUnitCity = getPrimaryCity();
				}

				if (NULL != pUnitCity)
				{
					for (int i = 0; i < kEvent.getNumUnits(); ++i)
					{
						initUnit(eUnit, (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession(), pUnitCity->getX_INLINE(), pUnitCity->getY_INLINE());
					}
				}
			}
		}
	}

	CvPlot* pPlot = GC.getMapINLINE().plotINLINE(pTriggeredData->m_iPlotX, pTriggeredData->m_iPlotY);
	if (NULL != pPlot)
	{
		if (::isPlotEventTrigger(pTriggeredData->m_eTrigger))
		{
			FAssert(pPlot->canApplyEvent(eEvent));
			pPlot->applyEvent(eEvent);
		}
	}

	CvUnit* pUnit = getUnit(pTriggeredData->m_iUnitId);
	if (NULL != pUnit)
	{
		FAssert(pUnit->canApplyEvent(eEvent));
		pUnit->applyEvent(eEvent);   // might kill the unit
	}

	for (int i = 0; i < GC.getNumUnitCombatInfos(); ++i)
	{
		if (NO_PROMOTION != kEvent.getUnitCombatPromotion(i))
		{
			int iLoop;
			for (CvUnit* pLoopUnit = firstUnit(&iLoop); NULL != pLoopUnit; pLoopUnit = nextUnit(&iLoop))
			{
				if (pLoopUnit->getUnitCombatType() == i)
				{
					pLoopUnit->changeFreePromotionCount((PromotionTypes)kEvent.getUnitCombatPromotion(i), 1);
				}
			}

			setFreePromotion((UnitCombatTypes)i, (PromotionTypes)kEvent.getUnitCombatPromotion(i), true);
		}
	}

	for (int i = 0; i < GC.getNumUnitClassInfos(); ++i)
	{
		if (NO_PROMOTION != kEvent.getUnitClassPromotion(i))
		{
			int iLoop;
			for (CvUnit* pLoopUnit = firstUnit(&iLoop); NULL != pLoopUnit; pLoopUnit = nextUnit(&iLoop))
			{
				if (pLoopUnit->getUnitClassType() == i)
				{
					pLoopUnit->changeFreePromotionCount((PromotionTypes)kEvent.getUnitClassPromotion(i), 1);
				}
			}

			setFreePromotion((UnitClassTypes)i, (PromotionTypes)kEvent.getUnitClassPromotion(i), true);
		}
	}

	if (NO_BONUS != kEvent.getBonusRevealed())
	{
		GET_TEAM(getTeam()).setForceRevealedBonus((BonusTypes)kEvent.getBonusRevealed(), true);
	}

	if (0 != kEvent.getOurAttitudeModifier())
	{
		if (NO_PLAYER != pTriggeredData->m_eOtherPlayer)
		{
			if (kEvent.getOurAttitudeModifier() > 0)
			{
				AI_changeMemoryCount(pTriggeredData->m_eOtherPlayer, MEMORY_EVENT_GOOD_TO_US, kEvent.getOurAttitudeModifier());
			}
			else
			{
				AI_changeMemoryCount(pTriggeredData->m_eOtherPlayer, MEMORY_EVENT_BAD_TO_US, -kEvent.getOurAttitudeModifier());
			}
		}
	}

	if (0 != kEvent.getAttitudeModifier())
	{
		if (NO_PLAYER != pTriggeredData->m_eOtherPlayer)
		{
			if (kEvent.getAttitudeModifier() > 0)
			{
				GET_PLAYER(pTriggeredData->m_eOtherPlayer).AI_changeMemoryCount(getID(), MEMORY_EVENT_GOOD_TO_US, kEvent.getAttitudeModifier());
			}
			else
			{
				GET_PLAYER(pTriggeredData->m_eOtherPlayer).AI_changeMemoryCount(getID(), MEMORY_EVENT_BAD_TO_US, -kEvent.getAttitudeModifier());
			}
		}
	}

	if (0 != kEvent.getTheirEnemyAttitudeModifier())
	{
		if (NO_PLAYER != pTriggeredData->m_eOtherPlayer)
		{
			TeamTypes eWorstEnemy = GET_TEAM(GET_PLAYER(pTriggeredData->m_eOtherPlayer).getTeam()).AI_getWorstEnemy();
			if (NO_TEAM != eWorstEnemy)
			{
				for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
				{
					CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes)iPlayer);
					if (kLoopPlayer.isAlive() && kLoopPlayer.getTeam() == eWorstEnemy)
					{
						if (kEvent.getTheirEnemyAttitudeModifier() > 0)
						{
							kLoopPlayer.AI_changeMemoryCount(getID(), MEMORY_EVENT_GOOD_TO_US, kEvent.getTheirEnemyAttitudeModifier());
							AI_changeMemoryCount((PlayerTypes)iPlayer, MEMORY_EVENT_GOOD_TO_US, kEvent.getTheirEnemyAttitudeModifier());
						}
						else
						{
							kLoopPlayer.AI_changeMemoryCount(getID(), MEMORY_EVENT_BAD_TO_US, -kEvent.getTheirEnemyAttitudeModifier());
							AI_changeMemoryCount((PlayerTypes)iPlayer, MEMORY_EVENT_BAD_TO_US, -kEvent.getTheirEnemyAttitudeModifier());
						}
					}
				}
			}
		}
	}

	if (!isEmpty(kEvent.getPythonCallback()))
	{
		long lResult;

		CyArgsList argsList;
		argsList.add(eEvent);
		argsList.add(gDLL->getPythonIFace()->makePythonObject(pTriggeredData));

		gDLL->getPythonIFace()->callFunction(PYRandomEventModule, kEvent.getPythonCallback(), argsList.makeFunctionArgs(), &lResult);
	}

	if (kEvent.getNumWorldNews() > 0)
	{
		int iText = GC.getGameINLINE().getSorenRandNum(kEvent.getNumWorldNews(), "Event World News choice");

		CvWString szGlobalText;

		TeamTypes eTheirWorstEnemy = NO_TEAM;
		if (NO_PLAYER != pTriggeredData->m_eOtherPlayer)
		{
			eTheirWorstEnemy = GET_TEAM(GET_PLAYER(pTriggeredData->m_eOtherPlayer).getTeam()).AI_getWorstEnemy();
		}

		szGlobalText = gDLL->getText(kEvent.getWorldNews(iText).GetCString(),
			getCivilizationAdjectiveKey(),
			NULL != pCity ? pCity->getNameKey() : L"",
			pTriggeredData->m_eOtherPlayer != NO_PLAYER ? GET_PLAYER(pTriggeredData->m_eOtherPlayer).getCivilizationAdjectiveKey() : L"",
			NULL != pOtherPlayerCity ? pOtherPlayerCity->getNameKey() : L"",
			L"",
			NO_TEAM != eTheirWorstEnemy ? GET_TEAM(eTheirWorstEnemy).getName().GetCString() : L""
			);

		for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes)iPlayer);

			if (kLoopPlayer.isAlive())
			{
				if (GET_TEAM(kLoopPlayer.getTeam()).isHasMet(getTeam()) && (NO_PLAYER == pTriggeredData->m_eOtherPlayer || GET_TEAM(GET_PLAYER(pTriggeredData->m_eOtherPlayer).getTeam()).isHasMet(getTeam())))
				{
					bool bShowPlot = GC.getEventTriggerInfo(pTriggeredData->m_eTrigger).isShowPlot();

					if (bShowPlot)
					{
						if (kLoopPlayer.getTeam() != getTeam())
						{
							if (NULL == pPlot || !pPlot->isRevealed(kLoopPlayer.getTeam(), false))
							{
								bShowPlot = false;
							}
						}
					}

					if (bShowPlot)
					{
						gDLL->getInterfaceIFace()->addMessage((PlayerTypes)iPlayer, false, GC.getEVENT_MESSAGE_TIME(), szGlobalText, "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MINOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"), pTriggeredData->m_iPlotX, pTriggeredData->m_iPlotY, true, true);
					}
					else
					{
						gDLL->getInterfaceIFace()->addMessage((PlayerTypes)iPlayer, false, GC.getEVENT_MESSAGE_TIME(), szGlobalText, "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MINOR_EVENT);
					}
				}
			}
		}

		GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getID(), szGlobalText, pTriggeredData->m_iPlotX, pTriggeredData->m_iPlotY, (ColorTypes)GC.getInfoTypeForString("COLOR_HIGHLIGHT_TEXT"));
	}

	if (!isEmpty(kEvent.getLocalInfoTextKey()))
	{
		CvWString szLocalText;

		TeamTypes eTheirWorstEnemy = NO_TEAM;
		if (NO_PLAYER != pTriggeredData->m_eOtherPlayer)
		{
			eTheirWorstEnemy = GET_TEAM(GET_PLAYER(pTriggeredData->m_eOtherPlayer).getTeam()).AI_getWorstEnemy();
		}

		szLocalText = gDLL->getText(kEvent.getLocalInfoTextKey(),
			getCivilizationAdjectiveKey(),
			NULL != pCity ? pCity->getNameKey() : L"",
			pTriggeredData->m_eOtherPlayer != NO_PLAYER ? GET_PLAYER(pTriggeredData->m_eOtherPlayer).getCivilizationAdjectiveKey() : L"",
			NULL != pOtherPlayerCity ? pOtherPlayerCity->getNameKey() : L"",
			L"",
			NO_TEAM != eTheirWorstEnemy ? GET_TEAM(eTheirWorstEnemy).getName().GetCString() : L""
			);

		if (GC.getEventTriggerInfo(pTriggeredData->m_eTrigger).isShowPlot())
		{
			gDLL->getInterfaceIFace()->addMessage(getID(), false, GC.getEVENT_MESSAGE_TIME(), szLocalText, "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MINOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"), pTriggeredData->m_iPlotX, pTriggeredData->m_iPlotY, true, true);
		}
		else
		{
			gDLL->getInterfaceIFace()->addMessage(getID(), false, GC.getEVENT_MESSAGE_TIME(), szLocalText, "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MINOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"));
		}

	}

	if (!isEmpty(kEvent.getOtherPlayerPopup()))
	{
		if (NO_PLAYER != pTriggeredData->m_eOtherPlayer)
		{
			CvWString szText = gDLL->getText(kEvent.getOtherPlayerPopup(),
				getCivilizationAdjectiveKey(),
				NULL != pCity ? pCity->getNameKey() : L"",
				pTriggeredData->m_eOtherPlayer != NO_PLAYER ? GET_PLAYER(pTriggeredData->m_eOtherPlayer).getCivilizationAdjectiveKey() : L"",
				NULL != pOtherPlayerCity ? pOtherPlayerCity->getNameKey() : L"",
				L""
				);

			CvPopupInfo* pInfo = new CvPopupInfo();

			if (NULL != pInfo)
			{
				pInfo->setText(szText);

				GET_PLAYER(pTriggeredData->m_eOtherPlayer).addPopup(pInfo);
			}
		}
	}

	bool bDeleteTrigger = bUpdateTrigger;

	for (int iEvent = 0; iEvent < GC.getNumEventInfos(); ++iEvent)
	{
		if (0 == kEvent.getAdditionalEventTime(iEvent))
		{
			if (kEvent.getAdditionalEventChance(iEvent) > 0)
			{
				if (canDoEvent((EventTypes)iEvent, *pTriggeredData))
				{
					if (GC.getGameINLINE().getSorenRandNum(100, "Additional Event") < kEvent.getAdditionalEventChance(iEvent))
					{
						applyEvent((EventTypes)iEvent, iEventTriggeredId, false);
					}
				}
			}
		}
		else
		{
			bool bSetTimer = true;
			if (kEvent.getAdditionalEventChance(iEvent) > 0)
			{
				if (GC.getGameINLINE().getSorenRandNum(100, "Additional Event 2") >= kEvent.getAdditionalEventChance(iEvent))
				{
					bSetTimer = false;
				}
			}

			if (bSetTimer)
			{
				EventTriggeredData kTriggered = *pTriggeredData;
				kTriggered.m_iTurn = (GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent() * kEvent.getAdditionalEventTime((EventTypes)iEvent)) / 100 + GC.getGameINLINE().getGameTurn();

				const EventTriggeredData* pExistingTriggered = getEventCountdown((EventTypes)iEvent);

				if (NULL != pExistingTriggered)
				{
					kTriggered.m_iTurn = std::min(kTriggered.m_iTurn, pExistingTriggered->m_iTurn);
				}

				setEventCountdown((EventTypes)iEvent, kTriggered);
				bDeleteTrigger = false;
			}
		}
	}

	if (bDeleteTrigger)
	{
		deleteEventTriggered(iEventTriggeredId);
	}
}
void CvPlayer::freeEuropeUnits()
{
	for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
	{
		SAFE_DELETE(m_aEuropeUnits[i]);
	}
	m_aEuropeUnits.clear();
}

int CvPlayer::getEventCost(EventTypes eEvent, PlayerTypes eOtherPlayer, bool bRandom) const
{
	CvEventInfo& kEvent = GC.getEventInfo(eEvent);

	int iGold = kEvent.getGold();
	if (bRandom)
	{
		iGold += kEvent.getRandomGold();
	}

	return iGold;
}


void CvPlayer::doEvents()
{
	if (GC.getGameINLINE().isOption(GAMEOPTION_NO_EVENTS))
	{
		return;
	}

	CvEventMap::iterator it = m_mapEventsOccured.begin();
	while (it != m_mapEventsOccured.end())
	{
		if (checkExpireEvent((*it).first, (*it).second))
		{
			expireEvent((*it).first, (*it).second, true);
			it = m_mapEventsOccured.erase(it);
		}
		else
		{
			++it;
		}
	}

	bool bNewEventEligible = true;
	if (GC.getGameINLINE().getElapsedGameTurns() < GC.getDefineINT("FIRST_EVENT_DELAY_TURNS"))
	{
		bNewEventEligible = false;
	}

	if (bNewEventEligible)
	{
		if (GC.getGameINLINE().getSorenRandNum(GC.getDefineINT("EVENT_PROBABILITY_ROLL_SIDES"), "Global event check") >= GC.getEraInfo(getCurrentEra()).getEventChancePerTurn())
		{
			bNewEventEligible = false;
		}
	}

	std::vector< std::pair<EventTriggeredData*, int> > aePossibleEventTriggerWeights;
	int iTotalWeight = 0;
	for (int i = 0; i < GC.getNumEventTriggerInfos(); ++i)
	{
		int iWeight = getEventTriggerWeight((EventTriggerTypes)i);
		if (iWeight < 0)
		{
			trigger((EventTriggerTypes)i);
		}
		else if (iWeight > 0 && bNewEventEligible)
		{
			EventTriggeredData* pTriggerData = initTriggeredData((EventTriggerTypes)i);
			if (NULL != pTriggerData)
			{
				iTotalWeight += iWeight;
				aePossibleEventTriggerWeights.push_back(std::make_pair(pTriggerData, iTotalWeight));
			}
		}
	}

	if (iTotalWeight > 0)
	{
		bool bFired = false;
		int iValue = GC.getGameINLINE().getSorenRandNum(iTotalWeight, "Event trigger");
		for (std::vector< std::pair<EventTriggeredData*, int> >::iterator it = aePossibleEventTriggerWeights.begin(); it != aePossibleEventTriggerWeights.end(); ++it)
		{
			EventTriggeredData* pTriggerData = (*it).first;
			if (NULL != pTriggerData)
			{
				if (iValue < (*it).second && !bFired)
				{
					trigger(*pTriggerData);
					bFired = true;
				}
				else
				{
					deleteEventTriggered(pTriggerData->getID());
				}
			}
		}
	}

	std::vector<int> aCleanup;
	for (int i = 0; i < GC.getNumEventInfos(); ++i)
	{
		const EventTriggeredData* pTriggeredData = getEventCountdown((EventTypes)i);
		if (NULL != pTriggeredData)
		{
			if (GC.getGameINLINE().getGameTurn() >= pTriggeredData->m_iTurn)
			{
				applyEvent((EventTypes)i, pTriggeredData->m_iId);
				resetEventCountdown((EventTypes)i);
				aCleanup.push_back(pTriggeredData->m_iId);
			}
		}
	}

	for (std::vector<int>::iterator it = aCleanup.begin(); it != aCleanup.end(); ++it)
	{
		bool bDelete = true;

		for (int i = 0; i < GC.getNumEventInfos(); ++i)
		{
			const EventTriggeredData* pTriggeredData = getEventCountdown((EventTypes)i);
			if (NULL != pTriggeredData)
			{
				if (pTriggeredData->m_iId == *it)
				{
					bDelete = false;
					break;
				}
			}
		}

		if (bDelete)
		{
			deleteEventTriggered(*it);
		}
	}
}


void CvPlayer::expireEvent(EventTypes eEvent, const EventTriggeredData& kTriggeredData, bool bFail)
{
	FAssert(getEventOccured(eEvent) == &kTriggeredData);
	FAssert(GC.getEventInfo(eEvent).isQuest() || GC.getGameINLINE().getGameTurn() - kTriggeredData.m_iTurn <= 4);

	if (GC.getEventInfo(eEvent).isQuest())
	{
		CvMessageQueue::iterator it;
		for (it = m_listGameMessages.begin(); it != m_listGameMessages.end(); ++it)
		{
			CvTalkingHeadMessage& message = *it;

			// the trigger ID is stored in the otherwise unused length field
			if (message.getLength() == kTriggeredData.getID())
			{
				m_listGameMessages.erase(it);
				gDLL->getInterfaceIFace()->dirtyTurnLog(getID());
				break;
			}
		}

		if (bFail)
		{
			gDLL->getInterfaceIFace()->addMessage(getID(), false, GC.getEVENT_MESSAGE_TIME(), gDLL->getText(GC.getEventInfo(eEvent).getQuestFailTextKey()), "AS2D_CIVIC_ADOPT", MESSAGE_TYPE_MINOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"));
		}
	}
}

bool CvPlayer::checkExpireEvent(EventTypes eEvent, const EventTriggeredData& kTriggeredData) const
{
	CvEventInfo& kEvent = GC.getEventInfo(eEvent);

	if (!isEmpty(kEvent.getPythonExpireCheck()))
	{
		long lResult;

		CyArgsList argsList;
		argsList.add(eEvent);
		argsList.add(gDLL->getPythonIFace()->makePythonObject(&kTriggeredData));

		gDLL->getPythonIFace()->callFunction(PYRandomEventModule, kEvent.getPythonExpireCheck(), argsList.makeFunctionArgs(), &lResult);

		if (0 != lResult)
		{
			return true;
		}
	}

	if (!kEvent.isQuest())
	{
		if (GC.getGameINLINE().getGameTurn() - kTriggeredData.m_iTurn > 2)
		{
			return true;
		}

		return false;
	}

	CvEventTriggerInfo& kTrigger = GC.getEventTriggerInfo(kTriggeredData.m_eTrigger);

	FAssert(kTriggeredData.m_ePlayer != NO_PLAYER);

	CvPlayer& kPlayer = GET_PLAYER(kTriggeredData.m_ePlayer);

	if (NO_CIVIC != kTrigger.getCivic())
	{
		if (!kPlayer.isCivic((CivicTypes)kTrigger.getCivic()))
		{
			return true;
		}
	}

	if (kTriggeredData.m_iCityId != -1)
	{
		if (NULL == kPlayer.getCity(kTriggeredData.m_iCityId))
		{
			return true;
		}
	}

	if (kTriggeredData.m_iUnitId != -1)
	{
		if (NULL == kPlayer.getUnit(kTriggeredData.m_iUnitId))
		{
			return true;
		}
	}

	if (NO_PLAYER != kTriggeredData.m_eOtherPlayer)
	{
		if (!GET_PLAYER(kTriggeredData.m_eOtherPlayer).isAlive())
		{
			return true;
		}

		if (kTriggeredData.m_iOtherPlayerCityId != -1)
		{
			if (NULL == GET_PLAYER(kTriggeredData.m_eOtherPlayer).getCity(kTriggeredData.m_iOtherPlayerCityId))
			{
				return true;
			}
		}
	}
	return false;
}


void CvPlayer::trigger(EventTriggerTypes eTrigger)
{
	initTriggeredData(eTrigger, true);
}

void CvPlayer::trigger(const EventTriggeredData& kData)
{
	if (isHuman())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_EVENT, kData.getID());
		addPopup(pInfo, GC.getEventTriggerInfo(kData.m_eTrigger).isFrontPopup());
	}
	else
	{
		EventTypes eEvent = AI_chooseEvent(kData.getID());
		if (NO_EVENT != eEvent)
		{
			applyEvent(eEvent, kData.getID());
		}
	}
}

bool CvPlayer::canTrigger(EventTriggerTypes eTrigger, PlayerTypes ePlayer) const
{
	if (!isAlive())
	{
		return false;
	}

	if (getID() == ePlayer)
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvEventTriggerInfo& kTrigger = GC.getEventTriggerInfo(eTrigger);

	if (getTeam() == kPlayer.getTeam())
	{
		return false;
	}

	if (!kTrigger.isPickPlayer())
	{
		return false;
	}

	if (!GET_TEAM(getTeam()).isHasMet(kPlayer.getTeam()))
	{
		return false;
	}

	if (isHuman() && kTrigger.isOtherPlayerAI())
	{
		return false;
	}

	if (!isNative() && kTrigger.isOtherPlayerNative())
	{
		return false;
	}

	if (!isPlayable() && kTrigger.isOtherPlayerPlayable())
	{
		return false;
	}

	if (GET_TEAM(getTeam()).isAtWar(kPlayer.getTeam()) != kTrigger.isOtherPlayerWar())
	{
		return false;
	}

	if (kTrigger.getOtherPlayerShareBorders() > 0)
	{
		int iCount = 0;

		for (int iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); ++iI)
		{
			CvPlot* pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iI);

			if (!pLoopPlot->isWater())
			{
				if ((pLoopPlot->getOwnerINLINE() == getID()) && pLoopPlot->isAdjacentPlayer(ePlayer, true))
				{
					++iCount;
				}
			}
		}

		if (iCount < kTrigger.getOtherPlayerShareBorders())
		{
			return false;
		}
	}

	return true;
}

CvCity* CvPlayer::pickTriggerCity(EventTriggerTypes eTrigger) const
{
	CvCity* pCity = NULL;
	std::vector<CvCity*> apCities;
	int iLoop;
	int iBestValue = MIN_INT;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		int iValue = pLoopCity->getTriggerValue(eTrigger);
		if (iValue > iBestValue)
		{
			if (NULL != pCity)
			{
				apCities.push_back(pCity);
			}

			iBestValue = iValue;
			pCity = pLoopCity;
		}
		else if (MIN_INT != iValue)
		{
			apCities.push_back(pLoopCity);
		}
	}

	if (NULL == pCity && apCities.size() > 0)
	{
		int iChosen = GC.getGameINLINE().getSorenRandNum(apCities.size(), "Event pick city");
		pCity = apCities[iChosen];
	}

	return pCity;
}

CvUnit* CvPlayer::pickTriggerUnit(EventTriggerTypes eTrigger, CvPlot* pPlot, bool bPickPlot) const
{
	CvUnit* pUnit = NULL;
	std::vector<CvUnit*> apUnits;
	int iLoop;
	int iBestValue = MIN_INT;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		int iValue = pLoopUnit->getTriggerValue(eTrigger, pPlot, bPickPlot);

		if (iValue > iBestValue)
		{
			if (NULL != pUnit)
			{
				apUnits.push_back(pUnit);
			}

			iBestValue = iValue;
			pUnit = pLoopUnit;
		}
		else if (MIN_INT != iValue)
		{
			apUnits.push_back(pLoopUnit);
		}
	}

	if (NULL == pUnit && apUnits.size() > 0)
	{
		int iChosen = GC.getGameINLINE().getSorenRandNum(apUnits.size(), "Event pick unit");
		pUnit = apUnits[iChosen];
	}

	return pUnit;
}

int CvPlayer::getEventTriggerWeight(EventTriggerTypes eTrigger) const
{
	CvEventTriggerInfo& kTrigger = GC.getEventTriggerInfo(eTrigger);

	if (NO_HANDICAP != kTrigger.getMinDifficulty())
	{
		if (GC.getGameINLINE().getHandicapType() < kTrigger.getMinDifficulty())
		{
			return 0;
		}
	}

	if (kTrigger.isTutorial())
	{
		if (!isHuman() || !isOption(PLAYEROPTION_TUTORIAL))
		{
			return 0;
		}
	}

	if (kTrigger.isSinglePlayer() && GC.getGameINLINE().isGameMultiPlayer())
	{
		return 0;
	}

	if (!GC.getGameINLINE().isEventActive(eTrigger))
	{
		return 0;
	}
	if (!kTrigger.isRecurring())
	{
		if (isTriggerFired(eTrigger))
		{
			return 0;
		}
	}
	if (kTrigger.getNumPrereqEvents() > 0)
	{
		bool bFoundValid = true;
		for (int iI = 0; iI < kTrigger.getNumPrereqEvents(); iI++)
		{
			if (NULL == getEventOccured((EventTypes)kTrigger.getPrereqEvent(iI)))
			{
				bFoundValid = false;
				break;
			}
		}

		if (!bFoundValid)
		{
			return 0;
		}
	}

	if (NO_CIVIC != kTrigger.getCivic())
	{
		bool bFoundValid = false;

		for (int iI = 0; iI < GC.getNumCivicOptionInfos(); ++iI)
		{
			if (getCivic((CivicOptionTypes)iI) == kTrigger.getCivic())
			{
				bFoundValid = true;
				break;
			}
		}

		if (!bFoundValid)
		{
			return 0;
		}
	}

	if (kTrigger.getMinTreasury() > 0)
	{
		if (getGold() < kTrigger.getMinTreasury())
		{
			return 0;
		}
	}

	if (GC.getMapINLINE().getNumLandAreas() < kTrigger.getMinMapLandmass())
	{
		return 0;
	}

	if (kTrigger.getMinOurLandmass() > 0 || kTrigger.getMaxOurLandmass() != -1)
	{
		int iNumLandmass = 0;

		int iLoop;
		for (CvArea* pArea = GC.getMapINLINE().firstArea(&iLoop); NULL != pArea; pArea = GC.getMapINLINE().nextArea(&iLoop))
		{
			if (!pArea->isWater())
			{
				if (pArea->getCitiesPerPlayer(getID()) > 0)
				{
					++iNumLandmass;
				}
			}
		}

		if (iNumLandmass < kTrigger.getMinOurLandmass())
		{
			return 0;
		}

		if (kTrigger.getMaxOurLandmass() != -1 && iNumLandmass > kTrigger.getMaxOurLandmass())
		{
			return 0;
		}
	}

	if (kTrigger.getProbability() < 0)
	{
		return kTrigger.getProbability();
	}

	int iProbability = kTrigger.getProbability();

	if (kTrigger.isProbabilityUnitMultiply() && kTrigger.getNumUnits() > 0)
	{
		int iNumUnits = 0;
		int iLoop;
		for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
		{
			if (MIN_INT != pLoopUnit->getTriggerValue(eTrigger, NULL, true))
			{
				++iNumUnits;
			}
		}

		iProbability *= iNumUnits;
	}

	if (kTrigger.isProbabilityBuildingMultiply() && kTrigger.getNumBuildings() > 0)
	{
		int iNumBuildings = 0;
		for (int i = 0; i < kTrigger.getNumBuildingsRequired(); ++i)
		{
			if (kTrigger.getBuildingRequired(i) != NO_BUILDINGCLASS)
			{
				iNumBuildings += getBuildingClassCount((BuildingClassTypes)kTrigger.getBuildingRequired(i));
			}
		}

		iProbability *= iNumBuildings;
	}

	return iProbability;
}

bool CvPlayer::isFreePromotion(UnitCombatTypes eUnitCombat, PromotionTypes ePromotion) const
{
	for (UnitCombatPromotionArray::const_iterator it = m_aFreeUnitCombatPromotions.begin(); it != m_aFreeUnitCombatPromotions.end(); ++it)
	{
		if ((*it).first == eUnitCombat && (*it).second == ePromotion)
		{
			return true;
		}
	}

	return false;
}

void CvPlayer::setFreePromotion(UnitCombatTypes eUnitCombat, PromotionTypes ePromotion, bool bFree)
{
	for (UnitCombatPromotionArray::iterator it = m_aFreeUnitCombatPromotions.begin(); it != m_aFreeUnitCombatPromotions.end(); ++it)
	{
		if ((*it).first == eUnitCombat && (*it).second == ePromotion)
		{
			if (!bFree)
			{
				m_aFreeUnitCombatPromotions.erase(it);
			}
			return;
		}
	}

	if (bFree)
	{
		m_aFreeUnitCombatPromotions.push_back(std::make_pair(eUnitCombat, ePromotion));
	}
}

bool CvPlayer::isFreePromotion(UnitClassTypes eUnitClass, PromotionTypes ePromotion) const
{
	for (UnitClassPromotionArray::const_iterator it = m_aFreeUnitClassPromotions.begin(); it != m_aFreeUnitClassPromotions.end(); ++it)
	{
		if ((*it).first == eUnitClass && (*it).second == ePromotion)
		{
			return true;
		}
	}

	return false;
}

void CvPlayer::setFreePromotion(UnitClassTypes eUnitClass, PromotionTypes ePromotion, bool bFree)
{
	if (bFree)
	{
		m_aFreeUnitClassPromotions.push_back(std::make_pair(eUnitClass, ePromotion));
		return;
	}

	for (UnitClassPromotionArray::iterator it = m_aFreeUnitClassPromotions.begin(); it != m_aFreeUnitClassPromotions.end(); ++it)
	{
		if ((*it).first == eUnitClass && (*it).second == ePromotion)
		{
			m_aFreeUnitClassPromotions.erase(it);

		}
	}
}

// CACHE: cache frequently used values
///////////////////////////////////////
void CvPlayer::invalidatePopulationRankCache()
{
	int iLoop;
	CvCity* pLoopCity;
	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		pLoopCity->invalidatePopulationRankCache();
	}
}

void CvPlayer::invalidateYieldRankCache(YieldTypes eYield)
{
	int iLoop;
	CvCity* pLoopCity;
	for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		pLoopCity->invalidateYieldRankCache();
	}
}

void CvPlayer::doUpdateCacheOnTurn()
{
	// add this back, after testing without it
	// invalidateYieldRankCache();
}

void CvPlayer::doEra()
{
	int iGameYear = GC.getGameINLINE().getGameTurnYear();
	EraTypes eBestEra = getCurrentEra();
	for (int i = eBestEra + 1; i < GC.getNumEraInfos(); i++)
	{
		EraTypes eLoopEra = (EraTypes) i;
		if (GC.getEraInfo(eLoopEra).isRevolution() == isInRevolution())
		{
			if (GC.getEraInfo(eLoopEra).getGameTurn() >= 0)
			{
				int iEraYear = GC.getGameINLINE().getTurnYear(GC.getEraInfo(eLoopEra).getGameTurn());

				if (iEraYear <= iGameYear)
				{
					eBestEra = eLoopEra;
				}
			}
			else
			{
				eBestEra = eLoopEra;
			}
		}
	}

	setCurrentEra(eBestEra);
}

PlayerTypes CvPlayer::pickConqueredCityOwner(const CvCity& kCity) const
{
	return getID();
}

void CvPlayer::forcePeace(PlayerTypes ePlayer)
{
	FAssert(GET_TEAM(getTeam()).canChangeWarPeace(GET_PLAYER(ePlayer).getTeam()));

	CLinkList<TradeData> playerList;
	CLinkList<TradeData> loopPlayerList;
	TradeData kTradeData;
	setTradeItem(&kTradeData, TRADE_PEACE_TREATY, 0, NULL);
	playerList.insertAtEnd(kTradeData);
	loopPlayerList.insertAtEnd(kTradeData);

	GC.getGameINLINE().implementDeal(getID(), ePlayer, &playerList, &loopPlayerList);
}

int CvPlayer::getNewCityProductionValue() const
{
	int iValue = 0;
	for (int iJ = 0; iJ < GC.getNumBuildingClassInfos(); iJ++)
	{
		BuildingTypes eBuilding = ((BuildingTypes)(GC.getCivilizationInfo(getCivilizationType()).getCivilizationBuildings(iJ)));

		if (NO_BUILDING != eBuilding)
		{
			if (GC.getBuildingInfo(eBuilding).getFreeStartEra() != NO_ERA)
			{
				if (GC.getGameINLINE().getStartEra() >= GC.getBuildingInfo(eBuilding).getFreeStartEra())
				{
					for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
					{
						int iModifier = 0;
						if (iYield == YIELD_HAMMERS)
						{
							iModifier = getProductionModifier(eBuilding);
						}
						iValue += (100 * getYieldProductionNeeded(eBuilding, (YieldTypes)iYield)) / std::max(1, 100 + iModifier);
					}
				}
			}
		}
	}

	iValue *= 100 + GC.getDefineINT("NEW_CITY_BUILDING_VALUE_MODIFIER");
	iValue /= 100;

	iValue += (GC.getDefineINT("ADVANCED_START_CITY_COST") * GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent()) / 100;

	int iPopulation = GC.getDefineINT("INITIAL_CITY_POPULATION") + GC.getEraInfo(GC.getGameINLINE().getStartEra()).getFreePopulation();
	for (int i = 1; i <= iPopulation; ++i)
	{
		iValue += (getGrowthThreshold(i) * GC.getDefineINT("ADVANCED_START_POPULATION_COST")) / 100;
	}

	return iValue;
}

int CvPlayer::getGrowthThreshold(int iPopulation) const
{
	int iThreshold;

	iThreshold = (GC.getDefineINT("BASE_CITY_GROWTH_THRESHOLD") + (iPopulation * GC.getDefineINT("CITY_GROWTH_MULTIPLIER")));

	iThreshold *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent();
	iThreshold /= 100;

	iThreshold *= GC.getEraInfo(GC.getGameINLINE().getStartEra()).getGrowthPercent();
	iThreshold /= 100;

	if (isNative())
	{
		iThreshold *= GC.getDefineINT("NATIVE_GROWTH_THRESHOLD_MULTIPLIER");
		iThreshold /= 100;
	}
	else if (!isHuman())
	{
		iThreshold *= GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIGrowthPercent();
		iThreshold /= 100;

		iThreshold *= std::max(0, ((GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIPerEraModifier() * getCurrentEra()) + 100));
		iThreshold /= 100;
	}

	return std::max(1, iThreshold);
}

void CvPlayer::verifyUnitStacksValid()
{
	int iLoop;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		pLoopUnit->verifyStackValid();
	}
}

CvCity* CvPlayer::getPopulationUnitCity(int iUnitId) const
{
	int iLoop;

	for (CvCity* pCity = firstCity(&iLoop); pCity != NULL; pCity = nextCity(&iLoop))
	{
		if (NULL != pCity->getPopulationUnitById(iUnitId))
		{
			return pCity;
		}
	}

	return NULL;
}

/**
* The price of yield when we want to buy to Europe
*/
int CvPlayer::getYieldSellPrice(YieldTypes eYield) const
{
	FAssert(eYield >= 0);
	FAssert(eYield < NUM_YIELD_TYPES);

	return std::max(1, getYieldBuyPrice(eYield) + GC.getYieldInfo(eYield).getSellPriceDifference() - getYieldDecreasePriceEraChange(eYield));
}

/**
* The price of yield when we want to sell to Europe
*/
int CvPlayer::getYieldBuyPrice(YieldTypes eYield) const
{
	FAssert(eYield >= 0);
	FAssert(eYield < NUM_YIELD_TYPES);
	CvYieldInfo& kYield = GC.getYieldInfo(eYield);
	if (!kYield.isRawMaterial()) 
	{
		int iBasePrice = (kYield.getBuyPriceLow() + kYield.getBuyPriceHigh()) / 2;
		return iBasePrice + iBasePrice * getYieldRandomPercent(eYield) / 100;
	}

	PlayerTypes eEuropePlayer = getParent();
	CvPlayer& pEuropePlayer = GET_PLAYER(eEuropePlayer != NO_PLAYER ? eEuropePlayer : getID());

	return kYield.getBuyPriceLow() + (kYield.getBuyPriceHigh() - kYield.getBuyPriceLow()) * pEuropePlayer.getPricePercent(eYield) / 100;
}

int CvPlayer::getYieldDecreasePriceEraChange(YieldTypes eYield) const
{
	NewEraTypes eNewEra = (NewEraTypes) getNewEra();
	if (eNewEra == NO_NEW_ERA)
	{
		return 0;
	}	
	CvNewEraInfo& kNewEra = GC.getNewEraInfo(eNewEra);
	return kNewEra.getYieldDecreasePrice(eYield);
}

int CvPlayer::getYieldRandomPercent(YieldTypes eYield) const
{
	FAssert(eYield >= 0);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiYieldRandomPercent[eYield];
}

void CvPlayer::setYieldRandomPercent(YieldTypes eYield, int iPrice)
{
	FAssert(eYield >= 0);
	FAssert(eYield < NUM_YIELD_TYPES);

	if (iPrice != getYieldRandomPercent(eYield))
	{
		m_aiYieldRandomPercent[eYield] = iPrice;
	}
}

void CvPlayer::sellYieldUnitToEurope(CvUnit* pUnit, int iAmount, int iCommission, YieldTypes eYieldSell)
{
	if (pUnit != NULL)
	{
		FAssert(pUnit->isGoods());
		FAssert(getParent() != NO_PLAYER);
		CvPlayer& kPlayerEurope = GET_PLAYER(getParent());
		bool bDelayedDeath = (pUnit->getUnitTravelState() == NO_UNIT_TRAVEL_STATE);

		if (pUnit->getUnitInfo().isTreasure())
		{
			int iAmount = pUnit->getYieldStored();
			int iNetAmount = iAmount * (100 - iCommission) / 100;
			iNetAmount -= (iNetAmount * getTaxRate()) / 100;
			changeGold(iNetAmount * getExtraTradeMultiplier(kPlayerEurope.getID()) / 100);

			pUnit->setYieldStored(0);
			pUnit->setTransportUnit(NULL);
			pUnit->kill(bDelayedDeath);

			CvWStringBuffer szMessage;
			GAMETEXT.setEuropeYieldSoldHelp(szMessage, *this, NO_YIELD, iAmount, iCommission);
			m_aszEuropeTradeMessages.push_back(szMessage.getCString());
			gDLL->getInterfaceIFace()->addMessage(getID(), true, GC.getEVENT_MESSAGE_TIME(), szMessage.getCString(), "AS2D_BUILD_BANK", MESSAGE_TYPE_LOG_ONLY);

			gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
		}
	}
}

bool CvPlayer::buyYieldUnitFromEurope(YieldTypes eYield, int iAmount, CvUnit* pTransport)
{
	if (!isYieldEuropeTradable(eYield))
	{
		return false;
	}

	FAssert(pTransport != NULL);
	if (NULL == pTransport)
	{
		return false;
	}

	int iAmountAvailable = pTransport->getLoadYieldAmount(eYield);

	iAmount = std::min(iAmount, iAmountAvailable);

	if (iAmount == 0)
	{
		FAssertMsg(false, "Can't load cargo yield.");
		return false;
	}

	FAssert(pTransport->getOwnerINLINE() == getID());
	FAssert(getParent() != NO_PLAYER);
	CvPlayer& kPlayerEurope = GET_PLAYER(getParent());
	int iPrice = iAmount * kPlayerEurope.getYieldSellPrice(eYield);
	if (eYield == YIELD_AMMUNITION)
	{
		iPrice = std::max(1, iPrice/10);
	}
	FAssert(iPrice <= getGold());
	if (iPrice > getGold())
	{
		m_aszTradeMessages.push_back(gDLL->getText("EUROPE_SCREEN_BUY_UNIT_LACK_FUNDS", GC.getYieldInfo(eYield).getTextKeyWide(), iPrice));
		gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
		return false;
	}

	pTransport->setNewCargoYield(eYield, iAmount);

	changeGold(-iPrice);
	changeYieldTradedTotal(eYield, iAmount);
	kPlayerEurope.changeYieldTradedTotal(eYield, iAmount);
	GC.getGameINLINE().changeYieldBoughtTotal(kPlayerEurope.getID(), eYield, iAmount);

	CvWStringBuffer szMessage;
	GAMETEXT.setEuropeYieldBoughtHelp(szMessage, *this, eYield, iAmount);
	m_aszTradeMessages.push_back(szMessage.getCString());
	gDLL->getInterfaceIFace()->addMessage(getID(), true, GC.getEVENT_MESSAGE_TIME(), szMessage.getCString(), "AS2D_BUILD_BANK", MESSAGE_TYPE_LOG_ONLY);

	gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);

	gDLL->getEventReporterIFace()->yieldBoughtFromEurope(getID(), eYield, iAmount);
	return true;
}

int CvPlayer::getEuropeUnitBuyPrice(UnitTypes eUnit) const
{
	CvUnitInfo& kUnit = GC.getUnitInfo(eUnit);

	long iCost =  (long) kUnit.getEuropeCost();
	bool bNegative = (iCost < 0);

	if (kUnit.getAllowEra() > getNewEra() && kUnit.getAllowEra() > 1)
	{
		bNegative = true;
	}

	iCost = std::abs(iCost);

	iCost += GET_TEAM(getTeam()).getEuropeUnitsPurchased((UnitClassTypes) kUnit.getUnitClassType()) * kUnit.getEuropeCostIncrease();

	iCost *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getTrainPercent();
	iCost /= 100;

	iCost *= GC.getEraInfo(GC.getGameINLINE().getStartEra()).getTrainPercent();
	iCost /= 100;

	if (!kUnit.isMechUnit())
	{
		iCost *= std::max(0, (100 - getRecruitPriceDiscountCross()));
		iCost /= 100;
	}
	else
	{
		iCost = iCost*GC.getHandicapInfo(getHandicapType()).getBuyShipCost()/100;
	}

	if (bNegative)
	{
		iCost = std::min(-iCost, -1L);
	}

	return (int)iCost;
}

CvUnit* CvPlayer::buyEuropeUnit(UnitTypes eUnit, int iPriceModifier)
{
	FAssert(canTradeWithEurope());
	if (!canTradeWithEurope())
	{
		return NULL;
	}

	if (iPriceModifier > 0 && getEuropeUnitBuyPrice(eUnit) < 0)
	{
		return NULL;
	}

	if (NO_UNIT == eUnit)
	{
		return NULL;
	}

	CvUnitInfo& kUnitInfo = GC.getUnitInfo(eUnit);

	int iPrice = getEuropeUnitBuyPrice(eUnit) * iPriceModifier / 100;
	if (iPrice > std::max(0, getGold()))
	{
		m_aszTradeMessages.push_back(gDLL->getText("EUROPE_SCREEN_BUY_UNIT_LACK_FUNDS", GC.getUnitInfo(eUnit).getTextKeyWide(), iPrice));
		gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
		return NULL;
	}

	CvUnit* pUnit = NULL;
	CvPlot* pInitPlot = getStartingPlot();
	CvSeaway* pSeaway = getSeawayNearCity();
	if (pSeaway != NULL)
	{
		CvPlot* pPlot = pSeaway->plot();
		if (pPlot != NULL)
		{
			pInitPlot = pPlot;
		}
	}

	FAssert(pInitPlot != NULL);
	if (pInitPlot !=  NULL)
	{
		if (GC.getUnitInfo(eUnit).getDomainType() == DOMAIN_SEA)
		{
			pUnit = initUnit(eUnit, (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession(), INVALID_PLOT_COORD, INVALID_PLOT_COORD);
			if (pUnit != NULL)
			{
				pUnit->setUnitTravelState(UNIT_TRAVEL_STATE_IN_EUROPE, false);
				//add unit to map after setting Europe state so that it doesn't bump enemy units
				pUnit->addToMap(pInitPlot->getX_INLINE(), pInitPlot->getY_INLINE());
			}
		}
		else
		{
			pUnit = initEuropeUnit(eUnit);
		}
	}

	FAssert(NULL != pUnit);
	if (NULL != pUnit)
	{
		changeGold(-iPrice);
		GET_TEAM(getTeam()).changeEuropeUnitsPurchased(pUnit->getUnitClassType(), 1);
		gDLL->getEventReporterIFace()->unitBoughtFromEurope(getID(), pUnit->getID());

		if (pUnit->getUnitInfo().isMechUnit())
		{
			int iMaxMunition = pUnit->getUnitInfo().getMaxMunition();
			int iMaxCannon = pUnit->getUnitInfo().getMaxCannon();
			if (iMaxMunition != -1)
			{
				pUnit->setMunition(iMaxMunition);
			}
			if (iMaxCannon != -1)
			{
				pUnit->setNbCannon(iMaxCannon);
			}
		}

		if (eUnit == UNIT_SCOUT || eUnit == UNIT_SAILOR_EXPERT)
		{
			//Expert scout or expert sailor
			pUnit->setLevelExploAndNavigation(1);
			pUnit->setExperienceExploAndNavigation(50);
		}
		if (eUnit == UNIT_SAILOR_EXPERT || eUnit == UNIT_VETERAN || eUnit == UNIT_ARTILLERYMAN_EXPERT)
		{
			pUnit->setExperience(100);
		}
		for (int iJ = 0; iJ < GC.getNumPromotionInfos(); iJ++)
		{
			PromotionTypes ePromotion = (PromotionTypes)iJ;
			if (pUnit->getUnitInfo().getFreePromotions(iJ) && !GC.getPromotionInfo(ePromotion).isExploAndNavigation() && !GC.getPromotionInfo(ePromotion).isOnlyFreePromo())
			{
				pUnit->changeLevel(1);
			}
		}
		if (!isHuman() && getParent() != NO_PLAYER)
		{
			//Soldat Vétéran
			if (eUnit == UNIT_VETERAN)
			{
				ProfessionTypes eProfession = NO_PROFESSION;
				switch(getNewEra())
				{
				case 0:
					//Swordman
					eProfession = PROFESSION_SWORDMAN;
					break;
				case 1:
					//Arquebusier
					eProfession = PROFESSION_ARQUEBUSIER;
					break;
				case 2:
					//Musketman
					eProfession = PROFESSION_SOLDIER;
					break;
				}
				if (eProfession != NO_PROFESSION)
				{
					pUnit->setProfession(eProfession, true);
					pUnit->AI_setUnitAIType(UNITAI_DEFENSIVE);
				}
			}
		}
	}

	return pUnit;
}

void CvPlayer::buyUnitsFromKing()
{
	PlayerTypes eParent = getParent();
	if (eParent == NO_PLAYER)
	{
		return;
	}

	CvPlayer& kParent = GET_PLAYER(eParent);

	int iNumUnits;
	UnitTypes eUnit;
	ProfessionTypes eProfession;
	int iPrice = kParent.AI_militaryHelp(getID(), iNumUnits, eUnit, eProfession);

	int iPriceModifier = GC.getDefineINT("KING_BUY_UNIT_PRICE_MODIFIER");
	for (int i = 0; i < iNumUnits; ++i)
	{
		CvUnit* pUnit = buyEuropeUnit(eUnit, 0);
		changeEuropeLoan(getEuropeUnitBuyPrice(eUnit) * iPriceModifier / 100);
		if (pUnit != NULL)
		{
			pUnit->setProfession(eProfession, true);
		}
	}

	if (iNumUnits > 0)
	{
		changeBellsStored(iNumUnits * getEuropeUnitBuyPrice(eUnit) * iPriceModifier / 100);
	}
}


int CvPlayer::getYieldTradedTotal(YieldTypes eYield) const
{
	FAssert(eYield >= 0);
	FAssert(eYield < NUM_YIELD_TYPES);

	return m_aiYieldTradedTotal[eYield];
}

void CvPlayer::setYieldTradedTotal(YieldTypes eYield, int iValue)
{
	FAssert(eYield >= 0);
	FAssert(eYield < NUM_YIELD_TYPES);

	if (iValue != getYieldTradedTotal(eYield))
	{
		m_aiYieldTradedTotal[eYield] = iValue;
	}
}

void CvPlayer::changeYieldTradedTotal(YieldTypes eYield, int iChange)
{
	setYieldTradedTotal(eYield, getYieldTradedTotal(eYield) + iChange);
}

void CvPlayer::changeYieldBoughtTotal(YieldTypes eYield, int iChange)
{
	setYieldBoughtTotal(eYield, getYieldBoughtTotal(eYield) + iChange);
}

YieldTypes CvPlayer::getHighestTradedYield() const
{
	YieldTypes eBestYield = NO_YIELD;
	for (int i = 0; i < NUM_YIELD_TYPES; i++)
	{
		YieldTypes eYield = (YieldTypes) i;
		if (isYieldEuropeTradable(eYield))
		{
			if (getCity(getHighestStoredYieldCityId(eYield)) != NULL)
			{
				if ((eBestYield == NO_YIELD) || (getYieldTradedTotal(eYield) > getYieldTradedTotal(eBestYield)))
				{
					eBestYield = eYield;
				}
			}
		}
	}

	return eBestYield;
}

int CvPlayer::getHighestStoredYieldCityId(YieldTypes eYield) const
{
	int iLoop;
	int iBestCityId = -1;
	int iBestAmount = 0;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		int iAmount = pLoopCity->getYieldStored(eYield);
		if (iAmount > iBestAmount)
		{
			iBestAmount = iAmount;
			iBestCityId = pLoopCity->getID();
		}
	}

	return iBestCityId;
}

int CvPlayer::getYieldBoughtTotal(YieldTypes eYield) const
{
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);
	return m_aiYieldBoughtTotal[eYield];
}

void CvPlayer::setYieldBoughtTotal(YieldTypes eYield, int iValue)
{
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);
	m_aiYieldBoughtTotal[eYield] = iValue;
}

int CvPlayer::getAcceptSeedlingTurn(PlayerTypes ePlayer) const
{
	int iTurn = 40;
	if (ePlayer == NO_PLAYER)
	{
		return 0;
	}

	iTurn += GET_PLAYER(ePlayer).getRandomValues(0)%50;

	iTurn -=  iTurn*GET_PLAYER(getID()).AI_getAttitudeVal(ePlayer)/10;

	if (GET_PLAYER(getID()).getRelationTradeTo(ePlayer)>=5)
	{
		return 0;//We can have the seedling
	}
	return range(iTurn, 40, 120);
}

int CvPlayer::getCrossesStored() const
{
	return m_iCrossesStored;
}

void CvPlayer::changeCrossesStored(int iChange)
{
	m_iCrossesStored += iChange;
	FAssert(getCrossesStored() >= 0);
}

int CvPlayer::getBellsStored() const
{
	return m_iBellsStored;
}

void CvPlayer::changeBellsStored(int iChange)
{
	m_iBellsStored += iChange;
	FAssert(getBellsStored() >= 0);
}

int CvPlayer::getTaxRate() const
{
	return m_iTaxRate;
}

void CvPlayer::setTaxRate(int iValue)
{
	m_iTaxRate = iValue;
	FAssert(getTaxRate() >= 0);
}

void CvPlayer::changeTaxRate(int iChange)
{
	if (iChange != 0)
	{
		int iOldRate = getTaxRate();
		setTaxRate(iOldRate + iChange);

		//reset yields traded
		/*for (int i=0;i<NUM_YIELD_TYPES;i++)
		{
		setYieldTradedTotal((YieldTypes) i, 0);
		}*/

		PlayerTypes eParent = getParent();
		if (eParent != NO_PLAYER)
		{
			CvString szTextKey = (iOldRate < getTaxRate() ? "TXT_KEY_TAX_RATE_CHANGED" : "TXT_KEY_TAX_RATE_LOWERED");
			CvWString szBuffer = gDLL->getText(szTextKey.GetCString(), GET_PLAYER(eParent).getNameKey(), iOldRate, getTaxRate());
			gDLL->getInterfaceIFace()->addMessage(getID(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_REVOLTSTART", MESSAGE_TYPE_MINOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"));
		}
	}
}

int CvPlayer::getLastTurnOfTaxesIncrease() const
{
	return m_iLastTurnOfTaxesIncrease;
}

void CvPlayer::setLastTurnOfTaxesIncrease(int iValue)
{
	m_iLastTurnOfTaxesIncrease = iValue;
}

int CvPlayer::getNativeCombatModifier() const
{
	return m_iNativeCombatModifier;
}

void CvPlayer::setNativeCombatModifier(int iValue)
{
	m_iNativeCombatModifier = iValue;
}

void CvPlayer::changeNativeCombatModifier(int iChange)
{
	setNativeCombatModifier(getNativeCombatModifier() + iChange);
}

int CvPlayer::getDominateNativeBordersCount() const
{
	return m_iDominateNativeBordersCount;
}

void CvPlayer::changeDominateNativeBordersCount(int iChange)
{
	if (iChange != 0)
	{
		m_iDominateNativeBordersCount += iChange;
		FAssert(getDominateNativeBordersCount() >= 0);

		GC.getMapINLINE().updateCulture();
	}
}

int CvPlayer::getRevolutionEuropeTradeCount() const
{
	return m_iRevolutionEuropeTradeCount;
}

void CvPlayer::changeRevolutionEuropeTradeCount(int iChange)
{
	if (iChange != 0)
	{
		m_iRevolutionEuropeTradeCount += iChange;
		FAssert(getRevolutionEuropeTradeCount() >= 0);


		if (GC.getGameINLINE().getActiveTeam() == getID())
		{
			gDLL->getInterfaceIFace()->setDirty(ColoredPlots_DIRTY_BIT, true);
		}
	}
}

bool CvPlayer::canTradeWithEurope() const
{
	if (!isInRevolution())
	{
		return true;
	}

	if (getRevolutionEuropeTradeCount() > 0)
	{
		return true;
	}

	return false;
}

void CvPlayer::doAction(PlayerActionTypes eAction, int iData1, int iData2, int iData3)
{
	switch (eAction)
	{
	case PLAYER_ACTION_SELL_YIELD_UNIT:
		sellYieldUnitToEurope(getUnit(iData3), iData2, iData1, NO_YIELD);
		break;
	case PLAYER_ACTION_BUY_YIELD_UNIT:
		buyYieldUnitFromEurope((YieldTypes) iData1, iData2, getUnit(iData3));
		break;
	case PLAYER_ACTION_LOAD_UNIT_FROM_EUROPE:
		loadUnitFromEurope(getEuropeUnitById(iData1), getUnit(iData2));
		break;
	case PLAYER_ACTION_UNLOAD_UNIT_TO_EUROPE:
		unloadUnitToEurope(getUnit(iData1));
		break;
	case PLAYER_ACTION_TRANSFER_UNIT_IN_EUROPE:
		transferUnitInEurope(getUnit(iData1), getUnit(iData2));
		break;
	case PLAYER_ACTION_SET_OPTION:
		setOption((PlayerOptionTypes)iData1, (bool) iData2);
		break;
	case PLAYER_ACTION_AUTO_MOVES:
		setAutoMoves(true);
		break;
	case PLAYER_ACTION_TURN_COMPLETE:
		setEndTurn(true);
		break;
	case PLAYER_ACTION_APPLY_EVENT:
		applyEvent((EventTypes) iData1, iData2);
		break;
	case PLAYER_ACTION_SET_CIVIC:
		setCivic((CivicOptionTypes) iData1, (CivicTypes) iData2);
		break;
	case PLAYER_ACTION_RECEIVE_GOODY:
		receiveRandomGoody(GC.getMapINLINE().plotByIndexINLINE(iData2), (GoodyTypes) iData1, getUnit(iData3));
		break;
	case PLAYER_ACTION_BUY_UNITS_FROM_KING:
		buyUnitsFromKing();
		break;
	case PLAYER_ACTION_EUROPE_CHANGE_PROFESSION:
		changeProfessionEurope(iData1, (ProfessionTypes) iData2, iData3);
		break;
	case PLAYER_ACTION_FEAT:
		setFeatAccomplished((FeatTypes) iData1, iData2 != 0);
		break;
		//DOANE PLAYER_ACTION
	case PLAYER_ACTION_DESTROY_CITY:
		destroyCity(/*CvCity*/getCity(iData1));
		break;
	case PLAYER_ACTION_REMOVE_RESSOURCE:
		removeResource(/*CvUnit*/ getUnit(iData1));
		break;
	case PLAYER_ACTION_REPAIR_SHIP:
		repairShip(/*CvUnit*/ getUnit(iData1), iData2);
		break;
	case PLAYER_ACTION_EXP_LEAD_UNIT:
		giveExpToUnit(/*CvUnit*/ getUnit(iData1), /*int*/ iData2);
		break;
	case PLAYER_ACTION_LOAD_CREW:
		loadCrew(/*CvUnit*/ getUnit(iData1), /*int*/ iData2, /*bool*/ iData3);
		break;
	case PLAYER_ACTION_REPAY_LOAN:
		repayLoan(/*int*/iData1);
		break;
		//DOANE Builder Pack
	case PLAYER_ACTION_RESUPPLY_BUILDER_PACK:
		resupplyBuilderPack(/*CvUnit*/ getUnit(iData1), /*bool*/ iData2);
		break;
	case PLAYER_ACTION_TRADE_PROPOSITION: //The third parameter must be false otherwise we have a infinity loop. :(
		(iData3 == 0) ? setIATradeProposition((YieldTypes)iData1, iData2, false) : setHumanTradeProposition((YieldTypes)iData1, iData2, false);
		break;
	case PLAYER_ACTION_PROCESS_TRADE_PROPOSITION:
		processTradeProposition(/*CvUnit*/ getUnit(iData1),/*int*/iData2);
		break;
	case PLAYER_ACTION_ADD_SEAWAY:
		addSeawayFromUnit(/*CvUnit*/ getUnit(iData1));
		break;
	case PLAYER_ACTION_REMOVE_SEAWAY:
		iData1 != -1 ? deleteSeaway(/*int*/iData1) : deleteSeawayByID(/*int*/iData2);
		break;
	case PLAYER_ACTION_ROB_UNIT_CREW:
		robUnitCrew(/*CvUnit*/ getUnit(iData1),/*int*/iData2);
		break;
	case PLAYER_ACTION_ROB_UNIT_PASSENGERS:
		robUnitPassengers(/*CvUnit*/ getUnit(iData1),/*CvUnit*/ getUnit(iData2), /*int*/iData3);
		break;
	case PLAYER_ACTION_ROB_UNIT_CARGO:
		robUnitCargo(/*CvUnit*/ getUnit(iData1),(YieldTypes)iData2, /*int*/iData3);
		break;
	case PLAYER_ACTION_ROB_UNIT_END:
		robUnitEnd(/*CvUnit*/ getUnit(iData1),/*CvUnit*/ getUnit(iData2));
		break;
	case PLAYER_ACTION_MANAGE_HOSPITALS:
		manageHospitals(/*CvCity*/ getCity(iData1), /*int*/iData2);
		break;
	case PLAYER_ACTION_MANAGE_HOSPITALS_BIS:
		manageHospitalsBis(/*CvCity*/ getCity(iData1), /*int*/iData2, /*bool*/iData3);
		break;
	case PLAYER_ACTION_NEW_AGREEMENT:
		addNewAgreement(/*CvUnit*/ getUnit(iData1),/*CvCity*/getCity(iData2));
		break;
	case PLAYER_ACTION_NEW_AGREEMENT_PART1:
		addNewAgreementPart1(/*CvUnit*/ getUnit(iData1),/*int*/iData2,/*int*/iData3);
		break;
	case PLAYER_ACTION_NEW_AGREEMENT_PART2:
		addNewAgreementPart2(/*CvUnit*/ getUnit(iData1),/*int*/iData2,/*int*/iData3);
		break;
	case PLAYER_ACTION_END_OF_AGREEMENT:
		endOfAgreement(/*int*/iData1,/*int*/iData2,/*int*/iData3);
		break;
	case PLAYER_ACTION_AUTOMATIC_AGREEMENT_BY_YIELDS:
		setHasAutomaticTradeByYieldWithNative((YieldTypes) iData1, /*bool*/iData2);
		break;
	case PLAYER_ACTION_CHANGE_CITY_AGREEMENT:
		changeCityAgremment(/*CvAgreement*/ getAgreement(iData1),/*CvCity*/ getCity(iData2));
		break;
	case PLAYER_ACTION_CHOOSE_AGREEMENTS:
		chooseAgreement(/*CvUnit*/ getUnit(iData1),/*int*/iData2,/*bool*/iData3);
		break;
	case PLAYER_ACTION_PROCESS_RECRUIT_CREW:
		recruitCrew(/*CvTempUnit*/getTempUnit(iData1),/*int*/iData2);
		break;
	case PLAYER_ACTION_PROCESS_RECRUIT_IMMIGRANT:
		recruitImmigrant(/*CvTempUnit*/getTempUnit(iData1),/*int*/iData2);
		break;
	case PLAYER_ACTION_PROCESS_BUY_USED_SHIP:
		buyUsedShip(/*CvTempUnit*/getTempUnit(iData1),/*int*/iData2);
		break;
	case PLAYER_ACTION_PROCESS_PURCHASE_BID:
		processPurchaseBid(/*CvUnit*/ getUnit(iData1),/*int*/iData2);
		break;
	case PLAYER_ACTION_ARSENAL_MANAGEMENT:
		arsenalManagement(/*CvCity*/ getCity(iData1), (YieldTypes) iData2);
		break;
	case PLAYER_ACTION_BUY_MISSING_EQUIPEMENT:
		buyMissingEquipement(/*CvUnit*/getUnit(iData1));
		break;
	case PLAYER_ACTION_UNLOAD_ALL_IN_EUROPE:
		unloadAllInEurope(/*CvUnit*/getUnit(iData1));
		break;
	case PLAYER_ACTION_PENALITE_PLAYER_SAVE:
		setPenalitePlayerSave(true); 
		break;
	case PLAYER_ACTION_PENALITE_PLAYER_LOAD:
		setPenalitePlayerLoad(true);
		break;
	case PLAYER_ACTION_SELL_SHIP:
		sellShip(/*CvUnit*/getUnit(iData1),/*int*/ iData2);
		break;
	case PLAYER_ACTION_BUY_NEW_SHIP_IN_EUROPE:
		buyNewShipInEurope((UnitTypes)iData1,/*int*/ iData2, /*int*/ iData3);
		break;
	case PLAYER_ACTION_TRANSFERT_EUROPE_TO_WAREHOUSE:
		transfertEuropeToWarehouse((YieldTypes)iData1, iData2);
		break;
	case PLAYER_ACTION_PROCESS_RECEIVE_SEEDLING:
		GET_TEAM(getTeam()).setHasSeedling((BonusTypes)iData1, true);
		break;
	case PLAYER_ACTION_RESUPPLY_EUROPE_UNIT:
		resupplyEuropeUnit(/*CvUnit*/ getEuropeUnitById(iData1));
		break;
	case PLAYER_ACTION_TRAIN_CREW_IN_EUROPE:
		trainCrewInEurope(/*CvTempUnit*/ getTempUnit(iData1), /*int*/ iData2);
		break;
	case PLAYER_ACTION_SHIP_JOIN_GROUP:
		shipJoinGroup(/*CvUnit*/getUnit(iData1), /*CvUnit*/getUnit(iData2));
		break;
	case PLAYER_ACTION_EJECT_UNIT_TO_GROUP:
		ejectUnitToGroup(/*CvUnit*/getUnit(iData1), /*CvUnit*/getUnit(iData2));
		break;
	case PLAYER_ACTION_ENROL_UNIT_TYPE:
		enrolUnitType((UnitClassTypes) iData1, iData2);
		break;
	case PLAYER_ACTION_MAKE_THE_FIRST_SEAWAY:
		makeTheFirstSeaway(/*CvUnit*/getUnit(iData1), /*CvCity*/getCity(iData2), /*int*/iData3);
		break;
	case PLAYER_ACTION_MOVE_SEAWAY_RANK:
		moveSeawayRank(/*int*/ iData1, /*int*/ iData2);
		break;
	case PLAYER_ACTION_LAUNCHED_POPUP:
		setLaunchedPopup((ButtonPopupTypes) iData1, (bool)iData2);
		break;
	case PLAYER_ACTION_LAUNCH_POPUP:
		launchPlayerActionPopup((ButtonPopupTypes) iData1,/*int*/iData2,/*int*/iData3);
		break;
	case PLAYER_ACTION_GIVE_MAP:
		transfertMap(/*CvUnit*/getUnit(iData1), /*CvUnit*/getUnit(iData2));
		break;
	case PLAYER_ACTION_KING_GIVE_AGRONOMIST:
		kingGiveAgronomist((BonusTypes)iData1);
		break;
	case PLAYER_ACTION_CHANGE_DISPLAY_SEEDLINGS:
		changeDisplaySeedling();
		break;
	case PLAYER_ACTION_SWAP_SAILORS:
		swapSailors(/*CvUnit*/getUnit(iData1), /*CvUnit*/getUnit(iData2));
		break;
	case PLAYER_ACTION_CREATE_TRADE_ROUTE:
		createTradeRoute(iData1, iData2);
		break;
	case PLAYER_ACTION_REMOVE_TRADE_ROUTE:
		removeTradeRoute(iData1);
		break;
	case PLAYER_ACTION_CHANGE_TRADE_TYPE:
		changeTradeType(/*CvTradeRoute*/getTradeRoute(iData1), (TradeTypes)iData2, (YieldTypes)iData3);
		break;
	case PLAYER_ACTION_SET_TRADE_ROUTE_RADIO_OPTION:
		setTradeRouteRadioOption(/*CvTradeRoute*/getTradeRoute(iData1), (TradeRouteRadioTypes)iData2, (YieldTypes)iData3);
		break;
	case PLAYER_ACTION_SET_TRADE_ROUTE_RADIO_OPTION_QUANTITY:
		setTradeRouteRadioOptionQuantity(/*CvTradeRoute*/getTradeRoute(iData1), (YieldTypes)iData2, iData3);
		break;
	case PLAYER_ACTION_SET_TRADE_ROUTE_EUROPEAN_RADIO_OPTION:
		setTradeRouteEuropeanRadioOption(/*CvTradeRoute*/getTradeRoute(iData1), (TradeRouteRadioEuropeanTypes)iData2, (YieldTypes)iData3);
		break;
	case PLAYER_ACTION_SET_TRADE_ROUTE_RADIO_EUROPEAN_OPTION_QUANTITY:
		setTradeRouteRadioEuropeanOptionQuantity( (YieldTypes)iData1, (TradeRouteRadioEuropeanTypes)iData2, iData3);
		break;
	case PLAYER_ACTION_SET_TRADE_ROUTE_CHECK_BOX_OPTION_ACTIVATE:
		setTradeRouteCheckBoxOption(/*CvTradeRoute*/getTradeRoute(iData1), (TradeRouteCheckBoxTypes)iData2, (YieldTypes)iData3, true);
		break;
	case PLAYER_ACTION_SET_TRADE_ROUTE_CHECK_BOX_OPTION_DESACTIVATE:
		setTradeRouteCheckBoxOption(/*CvTradeRoute*/getTradeRoute(iData1), (TradeRouteCheckBoxTypes)iData2, (YieldTypes)iData3, false);
		break;
	case PLAYER_ACTION_SET_TRADE_ROUTE_CHECK_BOX_OPTION_QUANTITY:
		setTradeRouteCheckBoxOptionQuantity((TradeRouteCheckBoxTypes)iData1, (YieldTypes)iData2, iData3);
		break;
	case PLAYER_ACTION_CHOOSE_TRADE_ROUTE:
		chooseTradeRoute(/*CvUnit*/ getUnit(iData1),/*int*/iData2,/*bool*/iData3);
		break;
	case PLAYER_ACTION_SET_CURRENT_TRADE_ROUTE:
		setCurrentTradeRouteId(iData1, true);
		break;
	case PLAYER_ACTION_SET_DESTINATION_CITY_TO_UNIT:
		setDestinationCityToUnit(/*CvUnit*/ getEuropeUnitById(iData1), /*CvCity*/getCity(iData2));
		break;
	case PLAYER_ACTION_TRADE_ROUTE_MOVE_TRANSPORT_TO_TRANSPORT:
		moveTransportToTransport(/*CvUnit*/ getUnit(iData1), /*CvUnit*/ getUnit(iData2));
		break;
	case PLAYER_ACTION_SELECT_SEAWAY_FOR_GROUP:
		selectSeawayForGroup(/*CvSelectionGroup*/ getSelectionGroup(iData1), /*SeawayId*/iData2);
		break;
	case PLAYER_ACTION_SET_TRADE_ROUTE_CHECK_BOX_OPTION_GROUP:
		setTradeRouteCheckBoxOptionGroup(/*CvSelectionGroup*/ getSelectionGroup(iData1), (TradeRouteCheckBoxGroupsTypes)iData2, iData3);
		break;
	case PLAYER_ACTION_SET_STARTING_SPECIALIST:
		setStartingSpecialist((UnitTypes) iData1, NULL, /*CvUnit*/ getUnit(iData2));
		break;
	case PLAYER_ACTION_USE_RELATION_POINTS_FOR_IMMIGRATION:
		useRelationPointsForImmigration((UnitTypes) iData1);
		break;
	case PLAYER_ACTION_CHANGE_MIN_GOLD_TO_CONSERVE_FOR_TRADE_ROUTE:
		changeMinGoldToConserveForTradeRoute(iData1, iData2);
		break;
	case PLAYER_ACTION_PIN_YIELD_IN_EUROPE:
		setHasPinnedYieldInEurope((YieldTypes) iData1, iData2);
		break;
	case PLAYER_ACTION_PERMUTE_DEBUG_MODE:
		permuteDebugMode();
		break;
	case PLAYER_ACTION_SEND_PLAYER_OPTION:
		sendPlayerOption((PlayerOptionTypes) iData1, iData2);
		break;
	case PLAYER_ACTION_SEND_SECONDARYPLAYER_OPTION:
		setSecondaryOption((SecondaryPlayerOptionTypes)iData1, iData2);
		//DON'T REMOVE THE below actions
	case PLAYER_ACTION_BUY_EUROPE_UNIT://Don't remove it
		//buyEuropeUnit((UnitTypes) iData1, 100);
		break;
	case PLAYER_ACTION_CONVINCE_FATHER://Don't remove it
		//GET_TEAM(getTeam()).convinceFather((FatherTypes) iData1, iData2 != 0);
		break;
	case PLAYER_ACTION_HURRY:
		//hurry((HurryTypes) iData1, iData2);
		break;
	default:
		FAssertMsg(false, "Unknown action");
		break;
	}
}

void CvPlayer::ejectCitizen(CvCity* pCity)
{
	while (pCity->getPopulation() > 1)
	{
		if (!pCity->AI_removeWorstPopulationUnit(false))
		{
			pCity->AI_removeWorstPopulationUnit(true);
		}
	}
}

int CvPlayer::getTradeYieldAmount(YieldTypes eYield) const
{
	if (!GC.getYieldInfo(eYield).isCargo())
	{
		return 0;
	}

	int iAmount = 0;
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		iAmount += pLoopCity->getYieldStored(eYield);
	}

	int iMaxAmount = 100;
	int iModuloValue = 0;

	switch(eYield) 
	{
	case YIELD_HORSES:
		iMaxAmount = 24;
		iModuloValue = 4;
		break;
	case YIELD_MUSKETS:
		iMaxAmount = 8;
		iModuloValue = 4;
		break;
	}

	if (iModuloValue > 0) 
	{
		iAmount -= iAmount % iModuloValue;
	}

	return std::min(iAmount, iMaxAmount);
}

void CvPlayer::setCityBillboardDirty(bool bNewValue)
{
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		pLoopCity->setBillboardDirty(bNewValue);
	}
}

bool CvPlayer::isInRevolution() const
{
	if (getParent() == NO_PLAYER)
	{
		return false;
	}

	if (getNewEra() < 3) 
	{
		return false;
	}

	if (!::atWar(getTeam(), GET_PLAYER(getParent()).getTeam()))
	{
		return false;
	}

	return true;
}

bool CvPlayer::checkIndependence() const
{
	if (getParent() == NO_PLAYER)
	{
		return false;
	}

	for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
	{
		CvPlayer& kParent = GET_PLAYER((PlayerTypes) iPlayer);

		if (kParent.isAlive() && GET_TEAM(kParent.getTeam()).isParentOf(getTeam()))
		{
			// land units on map
			int iNumUnits = kParent.countNumTravelUnits(NO_UNIT_TRAVEL_STATE, DOMAIN_LAND);
			if (iNumUnits > 0)
			{
				return false;
			}

			// need both ships and land units
			int iNumLandUnits = kParent.countNumDomainUnits(DOMAIN_LAND);
			int iShips = kParent.countNumDomainUnits(DOMAIN_SEA);

			for (int i = 0; i < getNumRevolutionEuropeUnits(); ++i)
			{
				switch (GC.getUnitInfo(getRevolutionEuropeUnit(i)).getDomainType())
				{
				case DOMAIN_SEA:
					++iShips;
					break;
				case DOMAIN_LAND:
					++iNumLandUnits;
					break;
				default:
					break;
				}
			}

			if (iShips > 0 && iNumLandUnits > 0)
			{
				return false;
			}
		}
	}

	return true;
}


void CvPlayer::applyMissionaryPoints(CvCity* pCity)
{
	FAssert(pCity->getOwnerINLINE() == getID());
	PlayerTypes ePlayer = pCity->getMissionaryPlayer();
	if (ePlayer != NO_PLAYER)
	{
		int iModifier = 100 + getMissionaryRateModifier() + GET_PLAYER(ePlayer).getMissionaryRateModifier();
		changeMissionaryPoints(ePlayer, pCity->getMissionaryRate() * iModifier / 100);
		int iThreshold = missionaryThreshold(ePlayer);
		if (getMissionaryPoints(ePlayer) >= iThreshold)
		{
			//spawn converted native
			bool bUnitCreated = false;
			UnitClassTypes eUnitClass = (UnitClassTypes) GC.getCivilizationInfo(getCivilizationType()).getCapturedCityUnitClass();
			if (eUnitClass != NO_UNITCLASS)
			{
				UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(GET_PLAYER(ePlayer).getCivilizationType()).getCivilizationUnits(eUnitClass);
				if (eUnit != NO_UNIT)
				{
					CvUnit* pUnit = GET_PLAYER(ePlayer).initUnit(eUnit, (ProfessionTypes) GC.getUnitInfo(eUnit).getDefaultProfession(), pCity->getX_INLINE(), pCity->getY_INLINE());
					if (pUnit != NULL)
					{
						bUnitCreated = true;
						gDLL->getEventReporterIFace()->missionaryConvertedUnit(pUnit);

						CvWString szBuffer = gDLL->getText("TXT_KEY_NATIVES_CONVERTED", pCity->getNameKey());
						gDLL->getInterfaceIFace()->addMessage(ePlayer, true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_UNIT_GREATPEOPLE", MESSAGE_TYPE_INFO, GC.getUnitInfo(eUnit).getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_UNIT_TEXT"), pCity->getX_INLINE(), pCity->getY_INLINE(), true, true);

						changeMissionaryPoints(ePlayer, -iThreshold);
						setMissionaryThresholdMultiplier(ePlayer, (getMissionaryThresholdMultiplier(ePlayer) * (100 + GC.getDefineINT("MISSIONARY_THRESHOLD_INCREASE"))) / 100);
					}
				}
			}
			FAssert(bUnitCreated);
		}
	}
}

int CvPlayer::getMissionaryPoints(PlayerTypes ePlayer) const
{
	FAssert(ePlayer >= 0 && ePlayer < MAX_PLAYERS);
	return m_aiMissionaryPoints[ePlayer];
}

void CvPlayer::changeMissionaryPoints(PlayerTypes ePlayer, int iChange)
{
	FAssert(ePlayer >= 0 && ePlayer < MAX_PLAYERS);
	if (iChange != 0)
	{
		m_aiMissionaryPoints[ePlayer] += iChange;
		FAssert(m_aiMissionaryPoints[ePlayer] >= 0);
	}
}

int CvPlayer::getMissionaryThresholdMultiplier(PlayerTypes ePlayer) const
{
	FAssert(ePlayer >= 0 && ePlayer < MAX_PLAYERS);
	return m_aiMissionaryThresholdMultiplier[ePlayer];
}

void CvPlayer::setMissionaryThresholdMultiplier(PlayerTypes ePlayer, int iValue)
{
	FAssert(ePlayer >= 0 && ePlayer < MAX_PLAYERS);
	m_aiMissionaryThresholdMultiplier[ePlayer] = iValue;
	FAssert(getMissionaryThresholdMultiplier(ePlayer) > 0);
}

int CvPlayer::missionaryThreshold(PlayerTypes ePlayer) const
{
	int iThreshold = ((GC.getDefineINT("MISSIONARY_THRESHOLD") * std::max(0, (getMissionaryThresholdMultiplier(ePlayer)))) / 100);

	iThreshold *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent();
	iThreshold /= 100;

	iThreshold *= GC.getEraInfo(GC.getGameINLINE().getStartEra()).getGrowthPercent();
	iThreshold /= 100;

	return std::max(1, iThreshold);
}

void CvPlayer::burnMissions(PlayerTypes ePlayer)
{
	int iLoop;
	for (CvCity* pCity = firstCity(&iLoop); pCity != NULL; pCity = nextCity(&iLoop))
	{
		if (pCity->getMissionaryPlayer() == ePlayer)
		{
			pCity->setMissionaryPlayer(NO_PLAYER);
		}
		pCity->killTeachUnits(ePlayer);//We kill teach units for the same occasion
	}
}

bool CvPlayer::canHaveMission(PlayerTypes ePlayer) const
{
	if (!isNative())
	{
		return false;
	}

	if (!GET_PLAYER(ePlayer).isAlive())
	{
		return false;
	}

	if (GET_PLAYER(ePlayer).isNative())
	{
		return false;
	}

	if (::atWar(GET_PLAYER(ePlayer).getTeam(), getTeam()))
	{
		return false;
	}

	return true;
}

void CvPlayer::validateMissions()
{
	for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
	{
		PlayerTypes ePlayer = (PlayerTypes) iPlayer;
		if (!canHaveMission(ePlayer))
		{
			burnMissions(ePlayer);
		}
	}
}

int CvPlayer::getMissionaryRateModifier() const
{
	return m_iMissionaryRateModifier;
}

void  CvPlayer::changeMissionaryRateModifier(int iChange)
{
	m_iMissionaryRateModifier += iChange;
}

int CvPlayer::getMissionarySuccessPercent() const
{
	return m_iMissionarySuccessPercent;
}

void CvPlayer::setMissionarySuccessPercent(int iValue)
{
	m_iMissionarySuccessPercent = iValue;
}

int CvPlayer::getAttackFocusEuropeanPlayer() const
{
	return m_iAttackFocusEuropeanPlayer;
}

void CvPlayer::setAttackFocusEuropeanPlayer(int iValue)
{
	m_iAttackFocusEuropeanPlayer = iValue;
}

short CvPlayer::getImmigrationRelationPoints() const
{
	return m_sImmigrationRelationPoints;
}

void CvPlayer::changeImmigrationRelationPoints(short sValue)
{
	m_sImmigrationRelationPoints += sValue;
}

void CvPlayer::processImmigrationRelationPoints()
{
	if (isEuropean())
	{
		changeImmigrationRelationPoints(1);
	}
}

short CvPlayer::getImmigrationRelationPointRequired(UnitTypes eUnit)
{
	CvUnitInfo& kUnit = GC.getUnitInfo(eUnit);
	short sImmigration = kUnit.getProbaImmigration();

	return (100 - sImmigration)/2;
}

int CvPlayer::getRebelCombatPercent() const
{
	int iPercent = 100;

	for (int iTrait = 0; iTrait < GC.getNumTraitInfos(); ++iTrait)
	{
		TraitTypes eTrait = (TraitTypes) iTrait;
		CvTraitInfo& kTrait = GC.getTraitInfo(eTrait);
		if (kTrait.getRebelCombatModifier() != 0)
		{
			if (hasTrait(eTrait))
			{
				iPercent += kTrait.getRebelCombatModifier();
			}
		}
	}

	iPercent *= GC.getDefineINT("MAX_REBEL_COMBAT_MULTIPLER");
	iPercent /= 100;

	return iPercent;
}

int CvPlayer::getProfessionEquipmentModifier(ProfessionTypes eProfession) const
{
	FAssert(eProfession >= 0 && eProfession < GC.getNumProfessionInfos());
	return m_aiProfessionEquipmentModifier[eProfession];
}

void CvPlayer::setProfessionEquipmentModifier(ProfessionTypes eProfession, int iValue)
{
	FAssert(eProfession >= 0 && eProfession < GC.getNumProfessionInfos());
	int iChange = iValue - getProfessionEquipmentModifier(eProfession);
	if (iChange != 0)
	{
		std::vector<CvUnit*> aProfessionUnits;
		int iLoop;
		for (CvUnit* pUnit = firstUnit(&iLoop); pUnit != NULL; pUnit = nextUnit(&iLoop))
		{
			if (pUnit->getProfession() == eProfession)
			{
				aProfessionUnits.push_back(pUnit);
			}
		}
		for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
		{
			CvUnit* pUnit = m_aEuropeUnits[i];
			if (pUnit->getProfession() == eProfession)
			{
				aProfessionUnits.push_back(pUnit);
			}
		}
		for (CvCity* pCity = firstCity(&iLoop); pCity != NULL; pCity = nextCity(&iLoop))
		{
			for (int i = 0; i < pCity->getPopulation(); ++i)
			{
				CvUnit* pUnit = pCity->getPopulationUnitByIndex(i);
				if (pUnit->getProfession() == eProfession)
				{
					aProfessionUnits.push_back(pUnit);
				}
			}
		}

		for (uint i = 0; i < aProfessionUnits.size(); ++i)
		{
			CvUnit* pUnit = aProfessionUnits[i];
			changeAssets(-pUnit->getAsset());
			int iPower = pUnit->getPower();
			changePower(-iPower);
			CvArea* pArea = pUnit->area();
			if (pArea != NULL)
			{
				pArea->changePower(getID(), -iPower);
			}
		}

		m_aiProfessionEquipmentModifier[eProfession] = iValue;

		for (uint i = 0; i < aProfessionUnits.size(); ++i)
		{
			CvUnit* pUnit = aProfessionUnits[i];
			changeAssets(pUnit->getAsset());
			int iPower = pUnit->getPower();
			changePower(iPower);
			CvArea* pArea = pUnit->area();
			if (pArea != NULL)
			{
				pArea->changePower(getID(), iPower);
			}
		}

		FAssert(getProfessionEquipmentModifier(eProfession) >= -100);
	}
}

int CvPlayer::getYieldEquipmentAmount(ProfessionTypes eProfession, YieldTypes eYield) const
{
	FAssert(eProfession >= 0 && eProfession < GC.getNumProfessionInfos());
	FAssert(eYield >= 0 && eYield < NUM_YIELD_TYPES);

	int iAmount = GC.getProfessionInfo(eProfession).getYieldEquipmentAmount(eYield);

	iAmount *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getTrainPercent();
	iAmount /= 100;

	iAmount *= 100 + getProfessionEquipmentModifier(eProfession);
	iAmount /= 100;

	return std::max(0, iAmount);
}

bool CvPlayer::isProfessionValid(ProfessionTypes eProfession, UnitTypes eUnit) const
{
	int iLoop;
	CvCity* pLoopCity;

	if (eProfession != NO_PROFESSION)
	{
		if (!GC.getCivilizationInfo(getCivilizationType()).isValidProfession(eProfession))
		{
			return false;
		}

		if (eUnit != NO_UNIT)
		{
			CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);

			UnitClassTypes eUnitClass = (UnitClassTypes) GC.getUnitInfo(eUnit).getUnitClassType();
			int i=0;
			int j=0;
			if (eUnitClass != NO_UNITCLASS)
			{
				for (int iI = 0; iI < GC.getNumUnitClassInfos(); iI++)
				{
					if (kProfession.isClassAllowed(iI))
					{
						j++;
						if (eUnitClass == (UnitClassTypes) iI)
						{
							i++;
						}
					}
					if (kProfession.isClassProhibited(iI))
					{
						if (eUnitClass == (UnitClassTypes) iI)
						{
							return false;
						}
					}
				}
			}
			if (j>0)
			{
				if (i==0)
				{
					return false;
				}
			}
			for (int iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
			{
				if (kProfession.isBuildingClassNeededInCity(iI))
				{
					BuildingTypes ePrereqBuilding = ((BuildingTypes)(GC.getCivilizationInfo(getCivilizationType()).getCivilizationBuildings(iI)));
					if (ePrereqBuilding != NO_BUILDING)
					{
						for (pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
						{
							if (pLoopCity->isHasRealBuilding(ePrereqBuilding))
							{
								return GC.getUnitInfo(eUnit).getDefaultProfession() != NO_PROFESSION;
							}
						}
						return false;
					}
				}
			}
			//endm07
			if (GC.getUnitInfo(eUnit).isOnlyDefensive())
			{
				// Natives cannot be combat professions
				//CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);
				if ((!kProfession.isUnarmed() && kProfession.getCombatChange() > 0 && eProfession != PROFESSION_SCOUT) || kProfession.getMissionaryRate() > 0)
				{
					return false;
				}
			}

			if (GC.getUnitInfo(eUnit).getDefaultProfession() == NO_PROFESSION)
			{
				return false;
			}
		}
	}

	return true;
}

int CvPlayer::getNumRevolutionEuropeUnits() const
{
	return m_aEuropeRevolutionUnits.size();
}

UnitTypes CvPlayer::getRevolutionEuropeUnit(int i) const
{
	FAssert(i >= 0 && i < getNumRevolutionEuropeUnits());

	return m_aEuropeRevolutionUnits[i].first;
}

ProfessionTypes CvPlayer::getRevolutionEuropeProfession(int i) const
{
	FAssert(i >= 0 && i < getNumRevolutionEuropeUnits());

	return m_aEuropeRevolutionUnits[i].second;
}

void CvPlayer::addRevolutionEuropeUnit(UnitTypes eUnit, ProfessionTypes eProfession)
{
	m_aEuropeRevolutionUnits.push_back(std::make_pair(eUnit, eProfession));
}

void CvPlayer::clearRevolutionEuropeUnits()
{
	m_aEuropeRevolutionUnits.clear();
}

UnitTypes CvPlayer::pickBestImmigrant()
{
	std::vector<int> aiWeights(GC.getNumUnitInfos(), 0);
	for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass)
	{
		UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(iUnitClass);
		if (NO_UNIT != eUnit)
		{
			int iWeight = GC.getUnitInfo(eUnit).getImmigrationWeight();
			for (int i = 0; i < NUM_YIELD_TYPES; i++)
			{
				YieldTypes eYield = (YieldTypes) i;
				if (GC.getUnitInfo(eUnit).getYieldModifier(eYield) > 0)
				{
					if (isHasYieldUnknown(eYield))
					{
						iWeight = 0;// We refuse immigrants who produce unknown yield
					}
				}
			}

			aiWeights[eUnit] += iWeight;
		}
	}

	UnitTypes eBestUnit = (UnitTypes) GC.getGameINLINE().getSorenRand().pickValue(aiWeights, "pick immigrant");
	FAssert(NO_UNIT != eBestUnit);

	return eBestUnit;
}

UnitTypes CvPlayer::pickFreeUnit()
{
	std::vector<int> aiWeights(GC.getNumUnitInfos(), 0);
	for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass)
	{
		UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(iUnitClass);
		if (NO_UNIT != eUnit)
		{
			aiWeights[eUnit] = GC.getUnitInfo(eUnit).isStartingUnit() ? 1 : 0;
		}
	}

	UnitTypes eBestUnit = (UnitTypes) GC.getGameINLINE().getSorenRand().pickValue(aiWeights, "pick free unit");
	FAssert(NO_UNIT != eBestUnit);

	return eBestUnit;
}


int CvPlayer::getNumNeededUnitClass(UnitClassTypes eUnitClass) const
{
	int iResult = 0;
	int iLoop;
	UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClass);
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		int iNeeded = pLoopCity->getNumNeededUnitClass(eUnitClass);
		iResult += iNeeded > 0 ? iNeeded : 0;
	}

	return std::max(0, iResult);
}


CvCity* CvPlayer::getBestWorkingCity(UnitClassTypes eUnitClass, bool bForceResult) const
{	
	CvCity* pBestCity = NULL;
	int iLoop, iBestValue = 0;

	if (isHuman())
	{
		return NULL;
	}

	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		int iTempValue = pLoopCity->getNumNeededUnitClass(eUnitClass);
		if (iTempValue > iBestValue)
		{
			iBestValue = iTempValue;
			pBestCity = pLoopCity; 
		}
	}

	if (pBestCity == NULL)
	{
		int index;
		pBestCity = firstCity(&index);
	}
	return pBestCity;
}

void CvPlayer::buyLand(CvPlot* pPlot, bool bFree)
{
	int iCulture = pPlot->getBuyCultureAmount(getID());
	int iGoldCost = bFree ? 0 : pPlot->getBuyPrice(getID());

	if (getGold() < iGoldCost)
	{
		return;
	}

	PlayerTypes eOldOwner = pPlot->getOwnerINLINE();
	if (eOldOwner != NO_PLAYER)
	{
		pPlot->setCulture(eOldOwner, 0, false);
		if (!GET_TEAM(pPlot->getTeam()).isAtWar(getTeam()))
		{
			GET_PLAYER(eOldOwner).changeGold((iGoldCost * GC.getDefineINT("BUY_PLOT_SELLER_INCOME_PERCENT")) / 100);
			GET_PLAYER(getID()).AI_changeGoldTradedTo(eOldOwner, iGoldCost);
		}
	}

	pPlot->changeCulture(getID(), iCulture, true);
	changeGold(-iGoldCost);
}

int CvPlayer::getNumTradeMessages() const
{
	return m_aszTradeMessages.size();
}

const wchar* CvPlayer::getTradeMessage(int i) const
{
	return m_aszTradeMessages[i].GetCString();
}

void CvPlayer::addTradeMessage(CvWString& szString)
{
	m_aszTradeMessages.push_back(szString);
}

int CvPlayer::getNumEuropeTradeMessages() const
{
	return m_aszEuropeTradeMessages.size();
}

const wchar* CvPlayer::getEuropeTradeMessage(int i) const
{
	return m_aszEuropeTradeMessages[i].GetCString();
}

void CvPlayer::addEuropeTradeMessage(CvWString& szString)
{
	m_aszEuropeTradeMessages.push_back(szString);
}

int CvPlayer::getNumEventEuropeMessages() const
{
	return m_aszEventEuropeMessages.size();
}

const wchar* CvPlayer::getEventEuropeMessage(int i) const
{
	return m_aszEventEuropeMessages[i].GetCString();
}

void CvPlayer::buildTradeTable(PlayerTypes eOtherPlayer, CLinkList<TradeData>& ourList, const IDInfo& kTransport) const
{
	TradeData item;
	int iLoop;

	//	Gold
	setTradeItem(&item, TRADE_GOLD, 0, &kTransport);
	if (canTradeItem(eOtherPlayer, item))
	{
		ourList.insertAtEnd(item);
	}

	//	Maps
	setTradeItem(&item, TRADE_MAPS, 0, &kTransport);
	if (canTradeItem(eOtherPlayer, item))
	{
		ourList.insertAtEnd(item);
	}

	//	Open Borders
	setTradeItem(&item, TRADE_OPEN_BORDERS, 0, &kTransport);
	if (canTradeItem(eOtherPlayer, item))
	{
		ourList.insertAtEnd(item);
	}

	//	Sharing vision
	setTradeItem(&item, TRADE_SHARE_VISION, 0, &kTransport);
	if (canTradeItem(eOtherPlayer, item))
	{
		ourList.insertAtEnd(item);
	}

	//	Defensive Pact
	setTradeItem(&item, TRADE_DEFENSIVE_PACT, 0, &kTransport);
	if (canTradeItem(eOtherPlayer, item))
	{
		ourList.insertAtEnd(item);
	}

	//	Permanent Alliance
	setTradeItem(&item, TRADE_PERMANENT_ALLIANCE, 0, &kTransport);
	if (canTradeItem(eOtherPlayer, item))
	{
		ourList.insertAtEnd(item);
	}

	if (::atWar(getTeam(), GET_PLAYER(eOtherPlayer).getTeam()))
	{
		//	We are at war, allow a peace treaty option
		setTradeItem(&item, TRADE_PEACE_TREATY, 0, &kTransport);
		ourList.insertAtEnd(item);
	}

	//	Initial build of the inventory lists and buttons.
	//	Go through all the possible headings
	for (int i = NUM_BASIC_ITEMS; i < NUM_TRADEABLE_HEADINGS; i++)
	{
		bool bFoundItemUs = false;

		//	Build what we need to build for this item
		switch (i)
		{
		case TRADE_YIELD:
			for (int j = 0; j < NUM_YIELD_TYPES; j++)
			{
				setTradeItem(&item, TRADE_YIELD, j, &kTransport);
				if (canTradeItem(eOtherPlayer, item))
				{
					bFoundItemUs = true;
					ourList.insertAtEnd(item);
				}
			}
			break;

		case TRADE_CITIES:
			for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
			{
				setTradeItem(&item, TRADE_CITIES, pLoopCity->getID(), &kTransport);
				if (canTradeItem(eOtherPlayer, item))
				{
					bFoundItemUs = true;
					ourList.insertAtEnd(item);
				}
			}
			break;

		case TRADE_PEACE:
			if (!isHuman())
			{
				for (int j = 0; j < MAX_TEAMS; j++)
				{
					if (GET_TEAM((TeamTypes)j).isAlive())
					{
						if (j != getTeam() && j != GET_PLAYER(eOtherPlayer).getTeam())
						{
							setTradeItem(&item, TRADE_PEACE, j, &kTransport);
							if (canTradeItem(eOtherPlayer, item))
							{
								ourList.insertAtEnd(item);
								bFoundItemUs = true;
							}
						}
					}
				}
			}
			break;

		case TRADE_WAR:
			if (!isHuman())
			{
				for (int j = 0; j < MAX_TEAMS; j++)
				{
					if (GET_TEAM((TeamTypes)j).isAlive())
					{
						if (j != getTeam() && j != GET_PLAYER(eOtherPlayer).getTeam())
						{
							setTradeItem(&item, TRADE_WAR, j, &kTransport);
							if (canTradeItem(eOtherPlayer, item))
							{
								ourList.insertAtEnd(item);
								bFoundItemUs = true;
							}
						}
					}
				}
			}
			break;

		case TRADE_EMBARGO:
			if (!isHuman())
			{
				for (int j = 0; j < MAX_TEAMS; j++)
				{
					if (GET_TEAM((TeamTypes)j).isAlive())
					{
						if (j != getTeam() && j != GET_PLAYER(eOtherPlayer).getTeam())
						{
							setTradeItem(&item, TRADE_EMBARGO, j, &kTransport);
							if (canTradeItem(eOtherPlayer, item))
							{
								ourList.insertAtEnd(item);
								bFoundItemUs = true;
							}
						}
					}
				}
			}
			break;
		}
	}
}

bool CvPlayer::getHeadingTradeString(PlayerTypes eOtherPlayer, TradeableItems eItem, CvWString& szString, CvString& szIcon) const
{
	szIcon.clear();

	switch ( eItem )
	{
	case TRADE_YIELD:
		szString = gDLL->getText("TXT_KEY_TRADE_YIELD_HEADING");
		break;

	case TRADE_CITIES:
		szString = gDLL->getText("TXT_KEY_TRADE_CITIES");
		break;

	case TRADE_PEACE:
		szString = gDLL->getText("TXT_KEY_TRADE_MAKE_PEACE_WITH");
		break;

	case TRADE_WAR:
		szString = gDLL->getText("TXT_KEY_TRADE_DECLARE_WAR_ON");
		break;

	case TRADE_EMBARGO:
		szString = gDLL->getText("TXT_KEY_TRADE_STOP_TRADING_WITH");
		break;

	default:
		szString.clear();
		return false;
		break;
	}

	return true;
}


bool CvPlayer::getItemTradeString(PlayerTypes eOtherPlayer, bool bOffer, bool bShowingCurrent, const TradeData& zTradeData, const IDInfo& kTransport, CvWString& szString, CvString& szIcon) const
{
	szIcon.clear();

	switch (zTradeData.m_eItemType)
	{
	case TRADE_GOLD:
		if (bOffer)
		{
			szString = gDLL->getText("TXT_KEY_TRADE_GOLD_NUM", zTradeData.m_iData1);
		}
		else
		{
			szString = gDLL->getText("TXT_KEY_TRADE_GOLD_NUM", getMaxGoldTrade(eOtherPlayer, kTransport));
		}
		break;
	case TRADE_MAPS:
		szString = gDLL->getText("TXT_KEY_TRADE_WORLD_MAP_STRING");
		break;
	case TRADE_OPEN_BORDERS:
		szString = gDLL->getText("TXT_KEY_TRADE_OPEN_BORDERS_STRING");
		break;
	case TRADE_SHARE_VISION:
		szString = gDLL->getText("TXT_KEY_TRADE_SHARE_VISION_STRING");
		break;
	case TRADE_DEFENSIVE_PACT:
		szString = gDLL->getText("TXT_KEY_TRADE_DEFENSIVE_PACT_STRING");
		break;
	case TRADE_PERMANENT_ALLIANCE:
		szString = gDLL->getText("TXT_KEY_TRADE_PERMANENT_ALLIANCE_STRING");
		break;
	case TRADE_PEACE_TREATY:
		szString = gDLL->getText("TXT_KEY_TRADE_PEACE_TREATY_STRING", GC.getDefineINT("PEACE_TREATY_LENGTH"));
		break;
	case TRADE_YIELD:
		{
			YieldTypes eYield = (YieldTypes) zTradeData.m_iData1;
			CvYieldInfo& kYield = GC.getYieldInfo(eYield);
			int iAmount = bOffer ? GET_PLAYER(eOtherPlayer).getTradeYieldAmount(eYield) : getTradeYieldAmount(eYield);
			szString.Format( L"%s (%d%c)", kYield.getDescription(), iAmount, kYield.getChar());
		}
		break;

	case TRADE_CITIES:
		{
			CvCity* pCity = bOffer ? GET_PLAYER(eOtherPlayer).getCity(zTradeData.m_iData1) : getCity(zTradeData.m_iData1);
			if (NULL != pCity)
			{
				if (pCity->getLiberationPlayer(false) == eOtherPlayer)
				{
					szString.Format(L"%s (%s)", pCity->getName().GetCString(), gDLL->getText("TXT_KEY_LIBERATE_CITY").GetCString());
				}
				else
				{
					szString = gDLL->getText("TXT_KEY_CITY_OF", pCity->getNameKey());
				}
			}
		}
		break;
	case TRADE_PEACE:
		if (bOffer)
		{
			szString = gDLL->getText("TXT_KEY_TRADE_PEACE_WITH");
			szString += GET_TEAM((TeamTypes)zTradeData.m_iData1).getName();
		}
		else
		{
			szString = GET_TEAM((TeamTypes)zTradeData.m_iData1).getName();
		}
		break;
	case TRADE_WAR:
		if (bOffer)
		{
			szString = gDLL->getText("TXT_KEY_TRADE_WAR_WITH");
			szString += GET_TEAM((TeamTypes)zTradeData.m_iData1).getName();
		}
		else
		{
			szString = GET_TEAM((TeamTypes)zTradeData.m_iData1).getName();
		}
		break;
	case TRADE_EMBARGO:
		if (bOffer)
		{
			szString = gDLL->getText("TXT_KEY_TRADE_STOP_TRADING_WITH");
			szString += L" " + GET_TEAM((TeamTypes)zTradeData.m_iData1).getName();
		}
		else
		{
			szString = GET_TEAM((TeamTypes)zTradeData.m_iData1).getName();
		}
		break;
	default:
		szString.clear();
		return false;
	}
	return true;
}

void CvPlayer::updateTradeList(PlayerTypes eOtherPlayer, CLinkList<TradeData>& ourInventory, const CLinkList<TradeData>& ourOffer, const CLinkList<TradeData>& theirOffer, const IDInfo& kTransport) const
{
	for (CLLNode<TradeData>* pNode = ourInventory.head(); pNode != NULL; pNode = ourInventory.next(pNode))
	{
		pNode->m_data.m_bHidden = false;

		// Don't show peace treaties when not at war
		if (!::atWar(getTeam(), GET_PLAYER(eOtherPlayer).getTeam()))
		{
			if (pNode->m_data.m_eItemType == TRADE_PEACE_TREATY)
			{
				pNode->m_data.m_bHidden = true;
			}
		}

		if (pNode->m_data.m_eItemType == TRADE_YIELD)
		{
			for (CLLNode<TradeData>* pOfferNode = theirOffer.head(); pOfferNode != NULL; pOfferNode = theirOffer.next(pOfferNode))
			{
				if (pOfferNode->m_data.m_eItemType == TRADE_YIELD)
				{
					pNode->m_data.m_bHidden = true;
					break;
				}
			}
		}
	}

	if (!isHuman() || !GET_PLAYER(eOtherPlayer).isHuman())  // everything allowed in human-human trades
	{
		CLLNode<TradeData>* pFirstOffer = ourOffer.head();
		if (pFirstOffer == NULL)
		{
			pFirstOffer = theirOffer.head();
		}

		if (pFirstOffer != NULL)
		{
			if (!CvDeal::isEndWar(pFirstOffer->m_data.m_eItemType) || !::atWar(getTeam(), GET_PLAYER(eOtherPlayer).getTeam()))
			{
				for (CLLNode<TradeData>* pNode = ourInventory.head(); pNode != NULL; pNode = ourInventory.next(pNode))
				{
					if (pFirstOffer->m_data.m_eItemType == TRADE_CITIES || pNode->m_data.m_eItemType == TRADE_CITIES)
					{
						pNode->m_data.m_bHidden = true;
					}
					else if (CvDeal::isAnnual(pFirstOffer->m_data.m_eItemType) != CvDeal::isAnnual(pNode->m_data.m_eItemType))
					{
						pNode->m_data.m_bHidden = true;
					}
				}
			}
		}
	}
}

int CvPlayer::getMaxGoldTrade(PlayerTypes eOtherPlayer, const IDInfo& kTransport) const
{
	CvCity* pTradeCity = NULL;
	CvUnit* pTransport = ::getUnit(kTransport);
	if (pTransport != NULL)
	{
		CvPlot* pPlot = pTransport->plot();
		if (pPlot != NULL)
		{
			pTradeCity = pPlot->getPlotCity();
		}
	}

	if (pTradeCity != NULL && pTradeCity->getOwnerINLINE() == getID())
	{
		return pTradeCity->AI_maxGoldTrade(eOtherPlayer);
	}
	else if (!isNative())
	{
		return AI_maxGoldTrade(eOtherPlayer);
	}

	return 0;
}

UnitTypes CvPlayer::getIdealUnitTypeForYield(YieldTypes eYield) const
{
	for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass)
	{
		UnitTypes eLoopUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(iUnitClass);
		if (eLoopUnit != NO_UNIT)
		{
			ProfessionTypes eProfession = (ProfessionTypes)GC.getUnitInfo(eLoopUnit).getIdealProfession();
			if (eProfession != NO_PROFESSION)
			{
				if (GC.getProfessionInfo(eProfession).getYieldProduced() == eYield)
				{
					return eLoopUnit;
				}
			}
		}
	}
	return NO_UNIT;
}

void CvPlayer::changeProfessionEurope(int iUnitId, ProfessionTypes eNewProfession, int iMode)
{
	FAssert(getParent() != NO_PLAYER);
	CvPlayer& kEuropePlayer = GET_PLAYER(getParent());
	CvUnit* pUnit = getEuropeUnitById(iUnitId);
	ProfessionTypes eOldProfession = pUnit->getProfession();

	CvProfessionInfo& kNewProfession = GC.getProfessionInfo(eNewProfession);
	CvProfessionInfo& kOldProfession = GC.getProfessionInfo(eOldProfession);

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;		
		int iNewProfessionYieldAmount = kNewProfession.getYieldEquipmentAmount(eYield);
		int iOldProfessionYieldAmount = kOldProfession.getYieldEquipmentAmount(eYield);

		if (iNewProfessionYieldAmount > iOldProfessionYieldAmount)
		{ // => iNewProfessionYieldAmount > 0
			int iEuropeStock = getEuropeWarehouseYield(eYield);
			int iEuropeMarketStock = kEuropePlayer.getEuropeWarehouseYield(eYield);
			int iYieldNeeded = iNewProfessionYieldAmount - iOldProfessionYieldAmount;
			int iQuantityToBuy = 0;

			switch(iMode)
			{
			case 1://We take all equipments in our warehouse
				changeEuropeWarehouseYield(eYield, -iYieldNeeded);
				break;
			case 2://We take all in equipments in our warehouse and we buy the missing yields
				{
					int iStock = getEuropeWarehouseYield(eYield);
					FAssert(iStock > 0);
					FAssert(iStock < iYieldNeeded);
					changeEuropeWarehouseYield(eYield, -iStock);
					int iMissingEquipments = iYieldNeeded - iStock;
					changeGold(-getBuyPriceForYield(eYield, iMissingEquipments));
					kEuropePlayer.changeEuropeWarehouseYield(eYield, -iMissingEquipments);
					FAssert(kEuropePlayer.getEuropeWarehouseYield(eYield) >= 0)
						FAssert(getGold() >= 0);
				}
				break;
			case 3:// We buy all equipments in Europe
				changeGold(-getBuyPriceForYield(eYield, iYieldNeeded));
				FAssert(getGold() >= 0);
				kEuropePlayer.changeEuropeWarehouseYield(eYield, -iYieldNeeded);
				FAssert(kEuropePlayer.getEuropeWarehouseYield(eYield) >= 0)
					break;
			default:
				break;
			}
		}
		else if (iOldProfessionYieldAmount - iNewProfessionYieldAmount > 0)
		{
			changeEuropeWarehouseYield(eYield, iOldProfessionYieldAmount - iNewProfessionYieldAmount);
		}
	}

	int iMunition = pUnit->getMunition();
	int iMaxMunition = kNewProfession.getMaxMunition();
	if (iMaxMunition > 0)
	{
		iMaxMunition = std::min(iMaxMunition - iMunition, getEuropeWarehouseYield(YIELD_AMMUNITION));
		pUnit->changeMunition(iMaxMunition);
		changeEuropeWarehouseYield(YIELD_AMMUNITION, -iMaxMunition);
	}
	else
	{
		pUnit->setMunition(0);
		changeEuropeWarehouseYield(YIELD_AMMUNITION, iMunition);
	}
	pUnit->setProfession(eNewProfession, true);
	gDLL->getInterfaceIFace()->setDirty(EuropeC3Screen_DIRTY_BIT, true);
}

bool CvPlayer::checkPopulation() const
{
	int iNumPopulation = 0;
	int iLoop;
	for (CvUnit* pLoopUnit = firstUnit(&iLoop); NULL != pLoopUnit; pLoopUnit = nextUnit(&iLoop))
	{
		if (pLoopUnit->getUnitInfo().isFound())
		{
			++iNumPopulation;
		}
	}
	for (CvCity* pLoopCity = firstCity(&iLoop); NULL != pLoopCity; pLoopCity = nextCity(&iLoop))
	{
		iNumPopulation += pLoopCity->getPopulation();
	}
	for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
	{
		if (m_aEuropeUnits[i]->getUnitInfo().isFound())
		{
			++iNumPopulation;
		}
	}

	return (iNumPopulation == getTotalPopulation());
}

bool CvPlayer::checkPower(bool bReset)
{
	int iPower = 0;
	int iAsset = 0;
	std::map<int, int> mapAreaPower;
	int iLoop;
	for (CvUnit* pUnit = firstUnit(&iLoop); pUnit != NULL; pUnit = nextUnit(&iLoop))
	{
		int iUnitPower = pUnit->getPower();
		iPower += iUnitPower;
		CvArea* pArea = pUnit->area();
		if (pArea != NULL)
		{
			mapAreaPower[pArea->getID()] += iUnitPower;
		}
		iAsset += pUnit->getAsset();
	}
	for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
	{
		iPower += m_aEuropeUnits[i]->getPower();
		iAsset += m_aEuropeUnits[i]->getAsset();
	}
	for (CvCity* pCity = firstCity(&iLoop); pCity != NULL; pCity = nextCity(&iLoop))
	{
		int iCityPower = 0;
		int iCityAsset = 0;
		for (int i = 0; i < pCity->getPopulation(); ++i)
		{
			iCityPower += pCity->getPopulationUnitByIndex(i)->getPower();
			iCityAsset += pCity->getPopulationUnitByIndex(i)->getAsset();
		}

		for (int i = 0; i < GC.getNumBuildingInfos(); ++i)
		{
			BuildingTypes eBuilding = (BuildingTypes) i;
			if (pCity->isHasBuilding(eBuilding))
			{
				iCityPower += GC.getBuildingInfo(eBuilding).getPowerValue();
				iCityAsset += GC.getBuildingInfo(eBuilding).getAssetValue();
			}
		}

		for (int i = 0; i < NUM_YIELD_TYPES; ++i)
		{
			YieldTypes eYield = (YieldTypes) i;
			iCityPower += pCity->getYieldStored(eYield) * GC.getYieldInfo(eYield).getPowerValue();
			iCityAsset += pCity->getYieldStored(eYield) * GC.getYieldInfo(eYield).getAssetValue();
		}

		iPower += iCityPower;
		iAsset += iCityAsset;
		mapAreaPower[pCity->area()->getID()] += iCityPower;
	}

	bool bCheck = true;
	if (iPower != getPower())
	{
		if (bReset)
		{
			changePower(iPower - getPower());
		}
		bCheck = false;
	}

	if (iAsset != getAssets())
	{
		if (bReset)
		{
			changeAssets(iAsset - getAssets());
		}
		bCheck = false;
	}

	for (CvArea* pArea = GC.getMapINLINE().firstArea(&iLoop); pArea != NULL; pArea = GC.getMapINLINE().nextArea(&iLoop))
	{
		if (mapAreaPower[pArea->getID()] != pArea->getPower(getID()))
		{
			if (bReset)
			{
				pArea->changePower(getID(), mapAreaPower[pArea->getID()] - pArea->getPower(getID()));
			}
			bCheck = false;
		}
	}

	return bCheck;
}

void CvPlayer::doREFReduction(int iGold)
{
	setRevolutionEuropeUnitThresholdMultiplier(getRevolutionEuropeUnitThresholdMultiplier() * (100 + iGold * GC.getHandicapInfo(getHandicapType()).getKingGoldThresholdPercent() / 100) / 100);
}

void CvPlayer::interceptEuropeUnits()
{
	if (!canTradeWithEurope())
	{
		std::vector<CvUnit*> apEuropeUnits;
		int iLoop;
		for (CvUnit* pUnit = firstUnit(&iLoop); pUnit != NULL; pUnit = nextUnit(&iLoop))
		{
			if (pUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_TO_EUROPE || pUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
			{
				if (!pUnit->isCargo())
				{
					apEuropeUnits.push_back(pUnit);
				}
			}
		}

		for (size_t i = 0; i < apEuropeUnits.size(); ++i)
		{
			CvUnit* pUnit = apEuropeUnits[i];
			CvPlot* pPlot = pUnit->plot();
			CvWString szMessage = gDLL->getText("TXT_KEY_YOU_UNITS_EUROPE_INTERCEPTED", pUnit->getNameOrProfessionKey());
			gDLL->getInterfaceIFace()->addMessage(getID(), true, GC.getEVENT_MESSAGE_TIME(), szMessage, "AS2D_COMBAT", MESSAGE_TYPE_DISPLAY_ONLY, pUnit->getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE(), true);
			pUnit->kill(false);
		}

		for (uint i = 0; i < m_aEuropeUnits.size(); ++i)
		{
			m_aEuropeUnits[i]->updateOwnerCache(-1);
		}
		freeEuropeUnits();
	}
}

void CvPlayer::transfertEuropeToWarehouse(YieldTypes eYield, int iCargoValue)
{
	PlayerTypes eEuropePlayer = getParent();
	if (eEuropePlayer == NO_PLAYER)
	{
		return;
	}
	CvPlayer& pEuropePlayer = GET_PLAYER(eEuropePlayer);

	if (eYield != NO_YIELD)
	{
		int iBuyPrice = getBuyPriceForYield(eYield, iCargoValue);
		int iSellPrice = getSellPriceForYield(eYield, -iCargoValue);
		pEuropePlayer.changeEuropeWarehouseYield(eYield, -iCargoValue);
		changeEuropeWarehouseYield(eYield, iCargoValue);
		if (iCargoValue > 0)
		{//can be neg if Transfert Warehouse to Europe
			changeEuropeBuyTradeYield(eYield, iCargoValue);
			changeEuropeGoldBuyTradeYield(eYield, iBuyPrice);
			changeYieldTradedTotal(eYield, iCargoValue);
			changeGold(-iBuyPrice);
			CvWStringBuffer szMessage;
			GAMETEXT.setEuropeYieldBoughtHelp(szMessage, *this, eYield, iCargoValue);
			m_aszEuropeTradeMessages.push_back(szMessage.getCString());
		}
		else
		{
			int iTaxSellGold = iSellPrice* getTaxRate() / 100;
			int iSellGold = iSellPrice - iTaxSellGold;
			int iLoan = getEuropeLoan();
			int iPrice2 = iSellPrice*getEuropeLoanPercent()/100;
			iPrice2 = std::min(iLoan, iPrice2);
			changeEuropeLoan(-iPrice2);
			iSellGold -= iPrice2;

			changeEuropeTaxTradeYield(eYield, iTaxSellGold);
			changeEuropeSellTradeYield(eYield, -iCargoValue);
			changeEuropeGoldSellTradeYield(eYield, iSellGold);
			changeYieldTradedTotal(eYield, -iCargoValue);
			pEuropePlayer.changeYieldBoughtTotal(eYield, -iCargoValue);
			changeGold(iSellGold);

			CvWStringBuffer szMessage;
			GAMETEXT.setEuropeYieldSoldHelp(szMessage, *this, eYield, -iCargoValue, getTaxRate() + getEuropeLoanPercent());
			m_aszEuropeTradeMessages.push_back(szMessage.getCString());

		}
		gDLL->getInterfaceIFace()->setDirty(EuropeC2Screen_DIRTY_BIT, true);
	}
}

bool CvPlayer::hasEnoughDefendersByColony(int iNumDefenderByColony) const
{
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop)) 
	{
		if (pLoopCity->getNumDefenders() < iNumDefenderByColony) 
		{
			return false;
		}
	}
	return getNumCities() > 0;// IF No colony return false
}

bool CvPlayer::hasEnoughCitizenByColony(int iNumCitizenByColony) const
{
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		if (pLoopCity->getPopulation() < iNumCitizenByColony)
		{
			return false;
		}
	}
	return getNumCities() > 0;// IF No colony return false
}

//Event Callbacks
void CvPlayer::kingGiveScout()
{
	//The king give us an scout
	if (getParent() == NO_PLAYER)
	{
		return;
	}

	int iTurn = GC.getGameINLINE().getGameTurn();

	if (iTurn != 8 + getRandomValues(0) % 10)
	{
		return;
	}

	UnitTypes eUnitScout = NO_UNIT;
	int iRandScout = GC.getGameINLINE().getSorenRandNum(100, "iRandScout");
	//A small prob. of having an expert scout
	if (iRandScout < 7)
	{
		eUnitScout = UNIT_SCOUT;
	}
	else
	{ //Otherwise use default unit
		eUnitScout = (UnitTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(GC.getDefineINT("DEFAULT_POPULATION_UNIT"));
	}

	if (eUnitScout != NO_UNIT)
	{
		CvPlayer& kKingPlayer = GET_PLAYER(getParent());
		CvUnit* pUnit = buyEuropeUnit(eUnitScout, 0);
		if (pUnit != NULL)
		{
			if (isHuman())
			{
				CvDiploParameters* pDiplo = new CvDiploParameters(kKingPlayer.getID());
				pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLO_KING_GIVE_SCOUT"));
				pDiplo->setAIContact(true);
				gDLL->beginDiplomacy(pDiplo, getID());
			}
			pUnit->setLikelyToLeave(false); //The scout will not abandon the docks...
			pUnit->setProfession(PROFESSION_SCOUT, true);
		}
	}
}
bool CvPlayer::haveAlreadySawSeeds() const
{
	int iLoop;
	for (CvCity* pLoopCity = firstCity(&iLoop); pLoopCity != NULL; pLoopCity = nextCity(&iLoop))
	{
		for (int iPlot = 0; iPlot < NUM_CITY_PLOTS; ++iPlot)
		{
			if (iPlot != CITY_HOME_PLOT)
			{
				CvPlot* pLoopPlot = plotCity(pLoopCity->getX_INLINE(), pLoopCity->getY_INLINE(), iPlot);
				if (pLoopPlot != NULL)
				{
					if (pLoopPlot->getPlotCity() != NULL)
					{
						continue;
					}
					BonusTypes eBonus = pLoopPlot->getBonusType();
					if (eBonus != NULL)
					{
						if (pLoopPlot->getImprovementLevel() > 0)
						{
							return false;
						}
					}
				}
			}
		}
	}
	return true;
}
//END DOANE

int CvPlayer::getNumCityJobs() const
{
	return (int)(m_cityJobs.size());
}

CvCityJob* CvPlayer::getCityJob(int iID) const
{
	CvCityJob* pJob = m_cityJobs.getById(iID);

	// Hack to fix pointers issues :'(
	// TODO Fix this issue

	if (pJob != NULL)
	{
		CvCity* pCity = ::getCity(pJob->getCity());
		if (pCity != NULL)
		{
			return pCity->getCityJobById(pJob->getID());
		}
	}
	return NULL;
}

CvCityJob* CvPlayer::addCityJob()
{
	return m_cityJobs.addNew();
}

int CvPlayer::getNumMarketTransactions() const
{
	return (int)(m_marketTransactions.size());
}

CvMarketTransaction* CvPlayer::getMarketTransaction(int iID) const
{
	return m_marketTransactions.getById(iID);
}

CvMarketTransaction* CvPlayer::getMarketTransaction(YieldTypes eYield, PlayerTypes eToPlayer) const
{
	if (m_mappedMarketTransactions.empty()) 
	{
		CvIdVector<CvMarketTransaction>::const_iterator it;
		for (it = m_marketTransactions.begin(); it != m_marketTransactions.end(); ++it)
		{
			CvMarketTransaction* marketTransaction = it->second;
			m_mappedMarketTransactions[std::make_pair(marketTransaction->getYield(), marketTransaction->getToPlayer())] = marketTransaction;
		}
	}

	return m_mappedMarketTransactions[std::make_pair(eYield, eToPlayer)]; 
}

void CvPlayer::initMarketTransactions()
{
	if (!isEurope())
	{
		return;
	}

	for (int iI = 0; iI < MAX_PLAYERS; iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER((PlayerTypes)iI);
		if (kPlayer.isEurope() && kPlayer.isAlive() && kPlayer.getID() != getID())
		{
			for (int i = 0; i < NUM_YIELD_TYPES; i++)
			{
				YieldTypes eYield = (YieldTypes) i;
				CvYieldInfo& kYield = GC.getYieldInfo(eYield);
		
				if (kYield.isRawMaterial())
				{
					CvMarketTransaction* kMarketTransaction = m_marketTransactions.addNew();
					kMarketTransaction->init(kPlayer.getID(), eYield);
				}
			}
		}
	}
	
}

void CvPlayer::haveNewEra(NewEraTypes eNewEra, bool hasReachedMaxTurnRequirementThanksToAnotherEuropean)
{
	PlayerTypes eParent = getParent();
	if (eParent == NO_PLAYER) 
	{
		return;
	}

	CvPlayer& kParent = GET_PLAYER(eParent);
	CvTeam& kParenTeam = GET_TEAM(kParent.getTeam());
	CvNewEraInfo& kNewEra = GC.getNewEraInfo(eNewEra);
	kParenTeam.setNewEra(eNewEra);

	if (kNewEra.changeProfession()) 
	{
		upgradeMilitaryProfessions();
	}

	if (getNextNewEra() != NO_NEW_ERA) 
	{
		//We don't have to highlight the new eras button for the last new era
		setHighlightNewErasButton(true);
	}
	VictoryTypes eVictory = GC.getGameINLINE().getVictoryTarget();
	NewEraTypes eVictoryNewEra = NO_NEW_ERA;
	if (eVictory != NO_VICTORY)
	{
		eVictoryNewEra = (NewEraTypes) GC.getVictoryInfo(eVictory).getTargetNewEra();
	}

	if (getTeam() == GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getTeam())
	{
		TeamTypes eWinner = GC.getGameINLINE().getWinner();
		bool bShoudDisplayPopup = eVictoryNewEra != eNewEra || (eWinner != NO_TEAM && eWinner != getTeam());
		if (bShoudDisplayPopup) 
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_WHEN_NEW_ERA, eNewEra, hasReachedMaxTurnRequirementThanksToAnotherEuropean);
			gDLL->getInterfaceIFace()->addPopup(pInfo, getID(), true);
		}	
	}
}

void CvPlayer::upgradeMilitaryProfessions()
{
	CvUnit* pLoopUnit;
	int iLoop;
	for (pLoopUnit = firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = nextUnit(&iLoop))
	{
		if (pLoopUnit->getProfession() == PROFESSION_ARQUEBUSIER || pLoopUnit->getProfession() == PROFESSION_SOLDIER)
		{
			if (getCapitalCity() != NULL)
			{
				/* We want to know if pLoopUnit doen't have any experience*/
				if (pLoopUnit->getUnitInfo().getCombat() == 0 && pLoopUnit->getExperience() == 0)
				{
					pLoopUnit->setProfession(PROFESSION_PATRIOTS);
				}
				else
				{
					pLoopUnit->setProfession(PROFESSION_SOLDIER);
				}
			}
			else
			{
				pLoopUnit->setProfession(PROFESSION_SOLDIER);
			}
		}
		if (pLoopUnit->getProfession() == PROFESSION_MOUNTED_ARQUEBUSIER)
		{
			pLoopUnit->setProfession(PROFESSION_DRAGOON);
		}
	}
}

int CvPlayer::getNumAgreementProposition(PlayerTypes eEuropenPlayerType) const
{
	if (!isNative())
	{
		return 0;
	}

	CvPlayer& kEuropeanPlayer = GET_PLAYER(eEuropenPlayerType);
	if (!kEuropeanPlayer.isEuropean())
	{
		return 0;
	}

	int iRelationTrade = getRelationTradeTo(eEuropenPlayerType); // Between -15 and 15

	return std::max(1, (iRelationTrade + 1) / 2);
}


int CvPlayer::getAgreementQuantity(int iProposition) const
{
	return (40 + 5*(getRandomValues(0) % 4)) * iProposition;
}

int CvPlayer::getAgreementMaxTurn(int iProposition) const
{
	return 10 * iProposition;
}

UnitTypes CvPlayer::getCheapestShip() const
{
	int iLowestCost = MAX_INT;
	UnitTypes eCheapestShip = NO_UNIT;

	for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass)
	{
		UnitTypes eLoopUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(iUnitClass);
		if (eLoopUnit != NO_UNIT && GC.getUnitInfo(eLoopUnit).isMechUnit())
		{
			int iCost = getEuropeUnitBuyPrice(eLoopUnit);
			if (iCost < iLowestCost && iCost >= 0)
			{
				iLowestCost = iCost;
				eCheapestShip = eLoopUnit;
			}
		}
	}

	return eCheapestShip;
}

void CvPlayer::processYieldTransactions(std::vector<PlayerTypes>& aliveEuropePlayer, YieldTypes eYield)
{
	int iStock = getEuropeWarehouseYield(eYield);
	int iEuropeConsumption = getEuropeConsumption(eYield);
	int iChange = std::min(iStock, iEuropeConsumption);
	// Europe King consumes what he needs
	changeEuropeWarehouseYield(eYield, -iChange);

	// We keep enough yield for us.
	int iYieldToTrade = getEuropeWarehouseYield(eYield) - iEuropeConsumption;
	if (iYieldToTrade <= 0)
	{
		// If not enough to trade we skip.
		saveYieldTransactions(aliveEuropePlayer, eYield);
		return;
	}

	int iTotalPlayers = 0;
	int iTotalNeeds = 0;
	CvTeam& kTeam = GET_TEAM(getTeam());

	for (std::vector<PlayerTypes>::iterator it = aliveEuropePlayer.begin(); it != aliveEuropePlayer.end(); ++it)
	{
		CvPlayer& kEuropePlayer = GET_PLAYER(*it);
		if (kEuropePlayer.getID() == getID())
		{
			continue;
		}

		if (kTeam.isAtWar(kEuropePlayer.getTeam())) 
		{
			continue;
		}

		int iYieldNeeds = kEuropePlayer.getEuropeConsumption(eYield) - kEuropePlayer.getEuropeWarehouseYield(eYield);
		if (iYieldNeeds > 0)
		{
			iTotalNeeds += iYieldNeeds;
			iTotalPlayers++;
		}
	}

	if (iTotalPlayers == 0) 
	{
		// No one needs this yield.
		saveYieldTransactions(aliveEuropePlayer, eYield);
		return;
	}

	int iExceed = std::max(0, (iYieldToTrade - iTotalNeeds) / 2 ) / iTotalPlayers; // we calculate the exceed that we will transfer to other europen (we keep the half fo us (/ 2) )
	int iTradePercent = 100 - std::max(0, (iTotalNeeds - iYieldToTrade) * 100 / iTotalNeeds); // If we do not have enough yield to trade.

	saveYieldTransactions(aliveEuropePlayer, eYield, iExceed, iTradePercent);
}


void CvPlayer::saveYieldTransactions(std::vector<PlayerTypes>& aliveEuropePlayer, YieldTypes eYield, int iExceed, int iTradePercent)
{
	CvTeam& kTeam = GET_TEAM(getTeam());

	for (std::vector<PlayerTypes>::iterator it = aliveEuropePlayer.begin(); it != aliveEuropePlayer.end(); ++it)
	{
		CvPlayer& kEuropePlayer = GET_PLAYER(*it);
		if (kEuropePlayer.getID() == getID())
		{
			continue;
		}
		CvMarketTransaction* kMarketTransaction = getMarketTransaction(eYield, kEuropePlayer.getID());
		if (kMarketTransaction != NULL)
		{
			int iYieldToTrade  = 0;
			bool isAtWar = kTeam.isAtWar(kEuropePlayer.getTeam());
			if (!isAtWar)
			{
				if (iExceed >= 0 && iTradePercent >= 0)
				{
					int iYieldNeeds = kEuropePlayer.getEuropeConsumption(eYield) - kEuropePlayer.getEuropeWarehouseYield(eYield);
					if (iYieldNeeds > 0)
					{
						iYieldToTrade = iYieldNeeds * iTradePercent / 100 + iExceed;
					}
				}
			}
			kMarketTransaction->setIsAtWar(isAtWar);
			kMarketTransaction->addTradeAmountHistory(iYieldToTrade);
		}
	}
}

void CvPlayer::applyYieldsTransactions()
{
	CvIdVector<CvMarketTransaction>::const_iterator it;
	for (it = m_marketTransactions.begin(); it != m_marketTransactions.end(); ++it)
	{
		CvMarketTransaction* marketTransaction = it->second;
		int iTradeAmount = marketTransaction->getTradeAmount();
		if (iTradeAmount <= 0)
		{
			continue;
		}

		CvPlayer& kToPlayer = GET_PLAYER(marketTransaction->getToPlayer());
		if (!kToPlayer.isAlive())
		{
			continue;
		}
		YieldTypes eYield = marketTransaction->getYield();

		kToPlayer.changeEuropeWarehouseYield(eYield, iTradeAmount);
		changeEuropeWarehouseYield(eYield, -iTradeAmount);
		// TODO Europe could earn money in this transaction. See later.
	}
}

int CvPlayer::isMarketTransactionBlocked(YieldTypes eYield, PlayerTypes eToPlayer) const
{
	CvMarketTransaction* marketTransaction = getMarketTransaction(eYield, eToPlayer);
	return marketTransaction != NULL ? marketTransaction->isAtWar() : false;
}

int CvPlayer::getEuropeTradeAmount(YieldTypes eYield, PlayerTypes eToPlayer) const
{
	int iTradeAmount = 0;
	CvMarketTransaction* marketTransaction = getMarketTransaction(eYield, eToPlayer);
	if (marketTransaction != NULL)
	{
		iTradeAmount = marketTransaction->getTradeAmount();
	}

	
	return std::max(0, iTradeAmount);
}

int CvPlayer::getEuropeTradeTotalAmount(YieldTypes eYield, PlayerTypes eToPlayer) const
{
	int iTradeAmount = 0;
	CvMarketTransaction* marketTransaction = getMarketTransaction(eYield, eToPlayer);
	if (marketTransaction != NULL)
	{
		iTradeAmount = marketTransaction->getEuropeTradeTotalAmount();
	}

	return std::max(0, iTradeAmount);
}

CvConvoy* CvPlayer::addConvoy()
{
	CvConvoy* pConvoy = m_convoys.addNew();
	pConvoy->init(pConvoy->getID(), getID());
	return pConvoy;
}

bool CvPlayer::removeConvoy(int iId)
{
	CvConvoy* pConvoy = getConvoy(iId);
	FAssert(pConvoy != NULL);
	if (pConvoy == NULL)
	{
		return false;
	}
	bool bResult = m_convoys.removeById(iId);
	return bResult;
}

CvConvoy* CvPlayer::getConvoy(int iId) const
{
	return m_convoys.getById(iId);
}

int CvPlayer::getNumConvoys() const
{
	return m_convoys.size();
}

void CvPlayer::getConvoys(std::vector<CvConvoy*>& aConvoys) const
{
	CvIdVector<CvConvoy>::const_iterator it;
	for (it = m_convoys.begin(); it != m_convoys.end(); ++it)
	{
		aConvoys.push_back(it->second);
	}
}

CvConvoy* CvPlayer::firstConvoy(int *pIterIdx) const
{
	if (m_convoys.empty())
	{
		return NULL;
	}

	CvIdVector<CvConvoy>::const_iterator it = m_convoys.begin();
	CvConvoy* pConvoy = it->second;

	++it;
	if (it == m_convoys.end())
	{
		*pIterIdx = -1;
	}
	else
	{
		*pIterIdx = it->first;
	}

	return pConvoy;
}


CvConvoy* CvPlayer::nextConvoy(int *pIterIdx) const
{
	CvIdVector<CvConvoy>::const_iterator it = m_convoys.find(*pIterIdx);

	if (it == m_convoys.end())
	{
		return NULL;
	}

	CvConvoy* pConvoy = it->second;

	++it;
	if (it == m_convoys.end())
	{
		*pIterIdx = -1;
	}
	else
	{
		*pIterIdx = it->first;
	}

	return pConvoy;
}