#pragma once

#ifndef CVCONVOY_H
#define CVCONVOY_H

class CvUnit;
class CvTradeRoute;


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  CLASS:      CvConvoy
//!  \brief		Handle units inside a convoy
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//
// Class:		CvConvoy
// Purpose:		Manages units inside a confoy
class CvConvoy
{
	friend class CvGlobals;
public:
	DllExport CvConvoy();
	DllExport virtual ~CvConvoy();
	void init(int iID, PlayerTypes eOwner);
	void reset(int iID = 0, PlayerTypes eOwner = NO_PLAYER);

	int getID() const;
	void setID(int iId);

	PlayerTypes getOwner() const;
	DomainTypes getDomainType() const;
	int getCargoNewSpace() const;
	int getLoadedCargoPercent() const;
	int getLoadedCargoHistPercent() const;	
	int getTradeProfits() const;
	int getTradeProfitsHist() const;
	
	unsigned char getLoadedCargoPercent(int iRotation) const;
	void setLoadedCargoPercent(int iRotation, unsigned char ucNewValue);
	int getTradeProfits(int iRotation) const;
	void setTradeProfits(int iRotation, int iTradeProfits);
	void processTravelRotation(int iAmount, int iMacCargoAmount, int iProfits);
	int getTravelRotation() const;
	void incrementTravelRotation();
	
	bool hasProcessedTravelRotation() const;
	void setProcessedTravelRotation(bool bNewValue);

	void doTurn();

	int getCurrentTurnLoadedAmount() const;
	void setCurrentTurnLoadedAmount(int iCurrentTurnLoadedAmount);
	void changeCurrentTurnLoadedAmount(int iChange);
	
	int getCurrentTurnMaxCargoAmount() const;
	void setCurrentTurnMaxCargoAmount(int iCurrentTurnMaxCargoAmount);
	void changeCurrentTurnMaxCargoAmount(int iChange);

	int getCurrentTurnTradeProfitsAmount() const;
	void setCurrentTurnTradeProfitsAmount(int iCurrentTurnLoadedAmount);
	void changeCurrentTurnTradeProfitsAmount(int iChange);

	int getAssignedTradeRouteId() const;	
	CvTradeRoute* getAssignedTradeRoute() const;
	void setAssignedTradeRouteId(int iTradeRouteId);
	CvCity* getDestinationCity() const;

	int getNumUnits() const;
	void addUnit(CvUnit* pUnit);
	void removeUnit(CvUnit* pUnit, bool bRemoveLink);
	bool hasUnit(int iUnitId) const;
	CvUnit* getUnit(IDInfo idInfo) const;
	CLLNode<IDInfo>* headUnitNode() const;
	CLLNode<IDInfo>* nextUnitNode(CLLNode<IDInfo>* pNode) const;
	void detach();

	void calculateRequiredUnits(int& requiredMerchantUnits, int& requiredMilitaryUnits, DomainTypes domainType, bool bCheckLoading = true, UnitTypes eTargetUnit = NO_UNIT) const;
	void countUnitByType(int& countMerchantUnits, int& countMilitaryUnits, UnitTypes eBestMerchantType, UnitTypes eBestMilitaryType) const;
	CvUnit* findWorstUnit(bool isOnlyDefensive, UnitTypes eBestUnitType) const;
	int getNewCargoYield(YieldTypes eYield) const;
	bool isFullCapacity() const;

	void read(FDataStreamBase* pStream);
	void write(FDataStreamBase* pStream);
	
	static const int MAX_FLEET_MILITARY_UNITS = 3;
	static const int MAX_FLEET_MERCHANT_UNITS = 7;
	static const int MAX_WAGON_MILITARY_UNITS = 0;
	static const int MAX_WAGON_MERCHANT_UNITS = 1;
	static const int EUROPE_CITY_ID = -1;

protected:
	int m_iId;
	int m_iTravelRotation;
	int m_iCurrentTurnLoadedAmount;
	int m_iCurrentTurnMaxCargoAmount;
	int m_iCurrentTurnTradeProfits;

	bool m_bHasProcessedTravelRotation;

	PlayerTypes m_eOwner;
	int m_iAssignedTradeRouteId;
	CLinkList<IDInfo> m_units;

	unsigned char* m_aucLoadedCargoPercent;
	int* m_aiTradeProfits;
};

#endif
