//
// Python wrapper class for CvSeaway
//
//
#include "CvGameCoreDLL.h"
#include "CvSeaway.h"
#include "CySeaway.h"

CySeaway::CySeaway() : m_pSeaway(NULL)
{
}
CySeaway::CySeaway(CvSeaway* pSeaway) : m_pSeaway(pSeaway)
{
}

int CySeaway::getID()
{
	return m_pSeaway ? m_pSeaway->getID() : -1;
}

std::wstring CySeaway::getName()
{
	return m_pSeaway ? m_pSeaway->getName() : std::wstring();
}

void CySeaway::setName(std::wstring szNewValue)
{
	if (m_pSeaway)
		m_pSeaway->setName(szNewValue);
}

CyPlot* CySeaway::plot()
{
	return m_pSeaway ? new CyPlot(m_pSeaway->plot()) : NULL;
}
int CySeaway::stepDistanceToPlot(CyPlot* pPlot)
{
	return m_pSeaway ? m_pSeaway->stepDistanceToPlot(pPlot ? pPlot->getPlot() : NULL) : -1;
}

bool CySeaway::isKnownTo(int /*PlayerTypes*/ ePlayer)
{
	return m_pSeaway ? m_pSeaway->isKnownTo((PlayerTypes)ePlayer) : false;
}
