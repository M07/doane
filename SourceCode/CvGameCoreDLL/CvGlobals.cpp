//
// globals.cpp
// Author -	Mustafa Thamer
//
#include "CvGameCoreDLL.h"
#include "CvGlobals.h"
#include "CvRandom.h"
#include "CvGameAI.h"
#include "CvDLLInterfaceIFaceBase.h"
#include "CvMap.h"
#include "CvPlayerAI.h"
#include "CvTeamAI.h"
#include "CvInfos.h"
#include "CvDLLUtilityIFaceBase.h"
#include "CvArtFileMgr.h"
#include "CvDLLXMLIFaceBase.h"
#include "CvPlayerAI.h"
#include "CvInfoWater.h"
#include "CvGameTextMgr.h"
#include "FProfiler.h"
#include "FVariableSystem.h"
#include "CvInitCore.h"

#define COPY(dst, src, typeName) \
	{ \
		int iNum = sizeof(src)/sizeof(typeName); \
		dst = new typeName[iNum]; \
		for (int i =0;i<iNum;i++) \
			dst[i] = src[i]; \
	}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

CvGlobals gGlobals;

//
// CONSTRUCTOR
//
CvGlobals::CvGlobals() :
m_bGraphicsInitialized(false),
m_bLogging(false),
m_bRandLogging(false),
m_bOverwriteLogs(false),
m_bSynchLogging(false),
m_pkMainMenu(NULL),
m_iNewPlayers(0),
m_bZoomOut(false),
m_bZoomIn(false),
m_bLoadGameFromFile(false),
m_pFMPMgr(NULL),
m_asyncRand(NULL),
m_interface(NULL),
m_game(NULL),
m_messageQueue(NULL),
m_hotJoinMsgQueue(NULL),
m_messageControl(NULL),
m_messageCodes(NULL),
m_dropMgr(NULL),
m_portal(NULL),
m_setupData(NULL),
m_initCore(NULL),
m_statsReporter(NULL),
m_map(NULL),
m_diplomacyScreen(NULL),
m_mpDiplomacyScreen(NULL),
m_pathFinder(NULL),
m_interfacePathFinder(NULL),
m_stepFinder(NULL),
m_routeFinder(NULL),
m_borderFinder(NULL),
m_areaFinder(NULL),
m_pDLL(NULL),
m_aiPlotDirectionX(NULL),
m_aiPlotDirectionY(NULL),
m_aiPlotCardinalDirectionX(NULL),
m_aiPlotCardinalDirectionY(NULL),
m_aiCityPlotX(NULL),
m_aiCityPlotY(NULL),
m_aiCityPlotPriority(NULL),
m_aeTurnLeftDirection(NULL),
m_aeTurnRightDirection(NULL),
m_VarSystem(NULL),
m_iMOVE_DENOMINATOR(0),
m_iFOOD_CONSUMPTION_PER_POPULATION(0),
m_iMAX_HIT_POINTS(0),
m_iHILLS_EXTRA_DEFENSE(0),
m_iRIVER_ATTACK_MODIFIER(0),
m_iAMPHIB_ATTACK_MODIFIER(0),
m_iHILLS_EXTRA_MOVEMENT(0),
m_iPEAK_EXTRA_MOVEMENT(0),
m_iMAX_PLOT_LIST_ROWS(0),
m_iUNIT_MULTISELECT_MAX(0),
m_iEVENT_MESSAGE_TIME(0),
m_iROUTE_FEATURE_GROWTH_MODIFIER(0),
m_iFEATURE_GROWTH_MODIFIER(0),
m_iMIN_CITY_RANGE(0),
m_iCITY_MAX_NUM_BUILDINGS(0),
m_iLAKE_MAX_AREA_SIZE(0),
m_iMIN_WATER_SIZE_FOR_OCEAN(0),
m_iFORTIFY_MODIFIER_PER_TURN(0),
m_iMAX_CITY_DEFENSE_DAMAGE(0),
m_iPEAK_SEE_THROUGH_CHANGE(0),
m_iHILLS_SEE_THROUGH_CHANGE(0),
m_iSEAWATER_SEE_FROM_CHANGE(0),
m_iPEAK_SEE_FROM_CHANGE(0),
m_iHILLS_SEE_FROM_CHANGE(0),
m_iMAX_REBEL_YIELD_MODIFIER(0),
m_fCAMERA_MIN_YAW(0),
m_fCAMERA_MAX_YAW(0),
m_fCAMERA_FAR_CLIP_Z_HEIGHT(0),
m_fCAMERA_MAX_TRAVEL_DISTANCE(0),
m_fCAMERA_START_DISTANCE(0),
m_fPLOT_SIZE(0),
m_fCAMERA_SPECIAL_PITCH(0),
m_fCAMERA_MAX_TURN_OFFSET(0),
m_fCAMERA_MIN_DISTANCE(0),
m_fCAMERA_UPPER_PITCH(0),
m_fCAMERA_LOWER_PITCH(0),
m_fFIELD_OF_VIEW(0),
m_fUNIT_MULTISELECT_DISTANCE(0),
///////////////////////////////////////////////////////////////
////////*		//DOANE Cache | Speed Improvement		*//////
///////////////////////////////////////////////////////////////
//DOANE Delayed Cache: Don't change default value, cache has to be called at least once!
m_bDelayedCacheInitialized(false),
//DOANE Remove Cache: Don't change default value, cache has to be cleaned only once!
m_bCacheGlobalsClean(true),
//DOANE Enrol Unit
m_ucMAX_ENROL_UNIT(0),
//DOANE Enrol Unit
m_ucMAX_MILITARY_PROFESSION_TURN(0),
m_ucSTART_MILITARY_PROFESSION_PENALTY(0),
//DOANE Fortification
m_ucFORTIFICATION_BREAK_DAMAGE(0),
m_ucPOWERFUL_FORTIFICATION_PENALTY(0),
//DOANE AI help
m_iGOLD_BY_POPULATION_BY_TURN(0),
m_iNUM_DISCOVERED_MAP_TILES_BEFORE_DELIVERING(0),
//DOANE Economy
m_iMAX_MANUFACTURED_YIELD_AMOUNT(0),
//DOANE Builder Pack
m_aiBuilderPackYieldRequire(NULL),
m_ucBUILDER_PACK_YIELD_COST_PERCENT(0),
m_ucBuilderPackToolCost(0),
m_ucBuilderPackLumberCost(0),
//DOANE Survivors on Transport Sinking
m_ucSURVIVORS_ON_TRANSPORT_SINKING_PROBABILITY(0),
//DoaNE num history turn for trade routes stats
m_ucNUM_TRADE_ROUTE_HIST_TURN(0),
//DoaNE  Percentage of boost building construction if current era is above of required era for building	
m_ucAVANCED_NEW_ERA_BOOST_FOR_CONSTRUCTION(0),
//DoaNE Immigration
m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_AVAILABLE(0),
m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_DOCK(0),
m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_CARGO(0),
m_ucPROBABILITY_NATURAL_IMMIGRATION_START(0),
m_ucPROBABILITY_NATURAL_IMMIGRATION_LATE(0),
m_ucPROBABILITY_NATURAL_IMMIGRATION_PERCENT(0),
m_iTURN_NATURAL_IMMIGRATION_CHANGE(0),
//DoaNE Disturbation
m_ucNAVAL_TRANSPORT_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY(0),
m_ucNAVAL_MILITARY_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY(0),
m_ucNAVAL_DISTURBATION_FOR_TRADE_ROUTE(0),
m_ucNAVAL_DISTURBATION_FOR_MILITARY_SHIP_KILLED_PROTECTING_A_CITY(0),
//Income and Expenditures
m_ucEXPENDITURE_BY_LAND_FORCE_INSIDE_COLONIES(0),
m_ucEXPENDITURE_BY_LAND_FORCE_OUTSIDE_COLONIES(0),
m_ucEXPENDITURE_BY_UNEMPLOYED(0),
m_ucEXPENDITURE_BY_SCOUT(0),
m_ucEXPENDITURE_BY_AGRONOMIST(0),
m_ucEXPENDITURE_BY_PIONEER(0),
m_ucEXPENDITURE_BY_MERCHANT_SHIP(0),
m_ucEXPENDITURE_BY_MILITARY_SHIP(0),


///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////*	//DOANE Python Callbacks | Speed improvement:	02/08/2012								///////
////////	Speed improvements for the mod by turning python calls in the SDK into python callbacks	///////
////////	so we could turn them off and save the game engine time. Migration from int to bool.	///////
////////																							///////
////////	*Changed Callback values from int to boolean. Initial value False.						///////
////////	*Converted all the "getUse..." functions to inline func.								///////
////////	*Modified schema: "CIV4GlobalDefinesSchema.xml"	new "bDefineBoolVal" boolean values.	///////
////////	*Changed "PythonCallbackDefines.xml" int defines with new "bDefineBoolVal" defines.		///////
////////	*Analized which callbacks are used outside the DLL and changed header file accordingly.	///////
////////																							///////
////////	WARNING: There are some callbacks actually used by the "exe" so it isn't possible to	///////
////////	modify them because the game doesn't load, this applies to DLL and schema changes.		*//////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//Vanilla Callbacks
m_bUSE_CAN_FOUND_CITIES_ON_WATER_CALLBACK(false),
m_bUSE_CANNOT_DO_CIVIC_CALLBACK(false),
m_bUSE_CAN_DO_CIVIC_CALLBACK(false),
m_bUSE_CANNOT_CONSTRUCT_CALLBACK(false),
m_bUSE_CAN_CONSTRUCT_CALLBACK(false),
m_bUSE_CAN_DECLARE_WAR_CALLBACK(false),
m_bUSE_GET_UNIT_COST_MOD_CALLBACK(false),
m_bUSE_GET_CITY_FOUND_VALUE_CALLBACK(false),
m_bUSE_CANNOT_HANDLE_ACTION_CALLBACK(false),
m_bUSE_CAN_BUILD_CALLBACK(false),
m_bUSE_CANNOT_TRAIN_CALLBACK(false),
m_bUSE_CAN_TRAIN_CALLBACK(false),
m_bUSE_UNIT_CANNOT_MOVE_INTO_CALLBACK(false),
m_iUSE_FINISH_TEXT_CALLBACK(0), //Warning: this is used by the exe!
m_iUSE_ON_UNIT_SET_XY_CALLBACK(0), //Warning: this is used by the exe!
m_iUSE_ON_UNIT_SELECTED_CALLBACK(0), //Warning: this is used by the exe!
m_iUSE_ON_MISSIONARY_CONVERTED_UNIT_CALLBACK(0), //Warning: this is used by the exe!
m_iUSE_ON_UPDATE_CALLBACK(0), //Warning: this is used by the exe!
m_iUSE_ON_UNIT_CREATED_CALLBACK(0), //Warning: this is used by the exe!
m_iUSE_ON_UNIT_LOST_CALLBACK(0), //Warning: this is used by the exe!
//CvCity
m_bUSE_ON_CITY_DO_GROWTH_CALLBACK(false),
m_bUSE_ON_CITIES_DESTROY_FEATURES_CALLBACK(false),
m_bUSE_ON_DO_CULTURE_CALLBACK(false),
m_bUSE_ON_DO_PLOT_CULTURE_CALLBACK(false),
m_bUSE_ON_DO_PRODUCTION_CALLBACK(false),
//CvCityAI
m_bUSE_AI_CHOOSE_PRODUCTION_CALLBACK(false),
//CvGame
m_bUSE_ON_UPDATE_COLORED_PLOTS_CALLBACK(false),
m_bUSE_CAN_NOT_SELECTION_LIST_MOVE_CALLBACK(false),
m_bUSE_CAN_NOT_SELECTION_LIST_GAME_NET_MESSAGE_CALLBACK(false),
m_bUSE_CAN_NOT_DO_CONTROL_CALLBACK(false),
m_bUSE_ON_DO_REVIVE_ACTIVE_PLAYER_CALLBACK(false),
m_bUSE_ON_CHECK_IS_VICTORY_CALLBACK(false),
m_bUSE_ON_CHECK_IS_VICTORY_TEST_CALLBACK(false),
//CvDLLButtonPopup
m_bUSE_ON_SKIP_PRODUCTION_POPUP_CALLBACK(false),
m_bUSE_ON_SHOW_EXAMINE_CITY_BUTTON_CALLBACK(false),
m_bUSE_GET_RECOMMENDED_UNIT_CALLBACK(false),
m_bUSE_GET_RECOMMENDED_BUILDING_CALLBACK(false),
//CvPlayer
m_bUSE_ON_DO_CITY_CAPTURE_GOLD_CALLBACK(false),
m_bUSE_CAN_RAZE_CITY_CALLBACK(false),
m_bUSE_ON_DO_GOLD_CALLBACK(false),
//CvPlayerAI
m_bUSE_AI_DO_DIPLO_CALLBACK(false),
//CvSelectionGroup
m_bUSE_ON_DO_COMBAT_CALLBACK(false),
//CvTeamAI
m_bUSE_AI_DO_WAR_CALLBACK(false),
//CvUnit
m_bUSE_ON_IS_ACTION_RECOMMENDED_CALLBACK(false),
m_bUSE_ON_DO_PILLAGE_GOLD_CALLBACK(false),
m_bUSE_GET_UPGRADE_PRICE_OVERRIDE_CALLBACK(false),
m_bUSE_GET_EXPERIENCE_NEEDED_CALLBACK(false),
//CvUnitAI
m_bUSE_AI_UNIT_UPDATE_CALLBACK(false),
///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////*									//END DOANE												*//////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

m_paHints(NULL),
m_paMainMenus(NULL)
{
}

CvGlobals::~CvGlobals()
{
}


/************************************************************************************************/
/* MINIDUMP_MOD                           04/10/11                                terkhen       */
/*                                                                                              */
/* See http://www.debuginfo.com/articles/effminidumps.html                                      */
/************************************************************************************************/

#include <dbghelp.h>
#pragma comment (lib, "dbghelp.lib")

void CreateMiniDump(EXCEPTION_POINTERS *pep)
{
	/* Open a file to store the minidump. */
	HANDLE hFile = CreateFile(_T("MiniDump.dmp"),
	                          GENERIC_READ | GENERIC_WRITE,
	                          0,
	                          NULL,
	                          CREATE_ALWAYS,
	                          FILE_ATTRIBUTE_NORMAL,
	                          NULL);

	if ((hFile == NULL) || (hFile == INVALID_HANDLE_VALUE))
{
		_tprintf(_T("CreateFile failed. Error: %u \n"), GetLastError());
		return;
	}
	/* Create the minidump. */
	MINIDUMP_EXCEPTION_INFORMATION mdei;

	mdei.ThreadId           = GetCurrentThreadId();
	mdei.ExceptionPointers  = pep;
	mdei.ClientPointers     = FALSE;

	MINIDUMP_TYPE mdt       = MiniDumpNormal;

	BOOL result = MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(),
	                                hFile,
	                                mdt,
	                                (pep != NULL) ? &mdei : NULL,
	                                NULL,
	                                NULL);

	/* Close the file. */
	CloseHandle(hFile);
}

LONG WINAPI CustomFilter(EXCEPTION_POINTERS *ExceptionInfo)
{
	CreateMiniDump(ExceptionInfo);
	return EXCEPTION_EXECUTE_HANDLER;
}

/************************************************************************************************/
/* MINIDUMP_MOD                                END                                              */
/************************************************************************************************/


//
// allocate
//
void CvGlobals::init()
{
/************************************************************************************************/
/* MINIDUMP_MOD                           04/10/11                                terkhen       */
/************************************************************************************************/

	/* Enable our custom exception that will write the minidump for us. */
	SetUnhandledExceptionFilter(CustomFilter);

/************************************************************************************************/
/* MINIDUMP_MOD                                END                                              */
/************************************************************************************************/
	//
	// These vars are used to initialize the globals.
	//

	int aiPlotDirectionX[NUM_DIRECTION_TYPES] =
	{
		0,	// DIRECTION_NORTH
		1,	// DIRECTION_NORTHEAST
		1,	// DIRECTION_EAST
		1,	// DIRECTION_SOUTHEAST
		0,	// DIRECTION_SOUTH
		-1,	// DIRECTION_SOUTHWEST
		-1,	// DIRECTION_WEST
		-1,	// DIRECTION_NORTHWEST
	};

	int aiPlotDirectionY[NUM_DIRECTION_TYPES] =
	{
		1,	// DIRECTION_NORTH
		1,	// DIRECTION_NORTHEAST
		0,	// DIRECTION_EAST
		-1,	// DIRECTION_SOUTHEAST
		-1,	// DIRECTION_SOUTH
		-1,	// DIRECTION_SOUTHWEST
		0,	// DIRECTION_WEST
		1,	// DIRECTION_NORTHWEST
	};

	int aiPlotCardinalDirectionX[NUM_CARDINALDIRECTION_TYPES] =
	{
		0,	// CARDINALDIRECTION_NORTH
		1,	// CARDINALDIRECTION_EAST
		0,	// CARDINALDIRECTION_SOUTH
		-1,	// CARDINALDIRECTION_WEST
	};

	int aiPlotCardinalDirectionY[NUM_CARDINALDIRECTION_TYPES] =
	{
		1,	// CARDINALDIRECTION_NORTH
		0,	// CARDINALDIRECTION_EAST
		-1,	// CARDINALDIRECTION_SOUTH
		0,	// CARDINALDIRECTION_WEST
	};

	int aiCityPlotX[NUM_CITY_PLOTS] =
	{
		0,
		0, 1, 1, 1, 0,-1,-1,-1,
//		0, 1, 2, 2, 2, 1, 0,-1,-2,-2,-2,-1,
	};

	int aiCityPlotY[NUM_CITY_PLOTS] =
	{
		0,
		1, 1, 0,-1,-1,-1, 0, 1,
//		2, 2, 1, 0,-1,-2,-2,-2,-1, 0, 1, 2,
	};

	int aiCityPlotPriority[NUM_CITY_PLOTS] =
	{
		0,
		1, 2, 1, 2, 1, 2, 1, 2,
//		3, 4, 4, 3, 4, 4, 3, 4, 4, 3, 4, 4,
	};

	int aaiXYCityPlot[CITY_PLOTS_DIAMETER][CITY_PLOTS_DIAMETER] =
	{
		{6, 7, 8,},

		{5, 0, 1,},

		{4, 3, 2,}
	};

	DirectionTypes aeTurnRightDirection[NUM_DIRECTION_TYPES] =
	{
		DIRECTION_NORTHEAST,	// DIRECTION_NORTH
		DIRECTION_EAST,				// DIRECTION_NORTHEAST
		DIRECTION_SOUTHEAST,	// DIRECTION_EAST
		DIRECTION_SOUTH,			// DIRECTION_SOUTHEAST
		DIRECTION_SOUTHWEST,	// DIRECTION_SOUTH
		DIRECTION_WEST,				// DIRECTION_SOUTHWEST
		DIRECTION_NORTHWEST,	// DIRECTION_WEST
		DIRECTION_NORTH,			// DIRECTION_NORTHWEST
	};

	DirectionTypes aeTurnLeftDirection[NUM_DIRECTION_TYPES] =
	{
		DIRECTION_NORTHWEST,	// DIRECTION_NORTH
		DIRECTION_NORTH,			// DIRECTION_NORTHEAST
		DIRECTION_NORTHEAST,	// DIRECTION_EAST
		DIRECTION_EAST,				// DIRECTION_SOUTHEAST
		DIRECTION_SOUTHEAST,	// DIRECTION_SOUTH
		DIRECTION_SOUTH,			// DIRECTION_SOUTHWEST
		DIRECTION_SOUTHWEST,	// DIRECTION_WEST
		DIRECTION_WEST,				// DIRECTION_NORTHWEST
	};

	DirectionTypes aaeXYDirection[DIRECTION_DIAMETER][DIRECTION_DIAMETER] =
	{
		DIRECTION_SOUTHWEST, DIRECTION_WEST,	DIRECTION_NORTHWEST,
		DIRECTION_SOUTH,     NO_DIRECTION,    DIRECTION_NORTH,
		DIRECTION_SOUTHEAST, DIRECTION_EAST,	DIRECTION_NORTHEAST,
	};

	FAssertMsg(gDLL != NULL, "Civ app needs to set gDLL");

	m_VarSystem = new FVariableSystem;
	m_asyncRand = new CvRandom;
	m_initCore = new CvInitCore;
	m_loadedInitCore = new CvInitCore;
	m_iniInitCore = new CvInitCore;

	gDLL->initGlobals();	// some globals need to be allocated outside the dll

	m_game = new CvGameAI;
	m_map = new CvMap;

	//DOANE Cache
	cacheAtStart();
	//END DOANE

	CvPlayerAI::initStatics();
	CvTeamAI::initStatics();

	m_pt3Origin = NiPoint3(0.0f, 0.0f, 0.0f);

	COPY(m_aiPlotDirectionX, aiPlotDirectionX, int);
	COPY(m_aiPlotDirectionY, aiPlotDirectionY, int);
	COPY(m_aiPlotCardinalDirectionX, aiPlotCardinalDirectionX, int);
	COPY(m_aiPlotCardinalDirectionY, aiPlotCardinalDirectionY, int);
	COPY(m_aiCityPlotX, aiCityPlotX, int);
	COPY(m_aiCityPlotY, aiCityPlotY, int);
	COPY(m_aiCityPlotPriority, aiCityPlotPriority, int);
	COPY(m_aeTurnLeftDirection, aeTurnLeftDirection, DirectionTypes);
	COPY(m_aeTurnRightDirection, aeTurnRightDirection, DirectionTypes);
	memcpy(m_aaiXYCityPlot, aaiXYCityPlot, sizeof(m_aaiXYCityPlot));
	memcpy(m_aaeXYDirection, aaeXYDirection,sizeof(m_aaeXYDirection));
}

//
// free
//
void CvGlobals::uninit()
{
	//
	// See also CvXMLLoadUtilityInit.cpp::CleanUpGlobalVariables()
	//
	SAFE_DELETE_ARRAY(m_aiPlotDirectionX);
	SAFE_DELETE_ARRAY(m_aiPlotDirectionY);
	SAFE_DELETE_ARRAY(m_aiPlotCardinalDirectionX);
	SAFE_DELETE_ARRAY(m_aiPlotCardinalDirectionY);
	SAFE_DELETE_ARRAY(m_aiCityPlotX);
	SAFE_DELETE_ARRAY(m_aiCityPlotY);
	SAFE_DELETE_ARRAY(m_aiCityPlotPriority);
	SAFE_DELETE_ARRAY(m_aeTurnLeftDirection);
	SAFE_DELETE_ARRAY(m_aeTurnRightDirection);

	//DOANE Cache
	uninitCacheAtStart();
	//END DOANE

	CvPlayerAI::freeStatics();
	CvTeamAI::freeStatics();

	SAFE_DELETE(m_game);
	SAFE_DELETE(m_map);

	SAFE_DELETE(m_asyncRand);
	SAFE_DELETE(m_initCore);
	SAFE_DELETE(m_loadedInitCore);
	SAFE_DELETE(m_iniInitCore);
	gDLL->uninitGlobals();	// free globals allocated outside the dll
	SAFE_DELETE(m_VarSystem);

	// already deleted outside of the dll, set to null for safety
	m_messageQueue=NULL;
	m_hotJoinMsgQueue=NULL;
	m_messageControl=NULL;
	m_setupData=NULL;
	m_messageCodes=NULL;
	m_dropMgr=NULL;
	m_portal=NULL;
	m_statsReporter=NULL;
	m_interface=NULL;
	m_diplomacyScreen=NULL;
	m_mpDiplomacyScreen=NULL;
	m_pathFinder=NULL;
	m_interfacePathFinder=NULL;
	m_stepFinder=NULL;
	m_routeFinder=NULL;
	m_borderFinder=NULL;
	m_areaFinder=NULL;

	deleteInfoArrays();
}

void CvGlobals::clearTypesMap()
{
	infoTypeFromStringReset();
	if (m_VarSystem)
	{
		m_VarSystem->UnInit();
	}
}


CvDiplomacyScreen* CvGlobals::getDiplomacyScreen()
{
	return m_diplomacyScreen;
}

CMPDiplomacyScreen* CvGlobals::getMPDiplomacyScreen()
{
	return m_mpDiplomacyScreen;
}

CvMessageCodeTranslator& CvGlobals::getMessageCodes()
{
	return *m_messageCodes;
}

FMPIManager*& CvGlobals::getFMPMgrPtr()
{
	return m_pFMPMgr;
}

CvPortal& CvGlobals::getPortal()
{
	return *m_portal;
}

CvSetupData& CvGlobals::getSetupData()
{
	return *m_setupData;
}

CvInitCore& CvGlobals::getInitCore()
{
	return *m_initCore;
}

CvInitCore& CvGlobals::getLoadedInitCore()
{
	return *m_loadedInitCore;
}

CvInitCore& CvGlobals::getIniInitCore()
{
	return *m_iniInitCore;
}

CvStatsReporter& CvGlobals::getStatsReporter()
{
	return *m_statsReporter;
}

CvStatsReporter* CvGlobals::getStatsReporterPtr()
{
	return m_statsReporter;
}

CvInterface& CvGlobals::getInterface()
{
	return *m_interface;
}

CvInterface* CvGlobals::getInterfacePtr()
{
	return m_interface;
}

CvRandom& CvGlobals::getASyncRand()
{
	return *m_asyncRand;
}

CMessageQueue& CvGlobals::getMessageQueue()
{
	return *m_messageQueue;
}

CMessageQueue& CvGlobals::getHotMessageQueue()
{
	return *m_hotJoinMsgQueue;
}

CMessageControl& CvGlobals::getMessageControl()
{
	return *m_messageControl;
}

CvDropMgr& CvGlobals::getDropMgr()
{
	return *m_dropMgr;
}

FAStar& CvGlobals::getPathFinder()
{
	return *m_pathFinder;
}

FAStar& CvGlobals::getInterfacePathFinder()
{
	return *m_interfacePathFinder;
}

FAStar& CvGlobals::getStepFinder()
{
	return *m_stepFinder;
}

FAStar& CvGlobals::getRouteFinder()
{
	return *m_routeFinder;
}

FAStar& CvGlobals::getBorderFinder()
{
	return *m_borderFinder;
}

FAStar& CvGlobals::getAreaFinder()
{
	return *m_areaFinder;
}

NiPoint3& CvGlobals::getPt3Origin()
{
	return m_pt3Origin;
}

std::vector<CvInterfaceModeInfo*>& CvGlobals::getInterfaceModeInfo()		// For Moose - XML Load Util and CvInfos
{
	return m_paInterfaceModeInfo;
}

CvInterfaceModeInfo& CvGlobals::getInterfaceModeInfo(InterfaceModeTypes e)
{
	FAssert(e > -1);
	FAssert(e < NUM_INTERFACEMODE_TYPES);
	return *(m_paInterfaceModeInfo[e]);
}

NiPoint3& CvGlobals::getPt3CameraDir()
{
	return m_pt3CameraDir;
}

bool& CvGlobals::getLogging()
{
	return m_bLogging;
}

bool& CvGlobals::getRandLogging()
{
	return m_bRandLogging;
}

bool& CvGlobals::getSynchLogging()
{
	return m_bSynchLogging;
}

bool& CvGlobals::overwriteLogs()
{
	return m_bOverwriteLogs;
}

int* CvGlobals::getPlotDirectionX()
{
	return m_aiPlotDirectionX;
}

int* CvGlobals::getPlotDirectionY()
{
	return m_aiPlotDirectionY;
}

int* CvGlobals::getPlotCardinalDirectionX()
{
	return m_aiPlotCardinalDirectionX;
}

int* CvGlobals::getPlotCardinalDirectionY()
{
	return m_aiPlotCardinalDirectionY;
}

int* CvGlobals::getCityPlotX()
{
	return m_aiCityPlotX;
}

int* CvGlobals::getCityPlotY()
{
	return m_aiCityPlotY;
}

int* CvGlobals::getCityPlotPriority()
{
	return m_aiCityPlotPriority;
}

int CvGlobals::getXYCityPlot(int i, int j)
{
	FAssertMsg(i < CITY_PLOTS_DIAMETER, "Index out of bounds");
	FAssertMsg(i > -1, "Index out of bounds");
	FAssertMsg(j < CITY_PLOTS_DIAMETER, "Index out of bounds");
	FAssertMsg(j > -1, "Index out of bounds");
	return m_aaiXYCityPlot[i][j];
}

DirectionTypes* CvGlobals::getTurnLeftDirection()
{
	return m_aeTurnLeftDirection;
}

DirectionTypes CvGlobals::getTurnLeftDirection(int i)
{
	FAssertMsg(i < NUM_DIRECTION_TYPES, "Index out of bounds");
	FAssertMsg(i > -1, "Index out of bounds");
	return m_aeTurnLeftDirection[i];
}

DirectionTypes* CvGlobals::getTurnRightDirection()
{
	return m_aeTurnRightDirection;
}

DirectionTypes CvGlobals::getTurnRightDirection(int i)
{
	FAssertMsg(i < NUM_DIRECTION_TYPES, "Index out of bounds");
	FAssertMsg(i > -1, "Index out of bounds");
	return m_aeTurnRightDirection[i];
}

DirectionTypes CvGlobals::getXYDirection(int i, int j)
{
	FAssertMsg(i < DIRECTION_DIAMETER, "Index out of bounds");
	FAssertMsg(i > -1, "Index out of bounds");
	FAssertMsg(j < DIRECTION_DIAMETER, "Index out of bounds");
	FAssertMsg(j > -1, "Index out of bounds");
	return m_aaeXYDirection[i][j];
}

int CvGlobals::getNumWorldInfos()
{
	return (int)m_paWorldInfo.size();
}

std::vector<CvWorldInfo*>& CvGlobals::getWorldInfo()
{
	return m_paWorldInfo;
}

CvWorldInfo& CvGlobals::getWorldInfo(WorldSizeTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumWorldInfos());
	return *(m_paWorldInfo[e]);
}

/////////////////////////////////////////////
// CLIMATE
/////////////////////////////////////////////

int CvGlobals::getNumClimateInfos()
{
	return (int)m_paClimateInfo.size();
}

std::vector<CvClimateInfo*>& CvGlobals::getClimateInfo()
{
	return m_paClimateInfo;
}

CvClimateInfo& CvGlobals::getClimateInfo(ClimateTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumClimateInfos());
	return *(m_paClimateInfo[e]);
}

/////////////////////////////////////////////
// SEALEVEL
/////////////////////////////////////////////

int CvGlobals::getNumSeaLevelInfos()
{
	return (int)m_paSeaLevelInfo.size();
}

std::vector<CvSeaLevelInfo*>& CvGlobals::getSeaLevelInfo()
{
	return m_paSeaLevelInfo;
}

CvSeaLevelInfo& CvGlobals::getSeaLevelInfo(SeaLevelTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumSeaLevelInfos());
	return *(m_paSeaLevelInfo[e]);
}

/////////////////////////////////////////////
// EUROPE
/////////////////////////////////////////////

int CvGlobals::getNumEuropeInfos()
{
	return (int)m_paEuropeInfo.size();
}

std::vector<CvEuropeInfo*>& CvGlobals::getEuropeInfo()
{
	return m_paEuropeInfo;
}

CvEuropeInfo& CvGlobals::getEuropeInfo(EuropeTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumEuropeInfos());
	return *(m_paEuropeInfo[e]);
}

int CvGlobals::getNumHints()
{
	return (int)m_paHints.size();
}

std::vector<CvInfoBase*>& CvGlobals::getHints()
{
	return m_paHints;
}

CvInfoBase& CvGlobals::getHints(int i)
{
	return *(m_paHints[i]);
}

int CvGlobals::getNumMainMenus()
{
	return (int)m_paMainMenus.size();
}

std::vector<CvMainMenuInfo*>& CvGlobals::getMainMenus()
{
	return m_paMainMenus;
}

CvMainMenuInfo& CvGlobals::getMainMenus(int i)
{
	if (i >= getNumMainMenus())
	{
		return *(m_paMainMenus[0]);
	}

	return *(m_paMainMenus[i]);
}

int CvGlobals::getNumColorInfos()
{
	return (int)m_paColorInfo.size();
}

std::vector<CvColorInfo*>& CvGlobals::getColorInfo()
{
	return m_paColorInfo;
}

CvColorInfo& CvGlobals::getColorInfo(ColorTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumColorInfos());
	return *(m_paColorInfo[e]);
}


int CvGlobals::getNumPlayerColorInfos()
{
	return (int)m_paPlayerColorInfo.size();
}

std::vector<CvPlayerColorInfo*>& CvGlobals::getPlayerColorInfo()
{
	return m_paPlayerColorInfo;
}

CvPlayerColorInfo& CvGlobals::getPlayerColorInfo(PlayerColorTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumPlayerColorInfos());
	return *(m_paPlayerColorInfo[e]);
}

int CvGlobals::getNumRouteModelInfos()
{
	return (int)m_paRouteModelInfo.size();
}

std::vector<CvRouteModelInfo*>& CvGlobals::getRouteModelInfo()
{
	return m_paRouteModelInfo;
}

CvRouteModelInfo& CvGlobals::getRouteModelInfo(int i)
{
	FAssert(i > -1);
	FAssert(i < GC.getNumRouteModelInfos());
	return *(m_paRouteModelInfo[i]);
}

int CvGlobals::getNumRiverModelInfos()
{
	return (int)m_paRiverModelInfo.size();
}

std::vector<CvRiverModelInfo*>& CvGlobals::getRiverModelInfo()
{
	return m_paRiverModelInfo;
}

CvRiverModelInfo& CvGlobals::getRiverModelInfo(int i)
{
	FAssert(i > -1);
	FAssert(i < GC.getNumRiverModelInfos());
	return *(m_paRiverModelInfo[i]);
}

int CvGlobals::getNumWaterPlaneInfos()
{
	return (int)m_paWaterPlaneInfo.size();
}

std::vector<CvWaterPlaneInfo*>& CvGlobals::getWaterPlaneInfo()
{
	return m_paWaterPlaneInfo;
}

CvWaterPlaneInfo& CvGlobals::getWaterPlaneInfo(int i)
{
	FAssert(i > -1);
	FAssert(i < GC.getNumWaterPlaneInfos());
	return *(m_paWaterPlaneInfo[i]);
}

int CvGlobals::getNumTerrainPlaneInfos()
{
	return (int)m_paTerrainPlaneInfo.size();
}

std::vector<CvTerrainPlaneInfo*>& CvGlobals::getTerrainPlaneInfo()
{
	return m_paTerrainPlaneInfo;
}

CvTerrainPlaneInfo& CvGlobals::getTerrainPlaneInfo(int i)
{
	FAssert(i > -1);
	FAssert(i < GC.getNumTerrainPlaneInfos());
	return *(m_paTerrainPlaneInfo[i]);
}

int CvGlobals::getNumCameraOverlayInfos()
{
	return (int)m_paCameraOverlayInfo.size();
}

std::vector<CvCameraOverlayInfo*>& CvGlobals::getCameraOverlayInfo()
{
	return m_paCameraOverlayInfo;
}

CvCameraOverlayInfo& CvGlobals::getCameraOverlayInfo(int i)
{
	FAssert(i > -1);
	FAssert(i < GC.getNumCameraOverlayInfos());
	return *(m_paCameraOverlayInfo[i]);
}

int CvGlobals::getNumAnimationPathInfos()
{
	return (int)m_paAnimationPathInfo.size();
}

std::vector<CvAnimationPathInfo*>& CvGlobals::getAnimationPathInfo()
{
	return m_paAnimationPathInfo;
}

CvAnimationPathInfo& CvGlobals::getAnimationPathInfo(AnimationPathTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumAnimationPathInfos());
	return *(m_paAnimationPathInfo[e]);
}

int CvGlobals::getNumAnimationCategoryInfos()
{
	return (int)m_paAnimationCategoryInfo.size();
}

std::vector<CvAnimationCategoryInfo*>& CvGlobals::getAnimationCategoryInfo()
{
	return m_paAnimationCategoryInfo;
}

CvAnimationCategoryInfo& CvGlobals::getAnimationCategoryInfo(AnimationCategoryTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumAnimationCategoryInfos());
	return *(m_paAnimationCategoryInfo[e]);
}

int CvGlobals::getNumEntityEventInfos()
{
	return (int)m_paEntityEventInfo.size();
}

std::vector<CvEntityEventInfo*>& CvGlobals::getEntityEventInfo()
{
	return m_paEntityEventInfo;
}

CvEntityEventInfo& CvGlobals::getEntityEventInfo(EntityEventTypes e)
{
	FAssert( e > -1 );
	FAssert( e < GC.getNumEntityEventInfos() );
	return *(m_paEntityEventInfo[e]);
}

int CvGlobals::getNumEffectInfos()
{
	return (int)m_paEffectInfo.size();
}

std::vector<CvEffectInfo*>& CvGlobals::getEffectInfo()
{
	return m_paEffectInfo;
}

CvEffectInfo& CvGlobals::getEffectInfo(int i)
{
	FAssert(i > -1);
	FAssert(i < GC.getNumEffectInfos());
	return *(m_paEffectInfo[i]);
}


int CvGlobals::getNumAttachableInfos()
{
	return (int)m_paAttachableInfo.size();
}

std::vector<CvAttachableInfo*>& CvGlobals::getAttachableInfo()
{
	return m_paAttachableInfo;
}

CvAttachableInfo& CvGlobals::getAttachableInfo(int i)
{
	FAssert(i > -1);
	FAssert(i < GC.getNumAttachableInfos());
	return *(m_paAttachableInfo[i]);
}

int CvGlobals::getNumUnitFormationInfos()
{
	return (int)m_paUnitFormationInfo.size();
}

std::vector<CvUnitFormationInfo*>& CvGlobals::getUnitFormationInfo()		// For Moose - CvUnitEntity
{
	return m_paUnitFormationInfo;
}

CvUnitFormationInfo& CvGlobals::getUnitFormationInfo(int i)
{
	FAssert(i > -1);
	FAssert(i < GC.getNumUnitFormationInfos());
	return *(m_paUnitFormationInfo[i]);
}

// TEXT
int CvGlobals::getNumGameTextXML()
{
	return (int)m_paGameTextXML.size();
}

std::vector<CvGameText*>& CvGlobals::getGameTextXML()
{
	return m_paGameTextXML;
}

// Landscape INFOS
int CvGlobals::getNumLandscapeInfos()
{
	return (int)m_paLandscapeInfo.size();
}

std::vector<CvLandscapeInfo*>& CvGlobals::getLandscapeInfo()
{
	return m_paLandscapeInfo;
}

CvLandscapeInfo& CvGlobals::getLandscapeInfo(int iIndex)
{
	FAssert(iIndex > -1);
	FAssert(iIndex < GC.getNumLandscapeInfos());
	return *(m_paLandscapeInfo[iIndex]);
}

int CvGlobals::getActiveLandscapeID()
{
	return m_iActiveLandscapeID;
}

void CvGlobals::setActiveLandscapeID(int iLandscapeID)
{
	m_iActiveLandscapeID = iLandscapeID;
}


int CvGlobals::getNumTerrainInfos()
{
	return (int)m_paTerrainInfo.size();
}

std::vector<CvTerrainInfo*>& CvGlobals::getTerrainInfo()		// For Moose - XML Load Util, CvInfos, CvTerrainTypeWBPalette
{
	return m_paTerrainInfo;
}

CvTerrainInfo& CvGlobals::getTerrainInfo(TerrainTypes eTerrainNum)
{
	FAssert(eTerrainNum > -1);
	FAssert(eTerrainNum < (int)m_paTerrainInfo.size());
	return *(m_paTerrainInfo[eTerrainNum]);
}

int CvGlobals::getNumBonusInfos()
{
	return (int)m_paBonusInfo.size();
}

std::vector<CvBonusInfo*>& CvGlobals::getBonusInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paBonusInfo;
}

CvBonusInfo& CvGlobals::getBonusInfo(BonusTypes eBonusNum)
{
	FAssert(eBonusNum > -1);
	FAssert(eBonusNum < (int)m_paBonusInfo.size());
	return *(m_paBonusInfo[eBonusNum]);
}

int CvGlobals::getNumFeatureInfos()
{
	return (int)m_paFeatureInfo.size();
}

std::vector<CvFeatureInfo*>& CvGlobals::getFeatureInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paFeatureInfo;
}

CvFeatureInfo& CvGlobals::getFeatureInfo(FeatureTypes eFeatureNum)
{
	FAssert(eFeatureNum > -1);
	FAssert(eFeatureNum < (int)m_paFeatureInfo.size());
	return *(m_paFeatureInfo[eFeatureNum]);
}

int CvGlobals::getNumCivilizationInfos()
{
	return (int)m_paCivilizationInfo.size();
}

std::vector<CvCivilizationInfo*>& CvGlobals::getCivilizationInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paCivilizationInfo;
}

CvCivilizationInfo& CvGlobals::getCivilizationInfo(CivilizationTypes eCivilizationNum)
{
	FAssert(eCivilizationNum > -1);
	FAssert(eCivilizationNum < (int)m_paCivilizationInfo.size());
	return *(m_paCivilizationInfo[eCivilizationNum]);
}


int CvGlobals::getNumLeaderHeadInfos()
{
	return (int)m_paLeaderHeadInfo.size();
}

std::vector<CvLeaderHeadInfo*>& CvGlobals::getLeaderHeadInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paLeaderHeadInfo;
}

CvLeaderHeadInfo& CvGlobals::getLeaderHeadInfo(LeaderHeadTypes eLeaderHeadNum)
{
	FAssert(eLeaderHeadNum > -1);
	FAssert(eLeaderHeadNum < GC.getNumLeaderHeadInfos());
	return *(m_paLeaderHeadInfo[eLeaderHeadNum]);
}


int CvGlobals::getNumTraitInfos()
{
	return (int)m_paTraitInfo.size();
}

std::vector<CvTraitInfo*>& CvGlobals::getTraitInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paTraitInfo;
}

CvTraitInfo& CvGlobals::getTraitInfo(TraitTypes eTraitNum)
{
	FAssert(eTraitNum > -1);
	FAssert(eTraitNum < GC.getNumTraitInfos());
	return *(m_paTraitInfo[eTraitNum]);
}


int CvGlobals::getNumCursorInfos()
{
	return (int)m_paCursorInfo.size();
}

std::vector<CvCursorInfo*>& CvGlobals::getCursorInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paCursorInfo;
}

CvCursorInfo& CvGlobals::getCursorInfo(CursorTypes eCursorNum)
{
	FAssert(eCursorNum > -1);
	FAssert(eCursorNum < GC.getNumCursorInfos());
	return *(m_paCursorInfo[eCursorNum]);
}

int CvGlobals::getNumSlideShowInfos()
{
	return (int)m_paSlideShowInfo.size();
}

std::vector<CvSlideShowInfo*>& CvGlobals::getSlideShowInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paSlideShowInfo;
}

CvSlideShowInfo& CvGlobals::getSlideShowInfo(int iIndex)
{
	FAssert(iIndex > -1);
	FAssert(iIndex < GC.getNumSlideShowInfos());
	return *(m_paSlideShowInfo[iIndex]);
}

int CvGlobals::getNumSlideShowRandomInfos()
{
	return (int)m_paSlideShowRandomInfo.size();
}

std::vector<CvSlideShowRandomInfo*>& CvGlobals::getSlideShowRandomInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paSlideShowRandomInfo;
}

CvSlideShowRandomInfo& CvGlobals::getSlideShowRandomInfo(int iIndex)
{
	FAssert(iIndex > -1);
	FAssert(iIndex < GC.getNumSlideShowRandomInfos());
	return *(m_paSlideShowRandomInfo[iIndex]);
}

int CvGlobals::getNumWorldPickerInfos()
{
	return (int)m_paWorldPickerInfo.size();
}

std::vector<CvWorldPickerInfo*>& CvGlobals::getWorldPickerInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paWorldPickerInfo;
}

CvWorldPickerInfo& CvGlobals::getWorldPickerInfo(int iIndex)
{
	FAssert(iIndex > -1);
	FAssert(iIndex < GC.getNumWorldPickerInfos());
	return *(m_paWorldPickerInfo[iIndex]);
}

int CvGlobals::getNumUnitInfos()
{
	return (int)m_paUnitInfo.size();
}

std::vector<CvUnitInfo*>& CvGlobals::getUnitInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paUnitInfo;
}

CvUnitInfo& CvGlobals::getUnitInfo(UnitTypes eUnitNum)
{
	FAssert(eUnitNum > -1);
	FAssert(eUnitNum < GC.getNumUnitInfos());
	return *(m_paUnitInfo[eUnitNum]);
}

int CvGlobals::getNumSpecialUnitInfos()
{
	return (int)m_paSpecialUnitInfo.size();
}

std::vector<CvSpecialUnitInfo*>& CvGlobals::getSpecialUnitInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paSpecialUnitInfo;
}

CvSpecialUnitInfo& CvGlobals::getSpecialUnitInfo(SpecialUnitTypes eSpecialUnitNum)
{
	FAssert(eSpecialUnitNum > -1);
	FAssert(eSpecialUnitNum < GC.getNumSpecialUnitInfos());
	return *(m_paSpecialUnitInfo[eSpecialUnitNum]);
}


int CvGlobals::getNumConceptInfos()
{
	return (int)m_paConceptInfo.size();
}

std::vector<CvInfoBase*>& CvGlobals::getConceptInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paConceptInfo;
}

CvInfoBase& CvGlobals::getConceptInfo(ConceptTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumConceptInfos());
	return *(m_paConceptInfo[e]);
}
int CvGlobals::getNumCalendarInfos()
{
	return (int)m_paCalendarInfo.size();
}

std::vector<CvInfoBase*>& CvGlobals::getCalendarInfo()
{
	return m_paCalendarInfo;
}

CvInfoBase& CvGlobals::getCalendarInfo(CalendarTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumCalendarInfos());
	return *(m_paCalendarInfo[e]);
}


int CvGlobals::getNumSeasonInfos()
{
	return (int)m_paSeasonInfo.size();
}

std::vector<CvInfoBase*>& CvGlobals::getSeasonInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paSeasonInfo;
}

CvInfoBase& CvGlobals::getSeasonInfo(SeasonTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumSeasonInfos());
	return *(m_paSeasonInfo[e]);
}


int CvGlobals::getNumMonthInfos()
{
	return (int)m_paMonthInfo.size();
}

std::vector<CvInfoBase*>& CvGlobals::getMonthInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paMonthInfo;
}

CvInfoBase& CvGlobals::getMonthInfo(MonthTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumMonthInfos());
	return *(m_paMonthInfo[e]);
}


int CvGlobals::getNumDenialInfos()
{
	return (int)m_paDenialInfo.size();
}

std::vector<CvInfoBase*>& CvGlobals::getDenialInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paDenialInfo;
}

CvInfoBase& CvGlobals::getDenialInfo(DenialTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumDenialInfos());
	return *(m_paDenialInfo[e]);
}


int CvGlobals::getNumInvisibleInfos()
{
	return (int)m_paInvisibleInfo.size();
}

std::vector<CvInfoBase*>& CvGlobals::getInvisibleInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paInvisibleInfo;
}

CvInfoBase& CvGlobals::getInvisibleInfo(InvisibleTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumInvisibleInfos());
	return *(m_paInvisibleInfo[e]);
}


int CvGlobals::getNumFatherInfos()
{
	return (int)m_paFatherInfo.size();
}

std::vector<CvFatherInfo*>& CvGlobals::getFatherInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paFatherInfo;
}

CvFatherInfo& CvGlobals::getFatherInfo(FatherTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumFatherInfos());
	return *(m_paFatherInfo[e]);
}

int CvGlobals::getNumFatherPointInfos()
{
	return (int)m_paFatherPointInfo.size();
}

std::vector<CvFatherPointInfo*>& CvGlobals::getFatherPointInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paFatherPointInfo;
}

CvFatherPointInfo& CvGlobals::getFatherPointInfo(FatherPointTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumFatherPointInfos());
	return *(m_paFatherPointInfo[e]);
}

int CvGlobals::getNumUnitCombatInfos()
{
	return (int)m_paUnitCombatInfo.size();
}

std::vector<CvInfoBase*>& CvGlobals::getUnitCombatInfo()
{
	return m_paUnitCombatInfo;
}

CvInfoBase& CvGlobals::getUnitCombatInfo(UnitCombatTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumUnitCombatInfos());
	return *(m_paUnitCombatInfo[e]);
}


std::vector<CvInfoBase*>& CvGlobals::getDomainInfo()
{
	return m_paDomainInfo;
}

CvInfoBase& CvGlobals::getDomainInfo(DomainTypes e)
{
	FAssert(e > -1);
	FAssert(e < NUM_DOMAIN_TYPES);
	return *(m_paDomainInfo[e]);
}


std::vector<CvInfoBase*>& CvGlobals::getUnitAIInfo()
{
	return m_paUnitAIInfos;
}

CvInfoBase& CvGlobals::getUnitAIInfo(UnitAITypes eUnitAINum)
{
	FAssert(eUnitAINum >= 0);
	FAssert(eUnitAINum < NUM_UNITAI_TYPES);
	return *(m_paUnitAIInfos[eUnitAINum]);
}

int CvGlobals::getNumFatherCategoryInfos()
{
	return (int)m_paFatherCategoryInfos.size();
}

std::vector<CvInfoBase*>& CvGlobals::getFatherCategoryInfo()
{
	return m_paFatherCategoryInfos;
}

CvInfoBase& CvGlobals::getFatherCategoryInfo(FatherCategoryTypes eFatherCategoryNum)
{
	FAssert(eFatherCategoryNum >= 0);
	FAssert(eFatherCategoryNum < GC.getNumFatherCategoryInfos());
	return *(m_paFatherCategoryInfos[eFatherCategoryNum]);
}

int CvGlobals::getNumGameOptionInfos()
{
	return (int)m_paGameOptionInfos.size();
}

std::vector<CvGameOptionInfo*>& CvGlobals::getGameOptionInfo()
{
	return m_paGameOptionInfos;
}

CvGameOptionInfo& CvGlobals::getGameOptionInfo(GameOptionTypes eGameOptionNum)
{
	FAssert(eGameOptionNum >= 0);
	FAssert(eGameOptionNum < GC.getNumGameOptionInfos());
	return *(m_paGameOptionInfos[eGameOptionNum]);
}

int CvGlobals::getNumMPOptionInfos()
{
	return (int)m_paMPOptionInfos.size();
}

std::vector<CvMPOptionInfo*>& CvGlobals::getMPOptionInfo()
{
	 return m_paMPOptionInfos;
}

CvMPOptionInfo& CvGlobals::getMPOptionInfo(MultiplayerOptionTypes eMPOptionNum)
{
	FAssert(eMPOptionNum >= 0);
	FAssert(eMPOptionNum < GC.getNumMPOptionInfos());
	return *(m_paMPOptionInfos[eMPOptionNum]);
}

int CvGlobals::getNumForceControlInfos()
{
	return (int)m_paForceControlInfos.size();
}

std::vector<CvForceControlInfo*>& CvGlobals::getForceControlInfo()
{
	return m_paForceControlInfos;
}

CvForceControlInfo& CvGlobals::getForceControlInfo(ForceControlTypes eForceControlNum)
{
	FAssert(eForceControlNum >= 0);
	FAssert(eForceControlNum < GC.getNumForceControlInfos());
	return *(m_paForceControlInfos[eForceControlNum]);
}

std::vector<CvPlayerOptionInfo*>& CvGlobals::getPlayerOptionInfo()
{
	return m_paPlayerOptionInfos;
}

CvPlayerOptionInfo& CvGlobals::getPlayerOptionInfo(PlayerOptionTypes ePlayerOptionNum)
{
	FAssert(ePlayerOptionNum >= 0);
	FAssert(ePlayerOptionNum < NUM_PLAYEROPTION_TYPES);
	return *(m_paPlayerOptionInfos[ePlayerOptionNum]);
}

std::vector<CvGraphicOptionInfo*>& CvGlobals::getGraphicOptionInfo()
{
	return m_paGraphicOptionInfos;
}

CvGraphicOptionInfo& CvGlobals::getGraphicOptionInfo(GraphicOptionTypes eGraphicOptionNum)
{
	FAssert(eGraphicOptionNum >= 0);
	FAssert(eGraphicOptionNum < NUM_GRAPHICOPTION_TYPES);
	return *(m_paGraphicOptionInfos[eGraphicOptionNum]);
}

int CvGlobals::getNumYieldInfos()
{
	return (int)m_paYieldInfo.size();
}

std::vector<CvYieldInfo*>& CvGlobals::getYieldInfo()	// For Moose - XML Load Util
{
	return m_paYieldInfo;
}

CvYieldInfo& CvGlobals::getYieldInfo(YieldTypes eYieldNum)
{
	FAssert(eYieldNum > -1);
	FAssert(eYieldNum < NUM_YIELD_TYPES);
	return *(m_paYieldInfo[eYieldNum]);
}

int CvGlobals::getNumRouteInfos()
{
	return (int)m_paRouteInfo.size();
}

std::vector<CvRouteInfo*>& CvGlobals::getRouteInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paRouteInfo;
}

CvRouteInfo& CvGlobals::getRouteInfo(RouteTypes eRouteNum)
{
	FAssert(eRouteNum > -1);
	FAssert(eRouteNum < GC.getNumRouteInfos());
	return *(m_paRouteInfo[eRouteNum]);
}

int CvGlobals::getNumImprovementInfos()
{
	return (int)m_paImprovementInfo.size();
}

std::vector<CvImprovementInfo*>& CvGlobals::getImprovementInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paImprovementInfo;
}

CvImprovementInfo& CvGlobals::getImprovementInfo(ImprovementTypes eImprovementNum)
{
	FAssert(eImprovementNum > -1);
	FAssert(eImprovementNum < GC.getNumImprovementInfos());
	return *(m_paImprovementInfo[eImprovementNum]);
}

int CvGlobals::getNumGoodyInfos()
{
	return (int)m_paGoodyInfo.size();
}

std::vector<CvGoodyInfo*>& CvGlobals::getGoodyInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paGoodyInfo;
}

CvGoodyInfo& CvGlobals::getGoodyInfo(GoodyTypes eGoodyNum)
{
	FAssert(eGoodyNum > -1);
	FAssert(eGoodyNum < GC.getNumGoodyInfos());
	return *(m_paGoodyInfo[eGoodyNum]);
}

int CvGlobals::getNumBuildInfos()
{
	return (int)m_paBuildInfo.size();
}

std::vector<CvBuildInfo*>& CvGlobals::getBuildInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paBuildInfo;
}

CvBuildInfo& CvGlobals::getBuildInfo(BuildTypes eBuildNum)
{
	FAssert(eBuildNum > -1);
	FAssert(eBuildNum < GC.getNumBuildInfos());
	return *(m_paBuildInfo[eBuildNum]);
}

int CvGlobals::getNumHandicapInfos()
{
	return (int)m_paHandicapInfo.size();
}

std::vector<CvHandicapInfo*>& CvGlobals::getHandicapInfo()	// Do NOT export outside of the DLL	// For Moose - XML Load Util
{
	return m_paHandicapInfo;
}

CvHandicapInfo& CvGlobals::getHandicapInfo(HandicapTypes eHandicapNum)
{
	FAssert(eHandicapNum > -1);
	FAssert(eHandicapNum < GC.getNumHandicapInfos());
	return *(m_paHandicapInfo[eHandicapNum]);
}

int CvGlobals::getNumGameSpeedInfos()
{
	return (int)m_paGameSpeedInfo.size();
}

std::vector<CvGameSpeedInfo*>& CvGlobals::getGameSpeedInfo()	// Do NOT export outside of the DLL	// For Moose - XML Load Util
{
	return m_paGameSpeedInfo;
}

CvGameSpeedInfo& CvGlobals::getGameSpeedInfo(GameSpeedTypes eGameSpeedNum)
{
	FAssert(eGameSpeedNum > -1);
	FAssert(eGameSpeedNum < GC.getNumGameSpeedInfos());
	return *(m_paGameSpeedInfo[eGameSpeedNum]);
}

int CvGlobals::getNumAlarmInfos()
{
	return (int)m_paAlarmInfo.size();
}

std::vector<CvAlarmInfo*>& CvGlobals::getAlarmInfo()	// Do NOT export outside of the DLL	// For Moose - XML Load Util
{
	return m_paAlarmInfo;
}

CvAlarmInfo& CvGlobals::getAlarmInfo(AlarmTypes eAlarm)
{
	FAssert(eAlarm > -1);
	FAssert(eAlarm < GC.getNumAlarmInfos());
	return *(m_paAlarmInfo[eAlarm]);
}

int CvGlobals::getNumTurnTimerInfos()
{
	return (int)m_paTurnTimerInfo.size();
}

std::vector<CvTurnTimerInfo*>& CvGlobals::getTurnTimerInfo()	// Do NOT export outside of the DLL	// For Moose - XML Load Util
{
	return m_paTurnTimerInfo;
}

CvTurnTimerInfo& CvGlobals::getTurnTimerInfo(TurnTimerTypes eTurnTimerNum)
{
	FAssert(eTurnTimerNum > -1);
	FAssert(eTurnTimerNum < GC.getNumTurnTimerInfos());
	return *(m_paTurnTimerInfo[eTurnTimerNum]);
}

int CvGlobals::getNumBuildingClassInfos()
{
	return (int)m_paBuildingClassInfo.size();
}

std::vector<CvBuildingClassInfo*>& CvGlobals::getBuildingClassInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paBuildingClassInfo;
}

CvBuildingClassInfo& CvGlobals::getBuildingClassInfo(BuildingClassTypes eBuildingClassNum)
{
	FAssert(eBuildingClassNum > -1);
	FAssert(eBuildingClassNum < GC.getNumBuildingClassInfos());
	return *(m_paBuildingClassInfo[eBuildingClassNum]);
}

int CvGlobals::getNumBuildingInfos()
{
	return (int)m_paBuildingInfo.size();
}

std::vector<CvBuildingInfo*>& CvGlobals::getBuildingInfo()	// For Moose - XML Load Util, CvInfos, CvCacheObject
{
	return m_paBuildingInfo;
}

CvBuildingInfo& CvGlobals::getBuildingInfo(BuildingTypes eBuildingNum)
{
	FAssert(eBuildingNum > -1);
	FAssert(eBuildingNum < GC.getNumBuildingInfos());
	return *(m_paBuildingInfo[eBuildingNum]);
}

int CvGlobals::getNumSpecialBuildingInfos()
{
	return (int)m_paSpecialBuildingInfo.size();
}

std::vector<CvSpecialBuildingInfo*>& CvGlobals::getSpecialBuildingInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paSpecialBuildingInfo;
}

CvSpecialBuildingInfo& CvGlobals::getSpecialBuildingInfo(SpecialBuildingTypes eSpecialBuildingNum)
{
	FAssert(eSpecialBuildingNum > -1);
	FAssert(eSpecialBuildingNum < GC.getNumSpecialBuildingInfos());
	return *(m_paSpecialBuildingInfo[eSpecialBuildingNum]);
}

int CvGlobals::getNumUnitClassInfos()
{
	return (int)m_paUnitClassInfo.size();
}

std::vector<CvUnitClassInfo*>& CvGlobals::getUnitClassInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paUnitClassInfo;
}

CvUnitClassInfo& CvGlobals::getUnitClassInfo(UnitClassTypes eUnitClassNum)
{
	FAssert(eUnitClassNum > -1);
	FAssert(eUnitClassNum < GC.getNumUnitClassInfos());
	return *(m_paUnitClassInfo[eUnitClassNum]);
}

int CvGlobals::getNumActionInfos()
{
	return (int)m_paActionInfo.size();
}

std::vector<CvActionInfo*>& CvGlobals::getActionInfo()	// For Moose - XML Load Util
{
	return m_paActionInfo;
}

CvActionInfo& CvGlobals::getActionInfo(int i)
{
	FAssertMsg(i < getNumActionInfos(), "Index out of bounds");
	FAssertMsg(i > -1, "Index out of bounds");
	return *(m_paActionInfo[i]);
}

std::vector<CvMissionInfo*>& CvGlobals::getMissionInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paMissionInfo;
}

CvMissionInfo& CvGlobals::getMissionInfo(MissionTypes eMissionNum)
{
	FAssert(eMissionNum > -1);
	FAssert(eMissionNum < NUM_MISSION_TYPES);
	return *(m_paMissionInfo[eMissionNum]);
}

std::vector<CvControlInfo*>& CvGlobals::getControlInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paControlInfo;
}

CvControlInfo& CvGlobals::getControlInfo(ControlTypes eControlNum)
{
	FAssert(eControlNum > -1);
	FAssert(eControlNum < NUM_CONTROL_TYPES);
	FAssert(m_paControlInfo.size() > 0);
	return *(m_paControlInfo[eControlNum]);
}

std::vector<CvCommandInfo*>& CvGlobals::getCommandInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paCommandInfo;
}

CvCommandInfo& CvGlobals::getCommandInfo(CommandTypes eCommandNum)
{
	FAssert(eCommandNum > -1);
	FAssert(eCommandNum < NUM_COMMAND_TYPES);
	return *(m_paCommandInfo[eCommandNum]);
}

int CvGlobals::getNumAutomateInfos()
{
	return (int)m_paAutomateInfo.size();
}

std::vector<CvAutomateInfo*>& CvGlobals::getAutomateInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paAutomateInfo;
}

CvAutomateInfo& CvGlobals::getAutomateInfo(int iAutomateNum)
{
	FAssertMsg(iAutomateNum < getNumAutomateInfos(), "Index out of bounds");
	FAssertMsg(iAutomateNum > -1, "Index out of bounds");
	return *(m_paAutomateInfo[iAutomateNum]);
}

int CvGlobals::getNumPromotionInfos()
{
	return (int)m_paPromotionInfo.size();
}

std::vector<CvPromotionInfo*>& CvGlobals::getPromotionInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paPromotionInfo;
}

CvPromotionInfo& CvGlobals::getPromotionInfo(PromotionTypes ePromotionNum)
{
	FAssert(ePromotionNum > -1);
	FAssert(ePromotionNum < GC.getNumPromotionInfos());
	return *(m_paPromotionInfo[ePromotionNum]);
}

int CvGlobals::getNumProfessionInfos()
{
	return (int)m_paProfessionInfo.size();
}

std::vector<CvProfessionInfo*>& CvGlobals::getProfessionInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paProfessionInfo;
}

CvProfessionInfo& CvGlobals::getProfessionInfo(ProfessionTypes eProfessionNum)
{
	FAssert(eProfessionNum > -1);
	FAssert(eProfessionNum < (int)m_paProfessionInfo.size());
	return *(m_paProfessionInfo[eProfessionNum]);
}
int CvGlobals::getNumCivicOptionInfos()
{
	return (int)m_paCivicOptionInfo.size();
}

std::vector<CvInfoBase*>& CvGlobals::getCivicOptionInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paCivicOptionInfo;
}

CvInfoBase& CvGlobals::getCivicOptionInfo(CivicOptionTypes eCivicOptionNum)
{
	FAssert(eCivicOptionNum > -1);
	FAssert(eCivicOptionNum < GC.getNumCivicOptionInfos());
	return *(m_paCivicOptionInfo[eCivicOptionNum]);
}

int CvGlobals::getNumCivicInfos()
{
	return (int)m_paCivicInfo.size();
}

std::vector<CvCivicInfo*>& CvGlobals::getCivicInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paCivicInfo;
}

CvCivicInfo& CvGlobals::getCivicInfo(CivicTypes eCivicNum)
{
	FAssert(eCivicNum > -1);
	FAssert(eCivicNum < GC.getNumCivicInfos());
	return *(m_paCivicInfo[eCivicNum]);
}

int CvGlobals::getNumDiplomacyInfos()
{
	return (int)m_paDiplomacyInfo.size();
}

std::vector<CvDiplomacyInfo*>& CvGlobals::getDiplomacyInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paDiplomacyInfo;
}

CvDiplomacyInfo& CvGlobals::getDiplomacyInfo(int iDiplomacyNum)
{
	FAssertMsg(iDiplomacyNum < getNumDiplomacyInfos(), "Index out of bounds");
	FAssertMsg(iDiplomacyNum > -1, "Index out of bounds");
	return *(m_paDiplomacyInfo[iDiplomacyNum]);
}

int CvGlobals::getNumEraInfos()
{
	return (int)m_aEraInfo.size();
}

std::vector<CvEraInfo*>& CvGlobals::getEraInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_aEraInfo;
}

CvEraInfo& CvGlobals::getEraInfo(EraTypes eEraNum)
{
	FAssert(eEraNum > -1);
	FAssert(eEraNum < GC.getNumEraInfos());
	return *(m_aEraInfo[eEraNum]);
}

int CvGlobals::getNumNewEraInfos()
{
	return (int)m_aNewEraInfo.size();
}

std::vector<CvNewEraInfo*>& CvGlobals::getNewEraInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_aNewEraInfo;
}

CvNewEraInfo& CvGlobals::getNewEraInfo(NewEraTypes eNewEraNum)
{
	FAssert(eNewEraNum > -1);
	FAssert(eNewEraNum < GC.getNumNewEraInfos());
	return *(m_aNewEraInfo[eNewEraNum]);
}

int CvGlobals::getNumEconomicTrendInfos()
{
	return (int)m_aEconomicTrendInfo.size();
}

std::vector<CvEconomicTrendInfo*>& CvGlobals::getEconomicTrendInfo()
{
	return m_aEconomicTrendInfo;
}

CvEconomicTrendInfo& CvGlobals::getEconomicTrendInfo(EconomicTrendTypes eEconomicTrendType)
{
	FAssert(eEconomicTrendType > -1);
	FAssert(eEconomicTrendType < GC.getNumEconomicTrendInfos());
	return *(m_aEconomicTrendInfo[eEconomicTrendType]);
}

int CvGlobals::getNumPirateInfos()
{
	return (int)m_aPirateInfo.size();
}

std::vector<CvPirateInfo*>& CvGlobals::getPirateInfo()
{
	return m_aPirateInfo;
}

CvPirateInfo& CvGlobals::getPirateInfo(PirateInfoTypes index)
{
	FAssert(index > -1);
	FAssert(index < GC.getNumPirateInfos());
	return *(m_aPirateInfo[index]);
}

int CvGlobals::getNumWarPlanInfos()
{
	return (int)m_aWarPlanInfo.size();
}

std::vector<CvWarPlanInfo*>& CvGlobals::getWarPlanInfo()
{
	return m_aWarPlanInfo;
}

CvWarPlanInfo& CvGlobals::getWarPlanInfo(WarPlanInfoTypes index)
{
	FAssert(index > -1);
	FAssert(index < GC.getNumWarPlanInfos());
	return *(m_aWarPlanInfo[index]);
}

int CvGlobals::getNumWarPlanStrategyInfos()
{
	return (int)m_aWarPlanStrategyInfo.size();
}

std::vector<CvWarPlanStrategyInfo*>& CvGlobals::getWarPlanStrategyInfo()
{
	return m_aWarPlanStrategyInfo;
}


int CvGlobals::getNumArtilleryPenaltyInfos()
{
	return (int)m_aArtilleryPenaltyInfo.size();
}

std::vector<CvArtilleryPenaltyInfo*>& CvGlobals::getArtilleryPenaltyInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_aArtilleryPenaltyInfo;
}

CvArtilleryPenaltyInfo& CvGlobals::getArtilleryPenaltyInfo(ArtilleryPenaltyTypes eArtilleryPenaltyNum)
{
	FAssert(eArtilleryPenaltyNum > -1);
	FAssert(eArtilleryPenaltyNum < GC.getNumArtilleryPenaltyInfos());
	return *(m_aArtilleryPenaltyInfo[eArtilleryPenaltyNum]);
}

int CvGlobals::getNumHurryInfos()
{
	return (int)m_paHurryInfo.size();
}

std::vector<CvHurryInfo*>& CvGlobals::getHurryInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paHurryInfo;
}

CvHurryInfo& CvGlobals::getHurryInfo(HurryTypes eHurryNum)
{
	FAssert(eHurryNum > -1);
	FAssert(eHurryNum < GC.getNumHurryInfos());
	return *(m_paHurryInfo[eHurryNum]);
}

int CvGlobals::getNumEmphasizeInfos()
{
	return (int)m_paEmphasizeInfo.size();
}

std::vector<CvEmphasizeInfo*>& CvGlobals::getEmphasizeInfo()	// For Moose - XML Load Util
{
	return m_paEmphasizeInfo;
}

CvEmphasizeInfo& CvGlobals::getEmphasizeInfo(EmphasizeTypes eEmphasizeNum)
{
	FAssert(eEmphasizeNum > -1);
	FAssert(eEmphasizeNum < GC.getNumEmphasizeInfos());
	return *(m_paEmphasizeInfo[eEmphasizeNum]);
}

int CvGlobals::getNumCultureLevelInfos()
{
	return (int)m_paCultureLevelInfo.size();
}

std::vector<CvCultureLevelInfo*>& CvGlobals::getCultureLevelInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paCultureLevelInfo;
}

CvCultureLevelInfo& CvGlobals::getCultureLevelInfo(CultureLevelTypes eCultureLevelNum)
{
	FAssert(eCultureLevelNum > -1);
	FAssert(eCultureLevelNum < GC.getNumCultureLevelInfos());
	return *(m_paCultureLevelInfo[eCultureLevelNum]);
}

int CvGlobals::getNumVictoryInfos()
{
	return (int)m_paVictoryInfo.size();
}

std::vector<CvVictoryInfo*>& CvGlobals::getVictoryInfo()	// For Moose - XML Load Util, CvInfos
{
	return m_paVictoryInfo;
}

CvVictoryInfo& CvGlobals::getVictoryInfo(VictoryTypes eVictoryNum)
{
	FAssert(eVictoryNum > -1);
	FAssert(eVictoryNum < GC.getNumVictoryInfos());
	return *(m_paVictoryInfo[eVictoryNum]);
}

int CvGlobals::getNumEventTriggerInfos()
{
	return (int)m_paEventTriggerInfo.size();
}

std::vector<CvEventTriggerInfo*>& CvGlobals::getEventTriggerInfo()
{
	return m_paEventTriggerInfo;
}

CvEventTriggerInfo& CvGlobals::getEventTriggerInfo(EventTriggerTypes eEventTrigger)
{
	FAssert(eEventTrigger > -1);
	FAssert(eEventTrigger < GC.getNumEventTriggerInfos());
	return *(m_paEventTriggerInfo[eEventTrigger]);
}

int CvGlobals::getNumEventInfos()
{
	return (int)m_paEventInfo.size();
}

std::vector<CvEventInfo*>& CvGlobals::getEventInfo()
{
	return m_paEventInfo;
}

CvEventInfo& CvGlobals::getEventInfo(EventTypes eEvent)
{
	FAssert(eEvent > -1);
	FAssert(eEvent < GC.getNumEventInfos());
	return *(m_paEventInfo[eEvent]);
}

int& CvGlobals::getNumEntityEventTypes()
{
	return m_iNumEntityEventTypes;
}

CvString*& CvGlobals::getEntityEventTypes()
{
	return m_paszEntityEventTypes;
}

CvString& CvGlobals::getEntityEventTypes(EntityEventTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumEntityEventTypes());
	return m_paszEntityEventTypes[e];
}

int& CvGlobals::getNumAnimationOperatorTypes()
{
	return m_iNumAnimationOperatorTypes;
}

CvString*& CvGlobals::getAnimationOperatorTypes()
{
	return m_paszAnimationOperatorTypes;
}

CvString& CvGlobals::getAnimationOperatorTypes(AnimationOperatorTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumAnimationOperatorTypes());
	return m_paszAnimationOperatorTypes[e];
}

CvString*& CvGlobals::getFunctionTypes()
{
	return m_paszFunctionTypes;
}

CvString& CvGlobals::getFunctionTypes(FunctionTypes e)
{
	FAssert(e > -1);
	FAssert(e < NUM_FUNC_TYPES);
	return m_paszFunctionTypes[e];
}

int& CvGlobals::getNumArtStyleTypes()
{
	return m_iNumArtStyleTypes;
}

CvString*& CvGlobals::getArtStyleTypes()
{
	return m_paszArtStyleTypes;
}

CvString& CvGlobals::getArtStyleTypes(ArtStyleTypes e)
{
	FAssert(e > -1);
	FAssert(e < GC.getNumArtStyleTypes());
	return m_paszArtStyleTypes[e];
}

//DOANE UnitArtStyles | Androrc
int CvGlobals::getNumUnitArtStyleTypeInfos()
{
    return (int)m_paUnitArtStyleTypeInfo.size();
}

std::vector<CvUnitArtStyleTypeInfo*>& CvGlobals::getUnitArtStyleTypeInfo()
{
	return m_paUnitArtStyleTypeInfo;
}

CvUnitArtStyleTypeInfo& CvGlobals::getUnitArtStyleTypeInfo(UnitArtStyleTypes eUnitArtStyleTypeNum)
{
	FAssert(eUnitArtStyleTypeNum > -1);
	FAssert(eUnitArtStyleTypeNum < GC.getNumUnitArtStyleTypeInfos());
	return *(m_paUnitArtStyleTypeInfo[eUnitArtStyleTypeNum]);
}
//END DOANE

std::vector<CvContactInfo*>& CvGlobals::getContactInfo()
{
	return m_paContactInfo;
}

CvContactInfo& CvGlobals::getContactInfo(ContactTypes eContactType)
{
	FAssert(eContactType > -1);
	return *(m_paContactInfo[eContactType]);
}

std::vector<CvAttitudeInfo*>& CvGlobals::getAttitudeInfo()
{
	return m_paAttitudeInfo;
}

CvAttitudeInfo& CvGlobals::getAttitudeInfo(AttitudeTypes eAttitudeType)
{
	FAssert(eAttitudeType > -1);
	return *(m_paAttitudeInfo[eAttitudeType]);
}

std::vector<CvMemoryInfo*>& CvGlobals::getMemoryInfo()
{
	return m_paMemoryInfo;
}

CvMemoryInfo& CvGlobals::getMemoryInfo(MemoryTypes eMemoryType)
{
	FAssert(eMemoryType > -1);
	return *(m_paMemoryInfo[eMemoryType]);
}

int& CvGlobals::getNumCitySizeTypes()
{
	return m_iNumCitySizeTypes;
}

CvString*& CvGlobals::getCitySizeTypes()
{
	return m_paszCitySizeTypes;
}

CvString& CvGlobals::getCitySizeTypes(int i)
{
	FAssertMsg(i < getNumCitySizeTypes(), "Index out of bounds");
	FAssertMsg(i > -1, "Index out of bounds");
	return m_paszCitySizeTypes[i];
}

CvString*& CvGlobals::getContactTypes()
{
	return m_paszContactTypes;
}

CvString& CvGlobals::getContactTypes(ContactTypes e)
{
	FAssert(e > -1);
	FAssert(e < NUM_CONTACT_TYPES);
	return m_paszContactTypes[e];
}

CvString*& CvGlobals::getDiplomacyPowerTypes()
{
	return m_paszDiplomacyPowerTypes;
}

CvString& CvGlobals::getDiplomacyPowerTypes(DiplomacyPowerTypes e)
{
	FAssert(e > -1);
	FAssert(e < NUM_DIPLOMACYPOWER_TYPES);
	return m_paszDiplomacyPowerTypes[e];
}

CvString*& CvGlobals::getAutomateTypes()
{
	return m_paszAutomateTypes;
}

CvString& CvGlobals::getAutomateTypes(AutomateTypes e)
{
	FAssert(e > -1);
	FAssert(e < NUM_AUTOMATE_TYPES);
	return m_paszAutomateTypes[e];
}

CvString*& CvGlobals::getDirectionTypes()
{
	return m_paszDirectionTypes;
}

CvString& CvGlobals::getDirectionTypes(AutomateTypes e)
{
	FAssert(e > -1);
	FAssert(e < NUM_DIRECTION_TYPES);
	return m_paszDirectionTypes[e];
}

int& CvGlobals::getNumFootstepAudioTypes()
{
	return m_iNumFootstepAudioTypes;
}

CvString*& CvGlobals::getFootstepAudioTypes()
{
	return m_paszFootstepAudioTypes;
}

CvString& CvGlobals::getFootstepAudioTypes(int i)
{
	FAssertMsg(i < getNumFootstepAudioTypes(), "Index out of bounds");
	FAssertMsg(i > -1, "Index out of bounds");
	return m_paszFootstepAudioTypes[i];
}

int CvGlobals::getFootstepAudioTypeByTag(CvString strTag)
{
	int iIndex = -1;

	if ( strTag.GetLength() <= 0 )
	{
		return iIndex;
	}

	for ( int i = 0; i < m_iNumFootstepAudioTypes; i++ )
	{
		if ( strTag.CompareNoCase(m_paszFootstepAudioTypes[i]) == 0 )
		{
			iIndex = i;
			break;
		}
	}

	return iIndex;
}

CvString*& CvGlobals::getFootstepAudioTags()
{
	return m_paszFootstepAudioTags;
}

CvString& CvGlobals::getFootstepAudioTags(int i)
{
//	FAssertMsg(i < getNumFootstepAudioTags(), "Index out of bounds")
	FAssertMsg(i > -1, "Index out of bounds");
	return m_paszFootstepAudioTags[i];
}

void CvGlobals::setCurrentXMLFile(const TCHAR* szFileName)
{
	m_szCurrentXMLFile = szFileName;
}

CvString& CvGlobals::getCurrentXMLFile()
{
	return m_szCurrentXMLFile;
}

FVariableSystem* CvGlobals::getDefinesVarSystem()
{
	return m_VarSystem;
}

void CvGlobals::cacheGlobals()
{
	//DOANE
	uint uiFVarSystemSizeStart = GC.getDefinesVarSystem()->GetSize();
	//FAssertMsg(uiFVarSystemSizeStart > 1 && m_bCacheGlobalsClean, "CacheGlobals is not expected to do a clean save before SetGlobalDefines has been called")
	//END DOANE

	m_iMOVE_DENOMINATOR = getDefineINT("MOVE_DENOMINATOR");
	m_iFOOD_CONSUMPTION_PER_POPULATION = getDefineINT("FOOD_CONSUMPTION_PER_POPULATION");
	m_iMAX_HIT_POINTS = getDefineINT("MAX_HIT_POINTS");
	m_iHILLS_EXTRA_DEFENSE = getDefineINT("HILLS_EXTRA_DEFENSE");
	m_iRIVER_ATTACK_MODIFIER = getDefineINT("RIVER_ATTACK_MODIFIER");
	m_iAMPHIB_ATTACK_MODIFIER = getDefineINT("AMPHIB_ATTACK_MODIFIER");
	m_iHILLS_EXTRA_MOVEMENT = getDefineINT("HILLS_EXTRA_MOVEMENT");
	m_iPEAK_EXTRA_MOVEMENT = getDefineINT("PEAK_EXTRA_MOVEMENT");
	m_iMAX_PLOT_LIST_ROWS = getDefineINT("MAX_PLOT_LIST_ROWS");
	m_iUNIT_MULTISELECT_MAX = getDefineINT("UNIT_MULTISELECT_MAX");
	m_iEVENT_MESSAGE_TIME = getDefineINT("EVENT_MESSAGE_TIME");
	m_iROUTE_FEATURE_GROWTH_MODIFIER = getDefineINT("ROUTE_FEATURE_GROWTH_MODIFIER");
	m_iFEATURE_GROWTH_MODIFIER = getDefineINT("FEATURE_GROWTH_MODIFIER");
	m_iMIN_CITY_RANGE = getDefineINT("MIN_CITY_RANGE");
	m_iCITY_MAX_NUM_BUILDINGS = getDefineINT("CITY_MAX_NUM_BUILDINGS");
	m_iLAKE_MAX_AREA_SIZE = getDefineINT("LAKE_MAX_AREA_SIZE");
	m_iMIN_WATER_SIZE_FOR_OCEAN = getDefineINT("MIN_WATER_SIZE_FOR_OCEAN");
	m_iFORTIFY_MODIFIER_PER_TURN = getDefineINT("FORTIFY_MODIFIER_PER_TURN");
	m_iMAX_CITY_DEFENSE_DAMAGE = getDefineINT("MAX_CITY_DEFENSE_DAMAGE");
	m_iPEAK_SEE_THROUGH_CHANGE = getDefineINT("PEAK_SEE_THROUGH_CHANGE");
	m_iHILLS_SEE_THROUGH_CHANGE = getDefineINT("HILLS_SEE_THROUGH_CHANGE");
	m_iSEAWATER_SEE_FROM_CHANGE = getDefineINT("SEAWATER_SEE_FROM_CHANGE");
	m_iPEAK_SEE_FROM_CHANGE = getDefineINT("PEAK_SEE_FROM_CHANGE");
	m_iHILLS_SEE_FROM_CHANGE = getDefineINT("HILLS_SEE_FROM_CHANGE");
	m_iMAX_REBEL_YIELD_MODIFIER = getDefineINT("MAX_REBEL_YIELD_MODIFIER");

	m_fCAMERA_MIN_YAW = getDefineFLOAT("CAMERA_MIN_YAW");
	m_fCAMERA_MAX_YAW = getDefineFLOAT("CAMERA_MAX_YAW");
	m_fCAMERA_FAR_CLIP_Z_HEIGHT = getDefineFLOAT("CAMERA_FAR_CLIP_Z_HEIGHT");
	m_fCAMERA_MAX_TRAVEL_DISTANCE = getDefineFLOAT("CAMERA_MAX_TRAVEL_DISTANCE");
	m_fCAMERA_START_DISTANCE = getDefineFLOAT("CAMERA_START_DISTANCE");
	m_fPLOT_SIZE = getDefineFLOAT("PLOT_SIZE");
	m_fCAMERA_SPECIAL_PITCH = getDefineFLOAT("CAMERA_SPECIAL_PITCH");
	m_fCAMERA_MAX_TURN_OFFSET = getDefineFLOAT("CAMERA_MAX_TURN_OFFSET");
	m_fCAMERA_MIN_DISTANCE = getDefineFLOAT("CAMERA_MIN_DISTANCE");
	m_fCAMERA_UPPER_PITCH = getDefineFLOAT("CAMERA_UPPER_PITCH");
	m_fCAMERA_LOWER_PITCH = getDefineFLOAT("CAMERA_LOWER_PITCH");
	m_fFIELD_OF_VIEW = getDefineFLOAT("FIELD_OF_VIEW");
	m_fUNIT_MULTISELECT_DISTANCE = getDefineFLOAT("UNIT_MULTISELECT_DISTANCE");

	///////////////////////////////////////////////////////////////
	////////*		//DOANE Cache| Speed Improvement		*//////
	///////////////////////////////////////////////////////////////
	//DOANE Enrol Unit
	m_ucMAX_ENROL_UNIT = getDefineUCHAR("MAX_ENROL_UNIT", &m_ucMAX_ENROL_UNIT, bCL);
	//DOANE Enrol Unit
	m_ucMAX_MILITARY_PROFESSION_TURN = getDefineUCHAR("MAX_MILITARY_PROFESSION_TURN", &m_ucMAX_MILITARY_PROFESSION_TURN, bCL);
	m_ucSTART_MILITARY_PROFESSION_PENALTY = getDefineUCHAR("START_MILITARY_PROFESSION_PENALTY", &m_ucSTART_MILITARY_PROFESSION_PENALTY, bCL);
	//DOANE Enrol Unit
	m_ucFORTIFICATION_BREAK_DAMAGE = getDefineUCHAR("FORTIFICATION_BREAK_DAMAGE", &m_ucFORTIFICATION_BREAK_DAMAGE, bCL);
	m_ucPOWERFUL_FORTIFICATION_PENALTY = getDefineUCHAR("POWERFUL_FORTIFICATION_PENALTY", &m_ucPOWERFUL_FORTIFICATION_PENALTY, bCL);
	//DOANE AI help
	m_iGOLD_BY_POPULATION_BY_TURN = getDefineINT("AI_GOLD_BY_POPULATION_BY_TURN");
	m_iNUM_DISCOVERED_MAP_TILES_BEFORE_DELIVERING = getDefineINT("NUM_DISCOVERED_MAP_TILES_BEFORE_DELIVERING");
	//DOANE Economy
	m_iMAX_MANUFACTURED_YIELD_AMOUNT = getDefineINT("MAX_MANUFACTURED_YIELD_AMOUNT");
	//DOANE Builder Pack
	m_ucBUILDER_PACK_YIELD_COST_PERCENT = getDefineUCHAR("BUILDER_PACK_YIELD_COST_PERCENT", &m_ucBUILDER_PACK_YIELD_COST_PERCENT, bCL);
	//DOANE Survivors on Transport Sinking
	m_ucSURVIVORS_ON_TRANSPORT_SINKING_PROBABILITY = getDefineUCHAR("SURVIVORS_ON_TRANSPORT_SINKING_PROBABILITY", &m_ucSURVIVORS_ON_TRANSPORT_SINKING_PROBABILITY, bCL);
	//DoaNE num history turn for trade routes stats
	m_ucNUM_TRADE_ROUTE_HIST_TURN = getDefineUCHAR("NUM_TRADE_ROUTE_HIST_TURN", &m_ucNUM_TRADE_ROUTE_HIST_TURN, bCL);
	//DoaNE  Percentage of boost building construction if current era is above of required era for building
	m_ucAVANCED_NEW_ERA_BOOST_FOR_CONSTRUCTION = getDefineUCHAR("AVANCED_NEW_ERA_BOOST_FOR_CONSTRUCTION", &m_ucAVANCED_NEW_ERA_BOOST_FOR_CONSTRUCTION, bCL);
	//DoaNE Immigration
	m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_AVAILABLE = getDefineUCHAR("MIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_AVAILABLE", &m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_AVAILABLE, bCL);
	m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_DOCK = getDefineUCHAR("MIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_DOCK", &m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_DOCK, bCL);
	m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_CARGO = getDefineUCHAR("MIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_CARGO", &m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_CARGO, bCL);
	m_ucPROBABILITY_NATURAL_IMMIGRATION_START = getDefineUCHAR("PROBABILITY_NATURAL_IMMIGRATION_START", &m_ucPROBABILITY_NATURAL_IMMIGRATION_START, bCL);
	m_ucPROBABILITY_NATURAL_IMMIGRATION_LATE = getDefineUCHAR("PROBABILITY_NATURAL_IMMIGRATION_LATE", &m_ucPROBABILITY_NATURAL_IMMIGRATION_LATE, bCL);
	m_ucPROBABILITY_NATURAL_IMMIGRATION_PERCENT = getDefineUCHAR("PROBABILITY_NATURAL_IMMIGRATION_PERCENT", &m_ucPROBABILITY_NATURAL_IMMIGRATION_PERCENT, bCL);
	m_iTURN_NATURAL_IMMIGRATION_CHANGE = getDefineINT("TURN_NATURAL_IMMIGRATION_CHANGE");
	// City disturbation
	m_ucNAVAL_TRANSPORT_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY = getDefineUCHAR("NAVAL_TRANSPORT_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY", &m_ucNAVAL_TRANSPORT_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY, bCL);
	m_ucNAVAL_MILITARY_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY = getDefineUCHAR("NAVAL_MILITARY_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY", &m_ucNAVAL_MILITARY_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY, bCL);
	m_ucNAVAL_DISTURBATION_FOR_TRADE_ROUTE = getDefineUCHAR("NAVAL_DISTURBATION_FOR_TRADE_ROUTE", &m_ucNAVAL_DISTURBATION_FOR_TRADE_ROUTE, bCL);
	m_ucNAVAL_DISTURBATION_FOR_MILITARY_SHIP_KILLED_PROTECTING_A_CITY = getDefineUCHAR("NAVAL_DISTURBATION_FOR_MILITARY_SHIP_KILLED_PROTECTING_A_CITY", &m_ucNAVAL_DISTURBATION_FOR_MILITARY_SHIP_KILLED_PROTECTING_A_CITY, bCL);
	// Income and expanditures
	m_ucEXPENDITURE_BY_LAND_FORCE_INSIDE_COLONIES = getDefineUCHAR("EXPENDITURE_BY_LAND_FORCE_INSIDE_COLONIES", &m_ucEXPENDITURE_BY_LAND_FORCE_INSIDE_COLONIES, bCL); 
	m_ucEXPENDITURE_BY_LAND_FORCE_OUTSIDE_COLONIES = getDefineUCHAR("EXPENDITURE_BY_LAND_FORCE_OUTSIDE_COLONIES", &m_ucEXPENDITURE_BY_LAND_FORCE_OUTSIDE_COLONIES, bCL); 
	m_ucEXPENDITURE_BY_UNEMPLOYED = getDefineUCHAR("EXPENDITURE_BY_UNEMPLOYED", &m_ucEXPENDITURE_BY_UNEMPLOYED, bCL); 
	m_ucEXPENDITURE_BY_SCOUT = getDefineUCHAR("EXPENDITURE_BY_SCOUT", &m_ucEXPENDITURE_BY_SCOUT, bCL); 
	m_ucEXPENDITURE_BY_AGRONOMIST = getDefineUCHAR("EXPENDITURE_BY_AGRONOMIST", &m_ucEXPENDITURE_BY_AGRONOMIST, bCL); 
	m_ucEXPENDITURE_BY_PIONEER = getDefineUCHAR("EXPENDITURE_BY_PIONEER", &m_ucEXPENDITURE_BY_PIONEER, bCL); 
	m_ucEXPENDITURE_BY_MERCHANT_SHIP = getDefineUCHAR("EXPENDITURE_BY_MERCHANT_SHIP", &m_ucEXPENDITURE_BY_MERCHANT_SHIP, bCL); 
	m_ucEXPENDITURE_BY_MILITARY_SHIP = getDefineUCHAR("EXPENDITURE_BY_MILITARY_SHIP", &m_ucEXPENDITURE_BY_MILITARY_SHIP, bCL); 
	///////////////////////////////////////////////////////////////
	////////*	//DOANE Python Callbacks | Speed Improvement*//////
	///////////////////////////////////////////////////////////////
	//Vanilla Callbacks
	m_bUSE_CAN_FOUND_CITIES_ON_WATER_CALLBACK = getDefineBOOL("USE_CAN_FOUND_CITIES_ON_WATER_CALLBACK", &m_bUSE_CAN_FOUND_CITIES_ON_WATER_CALLBACK, bCL);
	m_bUSE_CANNOT_DO_CIVIC_CALLBACK = getDefineBOOL("USE_CANNOT_DO_CIVIC_CALLBACK", &m_bUSE_CANNOT_DO_CIVIC_CALLBACK, bCL);
	m_bUSE_CAN_DO_CIVIC_CALLBACK = getDefineBOOL("USE_CAN_DO_CIVIC_CALLBACK", &m_bUSE_CAN_DO_CIVIC_CALLBACK, bCL);
	m_bUSE_CANNOT_CONSTRUCT_CALLBACK = getDefineBOOL("USE_CANNOT_CONSTRUCT_CALLBACK", &m_bUSE_CANNOT_CONSTRUCT_CALLBACK, bCL);
	m_bUSE_CAN_CONSTRUCT_CALLBACK = getDefineBOOL("USE_CAN_CONSTRUCT_CALLBACK", &m_bUSE_CAN_CONSTRUCT_CALLBACK, bCL);
	m_bUSE_CAN_DECLARE_WAR_CALLBACK = getDefineBOOL("USE_CAN_DECLARE_WAR_CALLBACK", &m_bUSE_CAN_DECLARE_WAR_CALLBACK, bCL);
	m_bUSE_GET_UNIT_COST_MOD_CALLBACK = getDefineBOOL("USE_GET_UNIT_COST_MOD_CALLBACK", &m_bUSE_GET_UNIT_COST_MOD_CALLBACK, bCL);
	m_bUSE_GET_BUILDING_COST_MOD_CALLBACK = getDefineBOOL("USE_GET_BUILDING_COST_MOD_CALLBACK", &m_bUSE_GET_BUILDING_COST_MOD_CALLBACK, bCL);
	m_bUSE_GET_CITY_FOUND_VALUE_CALLBACK = getDefineBOOL("USE_GET_CITY_FOUND_VALUE_CALLBACK", &m_bUSE_GET_CITY_FOUND_VALUE_CALLBACK, bCL);
	m_bUSE_CANNOT_HANDLE_ACTION_CALLBACK = getDefineBOOL("USE_CANNOT_HANDLE_ACTION_CALLBACK", &m_bUSE_CANNOT_HANDLE_ACTION_CALLBACK, bCL);
	m_bUSE_CAN_BUILD_CALLBACK = getDefineBOOL("USE_CAN_BUILD_CALLBACK", &m_bUSE_CAN_BUILD_CALLBACK, bCL);
	m_bUSE_CANNOT_TRAIN_CALLBACK = getDefineBOOL("USE_CANNOT_TRAIN_CALLBACK", &m_bUSE_CANNOT_TRAIN_CALLBACK, bCL);
	m_bUSE_CAN_TRAIN_CALLBACK = getDefineBOOL("USE_CAN_TRAIN_CALLBACK", &m_bUSE_CAN_TRAIN_CALLBACK, bCL);
	m_bUSE_UNIT_CANNOT_MOVE_INTO_CALLBACK = getDefineBOOL("USE_UNIT_CANNOT_MOVE_INTO_CALLBACK", &m_bUSE_CANNOT_HANDLE_ACTION_CALLBACK, bCL);
	m_iUSE_FINISH_TEXT_CALLBACK = getDefineINT("USE_FINISH_TEXT_CALLBACK"); //Warning: this is used by the exe!
	m_iUSE_ON_UNIT_SET_XY_CALLBACK = getDefineINT("USE_ON_UNIT_SET_XY_CALLBACK"); //Warning: this is used by the exe!
	m_iUSE_ON_UNIT_SELECTED_CALLBACK = getDefineINT("USE_ON_UNIT_SELECTED_CALLBACK"); //Warning: this is used by the exe!
	m_iUSE_ON_MISSIONARY_CONVERTED_UNIT_CALLBACK = getDefineINT("USE_ON_MISSIONARY_CONVERTED_UNIT_CALLBACK"); //Warning: this is used by the exe!
	m_iUSE_ON_UPDATE_CALLBACK = getDefineINT("USE_ON_UPDATE_CALLBACK"); //Warning: this is used by the exe!
	m_iUSE_ON_UNIT_CREATED_CALLBACK = getDefineINT("USE_ON_UNIT_CREATED_CALLBACK"); //Warning: this is used by the exe!
	m_iUSE_ON_UNIT_LOST_CALLBACK = getDefineINT("USE_ON_UNIT_LOST_CALLBACK"); //Warning: this is used by the exe!
	//CvCity
	m_bUSE_ON_CITY_DO_GROWTH_CALLBACK = getDefineBOOL("USE_ON_CITY_DO_GROWTH_CALLBACK", &m_bUSE_ON_CITY_DO_GROWTH_CALLBACK, bCL);
	m_bUSE_ON_CITIES_DESTROY_FEATURES_CALLBACK = getDefineBOOL("USE_ON_CITIES_DESTROY_FEATURES_CALLBACK", &m_bUSE_ON_CITIES_DESTROY_FEATURES_CALLBACK, bCL);
	m_bUSE_ON_DO_CULTURE_CALLBACK = getDefineBOOL("USE_ON_DO_CULTURE_CALLBACK", &m_bUSE_ON_DO_CULTURE_CALLBACK, bCL);
	m_bUSE_ON_DO_PLOT_CULTURE_CALLBACK = getDefineBOOL("USE_ON_DO_PLOT_CULTURE_CALLBACK", &m_bUSE_ON_DO_PLOT_CULTURE_CALLBACK, bCL);
	m_bUSE_ON_DO_PRODUCTION_CALLBACK = getDefineBOOL("USE_ON_DO_PRODUCTION_CALLBACK", &m_bUSE_ON_DO_PRODUCTION_CALLBACK, bCL);
	//CvCityAI
	m_bUSE_AI_CHOOSE_PRODUCTION_CALLBACK = getDefineBOOL("USE_AI_CHOOSE_PRODUCTION_CALLBACK", &m_bUSE_AI_CHOOSE_PRODUCTION_CALLBACK, bCL);
	//CvGame
	m_bUSE_ON_UPDATE_COLORED_PLOTS_CALLBACK = getDefineBOOL("USE_ON_UPDATE_COLORED_PLOTS_CALLBACK", &m_bUSE_ON_UPDATE_COLORED_PLOTS_CALLBACK, bCL);
	m_bUSE_CAN_NOT_SELECTION_LIST_MOVE_CALLBACK = getDefineBOOL("USE_CAN_NOT_SELECTION_LIST_MOVE_CALLBACK ", &m_bUSE_CAN_NOT_SELECTION_LIST_MOVE_CALLBACK, bCL);
	m_bUSE_CAN_NOT_SELECTION_LIST_GAME_NET_MESSAGE_CALLBACK = getDefineBOOL("USE_CAN_NOT_SELECTION_LIST_GAME_NET_MESSAGE_CALLBACK", &m_bUSE_CAN_NOT_SELECTION_LIST_GAME_NET_MESSAGE_CALLBACK, bCL);
	m_bUSE_CAN_NOT_DO_CONTROL_CALLBACK = getDefineBOOL("USE_CAN_NOT_DO_CONTROL_CALLBACK", &m_bUSE_CAN_NOT_DO_CONTROL_CALLBACK, bCL);
	m_bUSE_ON_DO_REVIVE_ACTIVE_PLAYER_CALLBACK = getDefineBOOL("USE_ON_DO_REVIVE_ACTIVE_PLAYER_CALLBACK", &m_bUSE_ON_DO_REVIVE_ACTIVE_PLAYER_CALLBACK, bCL);
	m_bUSE_ON_CHECK_IS_VICTORY_CALLBACK = getDefineBOOL("USE_ON_CHECK_IS_VICTORY_CALLBACK", &m_bUSE_ON_CHECK_IS_VICTORY_CALLBACK, bCL);
	m_bUSE_ON_CHECK_IS_VICTORY_TEST_CALLBACK = getDefineBOOL("USE_ON_CHECK_IS_VICTORY_TEST_CALLBACK", &m_bUSE_ON_CHECK_IS_VICTORY_TEST_CALLBACK, bCL);
	//CvDLLButtonPopup
	m_bUSE_ON_SKIP_PRODUCTION_POPUP_CALLBACK = getDefineBOOL("USE_ON_SKIP_PRODUCTION_POPUP_CALLBACK", &m_bUSE_ON_SKIP_PRODUCTION_POPUP_CALLBACK, bCL);
	m_bUSE_ON_SHOW_EXAMINE_CITY_BUTTON_CALLBACK = getDefineBOOL("USE_ON_SHOW_EXAMINE_CITY_BUTTON_CALLBACK", &m_bUSE_ON_SHOW_EXAMINE_CITY_BUTTON_CALLBACK, bCL);
	m_bUSE_GET_RECOMMENDED_UNIT_CALLBACK = getDefineBOOL("USE_GET_RECOMMENDED_UNIT_CALLBACK", &m_bUSE_GET_RECOMMENDED_UNIT_CALLBACK, bCL);
	m_bUSE_GET_RECOMMENDED_BUILDING_CALLBACK = getDefineBOOL("USE_GET_RECOMMENDED_BUILDING_CALLBACK", &m_bUSE_GET_RECOMMENDED_BUILDING_CALLBACK, bCL);
	//CvPlayer
	m_bUSE_ON_DO_CITY_CAPTURE_GOLD_CALLBACK = getDefineBOOL("USE_ON_DO_CITY_CAPTURE_GOLD_CALLBACK", &m_bUSE_ON_DO_CITY_CAPTURE_GOLD_CALLBACK, bCL);
	m_bUSE_CAN_RAZE_CITY_CALLBACK = getDefineBOOL("USE_CAN_RAZE_CITY_CALLBACK", &m_bUSE_CAN_RAZE_CITY_CALLBACK, bCL);
	m_bUSE_ON_DO_GOLD_CALLBACK = getDefineBOOL("USE_ON_DO_GOLD_CALLBACK", &m_bUSE_ON_DO_GOLD_CALLBACK, bCL);
	//CvPlayerAI
	m_bUSE_AI_DO_DIPLO_CALLBACK = getDefineBOOL("USE_AI_DO_DIPLO_CALLBACK", &m_bUSE_AI_DO_DIPLO_CALLBACK, bCL);
	//CvSelectionGroup
	m_bUSE_ON_DO_COMBAT_CALLBACK = getDefineBOOL("USE_ON_DO_COMBAT_CALLBACK", &m_bUSE_ON_DO_COMBAT_CALLBACK, bCL);
	//CvTeamAI
	m_bUSE_AI_DO_WAR_CALLBACK = getDefineBOOL("USE_AI_DO_WAR_CALLBACK", &m_bUSE_AI_DO_WAR_CALLBACK, bCL);
	//CvUnit
	m_bUSE_ON_IS_ACTION_RECOMMENDED_CALLBACK = getDefineBOOL("USE_ON_IS_ACTION_RECOMMENDED_CALLBACK", &m_bUSE_ON_IS_ACTION_RECOMMENDED_CALLBACK, bCL);
	m_bUSE_ON_DO_PILLAGE_GOLD_CALLBACK = getDefineBOOL("USE_ON_DO_PILLAGE_GOLD_CALLBACK", &m_bUSE_ON_DO_PILLAGE_GOLD_CALLBACK, bCL);
	m_bUSE_GET_UPGRADE_PRICE_OVERRIDE_CALLBACK = getDefineBOOL("USE_GET_UPGRADE_PRICE_OVERRIDE_CALLBACK", &m_bUSE_GET_UPGRADE_PRICE_OVERRIDE_CALLBACK, bCL);
	m_bUSE_GET_EXPERIENCE_NEEDED_CALLBACK = getDefineBOOL("USE_GET_EXPERIENCE_NEEDED_CALLBACK", &m_bUSE_GET_EXPERIENCE_NEEDED_CALLBACK, bCL);
	//CvUnitAI
	m_bUSE_AI_UNIT_UPDATE_CALLBACK =  getDefineBOOL("USE_AI_UNIT_UPDATE_CALLBACK", &m_bUSE_AI_UNIT_UPDATE_CALLBACK, bCL);

	///////////////////////////////////////////////////////////////
	////////*		//DOANE Cache| Speed Improvement		*//////
	///////////////////////////////////////////////////////////////
	uint uiFVarSystemSizeEnd = GC.getDefinesVarSystem()->GetSize(); //DOANE
	//Disallow future calls to variables already removed:
	m_bCacheGlobalsClean = false; //bCL
	///////////////////////////////////////////////////////////////
	////////*				END DOANE						*//////
	///////////////////////////////////////////////////////////////	
}

int CvGlobals::getDefineINT( const char * szName ) const
{
	int iReturn = 0;
	GC.getDefinesVarSystem()->GetValue( szName, iReturn );
	return iReturn;
}

float CvGlobals::getDefineFLOAT( const char * szName ) const
{
	float fReturn = 0;
	GC.getDefinesVarSystem()->GetValue( szName, fReturn );
	return fReturn;
}

const char * CvGlobals::getDefineSTRING( const char * szName ) const
{
	const char * szReturn = NULL;
	GC.getDefinesVarSystem()->GetValue( szName, szReturn );
	return szReturn;
}

///////////////////////////////////////////////////////////////////////////
////////*/	//DOANE XML Loading | Var System | Speed improvement	*//////
///////////////////////////////////////////////////////////////////////////
//DOANE Start Cache
/*
Start cache is initialized once during game loading before XML.
Start COL:
	*cacheAtStart() <-- CvGlobals::init()

*/
void CvGlobals::cacheAtStart()
{	/*
	Called once during the initialization of the game.
	Used to cache global values or defines
	*/
}

void CvGlobals::uninitCacheAtStart()
{	/*
	Called once during the initialization of the game.
	Delete cached values
	*/
}
//DOANE Delayed Cache:
/*
Delayed cache is initialized and set during runtime, but with XML already loaded. There are 4 possibilities:
Start COL:
	*resetDelayedCacheAtStart(bool bConstructorCall = true) <-- CvGame::reset()
Load save-game:
	*uninitDelayedCacheAtStart() <-- CvGame::uninit() <-- CvGame::reset()
	*resetDelayedCacheAtStart(bool bConstructorCall = false) <-- CvGame::reset()
	*delayedCacheAtStart() <-- CvGame::write()
New game:
	*uninitDelayedCacheAtStart() <-- CvGame::uninit() <-- CvGame::reset()
	*resetDelayedCacheAtStart(bool bConstructorCall = false) <-- CvGame::reset()
	*delayedCacheAtStart() <-- CvGame::init()
Exit game:
	*uninitDelayedCacheAtStart() <-- CvGame::uninit() <-- CvGame::reset()
	*resetDelayedCacheAtStart(bool bConstructorCall = false) <-- CvGame::reset()
*/
void CvGlobals::delayedCacheAtStart()
{	/*
	Called once after the initialization of the game, during CvGame::init() or CvGame::read (start of a save-game)
	Used to cache global values which require the XML files loaded first
	*/

	FAssertMsg(getDelayedCacheInitialized() == false, "delayedCacheAtStart() not expected to be called before reset");
	
	//Builder Wagon | Builder Pack
	unsigned char aiBuilderPackYieldRequire[NUM_YIELD_TYPES]; //Allocate max size possible
	for (unsigned char iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
{
		aiBuilderPackYieldRequire[iYield] = (unsigned char)getUnitInfo(UNIT_BUILDER_WAGON).getYieldCost(iYield);
	}

	//Get the yield cost for the builder pack by looking for builder wagon costs
	unsigned char ucBuilderPackToolCost(0);
	unsigned char ucBuilderPackLumberCost(0);
	ucBuilderPackToolCost = getUnitInfo(UNIT_BUILDER_WAGON).getYieldCost(YIELD_TOOLS);
	ucBuilderPackLumberCost = getUnitInfo(UNIT_BUILDER_WAGON).getYieldCost(YIELD_LUMBER);

	//Copy to Globals:
	COPY(m_aiBuilderPackYieldRequire, aiBuilderPackYieldRequire, unsigned char); //Builder Wagon | Builder Pack
	m_ucBuilderPackToolCost = ucBuilderPackToolCost; //Builder Wagon | Builder Pack
	m_ucBuilderPackLumberCost = ucBuilderPackLumberCost; //Builder Wagon | Builder Pack

	setDelayedCacheInitialized(true); //Disallow future calls...
}

void CvGlobals::uninitDelayedCacheAtStart()
{	/*
	Called during CvGame::uninit() or CvGame::reset() (the later calls also the first)
	Delete cached values.
	*/
	SAFE_DELETE_ARRAY(m_aiBuilderPackYieldRequire);
}

void CvGlobals::resetDelayedCacheAtStart(bool bConstructorCall)
{	/*
	Called during CvGame::reset() with its boolean bConstructorCall as argument.
	Reset cached values.
	*/
	if (!bConstructorCall)
	{
		//Builder Wagon | Builder Pack
		FAssertMsg(m_aiBuilderPackYieldRequire==NULL, "about to leak memory, CvGlobals::m_aiBuilderPackYieldRequire");
		m_aiBuilderPackYieldRequire = new unsigned char[NUM_YIELD_TYPES];
		for (unsigned char iI = 0; iI < NUM_YIELD_TYPES; iI++)
		{
			m_aiBuilderPackYieldRequire[iI] = 0;
		}
	}

	m_ucBuilderPackToolCost = 0; //Builder Wagon | Builder Pack
	m_ucBuilderPackLumberCost = 0; //Builder Wagon | Builder Pack

	//Reset Cache calls
	setDelayedCacheInitialized(false); //Allow future calls...
}
//TODO write and read the cache in savegames?
//DOANE Cache Globals:
void CvGlobals::forceCacheGlobals()
{	/*	Forces cacheGlobals() to load values directly from FVariable system if possible
		and then remove any cached variable from it*/
	setCacheClean(true);
	cacheGlobals();
}
//DOANE Global Define Booleans: allow to load bool variables directly from the runtime Var System
bool CvGlobals::getDefineBOOL( const char * szName ) const
{	/*Load from FVar system*/
	bool bReturn = false;
	GC.getDefinesVarSystem()->GetValue( szName, bReturn );
	return bReturn;
}

bool CvGlobals::getDefineBOOL( const char * szName, bool* bReturn, bool bRemove)
{	/*Load and remove variable from FVar system or just pass directly the input value*/
	if (bRemove)
	{	
		bool bNewReturn = *bReturn;
		GC.getDefinesVarSystem()->GetValueB( szName, bNewReturn , bRemove );
		return bNewReturn;
	}
	return *bReturn;	
}

void CvGlobals::setDefineBOOL( const char * szName, bool bValue, bool bForceCache )
{	/*Creates or modifies a define var. Can force cache to load new value from FVar System.*/
	if (bForceCache)
	{	
		setCacheClean(true);
	}
	GC.getDefinesVarSystem()->SetValue( szName, bValue );
	cacheGlobals();
}

//DOANE Global Define Signed Char: allow to load signed char (i1) variables (one byte) directly from the runtime Var System
signed char CvGlobals::getDefineSCHAR( const char * szName ) const
{	/*Load from FVar system*/
	char scReturn = 0;
	GC.getDefinesVarSystem()->GetValue( szName, scReturn );
	return (signed char)scReturn;
}

signed char CvGlobals::getDefineSCHAR( const char * szName, signed char* scReturn,  bool bRemove )
{	/*Load and remove variable from FVar system or just pass directly the input value*/
	if (bRemove)
	{	
		char scNewReturn = *scReturn;
		GC.getDefinesVarSystem()->GetValueSC( szName, scNewReturn , bRemove );
		return (signed char)scNewReturn;
	}
	return *scReturn;
}

void CvGlobals::setDefineSCHAR( const char * szName, signed char ucValue, bool bForceCache )
{	/*Creates or modifies a define var. Can force cache to load new value from FVar System.*/
	if (bForceCache)
	{	
		setCacheClean(true);
	}
	GC.getDefinesVarSystem()->SetValue( szName, ucValue );
	cacheGlobals();
}

//DOANE Global Define Unsigned Char: allow to load unsigned char (ui1) variables (one byte) directly from the runtime Var System
unsigned char CvGlobals::getDefineUCHAR( const char * szName ) const
{	/*Load from FVar system*/
	unsigned char ucReturn = 0;
	GC.getDefinesVarSystem()->GetValue( szName, ucReturn );
	return ucReturn;
}

unsigned char CvGlobals::getDefineUCHAR( const char * szName, unsigned char* ucReturn,  bool bRemove )
{	/*Load and remove variable from FVar system or just pass directly the input value*/
	if (bRemove)
	{	
		unsigned char ucNewReturn = *ucReturn;
		GC.getDefinesVarSystem()->GetValueUC( szName, ucNewReturn , bRemove );
		return ucNewReturn;
	}
	return *ucReturn;
}

void CvGlobals::setDefineUCHAR( const char * szName, unsigned char ucValue, bool bForceCache )
{	/*Creates or modifies a define var. Can force cache to load new value from FVar System.*/
	if (bForceCache)
	{	
		setCacheClean(true);
	}
	GC.getDefinesVarSystem()->SetValue( szName, ucValue );
	cacheGlobals();
}

//DOANE Global Define Short: allow to load short (i2) variables (two bytes) directly from the runtime Var System
short CvGlobals::getDefineSSHORT( const char * szName ) const
{	/*Load from FVar system*/
	short wReturn = 0;
	GC.getDefinesVarSystem()->GetValue( szName, wReturn );
	return wReturn;
}

short CvGlobals::getDefineSSHORT( const char * szName, short* wReturn,  bool bRemove )
{	/*Load and remove variable from FVar system or just pass directly the input value*/
	if (bRemove)
	{	
		short wNewReturn = *wReturn;
		GC.getDefinesVarSystem()->GetValueSS( szName, wNewReturn , bRemove );
		return wNewReturn;
	}
	return *wReturn;
}

void CvGlobals::setDefineSSHORT( const char * szName, short wValue, bool bForceCache )
{	/*Creates or modifies a define var. Can force cache to load new value from FVar System.*/
	if (bForceCache)
	{	
		setCacheClean(true);
	}
	GC.getDefinesVarSystem()->SetValue( szName, wValue );
	cacheGlobals();
}

//DOANE Global Define Unsigned Short: allow to load unsigned short (ui2) variables (two bytes) directly from the runtime Var System
unsigned short CvGlobals::getDefineUSHORT( const char * szName ) const
{	/*Load from FVar system*/
	unsigned short uwReturn = 0;
	GC.getDefinesVarSystem()->GetValue( szName, uwReturn );
	return uwReturn;
}

unsigned short CvGlobals::getDefineUSHORT( const char * szName, unsigned short* uwReturn,  bool bRemove )
{	/*Load and remove variable from FVar system or just pass directly the input value*/
	if (bRemove)
	{	
		unsigned short uwNewReturn = *uwReturn;
		GC.getDefinesVarSystem()->GetValueUS( szName, uwNewReturn , bRemove );
		return uwNewReturn;
	}
	return *uwReturn;
}

void CvGlobals::setDefineUSHORT( const char * szName, unsigned short uwValue, bool bForceCache )
{	/*Creates or modifies a define var. Can force cache to load new value from FVar System.*/
	if (bForceCache)
	{	
		setCacheClean(true);
	}
	GC.getDefinesVarSystem()->SetValue( szName, uwValue );
	cacheGlobals();
}

//DOANE Global Define Signed Int: allow to load signed int (i4) variables (four bytes) directly from the runtime Var System
int CvGlobals::getDefineSINT( const char * szName, int* iReturn,  bool bRemove )
{	/*Load and remove variable from FVar system or just pass directly the input value*/
	if (bRemove)
	{	
		int iNewReturn = *iReturn;
		GC.getDefinesVarSystem()->GetValueSI( szName, iNewReturn , bRemove );
		return iNewReturn;
	}
	return *iReturn;
}

void CvGlobals::setDefineSINT( const char * szName, int iValue, bool bForceCache )
{	/*Creates or modifies a define var. Can force cache to load new value from FVar System.*/
	if (bForceCache)
	{	
		setCacheClean(true);
	}
	GC.getDefinesVarSystem()->SetValue( szName, iValue );
	cacheGlobals();
}

//DOANE Global Define Unigned Int: allow to load unsigned int (ui4) variables (four bytes) directly from the runtime Var System
uint CvGlobals::getDefineUINT( const char * szName ) const
{	/*Load from FVar system*/
	uint uiReturn = 0;
	GC.getDefinesVarSystem()->GetValue( szName, uiReturn );
	return uiReturn;
}

uint CvGlobals::getDefineUINT( const char * szName, uint* uiReturn,  bool bRemove )
{	/*Load and remove variable from FVar system or just pass directly the input value*/
	if (bRemove)
	{	
		uint uiNewReturn = *uiReturn;
		GC.getDefinesVarSystem()->GetValueUI( szName, uiNewReturn , bRemove );
		return uiNewReturn;
	}
	return *uiReturn;
}

void CvGlobals::setDefineUINT( const char * szName, uint uiValue, bool bForceCache )
{	/*Creates or modifies a define var. Can force cache to load new value from FVar System.*/
	if (bForceCache)
	{	
		setCacheClean(true);
	}
	GC.getDefinesVarSystem()->SetValue( szName, uiValue );
	cacheGlobals();
}
///////////////////////////////////////////////////////////////////////////
////////*/						//END DOANE							*//////
///////////////////////////////////////////////////////////////////////////

void CvGlobals::setDefineINT( const char * szName, int iValue )
{
	GC.getDefinesVarSystem()->SetValue( szName, iValue );
	cacheGlobals();
}

void CvGlobals::setDefineFLOAT( const char * szName, float fValue )
{
	GC.getDefinesVarSystem()->SetValue( szName, fValue );
	cacheGlobals();
}

void CvGlobals::setDefineSTRING( const char * szName, const char * szValue )
{
	GC.getDefinesVarSystem()->SetValue( szName, szValue );
	cacheGlobals();
}

int CvGlobals::getMOVE_DENOMINATOR()
{
	return m_iMOVE_DENOMINATOR;
}

int CvGlobals::getFOOD_CONSUMPTION_PER_POPULATION()
{
	return m_iFOOD_CONSUMPTION_PER_POPULATION;
}

int CvGlobals::getMAX_HIT_POINTS()
{
	return m_iMAX_HIT_POINTS;
}

int CvGlobals::getHILLS_EXTRA_DEFENSE()
{
	return m_iHILLS_EXTRA_DEFENSE;
}

int CvGlobals::getRIVER_ATTACK_MODIFIER()
{
	return m_iRIVER_ATTACK_MODIFIER;
}

int CvGlobals::getAMPHIB_ATTACK_MODIFIER()
{
	return m_iAMPHIB_ATTACK_MODIFIER;
}

int CvGlobals::getHILLS_EXTRA_MOVEMENT()
{
	return m_iHILLS_EXTRA_MOVEMENT;
}

int CvGlobals::getPEAK_EXTRA_MOVEMENT()
{
	return m_iPEAK_EXTRA_MOVEMENT;
}

int CvGlobals::getMAX_PLOT_LIST_ROWS()
{
	return m_iMAX_PLOT_LIST_ROWS;
}

int CvGlobals::getUNIT_MULTISELECT_MAX()
{
	return m_iUNIT_MULTISELECT_MAX;
}

int CvGlobals::getEVENT_MESSAGE_TIME()
{
	return m_iEVENT_MESSAGE_TIME;
}

int CvGlobals::getROUTE_FEATURE_GROWTH_MODIFIER()
{
	return m_iROUTE_FEATURE_GROWTH_MODIFIER;
}

int CvGlobals::getFEATURE_GROWTH_MODIFIER()
{
	return m_iFEATURE_GROWTH_MODIFIER;
}

int CvGlobals::getMIN_CITY_RANGE()
{
	return m_iMIN_CITY_RANGE;
}

int CvGlobals::getCITY_MAX_NUM_BUILDINGS()
{
	return m_iCITY_MAX_NUM_BUILDINGS;
}
int CvGlobals::getLAKE_MAX_AREA_SIZE()
{
	return m_iLAKE_MAX_AREA_SIZE;
}
int CvGlobals::getMIN_WATER_SIZE_FOR_OCEAN()
{
	return m_iMIN_WATER_SIZE_FOR_OCEAN;
}

int CvGlobals::getFORTIFY_MODIFIER_PER_TURN()
{
	return m_iFORTIFY_MODIFIER_PER_TURN;
}

int CvGlobals::getMAX_CITY_DEFENSE_DAMAGE()
{
	return m_iMAX_CITY_DEFENSE_DAMAGE;
}

int CvGlobals::getPEAK_SEE_THROUGH_CHANGE()
{
	return m_iPEAK_SEE_THROUGH_CHANGE;
}

int CvGlobals::getHILLS_SEE_THROUGH_CHANGE()
{
	return m_iHILLS_SEE_THROUGH_CHANGE;
}

int CvGlobals::getSEAWATER_SEE_FROM_CHANGE()
{
	return m_iSEAWATER_SEE_FROM_CHANGE;
}

int CvGlobals::getPEAK_SEE_FROM_CHANGE()
{
	return m_iPEAK_SEE_FROM_CHANGE;
}

int CvGlobals::getHILLS_SEE_FROM_CHANGE()
{
	return m_iHILLS_SEE_FROM_CHANGE;
}

int CvGlobals::getMAX_REBEL_YIELD_MODIFIER()
{
	return m_iMAX_REBEL_YIELD_MODIFIER;
}

float CvGlobals::getCAMERA_MIN_YAW()
{
	return m_fCAMERA_MIN_YAW;
}

float CvGlobals::getCAMERA_MAX_YAW()
{
	return m_fCAMERA_MAX_YAW;
}

float CvGlobals::getCAMERA_FAR_CLIP_Z_HEIGHT()
{
	return m_fCAMERA_FAR_CLIP_Z_HEIGHT;
}

float CvGlobals::getCAMERA_MAX_TRAVEL_DISTANCE()
{
	return m_fCAMERA_MAX_TRAVEL_DISTANCE;
}

float CvGlobals::getCAMERA_START_DISTANCE()
{
	return m_fCAMERA_START_DISTANCE;
}

float CvGlobals::getPLOT_SIZE()
{
	return m_fPLOT_SIZE;
}

float CvGlobals::getCAMERA_SPECIAL_PITCH()
{
	return m_fCAMERA_SPECIAL_PITCH;
}

float CvGlobals::getCAMERA_MAX_TURN_OFFSET()
{
	return m_fCAMERA_MAX_TURN_OFFSET;
}

float CvGlobals::getCAMERA_MIN_DISTANCE()
{
	return m_fCAMERA_MIN_DISTANCE;
}

float CvGlobals::getCAMERA_UPPER_PITCH()
{
	return m_fCAMERA_UPPER_PITCH;
}

float CvGlobals::getCAMERA_LOWER_PITCH()
{
	return m_fCAMERA_LOWER_PITCH;
}

float CvGlobals::getFIELD_OF_VIEW()
{
	return m_fFIELD_OF_VIEW;
}

float CvGlobals::getUNIT_MULTISELECT_DISTANCE()
{
	return m_fUNIT_MULTISELECT_DISTANCE;
}

///////////////////////////////////////
////////*		DOANE Cache		*//////
///////////////////////////////////////
//DOANE enrol unit
unsigned char CvGlobals::getMAX_ENROL_UNIT()
{
	return m_ucMAX_ENROL_UNIT;
}

//DOANE enrol unit
unsigned char CvGlobals::getMAX_MILITARY_PROFESSION_TURN()
{
	return m_ucMAX_MILITARY_PROFESSION_TURN;
}

//DOANE enrol unit
unsigned char CvGlobals::getSTART_MILITARY_PROFESSION_PENALTY()
{
	return m_ucSTART_MILITARY_PROFESSION_PENALTY;
}

//DOANE fortification
unsigned char CvGlobals::getFORTIFICATION_BREAK_DAMAGE()
{
	return m_ucFORTIFICATION_BREAK_DAMAGE;
}

unsigned char CvGlobals::getPOWERFUL_FORTIFICATION_PENALTY()
{
	return m_ucPOWERFUL_FORTIFICATION_PENALTY;
}

int CvGlobals::getGOLD_BY_POPULATION_BY_TURN()
{
	return m_iGOLD_BY_POPULATION_BY_TURN;
}

int CvGlobals::getNUM_DISCOVERED_MAP_TILES_BEFORE_DELIVERING()
{
	return m_iNUM_DISCOVERED_MAP_TILES_BEFORE_DELIVERING;
}

//DOANE Economy
int CvGlobals::getMAX_MANUFACTURED_YIELD_AMOUNT()
{
	return m_iMAX_MANUFACTURED_YIELD_AMOUNT;
}

//DOANE Builder Pack
unsigned char CvGlobals::getBUILDER_PACK_YIELD_COST_PERCENT()
{
	return m_ucBUILDER_PACK_YIELD_COST_PERCENT;
}

unsigned char CvGlobals::getBuilderPackToolCost()
{
	return m_ucBuilderPackToolCost;
}

unsigned char CvGlobals::getBuilderPackLumberCost()
{
	return m_ucBuilderPackLumberCost;
}

//DOANE Survivors on Transport Sinking
unsigned char CvGlobals::getSURVIVORS_ON_TRANSPORT_SINKING_PROBABILITY()
{
	return m_ucSURVIVORS_ON_TRANSPORT_SINKING_PROBABILITY;
}

unsigned char CvGlobals::getNUM_TRADE_ROUTE_HIST_TURN() const
{	
	return m_ucNUM_TRADE_ROUTE_HIST_TURN;
}

unsigned char CvGlobals::getAVANCED_NEW_ERA_BOOST_FOR_CONSTRUCTION() const
{	
	return m_ucAVANCED_NEW_ERA_BOOST_FOR_CONSTRUCTION;
}

unsigned char CvGlobals::getMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_AVAILABLE() const
{
	return m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_AVAILABLE;
}

unsigned char CvGlobals::getMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_DOCK() const
{
	return m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_DOCK;
}

unsigned char CvGlobals::getMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_CARGO() const
{
	return m_ucMIN_TURN_TO_DELETE_IMMIGRANT_EUROPE_CARGO;
}

unsigned char CvGlobals::getPROBABILITY_NATURAL_IMMIGRATION_START() const
{
	return m_ucPROBABILITY_NATURAL_IMMIGRATION_START;
}

unsigned char CvGlobals::getPROBABILITY_NATURAL_IMMIGRATION_LATE() const
{
	return m_ucPROBABILITY_NATURAL_IMMIGRATION_LATE;
}

unsigned char CvGlobals::getPROBABILITY_NATURAL_IMMIGRATION_PERCENT() const
{
	return m_ucPROBABILITY_NATURAL_IMMIGRATION_PERCENT;
}

int CvGlobals::getTURN_NATURAL_IMMIGRATION_CHANGE() const
{
	return m_iTURN_NATURAL_IMMIGRATION_CHANGE;
}

unsigned char CvGlobals::getNAVAL_TRANSPORT_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY() const
{
	return m_ucNAVAL_TRANSPORT_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY;
}

unsigned char CvGlobals::getNAVAL_MILITARY_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY() const
{
	return m_ucNAVAL_MILITARY_DISTURBATION_FOR_ENEMY_CLOSE_TO_COLONY;
}

unsigned char CvGlobals::getNAVAL_DISTURBATION_FOR_TRADE_ROUTE() const
{
	return m_ucNAVAL_DISTURBATION_FOR_TRADE_ROUTE;
}

unsigned char CvGlobals::getNAVAL_DISTURBATION_FOR_MILITARY_SHIP_KILLED_PROTECTING_A_CITY() const
{
	return m_ucNAVAL_DISTURBATION_FOR_MILITARY_SHIP_KILLED_PROTECTING_A_CITY;
}

unsigned char CvGlobals::getEXPENDITURE_BY_LAND_FORCE_INSIDE_COLONIES() const
{
	return m_ucEXPENDITURE_BY_LAND_FORCE_INSIDE_COLONIES;
}

unsigned char CvGlobals::getEXPENDITURE_BY_LAND_FORCE_OUTSIDE_COLONIES() const
{
	return m_ucEXPENDITURE_BY_LAND_FORCE_OUTSIDE_COLONIES;
}

unsigned char CvGlobals::getEXPENDITURE_BY_UNEMPLOYED() const
{
	return m_ucEXPENDITURE_BY_UNEMPLOYED;
}

unsigned char CvGlobals::getEXPENDITURE_BY_SCOUT() const
{
	return m_ucEXPENDITURE_BY_SCOUT;
}

unsigned char CvGlobals::getEXPENDITURE_BY_AGRONOMIST() const
{
	return m_ucEXPENDITURE_BY_AGRONOMIST;
}

unsigned char CvGlobals::getEXPENDITURE_BY_PIONEER() const
{
	return m_ucEXPENDITURE_BY_PIONEER;
}

unsigned char CvGlobals::getEXPENDITURE_BY_MERCHANT_SHIP() const
{
	return m_ucEXPENDITURE_BY_MERCHANT_SHIP;
}

unsigned char CvGlobals::getEXPENDITURE_BY_MILITARY_SHIP() const
{
	return m_ucEXPENDITURE_BY_MILITARY_SHIP;
}

///////////////////////////////////////////////////////////////
////////*	DOANE Python Callbacks | Speed Improvement	*//////
///////////////////////////////////////////////////////////////
//Vanilla Callbacks:
/*	WARNING: These callbacks actually used by the "exe" so it isn't possible to
	modify them because the game doesn't load, this applies to DLL and schema changes.
*/
int CvGlobals::getUSE_FINISH_TEXT_CALLBACK()
{
	return m_iUSE_FINISH_TEXT_CALLBACK;
}

int CvGlobals::getUSE_ON_UNIT_SET_XY_CALLBACK()
{
	return m_iUSE_ON_UNIT_SET_XY_CALLBACK;
}

int CvGlobals::getUSE_ON_UNIT_SELECTED_CALLBACK()
{
	return m_iUSE_ON_UNIT_SELECTED_CALLBACK;
}

int CvGlobals::getUSE_ON_MISSIONARY_CONVERTED_UNIT_CALLBACK()
{
	return m_iUSE_ON_MISSIONARY_CONVERTED_UNIT_CALLBACK;
}

int CvGlobals::getUSE_ON_UPDATE_CALLBACK()
{
	return m_iUSE_ON_UPDATE_CALLBACK;
}

int CvGlobals::getUSE_ON_UNIT_CREATED_CALLBACK()
{
	return m_iUSE_ON_UNIT_CREATED_CALLBACK;
}

int CvGlobals::getUSE_ON_UNIT_LOST_CALLBACK()
{
	return m_iUSE_ON_UNIT_LOST_CALLBACK;
}
//Rest not needed since they use inline functions instead! -->CvGlobals.h
///////////////////////////////////////////////////////////////
////////*				END DOANE 						*//////
///////////////////////////////////////////////////////////////


int CvGlobals::getMAX_CIV_PLAYERS()
{
	return MAX_PLAYERS;
}

int CvGlobals::getMAX_PLAYERS()
{
	return MAX_PLAYERS;
}

int CvGlobals::getMAX_CIV_TEAMS()
{
	return MAX_TEAMS;
}

int CvGlobals::getMAX_TEAMS()
{
	return MAX_TEAMS;
}

int CvGlobals::getINVALID_PLOT_COORD()
{
	return INVALID_PLOT_COORD;
}

int CvGlobals::getNUM_CITY_PLOTS()
{
	return NUM_CITY_PLOTS;
}

int CvGlobals::getCITY_HOME_PLOT()
{
	return CITY_HOME_PLOT;
}

void CvGlobals::setDLLIFace(CvDLLUtilityIFaceBase* pDll)
{
	m_pDLL = pDll;
}

int CvGlobals::getNUM_ENGINE_DIRTY_BITS() const
{
	return NUM_ENGINE_DIRTY_BITS;
}

int CvGlobals::getNUM_INTERFACE_DIRTY_BITS() const
{
	return NUM_INTERFACE_DIRTY_BITS;
}

int CvGlobals::getNUM_YIELD_TYPES() const
{
	return NUM_YIELD_TYPES;
}

int CvGlobals::getNUM_FORCECONTROL_TYPES() const
{
	return NUM_FORCECONTROL_TYPES;
}

int CvGlobals::getNUM_INFOBAR_TYPES() const
{
	return NUM_INFOBAR_TYPES;
}

int CvGlobals::getNUM_HEALTHBAR_TYPES() const
{
	return NUM_HEALTHBAR_TYPES;
}

int CvGlobals::getNUM_CONTROL_TYPES() const
{
	return NUM_CONTROL_TYPES;
}

int CvGlobals::getNUM_LEADERANIM_TYPES() const
{
	return NUM_LEADERANIM_TYPES;
}


void CvGlobals::deleteInfoArrays()
{
	for (int i=0;i<(int)m_aInfoVectors.size();i++)
	{
		deleteInfoArray(*m_aInfoVectors[i]);
	}
	//DOANE UnitArtStyles | Androrc: this probably isn't necessary but just to be safe.
	deleteInfoArray(m_paUnitArtStyleTypeInfo);
	deleteInfoArray(m_paContactInfo);
	deleteInfoArray(m_paAttitudeInfo);
	deleteInfoArray(m_paMemoryInfo);
	//END DOANE
	SAFE_DELETE_ARRAY(GC.getEntityEventTypes());
	SAFE_DELETE_ARRAY(GC.getAnimationOperatorTypes());
	SAFE_DELETE_ARRAY(GC.getFunctionTypes());
	SAFE_DELETE_ARRAY(GC.getArtStyleTypes());
	SAFE_DELETE_ARRAY(GC.getCitySizeTypes());
	SAFE_DELETE_ARRAY(GC.getContactTypes());
	SAFE_DELETE_ARRAY(GC.getDiplomacyPowerTypes());
	SAFE_DELETE_ARRAY(GC.getAutomateTypes());
	SAFE_DELETE_ARRAY(GC.getDirectionTypes());
	SAFE_DELETE_ARRAY(GC.getFootstepAudioTypes());
	SAFE_DELETE_ARRAY(GC.getFootstepAudioTags());

	clearTypesMap();
	m_aInfoVectors.clear();
}


//
// Global Infos Hash Map
//

int CvGlobals::getInfoTypeForString(const char* szType, bool hideAssert) const
{
	FAssertMsg(szType, "null info type string");
	InfosMap::const_iterator it = m_infosMap.find(szType);
	if (it!=m_infosMap.end())
	{
		return it->second;
	}

	if (!hideAssert)
	{
		if (_tcscmp(szType,"NONE")!=0 && _tcscmp(szType,"")!=0)//DOANE
		{
			CvString szError;
			szError.Format("info type %s not found, Current XML file is: %s", szType, GC.getCurrentXMLFile().GetCString());
			FAssertMsg(strcmp(szType, "NONE")==0 || strcmp(szType, "")==0, szError.c_str());
			gDLL->logMsg("xml.log", szError);
		}//END DOANE
	}

	return -1;
}

void CvGlobals::setInfoTypeFromString(const char* szType, int idx)
{
	FAssertMsg(szType, "null info type string");
#ifdef _DEBUG
	InfosMap::const_iterator it = m_infosMap.find(szType);
	int iExisting = (it!=m_infosMap.end()) ? it->second : -1;
	FAssertMsg(iExisting==-1 || iExisting==idx || strcmp(szType, "ERROR")==0, CvString::format("xml info type entry %s already exists", szType).c_str());
#endif
	m_infosMap[szType] = idx;
}

void CvGlobals::infoTypeFromStringReset()
{
	m_infosMap.clear();
}

void CvGlobals::addToInfosVectors(void *infoVector)
{
	std::vector<CvInfoBase *> *infoBaseVector = (std::vector<CvInfoBase *> *) infoVector;
	m_aInfoVectors.push_back(infoBaseVector);
}

void CvGlobals::infosReset()
{
	for (int i=0;i<(int)m_aInfoVectors.size();i++)
	{
		std::vector<CvInfoBase *> *infoBaseVector = m_aInfoVectors[i];
		for (int j=0;j<(int)infoBaseVector->size();j++)
			infoBaseVector->at(j)->reset();
	}
}

int CvGlobals::getNumDirections() const { return NUM_DIRECTION_TYPES; }
int CvGlobals::getNumResourceLayers() const { return NUM_RESOURCE_LAYERS; }
int CvGlobals::getNumUnitLayerOptionTypes() const { return NUM_UNIT_LAYER_OPTION_TYPES; }
int CvGlobals::getNumGameOptions() const { return NUM_GAMEOPTION_TYPES; }
int CvGlobals::getNumMPOptions() const { return NUM_MPOPTION_TYPES; }
int CvGlobals::getNumSpecialOptions() const { return NUM_SPECIALOPTION_TYPES; }
int CvGlobals::getNumGraphicOptions() const { return NUM_GRAPHICOPTION_TYPES; }
int CvGlobals::getNumTradeableItems() const { return NUM_TRADEABLE_ITEMS; }
int CvGlobals::getNumBasicItems() const { return NUM_BASIC_ITEMS; }
int CvGlobals::getNumTradeableHeadings() const { return NUM_TRADEABLE_HEADINGS; }
int CvGlobals::getNumCommandInfos() const { return NUM_COMMAND_TYPES; }
int CvGlobals::getNumControlInfos() const { return NUM_CONTROL_TYPES; }
int CvGlobals::getNumMissionInfos() const { return NUM_MISSION_TYPES; }
int CvGlobals::getNumPlayerOptionInfos() const { return NUM_PLAYEROPTION_TYPES; }
int CvGlobals::getMaxNumSymbols() const { return MAX_NUM_SYMBOLS; }
int CvGlobals::getNumGraphicLevels() const { return NUM_GRAPHICLEVELS; }

//
// non-inline versions
//
CvMap& CvGlobals::getMap()
{ return *m_map; }
CvGameAI& CvGlobals::getGame()
{ return *m_game; }
CvGameAI *CvGlobals::getGamePointer()
{ return m_game; }

int CvGlobals::getMaxCivPlayers() const
{
	return MAX_PLAYERS;
}

bool CvGlobals::IsGraphicsInitialized() const { return m_bGraphicsInitialized;}
void CvGlobals::SetGraphicsInitialized(bool bVal)
{ m_bGraphicsInitialized = bVal;}
void CvGlobals::setInterface(CvInterface* pVal)
{ m_interface = pVal; }
void CvGlobals::setDiplomacyScreen(CvDiplomacyScreen* pVal)
{ m_diplomacyScreen = pVal; }
void CvGlobals::setMPDiplomacyScreen(CMPDiplomacyScreen* pVal)
{ m_mpDiplomacyScreen = pVal; }
void CvGlobals::setMessageQueue(CMessageQueue* pVal)
{ m_messageQueue = pVal; }
void CvGlobals::setHotJoinMessageQueue(CMessageQueue* pVal)
{ m_hotJoinMsgQueue = pVal; }
void CvGlobals::setMessageControl(CMessageControl* pVal)
{ m_messageControl = pVal; }
void CvGlobals::setSetupData(CvSetupData* pVal)
{ m_setupData = pVal; }
void CvGlobals::setMessageCodeTranslator(CvMessageCodeTranslator* pVal)
{ m_messageCodes = pVal; }
void CvGlobals::setDropMgr(CvDropMgr* pVal)
{ m_dropMgr = pVal; }
void CvGlobals::setPortal(CvPortal* pVal)
{ m_portal = pVal; }
void CvGlobals::setStatsReport(CvStatsReporter* pVal)
{ m_statsReporter = pVal; }
void CvGlobals::setPathFinder(FAStar* pVal)
{ m_pathFinder = pVal; }
void CvGlobals::setInterfacePathFinder(FAStar* pVal)
{ m_interfacePathFinder = pVal; }
void CvGlobals::setStepFinder(FAStar* pVal)
{ m_stepFinder = pVal; }
void CvGlobals::setRouteFinder(FAStar* pVal)
{ m_routeFinder = pVal; }
void CvGlobals::setBorderFinder(FAStar* pVal)
{ m_borderFinder = pVal; }
void CvGlobals::setAreaFinder(FAStar* pVal)
{ m_areaFinder = pVal; }
CvDLLUtilityIFaceBase* CvGlobals::getDLLIFaceNonInl()
{ return m_pDLL; }

// PatchMod: Achievements START
int CvGlobals::getNumAchieveInfos()
{
	return (int)m_paAchieveInfo.size();
}

std::vector<CvAchieveInfo*>& CvGlobals::getAchieveInfo()
{
	return m_paAchieveInfo;
}

CvAchieveInfo& CvGlobals::getAchieveInfo(AchieveTypes eAchieve)
{
	FAssert(eAchieve > -1);
	FAssert(eAchieve < GC.getNumAchieveInfos());
	return *(m_paAchieveInfo[eAchieve]);
}
// PatchMod: Achievements END