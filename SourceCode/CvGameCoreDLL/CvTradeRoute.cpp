#include "CvGameCoreDLL.h"
#include "CvPlayerAI.h"
#include "CvTradeRoute.h"
#include "CvInfos.h"
#include "CvConvoy.h"
#include "CvDLLInterfaceIFaceBase.h"

CvTradeRoute::CvTradeRoute() :
	m_iId(ANYWHERE_CITY_ID),
	m_szName(L"")
{
	m_aYields = new char[NUM_YIELD_TYPES];
	m_aTotalProfitsYield = new unsigned int[NUM_YIELD_TYPES];

	m_ppRadioOption = NULL;
	m_ppEuropeanRadioOption = NULL;
	m_ppCheckBoxOption = NULL;

	reset(0, true);	
}

CvTradeRoute::~CvTradeRoute()
{
	SAFE_DELETE_ARRAY(m_aYields);
	SAFE_DELETE_ARRAY(m_aTotalProfitsYield);

	if (m_ppRadioOption != NULL)
	{
		for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
		{
			SAFE_DELETE_ARRAY(m_ppRadioOption[iI]);
		}
		SAFE_DELETE_ARRAY(m_ppRadioOption);
	}
	if (m_ppEuropeanRadioOption != NULL)
	{
		for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
		{
			SAFE_DELETE_ARRAY(m_ppEuropeanRadioOption[iI]);
		}
		SAFE_DELETE_ARRAY(m_ppEuropeanRadioOption);
	}
	if (m_ppCheckBoxOption != NULL)
	{
		for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
		{
			SAFE_DELETE_ARRAY(m_ppCheckBoxOption[iI]);
		}
		SAFE_DELETE_ARRAY(m_ppCheckBoxOption);
	}
}

void CvTradeRoute::init(int iID, const IDInfo& kSourceCity, const IDInfo& kDestinationCity)
{
	reset(iID);
	m_kSourceCity = kSourceCity;
	m_kDestinationCity = kDestinationCity;
}

void CvTradeRoute::reset(int iID, bool bConstructorCall)
{
	m_iId = iID;
	m_iGoldAmountToAlwaysConserve = 200; // Hardcoded I know. TODO move it to settings

	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		m_aYields[iI] = NO_TRADE;		
		m_aTotalProfitsYield[iI] = 0;
	}

	if (!bConstructorCall)
	{
		FAssertMsg(m_ppRadioOption == NULL, "about to leak memory, CvTradeRoute::m_ppRadioOption");
		m_ppRadioOption = new OptionInfo*[NUM_YIELD_TYPES];
		for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
		{
			m_ppRadioOption[iI] = new OptionInfo[NUM_TRADE_ROUTE_RADIO_OPTIONS];
			for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_OPTIONS; iJ++)
			{
				m_ppRadioOption[iI][iJ].isActivate = false;
				m_ppRadioOption[iI][iJ].iQuantity = -1;
			}
		}

		FAssertMsg(m_ppEuropeanRadioOption == NULL, "about to leak memory, CvTradeRoute::m_ppEuropeanRadioOption");
		m_ppEuropeanRadioOption = new AdvancedOptionInfo*[NUM_YIELD_TYPES];
		for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
		{
			m_ppEuropeanRadioOption[iI] = new AdvancedOptionInfo[NUM_TRADE_ROUTE_RADIO_EUROPEAN_OPTIONS];
			for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_EUROPEAN_OPTIONS; iJ++)
			{
				m_ppEuropeanRadioOption[iI][iJ].isActivate = false;
				m_ppEuropeanRadioOption[iI][iJ].iQuantity = -1;
				m_ppEuropeanRadioOption[iI][iJ].iQuantityPerTurn = -1;
			}
		}

		FAssertMsg(m_ppCheckBoxOption == NULL, "about to leak memory, CvTradeRoute::m_ppCheckBoxOption");
		m_ppCheckBoxOption = new OptionInfo*[NUM_YIELD_TYPES];
		for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
		{
			m_ppCheckBoxOption[iI] = new OptionInfo[NUM_TRADE_ROUTE_CHECK_BOX_OPTIONS];
			for (int iJ = 0; iJ < NUM_TRADE_ROUTE_CHECK_BOX_OPTIONS; iJ++)
			{
				m_ppCheckBoxOption[iI][iJ].isActivate = false;
				m_ppCheckBoxOption[iI][iJ].iQuantity = -1;
			}
		}		
		m_aiConcoys.clear();
	}
}

int CvTradeRoute::getID() const
{
	return m_iId;
}

void CvTradeRoute::setID(int iId)
{
	m_iId = iId;
}

int CvTradeRoute::getGoldAmountToAlwaysConserve() const
{
	return m_iGoldAmountToAlwaysConserve;
}

void CvTradeRoute::setGoldAmountToAlwaysConserve(int iValue)
{
	m_iGoldAmountToAlwaysConserve = iValue;
}

const wchar* CvTradeRoute::getNameKey() const
{
	return m_szName.GetCString();
}

const CvWString & CvTradeRoute::getName() const
{
	return m_szName;
}

void CvTradeRoute::setName(const CvWString & szName)
{
	m_szName = szName;
}

CvCity* CvTradeRoute::getSourceOrDestinationCity() const
{
	if (getSourceCity().iID != EUROPE_CITY_ID)
	{
		return ::getCity(getSourceCity());
	}

	if (getDestinationCity().iID != EUROPE_CITY_ID)
	{
		return ::getCity(getDestinationCity());
	}

	return NULL;
}

const IDInfo& CvTradeRoute::getSourceCity() const
{
	return m_kSourceCity;
}

void CvTradeRoute::setSourceCity(const IDInfo& kCity)
{
	if (getSourceCity() != kCity)
	{
		m_kSourceCity = kCity;
	}
}

const wchar* CvTradeRoute::getSourceCityNameKey() const
{
	if (getSourceCity().iID == EUROPE_CITY_ID)
	{
		return L"TXT_KEY_CONCEPT_EUROPE";
	}

	CvCity* pCity = ::getCity(getSourceCity());
	FAssert(pCity != NULL);
	if (pCity != NULL)
	{
		return pCity->getNameKey();
	}

	return L"";
}

const IDInfo& CvTradeRoute::getDestinationCity() const
{
	return m_kDestinationCity;
}

void CvTradeRoute::setDestinationCity(const IDInfo& kCity)
{
	if (getDestinationCity() != kCity)
	{
		m_kDestinationCity = kCity;
	}
}

const wchar* CvTradeRoute::getDestinationCityNameKey() const
{
	if (getDestinationCity().iID == EUROPE_CITY_ID)
	{
		return L"TXT_KEY_CONCEPT_EUROPE";
	}

	CvCity* pCity = ::getCity(getDestinationCity());
	FAssert(pCity != NULL);
	if (pCity != NULL)
	{
		return pCity->getNameKey();
	}

	return L"";
}

bool CvTradeRoute::isEuropeanRoute() const
{
	return getSourceCity().iID == EUROPE_CITY_ID || getDestinationCity().iID == EUROPE_CITY_ID;
}

const wchar* CvTradeRoute::getCityNameKey() const
{
	return (getDestinationCity().iID == EUROPE_CITY_ID) ? getSourceCityNameKey() : getDestinationCityNameKey();
}

PlayerTypes CvTradeRoute::getOwner() const
{
	return getSourceCity().eOwner;
}

TradeTypes CvTradeRoute::getTradeType(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return (TradeTypes)m_aYields[eYield];
}

void CvTradeRoute::setTradeType(YieldTypes eYield, TradeTypes eValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getTradeType(eYield) != eValue)
	{
		m_aYields[eYield] = eValue;
		if (getTradeType(eYield) != NO_TRADE)
		{
			bool bAllDesactivate = true;
			for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_OPTIONS; iJ++)
			{
				if (m_ppRadioOption[eYield][iJ].isActivate)
				{					
					bAllDesactivate = false;
				}
			}
			if (bAllDesactivate)
			{
				setRadioOption(eYield, SEND_EVERYTHING_TO, true);
				setEuropeanRadioOption(eYield, DO_NOTHING, true);
			}
		}		
	}
}

void CvTradeRoute::ensureTheNeeds(YieldTypes eYield, TradeTypes eTrade, int iQuantityToEnsure)
{
	setTradeType(eYield, eTrade);
	setRadioOption(eYield, ENSURE_THE_NEEDS, true);
	setRadioOptionQuantity(eYield, iQuantityToEnsure);
}

void CvTradeRoute::buyUntilQuantityReached(YieldTypes eYield, int iQuantityToReach, int iMaxQuantityByTravel) 
{
	setTradeType(eYield, TRADE_IMPORT);
	setEuropeanRadioOption(eYield, BUY_UNTIL_QUANTITY_REACHED, true);
	setEuropeanRadioOptionQuantity(eYield, iQuantityToReach);

	//Do not take more than this quantity into a ship
	setCheckBoxOption(eYield, MAINTAIN_QUANTITY, true);
	setCheckBoxOptionQuantity(eYield, MAINTAIN_QUANTITY, iMaxQuantityByTravel);
}

unsigned int CvTradeRoute::getTotalProfits() const
{
	unsigned int uiTotal = 0;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		uiTotal += getTotalProfitsYield((YieldTypes)iYield);
	}
	return uiTotal;
}

unsigned int CvTradeRoute::getTotalProfitsYield(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return (TradeTypes)m_aTotalProfitsYield[eYield];
}

void CvTradeRoute::setTotalProfitsYield(YieldTypes eYield, unsigned int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (getTotalProfitsYield(eYield) != iValue)
	{
		m_aTotalProfitsYield[eYield] = iValue;
	}
}

void CvTradeRoute::changeTotalProfitsYield(YieldTypes eYield, unsigned int iChange)
{
	setTotalProfitsYield(eYield, getTotalProfitsYield(eYield) + iChange);
}

TradeRouteRadioTypes CvTradeRoute::getSelectedRadioOption(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);

	for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_OPTIONS; iJ++)
	{
		if (m_ppRadioOption[eYield][iJ].isActivate)
		{
			return (TradeRouteRadioTypes) iJ;
		}
	}
	FAssert(false);
	return NO_TRADE_ROUTE_RADIO_OPTION;
}

OptionInfo CvTradeRoute::getRadioOption(YieldTypes eYield, TradeRouteRadioTypes eType) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	FAssert(eType > -1);
	FAssert(eType < NUM_TRADE_ROUTE_RADIO_OPTIONS);

	return m_ppRadioOption[eYield][eType];
}

void CvTradeRoute::setRadioOption(YieldTypes eYield, TradeRouteRadioTypes eType, bool bValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	FAssert(eType > -1);
	FAssert(eType < NUM_TRADE_ROUTE_RADIO_OPTIONS);

	if (getRadioOption(eYield, eType).isActivate != bValue)
	{
		for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_OPTIONS; iJ++)
		{
			m_ppRadioOption[eYield][iJ].isActivate = false;
		}
		m_ppRadioOption[eYield][eType].isActivate = bValue;
	}
}

void CvTradeRoute::setRadioOptionQuantity(YieldTypes eYield, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_OPTIONS; iJ++)
	{
		if (m_ppRadioOption[eYield][iJ].isActivate)
		{
			if (getRadioOption(eYield, (TradeRouteRadioTypes)iJ).iQuantity != iValue)
			{
				m_ppRadioOption[eYield][iJ].iQuantity = iValue;
			}
		}
	}
}

void CvTradeRoute::changeRadioOptionQuantity(YieldTypes eYield, TradeRouteRadioTypes eType, int iChange)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	FAssert(eType == SEND_FIXED_QUANTITY);
	m_ppRadioOption[eYield][eType].iQuantity += iChange;
	if (m_ppRadioOption[eYield][eType].iQuantity == 0)
	{
		m_ppRadioOption[eYield][eType].isActivate = false;
		setTradeType(eYield, NO_TRADE);
	}
}

/// European Radio Option

TradeRouteRadioEuropeanTypes CvTradeRoute::getSelectedEuropeanRadioOption(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);

	for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_EUROPEAN_OPTIONS; iJ++)
	{
		if (m_ppEuropeanRadioOption[eYield][iJ].isActivate)
		{
			return (TradeRouteRadioEuropeanTypes) iJ;
		}
	}
	FAssert(false);
	return NO_TRADE_ROUTE_RADIO_EUROPEAN_OPTION;
}

AdvancedOptionInfo CvTradeRoute::getEuropeanRadioOption(YieldTypes eYield, TradeRouteRadioEuropeanTypes eType) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	FAssert(eType > -1);
	FAssert(eType < NUM_TRADE_ROUTE_RADIO_EUROPEAN_OPTIONS);

	return m_ppEuropeanRadioOption[eYield][eType];
}

void CvTradeRoute::setEuropeanRadioOption(YieldTypes eYield, TradeRouteRadioEuropeanTypes eType, bool bValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	FAssert(eType > -1);
	FAssert(eType < NUM_TRADE_ROUTE_RADIO_EUROPEAN_OPTIONS);

	if (getEuropeanRadioOption(eYield, eType).isActivate != bValue)
	{
		for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_EUROPEAN_OPTIONS; iJ++)
		{
			m_ppEuropeanRadioOption[eYield][iJ].isActivate = false;
		}
		m_ppEuropeanRadioOption[eYield][eType].isActivate = bValue;
	}
}

void CvTradeRoute::setEuropeanRadioOptionQuantity(YieldTypes eYield, int iQuantity)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_EUROPEAN_OPTIONS; iJ++)
	{
		if (m_ppEuropeanRadioOption[eYield][iJ].isActivate)
		{
			if (getEuropeanRadioOption(eYield, (TradeRouteRadioEuropeanTypes)iJ).iQuantity != iQuantity)
			{
				m_ppEuropeanRadioOption[eYield][iJ].iQuantity = iQuantity;
			}
		}
	}
}

void CvTradeRoute::changeEuropeanRadioOptionQuantity(YieldTypes eYield, TradeRouteRadioEuropeanTypes eType, int iChange)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	m_ppEuropeanRadioOption[eYield][eType].iQuantityPerTurnLeft -= iChange;
	if (eType == BUY_QUANTITY)
	{
		m_ppEuropeanRadioOption[eYield][eType].iQuantity += iChange;
		if (m_ppEuropeanRadioOption[eYield][eType].iQuantity == 0)
		{
			m_ppEuropeanRadioOption[eYield][eType].iQuantityPerTurnLeft = 0;
			m_ppEuropeanRadioOption[eYield][eType].isActivate = false;
			setEuropeanRadioOption(eYield, DO_NOTHING, true);
			setTradeType(eYield, NO_TRADE);
		}
	}
}

//CHECKBOXES

OptionInfo CvTradeRoute::getCheckBoxOption(YieldTypes eYield, TradeRouteCheckBoxTypes eType) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	FAssert(eType > -1);
	FAssert(eType < NUM_TRADE_ROUTE_CHECK_BOX_OPTIONS);

	return m_ppCheckBoxOption[eYield][eType];
}

void CvTradeRoute::setCheckBoxOption(YieldTypes eYield, TradeRouteCheckBoxTypes eType, bool bValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	FAssert(eType > -1);
	FAssert(eType < NUM_TRADE_ROUTE_CHECK_BOX_OPTIONS);

	if (getCheckBoxOption(eYield, eType).isActivate != bValue)
	{
		m_ppCheckBoxOption[eYield][eType].isActivate = bValue;
	}
}

void CvTradeRoute::setCheckBoxOptionQuantity(YieldTypes eYield, TradeRouteCheckBoxTypes eType, int iValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	FAssert(eType > -1);
	FAssert(eType < NUM_TRADE_ROUTE_CHECK_BOX_OPTIONS);
	if (getCheckBoxOption(eYield, eType).iQuantity != iValue)
	{
		m_ppCheckBoxOption[eYield][eType].iQuantity = iValue;
	}
}

bool CvTradeRoute::checkValid(PlayerTypes ePlayer) const
{
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	if (!kPlayer.canLoadYield(getSourceCity().eOwner))
	{
		return false;
	}

	if (!kPlayer.canLoadYield(getDestinationCity().eOwner))
	{
		return false;
	}

	return true;
}

int CvTradeRoute::getLoadedCargoPercent() const
{
	int iAverageLoad = 0;
	int iTotalSpace = 0;
	
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			int iLoadedCargoPercent = pConvoy->getLoadedCargoPercent();
			if (iLoadedCargoPercent > 0)
			{
				int iCargoSpace = pConvoy->getCargoNewSpace();
				iAverageLoad += iLoadedCargoPercent * iCargoSpace;
				iTotalSpace += iCargoSpace;
			}
		}
	}

	if (iTotalSpace == 0)
	{
		return 0;
	}

	return iAverageLoad/iTotalSpace;

}

int CvTradeRoute::getLoadedCargoHistPercent() const
{
	int iAverageLoad = 0;
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	int iTotalSpace = 0;

	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			int iLoadedCargoPercent = pConvoy->getLoadedCargoHistPercent();
			if (iLoadedCargoPercent > 0)
			{
				int iCargoSpace = pConvoy->getCargoNewSpace();
				iAverageLoad += iLoadedCargoPercent * iCargoSpace;
				iTotalSpace += iCargoSpace;
			}
		}
	}

	if (iTotalSpace == 0)
	{
		return 0;
	}

	return iAverageLoad/iTotalSpace;

}

int CvTradeRoute::getTradeProfits() const
{
	int iTotalTradeProfits = 0;
	CvPlayer& kPlayer = GET_PLAYER(getOwner());

	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			int iTradeProfits = pConvoy->getTradeProfits();
			if (iTradeProfits > 0)
			{
				iTotalTradeProfits += iTradeProfits;
			}
		}
	}
	return iTotalTradeProfits;
}

int CvTradeRoute::getTradeProfitsHist() const
{
	int iTotalTradeProfits = 0;
	CvPlayer& kPlayer = GET_PLAYER(getOwner());

	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			int iTradeProfits = pConvoy->getTradeProfitsHist();
			if (iTradeProfits > 0)
			{
				iTotalTradeProfits += iTradeProfits;
			}
		}
	}
	return iTotalTradeProfits;
}

int CvTradeRoute::getTransportUnitsCapacity() const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	int iTotalCapacity = 0;

	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			iTotalCapacity += pConvoy->getCargoNewSpace();
		}
	}

	return iTotalCapacity;
}

int CvTradeRoute::getYieldValue(YieldTypes eYield) const
{
	int iYieldValue = 0;
	CvPlayerAI& kPlayer = GET_PLAYER(getOwner());
	PlayerTypes eEuropePlayer = kPlayer.getParent();
	if (eEuropePlayer == NO_PLAYER)
	{
		return 0;
	}
	CvPlayer& kEuropePlayer = GET_PLAYER(eEuropePlayer);

	CvYieldInfo& kYield = GC.getYieldInfo(eYield);
	if (!kYield.isCargo())
	{
		return 0;
	}
	for (int iI = 0; iI <= 1; ++iI)
	{
		int iTempValue = 0;
		bool bIsEurope = (iI == 0 ? getSourceCity().iID : getDestinationCity().iID) == EUROPE_CITY_ID;
		if (bIsEurope)
		{
			CvCity* pCity = ::getCity(iI == 1 ? getSourceCity() : getDestinationCity());
			if (pCity != NULL)
			{
				switch(eYield)
				{
				case YIELD_FOOD:
				case YIELD_LUMBER:
				case YIELD_ORE:
				case YIELD_TOOLS:
					{
						int iYieldStored = pCity->getYieldStored(eYield);
						int iYieldNet = pCity->calculateNetYield(eYield);
						if (iYieldStored == 0 || iYieldNet < 0 && iYieldStored/(-iYieldNet) < 3)
						{
							iTempValue = iYieldStored == 0 ? 10 : -iYieldNet; //iYieldNet is neg value so we add to the value
						}
						break;
					}

				case YIELD_SWORDS:
					if (kPlayer.getGold() > 3000 && kPlayer.getGold() < 100000)
					{
						iTempValue = 5;
					}
					break;
				case YIELD_MUSKETS:
				case YIELD_CANNON:
				case YIELD_AMMUNITION:
					if (kPlayer.getGold() > 30000)
					{
						iTempValue = 10;
					}
					break;
				case YIELD_HORSES:
				case YIELD_TRADE_GOODS:
					{
						int iYieldStored = pCity->getYieldStored(eYield);
						int iYieldNet = pCity->calculateNetYield(eYield);
						if (iYieldNet == 0 && iYieldStored < 30)
						{
							iTempValue = 5;
						}
						break;
					}					
				}
			}
		}
		else
		{
			CvCity* pCity = ::getCity(iI == 0 ? getSourceCity() : getDestinationCity());
			if (pCity != NULL)
			{
				int iYieldStored = pCity->getYieldStored(eYield);
				int iYieldNet = pCity->calculateNetYield(eYield);
				if (kPlayer.AI_isYieldForSale(eYield))
				{
					int iPrice = kPlayer.getSellPriceForYield(eYield);
					iTempValue = iPrice * std::min(iYieldStored + iYieldNet, 200); // Useless to calculate if we more than 200 yields it is a good one :)
				}
				else
				{
					iTempValue = iYieldStored + iYieldNet;
				}
			}
		}
		iYieldValue += std::max(iTempValue, 0);
	}
	return iYieldValue;
}

bool CvTradeRoute::isRadioOptionWithQuantity(TradeRouteRadioTypes eType) const
{
	switch(eType)
	{
	case SEND_FIXED_QUANTITY:
	case ENSURE_THE_NEEDS:
		return true;
	}
	return false;
}

bool CvTradeRoute::isCheckBoxOptionWithQuantity(TradeRouteCheckBoxTypes eType) const
{
	switch(eType)
	{
	case DO_NOT_EXCEED:
	case ALWAYS_KEEP:
	case MAINTAIN_QUANTITY:
		return true;
	}
	return false;
}

int CvTradeRoute::getNumYieldsByTradeTypes(TradeTypes eTrade) const
{
	int iCount = 0;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (getTradeType(eYield) == eTrade)
		{
			iCount++;
		}
	}
	return iCount;
}

TransportTypes CvTradeRoute::getTransportType() const
{
	int iNumWagons = 0;
	int iNumFleets = 0;
	
	CvPlayerAI& kPlayer = GET_PLAYER(getOwner());
	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			if (pConvoy->getDomainType() == DOMAIN_LAND)
			{
				iNumWagons++;
			}
			else
			{
				iNumFleets++;
			}
		}
	}

	if (iNumWagons > 0 && iNumFleets > 0)
	{
		return SHIP_AND_WAGON;
	}

	if (iNumFleets > 0)
	{
		return ONLY_SHIP;		
	}

	if (iNumWagons > 0)
	{
		return ONLY_WAGON;
	}

	return NO_TRANSPORT;
}

void CvTradeRoute::calculateRequiredUnits(int& requiredMerchantUnits, int& requiredMilitaryUnits, DomainTypes domainType) const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			pConvoy->calculateRequiredUnits(requiredMerchantUnits, requiredMilitaryUnits, domainType);
		}
	}
}

CvConvoy* CvTradeRoute::AI_getBestConvoyForUnit(UnitTypes eUnit, int& iScore) const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvConvoy* pBestConvoy = NULL;
	bool isOnlyDefensive = GC.getUnitInfo(eUnit).isOnlyDefensive();
	DomainTypes domainType = (DomainTypes) GC.getUnitInfo(eUnit).getDomainType();
	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			int requiredMerchantUnits = 0;
			int requiredMilitaryUnits = 0;
			pConvoy->calculateRequiredUnits(requiredMerchantUnits, requiredMilitaryUnits, domainType, true, eUnit);
			int iCurrentScore = isOnlyDefensive ? requiredMerchantUnits : requiredMilitaryUnits;
			if (iCurrentScore > iScore )
			{
				iScore = iCurrentScore;
				pBestConvoy = pConvoy;
			}
		}
	}
	return pBestConvoy;
}

void CvTradeRoute::doTurn()
{
	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_EUROPEAN_OPTIONS; iJ++)
		{
			m_ppEuropeanRadioOption[iI][iJ].iQuantityPerTurnLeft = 0;
		}
	}
	
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	if (kPlayer.isEuropeanAI())
	{
		AI_processConvoys();
	}

	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			pConvoy->doTurn();
		}
	}
}

//----------------------
//	START Convoys
//----------------------

CvConvoy* CvTradeRoute::addConvoy()
{	
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvConvoy* pConvoy = kPlayer.addConvoy();

	if (pConvoy != NULL)
	{
		pConvoy->setAssignedTradeRouteId(getID());
		m_aiConcoys.push_back(pConvoy->getID());
	}
	return pConvoy;
}

void CvTradeRoute::removeConvoy(int iConvoyId)
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvConvoy* pConvoy = kPlayer.getConvoy(iConvoyId);
	if (pConvoy == NULL)
	{
		return;
	}
	pConvoy->detach();

	for (std::vector<int>::iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		if (iConvoyId == *it)
		{
			m_aiConcoys.erase(it);
		}
	}

	kPlayer.removeConvoy(iConvoyId);
}

int CvTradeRoute::getNumUnitsAffectedToRoute() const
{
	int numUnits = 0;
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			numUnits += pConvoy->getNumUnits();
		}
	}
	return numUnits;
}

void CvTradeRoute::AI_processConvoys()
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	for (std::vector<int>::const_iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		CvConvoy* pConvoy = kPlayer.getConvoy(*it);
		if (pConvoy != NULL)
		{
			if (!pConvoy->isFullCapacity())
			{
				return;
			}
		}
	}
	addConvoy();
}

//-------------------
//	End convoys
//-------------------

void CvTradeRoute::read(FDataStreamBase* pStream)
{
	// Init data before load
	reset();

	uint uiFlag = 0;
	pStream->Read(&uiFlag);	// flags for expansion

	pStream->Read(&m_iId);
	pStream->Read(&m_iGoldAmountToAlwaysConserve);

	m_kSourceCity.read(pStream);
	m_kDestinationCity.read(pStream);
	pStream->ReadString(m_szName);
	pStream->Read(NUM_YIELD_TYPES, m_aYields);
	pStream->Read(NUM_YIELD_TYPES, m_aTotalProfitsYield);

	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_OPTIONS; iJ++)
		{
			m_ppRadioOption[iI][iJ].read(pStream);
		}
	}
	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_EUROPEAN_OPTIONS; iJ++)
		{
			m_ppEuropeanRadioOption[iI][iJ].read(pStream);
		}
	}
	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		for (int iJ = 0; iJ < NUM_TRADE_ROUTE_CHECK_BOX_OPTIONS; iJ++)
		{
			m_ppCheckBoxOption[iI][iJ].read(pStream);
		}
	}

	int iSize;
	m_aiConcoys.clear();
	pStream->Read(&iSize);
	for (int i = 0; i < iSize; ++i)
	{
		int iID;
		pStream->Read(&iID);
		m_aiConcoys.push_back(iID);
	}
}

void CvTradeRoute::write(FDataStreamBase* pStream)
{
	uint uiFlag = 0;
	pStream->Write(uiFlag);		// flag for expansion

	pStream->Write(m_iId);
	pStream->Write(m_iGoldAmountToAlwaysConserve);

	m_kSourceCity.write(pStream);
	m_kDestinationCity.write(pStream);

	pStream->WriteString(m_szName);
	pStream->Write(NUM_YIELD_TYPES, m_aYields);
	pStream->Write(NUM_YIELD_TYPES, m_aTotalProfitsYield);

	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_OPTIONS; iJ++)
		{
			m_ppRadioOption[iI][iJ].write(pStream);
		}
	}
	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		for (int iJ = 0; iJ < NUM_TRADE_ROUTE_RADIO_EUROPEAN_OPTIONS; iJ++)
		{
			m_ppEuropeanRadioOption[iI][iJ].write(pStream);
		}
	}
	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		for (int iJ = 0; iJ < NUM_TRADE_ROUTE_CHECK_BOX_OPTIONS; iJ++)
		{
			m_ppCheckBoxOption[iI][iJ].write(pStream);
		}
	}
	pStream->Write(m_aiConcoys.size());
	for (std::vector<int>::iterator it = m_aiConcoys.begin(); it != m_aiConcoys.end(); ++it)
	{
		pStream->Write(*it);
	}
}