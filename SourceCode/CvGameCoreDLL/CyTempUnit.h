#pragma once
#ifndef CyTempUnit_h
#define CyTempUnit_h
#include "CyPlot.h"
//
// Python wrapper class for CyTempUnit
//

class CvTempUnit;

class CyTempUnit
{
public:
	CyTempUnit();

	DllExport CyTempUnit(CvTempUnit* pTempUnit);		// Call from C++
	CvTempUnit* getTempUnit() { return m_pTempUnit;	};	// Call from C++
	const CvTempUnit* getTempUnit() const { return m_pTempUnit;	};	// Call from C++
	bool isNone() { return (m_pTempUnit==NULL); }
	
	int getID();	
	int getGameTurnCreated() const;
	int getPrice() const ;
	int getDamage() const;

	bool requestTraining() const;
	
	int /*UnitTypes*/ getUnitType();
	int /*ProfessionTypes*/ getProfession();
	int /*ImmigrationTypes*/ getImmigrationType();
	
	std::string getFullLengthIcon() const;
protected:
	CvTempUnit* m_pTempUnit;
};
#endif	// #ifndef CyTempUnit
