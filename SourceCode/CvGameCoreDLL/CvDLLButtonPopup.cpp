// buttonPopup.cpp

#include "CvGameCoreDLL.h"
#include "CvDLLButtonPopup.h"
#include "CvGlobals.h"
#include "CvPlayerAI.h"
#include "CvGameAI.h"
#include "CvTeamAI.h"
#include "CvMap.h"
#include "CvPlot.h"
#include "CvArtFileMgr.h"
#include "CyCity.h"
#include "CvTradeRoute.h"
#include "CyArgsList.h"
#include "CvPopupReturn.h"
#include "CvInfos.h"
#include "CvInitCore.h"
#include "CvGameTextMgr.h"
#include "CvDLLPythonIFaceBase.h"
#include "CvDLLInterfaceIFaceBase.h"
#include "CvDLLEntityIFaceBase.h"
#include "CvGameCoreUtils.h"
#include "CvDLLEngineIFaceBase.h"
#include "CvDLLEventReporterIFaceBase.h"
#include "CvTrade.h"
#include "CvHospitals.h"
#include "CvAgreement.h"
#include "CvTempUnit.h"
#include <time.h>
// Public Functions...
#include <stdio.h>

#define PASSWORD_DEFAULT (L"*****")

CvDLLButtonPopup* CvDLLButtonPopup::m_pInst = NULL;

CvDLLButtonPopup& CvDLLButtonPopup::getInstance()
{
	if (m_pInst == NULL)
	{
		m_pInst = new CvDLLButtonPopup;
	}
	return *m_pInst;
}

void CvDLLButtonPopup::freeInstance()
{
	delete m_pInst;
	m_pInst = NULL;
}

CvDLLButtonPopup::CvDLLButtonPopup()
{
}


CvDLLButtonPopup::~CvDLLButtonPopup()
{
}

void CvDLLButtonPopup::OnAltExecute(CvPopup& popup, const PopupReturn& popupReturn, CvPopupInfo &info)
{
	switch (info.getButtonPopupType())
	{
	case BUTTONPOPUP_CHOOSE_PROFESSION:
		{
			CvPopupInfo* pInfo = new CvPopupInfo;
			if (pInfo)
			{
				*pInfo = info;
				gDLL->getInterfaceIFace()->addPopup(pInfo);
				gDLL->getInterfaceIFace()->popupSetAsCancelled(&popup);
			}
		}
	default:
		break;
	}
}


void CvDLLButtonPopup::OnEscape(CvPopup& popup, CvPopupInfo &info)
{
	switch (info.getButtonPopupType())
	{
	case BUTTONPOPUP_MAIN_MENU:
	case BUTTONPOPUP_CHOOSE_PROFESSION:
	case BUTTONPOPUP_PURCHASE_EUROPE_UNIT:
	case BUTTONPOPUP_LOADUNIT:
	case BUTTONPOPUP_LOAD_CARGO:
	case BUTTONPOPUP_DIPLOMACY:
	case BUTTONPOPUP_FREE_COLONY:
	case BUTTONPOPUP_PROMOTE:
	case BUTTONPOPUP_EMBARK_ON_SHIP:
	case BUTTONPOPUP_LOAD_CREW:
	case BUTTONPOPUP_GIVE_MAP:
	case BUTTONPOPUP_TRANSFERT_MAP_TO_EUROPE:
	case BUTTONPOPUP_AGREEMENT_LIST:
	case BUTTONPOPUP_SEEDLING_LIST:
	case BUTTONPOPUP_CHANGE_CITY_AGREEMENT:
	case BUTTONPOPUP_STOP_TRADE:
	case BUTTONPOPUP_SELL_SHIP:
	case BUTTONPOPUP_STOP_SELL_SHIP:
	case BUTTONPOPUP_RUM_DISCOVERED:
	case BUTTONPOPUP_ACHIEVEMENTS:
	case BUTTONPOPUP_FOUND_COLONY:
	case BUTTONPOPUP_WHEN_NEW_ERA:
	case BUTTONPOPUP_OPEN_BREACH_FORTIFICATION:
	case BUTTONPOPUP_RESUPPLY_SHIPS:
	case BUTTONPOPUP_TUTORIAL_DOANE_0:
	case BUTTONPOPUP_TUTORIAL_DOANE_1:
	case BUTTONPOPUP_TUTORIAL_DOANE_2:
	case BUTTONPOPUP_SELECT_YIELD_AMOUNT:
	case BUTTONPOPUP_DETAILS:
	case BUTTONPOPUP_ADMIN_PASSWORD:
	case BUTTONPOPUP_ADMIN:
	case BUTTONPOPUP_TALK_NATIVES:
	case BUTTONPOPUP_VICTORY_POPUP:
	case BUTTONPOPUP_FEAT:
	case BUTTONPOPUP_CONFIRMTASK:
	case BUTTONPOPUP_CONFIRMCOMMAND:
	case BUTTONPOPUP_TRADE_ROUTE_ADD_RESOURCE_POPUP:
		gDLL->getInterfaceIFace()->popupSetAsCancelled(&popup);
		break;
	default:
		break;
	}
}


void CvDLLButtonPopup::OnOkClicked(CvPopup* pPopup, PopupReturn *pPopupReturn, CvPopupInfo &info)
{
	int iExamineCityID;

	switch (info.getButtonPopupType())
	{
	case BUTTONPOPUP_TEXT:
		break;

	case BUTTONPOPUP_CONFIRM_MENU:
		if ( pPopupReturn->getButtonClicked() == 0 )
		{
			switch (info.getData1())
			{
			case 0:
				gDLL->SetDone(true);
				break;
			case 1:
				gDLL->getInterfaceIFace()->exitingToMainMenu();
				break;
			case 2:
				GC.getGameINLINE().doControl(CONTROL_RETIRE);
				break;
			case 3:
				GC.getGameINLINE().regenerateMap();
				break;
			case 4:
				GC.getGameINLINE().doControl(CONTROL_WORLD_BUILDER);
				break;
			}
		}
		break;

	case BUTTONPOPUP_MAIN_MENU:
		if (pPopupReturn->getButtonClicked() == 0)
		{	// exit to desktop
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CONFIRM_MENU);
			if (NULL != pInfo)
			{
				pInfo->setData1(0);
				gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), true);
			}
		}
		else if (pPopupReturn->getButtonClicked() == 1)
		{	// exit to main menu
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CONFIRM_MENU);
			if (NULL != pInfo)
			{
				pInfo->setData1(1);
				gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), true);
			}
		}
		else if (pPopupReturn->getButtonClicked() == 2)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CONFIRM_MENU);
			if (NULL != pInfo)
			{
				pInfo->setData1(2);
				gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), true);
			}
		}
		else if (pPopupReturn->getButtonClicked() == 3)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CONFIRM_MENU);
			if (NULL != pInfo)
			{
				pInfo->setData1(3);
				gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), true);
			}
		}
		else if (pPopupReturn->getButtonClicked() == 4)
		{	// load game
			GC.getGameINLINE().doControl(CONTROL_LOAD_GAME);
		}
		else if (pPopupReturn->getButtonClicked() == 5)
		{	// save game
			GC.getGameINLINE().doControl(CONTROL_SAVE_NORMAL);
		}
		else if (pPopupReturn->getButtonClicked() == 6)
		{	// options
			gDLL->getPythonIFace()->callFunction("CvScreensInterface", "showOptionsScreen");
		}
		else if (pPopupReturn->getButtonClicked() == 7)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CONFIRM_MENU);
			if (NULL != pInfo)
			{
				pInfo->setData1(4);
				gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), true);
			}
		}
		else if (pPopupReturn->getButtonClicked() == 8)
		{	// Game details
			GC.getGameINLINE().doControl(CONTROL_ADMIN_DETAILS);
		}
		else if (pPopupReturn->getButtonClicked() == 9)
		{	// player details
			GC.getGameINLINE().doControl(CONTROL_DETAILS);
		}
		// 10 - cancel
		break;

	case BUTTONPOPUP_DECLAREWARMOVE:
		if (pPopupReturn->getButtonClicked() == 0)
		{
			gDLL->sendChangeWar((TeamTypes)info.getData1(), true);
		}
		if (((pPopupReturn->getButtonClicked() == 0) || info.getOption2()) && info.getFlags() == 0)
		{
			GC.getGameINLINE().selectionListGameNetMessage(GAMEMESSAGE_PUSH_MISSION, MISSION_MOVE_TO, info.getData2(), info.getData3(), info.getFlags(), false, info.getOption1());
		}
		break;

	case BUTTONPOPUP_CONFIRMCOMMAND:
		if (pPopupReturn->getButtonClicked() == 0)
		{
			int iAction = info.getData1();
			GC.getGameINLINE().selectionListGameNetMessage(GAMEMESSAGE_DO_COMMAND, GC.getActionInfo(iAction).getCommandType(), GC.getActionInfo(iAction).getCommandData(), -1, 0, info.getOption1());
		}
		break;

	case BUTTONPOPUP_CONFIRMTASK:
		if (pPopupReturn->getButtonClicked() == 0)
		{
			gDLL->sendDoTask(info.getData1(), (TaskTypes) info.getData3(), info.getData2(), -1, false, false, false, false);
		}
		break;

	case BUTTONPOPUP_LOADUNIT:
		if (pPopupReturn->getButtonClicked() != 0)
		{
			CLLNode<IDInfo>* pUnitNode;
			CvSelectionGroup* pSelectionGroup;
			CvUnit* pLoopUnit;
			CvPlot* pPlot;
			int iCount;

			pSelectionGroup = gDLL->getInterfaceIFace()->getSelectionList();

			if (NULL != pSelectionGroup)
			{
				pPlot = pSelectionGroup->plot();

				iCount = pPopupReturn->getButtonClicked();

				pUnitNode = pPlot->headUnitNode();

				while (pUnitNode != NULL)
				{
					pLoopUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = pPlot->nextUnitNode(pUnitNode);

					if (pSelectionGroup->canDoCommand(COMMAND_LOAD_UNIT, pLoopUnit->getOwnerINLINE(), pLoopUnit->getID()))
					{
						iCount--;
						if (iCount == 0)
						{
							GC.getGameINLINE().selectionListGameNetMessage(GAMEMESSAGE_DO_COMMAND, COMMAND_LOAD_UNIT, pLoopUnit->getOwnerINLINE(), pLoopUnit->getID());
							break;
						}
					}
				}
			}
		}
		break;

	case BUTTONPOPUP_LOAD_CARGO:
		if (pPopupReturn->getButtonClicked() >= 0 && pPopupReturn->getButtonClicked() < NUM_YIELD_TYPES)
		{
			CvSelectionGroup* pSelectionGroup = gDLL->getInterfaceIFace()->getSelectionList();

			if (NULL != pSelectionGroup)
			{
				YieldTypes eYield = (YieldTypes) pPopupReturn->getButtonClicked();
				int iWantedValue = pPopupReturn->getSpinnerWidgetValue(eYield);
				CvUnit* pTransportWithSpace = NULL;
				//find transport that can carry yield
				int iNumUnits = pSelectionGroup->getNumUnits();
				for (int i = 0; i < iNumUnits; ++i)
				{
					CvUnit* pTransport = pSelectionGroup->getUnitAt(i);
					if (NULL != pTransport)
					{
						if (pTransport->canLoadYield(pTransport->plot(), eYield, false))
						{
							int iMaxAmount = pTransport->getMaxLoadYieldAmount(eYield, iWantedValue, -1);
							if (iMaxAmount > 0)
							{
								int iSpace = pTransport->getSpaceNewCargo();
								gDLL->sendDoCommand(pTransport->getID(), COMMAND_LOAD_YIELD, eYield, iMaxAmount, false);
								iWantedValue -= iMaxAmount;
								iSpace -= iMaxAmount;
								if (iSpace > 0)
								{
									pTransportWithSpace = pTransport;
								} 
							}
						}
					}
				}
				if (pTransportWithSpace != NULL)
				{
					gDLL->sendDoCommand(pTransportWithSpace->getID(), COMMAND_LOAD_CARGO, -1, -1, false);
				}
			}
		}
		break;

	case BUTTONPOPUP_LEADUNIT:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pLeaderUnit = kPlayer.getUnit(info.getData1());
			CvUnit* pSelectedUnit = kPlayer.getUnit(pPopupReturn->getButtonClicked());
			if (pSelectedUnit != NULL)
			{
				gDLL->sendPushMission(pLeaderUnit->getID(), MISSION_LEAD, pSelectedUnit->getID(), -1, 0, false);
			}
		}
		break;
	case BUTTONPOPUP_RAZECITY:
		if (pPopupReturn->getButtonClicked() == 1)
		{
			gDLL->sendDoTask(info.getData1(), TASK_RAZE, -1, -1, false, false, false, false);
		}
		else if (pPopupReturn->getButtonClicked() == 2)
		{
			CvCity* pCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(info.getData1());
			if (NULL != pCity)
			{
				gDLL->getEventReporterIFace()->cityAcquiredAndKept(GC.getGameINLINE().getActivePlayer(), pCity);
			}

			gDLL->sendDoTask(info.getData1(), TASK_GIFT, info.getData2(), -1, false, false, false, false);
		}
		else if (pPopupReturn->getButtonClicked() == 0)
		{
			CvCity* pCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(info.getData1());
			if (NULL != pCity)
			{
				pCity->chooseProduction();
				gDLL->getEventReporterIFace()->cityAcquiredAndKept(GC.getGameINLINE().getActivePlayer(), pCity);
			}
		}
		break;

	case BUTTONPOPUP_CHOOSEPRODUCTION:
		iExamineCityID = 0;
		iExamineCityID = std::max(iExamineCityID, GC.getNumUnitInfos());
		iExamineCityID = std::max(iExamineCityID, GC.getNumBuildingInfos());

		if (pPopupReturn->getButtonClicked() == iExamineCityID)
		{
			CvCity* pCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(info.getData1());
			if (pCity != NULL)
			{
				gDLL->getInterfaceIFace()->selectCity(pCity, true);
			}
		}
		break;

	case BUTTONPOPUP_CHOOSE_YIELD_BUILD:
		if (pPopupReturn->getButtonClicked() >= GC.getNumUnitInfos())
		{
			BuildingTypes eBuilding = (BuildingTypes) (pPopupReturn->getButtonClicked() - GC.getNumUnitInfos());
			gDLL->sendDoTask(info.getData1(), TASK_PUSH_CONSTRUCT_BUILDING, eBuilding, -1, false, false, false, false);
		}
		else if (pPopupReturn->getButtonClicked() >= 0)
		{
			UnitTypes eUnit = (UnitTypes) pPopupReturn->getButtonClicked();
			gDLL->sendDoTask(info.getData1(), TASK_PUSH_TRAIN_UNIT, eUnit, NO_UNITAI, false, false, false, false);
		}
		break;

	case BUTTONPOPUP_CHOOSE_EDUCATION:
		if (pPopupReturn->getButtonClicked() == GC.getNumUnitInfos())
		{
			CvCity* pCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(info.getData1());
			if (pCity != NULL)
			{
				gDLL->sendDoTask(pCity->getID(), TASK_CHANGE_PROFESSION, info.getData2(), PROFESSION_COLONIST, false, false, false, false);
			}
		}
		else if (pPopupReturn->getButtonClicked() >= 0 && pPopupReturn->getButtonClicked() < GC.getNumUnitInfos())
		{
			gDLL->sendDoTask(info.getData1(), TASK_EDUCATESTART, info.getData2(), pPopupReturn->getButtonClicked(), false, false, false, false);
		}
		else if (pPopupReturn->getButtonClicked() > GC.getNumUnitInfos())
		{
			int iProfession = pPopupReturn->getButtonClicked() - GC.getNumUnitInfos() - 1;
			gDLL->sendDoTask(info.getData1(), TASK_EDUCATESTART, info.getData2(), iProfession, true, false, false, false);
		}
		break;

	case BUTTONPOPUP_ALARM:
		break;

	case BUTTONPOPUP_DEAL_CANCELED:
		if (pPopupReturn->getButtonClicked() == 0)
		{
			gDLL->sendKillDeal(info.getData1(), info.getOption1(), GC.getGameINLINE().getActiveTeam());
		}
		break;

	case BUTTONPOPUP_PYTHON:
	case BUTTONPOPUP_FEAT:
		if (!info.getOnClickedPythonCallback().IsEmpty())
		{
			FAssertMsg(!GC.getGameINLINE().isNetworkMultiPlayer(), "Danger: Out of Sync");
			CyArgsList argsList;
			argsList.add(pPopupReturn->getButtonClicked());
			argsList.add(info.getData1());
			argsList.add(info.getData2());
			argsList.add(info.getData3());
			argsList.add(info.getFlags());
			argsList.add(info.getText());
			argsList.add(info.getOption1());
			argsList.add(info.getOption2());
			gDLL->getPythonIFace()->callFunction((info.getPythonModule().IsEmpty() ? PYScreensModule : info.getPythonModule()), info.getOnClickedPythonCallback(), argsList.makeFunctionArgs());
			break;
		}
		break;

	case BUTTONPOPUP_DETAILS:
		{
			// Civ details
			PlayerTypes eID = GC.getInitCore().getActivePlayer();

			CvWString szLeaderName = GC.getInitCore().getLeaderName(eID);
			CvWString szCivDescription = GC.getInitCore().getCivDescription(eID);
			CvWString szCivShortDesc = GC.getInitCore().getCivShortDesc(eID);
			CvWString szCivAdjective = GC.getInitCore().getCivAdjective(eID);
			CvWString szCivPassword = PASSWORD_DEFAULT;
			CvString szEmail = GC.getInitCore().getEmail(eID);
			CvString szSmtpHost = GC.getInitCore().getSmtpHost(eID);

			if (pPopupReturn->getEditBoxString(0) && *(pPopupReturn->getEditBoxString(0)))
			{
				szLeaderName = pPopupReturn->getEditBoxString(0);
			}
			if (pPopupReturn->getEditBoxString(1) && *(pPopupReturn->getEditBoxString(1)))
			{
				szCivDescription = pPopupReturn->getEditBoxString(1);
			}
			if (pPopupReturn->getEditBoxString(2) && *(pPopupReturn->getEditBoxString(2)))
			{
				szCivShortDesc = pPopupReturn->getEditBoxString(2);
			}
			if (pPopupReturn->getEditBoxString(3) && *(pPopupReturn->getEditBoxString(3)))
			{
				szCivAdjective = pPopupReturn->getEditBoxString(3);
			}
			if (GC.getGameINLINE().isHotSeat() || GC.getGameINLINE().isPbem())
			{
				if (pPopupReturn->getEditBoxString(4) && *(pPopupReturn->getEditBoxString(4)))
				{
					szCivPassword = pPopupReturn->getEditBoxString(4);
				}
			}
			if (GC.getGameINLINE().isPitboss() || GC.getGameINLINE().isPbem())
			{
				if (pPopupReturn->getEditBoxString(5) && *(pPopupReturn->getEditBoxString(5)))
				{
					szEmail = CvString(pPopupReturn->getEditBoxString(5));
				}
			}
			if (GC.getGameINLINE().isPbem())
			{
				if (pPopupReturn->getEditBoxString(6) && *(pPopupReturn->getEditBoxString(6)))
				{
					szSmtpHost = CvString(pPopupReturn->getEditBoxString(6));
				}
			}

			GC.getInitCore().setLeaderName(eID, szLeaderName);
			GC.getInitCore().setCivDescription(eID, szCivDescription);
			GC.getInitCore().setCivShortDesc(eID, szCivShortDesc);
			GC.getInitCore().setCivAdjective(eID, szCivAdjective);
			if (szCivPassword != PASSWORD_DEFAULT)
			{
				GC.getInitCore().setCivPassword(eID, szCivPassword);
			}
			GC.getInitCore().setEmail(eID, szEmail);
			GC.getInitCore().setSmtpHost(eID, szSmtpHost);
			gDLL->sendPlayerInfo(eID);

			if (GC.getGameINLINE().isPbem() && pPopupReturn->getButtonClicked() == 0)
			{
				gDLL->sendPbemTurn(NO_PLAYER);
			}

		}
		break;

	case BUTTONPOPUP_ADMIN:
		{
			// Game details
			CvWString szGameName;
			CvWString szAdminPassword = GC.getInitCore().getAdminPassword();
			if (pPopupReturn->getEditBoxString(0) && *(pPopupReturn->getEditBoxString(0)))
			{
				szGameName = pPopupReturn->getEditBoxString(0);
			}
			if (pPopupReturn->getEditBoxString(1) && CvWString(pPopupReturn->getEditBoxString(1)) != PASSWORD_DEFAULT)
			{
				if (*(pPopupReturn->getEditBoxString(1)))
				{
					szAdminPassword = CvWString(gDLL->md5String((char*)CvString(pPopupReturn->getEditBoxString(1)).GetCString()));
				}
				else
				{
					szAdminPassword = L"";
				}
			}
			if (!GC.getGameINLINE().isGameMultiPlayer())
			{
				int iResult = pPopupReturn->getCheckboxBitfield(2);
				if (gDLL->getChtLvl() != iResult)
				{
					gDLL->setChtLvl(iResult);
					GC.getGameINLINE().toggleDebugMode();
				}
			}

			gDLL->sendGameInfo(szGameName, szAdminPassword);

		}
		break;
	case BUTTONPOPUP_ADMIN_PASSWORD:
		{
			CvWString szAdminPassword;
			if (pPopupReturn->getEditBoxString(0) && CvWString(pPopupReturn->getEditBoxString(0)) != PASSWORD_DEFAULT)
			{
				szAdminPassword = pPopupReturn->getEditBoxString(0);
			}
			if (CvWString(gDLL->md5String((char*)CvString(szAdminPassword).GetCString())) == GC.getInitCore().getAdminPassword())
			{
				switch ((ControlTypes)info.getData1())
				{
				case CONTROL_WORLD_BUILDER:
					gDLL->getInterfaceIFace()->setWorldBuilder(!(gDLL->GetWorldBuilderMode()));
					break;
				case CONTROL_ADMIN_DETAILS:
					gDLL->getInterfaceIFace()->showAdminDetails();
					break;
				default:
					break;
				}
			}
			else
			{
				CvPopupInfo* pInfo = new CvPopupInfo();
				if (NULL != pInfo)
				{
					pInfo->setText(gDLL->getText("TXT_KEY_BAD_PASSWORD_DESC"));
					gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
				}
			}
		}
		break;

	case BUTTONPOPUP_EXTENDED_GAME:
		if (pPopupReturn->getButtonClicked() == 0)
		{
			if (GC.getGameINLINE().isNetworkMultiPlayer())
			{
				gDLL->sendExtendedGame();
			}
			else
			{
				GC.getGameINLINE().setGameState(GAMESTATE_EXTENDED);
			}
		}
		else if (pPopupReturn->getButtonClicked() == 1)
		{
			// exit to main menu
			if (GC.getGameINLINE().isNetworkMultiPlayer() && GC.getGameINLINE().canDoControl(CONTROL_RETIRE) && GC.getGameINLINE().countHumanPlayersAlive() > 1)
			{
				GC.getGameINLINE().doControl(CONTROL_RETIRE);
			}
			else
			{
				gDLL->getInterfaceIFace()->exitingToMainMenu();
			}
		}
		break;

	case BUTTONPOPUP_DIPLOMACY:
		if (pPopupReturn->getButtonClicked() != MAX_PLAYERS)
		{
			GET_PLAYER(GC.getGameINLINE().getActivePlayer()).contact((PlayerTypes)(pPopupReturn->getButtonClicked()));
		}
		break;

	case BUTTONPOPUP_ADDBUDDY:
		if (pPopupReturn->getButtonClicked() == 0)
		{
			gDLL->AcceptBuddy(CvString(info.getText()).GetCString(), info.getData1());
		}
		else
		{
			gDLL->RejectBuddy(CvString(info.getText()).GetCString(), info.getData1());
		}
		break;

	case BUTTONPOPUP_FORCED_DISCONNECT:
	case BUTTONPOPUP_PITBOSS_DISCONNECT:
	case BUTTONPOPUP_KICKED:
		gDLL->getInterfaceIFace()->exitingToMainMenu();
		break;

	case BUTTONPOPUP_EVENT:
		{
			CvPlayer& kActivePlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			EventTriggeredData* pTriggeredData = kActivePlayer.getEventTriggered(info.getData1());

			if (pPopupReturn->getButtonClicked() == GC.getNumEventInfos())
			{
				if (NULL != pTriggeredData)
				{
					CvCity* pCity = kActivePlayer.getCity(pTriggeredData->m_iCityId);
					if (NULL != pCity)
					{
						gDLL->getInterfaceIFace()->selectCity(pCity, true);
					}
				}

				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_EVENT, info.getData1());
				gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), false, true);
			}
			else if (-1 != pPopupReturn->getButtonClicked())
			{
				if (NULL != pTriggeredData && GC.getEventTriggerInfo(pTriggeredData->m_eTrigger).isSinglePlayer())
				{
					GET_PLAYER(GC.getGameINLINE().getActivePlayer()).doAction(PLAYER_ACTION_APPLY_EVENT, pPopupReturn->getButtonClicked(), info.getData1(), -1);
				}
				else
				{
					gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_APPLY_EVENT, pPopupReturn->getButtonClicked(), info.getData1(), -1);
				}
			}
		}
		break;

	case BUTTONPOPUP_FREE_COLONY:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			gDLL->sendDoTask(pPopupReturn->getButtonClicked(), TASK_LIBERATE, 0, -1, false, false, false, false);
		}
		break;

	case BUTTONPOPUP_CHOOSE_PROFESSION:
		if (pPopupReturn->getButtonClicked() == -1)
		{
			gDLL->sendDoTask(info.getData1(), TASK_SET_CITIZEN_AUTOMATED, info.getData2(), pPopupReturn->getButtonClicked(), false, false, false, false);
		}
		else if (pPopupReturn->getButtonClicked() == -2)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CONFIRMTASK, info.getData1(), info.getData2(), TASK_CLEAR_SPECIALTY);
			pInfo->setText(GC.getCommandInfo(COMMAND_CLEAR_SPECIALTY).getTextKeyWide());
			gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), true);
		}
		else if (pPopupReturn->getButtonClicked() < GC.getNumProfessionInfos())
		{
			ProfessionTypes eSelectedProfession = (ProfessionTypes) pPopupReturn->getButtonClicked();
			if (GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(info.getData2()) != NULL)
			{
				CLLNode<IDInfo>* pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();
				while (pSelectedUnitNode != NULL)
				{
					CvUnit* pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);
					pSelectedUnitNode = gDLL->getInterfaceIFace()->nextSelectionListNode(pSelectedUnitNode);
					CvCity* pCity = pSelectedUnit->getCity();

					gDLL->sendDoCommand(pSelectedUnit->getID(), COMMAND_PROFESSION, eSelectedProfession, -1, false);
					if (pCity != NULL && eSelectedProfession == PROFESSION_STUDENT)
					{
						CvPopupInfo* pPopupInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_EDUCATION, pCity->getID(), pSelectedUnit->getID());
						gDLL->getInterfaceIFace()->addPopup(pPopupInfo, pSelectedUnit->getOwner(), true, true);
					}
				}
			}
			else if (GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getEuropeUnitById(info.getData2()) != NULL)
			{
				gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_EUROPE_CHANGE_PROFESSION, info.getData2(), pPopupReturn->getButtonClicked(), -1);
			}
			else
			{
				gDLL->sendDoTask(info.getData1(), TASK_CHANGE_PROFESSION, info.getData2(), pPopupReturn->getButtonClicked(), false, false, false, false);
			}
		}
		break;
	case BUTTONPOPUP_HOSPITAL:
		if (pPopupReturn->getButtonClicked() >= 0 )
		{
			bool bImport = (pPopupReturn->getCheckboxBitfield(1) & 0x01);
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvCity* pCity = kPlayer.getCity(info.getData1());
			CvHospitals* pHospitals = kPlayer.getHospitals();
			int iValue1 = pPopupReturn->getSpinnerWidgetValue(1);
			int iValue2 = pPopupReturn->getSpinnerWidgetValue(2);

			if (pPopupReturn->getButtonClicked() == 1 )
			{
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_MANAGE_HOSPITALS, pCity->getID(), iValue1, -1);
			}
			gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_MANAGE_HOSPITALS_BIS, pCity->getID(), iValue2, bImport);
		}
		break;

	case BUTTONPOPUP_RESUPPLY_BUILDER_PACK:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			bool bAddPack = true;
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_RESUPPLY_BUILDER_PACK, info.getData1(), bAddPack, -1);
		}
		break;

	case BUTTONPOPUP_UNLOAD_NEW_CARGO:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());

			if (pUnit != NULL)
			{
				CvPlot* pPlot = pUnit->plot();
				if (pPlot != NULL)
				{
					CvCity* pCity = pPlot->getPlotCity();
					if (pCity != NULL)
					{
						int iYield = pPopupReturn->getButtonClicked();
						if (iYield < NUM_YIELD_TYPES)
						{
							int iValue1 = pPopupReturn->getSpinnerWidgetValue(iYield);
							gDLL->sendDoCommand(pUnit->getID(), COMMAND_UNLOAD_NEW_CARGO_IN_CITY, iYield, iValue1, false);
						}
						else
						{
							gDLL->sendDoCommand(pUnit->getID(), COMMAND_UNLOAD_ALL, -1, -1, false);
						}
					}
				}
			}
		}
		break;

	case BUTTONPOPUP_CHOOSE_QUANTITY_TRADE:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());

			if (pUnit != NULL)
			{
				CvPlot* pPlot = pUnit->plot();
				if (pPlot != NULL)
				{
					CvCity* pCity = pPlot->getPlotCity();
					if (pCity != NULL)
					{
						bool bUnit = info.getData3() == 1;
						YieldTypes eYield = (YieldTypes)info.getData2();
						int iNewCargo = pPopupReturn->getSpinnerWidgetValue(1);
						if (bUnit)
						{
							kPlayer.setHumanTradeProposition(eYield, iNewCargo);
						}
						else
						{
							kPlayer.setIATradeProposition(eYield, iNewCargo);
						}
					}
				}
			}
			gDLL->getInterfaceIFace()->setDirty(NativeYieldTrade_DIRTY_BIT, true);
		}
		break;

	case BUTTONPOPUP_CHOOSE_PROFESSION_IN_EUROPE:
		if (pPopupReturn->getButtonClicked() != -1)
		{
			int iValueReturn = pPopupReturn->getButtonClicked();

			CvPlayer& kPlayer = GET_PLAYER( GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getEuropeUnitById(info.getData2());

			ProfessionTypes eProfession = pUnit->getProfession();

			if (iValueReturn == -2)
			{
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_RESUPPLY_EUROPE_UNIT, pUnit->getID(), -1, -1);
			}
			else
			{
				if (iValueReturn < GC.getNumProfessionInfos())  {
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_VALIDATE_PROFESSION_IN_EUROPE, pUnit->getID(), iValueReturn);
					gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), true);
				}
			}
		}
		break;

	case BUTTONPOPUP_CHOOSE_CITY_DESTINATION_IN_EUROPE:
		if (pPopupReturn->getButtonClicked() >= 0 || pPopupReturn->getButtonClicked() == -2)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SET_DESTINATION_CITY_TO_UNIT, info.getData1(), pPopupReturn->getButtonClicked(), -1);
		}
		break;

	case BUTTONPOPUP_VALIDATE_PROFESSION_IN_EUROPE:
		if (pPopupReturn->getButtonClicked() > -1)
		{
			int iValueReturn = pPopupReturn->getButtonClicked();

			CvPlayer& kPlayer = GET_PLAYER( GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getEuropeUnitById(info.getData1());
			ProfessionTypes eProfession = (ProfessionTypes) info.getData2();
			gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_EUROPE_CHANGE_PROFESSION, pUnit->getID(), eProfession, iValueReturn);

		}
		break;

	case BUTTONPOPUP_EJECT_UNIT_TO_GROUP:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
			CvPlayer& kPlayer = GET_PLAYER(ePlayer);

			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvSelectionGroup* pGroup = pUnit->getGroup();

			std::vector<CvUnit*> apUnits;
			CvUnit* pFisrtUnit = NULL;

			CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
			while (pUnitNode != NULL)
			{
				CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pGroup->nextUnitNode(pUnitNode);
				if (pLoopUnit != NULL)
				{
					if (pPopupReturn->getCheckboxBitfield(pLoopUnit->getID()) > 0)
					{
						apUnits.push_back(pLoopUnit);
						if (pFisrtUnit == NULL)
						{
							pFisrtUnit = pLoopUnit;
						}
					}
				}
			}
			for (uint i = 0; i < apUnits.size(); ++i)
			{
				CvUnit* pLoopUnit = apUnits[i];
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_EJECT_UNIT_TO_GROUP, pFisrtUnit->getID(), pLoopUnit->getID(), -1);
			}
		}
		break;

	case BUTTONPOPUP_ASK_SAILOR_FORMATION:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			int iTempUnitID = info.getData1();
			int iReturnMode = pPopupReturn->getButtonClicked();
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_TRAIN_CREW_IN_EUROPE, iTempUnitID, iReturnMode, -1);
		}
		break;

	case BUTTONPOPUP_DESTROY_CITY:
		if (pPopupReturn->getButtonClicked() == 0)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_DESTROY_CITY, info.getData1(), -1, -1);
		}
		break;

	case BUTTONPOPUP_REMOVE_RESSOURCE:
		if (pPopupReturn->getButtonClicked() == 0)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_REMOVE_RESSOURCE, info.getData1(), -1, -1);
		}
		break;

	case BUTTONPOPUP_REPAIR_SHIP:
		if (pPopupReturn->getButtonClicked() == 1)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_REPAIR_SHIP, info.getData1(), info.getData2(), -1);
		}
		break;

	case BUTTONPOPUP_ARSENAL_MANAGEMENT:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvCity* pCity = kPlayer.getCity(info.getData1());
			gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ARSENAL_MANAGEMENT, pCity->getID(), pPopupReturn->getButtonClicked(), -1);	
		}
		break;

	case BUTTONPOPUP_LEADUNITEXP:
		if (pPopupReturn->getButtonClicked() >= 0) 
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pLeaderUnit = kPlayer.getUnit(info.getData1());
			CvUnit* pSelectedUnit = kPlayer.getUnit(pPopupReturn->getButtonClicked());
			int iStartPoints = info.getData2();
			int iDistributedPoints = pPopupReturn->getSpinnerWidgetValue(1);
			int iRemainingPoints = iStartPoints - iDistributedPoints;

			if (pSelectedUnit != NULL)
			{
				int iUnitExp = pSelectedUnit->getExperience() + iDistributedPoints * 10;
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_EXP_LEAD_UNIT, pSelectedUnit->getID(), iUnitExp, iDistributedPoints);
				pSelectedUnit->setExperience(iUnitExp);
				if (iDistributedPoints > 0) 
				{
					int iRandSound = 1 + GC.getASyncRand().get(4, "iRandSound general exp");
					gDLL->getInterfaceIFace()->playGeneralSound(CvString::format(_T("AS2D_GENERAL_EXP%d"), iRandSound)); //YATA!!
				}
				if (iRemainingPoints > 0) 
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_LEADUNITEXP, pLeaderUnit->getID(), iRemainingPoints);
					gDLL->getInterfaceIFace()->addPopup(pInfo, kPlayer.getID(), true);
				} 
				else 
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_LEADUNIT, pLeaderUnit->getID());
					gDLL->getInterfaceIFace()->addPopup(pInfo, kPlayer.getID(), true);
				}
			}
		}
		break;

	case BUTTONPOPUP_PURCHASE_EUROPE_UNIT:
		if (pPopupReturn->getButtonClicked() < GC.getNumUnitInfos())
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_BUY_EUROPE_UNIT, pPopupReturn->getButtonClicked(), -1, -1);
		}
		break;

	case BUTTONPOPUP_CIVIC_OPTION:
		if (pPopupReturn->getButtonClicked() == -1)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CIVIC_OPTION, info.getData1(), info.getData2(), info.getData3(), info.getFlags(), info.getOption1(), info.getOption2());
			gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), false, true);

			CivicTypes eCivic = NO_CIVIC;
			for (int iCivic = 0; iCivic < GC.getNumCivicInfos(); ++iCivic)
			{
				if (GC.getCivicInfo((CivicTypes) iCivic).getCivicOptionType() == info.getData1())
				{
					eCivic = (CivicTypes) iCivic;
					break;
				}
			}

			pInfo = new CvPopupInfo(BUTTONPOPUP_PYTHON_SCREEN);
			pInfo->setText(L"pediaJumpToCivic");
			pInfo->setData1(eCivic);
			gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), false, true);
		}
		else
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SET_CIVIC, info.getData1(), pPopupReturn->getButtonClicked(), -1);
		}
		break;

	case BUTTONPOPUP_YIELD_PRODUCTION:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvCity* pCity = kPlayer.getCity(info.getData1());
			int iYield = pPopupReturn->getButtonClicked();
			if (pCity != NULL)
			{
				int iValue1 = pPopupReturn->getSpinnerWidgetValue(iYield);
				gDLL->sendDoTask(info.getData1(), TASK_YIELD_PRODUCTION_PERCENT, iYield, iValue1, false, false, false, false);
			}
		}
		break;

	case BUTTONPOPUP_PROMOTE:
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvSelectionGroup* pSelectionGroup = kPlayer.getSelectionGroup(info.getData1());
			int iPromotion = pPopupReturn->getButtonClicked();
			if (iPromotion < GC.getNumPromotionInfos())
			{
				if (pSelectionGroup != NULL)
				{
					CLLNode<IDInfo>* pUnitNode = pSelectionGroup->headUnitNode();
					while (pUnitNode != NULL)
					{
						CvUnit* ploopUnit = ::getUnit(pUnitNode->m_data);
						pUnitNode = pSelectionGroup->nextUnitNode(pUnitNode);
						if (NULL != ploopUnit)
						{
							gDLL->sendDoCommand(ploopUnit->getID(), COMMAND_PROMOTION, iPromotion, -1, false);
							gDLL->sendDoCommand(ploopUnit->getID(), COMMAND_PROMOTE, -1, -1, false);
						}
					}
				}
			}
		}
		break;
	case BUTTONPOPUP_EMBARK_ON_SHIP:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvUnit* pSailorUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(info.getData1());
			if (pSailorUnit != NULL)
			{
				CvSelectionGroup* pSelectionGroup = pSailorUnit->getGroup();
				if (NULL != pSelectionGroup)
				{
					CvPlot* pPlot = pSelectionGroup->plot();
					int iCount = pPopupReturn->getButtonClicked();
					CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
					while (pUnitNode != NULL)
					{
						CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
						pUnitNode = pPlot->nextUnitNode(pUnitNode);
						if (pLoopUnit->canHaveCrew())
						{
							iCount--;
							if (iCount == 0)
							{
								gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_LOAD_CREW, pLoopUnit->getID(), pSailorUnit->getID(), 0);
							}
						}
					}
				}
			}
		}
		break;
	case BUTTONPOPUP_LOAD_CREW:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnitShip = kPlayer.getUnit(info.getData1());
			int iUnitReturn = pPopupReturn->getButtonClicked();
			int iNbUnit = 0;
			for (int i = 0; i < kPlayer.getNumEuropeUnits(); ++i)
			{
				CvUnit* pLoopUnit = kPlayer.getEuropeUnit(i);
				if (pLoopUnit->getProfession() == PROFESSION_SAILOR)
				{
					if (!pLoopUnit->isHurt() && pLoopUnit->canBeSailor())
					{
						iNbUnit++;
						if (iUnitReturn == iNbUnit)
						{
							gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_LOAD_CREW, pUnitShip->getID(), pLoopUnit->getID(), 1);
							break;
						}
					}
				}
			}
		}
		break;
	case BUTTONPOPUP_GIVE_MAP:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			int iUnitId = pPopupReturn->getButtonClicked();
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_GIVE_MAP, info.getData1(), iUnitId, -1);
		}
		break;
	case BUTTONPOPUP_TRANSFERT_MAP_TO_EUROPE:
		if (pPopupReturn->getButtonClicked())
		{
			if (pPopupReturn->getCheckboxBitfield(1) & 0x01)
			{
				gDLL->sendPlayerOption(PLAYEROPTION_SHOW_MAP_POPUP, false);
			}
		}
		break;
	case BUTTONPOPUP_REMOVE_ASSIGNED_UNIT:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvSelectionGroup* pGroup = pUnit->getGroup();
			pGroup->assignAgreement(info.getData2(), false);
			gDLL->getInterfaceIFace()->setDirty(NativeYieldTradeResetTrade_DIRTY_BIT, true);
			gDLL->sendDoCommand(pUnit->getID(), COMMAND_REMOVE_ASSIGNED_UNIT, info.getData2(), -1, false);
		}
		break;
	case BUTTONPOPUP_NEW_AGREEMENT:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvCity* pCitySource = kPlayer.getCity(pPopupReturn->getButtonClicked());
			CvCity* pCityDest = pUnit->plot()->getPlotCity();

			if (pCitySource != NULL)
			{
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_NEW_AGREEMENT, pUnit->getID(), pCitySource->getID(), -1);
			}
		}
		else
		{
			gDLL->getInterfaceIFace()->setDirty(NativeYieldTradeResetTrade_DIRTY_BIT, true);
		}
		break;
	case BUTTONPOPUP_NEW_AGREEMENT_PART1:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			//On selectionne la quantité du contrat
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvPlot* pPlot = pUnit->plot();
			CvCity* pCity = pPlot->getPlotCity();
			CvPlayer& kOtherPlayer = GET_PLAYER(pCity->getOwner());
			CvAgreement* pAgreement = kPlayer.getAgreement(info.getData2());
			int iChoice = pPopupReturn->getButtonClicked();
			int iQuantity = kOtherPlayer.getAgreementQuantity(iChoice);

			pAgreement->addYieldTrade(pCity->getNativeYieldProduce(), TRADE_IMPORT, iQuantity);
			pAgreement->setLevel(iChoice);

			gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_NEW_AGREEMENT_PART1, pUnit->getID(), pAgreement->getID(), iChoice);

			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_NEW_AGREEMENT_PART2, info.getData1(), info.getData2(), 70);
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
		break;
	case BUTTONPOPUP_NEW_AGREEMENT_PART2:
		if (pPopupReturn->getButtonClicked() >= -1)
		{
			//We negociate
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_NEW_AGREEMENT_PART2, info.getData1(), pPopupReturn->getButtonClicked(), info.getData3());
		}
		break;
	case BUTTONPOPUP_END_OF_AGREEMENT:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_END_OF_AGREEMENT, info.getData1(), info.getData2(), pPopupReturn->getButtonClicked());
		}
		break;

	case BUTTONPOPUP_AUTOMATIC_RENEW_AGREEMENT:
		if (pPopupReturn->getButtonClicked() > 0)
		{			
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_AUTOMATIC_AGREEMENT_BY_YIELDS);
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
		break;
	
	case BUTTONPOPUP_AUTOMATIC_AGREEMENT_BY_YIELDS:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
			{
				YieldTypes eYield = (YieldTypes) iI;
				if (GC.getGameINLINE().isWantedYieldForNatives(eYield))
				{
					bool bNewValue = pPopupReturn->getCheckboxBitfield(eYield) > 0;
				
					if (kPlayer.hasAutomaticTradeByYieldWithNative(eYield) != bNewValue)
					{
						gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_AUTOMATIC_AGREEMENT_BY_YIELDS, eYield, bNewValue, -1);
					}
				}
			}
		}
		break;
	case BUTTONPOPUP_WAREHOUSE_UPGRADE:
		break;
	case BUTTONPOPUP_AGREEMENT_LIST:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHANGE_CITY_AGREEMENT, pPopupReturn->getButtonClicked());
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
		else if (pPopupReturn->getButtonClicked() == -2)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_AUTOMATIC_AGREEMENT_BY_YIELDS);
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
		break;
	case BUTTONPOPUP_SEEDLING_LIST:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SEEDLING_LIST);
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER);
		}
		break;
	case BUTTONPOPUP_SEEDLING_DISCOVERY:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			int iReturn = pPopupReturn->getButtonClicked();
			if (iReturn == 12)
			{
				if (pPopupReturn->getCheckboxBitfield(1) & 0x01)
				{
					gDLL->sendPlayerOption(PLAYEROPTION_HIDE_HELPS_FOR_CULTURE, true);
				}
				gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_KING_GIVE_AGRONOMIST, info.getData1(), -1, -1);
			}
			else
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SEEDLING_DISCOVERY, /*bonusTypes*/ info.getData1(), iReturn);
				gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
			}
		}
		break;
	case BUTTONPOPUP_CHANGE_CITY_AGREEMENT:
		if (pPopupReturn->getButtonClicked() >= -1)
		{
			if (pPopupReturn->getButtonClicked() >= 0)
			{
				gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_CHANGE_CITY_AGREEMENT, info.getData1(), pPopupReturn->getButtonClicked(), -1);
			}
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_AGREEMENT_LIST);
			gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
		}
		break;
	case BUTTONPOPUP_CHOOSE_AGREEMENTS:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvSelectionGroup* pGroup = pUnit->getGroup();

			int iID = pPopupReturn->getButtonClicked();
			bool bAssign = pGroup->isAssignedAgreement(iID);

			gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_CHOOSE_AGREEMENTS, pUnit->getID(), iID, !bAssign);
		}
		break;
	case BUTTONPOPUP_CHOOSE_TRADE_ROUTES:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvSelectionGroup* pGroup = pUnit->getGroup();

			int iID = pPopupReturn->getButtonClicked();
			bool bAssign = pGroup->isAssignedTradeRoute(iID);

			gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_CHOOSE_TRADE_ROUTE, pUnit->getID(), iID, !bAssign);
		}
		break;
	case BUTTONPOPUP_SEND_TRADE_PROPOSITION:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
			CvPlayer& kPlayer = GET_PLAYER(ePlayer);
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			ProposalTypes pProposal = GAMETRADE.whatKindOfTradeProposal(ePlayer, info.getData2());

			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_PROCESS_TRADE_PROPOSITION, info.getData1(), pProposal, -1);
			gDLL->getInterfaceIFace()->addPopup(pInfo, ePlayer, true, true);
		}
		else
		{
			gDLL->getInterfaceIFace()->setDirty(NativeYieldTradeResetTrade_DIRTY_BIT, true);
		}
		break;
	case BUTTONPOPUP_PROCESS_TRADE_PROPOSITION:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_PROCESS_TRADE_PROPOSITION, info.getData1(), pPopupReturn->getButtonClicked(), -1);
		}
		else
		{
			gDLL->getInterfaceIFace()->setDirty(NativeYieldTradeResetTrade_DIRTY_BIT, true);
		}
		break;
	case BUTTONPOPUP_STOP_TRADE:
		break;

	case BUTTONPOPUP_SELL_SHIP:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnitShip = kPlayer.getUnit(info.getData1());
			int iValue1 = pPopupReturn->getSpinnerWidgetValue(1);
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SELL_SHIP, info.getData1(), iValue1, -1);
		}
		break;
	case BUTTONPOPUP_STOP_SELL_SHIP:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnitShip = kPlayer.getUnit(info.getData1());
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SELL_SHIP, info.getData1(), 0, -1);
		}
		break;
	case BUTTONPOPUP_RECRUIT_CREW:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvTempUnit* pTempUnit = kPlayer.getTempUnit(info.getData1());

			if (pPopupReturn->getButtonClicked() == 2)
			{
				int iDiscountPercent = 5 + GC.getASyncRand().get(11, "iDiscountPercent");
				if (kPlayer.isPenalitePlayerSave())
				{
					iDiscountPercent = 51;
				}
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_RECRUIT_CREW, info.getData1(), info.getData2(), info.getData3() + iDiscountPercent);
				gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
			}
			else
			{
				int iGold = info.getData2() - info.getData2()*info.getData3()/100;
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_PROCESS_RECRUIT_CREW, pTempUnit->getID(), iGold, -1);
			}
		}
		else
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_PROCESS_RECRUIT_CREW, info.getData1(), -1, -1);
		}
		break;
	case BUTTONPOPUP_RECRUIT_IMMIGRANT:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvTempUnit* pTempUnit = kPlayer.getTempUnit(info.getData1());
			if (pTempUnit == NULL)
			{
				return;
			}

			if (pPopupReturn->getButtonClicked() == 2)
			{
				int iDiscountPercent = 5 + GC.getASyncRand().get(11, "iDiscountPercent");
				if (kPlayer.isPenalitePlayerSave())
				{
					iDiscountPercent = 51;
				}
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_RECRUIT_IMMIGRANT, info.getData1(), info.getData2(), info.getData3() + iDiscountPercent);
				gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
			}
			else
			{
				int iGold = info.getData2() - info.getData2()*info.getData3()/100;
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_PROCESS_RECRUIT_IMMIGRANT, pTempUnit->getID(), iGold, -1);
			}
		}
		else if (pPopupReturn->getButtonClicked() == 0)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_PROCESS_RECRUIT_IMMIGRANT, info.getData1(), -1, -1);
		}
		break;
	case BUTTONPOPUP_BUY_USED_SHIP:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvTempUnit* pTempUnit = kPlayer.getTempUnit(info.getData1());
			if (pTempUnit != NULL)
			{				
				if (pPopupReturn->getButtonClicked() == 2)
				{
					int iDiscountPercent = 5 + GC.getASyncRand().get(11, "iDiscountPercent");
					if (kPlayer.isPenalitePlayerSave())
					{
						iDiscountPercent = 51;
					}
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_BUY_USED_SHIP, info.getData1(), info.getData2(), info.getData3() + iDiscountPercent);
					gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
				}
				else
				{
					int iGold = info.getData2() - info.getData2()*info.getData3()/100;
					gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_PROCESS_BUY_USED_SHIP, pTempUnit->getID(), iGold, -1);
				}
			}
		}
		else
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_PROCESS_BUY_USED_SHIP, info.getData1(), -1, -1);
		}
		break;
	case BUTTONPOPUP_BUY_NEW_SHIP_IN_EUROPE:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvPlayer& kPlayerEurope = GET_PLAYER(kPlayer.getParent());
			UnitTypes eUnit = (UnitTypes)info.getData1();
			int iPrice = kPlayer.getEuropeUnitBuyPrice(eUnit);
			iPrice += iPrice*kPlayerEurope.getUnitMarketPrice((UnitClassTypes)GC.getUnitInfo(eUnit).getUnitClassType())/100;
			int iTurn = std::max(1, GC.getUnitInfo(eUnit).getYieldCost(YIELD_HAMMERS)/200);
			gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_BUY_NEW_SHIP_IN_EUROPE, eUnit, iTurn, iPrice);
		}
		break;

	case BUTTONPOPUP_PURCHASE_BID:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			if (pUnit != NULL)
			{
				int iSellPrice = pUnit->getShipSellPrice();

				if (pPopupReturn->getButtonClicked() == 2)
				{
					int iRefusPercent = 20 + 5*GC.getASyncRand().get(100, "iDiscountPercent")%6;
					if (kPlayer.isPenalitePlayerSave())
					{
						iRefusPercent = 100;
					}
					int iIncreasePrice = 25 + 5*GC.getASyncRand().get(100, "iDiscountPercent")%6;
					int iNewPrice = (iSellPrice - info.getData2())*iIncreasePrice/100;
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_PURCHASE_BID, info.getData1(), info.getData2()+iNewPrice, info.getData3() + iRefusPercent);
					gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
				}
				else
				{
					if (pPopupReturn->getButtonClicked() != 3)
					{
						iSellPrice = info.getData2();
					}
					gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_PROCESS_PURCHASE_BID, pUnit->getID(), iSellPrice, -1);
				}
			}
		}
		else
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_PROCESS_PURCHASE_BID, info.getData1(), -1, -1);
		}
		break;
	case BUTTONPOPUP_RECEIVE_SEEDLING:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_PROCESS_RECEIVE_SEEDLING, pPopupReturn->getButtonClicked(), -1, -1);
		}
		break;

	case BUTTONPOPUP_RUM_DISCOVERED:
		break;

	case BUTTONPOPUP_ACHIEVEMENTS:
	case BUTTONPOPUP_VICTORY_POPUP:
		break;

	case BUTTONPOPUP_CHOOSE_START_SPECIALIST:
		if ( pPopupReturn->getButtonClicked() > 0 )
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SET_STARTING_SPECIALIST, pPopupReturn->getButtonClicked(), info.getData1(), -1);
		}
		break;
	case BUTTONPOPUP_ASK_USE_RELATION_POINTS_FOR_IMMIGRATION:
		if ( pPopupReturn->getButtonClicked() > 0 )
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_USE_RELATION_POINTS_FOR_IMMIGRATION, info.getData1(), -1, -1);
		}
		break;
	case BUTTONPOPUP_CHANGE_MIN_GOLD_TO_CONSERVE_FOR_TRADE_ROUTE:
		if ( pPopupReturn->getButtonClicked() > 0 )
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_CHANGE_MIN_GOLD_TO_CONSERVE_FOR_TRADE_ROUTE, info.getData1(), pPopupReturn->getCurrentSpinBoxValue(0), -1);
		}
		break;
	case BUTTONPOPUP_FOUND_COLONY:
		if ( pPopupReturn->getButtonClicked() == 1 )
		{
			gDLL->sendPushMission(info.getData1(), MISSION_FOUND, -1, -1, 0, false);
		}
		break;

	case BUTTONPOPUP_REPAY_LOAN:
		if ( pPopupReturn->getButtonClicked() == 1 )
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_REPAY_LOAN, pPopupReturn->getSpinnerWidgetValue(1), -1, -1);
		}
		break;
	case BUTTONPOPUP_WHEN_NEW_ERA:
	case BUTTONPOPUP_OPEN_BREACH_FORTIFICATION:
		break;
	case BUTTONPOPUP_RESUPPLY_SHIPS:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			int iButtonClicked = pPopupReturn->getButtonClicked();
			//	iButtonClicked :
			//		1 -> Max Cannon							//		2 -> Max Amunition
			//		3 -> Max Cannon and Max Amunition		//		4 -> Update
			//		5 -> Ask to buy missing Yield			//		5 -> Buy missing Yield
			//		6 -> Unload all							//		6 -> Unload all
			if (iButtonClicked == 4) 
			{
				gDLL->sendDoCommand(info.getData1(), COMMAND_RESUPPLY_SHIPS_EQUIPMENT, pPopupReturn->getSpinnerWidgetValue(1), pPopupReturn->getSpinnerWidgetValue(2), false);
			} 
			else if (iButtonClicked == 5) 
			{
				gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_BUY_MISSING_EQUIPEMENT, info.getData1(), -1, -1);
			}
			else if (iButtonClicked == 6) 
			{
				gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_UNLOAD_ALL_IN_EUROPE, info.getData1(), -1, -1);
			}
			else 
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_RESUPPLY_SHIPS, info.getData1(), info.getData2(), iButtonClicked);
				gDLL->getInterfaceIFace()->addPopup(pInfo, GC.getGameINLINE().getActivePlayer(), true);
			}
		}
		break;
	case BUTTONPOPUP_ROB_UNIT:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvUnit* pAttackUnit = kPlayer.getUnit(info.getData2());
			if (pPopupReturn->getButtonClicked() == 1)
			{
				if (!pUnit->hasTempCrew())
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CREW, pUnit->getID());
					gDLL->getInterfaceIFace()->addPopup(pInfo);
				}
				else
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_PASSENGERS, info.getData1());
					gDLL->getInterfaceIFace()->addPopup(pInfo);
				}
			}
			else if (pPopupReturn->getButtonClicked() == 2)
			{
				bool bDefCargo = pUnit->getOnlyNewCargo() > 0;
				bool bAttCargo = pAttackUnit->getOnlyNewCargo() > 0;
				bool bDefUnitCargo = (pUnit->getNewCargo(false) - pUnit->getOnlyNewCargo()) > 0;
				bool bAttUnitCargo = (pAttackUnit->getNewCargo(false) - pAttackUnit->getOnlyNewCargo()) > 0;
				if (bDefCargo || bDefUnitCargo)
				{
					if (!bDefCargo && !bAttCargo)
					{	//NO CARGO
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_PASSENGERS, pUnit->getID(), pAttackUnit->getID());
						gDLL->getInterfaceIFace()->addPopup(pInfo);
					}
					else if (!bDefUnitCargo && !bAttUnitCargo)
					{	//NO UNIT
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CARGO, pUnit->getID(), pAttackUnit->getID());
						gDLL->getInterfaceIFace()->addPopup(pInfo);
					}
					else
					{ //TWICE
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CHOICE, pUnit->getID(), pAttackUnit->getID());
						gDLL->getInterfaceIFace()->addPopup(pInfo);
					}
				}
			}
			else if (pPopupReturn->getButtonClicked() == 3)
			{
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_END, pUnit->getID(), pAttackUnit->getID(), -1);
			}
		}
		break;
	case BUTTONPOPUP_ROB_UNIT_CREW:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvUnit* pAttackUnit = kPlayer.getUnit(info.getData2());
			if (pPopupReturn->getButtonClicked() == 1)
			{
				int iGold = pPopupReturn->getSpinnerWidgetValue(0);
				int iValue;
				if (pUnit->acceptCorruption(iGold, pUnit->getEstimatedPrice(true), true))
				{
					iValue = 1;
					gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_CREW, pUnit->getID(), iGold, -1);
				}
				else
				{
					iValue = 2;
					gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_CREW, pUnit->getID(), -1, -1);
				}
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CREW, pUnit->getID(), info.getData2(), iValue);
				gDLL->getInterfaceIFace()->addPopup(pInfo);
			}
			else if (pPopupReturn->getButtonClicked() == 2)
			{
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_CREW, pUnit->getID(), -1, -1);
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CREW, pUnit->getID(), info.getData2(), 3);
				gDLL->getInterfaceIFace()->addPopup(pInfo);
			}
			else if (pPopupReturn->getButtonClicked() == 3)
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_PASSENGERS, pUnit->getID(), -1, -1);
				gDLL->getInterfaceIFace()->addPopup(pInfo);
			}
		}
		break;
	case BUTTONPOPUP_ROB_UNIT_PASSENGERS:
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvUnit* pAttackUnit = kPlayer.getUnit(info.getData2());
			if (pPopupReturn->getButtonClicked() >= 0)
			{
				CvUnit* pCargoUnit = kPlayer.getUnit(pPopupReturn->getButtonClicked());
				if (pCargoUnit != NULL)
				{
					int iGold = pPopupReturn->getSpinnerWidgetValue(pCargoUnit->getID());
					pCargoUnit->setProposePrice(iGold);//Useless to share this information on MP
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_PASSENGERS, pUnit->getID(), info.getData2());
					gDLL->getInterfaceIFace()->addPopup(pInfo);
				}
			}
			else if (pPopupReturn->getButtonClicked() == -1)
			{//We propose a transaction to passengers
				CvPlot* pPlot = pUnit->plot();
				bool bBorgetValidate = false;
				for (int iI = 0; iI < 2; ++iI)
				{
					CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
					while(pUnitNode != NULL)
					{
						CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
						pUnitNode = pPlot->nextUnitNode(pUnitNode);
						CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
						if (pTranportUnit == pUnit && pCargoUnit->getProfession() != NO_PROFESSION && !bBorgetValidate)
						{
							if (iI == 0)
							{
								if (pCargoUnit->getOriginalOwner() != kPlayer.getID())
								{
									bBorgetValidate = (pCargoUnit->getProposePrice() != pPopupReturn->getSpinnerWidgetValue(pCargoUnit->getID()));
								}
							}
							else
							{
								int iGold = pCargoUnit->getProposePrice();
								int iEstimatePrice = pCargoUnit->getEstimatedPrice(true);
								if (!pCargoUnit->acceptCorruption(iGold, iEstimatePrice, true))
								{
									pCargoUnit->setProposePrice(-1);//Useless to share this information on MP and allow us to know if a unit refuse the deal
									if (pAttackUnit != NULL)
									{
										gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_PASSENGERS, pCargoUnit->getID(), -1, -1);
									}
								}
							}
						}
					}
				}
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_PASSENGERS, pUnit->getID(), info.getData2(), bBorgetValidate ? -1 : 1);//We ask to player which units he wishes
				gDLL->getInterfaceIFace()->addPopup(pInfo);
			}
			else if (pPopupReturn->getButtonClicked() == -2)
			{//We refuse passengers
				CvPlot* pPlot = pUnit->plot();
				CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
				while(pUnitNode != NULL)
				{
					CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = pPlot->nextUnitNode(pUnitNode);
					CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
					if (pTranportUnit == pUnit && pCargoUnit->getProfession() != NO_PROFESSION)
					{
						gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_PASSENGERS, pCargoUnit->getID(), -1, -1);
					}
				}
				if (pAttackUnit != NULL)
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_PASSENGERS, pUnit->getID(), info.getData2(), 1);
					gDLL->getInterfaceIFace()->addPopup(pInfo);
				}
				else
				{
					gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_END, pUnit->getID(), -1, -1);
				}
			}
			else if (pPopupReturn->getButtonClicked() == -3)
			{//We choose some passengers
				CvPlot* pPlot = pUnit->plot();
				CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
				while(pUnitNode != NULL)
				{
					CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = pPlot->nextUnitNode(pUnitNode);
					CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
					if (pTranportUnit == pUnit && pCargoUnit->getProfession() != NO_PROFESSION)
					{
						int iGold = -1;
						if (pCargoUnit->getOriginalOwner() != kPlayer.getID())
						{
							if (pCargoUnit->getProposePrice() != -1 && pPopupReturn->getCheckboxBitfield(pCargoUnit->getID()) & 0x01)
							{
								iGold = pCargoUnit->getProposePrice();
							}
						}
						pCargoUnit->setProposePrice(0);//Useless to share this information on MP
						gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_PASSENGERS, pCargoUnit->getID(), -1, iGold);
					}
				}
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_END, pUnit->getID(), -1, -1);
			}
			else if (pPopupReturn->getButtonClicked() == -4)
			{//Come back to BUTTONPOPUP_ROB_UNIT_CHOICE
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CHOICE, pUnit->getID(), pAttackUnit->getID());
				gDLL->getInterfaceIFace()->addPopup(pInfo);
			}
			else if (pPopupReturn->getButtonClicked() == -6)
			{//Come back to BUTTONPOPUP_ROB_UNIT_CHOICE
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CHOICE, pUnit->getID(), pAttackUnit->getID(), 1);
				gDLL->getInterfaceIFace()->addPopup(pInfo);
			}
			else if (pPopupReturn->getButtonClicked() == -5 || pPopupReturn->getButtonClicked() == -8)
			{
				int iNumUnloadedUnit = 0;
				int iNumLoadedUnit = 0;
				CvPlot* pPlot = pAttackUnit->plot();
				CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
				while(pUnitNode != NULL)
				{
					CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = pPlot->nextUnitNode(pUnitNode);
					CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
					if (pTranportUnit == pAttackUnit && pCargoUnit->getProfession() != NO_PROFESSION)
					{
						if (!(pPopupReturn->getCheckboxBitfield(pCargoUnit->getID()) & 0x01))
						{
							iNumUnloadedUnit++;
						}
					}
				}
				pPlot = pUnit->plot();
				pUnitNode = pPlot->headUnitNode();
				while(pUnitNode != NULL)
				{
					CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = pPlot->nextUnitNode(pUnitNode);
					CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
					if (pTranportUnit == pUnit && pCargoUnit->getProfession() != NO_PROFESSION)
					{
						if (pPopupReturn->getCheckboxBitfield(pCargoUnit->getID()) & 0x01)
						{
							iNumLoadedUnit++;
						}
					}
				}
				int iAttSpace = pAttackUnit->getSpaceNewCargo()/30;
				int iDefSpace = pUnit->getSpaceNewCargo()/30;
				int iDifCargo = iNumLoadedUnit - iNumUnloadedUnit ;
				if (iAttSpace >= iDifCargo && iDefSpace >= -iDifCargo)
				{
					pPlot = pAttackUnit->plot();
					CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
					while(pUnitNode != NULL)
					{
						CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
						pUnitNode = pPlot->nextUnitNode(pUnitNode);
						CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
						if (pTranportUnit == pAttackUnit && pCargoUnit->getProfession() != NO_PROFESSION)
						{
							if (!(pPopupReturn->getCheckboxBitfield(pCargoUnit->getID()) & 0x01))
							{
								gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_PASSENGERS, pCargoUnit->getID(), pUnit->getID(), -1);
							}
						}
					}
					pPlot = pUnit->plot();
					pUnitNode = pPlot->headUnitNode();
					while(pUnitNode != NULL)
					{
						CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
						pUnitNode = pPlot->nextUnitNode(pUnitNode);
						CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
						if (pTranportUnit == pUnit && pCargoUnit->getProfession() != NO_PROFESSION)
						{
							if (pPopupReturn->getCheckboxBitfield(pCargoUnit->getID()) & 0x01)
							{
								gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_PASSENGERS, pCargoUnit->getID(), pAttackUnit->getID(), pCargoUnit->getProposePrice());
							}
						}
					}
					if (pPopupReturn->getButtonClicked() == -8)
					{
						kPlayer.setLocalPlayerActionPopupData3(1);
						gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_LAUNCH_POPUP, BUTTONPOPUP_ROB_UNIT_PASSENGERS, pUnit->getID(), pAttackUnit->getID());
					}
					else
					{
						kPlayer.setLocalPlayerActionPopupData3(1);
						gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_LAUNCH_POPUP, BUTTONPOPUP_ROB_UNIT_PASSENGERS, pUnit->getID(), pAttackUnit->getID());
					}
				}
				else
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_PASSENGERS, pUnit->getID(), info.getData2(), 1);
					gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
					pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_PASSENGERS, pUnit->getID(), info.getData2(), iAttSpace >= iDifCargo ? 2 : 3);
					gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true, true);
				}
			}
			else if (pPopupReturn->getButtonClicked() == -7)
			{
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_END, pUnit->getID(), pAttackUnit->getID(), -1);
			}
		}
		break;
	case BUTTONPOPUP_ROB_UNIT_CARGO:
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvUnit* pAttackUnit = kPlayer.getUnit(info.getData2());
			int iButton = pPopupReturn->getButtonClicked();
			if (info.getData3() == -1)
			{
				if (iButton >= 0)
				{
					int iState = iButton/NUM_YIELD_TYPES, iValue = 0;
					YieldTypes eYield = (YieldTypes) (iButton%NUM_YIELD_TYPES);
					if (iState == 0)
					{
						iValue = pUnit->getSpaceNewCargo();
						iValue = -std::min(iValue, pAttackUnit->getNewCargoYield(eYield));
					}
					else if (iState == 1)
					{
						iValue = -pPopupReturn->getSpinnerWidgetValue(iButton);
					}
					else if (iState == 2)
					{
						iValue = pAttackUnit->getSpaceNewCargo();
						iValue = std::min(iValue, pUnit->getNewCargoYield(eYield));
					}
					else if (iState == 3)
					{
						iValue = pPopupReturn->getSpinnerWidgetValue(iButton);
					}

					if (iValue != 0)
					{
						gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_CARGO, pAttackUnit->getID(), eYield, iValue);
						gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_CARGO, pUnit->getID(), eYield, -iValue);
					}
					kPlayer.setLocalPlayerActionPopupData3(-1);
					gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_LAUNCH_POPUP, BUTTONPOPUP_ROB_UNIT_CARGO, pUnit->getID(), pAttackUnit->getID());
				}
				else if (iButton == -1)
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CARGO, pUnit->getID(), pAttackUnit->getID(), 1);
					gDLL->getInterfaceIFace()->addPopup(pInfo);
				}
				else if (iButton == -2)
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CHOICE, pUnit->getID(), pAttackUnit->getID(), 1);
					gDLL->getInterfaceIFace()->addPopup(pInfo);
				}
			}
			else
			{
				if (iButton >= 0)
				{
					int iState = iButton/NUM_YIELD_TYPES, iValue = 0;
					YieldTypes eYield = (YieldTypes) (iButton%NUM_YIELD_TYPES);
					if (iState == 0)
					{
						iValue = -pAttackUnit->getNewCargoYield(eYield);
					}
					else if (iState == 1)
					{
						iValue = -pPopupReturn->getSpinnerWidgetValue(iButton);
					}
					else if (iState == 2)
					{
						iValue = pUnit->getNewCargoYield(eYield);
					}
					else if (iState == 3)
					{
						iValue = pPopupReturn->getSpinnerWidgetValue(iButton);
					}
					if (iValue > 0)
					{
						gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_CARGO, pUnit->getID(), eYield, -iValue);
					}
					else if (iValue < 0)
					{
						gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_CARGO, pAttackUnit->getID(), eYield, iValue);
					}
					kPlayer.setLocalPlayerActionPopupData3(1);
					gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_LAUNCH_POPUP, BUTTONPOPUP_ROB_UNIT_CARGO, pUnit->getID(), pAttackUnit->getID());
				}
				else if (iButton == -2)
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CARGO, pUnit->getID(), pAttackUnit->getID());
					gDLL->getInterfaceIFace()->addPopup(pInfo);
				}
			}
			if (iButton == -3)
			{
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_END, pUnit->getID(), pAttackUnit->getID(), -1);
			}
		}
		break;
	case BUTTONPOPUP_ROB_UNIT_CHOICE:
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			CvUnit* pUnit = kPlayer.getUnit(info.getData1());
			CvUnit* pAttackUnit = kPlayer.getUnit(info.getData2());
			if (pPopupReturn->getButtonClicked() == 1)
			{
				bool bCargoUnit = (pUnit->getNewCargo() - pUnit->getOnlyNewCargo()) > 0;
				if (info.getData3() == 1)
				{
					bCargoUnit = false;
				}
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_PASSENGERS, pUnit->getID(), pAttackUnit->getID(), bCargoUnit ? -1 : 1);//We ask to player which units he wishes
				gDLL->getInterfaceIFace()->addPopup(pInfo);
			}
			else if (pPopupReturn->getButtonClicked() == 2)
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT_CARGO, pUnit->getID(), pAttackUnit->getID());
				gDLL->getInterfaceIFace()->addPopup(pInfo);
			}
			else if (pPopupReturn->getButtonClicked() == 3)
			{
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_END, pUnit->getID(), pAttackUnit->getID(), -1);
			}
		}
		break;
	case BUTTONPOPUP_TUTORIAL_DOANE_0:
	case BUTTONPOPUP_TUTORIAL_DOANE_1:
	case BUTTONPOPUP_TUTORIAL_DOANE_2:
		if (pPopupReturn->getButtonClicked())
		{
			if (pPopupReturn->getCheckboxBitfield(1) & 0x01)
			{
				gDLL->sendPlayerOption(PLAYEROPTION_TUTORIAL_DOANE, false);
			}
		}
		break;
	case BUTTONPOPUP_MAKE_THE_FIRST_SEAWAY:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
			int iLoop;
			CvCity* pCity = kPlayer.firstCity(&iLoop);
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_MAKE_THE_FIRST_SEAWAY, info.getData1(), pCity->getID(), pPopupReturn->getButtonClicked());
		}
		break;
	case BUTTONPOPUP_TRADE_ROUTE_RADIO_QUANTITY:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SET_TRADE_ROUTE_RADIO_OPTION_QUANTITY, info.getData1(), info.getData2(), pPopupReturn->getCurrentSpinBoxValue(0));
		}
		break;
	case BUTTONPOPUP_TRADE_ROUTE_RADIO_EUROPEAN_QUANTITY:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SET_TRADE_ROUTE_RADIO_EUROPEAN_OPTION_QUANTITY, info.getData2(), info.getData3(), pPopupReturn->getCurrentSpinBoxValue(0));
		}
		break;
	case BUTTONPOPUP_TRADE_ROUTE_CHECK_BOX_QUANTITY:
		if (pPopupReturn->getButtonClicked() > 0)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_SET_TRADE_ROUTE_CHECK_BOX_OPTION_QUANTITY, info.getData3(), /*Yieldd*/info.getData2(), pPopupReturn->getCurrentSpinBoxValue(0));
		}
		break;
	case BUTTONPOPUP_TRADE_ROUTE_ADD_RESOURCE_POPUP:
		if (pPopupReturn->getButtonClicked() >= 0)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_CHANGE_TRADE_TYPE, /*TradeRouteId*/info.getData1(), /*tradeType*/info.getData2(), /*Yieldd*/pPopupReturn->getButtonClicked());
		}
		break;
	case BUTTONPOPUP_CHOOSE_GOODY:
		if (pPopupReturn->getButtonClicked() == 1)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_RECEIVE_GOODY, info.getData1(), info.getData2(), info.getData3());
		}
		break;

	case BUTTONPOPUP_SELECT_YIELD_AMOUNT:
		{
			CvUnit* pUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(info.getData2());
			if (pUnit != NULL)
			{
				if (pUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
				{
					gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), (PlayerActionTypes)info.getData3(), info.getData1(), pPopupReturn->getCurrentSpinBoxValue(0), info.getData2());
				}
				else
				{
					gDLL->sendDoCommand(info.getData2(), (CommandTypes)info.getData3(), info.getData1(), pPopupReturn->getCurrentSpinBoxValue(0), false);
				}
			}
		}
		break;

	case BUTTONPOPUP_TALK_NATIVES:
		if (pPopupReturn->getButtonClicked() >= 0 && pPopupReturn->getButtonClicked() < NUM_COMMAND_TYPES)
		{
			gDLL->sendDoCommand(info.getData1(), (CommandTypes) pPopupReturn->getButtonClicked(), -1, -1, false);
		}
		else if (pPopupReturn->getButtonClicked() == NUM_COMMAND_TYPES)
		{
			gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_FEAT, FEAT_TALK_NATIVES, 1, -1);
		}
		break;

	default:
		FAssert(false);
		break;
	}
}

void CvDLLButtonPopup::OnFocus(CvPopup* pPopup, CvPopupInfo &info)
{
	if (gDLL->getInterfaceIFace()->popupIsDying(pPopup))
	{
		return;
	}

	switch (info.getButtonPopupType())
	{
	case BUTTONPOPUP_CHOOSEPRODUCTION:
		{
			PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
			CvCity* pCity = GET_PLAYER(ePlayer).getCity(info.getData1());

			if (NULL == pCity || pCity->getOwnerINLINE() != ePlayer || pCity->isProduction())
			{
				gDLL->getInterfaceIFace()->popupSetAsCancelled(pPopup);
				break;
			}

			gDLL->getInterfaceIFace()->lookAtCityOffset(pCity->getID());
		}
		break;

		/*case BUTTONPOPUP_CHOOSE_EDUCATION:
		{
		PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
		CvCity* pCity = GET_PLAYER(ePlayer).getCity(info.getData1());
		if (NULL == pCity || pCity->getOwnerINLINE() != ePlayer)
		{
		gDLL->getInterfaceIFace()->popupSetAsCancelled(pPopup);
		}
		else
		{
		gDLL->getInterfaceIFace()->lookAtCityOffset(pCity->getID());
		}

		}
		break;*/

	case BUTTONPOPUP_RAZECITY:
		{
			PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
			CvCity* pCity = GET_PLAYER(ePlayer).getCity(info.getData1());

			if (NULL == pCity || pCity->getOwnerINLINE() != ePlayer)
			{
				gDLL->getInterfaceIFace()->popupSetAsCancelled(pPopup);
				break;
			}

			gDLL->getInterfaceIFace()->lookAtCityOffset(pCity->getID());
		}
		break;

	case BUTTONPOPUP_PYTHON:
	case BUTTONPOPUP_FEAT:
	case BUTTONPOPUP_PYTHON_SCREEN:
	case BUTTONPOPUP_MOVIE:
		if (!info.getOnFocusPythonCallback().IsEmpty())
		{
			long iResult;
			CyArgsList argsList;
			argsList.add(info.getData1());
			argsList.add(info.getData2());
			argsList.add(info.getData3());
			argsList.add(info.getFlags());
			argsList.add(info.getText());
			argsList.add(info.getOption1());
			argsList.add(info.getOption2());
			gDLL->getPythonIFace()->callFunction(PYScreensModule, info.getOnFocusPythonCallback(), argsList.makeFunctionArgs(), &iResult);
			if (0 != iResult)
			{
				gDLL->getInterfaceIFace()->popupSetAsCancelled(pPopup);
			}
		}
		break;
	}
}

// returns false if popup is not launched
bool CvDLLButtonPopup::launchButtonPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	bool bLaunched = false;
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer != NO_PLAYER)
	{
		CvPlayer& kPlayer = GET_PLAYER(ePlayer);
		if (kPlayer.isLaunchedPopup(info.getButtonPopupType()))
		{
			return false;
		}
	}

	switch (info.getButtonPopupType())
	{
	case BUTTONPOPUP_TEXT:
		bLaunched = launchTextPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSEPRODUCTION:
		bLaunched = launchProductionPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSE_YIELD_BUILD:
		bLaunched = launchChooseYieldBuildPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSE_EDUCATION:
		bLaunched = launchEducationPopup(pPopup, info);
		break;
	case BUTTONPOPUP_RAZECITY:
		bLaunched = launchRazeCityPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ALARM:
		bLaunched = launchAlarmPopup(pPopup, info);
		break;
	case BUTTONPOPUP_DECLAREWARMOVE:
		bLaunched = launchDeclareWarMovePopup(pPopup, info);
		break;
	case BUTTONPOPUP_CONFIRMCOMMAND:
		bLaunched = launchConfirmCommandPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CONFIRMTASK:
		bLaunched = launchConfirmTaskPopup(pPopup, info);
		break;
	case BUTTONPOPUP_LOADUNIT:
		bLaunched = launchLoadUnitPopup(pPopup, info);
		break;
	case BUTTONPOPUP_LOAD_CARGO:
		bLaunched = launchLoadCargoPopup(pPopup, info);
		break;
	case BUTTONPOPUP_LEADUNIT:
		bLaunched = launchLeadUnitPopup(pPopup, info);
		break;
	case BUTTONPOPUP_MAIN_MENU:
		bLaunched = launchMainMenuPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CONFIRM_MENU:
		bLaunched = launchConfirmMenu(pPopup, info);
		break;
	case BUTTONPOPUP_PYTHON_SCREEN:
		bLaunched = launchPythonScreen(pPopup, info);
		break;
	case BUTTONPOPUP_MOVIE:
		bLaunched = launchMovie(pPopup, info);
		break;
	case BUTTONPOPUP_DEAL_CANCELED:
		bLaunched = launchCancelDeal(pPopup, info);
		break;
	case BUTTONPOPUP_PYTHON:
	case BUTTONPOPUP_FEAT:
		bLaunched = launchPythonPopup(pPopup, info);
		break;
	case BUTTONPOPUP_DETAILS:
		bLaunched = launchDetailsPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ADMIN:
		bLaunched = launchAdminPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ADMIN_PASSWORD:
		bLaunched = launchAdminPasswordPopup(pPopup, info);
		break;
	case BUTTONPOPUP_EXTENDED_GAME:
		bLaunched = launchExtendedGamePopup(pPopup, info);
		break;
	case BUTTONPOPUP_DIPLOMACY:
		bLaunched = launchDiplomacyPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ADDBUDDY:
		bLaunched = launchAddBuddyPopup(pPopup, info);
		break;
	case BUTTONPOPUP_FORCED_DISCONNECT:
		bLaunched = launchForcedDisconnectPopup(pPopup, info);
		break;
	case BUTTONPOPUP_PITBOSS_DISCONNECT:
		bLaunched = launchPitbossDisconnectPopup(pPopup, info);
		break;
	case BUTTONPOPUP_KICKED:
		bLaunched = launchKickedPopup(pPopup, info);
		break;
	case BUTTONPOPUP_EVENT:
		bLaunched = launchEventPopup(pPopup, info);
		break;
	case BUTTONPOPUP_FREE_COLONY:
		bLaunched = launchFreeColonyPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSE_PROFESSION:
		bLaunched = launchChooseProfessionPopup(pPopup, info);
		break;
	case BUTTONPOPUP_HOSPITAL:
		bLaunched = launchManageHospitals(pPopup, info);
		break;
	case BUTTONPOPUP_RESUPPLY_BUILDER_PACK:
		bLaunched = launchResupplyBuilderPackPopup(pPopup, info);
		break;
	case BUTTONPOPUP_UNLOAD_NEW_CARGO:
		bLaunched = launchUnloadNewCargoPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSE_QUANTITY_TRADE:
		bLaunched = launchChooseQuantityTradePopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSE_PROFESSION_IN_EUROPE:
		bLaunched = launchChooseProfessionInEuropePopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSE_CITY_DESTINATION_IN_EUROPE:
		bLaunched = launchChooseCityDestinationInEuropePopup(pPopup, info);
		break;
	case BUTTONPOPUP_VALIDATE_PROFESSION_IN_EUROPE:
		bLaunched = launchValidateProfessionInEuropePopup(pPopup, info);
		break;
	case BUTTONPOPUP_EJECT_UNIT_TO_GROUP:
		bLaunched = launchEjectUnitToPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ASK_SAILOR_FORMATION:
		bLaunched = launchAskSailorFormationPopup(pPopup, info);
		break;
	case BUTTONPOPUP_DESTROY_CITY:
		bLaunched = launchDestroyCityPopup(pPopup, info);
		break;
	case BUTTONPOPUP_REMOVE_RESSOURCE:
		bLaunched = launchRemoveRessourcePopup(pPopup, info);
		break;
	case BUTTONPOPUP_REPAIR_SHIP:
		bLaunched = launchRepairShipPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ARSENAL_MANAGEMENT:
		bLaunched = launchArsenalManagementPopup(pPopup, info);
		break;
	case BUTTONPOPUP_LEADUNITEXP:
		bLaunched = launchLeadUnitExpPopup(pPopup, info);
		break;
	case BUTTONPOPUP_PURCHASE_EUROPE_UNIT:
		bLaunched = launchPurchaseEuropeUnitPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CIVIC_OPTION:
		bLaunched = launchCivicOptionPopup(pPopup, info);
		break;
	case BUTTONPOPUP_YIELD_PRODUCTION:
		bLaunched = launchYieldProductionPopup(pPopup, info);
		break;
	case BUTTONPOPUP_PROMOTE:
		bLaunched = launchPromotionPopup(pPopup, info);
		break;
	case BUTTONPOPUP_EMBARK_ON_SHIP:
		bLaunched = launchEmbarkOnShipPopup(pPopup, info);
		break;
	case BUTTONPOPUP_LOAD_CREW:
		bLaunched = launchLoadCrewPopup(pPopup, info);
		break;
	case BUTTONPOPUP_GIVE_MAP:
		bLaunched = launchGiveMapPopup(pPopup, info);
		break;
	case BUTTONPOPUP_TRANSFERT_MAP_TO_EUROPE:
		bLaunched = launchTransfertMapToEuropePopup(pPopup, info);
		break;
	case BUTTONPOPUP_REMOVE_ASSIGNED_UNIT:
		bLaunched = launchRemoveAssignedUnitPopup(pPopup, info);
		break;
	case BUTTONPOPUP_NEW_AGREEMENT:
		bLaunched = launchNewAgreementPopup(pPopup, info);
		break;
	case BUTTONPOPUP_NEW_AGREEMENT_PART1:
		bLaunched = launchNewAgreementPart1Popup(pPopup, info);
		break;
	case BUTTONPOPUP_NEW_AGREEMENT_PART2:
		bLaunched = launchNewAgreementPart2Popup(pPopup, info);
		break;
	case BUTTONPOPUP_END_OF_AGREEMENT:
		bLaunched = launchEndOfAgreementPopup(pPopup, info);
		break;
	case BUTTONPOPUP_AUTOMATIC_RENEW_AGREEMENT:
		bLaunched = launchAutomaticRenewAgreementPopup(pPopup, info);
		break;
	case BUTTONPOPUP_AUTOMATIC_AGREEMENT_BY_YIELDS:
		bLaunched = launchAutomaticAgreementByYieldsPopup(pPopup, info);
		break;
	case BUTTONPOPUP_WAREHOUSE_UPGRADE:
		bLaunched = launchWarehouseUpgradePopup(pPopup, info);
		break;
	case BUTTONPOPUP_AGREEMENT_LIST:
		bLaunched = launchAgreementListPopup(pPopup, info);
		break;
	case BUTTONPOPUP_SEEDLING_LIST:
		bLaunched = launchSeedlingListPopup(pPopup, info);
		break;
	case BUTTONPOPUP_SEEDLING_DISCOVERY:
		bLaunched = launchSeedlingDiscoveryGoodSituationPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHANGE_CITY_AGREEMENT:
		bLaunched = launchChangeCityAgreementPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSE_AGREEMENTS:
		bLaunched = launchChooseAgreementsPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSE_TRADE_ROUTES:
		bLaunched = launchChooseTradeRoutesPopup(pPopup, info);
		break;
	case BUTTONPOPUP_SEND_TRADE_PROPOSITION:
		bLaunched = launchSendTradePropositionPopup(pPopup, info);
		break;
	case BUTTONPOPUP_PROCESS_TRADE_PROPOSITION:
		bLaunched = launchProcessTradePropositionPopup(pPopup, info);
		break;
	case BUTTONPOPUP_STOP_TRADE:
		bLaunched = launchStopTradePopup(pPopup, info);
		break;
	case BUTTONPOPUP_SELL_SHIP:
		bLaunched = launchSellShipPopup(pPopup, info);
		break;
	case BUTTONPOPUP_STOP_SELL_SHIP:
		bLaunched = launchStopSellShipPopup(pPopup, info);
		break;
	case BUTTONPOPUP_RECRUIT_CREW:
		bLaunched = launchRecruitCrewPopup(pPopup, info);
		break;
	case BUTTONPOPUP_RECRUIT_IMMIGRANT:
		bLaunched = launchRecruitImmigrantPopup(pPopup, info);
		break;
	case BUTTONPOPUP_BUY_USED_SHIP:
		bLaunched = launchBuyShipUsedPopup(pPopup, info);
		break;
	case BUTTONPOPUP_BUY_NEW_SHIP_IN_EUROPE:
		bLaunched = launchBuyNewShipInEuropePopup(pPopup, info);
		break;
	case BUTTONPOPUP_PURCHASE_BID:
		bLaunched = launchPurchaseBidPopup(pPopup, info);
		break;
	case BUTTONPOPUP_RECEIVE_SEEDLING:
		bLaunched = launchReceiveSeedlingPopup(pPopup, info);
		break;
	case BUTTONPOPUP_RUM_DISCOVERED:
		bLaunched = launchRumDiscoveredPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ACHIEVEMENTS:
		bLaunched = launchAchievementPopup(pPopup, info);
		break;
	case BUTTONPOPUP_FOUND_COLONY:
		bLaunched = launchFoundColonyPopup(pPopup, info);
		break;
	case BUTTONPOPUP_REPAY_LOAN:
		bLaunched = launchRepayLoan(pPopup, info);
		break;
	case BUTTONPOPUP_WHEN_NEW_ERA:
		bLaunched = launchWhenNewEraPopup(pPopup, info);
		break;
	case BUTTONPOPUP_OPEN_BREACH_FORTIFICATION:
		bLaunched = launchOpenBreachFortificationPopup(pPopup, info);
		break;
	case BUTTONPOPUP_RESUPPLY_SHIPS:
		bLaunched = launchResupplyShipsPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ROB_UNIT:
		bLaunched = launchRobUnitPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ROB_UNIT_CREW:
		bLaunched = launchRobUnitCrewPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ROB_UNIT_PASSENGERS:
		bLaunched = launchRobUnitPassengersPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ROB_UNIT_CARGO:
		bLaunched = launchRobUnitCargoPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ROB_UNIT_CHOICE:
		bLaunched = launchRobUnitChoicePopup(pPopup, info);
		break;
	case BUTTONPOPUP_TUTORIAL_DOANE_0:
		bLaunched = launchTutorialDoanePopup0(pPopup, info);
		break;
	case BUTTONPOPUP_TUTORIAL_DOANE_1:
		bLaunched = launchTutorialDoanePopup1(pPopup, info);
		break;
	case BUTTONPOPUP_TUTORIAL_DOANE_2:
		bLaunched = launchTutorialDoanePopup2(pPopup, info);
		break;
	case BUTTONPOPUP_MAKE_THE_FIRST_SEAWAY:
		bLaunched = launchMakeTheFirstSeawayPopup(pPopup, info);
		break;
	case BUTTONPOPUP_TRADE_ROUTE_RADIO_QUANTITY:
		bLaunched = launchTradeRouteRadioQuantityPopup(pPopup, info);
		break;
	case BUTTONPOPUP_TRADE_ROUTE_RADIO_EUROPEAN_QUANTITY:
		bLaunched = launchTradeRouteRadioEuropeanQuantityPopup(pPopup, info);
		break;
	case BUTTONPOPUP_TRADE_ROUTE_CHECK_BOX_QUANTITY:
		bLaunched = launchTradeRouteCheckBoxQuantityPopup(pPopup, info);
		break;
	case BUTTONPOPUP_TRADE_ROUTE_ADD_RESOURCE_POPUP:
		bLaunched = launchTradeRouteAddResourcePopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSE_GOODY:
		bLaunched = launchChooseGoodyPopup(pPopup, info);
		break;
	case BUTTONPOPUP_SELECT_YIELD_AMOUNT:
		bLaunched = launchSelectYieldAmountPopup(pPopup, info);
		break;
	case BUTTONPOPUP_TALK_NATIVES:
		bLaunched = launchTalkNativesPopup(pPopup, info);
		break;
	case BUTTONPOPUP_VICTORY_POPUP:
		bLaunched = launchVictoryPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHOOSE_START_SPECIALIST:
		bLaunched = launchChooseStartSpecialistPopup(pPopup, info);
		break;
	case BUTTONPOPUP_ASK_USE_RELATION_POINTS_FOR_IMMIGRATION:
		bLaunched = launchAskUseRelationPointsForImmigrationPopup(pPopup, info);
		break;
	case BUTTONPOPUP_CHANGE_MIN_GOLD_TO_CONSERVE_FOR_TRADE_ROUTE:
		bLaunched = launchChangeMinGoldToConserveForTradeRoutePopup(pPopup, info);
		break;
	default:
		FAssert(false);
		break;
	}

	if (bLaunched && ePlayer != NO_PLAYER)
	{
		gDLL->sendPlayerAction(ePlayer, PLAYER_ACTION_LAUNCHED_POPUP, info.getButtonPopupType(), true, -1);
	}

	return (bLaunched);
}

// returns true if popup has to be launched once in the game
bool CvDLLButtonPopup::launchOnlyOncePopup(ButtonPopupTypes eButtonPopup)
{
	switch (eButtonPopup)
	{
	case BUTTONPOPUP_TUTORIAL_DOANE_0:
	case BUTTONPOPUP_TUTORIAL_DOANE_1:
	case BUTTONPOPUP_TUTORIAL_DOANE_2:
	case BUTTONPOPUP_MAKE_THE_FIRST_SEAWAY:
		return true;
	default:
		return false;
	}
}

// returns true if popup has to be launched once by turn
bool CvDLLButtonPopup::launchOnlyOnceByTurnPopup(ButtonPopupTypes eButtonPopup)
{
	switch (eButtonPopup)
	{
	case BUTTONPOPUP_TRANSFERT_MAP_TO_EUROPE:
		return true;
	default:
		return false;
	}
}

bool CvDLLButtonPopup::launchTextPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, info.getText());
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);
	return (true);
}

bool CvDLLButtonPopup::launchVictoryPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	VictoryTypes eVictory = (VictoryTypes)info.getData1();
	if (eVictory == NO_VICTORY)
	{
		return false;
	}
	CvVictoryInfo& kVictory = GC.getVictoryInfo(eVictory);
	bool bWinner = info.getData2();

	TeamTypes eTeam = (TeamTypes)info.getData3();
	if (eTeam == NO_TEAM) 
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText(bWinner ? "TXT_KEY_POPUP_VICTORY_TITLE" : "TXT_KEY_POPUP_DEFEAT_TITLE"));

	if (bWinner && kVictory.getPictureFileName() != NULL )
	{
		gDLL->getInterfaceIFace()->popupAddDDS(pPopup, kVictory.getPictureFileName(), 300, 200);
	}
	if (!bWinner && kVictory.getPictureFileNameDefeat() != NULL )
	{
		gDLL->getInterfaceIFace()->popupAddDDS(pPopup, kVictory.getPictureFileNameDefeat(), 300, 200);
	}
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, info.getText());
	CvTeam& kTeam = GET_TEAM(eTeam);
	if (kTeam.getNumMembers() == 1)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText(bWinner ? "TXT_KEY_GAME_WON_ADDED_TEXTS" : "TXT_KEY_MISC_DEFEAT_ADDED_TEXTS", GET_TEAM(eTeam).getCivilizationAdjectiveKey().GetCString()));
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchProductionPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvWString szBuffer;
	CvString szArtFilename;

	CvCity* pCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(info.getData1());
	if (NULL == pCity || pCity->isProductionAutomated())
	{
		return (false);
	}

	//DOANE Python Callbacks | Speed Improvement
	if (GC.getUSE_ON_SKIP_PRODUCTION_POPUP_CALLBACK())
	{
		CyCity* pyCity = new CyCity(pCity);
		CyArgsList argsList;
		argsList.add(gDLL->getPythonIFace()->makePythonObject(pyCity));	// pass in plot class
		long lResult=0;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "skipProductionPopup", argsList.makeFunctionArgs(), &lResult); //Returns false as default
		delete pyCity;	// python fxn must not hold on to this pointer
		if (lResult == 1)
		{
			return (false);
		}
	}
	//END DOANE

	FAssertMsg(pCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer(), "City must belong to Active Player");

	UnitTypes eTrainUnit = NO_UNIT;
	BuildingTypes eConstructBuilding = NO_BUILDING;
	switch (info.getData2())
	{
	case (ORDER_TRAIN):
		eTrainUnit = (UnitTypes)info.getData3();
		break;
	case (ORDER_CONSTRUCT):
		eConstructBuilding = (BuildingTypes)info.getData3();
		break;
	default:
		break;
	}
	bool bFinish = info.getOption1();

	if (eTrainUnit != NO_UNIT)
	{
		if (bFinish)
		{
			szBuffer = gDLL->getText("TXT_KEY_POPUP_CONSTRUCTED_WORK_ON_NEXT", GC.getUnitInfo(eTrainUnit).getTextKeyWide(), pCity->getNameKey());
		}
		else
		{
			CvWString szRequires;
			for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
			{
				if (GC.getYieldInfo((YieldTypes) iYield).isCargo())
				{
					int iYieldNeeded = GET_PLAYER(pCity->getOwnerINLINE()).getYieldProductionNeeded(eTrainUnit, (YieldTypes) iYield);
					if (iYieldNeeded > pCity->getYieldStored((YieldTypes) iYield))
					{
						if (!szRequires.empty())
						{
							szRequires += L", ";
						}
						szRequires += CvWString::format(L"%d %s", iYieldNeeded, GC.getYieldInfo((YieldTypes) iYield).getDescription());
					}
				}
			}
			if (!szRequires.empty())
			{
				szRequires = gDLL->getText("TXT_KEY_POPUP_REQUIRES", szRequires.GetCString());;
			}

			szBuffer = gDLL->getText("TXT_KEY_POPUP_CANNOT_TRAIN_WORK_NEXT", GC.getUnitInfo(eTrainUnit).getTextKeyWide(), pCity->getNameKey(), szRequires.GetCString());
		}
		szArtFilename = GET_PLAYER(pCity->getOwnerINLINE()).getUnitButton(eTrainUnit);
	}
	else if (eConstructBuilding != NO_BUILDING)
	{
		if (bFinish)
		{
			szBuffer = gDLL->getText("TXT_KEY_POPUP_CONSTRUCTED_WORK_ON_NEXT", GC.getBuildingInfo(eConstructBuilding).getTextKeyWide(), pCity->getNameKey());
		}
		else
		{
			CvWString szRequires;
			for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
			{
				if (GC.getYieldInfo((YieldTypes) iYield).isCargo())
				{
					int iYieldNeeded = GET_PLAYER(pCity->getOwnerINLINE()).getYieldProductionNeeded(eConstructBuilding, (YieldTypes) iYield);
					if (iYieldNeeded > pCity->getYieldStored((YieldTypes) iYield))
					{
						if (!szRequires.empty())
						{
							szRequires += L", ";
						}
						szRequires += CvWString::format(L"%d %s", iYieldNeeded, GC.getYieldInfo((YieldTypes) iYield).getDescription());
					}
				}
			}
			if (!szRequires.empty())
			{
				szRequires = gDLL->getText("TXT_KEY_POPUP_REQUIRES", szRequires.GetCString());;
			}

			szBuffer = gDLL->getText("TXT_KEY_POPUP_CANNOT_CONSTRUCT_WORK_NEXT", GC.getBuildingInfo(eConstructBuilding).getTextKeyWide(), pCity->getNameKey(), szRequires.GetCString());
		}
		szArtFilename = GC.getBuildingInfo(eConstructBuilding).getButton();
	}
	else
	{
		szBuffer = gDLL->getText("TXT_KEY_POPUP_WHAT_TO_BUILD", pCity->getNameKey());
		szArtFilename = ARTFILEMGR.getInterfaceArtInfo("INTERFACE_POPUPBUTTON_PRODUCTION")->getPath();
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, szBuffer, DLL_FONT_LEFT_JUSTIFY);

	//DOANE Python Callbacks | Speed Improvement
	if (GC.getUSE_ON_SHOW_EXAMINE_CITY_BUTTON_CALLBACK())
	{
		CyCity* pyCity = new CyCity(pCity);
		CyArgsList argsList2;
		argsList2.add(gDLL->getPythonIFace()->makePythonObject(pyCity));	// pass in plot class
		long lResult=1;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "showExamineCityButton", argsList2.makeFunctionArgs(), &lResult); //Returns True as default
		delete pyCity;	// python fxn must not hold on to this pointer
		if (lResult == 1)
		{
			int iExamineCityID = 0;
			iExamineCityID = std::max(iExamineCityID, GC.getNumUnitInfos());
			iExamineCityID = std::max(iExamineCityID, GC.getNumBuildingInfos());

			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_EXAMINE_CITY").c_str(), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CITYSELECTION")->getPath(), iExamineCityID, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY);
		}
	}
	else //We want to run the code as default since python function always returns true
	{
		int iExamineCityID = 0;
		iExamineCityID = std::max(iExamineCityID, GC.getNumUnitInfos());
		iExamineCityID = std::max(iExamineCityID, GC.getNumBuildingInfos());

		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_EXAMINE_CITY").c_str(), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CITYSELECTION")->getPath(), iExamineCityID, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY);
	}
	//END DOANE

	UnitTypes eProductionUnit = pCity->getProductionUnit();
	BuildingTypes eProductionBuilding = pCity->getProductionBuilding();

	int iNumBuilds = 0;

	//DOANE Python Callbacks | Speed Improvement
	if (GC.getUSE_GET_RECOMMENDED_UNIT_CALLBACK())
	{
		CyCity* pyCity = new CyCity(pCity);
		CyArgsList argsList3;
		argsList3.add(gDLL->getPythonIFace()->makePythonObject(pyCity));	// pass in city class
		long lResult=-1;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "getRecommendedUnit", argsList3.makeFunctionArgs(), &lResult); //Returns NO_UNIT as default
		eProductionUnit = ((UnitTypes)lResult);
		delete pyCity;	// python fxn must not hold on to this pointer
	}

	if (GC.getUSE_GET_RECOMMENDED_BUILDING_CALLBACK())
	{
		CyCity* pyCity = new CyCity(pCity);
		CyArgsList argsList4; // XXX
		argsList4.add(gDLL->getPythonIFace()->makePythonObject(pyCity));	// pass in city class
		long lResult=-1;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "getRecommendedBuilding", argsList4.makeFunctionArgs(), &lResult); //Returns NO_BUILDING as default
		eProductionBuilding = ((BuildingTypes)lResult);
		delete pyCity;	// python fxn must not hold on to this pointer
	}
	//END DOANE


	if (eProductionUnit == NO_UNIT)
	{
		eProductionUnit = pCity->AI_bestUnit(true);
	}

	if (eProductionBuilding == NO_BUILDING)
	{
		eProductionBuilding = pCity->AI_bestBuilding(0, 0, true);
	}

	if (eProductionUnit != NO_UNIT)
	{
		if ((eProductionUnit != eTrainUnit) || bFinish)
		{
			int iTurns = pCity->getProductionTurnsLeft(eProductionUnit, 0);
			szBuffer = gDLL->getText("TXT_KEY_POPUP_RECOMMENDED", GC.getUnitInfo(eProductionUnit).getTextKeyWide(), iTurns);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, GET_PLAYER(pCity->getOwnerINLINE()).getUnitButton(eProductionUnit), GC.getUnitInfo(eProductionUnit).getUnitClassType(), WIDGET_TRAIN, GC.getUnitInfo(eProductionUnit).getUnitClassType(), pCity->getID(), true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
			iNumBuilds++;
		}
	}

	if (eProductionBuilding != NO_BUILDING)
	{
		if (eProductionBuilding != eConstructBuilding)
		{
			int iTurns = pCity->getProductionTurnsLeft(eProductionBuilding, 0);
			szBuffer = gDLL->getText("TXT_KEY_POPUP_RECOMMENDED", GC.getBuildingInfo(eProductionBuilding).getTextKeyWide(), iTurns);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, GC.getBuildingInfo(eProductionBuilding).getButton(), GC.getBuildingInfo(eProductionBuilding).getBuildingClassType(), WIDGET_CONSTRUCT, GC.getBuildingInfo(eProductionBuilding).getBuildingClassType(), pCity->getID(), true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
			iNumBuilds++;
		}
	}

	for (int iI = 0; iI < GC.getNumUnitClassInfos(); iI++)
	{
		UnitTypes eLoopUnit = (UnitTypes)GC.getCivilizationInfo(pCity->getCivilizationType()).getCivilizationUnits(iI);

		if (eLoopUnit != NO_UNIT)
		{
			if (eLoopUnit != eProductionUnit)
			{
				if (pCity->canTrain(eLoopUnit))
				{
					if ((eLoopUnit != eTrainUnit) || bFinish)
					{
						int iTurns = pCity->getProductionTurnsLeft(eLoopUnit, 0);
						szBuffer.Format(L"%s (%d)", GC.getUnitInfo(eLoopUnit).getDescription(), iTurns);
						gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, GET_PLAYER(pCity->getOwnerINLINE()).getUnitButton(eLoopUnit), iI, WIDGET_TRAIN, iI, pCity->getID(), true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
						iNumBuilds++;
					}
				}
			}
		}
	}
	for (int iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
	{
		BuildingTypes eLoopBuilding = ((BuildingTypes)(GC.getCivilizationInfo(pCity->getCivilizationType()).getCivilizationBuildings(iI)));

		if (eLoopBuilding != NO_BUILDING)
		{
			if (eLoopBuilding != eProductionBuilding)
			{
				if (pCity->canConstruct(eLoopBuilding))
				{
					if (eLoopBuilding != eConstructBuilding)
					{
						int iTurns = pCity->getProductionTurnsLeft(eLoopBuilding, 0);
						if (iTurns > 0)
						{
							szBuffer.Format(L"%s (%d)", GC.getBuildingInfo(eLoopBuilding).getDescription(), iTurns);
						}
						else
						{
							szBuffer.Format(L"%s", GC.getBuildingInfo(eLoopBuilding).getDescription());
						}
						gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, GC.getBuildingInfo(eLoopBuilding).getButton(), iI, WIDGET_CONSTRUCT, iI, pCity->getID(), true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY);
						iNumBuilds++;
					}
				}
			}
		}
	}

	if (0 == iNumBuilds)
	{
		// city cannot build anything, so don't show popup after all
		return (false);
	}

	CvWStringBuffer szExtraBuilds;
	bool bFirst = true;
	for (int iI = 0; iI < GC.getNumUnitClassInfos(); iI++)
	{
		UnitTypes eLoopUnit = (UnitTypes)GC.getCivilizationInfo(pCity->getCivilizationType()).getCivilizationUnits(iI);
		if (eLoopUnit != NO_UNIT)
		{
			if (pCity->canTrain(eLoopUnit, false, true) && !pCity->canTrain(eLoopUnit))
			{
				if (bFirst)
				{
					bFirst = false;
				}
				else
				{
					szExtraBuilds.append(L", ");
				}
				szExtraBuilds.append(GC.getUnitInfo(eLoopUnit).getDescription());

				for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
				{
					YieldTypes eYield = (YieldTypes) iYield;
					if (GC.getYieldInfo(eYield).isCargo())
					{
						int iNeeded = pCity->getYieldProductionNeeded(eLoopUnit, eYield) - pCity->getYieldStored(eYield);
						if (iNeeded > 0)
						{
							szExtraBuilds.append(CvWString::format(L" (%d%c)", iNeeded, GC.getYieldInfo(eYield).getChar()));
						}
					}
				}
			}
		}
	}
	for (int iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
	{
		BuildingTypes eLoopBuilding = ((BuildingTypes)(GC.getCivilizationInfo(pCity->getCivilizationType()).getCivilizationBuildings(iI)));
		if (eLoopBuilding != NO_BUILDING)
		{
			if (pCity->canConstruct(eLoopBuilding, false, true) && !pCity->canConstruct(eLoopBuilding))
			{
				if (bFirst)
				{
					bFirst = false;
				}
				else
				{
					szExtraBuilds.append(L", ");
				}
				szExtraBuilds.append(GC.getBuildingInfo(eLoopBuilding).getDescription());
				for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
				{
					YieldTypes eYield = (YieldTypes) iYield;
					if (GC.getYieldInfo(eYield).isCargo())
					{
						int iNeeded = pCity->getYieldProductionNeeded(eLoopBuilding, eYield) - pCity->getYieldStored(eYield);
						if (iNeeded > 0)
						{
							szExtraBuilds.append(CvWString::format(L" (%d%c)", iNeeded, GC.getYieldInfo(eYield).getChar()));
						}
					}
				}
			}
		}
	}

	if (!szExtraBuilds.isEmpty())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_CANNOT_BUILD") + szExtraBuilds.getCString(), DLL_FONT_LEFT_JUSTIFY);
	}

	gDLL->getInterfaceIFace()->popupSetPopupType(pPopup, POPUPEVENT_PRODUCTION, szArtFilename);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_MINIMIZED, 252);

	switch (info.getData2())
	{
	case ORDER_TRAIN:
		gDLL->getInterfaceIFace()->playGeneralSound(GC.getUnitInfo((UnitTypes)info.getData3()).getArtInfo(0, NO_PROFESSION)->getTrainSound());
		break;

	case ORDER_CONSTRUCT:
		gDLL->getInterfaceIFace()->playGeneralSound(GC.getBuildingInfo((BuildingTypes)info.getData3()).getConstructSound());
		break;

	default:
		break;
	}

	return (true);
}

bool CvDLLButtonPopup::launchChooseYieldBuildPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvCity* pCity = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCity(info.getData1());
	if (NULL == pCity || pCity->isProductionAutomated())
	{
		return false;
	}

	FAssertMsg(pCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer(), "City must belong to Active Player");

	YieldTypes eYield = (YieldTypes) info.getData2();
	if (eYield == NO_YIELD)
	{
		return false;
	}

	CvWString szBuffer = gDLL->getText("TXT_KEY_POPUP_CHOOSE_COMLPETED_BUILD", GC.getYieldInfo(eYield).getTextKeyWide(), pCity->getNameKey());
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, szBuffer, DLL_FONT_LEFT_JUSTIFY);


	std::vector< std::pair<OrderTypes, int> > aOrders;
	pCity->getOrdersWaitingForYield(aOrders, eYield, true, pCity->getYieldStored(eYield) + pCity->getYieldRushed(eYield));

	if (aOrders.empty())
	{
		return false;
	}

	for (uint i = 0; i < aOrders.size(); ++i)
	{
		if (aOrders[i].first == ORDER_TRAIN)
		{
			UnitTypes eUnit = (UnitTypes) aOrders[i].second;
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, GC.getUnitInfo(eUnit).getDescription(), GET_PLAYER(pCity->getOwnerINLINE()).getUnitButton(eUnit), eUnit, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
		}
		else if (aOrders[i].first == ORDER_CONSTRUCT)
		{
			BuildingTypes eBuilding = (BuildingTypes) aOrders[i].second;
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, GC.getBuildingInfo(eBuilding).getDescription(), GC.getBuildingInfo(eBuilding).getButton(), GC.getNumUnitInfos() + eBuilding, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
		}
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WB_CITY_NOTHING"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CANCEL")->getPath(), -1, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupSetPopupType(pPopup, POPUPEVENT_PRODUCTION, ARTFILEMGR.getInterfaceArtInfo("INTERFACE_POPUPBUTTON_PRODUCTION")->getPath());
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_MINIMIZED, 252);

	return (true);
}

bool CvDLLButtonPopup::launchEducationPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvCity* pCity = kPlayer.getCity(info.getData1());
	if (pCity == NULL)
	{
		return false;
	}
	FAssertMsg(pCity->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer(), "City must belong to Active Player");

	CvUnit* pUnit = pCity->getPopulationUnitById(info.getData2());
	if (pUnit == NULL)
	{
		pUnit = kPlayer.getUnit(info.getData2());
	}
	if (pUnit == NULL)
	{
		return false;
	}

	CvWString szText = gDLL->getText("TXT_KEY_CHOOSE_SPECIALTY", pUnit->getNameKey(), pCity->getNameKey());
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, szText, DLL_FONT_LEFT_JUSTIFY);

	int iClassEducationCity = 1; //there are min schoolhouse
	int iClassEducationUnit = -1;

	for (int iJ = 0; iJ < GC.getNumBuildingClassInfos(); ++iJ)
	{
		BuildingTypes eBuilding = (BuildingTypes) iJ;
		if (eBuilding != NO_BUILDING)
		{
			if (GC.getBuildingInfo(eBuilding).getRefBuilding() == 5 && pCity->isHasBuilding(eBuilding))
			{
				iClassEducationCity = 2;
			}
			if (GC.getBuildingInfo(eBuilding).getRefBuilding() == 6 && pCity->isHasBuilding(eBuilding))
			{
				iClassEducationCity = 3;
			}
		}
	}
	// iClassEducationCity = 1 if there are schoolhouse in city
	// iClassEducationCity = 2 if there are college in city
	// iClassEducationCity = 3 if there are University in city

	if (iClassEducationCity >= 2 && kPlayer.getNewEra() > 0)
	{
		if (!pUnit->canBeSailor())
		{
			CvProfessionInfo& kProfession = GC.getProfessionInfo(PROFESSION_SAILOR);
			int iCost =  GC.getDefineINT("CREW_FORMATION_PRICE");
			if (kPlayer.getGold() >= iCost )
			{
				szText.Format(L"%s", kProfession.getDescription());
				szText += CvWString::format(L" (%d%c)", iCost, gDLL->getSymbolID(GOLD_CHAR));

				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szText, kProfession.getButton(), PROFESSION_SAILOR + GC.getNumUnitInfos()+1, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY);
			}
		}
	}
	for (int iI = 0; iI < GC.getNumUnitInfos(); iI++)
	{
		CvUnitInfo& kUnit = GC.getUnitInfo((UnitTypes) iI);
		iClassEducationUnit = kUnit.getClassEducation();
		int iPrice = pCity->getSpecialistTuition((UnitTypes) iI);
		if (iPrice >= 0 && iPrice <= kPlayer.getGold())
		{
			if (iClassEducationUnit != -1 && iClassEducationCity >= iClassEducationUnit)
			{
				szText.Format(L"%s", kUnit.getDescription());
				if (iPrice > 0)
				{
					szText += CvWString::format(L" (%d%c)", iPrice, gDLL->getSymbolID(GOLD_CHAR));
				}
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szText, kUnit.getButton(), iI, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY);
			}
		}
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CANCEL")->getPath(), GC.getNumUnitInfos(), WIDGET_GENERAL, -1, -1);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return true;
}


bool CvDLLButtonPopup::launchRazeCityPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& player = GET_PLAYER(GC.getGameINLINE().getActivePlayer());

	CvCity* pNewCity = player.getCity(info.getData1());
	if (NULL == pNewCity)
	{
		FAssert(false);
		return (false);
	}

	if (0 != GC.getDefineINT("PLAYER_ALWAYS_RAZES_CITIES"))
	{
		player.raze(pNewCity, false);
		return false;
	}

	PlayerTypes eHighestCulturePlayer = (PlayerTypes)info.getData2();

	int iCaptureGold = info.getData3();
	bool bRaze = player.canRaze(pNewCity, false);
	bool bGift = ((eHighestCulturePlayer != NO_PLAYER)
		&& (eHighestCulturePlayer != player.getID())
		&& ((player.getTeam() == GET_PLAYER(eHighestCulturePlayer).getTeam()) || GET_TEAM(player.getTeam()).isOpenBorders(GET_PLAYER(eHighestCulturePlayer).getTeam())));

	CvWString szBuffer;
	if (iCaptureGold > 0)
	{
		szBuffer = gDLL->getText("TXT_KEY_POPUP_GOLD_CITY_CAPTURE", iCaptureGold, pNewCity->getNameKey());
	}
	else
	{
		szBuffer = gDLL->getText("TXT_KEY_POPUP_CITY_CAPTURE_KEEP", pNewCity->getNameKey());
	}
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szBuffer);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_KEEP_CAPTURED_CITY").c_str(), NULL, 0, WIDGET_GENERAL);

	if (bRaze)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_RAZE_CAPTURED_CITY").c_str(), NULL, 1, WIDGET_GENERAL);
	}
	if (bGift)
	{
		szBuffer = gDLL->getText("TXT_KEY_POPUP_RETURN_ALLIED_CITY", GET_PLAYER(eHighestCulturePlayer).getCivilizationDescriptionKey());
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, NULL, 2, WIDGET_GENERAL, 2, eHighestCulturePlayer);
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	gDLL->getInterfaceIFace()->playGeneralSound("AS2D_CITYCAPTURE");

	return (true);
}

bool CvDLLButtonPopup::launchAlarmPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->playGeneralSound("AS2D_ALARM");

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_ALARM_TITLE").c_str());
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, info.getText());
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);

	return (true);
}


bool CvDLLButtonPopup::launchDeclareWarMovePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	TeamTypes eRivalTeam = (TeamTypes)info.getData1();
	int iX = info.getData2();
	int iY = info.getData3();

	FAssert(eRivalTeam != NO_TEAM);

	CvPlot* pPlot = GC.getMapINLINE().plotINLINE(iX, iY);
	bool bOpenBorders = info.getOption2();

	CvWString szBuffer;
	if ((pPlot != NULL) && pPlot->isOwned() && !bOpenBorders)
	{
		szBuffer = gDLL->getText("TXT_KEY_POPUP_ENTER_LANDS_WAR", GET_PLAYER(pPlot->getOwnerINLINE()).getCivilizationAdjective());

		if (GET_TEAM(GC.getGameINLINE().getActiveTeam()).isOpenBordersTrading())
		{
			szBuffer += gDLL->getText("TXT_KEY_POPUP_ENTER_WITH_OPEN_BORDERS");
		}
	}
	else
	{
		szBuffer = gDLL->getText("TXT_KEY_POPUP_DOES_THIS_MEAN_WAR", GET_TEAM(eRivalTeam).getName().GetCString());
	}
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szBuffer);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_DECLARE_WAR_YES").c_str(), NULL, 0);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_DECLARE_WAR_NO").c_str());
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}


bool CvDLLButtonPopup::launchConfirmCommandPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	int iAction = info.getData1();
	CvWString szBuffer;
	szBuffer = gDLL->getText("TXT_KEY_POPUP_ARE_YOU_SURE_ACTION", GC.getActionInfo(iAction).getTextKeyWide());
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szBuffer);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_YES").c_str(), NULL, 0);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_NO").c_str());
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}

bool CvDLLButtonPopup::launchConfirmTaskPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	int iAction = info.getData1();
	CvWString szBuffer;
	szBuffer = gDLL->getText("TXT_KEY_POPUP_ARE_YOU_SURE_ACTION", info.getText().GetCString());
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szBuffer);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_YES").c_str(), NULL, 0);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_NO").c_str());
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}


bool CvDLLButtonPopup::launchLoadUnitPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CLLNode<IDInfo>* pUnitNode;
	CvSelectionGroup* pSelectionGroup;
	CvUnit* pLoopUnit;
	CvPlot* pPlot;
	CvWStringBuffer szBuffer;
	int iCount;
	CvUnit* pFirstUnit = NULL;

	pSelectionGroup = gDLL->getInterfaceIFace()->getSelectionList();

	if (NULL == pSelectionGroup)
	{
		return (false);
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_CHOOSE_TRANSPORT"));

	pPlot = pSelectionGroup->plot();
	if (NULL == pPlot)
	{
		return (false);
	}

	iCount = 1;

	pUnitNode = pPlot->headUnitNode();

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);

		if (pSelectionGroup->canDoCommand(COMMAND_LOAD_UNIT, pLoopUnit->getOwnerINLINE(), pLoopUnit->getID()))
		{
			if (!pFirstUnit)
			{
				pFirstUnit = pLoopUnit;
			}
			szBuffer.clear();
			GAMETEXT.setUnitHelp(szBuffer, pLoopUnit, true);
			szBuffer.append(L", ");
			szBuffer.append(gDLL->getText("TXT_KEY_UNIT_HELP_CARGO_SPACE", pLoopUnit->getCargo(), pLoopUnit->cargoSpace()));
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, CvWString(szBuffer.getCString()), NULL, iCount, WIDGET_GENERAL);
			iCount++;
		}
	}

	if (iCount <= 2)
	{
		if (pFirstUnit)
		{
			GC.getGameINLINE().selectionListGameNetMessage(GAMEMESSAGE_DO_COMMAND, COMMAND_LOAD_UNIT, pFirstUnit->getOwnerINLINE(), pFirstUnit->getID());
		}
		return (false);
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, 0, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}


bool CvDLLButtonPopup::launchLoadCargoPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvWStringBuffer szBuffer;
	CvUnit* pFirstUnit = NULL;

	CvSelectionGroup* pSelectionGroup = gDLL->getInterfaceIFace()->getSelectionList();
	if (NULL == pSelectionGroup)
	{
		return false;
	}

	CvPlot* pPlot = pSelectionGroup->plot();
	if (NULL == pPlot)
	{
		return false;
	}

	CvCity* pCity = pPlot->getPlotCity();
	if (NULL == pCity)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_CHOOSE_GOODS"));
	int iCurrentYieldsAmount = 0, iMaxYieldsAmount = 0;
	for (int i = 0; i < pSelectionGroup->getNumUnits(); ++i)
	{
		CvUnit* pLoopUnit = pSelectionGroup->getUnitAt(i);
		if (pLoopUnit != NULL && pLoopUnit->isOnlyDefensive())
		{
			iMaxYieldsAmount += pLoopUnit->getUnitInfo().getCargoNewSpace();
			iCurrentYieldsAmount += pLoopUnit->getNewCargo();
		}
	}
	int iSpaceAvailable = iMaxYieldsAmount - iCurrentYieldsAmount;
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_LOAD_YIELD_INFO_UPGRADE", iCurrentYieldsAmount, iMaxYieldsAmount, iSpaceAvailable));

	CvWString szYieldAmountMax;
	CvWString szYieldStored;

	int iNumYields = 0;
	for (int iYield = 0; iYield <  NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		CvYieldInfo& kYield = GC.getYieldInfo(eYield);
		if (kYield.isCargo())
		{
			int iYieldStored = pCity->getYieldStored(eYield);
			if (iYieldStored > 0)
			{				
				++iNumYields;
				int iYieldAmountMax = std::min(iYieldStored, iSpaceAvailable);
				if (iYieldAmountMax < 1000)
				{
					szYieldAmountMax.Format(L"%d", iYieldAmountMax);
				}
				else
				{
					szYieldAmountMax.Format(L"%dK", iYieldAmountMax/1000);
				}

				if (iYieldStored < 1000)
				{
					szYieldStored.Format(L"%d", iYieldStored);
				}
				else
				{
					szYieldStored.Format(L"%dK", iYieldStored/1000);
				}

				CvWString szBuffer = gDLL->getText("TXT_KEY_POPUP_LOAD_YIELD_UPGRADE", szYieldAmountMax.GetCString(), szYieldStored.GetCString(), kYield.getChar());
				//gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer.GetCString(), kUnit.getButton(), iYield, WIDGET_LOAD_ALL_THE_YIELD_IN_GROUP, iYield, -1);
				gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", kYield.getButton(), -1, WIDGET_LOAD_ALL_THE_YIELD_IN_GROUP, iYield, -1);
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szBuffer);
				gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, iYield, L"", 0, 10, iYieldAmountMax, 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, iYield, WIDGET_GENERAL);
				gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
			}
		}
	}

	if (iNumYields == 0)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, -1, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}


bool CvDLLButtonPopup::launchLeadUnitPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pLeaderUnit = kPlayer.getUnit(info.getData1());
	if (pLeaderUnit == NULL)
	{
		return false;
	}

	CvSelectionGroup* pSelectionGroup = pLeaderUnit->getGroup();
	if (pSelectionGroup == NULL)
	{
		return false;
	}

	CvPlot* pPlot = pSelectionGroup->plot();
	if (pPlot == NULL)
	{
		return false;
	}
	PromotionTypes eLeaderPromotion = (PromotionTypes)pLeaderUnit->getUnitInfo().getLeaderPromotion();

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_CHOOSE_UNIT_TO_LEAD"));

	CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);
		if (pLoopUnit->canPromote(eLeaderPromotion, pLeaderUnit->getID()))
		{
			CvWStringBuffer szBuffer;
			GAMETEXT.setUnitHelp(szBuffer, pLoopUnit, true);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer.getCString(), NULL, pLoopUnit->getID(), WIDGET_GENERAL);
		}
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchMainMenuPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetStyle( pPopup, "Window_NoTitleBar_Style" );

	// 288,72
	gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "resource/temp/civ4_title_small.dds", 192, 48);

	gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_EXIT_TO_DESKTOP").c_str(), NULL, 0, WIDGET_GENERAL, 0, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);

	// commenting out since you can't exit to main menu and then restart a game
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_EXIT_TO_MAIN_MENU").c_str(), NULL, 1, WIDGET_GENERAL, 1, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);

	if (GC.getGameINLINE().canDoControl(CONTROL_RETIRE))
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_RETIRE").c_str(), NULL, 2, WIDGET_GENERAL, 2, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);
	}

	if ((GC.getGameINLINE().getElapsedGameTurns() == 0) && !(GC.getGameINLINE().isGameMultiPlayer()) && !(GC.getInitCore().getWBMapScript()))
	{
		// Don't allow if there has already been diplomacy
		bool bShow = true;
		for (int i = 0; bShow && i < MAX_TEAMS; i++)
		{
			for (int j = i+1; bShow && j < MAX_TEAMS; j++)
			{
				if (GET_TEAM((TeamTypes)i).isHasMet((TeamTypes)j))
				{
					bShow = false;
				}
			}
		}

		if (bShow)
		{
			if (!GC.getGameINLINE().getScriptData().empty())
			{
				bShow = false;
			}
		}

		if (bShow)
		{
			for (int i = 0; i < GC.getMapINLINE().numPlots(); ++i)
			{
				CvPlot* pPlot = GC.getMapINLINE().plotByIndexINLINE(i);
				if (!pPlot->getScriptData().empty())
				{
					bShow = false;
					break;
				}
			}
		}

		if (bShow)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_REGENERATE_MAP").c_str(), NULL, 3, WIDGET_GENERAL, 3, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);
		}
	}

	if (GC.getGameINLINE().canDoControl(CONTROL_LOAD_GAME))
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_LOAD_GAME").c_str(), NULL, 4, WIDGET_GENERAL, 4, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);
	}
	if (GC.getGameINLINE().canDoControl(CONTROL_SAVE_NORMAL))
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_SAVE_GAME").c_str(), NULL, 5, WIDGET_GENERAL, 5, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_OPTIONS").c_str(), NULL, 6, WIDGET_GENERAL, 6, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);

	if (GC.getGameINLINE().canDoControl(CONTROL_WORLD_BUILDER))
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ENTER_WB").c_str(), NULL, 7, WIDGET_GENERAL, 7, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);
	}

	if (GC.getGameINLINE().canDoControl(CONTROL_ADMIN_DETAILS))
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_GAME_DETAILS").c_str(), NULL, 8, WIDGET_GENERAL, 8, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);
	}

	if (GC.getGameINLINE().canDoControl(CONTROL_DETAILS))
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_DETAILS_TITLE").c_str(), NULL, 9, WIDGET_GENERAL, 9, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL").c_str(), NULL, 10, WIDGET_GENERAL, 10, 0, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}

bool CvDLLButtonPopup::launchConfirmMenu(CvPopup *pPopup, CvPopupInfo &info)
{
	if (info.getData1() == 2) //retire
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ARE_YOU_SURE_ACTION", GC.getControlInfo(CONTROL_RETIRE).getTextKeyWide()).c_str());
	}
	else
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ARE_YOU_SURE").c_str());
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_YES").c_str(), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_NO").c_str(), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchPythonScreen(CvPopup* pPopup, CvPopupInfo &info)
{
	// this is not really a popup, but a Python screen
	// we trick the app into thinking that it's a popup so that we can take advantage of the popup queuing system

	CyArgsList argsList;
	argsList.add(info.getData1());
	argsList.add(info.getData2());
	argsList.add(info.getData3());
	argsList.add(info.getOption1());
	argsList.add(info.getOption2());
	gDLL->getPythonIFace()->callFunction(PYScreensModule, CvString(info.getText()).GetCString(), argsList.makeFunctionArgs());

	return (false); // return false, so the Popup object is deleted, since it's just a dummy
}

bool CvDLLButtonPopup::launchMovie(CvPopup* pPopup, CvPopupInfo &info)
{
	// this is not really a popup, but a Python screen
	// we trick the app into thinking that it's a popup so that we can take advantage of the popup queuing system

	CyArgsList argsList;
	argsList.add(CvString(info.getText()));
	gDLL->getPythonIFace()->callFunction(PYScreensModule, "showVictoryMovie", argsList.makeFunctionArgs());

	return false; // return false, so the Popup object is deleted, since it's just a dummy
}

bool CvDLLButtonPopup::launchCancelDeal(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup,  gDLL->getText("TXT_KEY_POPUP_CANCEL_DEAL") );

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL_DEAL_YES"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL_DEAL_NO"), NULL, 1, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchPythonPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, info.getText());
	for (int i = 0; i < info.getNumPythonButtons(); i++)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, info.getPythonButtonText(i), info.getPythonButtonArt(i).IsEmpty() ? NULL : info.getPythonButtonArt(i).GetCString(), i);
	}

	gDLL->getInterfaceIFace()->popupSetPopupType(pPopup, POPUPEVENT_WARNING, ARTFILEMGR.getInterfaceArtInfo("INTERFACE_POPUPBUTTON_WARNING")->getPath());
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}

bool CvDLLButtonPopup::launchDetailsPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	if (!info.getOption1())
	{
		gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_DETAILS_TITLE"));

		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_MENU_LEADER_NAME"));
		gDLL->getInterfaceIFace()->popupCreateEditBox(pPopup, GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getName(), WIDGET_GENERAL, gDLL->getText("TXT_KEY_MENU_LEADER_NAME"), 0, POPUP_LAYOUT_STRETCH, 0, MAX_PLAYERINFO_CHAR_COUNT);
		gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_MENU_CIV_DESC"));
		gDLL->getInterfaceIFace()->popupCreateEditBox(pPopup, GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCivilizationDescription(), WIDGET_GENERAL, gDLL->getText("TXT_KEY_MENU_CIV_DESC"), 1, POPUP_LAYOUT_STRETCH, 0, MAX_PLAYERINFO_CHAR_COUNT);
		gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_MENU_CIV_SHORT_DESC"));
		gDLL->getInterfaceIFace()->popupCreateEditBox(pPopup, GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCivilizationShortDescription(), WIDGET_GENERAL, gDLL->getText("TXT_KEY_MENU_CIV_SHORT_DESC"), 2, POPUP_LAYOUT_STRETCH, 0, MAX_PLAYERINFO_CHAR_COUNT);
		gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_MENU_CIV_ADJ"));
		gDLL->getInterfaceIFace()->popupCreateEditBox(pPopup, GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getCivilizationAdjective(), WIDGET_GENERAL, gDLL->getText("TXT_KEY_MENU_CIV_ADJ"), 3, POPUP_LAYOUT_STRETCH, 0, MAX_PLAYERINFO_CHAR_COUNT);
		gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
	}
	else if (!GC.getInitCore().getCivPassword(GC.getInitCore().getActivePlayer()).empty())
	{
		// the purpose of the popup with the option set to true is to ask for the civ password if it's not set
		return false;
	}
	if (GC.getGameINLINE().isPbem() || GC.getGameINLINE().isHotSeat() || GC.getGameINLINE().isPitboss())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_PASSWORD"));
		gDLL->getInterfaceIFace()->popupCreateEditBox(pPopup, PASSWORD_DEFAULT, WIDGET_GENERAL, gDLL->getText("TXT_KEY_MAIN_MENU_PASSWORD"), 4, POPUP_LAYOUT_STRETCH, 0, MAX_PASSWORD_CHAR_COUNT);
		gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
	}
	if ( (GC.getGameINLINE().isPitboss() || GC.getGameINLINE().isPbem()) && !info.getOption1() )
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_DETAILS_EMAIL"));
		gDLL->getInterfaceIFace()->popupCreateEditBox(pPopup, CvWString(GC.getInitCore().getEmail(GC.getInitCore().getActivePlayer())), WIDGET_GENERAL, gDLL->getText("TXT_KEY_POPUP_DETAILS_EMAIL"), 5, POPUP_LAYOUT_STRETCH, 0, MAX_PLAYEREMAIL_CHAR_COUNT);
		gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
	}
	if (GC.getGameINLINE().isPbem() && !info.getOption1())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_DETAILS_SMTP"));
		gDLL->getInterfaceIFace()->popupCreateEditBox(pPopup, CvWString(GC.getInitCore().getSmtpHost(GC.getInitCore().getActivePlayer())), WIDGET_GENERAL, gDLL->getText("TXT_KEY_POPUP_DETAILS_SMTP"), 6, POPUP_LAYOUT_STRETCH, 0, MAX_PLAYEREMAIL_CHAR_COUNT);
		gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);

		if (GC.getGameINLINE().getPbemTurnSent())
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MISC_SEND"), NULL, 0, WIDGET_GENERAL);
		}
	}

	// Disable leader name edit box for internet games
	if (GC.getInitCore().getMultiplayer() && gDLL->isFMPMgrPublic())
	{
		gDLL->getInterfaceIFace()->popupEnableEditBox(pPopup, 0, false);
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);
	return (true);
}

bool CvDLLButtonPopup::launchAdminPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_GAME_DETAILS"));

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_GAME_NAME"));
	gDLL->getInterfaceIFace()->popupCreateEditBox(pPopup, GC.getInitCore().getGameName(), WIDGET_GENERAL, gDLL->getText("TXT_KEY_MAIN_MENU_GAME_NAME"), 0, POPUP_LAYOUT_STRETCH, 0, MAX_GAMENAME_CHAR_COUNT);
	gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ADMIN_PASSWORD"));
	gDLL->getInterfaceIFace()->popupCreateEditBox(pPopup, PASSWORD_DEFAULT, WIDGET_GENERAL, gDLL->getText("TXT_KEY_POPUP_ADMIN_PASSWORD"), 1, POPUP_LAYOUT_STRETCH, 0, MAX_PASSWORD_CHAR_COUNT);
	gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
	if (!GC.getGameINLINE().isGameMultiPlayer())
	{
		gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, 2);
		gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_ADMIN_ALLOW_CHEATS"), 2);
		gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, gDLL->getChtLvl() > 0, 2);
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);
	return (true);
}


bool CvDLLButtonPopup::launchAdminPasswordPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ADMIN_PASSWORD"));
	gDLL->getInterfaceIFace()->popupCreateEditBox(pPopup, L"", WIDGET_GENERAL, gDLL->getText("TXT_KEY_POPUP_ADMIN_PASSWORD"), 0, POPUP_LAYOUT_STRETCH, 0, MAX_PASSWORD_CHAR_COUNT);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);
	return (true);
}


bool CvDLLButtonPopup::launchExtendedGamePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_EXTENDED_GAME_TITLE"));

	if (GC.getGameINLINE().countHumanPlayersAlive() > 0)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EXTENDED_GAME_YES"), NULL, 0, WIDGET_GENERAL);
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EXTENDED_GAME_NO_MENU"), NULL, 1, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false);
	return (true);
}

bool CvDLLButtonPopup::launchDiplomacyPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_DIPLOMACY_TITLE"));

	int iCount = 0;

	for (int iI = 0; iI < MAX_PLAYERS; iI++)
	{
		PlayerTypes ePlayer = (PlayerTypes) iI;
		CvPlayer& kLoopPlayer = GET_PLAYER(ePlayer);

		if (kLoopPlayer.isAlive())
		{
			if (GET_PLAYER(GC.getGameINLINE().getActivePlayer()).canContact(ePlayer))
			{
				CvWString szTempBuffer = CvWString::format(SETCOLR L"%s" ENDCOLR, kLoopPlayer.getPlayerTextColorR(), kLoopPlayer.getPlayerTextColorG(), kLoopPlayer.getPlayerTextColorB(), kLoopPlayer.getPlayerTextColorA(), kLoopPlayer.getName());
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szTempBuffer, GC.getLeaderHeadInfo(kLoopPlayer.getLeaderType()).getButton(), iI, WIDGET_GENERAL);
				iCount++;
			}
		}
	}

	if (iCount == 0)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CANCEL")->getPath(), MAX_PLAYERS, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return true;
}


bool CvDLLButtonPopup::launchAddBuddyPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString( pPopup, gDLL->getText("TXT_KEY_SYSTEM_ADD_BUDDY", info.getText().GetCString()) );
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_YES"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_NO"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false);
	return (true);
}

bool CvDLLButtonPopup::launchForcedDisconnectPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString( pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_FORCED_DISCONNECT_INGAME") );
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true);
	return (true);
}

bool CvDLLButtonPopup::launchPitbossDisconnectPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString( pPopup, gDLL->getText("TXT_KEY_PITBOSS_DISCONNECT") );
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true);
	return (true);
}

bool CvDLLButtonPopup::launchKickedPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString( pPopup, gDLL->getText("TXT_KEY_POPUP_KICKED") );
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true);
	return (true);
}

bool CvDLLButtonPopup::launchEventPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kActivePlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	EventTriggeredData* pTriggeredData = kActivePlayer.getEventTriggered(info.getData1());
	if (NULL == pTriggeredData)
	{
		return false;
	}

	if (pTriggeredData->m_eTrigger == NO_EVENTTRIGGER)
	{
		return false;
	}

	CvEventTriggerInfo& kTrigger = GC.getEventTriggerInfo(pTriggeredData->m_eTrigger);

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, pTriggeredData->m_szText);

	bool bEventAvailable = false;
	for (int i = 0; i < kTrigger.getNumEvents(); i++)
	{
		if (GET_PLAYER(GC.getGameINLINE().getActivePlayer()).canDoEvent((EventTypes)kTrigger.getEvent(i), *pTriggeredData))
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, GC.getEventInfo((EventTypes)kTrigger.getEvent(i)).getDescription(), GC.getEventInfo((EventTypes)kTrigger.getEvent(i)).getButton(), kTrigger.getEvent(i), WIDGET_CHOOSE_EVENT, kTrigger.getEvent(i), info.getData1());
			bEventAvailable = true;
		}
		else
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, GC.getEventInfo((EventTypes)kTrigger.getEvent(i)).getDescription(), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_EVENT_UNAVAILABLE_BULLET")->getPath(), -1, WIDGET_CHOOSE_EVENT, kTrigger.getEvent(i), info.getData1(), false);
		}
	}

	if (!bEventAvailable)
	{
		return false;
	}

	if (kTrigger.isPickCity())
	{
		CvCity* pCity = kActivePlayer.getCity(pTriggeredData->m_iCityId);
		FAssert(NULL != pCity);
		if (NULL != pCity)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_EXAMINE_CITY").c_str(), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CITYSELECTION")->getPath(), GC.getNumEventInfos(), WIDGET_GENERAL, -1, -1);
		}
	}

	if (kTrigger.isShowPlot())
	{
		CvPlot* pPlot = GC.getMapINLINE().plot(pTriggeredData->m_iPlotX, pTriggeredData->m_iPlotY);
		if (NULL != pPlot)
		{
			gDLL->getEngineIFace()->addColoredPlot(pPlot->getX_INLINE(), pPlot->getY_INLINE(), GC.getColorInfo((ColorTypes)GC.getInfoTypeForString("COLOR_WARNING_TEXT")).getColor(), PLOT_STYLE_CIRCLE, PLOT_LANDSCAPE_LAYER_RECOMMENDED_PLOTS);
			gDLL->getInterfaceIFace()->lookAt(pPlot->getPoint(), CAMERALOOKAT_NORMAL);
		}
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, !bEventAvailable, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchFreeColonyPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	int iLoop;
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_FREE_COLONY"));

	for (CvCity* pLoopCity = GET_PLAYER(ePlayer).firstCity(&iLoop); pLoopCity != NULL; pLoopCity = GET_PLAYER(ePlayer).nextCity(&iLoop))
	{
		PlayerTypes ePlayer = pLoopCity->getLiberationPlayer(false);
		if (NO_PLAYER != ePlayer)
		{
			CvWString szCity = gDLL->getText("TXT_KEY_CITY_LIBERATE", pLoopCity->getNameKey(), GET_PLAYER(ePlayer).getNameKey());
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szCity, ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CITYSELECTION")->getPath(), pLoopCity->getID(), WIDGET_GENERAL);
		}
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CANCEL")->getPath(), -1, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchChooseProfessionPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	CvCity* pCity = GET_PLAYER(ePlayer).getCity(info.getData1());

	bool bEuropeUnit = false;
	CvUnit* pUnit = NULL;
	if (pCity != NULL)
	{
		pUnit = pCity->getPopulationUnitById(info.getData2());
	}
	else
	{
		pUnit = GET_PLAYER(ePlayer).getEuropeUnitById(info.getData2());
		bEuropeUnit = (pUnit != NULL);
	}
	if (NULL == pUnit)
	{
		pUnit = GET_PLAYER(ePlayer).getUnit(info.getData2());
	}
	if (NULL == pUnit)
	{
		return false;
	}

	bool bShowOnlyNonCitizens = (info.getData3() == 0);
	bool bShowOnlyPlotCitizens = (info.getData3() == 1);

	CvPlot* pWorkingPlot = NULL;
	if (pCity != NULL)
	{
		pWorkingPlot = pCity->getPlotWorkedByUnit(pUnit);
	}

	ProfessionTypes eProfession = pUnit->getProfession();
	FAssert(NULL == pWorkingPlot || (NO_PROFESSION != eProfession && GC.getProfessionInfo(eProfession).isWorkPlot()));

	CvWString szProfession;
	if (NO_PROFESSION == eProfession)
	{
		szProfession = L"TXT_KEY_PROFESSION_IDLE_CITIZEN";
	}
	else
	{
		szProfession = GC.getProfessionInfo(eProfession).getTextKeyWide();
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_CHOOSE_PROFESSION", pUnit->getNameKey(), szProfession.GetCString()));

	if (!bShowOnlyPlotCitizens)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CANCEL")->getPath(), GC.getNumProfessionInfos(), WIDGET_GENERAL);

		if (pUnit->isColonistLocked() && !bShowOnlyNonCitizens)
		{
			FAssert(pCity != NULL);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_AUTOMATE_CITIZEN"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_CITY_AUTOMATE_CITIZENS")->getPath(), -1, WIDGET_GENERAL);
		}
	}

	int iNumButtons = 0;
	for (int iProfession = 0; iProfession < GC.getNumProfessionInfos(); ++iProfession)
	{
		ProfessionTypes eLoopProfession = (ProfessionTypes) iProfession;
		CvProfessionInfo& kProfession = GC.getProfessionInfo(eLoopProfession);

		if ((iProfession != pUnit->getProfession() || bShowOnlyPlotCitizens) && pUnit->canHaveProfession(eLoopProfession, false))
		{
			//inside or outside city
			if (kProfession.isCitizen() && pCity != NULL)
			{
				if (!bShowOnlyNonCitizens)
				{
					YieldTypes eProfessionYield = (YieldTypes)kProfession.getYieldProduced();
					if (NO_YIELD != eProfessionYield)
					{
						CvWString szText;
						int iProfessionYieldChar = GC.getYieldInfo(eProfessionYield).getChar();
						int iBestYieldAmount = pCity->getBestYieldAmountAvailable(eLoopProfession, pUnit);
						if (kProfession.isWorkPlot() && NULL != pWorkingPlot)
						{
							int iYieldAmount = pWorkingPlot->calculatePotentialProfessionYieldAmount(eLoopProfession, pUnit, false);
							if (iYieldAmount > 0)
							{
								++iNumButtons;
								if (bShowOnlyPlotCitizens)
								{
									szText = gDLL->getText("TXT_KEY_CHOOSE_PROFESSION_ITEM", kProfession.getTextKeyWide(), iYieldAmount, iProfessionYieldChar);
								}
								else
								{
									szText = gDLL->getText("TXT_KEY_CHOOSE_PROFESSION_ITEM_MAX", kProfession.getTextKeyWide(), iYieldAmount, iBestYieldAmount, iProfessionYieldChar);
								}
								gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szText, kProfession.getButton(), iProfession, WIDGET_GENERAL);
							}
						}
						else if (!bShowOnlyPlotCitizens)
						{
							if (iBestYieldAmount > 0)
							{
								++iNumButtons;
								szText = gDLL->getText("TXT_KEY_CHOOSE_PROFESSION_ITEM", kProfession.getTextKeyWide(), iBestYieldAmount, iProfessionYieldChar);
								gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szText, kProfession.getButton(), iProfession, WIDGET_GENERAL);
							}
						}
					}
				}
			}
			else
			{
				if (!bShowOnlyPlotCitizens)
				{
					CvWString szText = kProfession.getDescription();
					if (!bEuropeUnit)
					{
						szText += gDLL->getText("TXT_KEY_PROFESSION_NON_CITIZEN");
					}

					++iNumButtons;
					gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szText, kProfession.getButton(), iProfession, WIDGET_GENERAL);
				}
			}
		}
	}

	if (bShowOnlyPlotCitizens)
	{
		if (iNumButtons <= 1)
		{
			return false;
		}
	}

	if (pUnit->canClearSpecialty() && !bShowOnlyPlotCitizens && !bShowOnlyNonCitizens)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, GC.getCommandInfo(COMMAND_CLEAR_SPECIALTY).getDescription(), GC.getCommandInfo(COMMAND_CLEAR_SPECIALTY).getButton(), -2, WIDGET_GENERAL);
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}
bool CvDLLButtonPopup::launchManageHospitals(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvCity* pCity = kPlayer.getCity(info.getData1());
	if (pCity == NULL)
	{
		return false;
	}
	CvHospitals* pHospitals = kPlayer.getHospitals();
	if (pHospitals == NULL)
	{
		return false;
	}
	int iNumHospitals = pHospitals->getTotalNumber();
	int iHealRate =	pHospitals->getHealRate();
	int iNetCloth = pHospitals->calculateClothNetYield();

	int iStoredCloth = pCity->getYieldStored(YIELD_CLOTH);
	int iNetFood = pCity->calculateNetYield(YIELD_FOOD);


	int iBirthRate = pHospitals->getBirthRate(iNetFood);
	int iNetClothHospitals = pHospitals->getNetClothYield(pCity->isAutoHospital(), iStoredCloth, iNetCloth);
	int iSurplus = iNetCloth - (iNumHospitals)*pHospitals->getConsumption();
	int iDifStock = pHospitals->getMaxStock() - pHospitals->getStock();
	CvWString szBuffer;
	szBuffer = gDLL->getText("TXT_KEY_POPUP_HOSPITAL", info.getText().GetCString());

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HOSPITAL"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szBuffer);
	if (iBirthRate > -1)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_HEAL", iHealRate, iBirthRate));
	}
	else
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_HEAL_STABLE", iHealRate));
	}
	if (iNumHospitals > 1)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_NUM_TOTAL", iNumHospitals, (iNumHospitals)*pHospitals->getConsumption()));
	}
	else
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_NUM_TOTAL1", pHospitals->getConsumption()));
	}
	if (iNetClothHospitals > 0)
	{
		if (iDifStock == 0)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_MAXIMAL"));
		}
		else
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_VIDAGE", iNetClothHospitals));
		}
	}
	else
	{
		if (iNetClothHospitals == 0)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_EGALE"));
		}
		else
		{
			if (iDifStock == 0)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_MAXIMAL"));
			}
			else
			{
				if ( iDifStock <= iNetClothHospitals)
				{
					gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_HAUSSE", iDifStock));
				}
				else
				{
					gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_HAUSSE", iNetClothHospitals));
				}
			}
		}
	}
	gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_STOCK", pHospitals->getStock(), pHospitals->getMaxStock()));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_CONSOM", pHospitals->getConsumption()));
	gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
	gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_VARIA"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_VARIA"));
	gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
	gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
	if ( iDifStock < iStoredCloth)
	{
		gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 1, L"", 0, 5, iDifStock, -pHospitals->getStock());
	}
	else
	{
		gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 1, L"", 0, 5, iStoredCloth, -pHospitals->getStock());
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup,"" );
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 2, L"", 0, 1, pHospitals->getMaxConsumption() - pHospitals->getConsumption(), -pHospitals->getConsumption());
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 2, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
	gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, 1, WIDGET_GENERAL, POPUP_LAYOUT_TOP);
	gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_HOSPITAL_AUTO"),1);
	gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, pCity->isAutoHospital(), 1);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_CLOSED"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchResupplyBuilderPackPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	if (pUnit == NULL)
	{
		return false;
	}

	bool isInEurope = (pUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);
	CvCity* pCity = pUnit->getCity();
	if (pCity == NULL && !isInEurope)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_NAME_RESUPPLY_BUILDER_PACK_HEADER"));

	//Get the builder wagon cost
	unsigned char iModifier = GC.getBUILDER_PACK_YIELD_COST_PERCENT();
	short iToolCost = GC.getBuilderPackToolCost();
	short iLumberCost = GC.getBuilderPackLumberCost();

	//Apply Cost Modifier
	iToolCost *= iModifier;
	iToolCost /= 100;
	iLumberCost *= iModifier;
	iLumberCost /= 100;

	if (isInEurope)
	{
		int iToolCostPrice = kPlayer.getBuyPriceForYield(YIELD_TOOLS, iToolCost);
		int iLumberCostPrice = kPlayer.getBuyPriceForYield(YIELD_LUMBER, iLumberCost);

		bool hasEnoughYieldsToWarehouse = kPlayer.getEuropeWarehouseYield(YIELD_TOOLS) >= iToolCost && kPlayer.getEuropeWarehouseYield(YIELD_LUMBER) >= iLumberCost;
		int iBuilderBackCost = iToolCostPrice + iLumberCostPrice;
		bool hasEnoughGold = kPlayer.getGold() >= iBuilderBackCost;
		if (hasEnoughYieldsToWarehouse)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NAME_RESUPPLY_BUILDER_PACK_EUROPE_ENOUGH_RESOURCES"));
		}
		else
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText(hasEnoughGold ? "TXT_KEY_NAME_RESUPPLY_BUILDER_PACK_EUROPE" : "TXT_KEY_NAME_RESUPPLY_BUILDER_PACK_EUROPE_NOT_ENOUGH_MONEY", iBuilderBackCost));
		}
		if (hasEnoughYieldsToWarehouse || hasEnoughGold)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1);
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL"), NULL, 0);

		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
		return true;
	}

	//Get city yields:
	short iTools = pCity->getYieldStored(YIELD_TOOLS);
	short iLumber = pCity->getYieldStored(YIELD_LUMBER);

	//Builder pack allowed
	if (info.getData2())
	{
		//Pass the available yields and costs in the texts
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NAME_RESUPPLY_BUILDER_PACK", iToolCost, iTools, iLumberCost, iLumber));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1);
	}  else {//Builder pack not allowed
		//Pass the costs in the text
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NAME_RESUPPLY_BUILDER_PACK_PB", iToolCost, iTools, iLumberCost, iLumber));
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL"), NULL, 0);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return true;
}

bool CvDLLButtonPopup::launchUnloadNewCargoPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	if (pUnit == NULL)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_NAME_UNLOAD_NEW_CARGO_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NAME_UNLOAD_NEW_CARGO"));
	int iRessource = 0;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		CvYieldInfo& kYield = GC.getYieldInfo(eYield);
		int iNewCargo = pUnit->getNewCargoYield(eYield);
		if (iNewCargo > 0)
		{
			gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", kYield.getButton(), -2, WIDGET_UNLOAD_YIELD, iYield, info.getData1());
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NEW_CARGO_CORPS"));
			gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, iYield, L"", iNewCargo, 5, iNewCargo, 0);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, iYield, WIDGET_GENERAL);
			gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
			iRessource++;
		}
	}
	if (iRessource==0)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_COMMAND_UNLOAD_ALL"), NULL, NUM_YIELD_TYPES);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, -1);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return true;
}
bool CvDLLButtonPopup::launchChooseQuantityTradePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	if (pUnit == NULL)
	{
		return false;
	}
	bool bUnit = info.getData3() == 1;
	YieldTypes eYield = (YieldTypes)info.getData2();
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_CHOOSE_QUANTITY_TRADE_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_SELECT_YIELD_POPUP", GC.getYieldInfo(eYield).getTextKeyWide()));

	int iNewCargo;
	if (bUnit)
	{
		iNewCargo = pUnit->getNewCargoYield(eYield);
	}
	else
	{
		CvPlot* pPlot = pUnit->plot();
		CvCity* pCity = pPlot->getPlotCity();
		iNewCargo = pCity->getYieldStored(eYield);
	}
	if (iNewCargo == 0)
	{
		return false;
	}
	int iStart = bUnit ? kPlayer.getHumanTradeProposition(eYield) : kPlayer.getIATradeProposition(eYield);
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 1, L"", iStart, 1, iNewCargo, 0);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL"), NULL, 0);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return true;
}

bool CvDLLButtonPopup::launchChooseProfessionInEuropePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getEuropeUnitById(info.getData2());
	if (pUnit == NULL)
	{
		return false;
	}
	ProfessionTypes eProfession = pUnit->getProfession();

	CvWString szProfession;
	if (eProfession == NO_PROFESSION)
	{
		return false;
	}

	szProfession = GC.getProfessionInfo(eProfession).getTextKeyWide();

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_CHOOSE_PROFESSION", pUnit->getNameKey(), szProfession.GetCString()));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CANCEL")->getPath(), GC.getNumProfessionInfos(), WIDGET_GENERAL);
	int iMaxMunititon = GC.getProfessionInfo(eProfession).getMaxMunition();

	if (iMaxMunititon != -1)
	{
		if (pUnit->getMunition() < iMaxMunititon)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_COMMAND_RESUPPLY"), GC.getCommandInfo(COMMAND_RESUPPLY).getButton(), -2, WIDGET_GENERAL);
		}
	}

	int iNumButtons = 0;
	for (int iProfession = 0; iProfession < GC.getNumProfessionInfos(); ++iProfession)
	{
		ProfessionTypes eLoopProfession = (ProfessionTypes) iProfession;
		CvProfessionInfo& kProfession = GC.getProfessionInfo(eLoopProfession);

		if ((iProfession != pUnit->getProfession()) && pUnit->canHaveProfessionInEurope(eLoopProfession))
		{
			CvWString szText = kProfession.getDescription();
			iNumButtons++;
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szText, kProfession.getButton(), iProfession, WIDGET_GENERAL);
		}
	}
	if (iNumButtons < 1)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchChooseCityDestinationInEuropePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getEuropeUnitById(info.getData1());
	if (pUnit == NULL)
	{
		return false;
	}

	if (kPlayer.getNumCities() == 0)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_CHOOSE_CITY_DESTINATION_IN_EUROPE_POPUP_BODY", pUnit->getNameKey()));

	CvWString szBuffer;

	int iLoop;
	for (CvCity* pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
	{
		if (pLoopCity->isCoastal(GC.getMIN_WATER_SIZE_FOR_OCEAN()))
		{
			int iCityId = -1;
			bool hasAutomaticTransportToEurope = pLoopCity->hasAutomaticTransportToEurope();
			if (hasAutomaticTransportToEurope)
			{
				szBuffer.Format(SETCOLR L"%s " ENDCOLR, TEXT_COLOR("COLOR_GREEN"), pLoopCity->getName().GetCString());
				iCityId = pLoopCity->getID();
				if (pUnit->getDestinationCity() == pLoopCity->getID())
				{
					szBuffer.append(gDLL->getText("TXT_KEY_HELP_DESTINATION_CITY_SELECTED"));
				}
			}
			else
			{
				szBuffer.Format(SETCOLR L"%s" ENDCOLR, TEXT_COLOR("COLOR_RED"), pLoopCity->getName().GetCString());
			}

			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, ARTFILEMGR.getInterfaceArtInfo("INTERFACE_VISUEL_TOPO_CONTRAT_EUROPEEN")->getPath(), iCityId, WIDGET_HELP_DESTINATION_CITY, pLoopCity->getID(), hasAutomaticTransportToEurope);
		}
	}

	if (pUnit->getDestinationCity() >= 0)
	{
		CvCity* pCity = kPlayer.getCity(pUnit->getDestinationCity());
		if (pCity != NULL)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_HELP_DESTINATION_CITY_AUTOMATIC_TRANSPORT_REMOVE", pCity->getName().GetCString()), NULL, -2);
		}
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL"), NULL, -1);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}


bool CvDLLButtonPopup::launchValidateProfessionInEuropePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvPlayer& kEuropePlayer = GET_PLAYER(kPlayer.getParent());
	CvUnit* pUnit = kPlayer.getEuropeUnitById(info.getData1());
	ProfessionTypes eNewProfession = (ProfessionTypes)info.getData2();
	ProfessionTypes eOldProfession = pUnit->getProfession();

	if (eNewProfession == NO_PROFESSION || eOldProfession == NO_PROFESSION)
	{
		return false;
	}

	CvProfessionInfo& kNewProfession = GC.getProfessionInfo(eNewProfession);
	CvProfessionInfo& kOldProfession = GC.getProfessionInfo(eOldProfession);
	bool bHasAllEquipments = true;
	bool bProfessionRequiresEquipments = false;
	bool bHasSomeEquipments = false;
	bool bNotEnoughYieldsForMissingEquipments = false;
	bool bNotEnoughYieldsForEquipments = false;

	int iGold = kPlayer.getGold();
	int iTotalBuyPrice = 0;
	int iRequiresEquipmentsBuyPrice = 0;

	CvWString szRequiresEquipment;
	CvWString szRequiresMissingEquipment;

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;		
		int iNewProfessionYieldAmount = kNewProfession.getYieldEquipmentAmount(eYield);
		int iOldProfessionYieldAmount = kOldProfession.getYieldEquipmentAmount(eYield);

		if (iNewProfessionYieldAmount > iOldProfessionYieldAmount)
		{ // => iNewProfessionYieldAmount > 0
			int iEuropeStock = kPlayer.getEuropeWarehouseYield(eYield);
			int iEuropeMarketStock = kEuropePlayer.getEuropeWarehouseYield(eYield);
			int iYieldNeeded = iNewProfessionYieldAmount - iOldProfessionYieldAmount;

			if (iEuropeStock < iYieldNeeded)
			{
				bHasAllEquipments = false;
				int iMissingYield = iYieldNeeded - iEuropeStock;
				iRequiresEquipmentsBuyPrice += kPlayer.getBuyPriceForYield(eYield, iMissingYield);
				if (!szRequiresMissingEquipment.empty())
				{
					szRequiresMissingEquipment += L", ";
				}
				szRequiresMissingEquipment += CvWString::format(L"%d%c", iMissingYield, GC.getYieldInfo(eYield).getChar());
				if (iEuropeMarketStock < iMissingYield)
				{
					bNotEnoughYieldsForMissingEquipments = true;
				}
			}
			if (iEuropeStock > 0)
			{
				bHasSomeEquipments = true;
			}
			if (!szRequiresEquipment.empty())
			{
				szRequiresEquipment += L", ";
			}
			if (iEuropeMarketStock < iYieldNeeded)
			{
				bNotEnoughYieldsForEquipments = true;
			}
			szRequiresEquipment += CvWString::format(L"%d%c", iYieldNeeded, GC.getYieldInfo(eYield).getChar());
			iTotalBuyPrice += kPlayer.getBuyPriceForYield(eYield, iYieldNeeded);
			bProfessionRequiresEquipments = true;
		}
	}

	if (!bProfessionRequiresEquipments)
	{
		gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_EUROPE_CHANGE_PROFESSION, pUnit->getID(), eNewProfession, -1);
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_HEADER"));
	if (bHasAllEquipments)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_ALL_EQUIPMENTS", szRequiresEquipment.GetCString()));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_USE_STOCK_ACTION"), NULL, 1);
	}

	if (bHasSomeEquipments && bNotEnoughYieldsForMissingEquipments)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_BUY_MISSING_EQUIPMENTS_NO_STOCK", szRequiresEquipment.GetCString(), szRequiresMissingEquipment.GetCString()));
	}
	else
	{
		if (bHasSomeEquipments && iRequiresEquipmentsBuyPrice > iGold)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_BUY_MISSING_EQUIPMENTS_NO_GOLD", szRequiresEquipment.GetCString(), szRequiresMissingEquipment.GetCString(), iRequiresEquipmentsBuyPrice));
		}
		else
		{
			if (bHasSomeEquipments && !bHasAllEquipments)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_BUY_MISSING_EQUIPMENTS", szRequiresEquipment.GetCString(), szRequiresMissingEquipment.GetCString(), iRequiresEquipmentsBuyPrice));
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_BUY_MISSING_EQUIPMENTS_ACTION"), NULL, 2);
			}
			if (bNotEnoughYieldsForEquipments)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_BUY_EQUIPMENTS_NO_STOCK", szRequiresEquipment.GetCString()));
			}
			else if (iTotalBuyPrice > iGold)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_BUY_EQUIPMENTS_NO_GOLD", szRequiresEquipment.GetCString(), iTotalBuyPrice));
			}
			else
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_BUY_EQUIPMENTS", szRequiresEquipment.GetCString(), iTotalBuyPrice));
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_VALIDATE_PROFESSION_IN_EUROPE_BUY_DIRECTLY_EQUIPMENTS_ACTION"), NULL, 3);
			}
		}
	}


	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL"), NULL, -1);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return true;
}
bool CvDLLButtonPopup::launchEjectUnitToPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());

	if (pUnit == NULL)
	{
		return false;
	}

	CvSelectionGroup* pGroup = pUnit->getGroup();
	if (pGroup == NULL || pGroup->getNumUnits() <= 1)
	{
		return false;
	}
	if (pGroup->getNumUnits() == 2 && info.getData2() < 0)
	{
		gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_EJECT_UNIT_TO_GROUP, pUnit->getID(), pUnit->getID(), -1);
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_EJECT_UNIT_TO_GROUP_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_EJECT_UNIT_TO_GROUP_BODY"));

	CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pGroup->nextUnitNode(pUnitNode);
		if (pLoopUnit != NULL)
		{
			CvWStringBuffer szBuffer;
			GAMETEXT.setUnitHelp(szBuffer, pLoopUnit, true);
			gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, pLoopUnit->getID(), WIDGET_GENERAL, POPUP_LAYOUT_LEFT);
			gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, szBuffer.getCString(), pLoopUnit->getID());
		}
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_CREATE_GROUP"), NULL, 1);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL"), NULL, -1);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchAskSailorFormationPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvTempUnit* pTempUnit = kPlayer.getTempUnit(info.getData1());

	if (pTempUnit == NULL)
	{
		return false;
	}

	int iMerchantSailorCost = GC.getDefineINT("MERCHANT_CREW_FORMATION_PRICE");
	int iMerchantSailorTurn = GC.getDefineINT("MERCHANT_CREW_FORMATION_TURN");
	int iMilitarySailorCost = GC.getDefineINT("MILITARY_CREW_FORMATION_PRICE");
	int iMilitarySailorTurn = GC.getDefineINT("MILITARY_CREW_FORMATION_TURN");

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_NAME_ASK_SAILOR_FORMATION_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NAME_ASK_SAILOR_FORMATION", iMerchantSailorCost, iMilitarySailorCost));

	if (iMerchantSailorCost > kPlayer.getGold())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NAME_ASK_SAILOR_FORMATION_PB1"));
	}
	else if (iMilitarySailorCost > kPlayer.getGold())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NAME_ASK_SAILOR_FORMATION_PB2"));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NAME_ASK_SAILOR_FORMATION_1", iMerchantSailorTurn), NULL, 1);
	}
	else
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NAME_ASK_SAILOR_FORMATION_1", iMerchantSailorTurn), NULL, 1);
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NAME_ASK_SAILOR_FORMATION_2", iMilitarySailorTurn), NULL, 2);
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL"), NULL, 0);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchDestroyCityPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_SCREEN_HEADER_ABANDON_COLONY"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_SCREEN_ABANDON_COLONY"));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_ABANDON_COLONY_OK"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_ABANDON_COLONY_OUPS"), NULL, 1, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return (true);
}

bool CvDLLButtonPopup::launchRemoveRessourcePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_SCREEN_HEADER_REMOVE_RESSOURCE"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_SCREEN_REMOVE_RESSOURCE"));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_CANCEL"), NULL, 1, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return (true);
}

bool CvDLLButtonPopup::launchRepairShipPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());

	int iCost = pUnit->getReparationCostInEurope(info.getData2());
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_SCREEN_HEADER_REPAIR_SHIP"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_SCREEN_REPAIR_SHIP", iCost));
	if (kPlayer.getGold() >= iCost)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1, WIDGET_GENERAL);
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_CANCEL"), NULL, 0, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return (true);
}

bool CvDLLButtonPopup::launchArsenalManagementPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER) 
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvCity* pCity = kPlayer.getCity(info.getData1());
	if (pCity == NULL)
	{
		return false;
	}

	int iBuilding = 1;

	for (int iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
	{
		BuildingTypes eBuilding = ((BuildingTypes)(GC.getCivilizationInfo(pCity->getCivilizationType()).getCivilizationBuildings(iI)));
		if (eBuilding != NO_BUILDING)
		{
			if (GC.getBuildingInfo(eBuilding).getRefBuilding() == 1)
			{
				if (!pCity->isHasRealBuilding(eBuilding))
				{
					return false;
				}
			}
			if (GC.getBuildingInfo(eBuilding).getRefBuilding() == 2)
			{
				if (pCity->isHasRealBuilding(eBuilding))
				{
					iBuilding = 2;
				}
			}
			if (GC.getBuildingInfo(eBuilding).getRefBuilding() == 3)
			{
				if (pCity->isHasRealBuilding(eBuilding))
				{
					iBuilding = 3;
				}
			}
		}
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_SCREEN_HEADER_ARSENAL_MANAGEMENT"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_SCREEN_ARSENAL_MANAGEMENT_FISRT_TIME"));

	YieldTypes eCurrentYield = YIELD_SWORDS;
	CvYieldInfo& kSwordInfo = GC.getYieldInfo(eCurrentYield);
	bool isProducingCurrentYield = pCity->getYieldProductionPercent(eCurrentYield) > 0;
	bool bCanProducedCurrentYield = true;
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, getArsenalYieldText(eCurrentYield, bCanProducedCurrentYield, isProducingCurrentYield), kSwordInfo.getButton(), eCurrentYield, WIDGET_HELP_YIELD, eCurrentYield);

	eCurrentYield = YIELD_MUSKETS;
	CvYieldInfo& kMusketInfo = GC.getYieldInfo(eCurrentYield);
	isProducingCurrentYield = pCity->getYieldProductionPercent(eCurrentYield) > 0;
	bCanProducedCurrentYield = iBuilding >= 2;
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, getArsenalYieldText(eCurrentYield, bCanProducedCurrentYield, isProducingCurrentYield), kMusketInfo.getButton(), eCurrentYield, WIDGET_HELP_YIELD, eCurrentYield);

	eCurrentYield = YIELD_AMMUNITION;
	CvYieldInfo& kAmmunitionInfo = GC.getYieldInfo(eCurrentYield);
	isProducingCurrentYield = pCity->getYieldProductionPercent(eCurrentYield) > 0;
	bCanProducedCurrentYield = iBuilding >= 2;
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, getArsenalYieldText(eCurrentYield, bCanProducedCurrentYield, isProducingCurrentYield), kAmmunitionInfo.getButton(), eCurrentYield, WIDGET_HELP_YIELD, eCurrentYield);

	eCurrentYield = YIELD_CANNON;
	CvYieldInfo& kCannonInfo = GC.getYieldInfo(eCurrentYield);
	isProducingCurrentYield = pCity->getYieldProductionPercent(eCurrentYield) > 0;
	bCanProducedCurrentYield = iBuilding == 3;
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, getArsenalYieldText(eCurrentYield, bCanProducedCurrentYield, isProducingCurrentYield), kCannonInfo.getButton(), eCurrentYield, WIDGET_HELP_YIELD, eCurrentYield);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, -1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}


CvWString CvDLLButtonPopup::getArsenalYieldText(YieldTypes eYield, bool bCanbeProduced, bool bIsActive)
{
	CvYieldInfo& kYieldInfo = GC.getYieldInfo(eYield);
	if (!bCanbeProduced) 
	{
		return gDLL->getText("TXT_KEY_ARSENAL_NO_CANNON");
	}

	if (bIsActive)
	{
		return gDLL->getText("TXT_KEY_ARSENAL_ACTIVE_YIELD", kYieldInfo.getDescription());
	}

	return gDLL->getText("TXT_KEY_ARSENAL_NON_ACTIVE_YIELD", kYieldInfo.getDescription());
}

bool CvDLLButtonPopup::launchLeadUnitExpPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER) 
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pLeaderUnit = kPlayer.getUnit(info.getData1());
	if (pLeaderUnit == NULL) 
	{
		return false;
	}

	CvSelectionGroup* pSelectionGroup = pLeaderUnit->getGroup();
	if (pSelectionGroup == NULL) 
	{
		return false;
	}

	CvPlot* pPlot = pSelectionGroup->plot();
	if (pPlot == NULL) 
	{
		return false;
	}

	int iRemainingPoints = info.getData2();
	PromotionTypes eLeaderPromotion = (PromotionTypes)pLeaderUnit->getUnitInfo().getLeaderPromotion();

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_POINT_EXP"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_POINT_EXP_RESTANT", iRemainingPoints));

	gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_POINT_EXP_GIVE"));
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 1, L"", 0, 5, iRemainingPoints, 0);
	gDLL->getInterfaceIFace()->popupEndLayout(pPopup);

	int iCount = 0;
	CvUnit* pLastUnit = NULL;
	CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
	while (pUnitNode != NULL) 
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);
		if (pLoopUnit->canPromote(eLeaderPromotion, pLeaderUnit->getID())) 
		{
			pLastUnit = pLoopUnit;
			CvWStringBuffer szBuffer;
			GAMETEXT.setUnitHelp(szBuffer, pLoopUnit, true);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer.getCString(), NULL, pLoopUnit->getID(), WIDGET_GENERAL);
			iCount++;
		}
	}

	int iMaxDistributedPoints = GC.getDefineINT("GREAT_GENERALS_DISTRIBUTED_POINTS");
	if (iCount < 2) 
	{
		if (iCount == 1 && pLastUnit != NULL)
		{
			iMaxDistributedPoints *= 10;
			gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_EXP_LEAD_UNIT, pLastUnit->getID(), pLastUnit->getExperience() + iMaxDistributedPoints, iMaxDistributedPoints);
			gDLL->sendPushMission(pLeaderUnit->getID(), MISSION_LEAD, pLastUnit->getID(), -1, 0, false);
		}
		return false;
	}

	if (iRemainingPoints == iMaxDistributedPoints) 
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, -1, WIDGET_GENERAL);
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchPurchaseEuropeUnitPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_PURCHASE_EUROPE_UNIT"));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CANCEL")->getPath(), GC.getNumUnitInfos(), WIDGET_GENERAL);

	bool bFound = false;
	for (int iUnitClass = 0; iUnitClass < GC.getNumUnitClassInfos(); ++iUnitClass)
	{
		UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(kPlayer.getCivilizationType()).getCivilizationUnits(iUnitClass);

		if (NO_UNIT != eUnit)
		{
			int iCost = kPlayer.getEuropeUnitBuyPrice(eUnit);
			if (iCost >= 0)
			{
				CvWString szText = gDLL->getText("TXT_KEY_EUROPE_UNIT_BUY_PRICE", GC.getUnitInfo(eUnit).getTextKeyWide(), iCost);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szText, kPlayer.getUnitButton(eUnit), eUnit, WIDGET_PEDIA_JUMP_TO_UNIT, eUnit, 1);
				bFound = true;
			}
		}
	}

	if (!bFound)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchCivicOptionPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CivicOptionTypes eCivicOption = (CivicOptionTypes) info.getData1();

	if (eCivicOption == NO_CIVICOPTION)
	{
		return false;
	}

	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_CONSTITUTION_POPUP", GC.getCivicOptionInfo(eCivicOption).getTextKeyWide()));

	bool bFoundValid = false;
	for (int iCivic = 0; iCivic < GC.getNumCivicInfos(); ++iCivic)
	{
		CvCivicInfo& kCivicInfo = GC.getCivicInfo((CivicTypes) iCivic);
		if (kCivicInfo.getCivicOptionType() == eCivicOption)
		{
			if (GET_PLAYER(ePlayer).canDoCivics((CivicTypes) iCivic))
			{
				bFoundValid = true;
				CvWStringBuffer szBuffer;
				GAMETEXT.parseCivicInfo(szBuffer, (CivicTypes) iCivic, false, true, false);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer.getCString(), kCivicInfo.getButton(), iCivic, WIDGET_GENERAL);
			}
		}
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_FEAT_ACCOMPLISHED_MORE"), "", -1, WIDGET_GENERAL);

	if (!bFoundValid)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchYieldProductionPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	int iYieldProduction;
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvCity* pCity = kPlayer.getCity(info.getData1());
	if (pCity == NULL)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_YIELD_PRODUCTION_PERCENT_POPUP", pCity->getNameKey()));

	bool bProductionPercent = false;//We want to know the real production without the production percent fixed by user
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		CvYieldInfo& kYield = GC.getYieldInfo(eYield);
		if (kYield.isEditProduction() && pCity->getBaseRawYieldProduced(eYield, bProductionPercent) > 0)
		{
			iYieldProduction = pCity->getYieldProductionPercent(eYield);
			gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", kYield.getButton(), -1, WIDGET_HELP_YIELD, iYield);
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, kYield.getDescription());
			gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, iYield, L"", iYieldProduction, 5, 100, 0);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, iYield, WIDGET_GENERAL);
			gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
		}
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);

	return true;
}
bool CvDLLButtonPopup::launchPromotionPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvSelectionGroup* pSelectionGroup = kPlayer.getSelectionGroup(info.getData1());
	if (pSelectionGroup == NULL)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_CHOOSE_PROMOTION"));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CANCEL")->getPath(), GC.getNumPromotionInfos(), WIDGET_GENERAL);

	int iNumPromotions = 0;
	for (int iPromotion = 0; iPromotion < GC.getNumPromotionInfos(); iPromotion++)
	{
		if (iPromotion != info.getData2())
		{
			CLLNode<IDInfo>* pSelectedUnitNode = pSelectionGroup->headUnitNode();

			while (pSelectedUnitNode != NULL)
			{
				CvUnit* pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);
				pSelectedUnitNode = pSelectionGroup->nextUnitNode(pSelectedUnitNode);
				if (pSelectedUnit->canPromote((PromotionTypes) iPromotion, info.getData2()))
				{
					gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText(GC.getPromotionInfo((PromotionTypes) iPromotion).getTextKeyWide()), GC.getPromotionInfo((PromotionTypes)iPromotion).getButton(), iPromotion, WIDGET_HELP_PROMOTION, iPromotion, -1);
					++iNumPromotions;
					break;
				}
			}
		}
	}

	if (iNumPromotions == 0)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchEmbarkOnShipPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvUnit* pSailorUnit = GET_PLAYER(GC.getGameINLINE().getActivePlayer()).getUnit(info.getData1());
	if (pSailorUnit == NULL)
	{
		return false;
	}

	CvSelectionGroup* pSelectionGroup = pSailorUnit->getGroup();
	if (NULL == pSelectionGroup)
	{
		return false;
	}

	CvPlot* pPlot = pSelectionGroup->plot();
	if (pPlot == NULL)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_EMBARK_ON_SHIP"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_BODY_EMBARK_ON_SHIP"));

	int iCount = 1;
	CvUnit* pFirstUnit = NULL;
	CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);
		if (pLoopUnit->canHaveCrew())
		{
			if (!pFirstUnit)
			{
				pFirstUnit = pLoopUnit;
			}
			CvWStringBuffer szBuffer;
			GAMETEXT.setUnitHelp(szBuffer, pLoopUnit, true);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer.getCString(), NULL, iCount, WIDGET_GENERAL);
			iCount++;
		}
	}
	if (iCount <= 2)
	{
		if (pFirstUnit)
		{
			pUnitNode = pPlot->headUnitNode();
			while (pUnitNode != NULL)
			{
				CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pPlot->nextUnitNode(pUnitNode);

				if (pLoopUnit->canHaveCrew())
				{
					gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_LOAD_CREW, pLoopUnit->getID(), pSailorUnit->getID(), 0);
				}
			}
		}
		return false;
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}

bool CvDLLButtonPopup::launchLoadCrewPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnitShip = kPlayer.getUnit(info.getData1());
	if (pUnitShip == NULL)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_LOAD_CREW"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_LOAD_CREW"));

	int iNbUnit = 0;
	for (int i = 0; i < kPlayer.getNumEuropeUnits(); ++i)
	{
		CvUnit* pLoopUnit = kPlayer.getEuropeUnit(i);
		if (pLoopUnit->getProfession() == PROFESSION_SAILOR)
		{
			if (!pLoopUnit->isHurt() && pLoopUnit->canBeSailor())
			{
				iNbUnit++;
				CvWStringBuffer szBuffer;
				GAMETEXT.setSailorHelp(szBuffer, pLoopUnit);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer.getCString(), NULL, iNbUnit, WIDGET_GENERAL);
			}
		}
	}
	if (iNbUnit == 0)
	{
		return false;
	}
	if (iNbUnit == 1)
	{
		for (int i = 0; i < kPlayer.getNumEuropeUnits(); ++i)
		{
			CvUnit* pLoopUnit = kPlayer.getEuropeUnit(i);
			if (pLoopUnit->getProfession() == PROFESSION_SAILOR)
			{
				if (!pLoopUnit->isHurt() && pLoopUnit->canBeSailor())
				{
					gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_LOAD_CREW, pUnitShip->getID(), pLoopUnit->getID(), 1);
					return false;
				}
			}
		}
		return false;
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}

bool CvDLLButtonPopup::launchGiveMapPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	if (pUnit == NULL)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_GIVE_MAP"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_GIVE_MAP"));

	int iPotentialUnitCount = 0;
	int iLastUnitId = -1;
	CvPlot* pUnitPlot = pUnit->plot();
	if (pUnitPlot == NULL)
	{
		return false;
	}

	int iDistance = 1;
	for (int iDX = -iDistance; iDX <= iDistance; iDX++)
	{
		for (int iDY = -iDistance; iDY <= iDistance; iDY++)
		{
			CvPlot* pPlot = plotXY(pUnitPlot->getX_INLINE(), pUnitPlot->getY_INLINE(), iDX, iDY);
			if (pPlot != NULL)
			{
				CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
				while (pUnitNode != NULL)
				{
					CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = pPlot->nextUnitNode(pUnitNode);
					if (pLoopUnit != pUnit)
					{
						if (pLoopUnit->getOwner() == pUnit->getOwner() && pLoopUnit->canHaveMap())
						{
							iPotentialUnitCount++;
							iLastUnitId = pLoopUnit->getID();
							CvWStringBuffer szBuffer;
							GAMETEXT.setUnitHelp(szBuffer, pLoopUnit, true);
							gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer.getCString(), NULL, pLoopUnit->getID(), WIDGET_GENERAL);
						}
					}
				}
			}
		}
	}
	if (iPotentialUnitCount == 1 && iLastUnitId != -1)
	{
		gDLL->sendPlayerAction(GC.getGameINLINE().getActivePlayer(), PLAYER_ACTION_GIVE_MAP, info.getData1(), iLastUnitId, -1);							
		return false;
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, -1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return iPotentialUnitCount > 1;
}
bool CvDLLButtonPopup::launchTransfertMapToEuropePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());

	if (!kPlayer.isOption(PLAYEROPTION_SHOW_MAP_POPUP))
	{
		gDLL->getInterfaceIFace()->addMessage(GC.getGameINLINE().getActivePlayer(), true, GC.getEVENT_MESSAGE_TIME(), gDLL->getText("TXT_KEY_INFO_MAP_REVEALED"), "AS2D_POSITIVE_DINK", MESSAGE_TYPE_INFO);
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_REVEALED_MAP"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REVEALED_MAP"));

	gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, 1, WIDGET_GENERAL, POPUP_LAYOUT_LEFT);
	gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_DISABLE_SHOW_MAP_POPUP"),1);
	gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, 0, 1);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}
bool CvDLLButtonPopup::launchRemoveAssignedUnitPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	if (pUnit == NULL)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REMOVE_ASSIGNED_UNIT_TITLE"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REMOVE_ASSIGNED_UNIT_BODY"));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_YES"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_NO"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);


	return (true);
}
bool CvDLLButtonPopup::launchNewAgreementPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	CvWString szBuffer;
	if (pUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();
	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL)
	{
		return false;
	}
	CvSelectionGroup* pGroup = pUnit->getGroup();
	if (pGroup == NULL)
	{
		return false;
	}
	CvPlayer& kNativePlayer = GET_PLAYER(pCity->getOwner());

	CvCity* pPreviousCity = kPlayer.getCity(info.getData2());


	int iNumCities = kPlayer.getNumCities();
	if (iNumCities==0)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPPUP_AGREEMENT_CHOOSE_CITY_TITLE"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPPUP_AGREEMENT_CHOOSE_CITY_BODY"));


	int iLoop;
	int iI = 0;
	int iTurn;
	for (CvCity* pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
	{
		pGroup->generatePath(pPlot, pLoopCity->plot(), 0, false, &iTurn);
		bool isPreviousCity = pPreviousCity == pLoopCity;
		if (iTurn < 10)
		{
			if (!pUnit->getUnitInfo().isMechUnit())
			{
				szBuffer = gDLL->getText("TXT_KEY_POPUP_AGREEMENT_INFO_CITY_1", pLoopCity->getNameKey(), iTurn);
			}
			else
			{
				szBuffer = gDLL->getText("TXT_KEY_POPUP_AGREEMENT_INFO_CITY_2", pLoopCity->getNameKey(), iTurn);
			}
			if (isPreviousCity)
			{
				szBuffer.Format(SETCOLR L"%s" ENDCOLR, TEXT_COLOR("COLOR_GREEN"), szBuffer.GetCString());
			}
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, NULL, pLoopCity->getID(), WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
			iI++;
		}
	}
	if (iI==0)
	{
		return false;
	}

	if (info.getData3() > 0)
	{
		gDLL->sendDoCommand(pUnit->getID(), COMMAND_YIELD_TRADE, 1, -1, false);
		gDLL->getInterfaceIFace()->selectUnit(pUnit, true);
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, -1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);


	return (true);
}
bool CvDLLButtonPopup::launchNewAgreementPart1Popup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	CvWString szBuffer;
	if (pUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();
	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL)
	{
		return false;
	}
	if (pCity->getOwner() == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kOtherPlayer = GET_PLAYER(pCity->getOwner());
	YieldTypes eYield = pCity->getNativeYieldProduce();
	if (eYield == NO_YIELD)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_AGREEMENT_PART_1_TITLE"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_AGREEMENT_PART_1_BODY"));

	int iQuantity;
	int iTurns;
	int iNumAgreementProposition = kOtherPlayer.getNumAgreementProposition(kPlayer.getID());

	for (int iI = 1; iI <= iNumAgreementProposition; iI++)
	{
		iQuantity = kOtherPlayer.getAgreementQuantity(iI);
		iTurns = kOtherPlayer.getAgreementMaxTurn(iI);
		
		CvWString szTempString = gDLL->getText("TXT_KEY_AGREEMENT_PART_1_ELEMENT", iQuantity, GC.getYieldInfo(eYield).getTextKeyWide(), iTurns);
		szTempString = CvWString::format(SETCOLR L"%s" ENDCOLR, 205, 153, 0, 255, szTempString.GetCString());
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szTempString, NULL, iI, WIDGET_GENERAL);
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}
bool CvDLLButtonPopup::launchNewAgreementPart2Popup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	CvWString szBuffer;
	if (pUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();
	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL)
	{
		return false;
	}
	if (pCity->getOwner() == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kOtherPlayer = GET_PLAYER(pCity->getOwner());
	YieldTypes eWantedYield = pCity->getWantedYield();
	if (eWantedYield == NO_YIELD)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_AGREEMENT_PART_2_TITLE"));

	CvAgreement* pAgreement = kPlayer.getAgreement(info.getData2());
	if (pAgreement == NULL)
	{
		return false;
	}
	YieldTypes eSourceYield = pCity->getNativeYieldProduce();
	int iNumSourceYield = pAgreement->getOriginalAmount(eSourceYield);
	int iTurn = pAgreement->getTurnMax();
	int iIncrease = info.getData3();
	if (iIncrease > 0)
	{
		int iWantedYieldValue = kOtherPlayer.getPlayerValue(eWantedYield);
		if (iWantedYieldValue == -100)
		{
			return false;
		}
		int iValue = kOtherPlayer.getPlayerValue(eSourceYield, iNumSourceYield);
		int iNumWantedYield =  kOtherPlayer.getNumWantedValue(iValue, eWantedYield, iIncrease);
		if (iNumWantedYield <= 0)
		{
			return false;
		}
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_AGREEMENT_PART_2_BODY", iNumSourceYield, GC.getYieldInfo(eSourceYield).getChar(), iNumWantedYield, GC.getYieldInfo(eWantedYield).getChar(), iTurn));
		if (!info.getOption1())
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW"), NULL, 2, WIDGET_GENERAL);
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_ACCEPT_NEGO_CREW"), NULL, 1, WIDGET_GENERAL);
		if (iIncrease >= 55)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_CANCEL_NEGO_CREW"), NULL, -1, WIDGET_GENERAL);
		}
		else
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_CANCEL_NEGO_CREW"), NULL, 0, WIDGET_GENERAL);
		}
	}
	else
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_AGREEMENT_PART_2_BODY_REFUSE"));
		int iMemoryAiGiftTrade = kOtherPlayer.AI_getMemoryCount(ePlayer, MEMORY_AI_GIFT_TRADE);
		int iRelationTrade = kOtherPlayer.getRelationTradeTo(ePlayer); // Between -15 and 15
		if (iRelationTrade > 5 && iMemoryAiGiftTrade >= 10)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW"), NULL, 3, WIDGET_GENERAL);
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW_REFUSE_ANSWER"), NULL, 0, WIDGET_GENERAL);

	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}
bool CvDLLButtonPopup::launchEndOfAgreementPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvAgreement* pAgreement = kPlayer.getAgreement(info.getData1());
	if (pAgreement == NULL) 
	{
		return false;
	}

	CvPlayer& kOther = GET_PLAYER(pAgreement->getDestinationCity().eOwner);

	int iPercent = info.getData2();
	const wchar* szCity1 = iPercent != -2 ? pAgreement->getDestinationCityNameKey() : NULL;
	const wchar* szCity2 =  iPercent != -2 ? pAgreement->getSourceCityNameKey() : NULL;
	YieldTypes eYield1 = pAgreement->getImportYield();
	YieldTypes eYield2 = pAgreement->getExportYield();

	if (eYield2 == NO_YIELD)
	{
		return false;
	}

	int iNumMax2 = pAgreement->getOriginalAmount(eYield2);
	CvYieldInfo& kYield2 = GC.getYieldInfo(eYield2);
	//Be careful. Only working because only two ressources
	if (eYield1 == NO_YIELD )
	{
		if (pAgreement->getFixedPrice() > 0)
		{
			gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_END_OF_AGREEMENT_ALLRIGHT_TITLE"));
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_END_OF_AGREEMENT_PRIME",  iNumMax2, kYield2.getChar(), pAgreement->getFixedPrice(), pAgreement->getPrime()));
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY );

			gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

			return (true);
		}

		return false;
	}

	int iNumMax1 = pAgreement->getOriginalAmount(eYield1);
	CvYieldInfo& kYield1 = GC.getYieldInfo(eYield1);

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_END_OF_AGREEMENT_ALLRIGHT_TITLE"));
	if (iPercent == -1)
	{
		// Aggreement fully completed
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_END_OF_AGREEMENT_ALLRIGHT_BODY", szCity1, szCity2, iNumMax1, kYield1.getChar(), iNumMax2, kYield2.getChar(), kYield1.getTextKeyWide(), kYield2.getTextKeyWide()));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_END_OF_AGREEMENT_BUTTON_RENEW"), NULL, 2, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY );

	}
	else if (iPercent == -2)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_END_OF_AGREEMENT_CITY_DESTROY_BODY", kYield1.getTextKeyWide(), kYield2.getTextKeyWide()));
	}
	else
	{
		//Le cas où le contract ne s'est pas bien déroulé
		if (iPercent > 66)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_END_OF_AGREEMENT_BAD04_BODY", szCity1, szCity2, kYield1.getTextKeyWide(), kYield2.getTextKeyWide()));
		}
		else if (iPercent > 33)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_END_OF_AGREEMENT_BAD03_BODY", szCity1, szCity2, kYield1.getTextKeyWide(), kYield2.getTextKeyWide()));
		}
		else if (iPercent > 0)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_END_OF_AGREEMENT_BAD02_BODY", szCity1, szCity2, kYield1.getTextKeyWide(), kYield2.getTextKeyWide()));
		}
		else if (iPercent == 0)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_END_OF_AGREEMENT_BAD01_BODY", szCity1, szCity2, kYield1.getTextKeyWide(), kYield2.getTextKeyWide()));
		}
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY );

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}

bool CvDLLButtonPopup::launchAutomaticRenewAgreementPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvAgreement* pAgreement = kPlayer.getAgreement(info.getData1());
	if (pAgreement == NULL) 
	{
		return false;
	}

	CvPlayer& kOther = GET_PLAYER(pAgreement->getDestinationCity().eOwner);

	int iPercent = info.getData2();
	const wchar* szCity1 = iPercent != -2 ? pAgreement->getDestinationCityNameKey() : NULL;
	const wchar* szCity2 =  iPercent != -2 ? pAgreement->getSourceCityNameKey() : NULL;
	YieldTypes eYield1 = pAgreement->getImportYield();
	YieldTypes eYield2 = pAgreement->getExportYield();

	if (eYield1 == NO_YIELD || eYield2 == NO_YIELD)
	{
		return false;
	}
	
	int iNumMax1 = pAgreement->getOriginalAmount(eYield1);
	int iNumMax2 = pAgreement->getOriginalAmount(eYield2);
	CvYieldInfo& kYield1 = GC.getYieldInfo(eYield1);
	CvYieldInfo& kYield2 = GC.getYieldInfo(eYield2);	

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_BUTTONPOPUP_AUTOMATIC_RENEW_AGREEMENT_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_BUTTONPOPUP_AUTOMATIC_RENEW_AGREEMENT_BODY", szCity1, szCity2, iNumMax1, kYield1.getChar(), iNumMax2, kYield2.getChar(), kYield1.getTextKeyWide(), kYield2.getTextKeyWide()));
	
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 0, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY );
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_BUTTONPOPUP_AUTOMATIC_AGREEMENT_BY_YIELDS_HEADER"), NULL, 1, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY );
	
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}

bool CvDLLButtonPopup::launchAutomaticAgreementByYieldsPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_BUTTONPOPUP_AUTOMATIC_AGREEMENT_BY_YIELDS_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_BUTTONPOPUP_AUTOMATIC_AGREEMENT_BY_YIELDS_BODY"));

	for (int iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		YieldTypes eYield = (YieldTypes) iI;
		if (GC.getGameINLINE().isWantedYieldForNatives(eYield))
		{
			CvYieldInfo& kYield = GC.getYieldInfo(eYield);
			gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, eYield, WIDGET_GENERAL, POPUP_LAYOUT_LEFT);
			gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, kYield.getDescription(), eYield);
			gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, kPlayer.hasAutomaticTradeByYieldWithNative(eYield), eYield);
		}
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY );

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}

//DOANE Europe Warehouse
bool CvDLLButtonPopup::launchWarehouseUpgradePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER((PlayerTypes)info.getData1());
	int iNumAgreement = kPlayer.getNumAgreement();
	int iYieldsTraded = kPlayer.getTotalTradeYields();
	int iNewCapacity = info.getData2();
	int iCapacity = info.getData3();

	//First Warehouse? Old capacity must be zero...
	if (!iCapacity)
	{
		gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_WAREHOUSE_UPGRADE_FIRST_TITLE"));
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_WAREHOUSE_UPGRADE_FIRST_BODY", iNewCapacity));
	}
	//Warehouse Upgrade
	else
	{
		gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_WAREHOUSE_UPGRADE_TITLE"));
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_WAREHOUSE_UPGRADE_BODY", iNumAgreement, iYieldsTraded, iNewCapacity));
	}

	gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY );
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}
bool CvDLLButtonPopup::launchAgreementListPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvWString szBuffer;
	CvString szArtFilename;

	std::vector<CvAgreement*> aiAgreements;
	kPlayer.getAgreements(aiAgreements);
	int iNumAgreement = 1;

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_SITUATION_AGREEMENT_TITLE"));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_BUTTONPOPUP_AUTOMATIC_AGREEMENT_BY_YIELDS_HEADER"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_VISUEL_TOPO_CONTRAT_EUROPEEN")->getPath(), -2, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY);
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_SITUATION_AGREEMENT_BODY"));
	for (uint i = 0; i < aiAgreements.size(); ++i)
	{
		CvAgreement* pAgreement = aiAgreements[i];
		if (pAgreement != NULL)
		{
			CvPlayer& kPlayer1 = GET_PLAYER(pAgreement->getSourceCity().eOwner);
			CvPlayer& kPlayer2 = GET_PLAYER(pAgreement->getDestinationCity().eOwner);
			YieldTypes eYield1 = NO_YIELD, eYield2 = NO_YIELD;
			//On récupère et vérifie qu'il n'y a que deux ressources
			if (pAgreement->getYieldOfType(TRADE_IMPORT, &eYield1) == 1 && pAgreement->getYieldOfType(TRADE_EXPORT, &eYield2) == 1)
			{
				if (kPlayer2.isNative())
				{
					szArtFilename = ARTFILEMGR.getInterfaceArtInfo("INTERFACE_VISUEL_TOPO_CONTRAT_NATIFS")->getPath();
				}
				else
				{
					szArtFilename = ARTFILEMGR.getInterfaceArtInfo("INTERFACE_VISUEL_TOPO_CONTRAT_MARCHAND_EUROPE")->getPath();
				}
				if (pAgreement->getNumAssignedGroups() == 0)
				{
					szBuffer = gDLL->getText("TXT_KEY_POPUP_CHOOSE_AGREEMENT_INFO_W", iNumAgreement, GC.getYieldInfo(eYield1).getTextKeyWide(), GC.getYieldInfo(eYield2).getTextKeyWide());
				}
				else
				{
					szBuffer = gDLL->getText("TXT_KEY_POPUP_CHOOSE_AGREEMENT_INFO", iNumAgreement, GC.getYieldInfo(eYield1).getTextKeyWide(), GC.getYieldInfo(eYield2).getTextKeyWide());
				}
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, szArtFilename, pAgreement->getID(), WIDGET_HELP_AGREEMENT, -1, pAgreement->getID(), true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
				iNumAgreement++;
			}
		}
	}
	if (iNumAgreement == 1)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_NO_SITUATION_AGREEMENT_SUB_TITLE"));
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchSeedlingListPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvTeam& kCurrentTeam = GET_TEAM(kPlayer.getTeam());
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_VIEW_SEEDLING_TITLE"));

	std::vector<bool> aForeignersHasSeedling(GC.getNumBonusInfos(), false);
	for (int iI = 0; iI < MAX_TEAMS; iI++)
	{
		CvTeam& kLoopTeam = GET_TEAM((TeamTypes)iI);
		if (kCurrentTeam.getID() != kLoopTeam.getID() && kLoopTeam.isAlive() && kLoopTeam.hasAnEuropean())
		{
			for (int iJ = 0; iJ < GC.getNumBonusInfos(); ++iJ)
			{
				BonusTypes eBonus = (BonusTypes)iJ;
				if (kLoopTeam.hasSeedling(eBonus))
				{
					aForeignersHasSeedling[eBonus] = true;
				}
			}
		}
	}

	for (int iI = 0; iI < GC.getNumBonusInfos(); ++iI) 
	{
		BonusTypes eBonus = (BonusTypes)iI;
		if (kCurrentTeam.hasSeedling(eBonus)) 
		{
			// we have the seedling
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_VIEW_SEEDLING_ROW_OWN", GC.getBonusInfo(eBonus).getDescription()), GC.getBonusInfo(eBonus).getButton(), eBonus, WIDGET_PEDIA_JUMP_TO_BONUS, eBonus);
		}
	}

	for (int iI = 0; iI < GC.getNumBonusInfos(); ++iI) 
	{
		BonusTypes eBonus = (BonusTypes)iI;
		if (!kCurrentTeam.hasSeedling(eBonus)) 
		{
			if (aForeignersHasSeedling[eBonus]) 
			{ 
				// Another european have the seedling
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_VIEW_SEEDLING_ROW_OWN_BY_SOMEONE_ELSE", GC.getBonusInfo(eBonus).getDescription()), GC.getBonusInfo(eBonus).getButton(), eBonus, WIDGET_PEDIA_JUMP_TO_BONUS, eBonus);
			}
		}
	}

	for (int iI = 0; iI < GC.getNumBonusInfos(); ++iI) 
	{
		BonusTypes eBonus = (BonusTypes)iI;
		if (!kCurrentTeam.hasSeedling(eBonus) && !aForeignersHasSeedling[eBonus] && GC.getBonusInfo(eBonus).isSeed()) 
		{
			// Nobody has the plant			
			YieldTypes eYield = (YieldTypes) GC.getBonusInfo(eBonus).getMainYield();
			if (eYield != NO_YIELD && !kPlayer.isHasYieldUnknown(eYield)) 
			{
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_VIEW_SEEDLING_ROW_NOT_OWN", GC.getBonusInfo(eBonus).getDescription()), GC.getBonusInfo(eBonus).getButton(), eBonus, WIDGET_PEDIA_JUMP_TO_BONUS, eBonus);
			}
		}
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CANCEL")->getPath(), -1, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchSeedlingDiscoveryGoodSituationPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	BonusTypes eBonus = (BonusTypes)info.getData1();
	if (eBonus == NO_BONUS)
	{
		return false;
	}
	CvBonusInfo& kBonus = GC.getBonusInfo(eBonus);

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_NEW_SEEDLING_HEADER"));
	int iState = info.getData2();
	CvCity* pFoundedCity = info.getData3() != -1 ? kPlayer.getCity(info.getData3()) : NULL;

	CvWString szBenefitsTo;
	if (kBonus.getMainYield() != YIELD_FOOD)
	{
		szBenefitsTo = gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_GOOD_SITUATION_EUROPE_PROFIT");
	}
	else
	{
		szBenefitsTo = gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_GOOD_SITUATION_COLONY_FEED");
	}

	if (iState == -1)
	{
		//-------------------------------------
		//	FIRST TIME THE POPUP iS LAUNCHED
		//-------------------------------------
		if (pFoundedCity == NULL)
		{			
			CvWString szCities;
			CvWString szIdealCities;
			int iNumIdealCities = 0;
			int iLoop;
			for (CvCity* pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
			{
				if (pLoopCity->getBonusLandsValue(eBonus) > 0)
				{
					iNumIdealCities++;
					if (!szIdealCities.empty())
					{
						szIdealCities += L", ";
					}
					szIdealCities += CvWString::format(L"%s", pLoopCity->getName().GetCString());
				}
				if (!szCities.empty())
				{
					szCities += L", ";
				}
				szCities += CvWString::format(L"%s", pLoopCity->getName().GetCString());
			}
			if (iNumIdealCities > 0)
			{
				// At least one of the colonies player is ideal
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText(iNumIdealCities == 1 ? "TXT_KEY_NEW_SEEDLING_DISCOVERY_GOOD_SITUATION_ONE_COLONY" : "TXT_KEY_NEW_SEEDLING_DISCOVERY_GOOD_SITUATION_MANY_COLONIES", kBonus.getDescription(), szIdealCities.GetCString(), szBenefitsTo.GetCString()));

				if (!kPlayer.isOption(PLAYEROPTION_HIDE_HELPS_FOR_CULTURE))
				{
					gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEXT"), NULL, 10, WIDGET_GENERAL);
				}
				else
				{
					gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEXT"), NULL, 12, WIDGET_GENERAL);//We miss some steps
				}
			}
			else
			{
				//None of conies player are ideal
				switch(kPlayer.getNumCities())
				{
				case 0:
					gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_BAD_SITUATION_NO_COLONY", kBonus.getDescription()));
					break;
				case 1:
					gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_BAD_SITUATION_ONE_COLONY", kBonus.getDescription(), szCities.GetCString()));
					break;
				default:			
					gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_BAD_SITUATION_MANY_COLONIES", kBonus.getDescription(), szCities.GetCString()));
				}
				if (kBonus.getMainYield() != YIELD_FOOD)
				{
					gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_EUROPE_PROFIT"));
				}
				else
				{
					gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_COLONY_FEED"));
				}


				if (!kPlayer.isOption(PLAYEROPTION_HIDE_HELPS_FOR_CULTURE))
				{				
					gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEXT"), NULL, 20, WIDGET_GENERAL);
				}
				else
				{
					gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, -1, WIDGET_GENERAL);
				}
			}
		}
		else
		{
			//POPUP IS LAUNCHED AFTER A COLONY FOUND

			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_GOOD_SITUATION_FOUND_COLONY", kBonus.getDescription(), pFoundedCity->getName().GetCString(), szBenefitsTo.GetCString()));

			if (!kPlayer.isOption(PLAYEROPTION_HIDE_HELPS_FOR_CULTURE))
			{
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEXT"), NULL, 10, WIDGET_GENERAL);
			}
			else
			{
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEXT"), NULL, 12, WIDGET_GENERAL);//We miss some steps
			}
		}
	}
	else if (iState >= 10 && iState < 20)
	{
		//-------------------------------------------
		//	IF PLAYER HAS AT LEAST ONE IDEAL COLONY
		//-------------------------------------------
		if (iState == 10)
		{
			gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Popups/Help_Seedling_Plants_01.dds", 360, 100);
			CvWString szBuffer;
			int iCount = 0;
			for (int iK = 0; iK < GC.getNumTerrainInfos(); ++iK)
			{
				//Terrain
				if (kBonus.isTerrain(iK))
				{
					if (iCount > 0)
					{
						szBuffer += L", "; //This puts commas after all values but first and last...
					}
					szBuffer += CvWString::format(L"%s", GC.getTerrainInfo((TerrainTypes)iK).getDescription());
					iCount++;
				}
				else if (kBonus.isFeatureTerrain(iK))
				{//Features in Terrain
					for (int iJ = 0; iJ < GC.getNumFeatureInfos(); ++iJ)
					{
						if (kBonus.isFeature(iJ))
						{
							if (iCount > 0)
							{
								szBuffer += L", "; //This puts commas after all values but first and last...
							}
							szBuffer += CvWString::format(L"%s (in %s)", GC.getFeatureInfo((FeatureTypes)iJ).getDescription(), GC.getTerrainInfo((TerrainTypes)iK).getDescription());
							iCount++;
						}
					}
				}	
			} 

			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_GOOD_SITUATION_FARM_EXPLANATION", kBonus.getDescription(), szBuffer.GetCString()));

			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEXT"), NULL, 11, WIDGET_GENERAL);

		}
		else if (iState == 11)
		{
			gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Popups/Help_Seedling_Plants_02.dds", 360, 100);

			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_GOOD_SITUATION_AGRONOMIST_EXPLANATION"));

			gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, 1, WIDGET_GENERAL, POPUP_LAYOUT_LEFT);
			gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_DO_NOT_DISPLAY_ANYMORE"),1);
			gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, 0, 1);

			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEXT"), NULL, 12, WIDGET_GENERAL);
		}
	}
	else
	{
		//-------------------------------------------
		//	IF PLAYER HAS NOT IDEAL COLONY
		//-------------------------------------------
		gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Popups/Help_Seedling_Plants_01.dds", 360, 100);

		CvWString szBuffer;
		int iCount = 0;
		for (int iK = 0; iK < GC.getNumTerrainInfos(); ++iK)
		{
			//Terrain
			if (kBonus.isTerrain(iK))
			{
				if (iCount > 0)
				{
					szBuffer += L", "; //This puts commas after all values but first and last...
				}
				szBuffer += CvWString::format(L"%s", GC.getTerrainInfo((TerrainTypes)iK).getDescription());
				iCount++;
			}
			else if (kBonus.isFeatureTerrain(iK))
			{//Features in Terrain
				for (int iJ = 0; iJ < GC.getNumFeatureInfos(); ++iJ)
				{
					if (kBonus.isFeature(iJ))
					{
						if (iCount > 0)
						{
							szBuffer += L", "; //This puts commas after all values but first and last...
						}
						szBuffer += CvWString::format(L"%s (in %s)", GC.getFeatureInfo((FeatureTypes)iJ).getDescription(), GC.getTerrainInfo((TerrainTypes)iK).getDescription());
						iCount++;
					}
				}
			}
		}

		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_NEW_SEEDLING_DISCOVERY_BAD_SITUATION_FARM_EXPLANATION", kBonus.getDescription(), kBonus.getMinIdealLatitude(), kBonus.getMaxIdealLatitude(), szBuffer.GetCString()));

		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, -1, WIDGET_GENERAL);			
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchChangeCityAgreementPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvWString szBuffer;

	CvAgreement* pAgreement = kPlayer.getAgreement(info.getData1());
	if (pAgreement == NULL)
	{
		return false;
	}
	if (kPlayer.getNumCities() == 0)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_CHANGE_CITY_AGREEMENT_TITLE"));
	CvCity* pSourceCity = ::getCity(pAgreement->getSourceCity());

	if (pSourceCity == NULL)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_INIT_CITY_AGREEMENT_NO_CHOICE_BODY"));
		int iLoop;
		for (CvCity* pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
		{
			szBuffer = gDLL->getText("TXT_KEY_AGREEMENT_INFO_SOURCE1", pLoopCity->getNameKey());
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, NULL, pLoopCity->getID(), WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY );
		}
	}
	else
	{
		if (kPlayer.getNumCities() == 1)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_CHANGE_CITY_AGREEMENT_NO_CHOICE_BODY", pSourceCity->getNameKey()));
			gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);

			return true;
		}

		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_CHANGE_CITY_AGREEMENT_BODY", pSourceCity->getNameKey()));
		int iLoop;
		for (CvCity* pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
		{
			if (pLoopCity != pSourceCity)
			{
				szBuffer = gDLL->getText("TXT_KEY_AGREEMENT_INFO_SOURCE1", pLoopCity->getNameKey());
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, NULL, pLoopCity->getID(), WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY );
			}
		}
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, -1, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_CENTER_JUSTIFY );

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}
bool CvDLLButtonPopup::launchChooseAgreementsPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	CvWString szBuffer;
	CvString szArtFilename;
	if (pUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();

	CvSelectionGroup* pGroup = pUnit->getGroup();
	if (pGroup == NULL)
	{
		return false;
	}

	std::vector<CvAgreement*> aiAgreements;
	kPlayer.getAgreements(aiAgreements);
	int iNumAgreement = 1;
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_CHOOSE_AGREEMENT_TITLE"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_CHOOSE_AGREEMENT_SUB_TITLE"));
	gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
	for (uint i = 0; i < aiAgreements.size(); ++i)
	{
		CvAgreement* pAgreement = aiAgreements[i];
		if (pGroup->canAssignAgreement(pAgreement->getID()))
		{
			YieldTypes eYield1, eYield2;
			if (pAgreement->getYieldOfType(TRADE_IMPORT, &eYield1) == 1 && pAgreement->getYieldOfType(TRADE_EXPORT, &eYield2) == 1)
			{
				if (pGroup->isAssignedAgreement(pAgreement->getID()))
				{
					szArtFilename = ARTFILEMGR.getInterfaceArtInfo("INTERFACE_UNIT_COCHE_ACTIVATE")->getPath();
				}
				else
				{
					szArtFilename = ARTFILEMGR.getInterfaceArtInfo("INTERFACE_UNIT_COCHE_NO_ACTIVATE")->getPath();
				}
				if (pAgreement->getNumAssignedGroups() == 0)
				{
					szBuffer = gDLL->getText("TXT_KEY_POPUP_CHOOSE_AGREEMENT_INFO_W", iNumAgreement, GC.getYieldInfo(eYield1).getTextKeyWide(), GC.getYieldInfo(eYield2).getTextKeyWide());
				}
				else
				{
					szBuffer = gDLL->getText("TXT_KEY_POPUP_CHOOSE_AGREEMENT_INFO", iNumAgreement, GC.getYieldInfo(eYield1).getTextKeyWide(), GC.getYieldInfo(eYield2).getTextKeyWide());
				}

				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, szArtFilename, pAgreement->getID(), WIDGET_HELP_AGREEMENT, info.getData1(), pAgreement->getID(), true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
				iNumAgreement++;
			}
		}
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);

	return (true);
}
bool CvDLLButtonPopup::launchChooseTradeRoutesPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	CvWString szBuffer;
	CvString szArtFilename;
	if (pUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();

	CvSelectionGroup* pGroup = pUnit->getGroup();
	if (pGroup == NULL)
	{
		return false;
	}
	CvCity* pCity = NULL;
	bool bIsEurope = info.getData2() == -2;

	if (info.getData2() >= 0)
	{
		pCity = kPlayer.getCity(info.getData2());	
	} 

	std::vector<CvTradeRoute*> aiTradeRoutes;
	kPlayer.getTradeRoutes(aiTradeRoutes);
	int iNumTradeRoute = 1;
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_CHOOSE_TRADE_ROUTES_TITLE"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_CHOOSE_TRADE_ROUTES_SUB_TITLE"));
	gDLL->getInterfaceIFace()->popupAddSeparator(pPopup);
	for (uint i = 0; i < aiTradeRoutes.size(); ++i)
	{
		CvTradeRoute* pTradeRoute = aiTradeRoutes[i];
		if (pGroup->canAssignTradeRoute(pTradeRoute->getID()))
		{
			if (pCity == NULL || pTradeRoute->getSourceCity() == pCity->getIDInfo() || pTradeRoute->getDestinationCity() == pCity->getIDInfo())
			{
				if (!bIsEurope || pTradeRoute->getSourceCity().iID == CvTradeRoute::EUROPE_CITY_ID || pTradeRoute->getDestinationCity().iID == CvTradeRoute::EUROPE_CITY_ID)
				{		
					if (pGroup->isAssignedTradeRoute(pTradeRoute->getID()))
					{
						szArtFilename = ARTFILEMGR.getInterfaceArtInfo("INTERFACE_UNIT_COCHE_ACTIVATE")->getPath();
					}
					else
					{
						szArtFilename = ARTFILEMGR.getInterfaceArtInfo("INTERFACE_UNIT_COCHE_NO_ACTIVATE")->getPath();
					}

					szBuffer = gDLL->getText("TXT_KEY_TRADE_ROUTES_ROW", pTradeRoute->getSourceCityNameKey(), pTradeRoute->getDestinationCityNameKey());
					gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, szArtFilename, pTradeRoute->getID(), WIDGET_HELP_TRADE_ROUTES, info.getData1(), pTradeRoute->getID(), true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
					iNumTradeRoute++;
				}
			}
		}
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);

	return (true);
}
bool CvDLLButtonPopup::launchSendTradePropositionPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	if (pUnit == NULL)
	{
		return false;
	}

	CvPlot* pPlot = pUnit->plot();
	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL)
	{
		return false;
	}

	if (pCity->getBanTurnTo(kPlayer.getID()) > 0)
	{
		// The player is banned for some turns
		gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_INCORRECTLY_TRANSACT"));
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REFUS_TRANSACT"));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, 0, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

		return true;
	}

	int iTest;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		iTest = 0;
		if (kPlayer.getHumanTradeProposition(eYield) > 0)
		{
			iTest++;
		}

		if (kPlayer.getIATradeProposition(eYield) > 0)
		{
			iTest++;
		}

		if (iTest == 2)
		{
			// it means the resource is used as request and offer in the same time
			// So we cancel the trade
			gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_INCORRECTLY_TRANSACT"));
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_INCORRECTLY_TRANSACT1"));
			break;
		}
	}

	if (iTest != 2)
	{
		gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_CONFIRM_TRANSACT"));
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_CONFIRM_TRANSACT"));

		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_CONFIRM_TRANSACT_OK"), NULL, 1, WIDGET_GENERAL);
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchProcessTradePropositionPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());

	switch(info.getData2())
	{
	case PROPOSAL_GIFT:
		return GAMETRADE.askGiftOrRequest(pPopup, pUnit, true, false);
	case PROPOSAL_REQUEST:
		return GAMETRADE.askGiftOrRequest(pPopup, pUnit, false, true);
	case PROPOSAL_EXCHANGE:
		return GAMETRADE.askProposal(pPopup, pUnit, true, true);
	case PROPOSAL_ASK_YIELDS:
		return GAMETRADE.askProposal(pPopup, pUnit, true, false);
	case PROPOSAL_SUGGEST_YIELDS:
		return GAMETRADE.askProposal(pPopup, pUnit, false, true);
	case PROPOSAL_FROM_SCRATCH:
		return GAMETRADE.askProposal(pPopup, pUnit, false, false);
	default:
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_WINDOW_CLOSE"), NULL, 1, WIDGET_GENERAL);
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchStopTradePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_STOP_TRADE"));

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REFUS_TRANSACT"));

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}

bool CvDLLButtonPopup::launchSellShipPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnitShip = kPlayer.getUnit(info.getData1());
	if (pUnitShip == NULL)
	{
		return false;
	}

	int iGold = pUnitShip->getSellPrice();
	int iGoldMax = pUnitShip->getUnitInfo().getEuropeCost();

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_SELL_SHIP"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_SELL_SHIP", iGold));

	int iSuggestedPrice = iGold - iGold * 15 / 100;
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 1, L"", iSuggestedPrice, 100, iGoldMax, 100);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_SELL_UNIT_OK"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchStopSellShipPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_STOP_SELL_SHIP"));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_YES"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchRecruitCrewPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvTempUnit* pTempUnit = kPlayer.getTempUnit(info.getData1());
	if (pTempUnit == NULL)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_RECRUIT_CREW"));
	gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Europe/Bandeau_Nego_Equipages_Navires.dds", 360, 100);

	int iBasePrice = info.getData2();
	int iDiscountPercent = info.getData3();
	int iRandomPercent = GC.getASyncRand().get(50-iDiscountPercent/2, "launchRecruitCrewPopup ASYNC");
	int iPrice = iBasePrice - iBasePrice * iDiscountPercent/100;
	int iGold = kPlayer.getGold();
	if (iDiscountPercent > iRandomPercent )
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_CREW_REFUSE"));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW_REFUSE_ANSWER"), NULL, 0, WIDGET_GENERAL);
	}
	else
	{
		if (iDiscountPercent)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_CREW_ACCEPT", iPrice));
		}
		else
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_CREW", iPrice));
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW"), NULL, 2, WIDGET_GENERAL);

		if (iGold >= iPrice)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_ACCEPT_NEGO_CREW"), NULL, 1, WIDGET_GENERAL);
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_CANCEL_NEGO_CREW"), NULL, 0, WIDGET_GENERAL);
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchRecruitImmigrantPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvPlayer& kKingPlayer = GET_PLAYER(kPlayer.getParent());
	CvTempUnit* pTempUnit = kPlayer.getTempUnit(info.getData1());
	if (pTempUnit == NULL)
	{
		return false;
	}

	ImmigrationTypes eType = pTempUnit->getImmigrationType();
	int iGold = kPlayer.getGold();
	int iBasePrice = info.getData2();
	int iDiscountPercent = info.getData3();
	if (eType == NO_IMMIGRATION)
	{
		return false;
	}

	if (eType == IMMIGRATION_CONTRACT && iDiscountPercent == 0)
	{
		int iCost = kPlayer.getEuropeUnitBuyPrice(pTempUnit->getUnitType());
		iCost += iCost*kKingPlayer.getUnitMarketPrice((UnitClassTypes)GC.getUnitInfo(pTempUnit->getUnitType()).getUnitClassType())/100;
		if (iCost > iGold)
		{
			gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_RECRUIT_NO_GOLD"));
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_BODY_RECRUIT_NO_GOLD"));
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW"), NULL, 2, WIDGET_GENERAL);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_LET_ME_THINK_ABOUT_IT"), NULL, -1, WIDGET_GENERAL);
			gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
			return true;
		}
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_RECRUIT_CREW"));
	gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Europe/Bandeau_Nego_Equipages_Navires.dds", 360, 100);

	int iRandomPercent = GC.getASyncRand().get(50-iDiscountPercent/2, "iRandomPercent launchRecruitImmigrantPopup ASYNC");
	int iPrice = iBasePrice - iBasePrice * iDiscountPercent/100;

	if (iDiscountPercent > iRandomPercent )
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_CREW_REFUSE"));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW_REFUSE_ANSWER"), NULL, 0, WIDGET_GENERAL);
	}
	else
	{
		if (iDiscountPercent)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_CREW_ACCEPT", iPrice));
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW"), NULL, 2, WIDGET_GENERAL);
		}
		else
		{
			if (eType == IMMIGRATION_CONTRACT)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_IMMIGRANT_CONTRACT", iPrice));
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW"), NULL, 2, WIDGET_GENERAL);
			}
			else if (eType == IMMIGRATION_RELIGIOUS)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_IMMIGRANT_RELIGIOUS"));
			}
			else if (eType == IMMIGRATION_FOOD)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_IMMIGRANT_FOOD"));
			}
			else if (eType == IMMIGRATION_ECONOMIC)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_IMMIGRANT_ECONOMIC"));
			}
			else /* if (eType == IMMIGRATION_NATURAL)*/ {
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_IMMIGRANT_NATURAL"));
			}
		}

		if (iGold >= iPrice)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_ACCEPT_NEGO_CREW"), NULL, 1, WIDGET_GENERAL);
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_CANCEL_NEGO_CREW"), NULL, 0, WIDGET_GENERAL);
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchBuyShipUsedPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvTempUnit* pTempUnit = kPlayer.getTempUnit(info.getData1());
	if (pTempUnit == NULL)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_EUROPE_TITLE_BUYING_USED"));
	gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Europe/Bandeau_Nego_Equipages_Navires.dds", 360, 100);

	int iBasePrice = info.getData2();
	int iDiscountPercent = info.getData3();
	int iRandomPercent = GC.getASyncRand().get(50-iDiscountPercent/2, "iRandomPercent launchBuyShipUsedPopup");
	int iPrice = iBasePrice - iBasePrice * iDiscountPercent/100;
	int iGold = kPlayer.getGold();
	if ( iDiscountPercent > iRandomPercent )
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_BUY_SHIP_USED_REFUSE"));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW_REFUSE_ANSWER"), NULL, 0, WIDGET_GENERAL);
	}
	else
	{
		if (iDiscountPercent)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_RECRUIT_CREW_ACCEPT", iPrice));
		}
		else
		{
			CvWString szString;
			if (pTempUnit->getDamage() > 50)
			{
				szString = gDLL->getText("TXT_KEY_TITLE_POPUP_BUY_SHIP_USED_BADLY_DAMAGED");
			}
			else
			{
				szString = gDLL->getText("TXT_KEY_TITLE_POPUP_BUY_SHIP_USED_DAMAGED");
			}

			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_BUY_SHIP_USED", iPrice, szString.c_str()));
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW"), NULL, 2, WIDGET_GENERAL);
		if (iGold >= iPrice)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_ACCEPT_NEGO_CREW"), NULL, 1, WIDGET_GENERAL);
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_CANCEL_NEGO_CREW"), NULL, 0, WIDGET_GENERAL);
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}
bool CvDLLButtonPopup::launchBuyNewShipInEuropePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	UnitTypes eUnit = (UnitTypes)info.getData1();
	if (eUnit == NO_UNIT)
	{
		return false;
	}
	if (kPlayer.getUnitTurnRemaining((UnitClassTypes)GC.getUnitInfo(eUnit).getUnitClassType()) > 0)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_EUROPE_TITLE_BUYING_NEW_SHIPS"));
	gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Europe/Bandeau_Nego_Equipages_Navires.dds", 360, 100);

	int iPrice = kPlayer.getEuropeUnitBuyPrice(eUnit);
	CvPlayer& kPlayerEurope = GET_PLAYER(kPlayer.getParent());
	iPrice += iPrice*kPlayerEurope.getUnitMarketPrice((UnitClassTypes)GC.getUnitInfo(eUnit).getUnitClassType())/100;
	int iGold = kPlayer.getGold();
	int iTurn = std::max(1, GC.getUnitInfo(eUnit).getYieldCost(YIELD_HAMMERS)/200);
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_BUY_NEW_SHIP_IN_EUROPE", iPrice, iTurn, GC.getUnitInfo(eUnit).getTextKeyWide()));
	if (iGold >= iPrice)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_ACCEPT_NEGO_CREW"), NULL, 1, WIDGET_GENERAL);
	}
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_CANCEL_NEGO_CREW"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}


bool CvDLLButtonPopup::launchPurchaseBidPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	if (pUnit == NULL)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_HEADER_PURCHASE_BID"));
	gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Europe/Bandeau_Nego_Equipages_Navires.dds", 360, 100);
	int iEstimatePrice = pUnit->getSellPrice();
	int iPriceMax = pUnit->getUnitInfo().getEuropeCost();
	int iSellPrice = pUnit->getShipSellPrice();
	int iRandomAccept = GC.getASyncRand().get(100, "iRandomAccept")*iEstimatePrice/100 ;
	int iProposePrice = info.getData2();

	int iRefusPercent = info.getData3();
	int iRandomRefusPercent = GC.getASyncRand().get(100, "iRandomRefusPercent");
	if (iSellPrice <= iEstimatePrice/2 || iRandomAccept >= iSellPrice)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_ACCEPT_PRICE_PURCHASE_BID", iSellPrice));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_ACCEPT_NEGO_CREW"), NULL, 3, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_CANCEL_NEGO_CREW"), NULL, 0, WIDGET_GENERAL);
	}
	else
	{
		if ( iRefusPercent > iRandomRefusPercent )
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_REFUSE_PRICE_PURCHASE_BID"));
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW_REFUSE_ANSWER"), NULL, 0, WIDGET_GENERAL);
		}
		else
		{
			if (iRefusPercent)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_NEGO2_PRICE_PURCHASE_BID", iProposePrice));
			}
			else
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TITLE_POPUP_NEGO_PRICE_PURCHASE_BID", iProposePrice));
			}
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_NEGO_CREW"), NULL, 2, WIDGET_GENERAL);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_ACCEPT_NEGO_CREW"), NULL, 1, WIDGET_GENERAL);
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_EUROPE_CANCEL_NEGO_CREW"), NULL, 0, WIDGET_GENERAL);
		}
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchReceiveSeedlingPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvTeamAI& kActiveTeam = GET_TEAM(kPlayer.getTeam());
	int iValue = info.getData1();
	int iNumSeed = 0;
	CvWString szBuffer;

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_RECEIVE_SEEDLING_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_RECEIVE_SEEDLING_BODY", iValue));
	for (int iI = 0; iI < GC.getNumBonusInfos(); ++iI)
	{
		BonusTypes eBonus = (BonusTypes)iI;
		if (GC.getBonusInfo(eBonus).isSeed() && (GC.getBonusInfo(eBonus).getRefBonus() != 1))
		{
			//Pas de mais
			YieldTypes eProducedYield = NO_YIELD;
			for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
			{
				YieldTypes eYield = (YieldTypes)iYield;

				if (GC.getBonusInfo(eBonus).getYieldChange(eYield) > 0)
				{
					if (eProducedYield != NO_YIELD)
					{
						//Je n'accepte pas un bonus ayant plus d'une ressource
						eProducedYield = NO_YIELD;
						break;
					}
					eProducedYield = eYield;
				}
			}
			if (eProducedYield != NO_YIELD && !GC.getYieldInfo(eProducedYield).isNewWorld())
			{
				if (!kActiveTeam.hasSeedling(eBonus))
				{
					szBuffer.Format(L"%s", GC.getBonusInfo(eBonus).getDescription());
					gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szBuffer, GC.getBonusInfo(eBonus).getButton(), iI, WIDGET_SEED_INFO, iI, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
					iNumSeed++;
				}
			}
		}
	}
	if (iNumSeed == 0)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return true;
}

bool CvDLLButtonPopup::launchRumDiscoveredPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_RUM_DISCOVERED_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_RUM_DISCOVERED_BODY"));
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);
	return true;
}

bool CvDLLButtonPopup::launchAchievementPopup(CvPopup* pPopup, CvPopupInfo &info)
{

	CvAchieveInfo pAchievement = GC.getAchieveInfo((AchieveTypes)info.getData1());

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText(pAchievement.getHeaderTextKey()));
	if (pAchievement.getPictureFileName() != NULL )
	{
		gDLL->getInterfaceIFace()->popupAddDDS(pPopup, pAchievement.getPictureFileName(), 300, 200);
	}
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText(pAchievement.getBodyTextKey()));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_CONTINUE"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchFoundColonyPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_FOUND_COLONY_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_FOUND_COLONY_BODY"));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_CONTINUE"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_CLOSED"), NULL, 0, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchRepayLoan(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_TRADE_GOLD"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TRADE_GOLD_TO_REPAY"));
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	int iGold = kPlayer.getGold();
	int iLoan =  kPlayer.getEuropeLoan();
	int iMax = std::min(iGold, iLoan);
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 1, L"", 0, 100, iMax, 0);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_CONTINUE"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}
bool CvDLLButtonPopup::launchWhenNewEraPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	NewEraTypes eNewEra = (NewEraTypes)info.getData1();
	bool hasReachedMaxTurnRequirementThanksToAnotherEuropean = info.getData2();
	if (eNewEra == NO_NEW_ERA)
	{
		return false;
	}
	CvNewEraInfo& kNewEra = GC.getNewEraInfo(eNewEra);
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText(kNewEra.getHeaderTextKey()));
	if (kNewEra.getPictureFileName() != NULL ) 
	{
		gDLL->getInterfaceIFace()->popupAddDDS(pPopup, kNewEra.getPictureFileName(), 300, 200);
	}
	if (hasReachedMaxTurnRequirementThanksToAnotherEuropean) 
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_BUTTONPOPUP_WHEN_NEW_ERA_BODY_AUTOMATIC", kNewEra.getDescription()));
	}
	else
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText(kNewEra.getBodyTextKey()));
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchOpenBreachFortificationPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_OPEN_BREACH_FORTIFICATION_HEAD"));
	gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Popups/OpenBreachFortification.dds", 300, 200);

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_OPEN_BREACH_FORTIFICATION_BODY"));
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchResupplyShipsPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER) 
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvPlayer& kEuropePlayer = GET_PLAYER(kPlayer.getParent());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	bool bInEurope = (pUnit->getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);
	int iMaxPercentCannon = 100;
	int iMaxPercentMunition = 100;
	int iNumShips = 0;
	int iCannonReserve, iAmmunitionReserve, iTempCannons;
	int iTotalCannon = 0;
	int iTotalMunition = 0;
	int iMaxCannon = 0;
	int iMaxMunition = 0;
	int iStatePopup = info.getData3();
	int iGold = pUnit->getMissingEquipementPrice();

	if (iStatePopup == 5)
	{
		gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_BUY_MISSING_EQUIPMENT_TITLE"));

		if (iGold == -1) 
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_BUY_MISSING_EQUIPMENT_EUROPE_PB_BODY"));
		} 
		else 
		{
			if (kPlayer.getGold() >= iGold) 
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_BUY_MISSING_EQUIPMENT_OK_BODY", iGold));
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 6, WIDGET_GENERAL);
			} 
			else 
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_BUY_MISSING_EQUIPMENT_PB_BODY", iGold, kPlayer.getGold()));
			}
		}

		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_CLOSED"), NULL, -1, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

		return true;
	}

	if (!bInEurope)
	{
		CvCity* pCity = kPlayer.getCity(info.getData2());
		if (pCity == NULL) 
		{
			return false;
		}
		iCannonReserve = pCity->getYieldStored(YIELD_CANNON);
		iAmmunitionReserve = pCity->getYieldStored(YIELD_AMMUNITION);

		CvSelectionGroup* pGroup = pUnit->getGroup();
		if (pGroup != NULL)
		{
			CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
			while (pUnitNode != NULL)
			{
				CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pGroup->nextUnitNode(pUnitNode);
				if (pLoopUnit != NULL) 
				{
					if (pLoopUnit->IsSelected()) 
					{
						iTempCannons = pLoopUnit->getMaxCannon();
						if (pLoopUnit->getUnitInfo().isMechUnit() && iTempCannons > 0) 
						{
							iNumShips++;
							iTotalCannon += pLoopUnit->getNbCannon();
							iMaxCannon += iTempCannons;
							iTotalMunition += pLoopUnit->getMunition();
							iMaxMunition += pLoopUnit->getMaxMunition();
						}
					}
				}
			}
		}

		if ( iNumShips == 0 || iMaxCannon == 0 || iMaxMunition == 0)
		{
			return false;
		}

		if (iMaxCannon - iTotalCannon > iCannonReserve) 
		{
			iMaxPercentCannon = (iCannonReserve+iTotalCannon)*100/iMaxCannon;
		}

		if (iMaxMunition - iTotalMunition > iAmmunitionReserve) 
		{
			iMaxPercentMunition = (iAmmunitionReserve+iTotalMunition)*100/iMaxMunition;
		}
	} 
	else 
	{
		iNumShips = 1;
		iTotalCannon = pUnit->getNbCannon();
		iTotalMunition = pUnit->getMunition();
		iMaxCannon =  pUnit->getMaxCannon();
		iMaxMunition = pUnit->getMaxMunition();
		iCannonReserve = kPlayer.getEuropeWarehouseYield(YIELD_CANNON);
		iAmmunitionReserve = kPlayer.getEuropeWarehouseYield(YIELD_AMMUNITION);

		if (iMaxCannon - iTotalCannon > iCannonReserve)
		{
			iMaxPercentCannon = (iCannonReserve+iTotalCannon)*100/iMaxCannon;
		}

		if (iMaxMunition - iTotalMunition > iAmmunitionReserve) 
		{
			iMaxPercentMunition = (iAmmunitionReserve+iTotalMunition)*100/iMaxMunition;
		}
	}

	int iCurrentCannonValue = iMaxCannon > 0 ? iTotalCannon * 100 / iMaxCannon : 0;
	int iValueCannonButton = 3;
	int iCurrentAmmunitionValue = iMaxMunition > 0 ? iTotalMunition * 100 / iMaxMunition : 0;
	int iValueAmmunitionButton = 3;

	switch(iStatePopup)
	{
	case 1:
		iValueCannonButton = 1;
		iCurrentCannonValue = iMaxPercentCannon;
		break;
	case 2:
		iValueAmmunitionButton = 2;
		iCurrentAmmunitionValue = iMaxPercentMunition;
		break;
	case 3:
		iCurrentCannonValue = iMaxPercentCannon;
		iCurrentAmmunitionValue = iMaxPercentMunition;
		break;
	default:
		iValueCannonButton = 1;
		iValueAmmunitionButton = 2;
		break;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_SCREEN_HEADER_RESUPPLY_CANNON"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_SCREEN_RESUPPLY_CANNON", iNumShips, iTotalCannon, iTotalMunition));

	if (bInEurope && kPlayer.getEuropeWarehouseCapacity() > 0) 
	{
		if (iCurrentCannonValue != 100 && iCannonReserve == 0)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_RESUPPLY_SHIP_WARNING", GC.getYieldInfo(YIELD_CANNON).getDescription()));
		}

		if (iCurrentAmmunitionValue != 100 && iAmmunitionReserve == 0) 
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_RESUPPLY_SHIP_WARNING", GC.getYieldInfo(YIELD_AMMUNITION).getDescription()));
		}
	}

	if (!bInEurope || kPlayer.getEuropeWarehouseCapacity() > 0) 
	{
		gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"",  GC.getYieldInfo(YIELD_CANNON).getButton(), -1, WIDGET_HELP_YIELD, YIELD_CANNON, -1, false);
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_RESUPPLY_SHIPS_ROW"));
		gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 1, L"", iCurrentCannonValue, 5, iMaxPercentCannon, 0);
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_MAX_VALUE"), NULL, iValueCannonButton, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
		gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", GC.getYieldInfo(YIELD_AMMUNITION).getButton(), -1, WIDGET_HELP_YIELD, YIELD_AMMUNITION, -1, false);
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_RESUPPLY_SHIPS_ROW"));
		gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 2, L"", iCurrentAmmunitionValue, 5, iMaxPercentMunition, 0);
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_MAX_VALUE"), NULL, iValueAmmunitionButton, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupEndLayout(pPopup);

		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_RESUPPLY_SHIP_LOAD_EQUIPMENT"), NULL, 4, WIDGET_GENERAL);
	}

	if (bInEurope) 
	{
		if (iGold > 0 || iGold == -1) 
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_RESUPPLY_SHIP_BUY_EQUIPMENT"), NULL, 5, WIDGET_GENERAL);
		}

		if (iTotalCannon > 0 || iTotalMunition > 0)
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_COMMAND_UNLOAD_ALL"), NULL, 6, WIDGET_GENERAL);
		}
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_CLOSED"), NULL, -1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}



bool CvDLLButtonPopup::launchChooseStartSpecialistPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_SCREEN_HEADER_CHOOSE_START_SPECIALIST"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_SCREEN_BODY_CHOOSE_START_SPECIALIST"));


	for (int iI = 0; iI < GC.getNumUnitInfos(); iI++)
	{
		CvUnitInfo& kUnit = GC.getUnitInfo((UnitTypes) iI);
		if (kUnit.isStartingUnit())
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, kUnit.getDescription(),  kUnit.getButton(), iI, WIDGET_GENERAL, -1, -1, true, POPUP_LAYOUT_STRETCH, DLL_FONT_LEFT_JUSTIFY );
		}
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return true;
}

bool CvDLLButtonPopup::launchAskUseRelationPointsForImmigrationPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	UnitTypes eUnit = (UnitTypes)info.getData1();
	CvUnitInfo& kUnit = GC.getUnitInfo(eUnit);
	short sTotalRelationPoints = kPlayer.getImmigrationRelationPoints();
	short sRequiredPoints = kPlayer.getImmigrationRelationPointRequired(eUnit);

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_ASK_USE_RELATION_POINTS_FOR_IMMIGRATION_HEADER"));

	if (sTotalRelationPoints >= sRequiredPoints)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ASK_USE_RELATION_POINTS_FOR_IMMIGRATION_BODY_AVAILABLE", sTotalRelationPoints, sRequiredPoints, kUnit.getDescription()));

		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1, WIDGET_GENERAL);
	}
	else
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ASK_USE_RELATION_POINTS_FOR_IMMIGRATION_BODY_UNAVAILABLE", kUnit.getDescription(), sRequiredPoints - sTotalRelationPoints));
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_CANCEL"), NULL, 0, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
	return true;
}

bool CvDLLButtonPopup::launchChangeMinGoldToConserveForTradeRoutePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvTradeRoute* pTradeRoute = kPlayer.getTradeRoute(info.getData1());
	if (pTradeRoute == NULL)
	{
		return false;
	}
	int iActualGoldAmountToAlwaysConserve = pTradeRoute->getGoldAmountToAlwaysConserve();
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_CHANGE_MIN_GOLD_TO_CONSERVE_FOR_TRADE_ROUTE_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup,  gDLL->getText("TXT_KEY_POPUP_CHANGE_MIN_GOLD_TO_CONSERVE_FOR_TRADE_ROUTE_BODY", iActualGoldAmountToAlwaysConserve));
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 0, L"", iActualGoldAmountToAlwaysConserve, 100, 100000, 0);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_VALIDATE"), NULL, 1);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_KEEP_PREVIOUS_VALUE"), NULL, 0);	
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchRobUnitPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	if (pUnit == NULL)
	{
		return false;
	}
	CvUnit* pAttackUnit = kPlayer.getUnit(info.getData2());
	if (pAttackUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();
	if (pPlot == NULL)
	{
		return false;
	}
	CvWStringBuffer szBuffer1, szBuffer2, szBuffer3, szTempBuffer;
	CvWString szTempBuffer2;
	szBuffer1.clear();
	szTempBuffer2.clear();
	UnitTypes eSailor = (UnitTypes)pUnit->getUnitSailorType();
	bool bEmpty1 = false, bEmpty2 = false;
	if (!pUnit->hasTempCrew())
	{
		if (eSailor == UNIT_SAILOR_EXPERT)
		{
			szBuffer1.assign(gDLL->getText("TXT_KEY_EUROPE_SCREEN_TACTICAL_INFORMATION_CREW_EXPERIENCED"));
		}
		else
		{
			szBuffer1.assign(gDLL->getText("TXT_KEY_EUROPE_SCREEN_TACTICAL_INFORMATION_CREW_BEGINNER"));
		}
		for (int iI = 0; iI < GC.getNumPromotionInfos(); ++iI)
		{
			if (!GC.getPromotionInfo((PromotionTypes)iI).isGraphicalOnly() && pUnit->isHasPromotion((PromotionTypes)iI))
			{
				szTempBuffer2.Format(CvWString::format(L" <img=%S size=16></img>", GC.getPromotionInfo((PromotionTypes)iI).getButton()));
				szBuffer1.append(szTempBuffer2);
			}
		}
	}
	else
	{
		szBuffer1.assign(gDLL->getText("TXT_KEY_MAIN_MENU_NONE"));
	}
	szBuffer2.clear();

	int iTreasure = 0;
	szTempBuffer2.clear();
	CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
	while(pUnitNode != NULL)
	{
		CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);
		CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
		if (pTranportUnit == pUnit )
		{
			if (pCargoUnit->getProfession() != NO_PROFESSION)
			{
				szTempBuffer2.Format(L"<img=%S size=16></img>", pCargoUnit->getButton());
				szBuffer2.append(szTempBuffer2);
			}
			if ( pCargoUnit->getUnitInfo().isTreasure())
			{
				iTreasure += pCargoUnit->getYieldStored();
			}
		}
	}

	if (szBuffer2.isEmpty())
	{
		szBuffer2.append(gDLL->getText("TXT_KEY_MAIN_MENU_NONE"));
		bEmpty1 = true;
	}

	szBuffer3.clear();
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (pUnit->getNewCargoYield(eYield) > 0)
		{
			if (!szBuffer3.isEmpty())
			{
				szBuffer3.append(CvWString::format(L","));
			}
			szBuffer3.append(CvWString::format(L" %d%c", pUnit->getNewCargoYield(eYield), GC.getYieldInfo(eYield).getChar()));
		}
	}
	if (szBuffer3.isEmpty())
	{
		szBuffer3.append(gDLL->getText("TXT_KEY_MAIN_MENU_NONE"));
		bEmpty2 = true;
	}

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_HEADER"));
	if (iTreasure > 0)
	{
		szBuffer3.append(gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_HELP_TREASURE", iTreasure));
	}
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_BODY", szBuffer1.getCString(), szBuffer2.getCString(), szBuffer3.getCString()));

	if (iTreasure > 0)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_ANSWER_RECOMMENDED_1"), NULL, 1, WIDGET_GENERAL);
	}
	else
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_ANSWER_1"), NULL, 1, WIDGET_GENERAL);
	}
	if (bEmpty1 && bEmpty2)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_ANSWER_2_BIS"), NULL, 3, WIDGET_GENERAL);
	}
	else
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_ANSWER_2"), NULL, 2, WIDGET_GENERAL);
	}
	//gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_ANSWER_3"), NULL, 3, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchRobUnitCrewPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	CvUnit* pAttackUnit = kPlayer.getUnit(info.getData2());
	if (pUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();
	if (pPlot == NULL)
	{
		return false;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CREW_HEADER"));
	int iAnswer = info.getData3();
	if (iAnswer > -1)
	{
		if (iAnswer == 1)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CREW_BODY_ANSWER1"));
		}
		else
		{
			if (iAnswer == 2)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CREW_BODY_ANSWER2"));
			}
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CREW_BODY_ANSWER3"));
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_CONTINUE"), NULL, 3, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
		return true;
	}

	CvWStringBuffer szBuffer1;
	CvWString szTempBuffer2;
	szBuffer1.clear();
	szTempBuffer2.clear();

	UnitTypes eSailor = (UnitTypes)pUnit->getUnitSailorType();
	if (eSailor == UNIT_SAILOR_EXPERT)
	{
		szBuffer1.assign(gDLL->getText("TXT_KEY_EUROPE_SCREEN_TACTICAL_INFORMATION_CREW_EXPERIENCED"));
	}
	else
	{
		szBuffer1.assign(gDLL->getText("TXT_KEY_EUROPE_SCREEN_TACTICAL_INFORMATION_CREW_BEGINNER"));
	}
	for (int iI = 0; iI < GC.getNumPromotionInfos(); ++iI)
	{
		if (!GC.getPromotionInfo((PromotionTypes)iI).isGraphicalOnly() && pUnit->isHasPromotion((PromotionTypes)iI))
		{
			szTempBuffer2.Format(CvWString::format(L" <img=%S size=16></img>", GC.getPromotionInfo((PromotionTypes)iI).getButton()));
			szBuffer1.append(szTempBuffer2);
		}
	}
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CREW_BODY", szBuffer1.getCString()));

	gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CREW_GOLD"));
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 0, L"", 0, 200, kPlayer.getGold(), 0);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupEndLayout(pPopup);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CREW_REFUSE"), NULL, 2, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchRobUnitPassengersPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	CvUnit* pAttackUnit = kPlayer.getUnit(info.getData2());
	CLLNode<IDInfo>* pUnitNode;
	if (pUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();
	CvPlot* pAttPlot = pAttackUnit != NULL ? pAttackUnit->plot() : NULL;
	if (pPlot == NULL)
	{
		return false;
	}
	if ( pAttackUnit != NULL && pAttPlot == NULL)
	{
		return false;
	}
	if (info.getData3() >= 2)
	{//Inform Player that he can't transfert units
		gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_ERROR_HEADER"));
		if (info.getData3() == 2)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_ERROR_THEIR_SHIP"));
		}
		if (info.getData3() == 3)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_ERROR_OUR_SHIP"));
		}

		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, -10, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
		return true;
	}
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_HEADER"));

	if (info.getData3() == 1)
	{//We ask to player which units he wishes
		int iNumUnit = 0;
		int iAcceptUnit = 0;
		if (pAttackUnit != NULL)
		{
			int iSpace = pAttackUnit->getSpaceNewCargo()/30;
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_PASSENGERS_BODY2", iSpace));
			//We show all passenger in our ship
			pUnitNode = pAttPlot->headUnitNode();
			while(pUnitNode != NULL)
			{
				CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pAttPlot->nextUnitNode(pUnitNode);
				CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
				if (pTranportUnit == pAttackUnit && pCargoUnit->getProfession() != NO_PROFESSION)
				{
					gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
					gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", pCargoUnit->getButton(), -10, WIDGET_SHIP_CARGO, pCargoUnit->getID(), -1, false);
					gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, pCargoUnit->getID(), WIDGET_GENERAL, POPUP_LAYOUT_RIGHT);
					gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_OUR_PASSENGERS"), pCargoUnit->getID());
					gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, true, pCargoUnit->getID());
					gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
					iNumUnit++;
				}
			}
			if ( iNumUnit == 0)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_NO_PASSENGER"));
			}
			else
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_SELECT_PASSENGERS"));
			}
			iSpace = pUnit->getSpaceNewCargo()/30;
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_PASSENGERS_BODY3", iSpace));
		}
		else
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_PASSENGERS_BODY"));
		}
		int iTempPassenger = iNumUnit;
		//We show all passenger in the accosted ship
		pUnitNode = pPlot->headUnitNode();
		while(pUnitNode != NULL)
		{
			CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);
			CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
			if (pTranportUnit == pUnit && pCargoUnit->getProfession() != NO_PROFESSION)
			{
				if (pCargoUnit->getOriginalOwner() != ePlayer)
				{//If it's a unit of defensive player
					gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
					if (pCargoUnit->getProfession() == PROFESSION_PRISONER)
					{
						gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", GC.getUnitInfo(pCargoUnit->getUnitType()).getButton(), -10, WIDGET_SHIP_CARGO, pCargoUnit->getID(), -1, false);
						gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, pCargoUnit->getID(), WIDGET_GENERAL, POPUP_LAYOUT_RIGHT);
						gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_PRISONER"), pCargoUnit->getID());
						gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, false, pCargoUnit->getID());
					}
					else
					{
						gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", pCargoUnit->getButton(), -10, WIDGET_SHIP_CARGO, pCargoUnit->getID(), -1, false);
						if (pCargoUnit->getProposePrice() >= 0)
						{
							gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, pCargoUnit->getID(), WIDGET_GENERAL, POPUP_LAYOUT_RIGHT);
							if (pCargoUnit->getProposePrice() == 0)
							{
								gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_PASSENGERS_ACCEPT_FREE"), pCargoUnit->getID());
							}
							else
							{
								gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_PASSENGERS_ACCEPT", pCargoUnit->getProposePrice()), pCargoUnit->getID());
							}
							gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, false, pCargoUnit->getID());
							iAcceptUnit++;
						}
						else
						{
							gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_PASSENGERS_REFUSE", GC.getProfessionInfo(pCargoUnit->getProfession()).getDescription()));
						}
					}
					gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
					iNumUnit++;
				}
				else
				{//If it's a one of our units (may be prisoner)
					if (pAttackUnit != NULL)
					{
						gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
						gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", pCargoUnit->getButton(), -10, WIDGET_SHIP_CARGO, pCargoUnit->getID(), -1, false);
						gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, pCargoUnit->getID(), WIDGET_GENERAL, POPUP_LAYOUT_RIGHT);
						if (pCargoUnit->getProfession() != PROFESSION_PRISONER)
						{
							gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_PASSENGERS_OUR_NATION"), pCargoUnit->getID());
						}
						else
						{
							gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PRISONERS_FREEDOM"), pCargoUnit->getID());
						}
						gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, false, pCargoUnit->getID());
						gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
						iNumUnit++;
					}
				}
			}
		}
		if (pAttackUnit != NULL)
		{
			if (iNumUnit - iTempPassenger == 0)
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_NO_PASSENGER"));
			}
			else
			{
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_VALIDATE_SELECT_PASSENGERS2"));
			}
			bool bDefCargo = pUnit->getOnlyNewCargo() > 0;
			bool bAttCargo = pAttackUnit->getOnlyNewCargo() > 0;
			if (!bDefCargo && !bAttCargo)
			{	//NO CARGO
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_ANSWER4"), NULL, -8, WIDGET_GENERAL);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_ANSWER3"), NULL, -7, WIDGET_GENERAL);
			}
			else
			{
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_ANSWER4"), NULL, -5, WIDGET_GENERAL);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_PEDIA_SCREEN_BACK"), NULL, -6, WIDGET_GENERAL);
			}
		}
		else
		{
			if (iNumUnit == 0)
			{
				gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_END, pUnit->getID(), -1, -1);
				return false;
			}
			if (iAcceptUnit > 0)
			{
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_ANSWER3"), NULL, -3, WIDGET_GENERAL);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_ANSWER2"), NULL, -2, WIDGET_GENERAL);
			}
			else
			{
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, -3, WIDGET_GENERAL);
			}
		}
		gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);
		return true;
	}

	//In this part, we can enrol passengers and propose some golds
	int iNumUnit = 0, iNumFree = 0;
	int iTotalProposePrice = 0;
	pUnitNode = pPlot->headUnitNode();
	while(pUnitNode != NULL)
	{
		CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);
		CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
		if (pTranportUnit == pUnit && pCargoUnit->getProfession() != NO_PROFESSION)
		{
			iTotalProposePrice += pCargoUnit->getProposePrice();
			if (pCargoUnit->getOriginalOwner() == kPlayer.getID())
			{
				iNumFree++;
			}
			iNumUnit++;
		}
	}
	if (iNumUnit == 0)
	{
		gDLL->sendPlayerAction(kPlayer.getID(), PLAYER_ACTION_ROB_UNIT_END, pUnit->getID(), -1, -1);
		return false;
	}
	if (iNumUnit > iNumFree)
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_BODY"));
	}
	else
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_FREEDOM_BODY"));
	}

	// We show the passengers list
	pUnitNode = pPlot->headUnitNode();
	while(pUnitNode != NULL)
	{
		CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);
		CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
		if (pTranportUnit == pUnit && pCargoUnit->getProfession() != NO_PROFESSION)
		{
			gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
			if (pCargoUnit->getOriginalOwner() != kPlayer.getID())
			{
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", pCargoUnit->getButton(), -10, WIDGET_SHIP_CARGO, pCargoUnit->getID(), -1, false);
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, GC.getProfessionInfo(pCargoUnit->getProfession()).getDescription());
				gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, pCargoUnit->getID(), L"", pCargoUnit->getProposePrice(), 200, kPlayer.getGold() - iTotalProposePrice + pCargoUnit->getProposePrice(), 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, pCargoUnit->getID(), WIDGET_GENERAL);
			}
			else
			{
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", GC.getUnitInfo(pCargoUnit->getUnitType()).getButton(), -10, WIDGET_SHIP_CARGO, pCargoUnit->getID(), -1, false);
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PRISONERS_FREEDOM"));
			}
			gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
		}
	}

	if (iNumUnit > iNumFree)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_ANSWER1"), NULL, -1, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_PASSENGERS_ANSWER2"), NULL, -2, WIDGET_GENERAL);
	}
	else
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, -3, WIDGET_GENERAL);
	}
	if (pAttackUnit != NULL)
	{
		bool bDefCargo = pUnit->getOnlyNewCargo() > 0;
		bool bAttCargo = pAttackUnit->getOnlyNewCargo() > 0;
		if (!bDefCargo && !bAttCargo)
		{	//NO CARGO so we can't come back
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_ANSWER3"), NULL, -7, WIDGET_GENERAL);
		}
		else
		{
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_PEDIA_SCREEN_BACK"), NULL, -4, WIDGET_GENERAL);
		}
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchRobUnitCargoPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	CvUnit* pAttackUnit = kPlayer.getUnit(info.getData2());
	if (pUnit == NULL)
	{
		return false;
	}
	if (pAttackUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();
	if (pPlot == NULL)
	{
		return false;
	}
	//Manage goods part
	if (info.getData3() == -1)
	{
		gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_HEADER"));
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_BODY1", pAttackUnit->getNewCargo(), pAttackUnit->getUnitInfo().getCargoNewSpace()));
		//We show all goods to transfer to the accosted ship
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
		{
			YieldTypes eYield = (YieldTypes) iYield;
			CvYieldInfo& kYield = GC.getYieldInfo(eYield);
			int iNewCargo = pAttackUnit->getNewCargoYield(eYield);
			if (iNewCargo > 0)
			{
				gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", kYield.getButton(), iYield+(0*NUM_YIELD_TYPES), WIDGET_HELP_ROB_YIELD, eYield, 1);
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_CAPACITY", iNewCargo, kYield.getChar()));
				gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, iYield+(1*NUM_YIELD_TYPES), L"", 0, 5, std::min(iNewCargo, pUnit->getSpaceNewCargo()), 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, iYield+(1*NUM_YIELD_TYPES), WIDGET_GENERAL);
				gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
			}
		}
		if (pAttackUnit->getOnlyNewCargo() == 0)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_NO_CARGO"));
		}

		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_BODY2", pUnit->getNewCargo(), pUnit->getUnitInfo().getCargoNewSpace()));

		//We show all goods we can transfer to our ship
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
		{
			YieldTypes eYield = (YieldTypes) iYield;
			CvYieldInfo& kYield = GC.getYieldInfo(eYield);
			int iNewCargo = pUnit->getNewCargoYield(eYield);
			if (iNewCargo > 0)
			{
				gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", kYield.getButton(), iYield+(2*NUM_YIELD_TYPES), WIDGET_HELP_ROB_YIELD, eYield, 2);
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_CAPACITY", iNewCargo, kYield.getChar()));
				gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, iYield+(3*NUM_YIELD_TYPES), L"", 0, 5, std::min(iNewCargo, pAttackUnit->getSpaceNewCargo()), 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, iYield+(3*NUM_YIELD_TYPES), WIDGET_GENERAL);
				gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
			}
		}
		if (pUnit->getOnlyNewCargo() == 0)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_NO_CARGO"));
		}
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_THROW"), NULL, -1, WIDGET_GENERAL);
	}
	else
	{//Throw overboard goods part
		gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_THROW_HEADER"));
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_THROW_BODY1", pAttackUnit->getNewCargo(), pAttackUnit->getUnitInfo().getCargoNewSpace()));
		//We show all goods of our ship we could throw overboard
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
		{
			YieldTypes eYield = (YieldTypes) iYield;
			CvYieldInfo& kYield = GC.getYieldInfo(eYield);
			int iNewCargo = pAttackUnit->getNewCargoYield(eYield);
			if (iNewCargo > 0)
			{
				gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", kYield.getButton(), iYield+(0*NUM_YIELD_TYPES), WIDGET_HELP_ROB_YIELD, eYield, 1);
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_CAPACITY", iNewCargo, kYield.getChar()));
				gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, iYield+(1*NUM_YIELD_TYPES), L"", 0, 5, iNewCargo, 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, iYield+(1*NUM_YIELD_TYPES), WIDGET_GENERAL);
				gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
			}
		}
		if (pAttackUnit->getOnlyNewCargo() == 0)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_NO_CARGO"));
		}

		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_THROW_BODY2", pUnit->getNewCargo(), pUnit->getUnitInfo().getCargoNewSpace()));

		//We show all goods of the accosted ship we could throw overboard
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
		{
			YieldTypes eYield = (YieldTypes) iYield;
			CvYieldInfo& kYield = GC.getYieldInfo(eYield);
			int iNewCargo = pUnit->getNewCargoYield(eYield);
			if (iNewCargo > 0)
			{
				gDLL->getInterfaceIFace()->popupStartHLayout(pPopup, 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, L"", kYield.getButton(), iYield+(2*NUM_YIELD_TYPES), WIDGET_HELP_ROB_YIELD, eYield, 2);
				gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_CAPACITY", iNewCargo, kYield.getChar()));
				gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, iYield+(3*NUM_YIELD_TYPES), L"", 0, 5, iNewCargo, 0);
				gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_MAIN_MENU_OK"), NULL, iYield+(3*NUM_YIELD_TYPES), WIDGET_GENERAL);
				gDLL->getInterfaceIFace()->popupEndLayout(pPopup);
			}
		}
		if (pUnit->getOnlyNewCargo() == 0)
		{
			gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CARGO_NO_CARGO"));
		}
	}
	bool bDefUnitCargo = (pUnit->getNewCargo(false) - pUnit->getOnlyNewCargo()) > 0;
	bool bAttUnitCargo = (pAttackUnit->getNewCargo(false) - pAttackUnit->getOnlyNewCargo()) > 0;
	if (!bDefUnitCargo && !bAttUnitCargo && info.getData3() == -1)
	{//NO UNIT so we can't come back
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_ANSWER3"), NULL, -3, WIDGET_GENERAL);
	}
	else
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_PEDIA_SCREEN_BACK"), NULL, -2, WIDGET_GENERAL);
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchRobUnitChoicePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	CvUnit* pAttackUnit = kPlayer.getUnit(info.getData2());
	CLLNode<IDInfo>* pUnitNode;
	if (pUnit == NULL)
	{
		return false;
	}
	if (pAttackUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();
	CvPlot* pAttPlot = pAttackUnit->plot();
	if (pPlot == NULL)
	{
		return false;
	}
	if (pAttPlot == NULL)
	{
		return false;
	}
	CvWStringBuffer szBuffer1, szBuffer2;
	CvWString szTempBuffer;
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_HEADER"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_OUR_SHIP_BODY", pAttackUnit->getNewCargo(), pAttackUnit->getUnitInfo().getCargoNewSpace()));
	szBuffer1.clear();
	szTempBuffer.clear();
	//We search all passengers of our ship
	pUnitNode = pAttPlot->headUnitNode();
	while(pUnitNode != NULL)
	{
		CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pAttPlot->nextUnitNode(pUnitNode);
		CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
		if (pTranportUnit == pAttackUnit && pCargoUnit->getProfession() != NO_PROFESSION)
		{
			szTempBuffer.Format(L" <img=%S size=25></img>", pCargoUnit->getButton());
			szBuffer1.append(szTempBuffer);
		}
	}
	if (!szBuffer1.isEmpty())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szBuffer1.getCString());
	}

	//We search all goods of our ship
	szBuffer2.clear();
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (pAttackUnit->getNewCargoYield(eYield) > 0)
		{
			if (!szBuffer2.isEmpty())
			{
				szBuffer2.append(CvWString::format(L","));
			}
			szBuffer2.append(CvWString::format(L" %d%c", pAttackUnit->getNewCargoYield(eYield), GC.getYieldInfo(eYield).getChar()));
		}
	}
	if (!szBuffer2.isEmpty())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szBuffer2.getCString());
	}
	if (szBuffer1.isEmpty() && szBuffer2.isEmpty())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_NO_CARGO"));
	}
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_THEIR_SHIP_BODY", pUnit->getNewCargo(), pUnit->getUnitInfo().getCargoNewSpace()));

	//We search all passengers of the accosted ship
	szBuffer1.clear();
	szTempBuffer.clear();
	pUnitNode = pPlot->headUnitNode();
	while(pUnitNode != NULL)
	{
		CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);
		CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
		if (pTranportUnit == pUnit && pCargoUnit->getProfession() != NO_PROFESSION)
		{
			szTempBuffer.Format(L" <img=%S size=25></img>", pCargoUnit->getButton());
			szBuffer1.append(szTempBuffer);
		}
	}
	if (!szBuffer1.isEmpty())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szBuffer1.getCString());
	}

	//We search all goods of the accosted ship
	szBuffer2.clear();
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (pUnit->getNewCargoYield(eYield) > 0)
		{
			if (!szBuffer2.isEmpty())
			{
				szBuffer2.append(CvWString::format(L","));
			}
			szBuffer2.append(CvWString::format(L" %d%c", pUnit->getNewCargoYield(eYield), GC.getYieldInfo(eYield).getChar()));
		}
	}
	if (!szBuffer2.isEmpty())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szBuffer2.getCString());
	}

	if (szBuffer1.isEmpty() && szBuffer2.isEmpty())
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_NO_CARGO"));
	}
	//Manage passengers
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_ANSWER1"), NULL, 1, WIDGET_GENERAL);
	//Manage goods
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_ANSWER2"), NULL, 2, WIDGET_GENERAL);
	//End of the plunder
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_ROB_UNIT_CHOICE_ANSWER3"), NULL, 3, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchTutorialDoanePopup0(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_TUTORIAL_PACK_FOUND_HEAD"));
	gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Popups/PackFound.dds", 300, 200);
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_TUTORIAL_PACK_FOUND_BODY"));

	gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, 1, WIDGET_GENERAL, POPUP_LAYOUT_LEFT);
	gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_DISABLE_TUTORIAL_DOANE"),1);
	gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, 0, 1);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_CONTINUE"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}
bool CvDLLButtonPopup::launchTutorialDoanePopup1(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_TUTORIAL_INFLUENCE_HEAD"));
	gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Popups/Negociation_ZI_Popup.dds", 300, 200);
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_TUTORIAL_INFLUENCE_BODY"));

	gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, 1, WIDGET_GENERAL, POPUP_LAYOUT_LEFT);
	gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_DISABLE_TUTORIAL_DOANE"),1);
	gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, 0, 1);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_CONTINUE"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}
bool CvDLLButtonPopup::launchTutorialDoanePopup2(CvPopup* pPopup, CvPopupInfo &info)
{
	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_TUTORIAL_KEPT_AUTO_IN_WARTIME_HEAD"));
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_TUTORIAL_KEPT_AUTO_IN_WARTIME_BODY"));

	gDLL->getInterfaceIFace()->popupCreateCheckBoxes(pPopup, 1, 1, WIDGET_GENERAL, POPUP_LAYOUT_LEFT);
	gDLL->getInterfaceIFace()->popupSetCheckBoxText(pPopup, 0, gDLL->getText("TXT_KEY_POPUP_DISABLE_TUTORIAL_DOANE"),1);
	gDLL->getInterfaceIFace()->popupSetCheckBoxState(pPopup, 0, 0, 1);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_SCREEN_CONTINUE"), NULL, 1, WIDGET_GENERAL);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return (true);
}

bool CvDLLButtonPopup::launchMakeTheFirstSeawayPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	FAssertMsg(pUnit != NULL, "pUnit is expected to exist");
	if (pUnit == NULL)
	{
		return false;
	}
	
	CvPlot* pUnitPlot = pUnit->plot();
	if (pUnitPlot == NULL)
	{
		return false;
	}

	int iLoop;
	CvCity* pCity = kPlayer.firstCity(&iLoop);
	FAssertMsg(pCity != NULL, "player should have at least one colony");
	if (pCity == NULL)
	{
		return false;
	}

	CvPlot* pBestPlot = pCity->getNearestOceanPlot(false);
	FAssertMsg(pBestPlot != NULL, "getNearestOceanPlot is expected to return a plot");
	if (pBestPlot == NULL)
	{
		return false;
	}

	int iCurrentDist = stepDistance(pBestPlot->getX_INLINE(), pBestPlot->getY_INLINE(), pUnitPlot->getX_INLINE(), pUnitPlot->getY_INLINE());
	int iMaxDistance = 3;
	bool bCloserToBestPlot = (iCurrentDist >= 0 && iCurrentDist < iMaxDistance);

	gDLL->getInterfaceIFace()->popupSetHeaderString(pPopup, gDLL->getText("TXT_KEY_POPUP_MAKE_THE_FIRST_SEAWAY_HEAD"));
	gDLL->getInterfaceIFace()->popupAddDDS(pPopup, "Art/Interface/Popups/Bandeau_Seaways_Auto.dds", 360, 100);
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_MAKE_THE_FIRST_SEAWAY_PART1_BODY"));
	if (bCloserToBestPlot)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_MAKE_THE_FIRST_SEAWAY_OPTION1"), NULL, 1, WIDGET_GENERAL);
	}
	else
	{
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_POPUP_MAKE_THE_FIRST_SEAWAY_PART2_BODY", pCity->getNameKey()));
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_MAKE_THE_FIRST_SEAWAY_OPTION2"), NULL, 2, WIDGET_GENERAL);
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_MAKE_THE_FIRST_SEAWAY_OPTION3", pCity->getNameKey()), NULL, 3, WIDGET_GENERAL);
	}

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchTradeRouteRadioQuantityPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());

	CvTradeRoute* pTradeRoute = kPlayer.getTradeRoute(info.getData1());
	if (pTradeRoute == NULL)
	{
		return false;
	}
	YieldTypes eYield = (YieldTypes) info.getData2();
	if (eYield == NO_YIELD)
	{
		return false;
	}
	TradeRouteRadioTypes eRadioType = (TradeRouteRadioTypes) info.getData3();

	int iActualValue = pTradeRoute->getRadioOption(eYield, eRadioType).iQuantity;
	bool hadValue = iActualValue > 0;
	int iMinimalValue;
	if (!hadValue)
	{
		iActualValue = 10;
	}
	CvWString szText;
	switch(eRadioType)
	{
	case ENSURE_THE_NEEDS:
		szText = gDLL->getText("TXT_KEY_TRADE_ROUTE_RADIO_QUANTITY_ENSURE_THE_NEEDS_BODY", GC.getYieldInfo(eYield).getTextKeyWide(), pTradeRoute->getDestinationCityNameKey());
		iMinimalValue = 0;
		break;
	case SEND_FIXED_QUANTITY:
		szText = gDLL->getText("TXT_KEY_TRADE_ROUTE_RADIO_QUANTITY_BODY", GC.getYieldInfo(eYield).getTextKeyWide());
		iMinimalValue = 1;
		break;
	default:
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szText);
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 0, L"", iActualValue, 10, 1000, iMinimalValue);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_VALIDATE"), NULL, 1);
	if (hadValue)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_KEEP_PREVIOUS_VALUE"), NULL, 0);	
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchTradeRouteRadioEuropeanQuantityPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());

	CvTradeRoute* pTradeRoute = kPlayer.getTradeRoute(info.getData1());
	if (pTradeRoute == NULL || info.getData1() != kPlayer.getCurrentTradeRouteId())
	{
		return false;
	}
	YieldTypes eYield = (YieldTypes) info.getData2();
	if (eYield == NO_YIELD)
	{
		return false;
	}
	TradeRouteRadioEuropeanTypes eRadioType = (TradeRouteRadioEuropeanTypes)info.getData3();
	//

	int iActualValue = pTradeRoute->getEuropeanRadioOption(eYield, eRadioType).iQuantity;
	//int iActualValuePerTurn = pTradeRoute->getEuropeanRadioOption(eYield, eRadioType).iQuantityPerTurn;
	bool hadValue = iActualValue > 0;
	int iMinimalValue = 1;
	if (!hadValue)
	{
		iActualValue = 15;
		//iActualValuePerTurn = 0;
	}	

	if (eRadioType == BUY_UNTIL_QUANTITY_REACHED)
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TRADE_ROUTE_EUROPEAN_RADIO_UNTIL_REACH_QUANTITY_BODY", GC.getYieldInfo(eYield).getTextKeyWide(), pTradeRoute->getCityNameKey()));
	else {
		gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TRADE_ROUTE_EUROPEAN_RADIO_QUANTITY_BODY", GC.getYieldInfo(eYield).getTextKeyWide()));
	}
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 0, L"", iActualValue, 10, 1000, iMinimalValue);

	/*gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TRADE_ROUTE_EUROPEAN_RADIO_QUANTITY_PER_TURN_BODY", GC.getYieldInfo(eYield).getTextKeyWide()));
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 1, L"", iActualValuePerTurn, 10, 200, 0);*/

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_VALIDATE"), NULL, 1);
	if (hadValue)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_KEEP_PREVIOUS_VALUE"), NULL, 0);	
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}


bool CvDLLButtonPopup::launchTradeRouteCheckBoxQuantityPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());

	CvTradeRoute* pTradeRoute = kPlayer.getTradeRoute(info.getData1());
	if (pTradeRoute == NULL || info.getData1() != kPlayer.getCurrentTradeRouteId())
	{
		return false;
	}
	YieldTypes eYield = (YieldTypes) info.getData2();
	if (eYield == NO_YIELD)
	{
		return false;
	}
	TradeRouteCheckBoxTypes eCheckBoxType = (TradeRouteCheckBoxTypes) info.getData3();

	int iActualValue = pTradeRoute->getCheckBoxOption(eYield, eCheckBoxType).iQuantity;
	bool hadValue = iActualValue > 0;
	int iMinimalValue;
	if (!hadValue)
	{
		iActualValue = 10;
	}
	CvWString szText;
	switch(eCheckBoxType)
	{
	case DO_NOT_EXCEED:
	case ALWAYS_KEEP:
	case MAINTAIN_QUANTITY:
		szText = gDLL->getText("TXT_KEY_TRADE_ROUTE_RADIO_QUANTITY_BODY", GC.getYieldInfo(eYield).getTextKeyWide());
		iMinimalValue = 1;
		break;
	default:
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, szText);
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 0, L"", iActualValue, 10, 1000, iMinimalValue);

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_VALIDATE"), NULL, 1);
	if (hadValue)
	{
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_KEEP_PREVIOUS_VALUE"), NULL, 0);	
	}
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}


bool CvDLLButtonPopup::launchTradeRouteAddResourcePopup(CvPopup* pPopup, CvPopupInfo &info)
{
	CvPlayer& kPlayer = GET_PLAYER(GC.getGameINLINE().getActivePlayer());

	CvTradeRoute* pTradeRoute = kPlayer.getTradeRoute(info.getData1());
	if (pTradeRoute == NULL || info.getData1() != kPlayer.getCurrentTradeRouteId())
	{
		return false;
	}
	TradeTypes eType = (TradeTypes) info.getData2();
	if (eType == NO_TRADE)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TRADE_ROUTE_ADD_RESOURCE_POPUP_BODY"));

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (GC.getYieldInfo(eYield).isCargo() && !kPlayer.isHasYieldUnknown(eYield))
		{
			CvWString szTempBuffer = CvWString::format(L"%s", GC.getYieldInfo(eYield).getDescription());
			gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szTempBuffer, GC.getYieldInfo(eYield).getButton(), iYield, WIDGET_GENERAL);
		}
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_NEVER_MIND"), ARTFILEMGR.getInterfaceArtInfo("INTERFACE_BUTTONS_CANCEL")->getPath(), -1, WIDGET_GENERAL);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchChooseGoodyPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	GoodyTypes eGoody = (GoodyTypes) info.getData1();
	if (eGoody == NO_GOODY)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText(GC.getGoodyInfo(eGoody).getAnnounceTextKey()));
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_YES"), NULL, 1);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_POPUP_NO"), NULL, 0);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchSelectYieldAmountPopup(CvPopup* pPopup, CvPopupInfo &info)
{
	YieldTypes eYield = (YieldTypes) info.getData1();
	if (eYield == NO_YIELD)
	{
		return false;
	}

	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvUnit* pUnit = kPlayer.getUnit(info.getData2());
	if (pUnit == NULL)
	{
		return false;
	}

	if (pUnit->getUnitTravelState() != UNIT_TRAVEL_STATE_IN_EUROPE)
	{
		FAssert(pUnit->getUnitTravelState() == NO_UNIT_TRAVEL_STATE);
		if (!pUnit->canDoCommand((CommandTypes) info.getData3(), info.getData1(), -1))
		{
			return false;
		}
	}

	int iMaxAmount = info.getOption1() ? pUnit->getMaxLoadYieldAmount(eYield) : pUnit->getYieldStored();
	if (iMaxAmount <= 0)
	{
		return false;
	}

	int i = 1;
	if (eYield == YIELD_AMMUNITION)
	{
		i = 10;
	}
	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_SELECT_YIELD_POPUP", GC.getYieldInfo(eYield).getTextKeyWide()));
	gDLL->getInterfaceIFace()->popupCreateSpinBox(pPopup, 0, L"", iMaxAmount, i, iMaxAmount*i, 0);
	gDLL->getInterfaceIFace()->popupLaunch(pPopup, true, POPUPSTATE_IMMEDIATE);

	return true;
}

bool CvDLLButtonPopup::launchTalkNativesPopup(CvPopup* pPopup, CvPopupInfo& info)
{
	PlayerTypes ePlayer = GC.getGameINLINE().getActivePlayer();
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(ePlayer);
	CvUnit* pUnit = kPlayer.getUnit(info.getData1());
	if (pUnit == NULL)
	{
		return false;
	}
	CvPlot* pPlot = pUnit->plot();
	if (pPlot == NULL)
	{
		return false;
	}
	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupSetBodyString(pPopup, gDLL->getText("TXT_KEY_TALK_NATIVES_POPUP", pCity->getNameKey()));

	int iNumActions = 0;
	if (pUnit->canSpeakWithChief(pUnit->plot()))
	{
		++iNumActions;
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_TALK_NATIVES_POPUP_CHIEF"), NULL, COMMAND_SPEAK_WITH_CHIEF);
	}

	if (pUnit->canLearn())
	{
		++iNumActions;
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_TALK_NATIVES_POPUP_LEARN"), NULL, COMMAND_LEARN);
	}

	if (pUnit->canEstablishMission())
	{
		++iNumActions;
		CvWString szText = gDLL->getText("TXT_KEY_TALK_NATIVES_POPUP_MISSION");
		szText += L" (" + gDLL->getText("TXT_KEY_TALK_NATIVES_POPUP_MISSION2", std::min(100, pUnit->getMissionarySuccessPercent())) + L")";
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, szText, NULL, COMMAND_ESTABLISH_MISSION);
	}

	if (pUnit->canTradeYield(pUnit->plot()))
	{
		++iNumActions;
		gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_TALK_NATIVES_POPUP_TRADE"), NULL, COMMAND_YIELD_TRADE);
	}

	if (iNumActions == 0)
	{
		return false;
	}

	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_TALK_NATIVES_POPUP_NOTHING"), NULL, -1);
	gDLL->getInterfaceIFace()->popupAddGenericButton(pPopup, gDLL->getText("TXT_KEY_TALK_NATIVES_POPUP_NEVER"), NULL, NUM_COMMAND_TYPES);

	gDLL->getInterfaceIFace()->popupLaunch(pPopup, false, POPUPSTATE_IMMEDIATE);

	return true;
}
