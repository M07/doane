#pragma once

// CvDLLButtonPopup.h

#ifndef CIV4_DLL_BUTTON_POPUP_H
#define CIV4_DLL_BUTTON_POPUP_H

//#include "CvEnums.h"
#include "CvPopupInfo.h"
#include "CvPopupReturn.h"

class CvPopup;
class CvDLLButtonPopup
{

public:
	CvDLLButtonPopup();
	virtual ~CvDLLButtonPopup();

	DllExport static CvDLLButtonPopup& getInstance();
	DllExport static void freeInstance();

	DllExport virtual void OnOkClicked(CvPopup* pPopup, PopupReturn *pPopupReturn, CvPopupInfo &info);
	DllExport virtual void OnAltExecute(CvPopup& popup, const PopupReturn& popupReturn, CvPopupInfo &info);
	DllExport virtual void OnEscape(CvPopup& pPopup, CvPopupInfo &info);
	DllExport virtual void OnFocus(CvPopup* pPopup, CvPopupInfo &info);

	DllExport bool launchButtonPopup(CvPopup* pPopup, CvPopupInfo &info);

	static bool launchOnlyOncePopup(ButtonPopupTypes eButtonPopup);
	static bool launchOnlyOnceByTurnPopup(ButtonPopupTypes eButtonPopup);

private:

	static CvDLLButtonPopup* m_pInst;

	bool launchTextPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchVictoryPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchProductionPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchChooseYieldBuildPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchEducationPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRazeCityPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchAlarmPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchDeclareWarMovePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchConfirmCommandPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchConfirmTaskPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchLoadUnitPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchLoadCargoPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchLeadUnitPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchMainMenuPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchConfirmMenu(CvPopup *pPopup, CvPopupInfo &info);
	bool launchPythonScreen(CvPopup* pPopup, CvPopupInfo &info);
	bool launchMovie(CvPopup* pPopup, CvPopupInfo &info);
	bool launchPythonPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchDetailsPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchAdminPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchAdminPasswordPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchCancelDeal(CvPopup* pPopup, CvPopupInfo &info);
	bool launchExtendedGamePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchDiplomacyPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchAddBuddyPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchForcedDisconnectPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchPitbossDisconnectPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchKickedPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchEventPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchFreeColonyPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchChooseProfessionPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchManageHospitals(CvPopup* pPopup, CvPopupInfo &info);
	bool launchResupplyBuilderPackPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchUnloadNewCargoPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchChooseQuantityTradePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchChooseProfessionInEuropePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchChooseCityDestinationInEuropePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchValidateProfessionInEuropePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchEjectUnitToPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchAskSailorFormationPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchDestroyCityPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRemoveRessourcePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRepairShipPopup(CvPopup* pPopup, CvPopupInfo &info);

	bool launchArsenalManagementPopup(CvPopup* pPopup, CvPopupInfo &info);
	CvWString getArsenalYieldText(YieldTypes eYield, bool bCanbeProduced, bool bIsActive);

	bool launchLeadUnitExpPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchPurchaseEuropeUnitPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchCivicOptionPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchYieldProductionPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchPromotionPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchEmbarkOnShipPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchLoadCrewPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchGiveMapPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchTransfertMapToEuropePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRemoveAssignedUnitPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchNewAgreementPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchNewAgreementPart1Popup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchNewAgreementPart2Popup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchAutomaticRenewAgreementPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchEndOfAgreementPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchAutomaticAgreementByYieldsPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchWarehouseUpgradePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchAgreementListPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchSeedlingListPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchSeedlingDiscoveryGoodSituationPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchChangeCityAgreementPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchChooseAgreementsPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchChooseTradeRoutesPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchSendTradePropositionPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchProcessTradePropositionPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchStopTradePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchSellShipPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchStopSellShipPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRecruitCrewPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRecruitImmigrantPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchBuyShipUsedPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchBuyNewShipInEuropePopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchPurchaseBidPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchReceiveSeedlingPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRumDiscoveredPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchAchievementPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchFoundColonyPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRepayLoan(CvPopup* pPopup, CvPopupInfo &info);
	bool launchWhenNewEraPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchOpenBreachFortificationPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchResupplyShipsPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchChooseStartSpecialistPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchAskUseRelationPointsForImmigrationPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchChangeMinGoldToConserveForTradeRoutePopup(CvPopup* pPopup, CvPopupInfo &info);

	bool launchRobUnitPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRobUnitCrewPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRobUnitPassengersPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRobUnitCargoPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchRobUnitChoicePopup(CvPopup* pPopup, CvPopupInfo &info);	

	bool launchTutorialDoanePopup0(CvPopup* pPopup, CvPopupInfo &info);
	bool launchTutorialDoanePopup1(CvPopup* pPopup, CvPopupInfo &info);
	bool launchTutorialDoanePopup2(CvPopup* pPopup, CvPopupInfo &info);
	
	bool launchMakeTheFirstSeawayPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchTradeRouteRadioQuantityPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchTradeRouteRadioEuropeanQuantityPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchTradeRouteCheckBoxQuantityPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchTradeRouteAddResourcePopup(CvPopup* pPopup, CvPopupInfo &info);
	
	bool launchChooseGoodyPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchSelectYieldAmountPopup(CvPopup* pPopup, CvPopupInfo &info);
	bool launchTalkNativesPopup(CvPopup* pPopup, CvPopupInfo &info);
};

#endif
