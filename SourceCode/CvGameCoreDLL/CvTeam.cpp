// team.cpp

#include "CvGameCoreDLL.h"
#include "CvPlayerAI.h"
#include "CvPlayer.h"
#include "CvDefines.h"
#include "CvGameCoreUtils.h"
#include "CvGlobals.h"
#include "CvTeamAI.h"
#include "CvPlayerAI.h"
#include "CvGameAI.h"
#include "CvMap.h"
#include "CvPlot.h"
#include "CvTeam.h"
#include "CvDLLInterfaceIFaceBase.h"
#include "CvDLLEventReporterIFaceBase.h"
#include "CvDLLEngineIFaceBase.h"
#include "CvDiploParameters.h"
#include "CvInfos.h"
#include "CvPopupInfo.h"
#include "CvDLLPythonIFaceBase.h"
#include "CyArgsList.h"
#include "FProfiler.h"

// Public Functions...

CvTeam::CvTeam()
{
	m_abAtWar = new bool[MAX_TEAMS];
	m_abIsFirstTurnOfWar = new bool[MAX_TEAMS];
	m_abHasMet = new bool[MAX_TEAMS];
	m_abPermanentWarPeace = new bool[MAX_TEAMS];
	m_abOpenBorders = new bool[MAX_TEAMS];
	m_abSharingVisions = new bool[MAX_TEAMS];
	m_abDefensivePact = new bool[MAX_TEAMS];
	m_abForcePeace = new bool[MAX_TEAMS];

	m_aucRandomValues = new unsigned char[NUM_RAND_VALUES];

	m_pabHasSeedling = NULL;
	m_aiUnitClassCount = NULL;
	m_aiBuildingClassCount = NULL;
	m_aiEuropeUnitsPurchased = NULL;

	reset((TeamTypes)0, true);
}


CvTeam::~CvTeam()
{
	uninit();
	SAFE_DELETE_ARRAY(m_abAtWar);
	SAFE_DELETE_ARRAY(m_abIsFirstTurnOfWar);
	SAFE_DELETE_ARRAY(m_abHasMet);
	SAFE_DELETE_ARRAY(m_abPermanentWarPeace);
	SAFE_DELETE_ARRAY(m_abOpenBorders);
	SAFE_DELETE_ARRAY(m_abSharingVisions);
	SAFE_DELETE_ARRAY(m_abDefensivePact);
	SAFE_DELETE_ARRAY(m_abForcePeace);

	SAFE_DELETE_ARRAY(m_aucRandomValues);
}


void CvTeam::init(TeamTypes eID)
{
	//--------------------------------
	// Init saved data
	reset(eID);

	//--------------------------------
	// Init non-saved data

	//--------------------------------
	// Init other game data
	AI_init();
}


void CvTeam::uninit()
{
	SAFE_DELETE_ARRAY(m_pabHasSeedling);
	SAFE_DELETE_ARRAY(m_aiUnitClassCount);
	SAFE_DELETE_ARRAY(m_aiBuildingClassCount);
	SAFE_DELETE_ARRAY(m_aiEuropeUnitsPurchased);
}


// FUNCTION: reset()
// Initializes data members that are serialized.
void CvTeam::reset(TeamTypes eID, bool bConstructorCall)
{
	int iI;

	//--------------------------------
	// Uninit class
	uninit();

	m_iNumMembers = 0;
	m_iAliveCount = 0;
	m_iEverAliveCount = 0;
	m_iNumCities = 0;
	m_iTotalLand = 0;
	m_iMapTradingCount = GC.getDefineINT("ENABLE_MAP_TRADING");
	m_iGoldTradingCount = GC.getDefineINT("ENABLE_GOLD_TRADING");
	m_iOpenBordersTradingCount = GC.getDefineINT("ENABLE_OPEN_BORDERS");
	m_iDefensivePactTradingCount = GC.getDefineINT("ENABLE_DEFENSIVE_PACT_TRADING");
	m_iPermanentAllianceTradingCount = GC.getDefineINT("ENABLE_ALLIANCE_TRADING");
	m_iLandDiscovered = 0;
	m_ucAliveEuropeanPlayers = 0;
	m_ucNumEuropeanCities = 0;

	for (iI = 0; iI < NUM_RAND_VALUES; iI++)
	{
		m_aucRandomValues[iI] = 0;
	}

	m_bMapCentering = false;
	m_bSharingVision = false;

	m_eID = eID;
	m_eNewEra = NO_NEW_ERA;

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		m_abHasMet[iI] = false;
		m_abAtWar[iI] = false;
		m_abIsFirstTurnOfWar[iI] = false;
		m_abPermanentWarPeace[iI] = false;
		m_abOpenBorders[iI] = false;
		m_abSharingVisions[iI] = false;
		m_abDefensivePact[iI] = false;
		m_abForcePeace[iI] = false;
	}

	if (!bConstructorCall)
	{
		FAssertMsg(m_aiUnitClassCount==NULL, "about to leak memory, CvTeam::m_aiUnitClassCount");
		FAssertMsg(m_aiEuropeUnitsPurchased==NULL, "about to leak memory, CvTeam::m_aiEuropeUnitsPurchased");
		m_aiUnitClassCount = new int [GC.getNumUnitClassInfos()];
		m_aiEuropeUnitsPurchased = new int [GC.getNumUnitClassInfos()];
		for (iI = 0; iI < GC.getNumUnitClassInfos(); iI++)
		{
			m_aiUnitClassCount[iI] = 0;
			m_aiEuropeUnitsPurchased[iI] = 0;
		}

		FAssertMsg(m_aiBuildingClassCount==NULL, "about to leak memory, CvTeam::m_aiBuildingClassCount");
		m_aiBuildingClassCount = new int [GC.getNumBuildingClassInfos()];
		for (iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
		{
			m_aiBuildingClassCount[iI] = 0;
		}

		FAssertMsg(m_pabHasSeedling==NULL, "about to leak memory, CvPlayer::m_pabHasSeedling");
		m_pabHasSeedling = new bool [GC.getNumBonusInfos()];
		for (iI = 0; iI < GC.getNumBonusInfos(); ++iI)
		{
			m_pabHasSeedling[iI] = false;
		}

		m_aeRevealedBonuses.clear();
		m_aPlayers.clear();

		AI_reset();
	}
}


void CvTeam::addTeam(TeamTypes eTeam)
{
	CLLNode<TradeData>* pNode;
	CvDeal* pLoopDeal;
	CvPlot* pLoopPlot;
	CvWString szBuffer;
	bool bValid;
	int iLoop;
	int iI, iJ;

	FAssert(eTeam != NO_TEAM);
	FAssert(eTeam != getID());

	for (iI = 0; iI < MAX_PLAYERS; iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes) iI);
		if (kLoopPlayer.isAlive())
		{
			if ((kLoopPlayer.getTeam() != getID()) && (kLoopPlayer.getTeam() != eTeam))
			{
				if (GET_TEAM(kLoopPlayer.getTeam()).isHasMet(getID()) && GET_TEAM(kLoopPlayer.getTeam()).isHasMet(eTeam))
				{
					szBuffer = gDLL->getText("TXT_KEY_MISC_PLAYER_PERMANENT_ALLIANCE", getName().GetCString(), GET_TEAM(eTeam).getName().GetCString());
					gDLL->getInterfaceIFace()->addMessage(kLoopPlayer.getID(), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIRALLIANCE", MESSAGE_TYPE_MINOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_HIGHLIGHT_TEXT"));
				}
			}
		}
	}

	szBuffer = gDLL->getText("TXT_KEY_MISC_PLAYER_PERMANENT_ALLIANCE", getName().GetCString(), GET_TEAM(eTeam).getName().GetCString());
	GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getLeaderID(), szBuffer, -1, -1, (ColorTypes)GC.getInfoTypeForString("COLOR_HIGHLIGHT_TEXT"));


	for (pLoopDeal = GC.getGameINLINE().firstDeal(&iLoop); pLoopDeal != NULL; pLoopDeal = GC.getGameINLINE().nextDeal(&iLoop))
	{
		if (((GET_PLAYER(pLoopDeal->getFirstPlayer()).getTeam() == getID()) && (GET_PLAYER(pLoopDeal->getSecondPlayer()).getTeam() == eTeam)) ||
			((GET_PLAYER(pLoopDeal->getFirstPlayer()).getTeam() == eTeam) && (GET_PLAYER(pLoopDeal->getSecondPlayer()).getTeam() == getID())))
		{
			bValid = true;

			if (pLoopDeal->getFirstTrades() != NULL)
			{
				for (pNode = pLoopDeal->getFirstTrades()->head(); pNode; pNode = pLoopDeal->getFirstTrades()->next(pNode))
				{
					if ((pNode->m_data.m_eItemType == TRADE_OPEN_BORDERS) ||
						(pNode->m_data.m_eItemType == TRADE_DEFENSIVE_PACT) ||
						(pNode->m_data.m_eItemType == TRADE_PEACE_TREATY))
					{
						bValid = false;
					}
				}
			}

			if (pLoopDeal->getSecondTrades() != NULL)
			{
				for (pNode = pLoopDeal->getSecondTrades()->head(); pNode; pNode = pLoopDeal->getSecondTrades()->next(pNode))
				{
					if ((pNode->m_data.m_eItemType == TRADE_OPEN_BORDERS) ||
						(pNode->m_data.m_eItemType == TRADE_DEFENSIVE_PACT) ||
						(pNode->m_data.m_eItemType == TRADE_PEACE_TREATY))
					{
						bValid = false;
					}
				}
			}

			if (!bValid)
			{
				pLoopDeal->kill(true, NO_TEAM);
			}
		}
	}

	shareItems(eTeam);
	GET_TEAM(eTeam).shareItems(getID());


	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if ((iI != getID()) && (iI != eTeam))
		{
			if (GET_TEAM((TeamTypes)iI).isAlive())
			{
				if (GET_TEAM(eTeam).isHasMet((TeamTypes)iI))
				{
					meet(((TeamTypes)iI), false);
				}
				else if (isHasMet((TeamTypes)iI))
				{
					GET_TEAM(eTeam).meet(((TeamTypes)iI), false);
				}
			}
		}
	}

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if ((iI != getID()) && (iI != eTeam))
		{
			if (GET_TEAM((TeamTypes)iI).isAlive())
			{
				if (GET_TEAM(eTeam).isAtWar((TeamTypes)iI))
				{
					declareWar(((TeamTypes)iI), false, GET_TEAM(eTeam).AI_getWarPlan((TeamTypes)iI));
				}
				else if (isAtWar((TeamTypes)iI))
				{
					GET_TEAM(eTeam).declareWar(((TeamTypes)iI), false, AI_getWarPlan((TeamTypes)iI));
				}
			}
		}
	}

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if ((iI != getID()) && (iI != eTeam))
		{
			if (GET_TEAM((TeamTypes)iI).isAlive())
			{
				if (GET_TEAM(eTeam).isPermanentWarPeace((TeamTypes)iI))
				{
					setPermanentWarPeace(((TeamTypes)iI), true);
				}
				else if (isPermanentWarPeace((TeamTypes)iI))
				{
					GET_TEAM(eTeam).setPermanentWarPeace(((TeamTypes)iI), true);
				}
			}
		}
	}

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if ((iI != getID()) && (iI != eTeam))
		{
			CvTeam& loopTeam = GET_TEAM((TeamTypes)iI);
			if (loopTeam.isAlive())
			{
				if (GET_TEAM(eTeam).isOpenBorders((TeamTypes)iI))
				{
					setOpenBorders(((TeamTypes)iI), true);
					loopTeam.setOpenBorders(getID(), true);
				}
				else if (isOpenBorders((TeamTypes)iI))
				{
					GET_TEAM(eTeam).setOpenBorders(((TeamTypes)iI), true);
					loopTeam.setOpenBorders(eTeam, true);
				}
			}
		}
	}

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if ((iI != getID()) && (iI != eTeam))
		{
			CvTeam& loopTeam = GET_TEAM((TeamTypes)iI);
			if (loopTeam.isAlive())
			{
				if (GET_TEAM(eTeam).isDefensivePact((TeamTypes)iI))
				{
					setDefensivePact(((TeamTypes)iI), true);
					loopTeam.setDefensivePact(getID(), true);
				}
				else if (isDefensivePact((TeamTypes)iI))
				{
					GET_TEAM(eTeam).setDefensivePact(((TeamTypes)iI), true);
					loopTeam.setDefensivePact(eTeam, true);
				}
			}
		}
	}

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if ((iI != getID()) && (iI != eTeam))
		{
			CvTeam& loopTeam = GET_TEAM((TeamTypes)iI);
			if (loopTeam.isAlive())
			{
				if (GET_TEAM(eTeam).isForcePeace((TeamTypes)iI))
				{
					setForcePeace(((TeamTypes)iI), true);
					loopTeam.setForcePeace(getID(), true);
				}
				else if (isForcePeace((TeamTypes)iI))
				{
					GET_TEAM(eTeam).setForcePeace(((TeamTypes)iI), true);
					loopTeam.setForcePeace(eTeam, true);
				}
			}
		}
	}

	shareCounters(eTeam);
	GET_TEAM(eTeam).shareCounters(getID());

	bool bActiveTeamSwitched = (GC.getGameINLINE().getActiveTeam() == eTeam);

	for (iI = 0; iI < MAX_PLAYERS; iI++)
	{
		if (GET_PLAYER((PlayerTypes)iI).getTeam() == eTeam)
		{
			GET_PLAYER((PlayerTypes)iI).setTeam(getID());
		}
	}

	for (iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); iI++)
	{
		pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iI);

		pLoopPlot->changeVisibilityCount(getID(), pLoopPlot->getVisibilityCount(eTeam), NO_INVISIBLE);

		for (iJ = 0; iJ < GC.getNumInvisibleInfos(); iJ++)
		{
			pLoopPlot->changeInvisibleVisibilityCount(getID(), ((InvisibleTypes)iJ), pLoopPlot->getInvisibleVisibilityCount(eTeam, ((InvisibleTypes)iJ)));
		}

		if (pLoopPlot->isRevealed(eTeam, false))
		{
			pLoopPlot->setRevealed(getID(), true, false, eTeam);
		}
	}

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if ((iI != getID()) && (iI != eTeam))
		{
			CvTeam& loopTeam = GET_TEAM((TeamTypes)iI);
			loopTeam.AI_setAtWarCounter(getID(), ((loopTeam.AI_getAtWarCounter(getID()) + loopTeam.AI_getAtWarCounter(eTeam)) / 2));
			loopTeam.AI_setAtPeaceCounter(getID(), ((loopTeam.AI_getAtPeaceCounter(getID()) + loopTeam.AI_getAtPeaceCounter(eTeam)) / 2));
			loopTeam.AI_setHasMetCounter(getID(), ((loopTeam.AI_getHasMetCounter(getID()) + loopTeam.AI_getHasMetCounter(eTeam)) / 2));
			loopTeam.AI_setDefensivePactCounter(getID(), ((loopTeam.AI_getDefensivePactCounter(getID()) + loopTeam.AI_getDefensivePactCounter(eTeam)) / 2));
			loopTeam.AI_setShareWarCounter(getID(), ((loopTeam.AI_getShareWarCounter(getID()) + loopTeam.AI_getShareWarCounter(eTeam)) / 2));
			loopTeam.AI_setWarSuccess(getID(), ((loopTeam.AI_getWarSuccess(getID()) + loopTeam.AI_getWarSuccess(eTeam)) / 2));
			loopTeam.AI_setEnemyPeacetimeTradeValue(getID(), ((loopTeam.AI_getEnemyPeacetimeTradeValue(getID()) + loopTeam.AI_getEnemyPeacetimeTradeValue(eTeam)) / 2));
			loopTeam.AI_setEnemyPeacetimeGrantValue(getID(), ((loopTeam.AI_getEnemyPeacetimeGrantValue(getID()) + loopTeam.AI_getEnemyPeacetimeGrantValue(eTeam)) / 2));

			if (loopTeam.isAlive())
			{
				loopTeam.AI_setWarPlan(getID(), NO_WARPLAN, false);
				loopTeam.AI_setWarPlan(eTeam, NO_WARPLAN, false);
			}
		}
	}

	AI_updateWorstEnemy();

	AI_updateAreaStragies();

	GC.getGameINLINE().updateScore(true);

	if (bActiveTeamSwitched)
	{
		for (iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); iI++)
		{
			pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iI);
			pLoopPlot->updateSymbols();
			pLoopPlot->updateFog();
			pLoopPlot->updateVisibility();
		}
		gDLL->getInterfaceIFace()->setDirty(MinimapSection_DIRTY_BIT, true);
		gDLL->getInterfaceIFace()->setDirty(GlobeLayer_DIRTY_BIT, true);
		gDLL->getInterfaceIFace()->setDirty(ColoredPlots_DIRTY_BIT, true);
	}

}


void CvTeam::shareItems(TeamTypes eTeam)
{
	FAssert(eTeam != NO_TEAM);
	FAssert(eTeam != getID());

	for (int iI = 0; iI < GC.getNumUnitClassInfos(); ++iI)
	{
		UnitClassTypes eUnitClass = (UnitClassTypes) iI;
		int iOtherTeamUnits = GET_TEAM(eTeam).getEuropeUnitsPurchased(eUnitClass);
		if (getEuropeUnitsPurchased(eUnitClass) < iOtherTeamUnits)
		{
			changeEuropeUnitsPurchased(eUnitClass, iOtherTeamUnits - getEuropeUnitsPurchased(eUnitClass));
		}
	}

	for (int iI = 0; iI < GC.getNumBonusInfos(); ++iI)
	{
		if (GET_TEAM(eTeam).isForceRevealedBonus((BonusTypes)iI))
		{
			setForceRevealedBonus((BonusTypes)iI, true);
		}
	}
}


void CvTeam::shareCounters(TeamTypes eTeam)
{
	CvTeam& kOtherTeam = GET_TEAM(eTeam);
	int iI;
	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if ((iI != getID()) && (iI != eTeam))
		{
			if (kOtherTeam.AI_getAtWarCounter((TeamTypes)iI) > AI_getAtWarCounter((TeamTypes)iI))
			{
				AI_setAtWarCounter(((TeamTypes)iI), kOtherTeam.AI_getAtWarCounter((TeamTypes)iI));
			}

			if (kOtherTeam.AI_getAtPeaceCounter((TeamTypes)iI) > AI_getAtPeaceCounter((TeamTypes)iI))
			{
				AI_setAtPeaceCounter(((TeamTypes)iI), kOtherTeam.AI_getAtPeaceCounter((TeamTypes)iI));
			}

			if (kOtherTeam.AI_getHasMetCounter((TeamTypes)iI) > AI_getHasMetCounter((TeamTypes)iI))
			{
				AI_setHasMetCounter(((TeamTypes)iI), kOtherTeam.AI_getHasMetCounter((TeamTypes)iI));
			}

			if (kOtherTeam.AI_getOpenBordersCounter((TeamTypes)iI) > AI_getOpenBordersCounter((TeamTypes)iI))
			{
				AI_setOpenBordersCounter(((TeamTypes)iI), kOtherTeam.AI_getOpenBordersCounter((TeamTypes)iI));
			}

			if (kOtherTeam.AI_getDefensivePactCounter((TeamTypes)iI) > AI_getDefensivePactCounter((TeamTypes)iI))
			{
				AI_setDefensivePactCounter(((TeamTypes)iI), kOtherTeam.AI_getDefensivePactCounter((TeamTypes)iI));
			}

			if (kOtherTeam.AI_getShareWarCounter((TeamTypes)iI) > AI_getShareWarCounter((TeamTypes)iI))
			{
				AI_setShareWarCounter(((TeamTypes)iI), kOtherTeam.AI_getShareWarCounter((TeamTypes)iI));
			}

			if (kOtherTeam.AI_getWarSuccess((TeamTypes)iI) > AI_getWarSuccess((TeamTypes)iI))
			{
				AI_setWarSuccess(((TeamTypes)iI), kOtherTeam.AI_getWarSuccess((TeamTypes)iI));
			}

			if (kOtherTeam.AI_getEnemyPeacetimeTradeValue((TeamTypes)iI) > AI_getEnemyPeacetimeTradeValue((TeamTypes)iI))
			{
				AI_setEnemyPeacetimeTradeValue(((TeamTypes)iI), kOtherTeam.AI_getEnemyPeacetimeTradeValue((TeamTypes)iI));
			}

			if (kOtherTeam.AI_getEnemyPeacetimeGrantValue((TeamTypes)iI) > AI_getEnemyPeacetimeGrantValue((TeamTypes)iI))
			{
				AI_setEnemyPeacetimeGrantValue(((TeamTypes)iI), kOtherTeam.AI_getEnemyPeacetimeGrantValue((TeamTypes)iI));
			}

			kOtherTeam.AI_setWarPlan(((TeamTypes)iI), NO_WARPLAN, false);
		}
	}

	for (iI = 0; iI < GC.getNumUnitClassInfos(); iI++)
	{
		changeUnitClassCount(((UnitClassTypes)iI), kOtherTeam.getUnitClassCount((UnitClassTypes)iI));
	}

	for (iI = 0; iI < GC.getNumBuildingClassInfos(); iI++)
	{
		changeBuildingClassCount((BuildingClassTypes)iI, kOtherTeam.getBuildingClassCount((BuildingClassTypes)iI));
	}
}

void CvTeam::doTurn()
{
	PROFILE_FUNC();
	FAssertMsg(isAlive(), "isAlive is expected to be true");
	AI_doTurnPre();

	for (int iTeam = 0; iTeam < MAX_TEAMS; ++iTeam)
	{
		setIsFirstTurnOfWar((TeamTypes)iTeam, false);
	}

	AI_doTurnPost();
}


void CvTeam::updateYield()
{
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			kLoopPlayer.updateYield();
		}
	}
}

bool CvTeam::canChangeWarPeace(TeamTypes eTeam) const
{
	if (GC.getGameINLINE().isOption(GAMEOPTION_NO_CHANGING_WAR_PEACE))
	{
		return false;
	}

	if (eTeam == getID())
	{
		return false;
	}

	if (isPermanentWarPeace(eTeam) || GET_TEAM(eTeam).isPermanentWarPeace(getID()))
	{
		return false;
	}

	return true;
}


bool CvTeam::canDeclareWar(TeamTypes eTeam) const
{
	if (eTeam == getID())
	{
		return false;
	}

	if (isAtWar(eTeam))
	{
		return false;
	}
	CvTeam& kTeam = GET_TEAM(eTeam);

	if (!isAlive() || !kTeam.isAlive())
	{
		return false;
	}

	if (!isHasMet(eTeam))
	{
		return false;
	}

	if (kTeam.isParentOf(getID()))
	{
		if (!canDoRevolution())
		{
			return false;
		}
	}
	/*else if (GET_TEAM(eTeam).hasEuropePlayer())
	{
	if (hasColonialPlayer() && (!isInRevolution() && GET_TEAM(eTeam).getAtWarCount() == 0))
	{
	return false;
	}
	}*/

	if (hasColonialPlayer() && kTeam.hasColonialPlayer())
	{
		if (GC.getGameINLINE().getElapsedGameTurns() < GC.getDefineINT("COLONIAL_FORCED_PEACE_TURNS"))
		{
			return false;
		}
	}

	if (hasEuropePlayer())
	{
		return false;
	}

	if (isForcePeace(eTeam))
	{
		return false;
	}

	if (!canChangeWarPeace(eTeam))
	{
		return false;
	}

	if (GC.getGameINLINE().isOption(GAMEOPTION_ALWAYS_PEACE))
	{
		return false;
	}


	bool isNativeTeam = isNative();
	bool isNativekWhoToTeam = kTeam.isNative();

	//European AI should not attack Natives the first era
	if (!isHuman() && !isNativeTeam && isNativekWhoToTeam && kTeam.getNewEra() == 0)
	{
		return false;
	}

	if (GC.getUSE_CAN_DECLARE_WAR_CALLBACK())
	{
		CyArgsList argsList;
		argsList.add(getID());	// Team ID
		argsList.add(eTeam);	// pass in city class
		long lResult=0;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "canDeclareWar", argsList.makeFunctionArgs(), &lResult); //Returns true as default

		if (lResult == 0)
		{
			return false;
		}
	}

	return true;
}


void CvTeam::declareWarNoRevolution(TeamTypes eTeam, bool bNewDiplo, WarPlanTypes eWarPlan, bool bPlaySound)
{
	PROFILE_FUNC();

	CLLNode<TradeData>* pNode;
	CvDiploParameters* pDiplo;
	CvDeal* pLoopDeal;
	CvWString szBuffer;
	bool bCancelDeal;
	int iLoop;
	int iI, iJ;

	FAssertMsg(eTeam != NO_TEAM, "eTeam is not assigned a valid value");
	FAssertMsg(eTeam != getID(), "eTeam is not expected to be equal with getID()");
	CvTeam& kOtherTeam = GET_TEAM(eTeam);

	if (!isAtWar(eTeam))
	{
		for (pLoopDeal = GC.getGameINLINE().firstDeal(&iLoop); pLoopDeal != NULL; pLoopDeal = GC.getGameINLINE().nextDeal(&iLoop))
		{
			if (((GET_PLAYER(pLoopDeal->getFirstPlayer()).getTeam() == getID()) && (GET_PLAYER(pLoopDeal->getSecondPlayer()).getTeam() == eTeam)) ||
				((GET_PLAYER(pLoopDeal->getFirstPlayer()).getTeam() == eTeam) && (GET_PLAYER(pLoopDeal->getSecondPlayer()).getTeam() == getID())))
			{
				pLoopDeal->kill(true, getID());
			}
		}

		FAssertMsg(eTeam != getID(), "eTeam is not expected to be equal with getID()");
		setAtWar(eTeam, true);
		setIsFirstTurnOfWar(eTeam, true);
		updateFocusEuropeanPlayerInWarTime(eTeam);
		kOtherTeam.setAtWar(getID(), true);
		kOtherTeam.setIsFirstTurnOfWar(getID(), true);
		kOtherTeam.updateFocusEuropeanPlayerInWarTime(getID());

		for (iI = 0; iI < getPlayerCount(); iI++)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
			kLoopPlayer.validateMissions();
		}

		for (iI = 0; iI < kOtherTeam.getPlayerCount(); iI++)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER(kOtherTeam.getPlayer(iI));
			kLoopPlayer.validateMissions();
		}

		meet(eTeam, false);

		AI_setAtPeaceCounter(eTeam, 0);
		kOtherTeam.AI_setAtPeaceCounter(getID(), 0);

		AI_setShareWarCounter(eTeam, 0);
		kOtherTeam.AI_setShareWarCounter(getID(), 0);

		kOtherTeam.AI_setWarPlan(getID(), WARPLAN_ATTACKED_RECENT);

		for (iI = 0; iI < MAX_TEAMS; iI++)
		{
			if (GET_TEAM((TeamTypes)iI).isAlive())
			{
				if (!kOtherTeam.isAtWar((TeamTypes)iI) && kOtherTeam.AI_isChosenWar((TeamTypes)iI))
				{
					kOtherTeam.AI_setWarPlan(((TeamTypes)iI), NO_WARPLAN);
				}
			}
		}

		if (NO_WARPLAN != eWarPlan)
		{
			AI_setWarPlan(eTeam, eWarPlan);
		}

		FAssert(!(AI_isSneakAttackPreparing(eTeam)));
		if ((AI_getWarPlan(eTeam) == NO_WARPLAN) || AI_isSneakAttackPreparing(eTeam))
		{
			if (isHuman())
			{
				AI_setWarPlan(eTeam, WARPLAN_TOTAL);
			}
			else if (kOtherTeam.getAtWarCount() == 1)
			{
				AI_setWarPlan(eTeam, WARPLAN_LIMITED);
			}
			else
			{
				AI_setWarPlan(eTeam, WARPLAN_DOGPILE);
			}
		}

		GET_TEAM(getID()).AI_doDamages(eTeam, false);

		GC.getMapINLINE().verifyUnitValidPlot();

		for (iI = 0; iI < getPlayerCount(); iI++)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
			kLoopPlayer.verifyUnitStacksValid();
		}

		GC.getGameINLINE().AI_makeAssignWorkDirty();

		if ((getID() == GC.getGameINLINE().getActiveTeam()) || (eTeam == GC.getGameINLINE().getActiveTeam()))
		{
			gDLL->getInterfaceIFace()->setDirty(Score_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(CityInfo_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(ColoredPlots_DIRTY_BIT, true);
		}

		for (iI = 0; iI < getPlayerCount(); iI++)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
			if (kLoopPlayer.isAlive())
			{
				PlayerTypes eParent = kLoopPlayer.getParent();
				for (iJ = 0; iJ < kOtherTeam.getPlayerCount(); iJ++)
				{
					CvPlayer& kLoopPlayer2 = GET_PLAYER(kOtherTeam.getPlayer(iJ));
					kLoopPlayer.AI_setFirstContact(kLoopPlayer2.getID(), true);
					kLoopPlayer2.AI_setFirstContact(kLoopPlayer.getID(), true);

					PlayerTypes eOtherParent = kLoopPlayer2.getParent();
					if (eParent != NO_PLAYER &&  eOtherParent != NO_PLAYER && eOtherParent != eParent)
					{
						CvPlayer& kParent = GET_PLAYER(eParent);
						CvTeam& kParentTeam = GET_TEAM(kParent.getTeam());
						CvPlayer& kOtherParent = GET_PLAYER(eOtherParent);
						kParentTeam.declareWarNoRevolution(kOtherParent.getTeam(), true, WARPLAN_TOTAL, false);
					}
				}
			}
		}

		for (iI = 0; iI < getPlayerCount(); iI++)
		{
			CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
			if (kPlayer.isAlive())
			{
				for (iJ = 0; iJ < MAX_PLAYERS; iJ++)
				{
					CvPlayer& kPlayer2 = GET_PLAYER((PlayerTypes)iJ);
					if (kPlayer2.isAlive())
					{
						CvTeamAI& kTeam = GET_TEAM(kPlayer2.getTeam());

						if (kTeam.getID() == eTeam)
						{
							kPlayer2.AI_changeMemoryCount(kPlayer.getID(), MEMORY_DECLARED_WAR, 1);
						}
						else if (kTeam.getID() != getID())
						{
							if (kTeam.isHasMet(eTeam))
							{
								if (kTeam.AI_getAttitude(eTeam) >= ATTITUDE_PLEASED)
								{
									kPlayer2.AI_changeMemoryCount(kPlayer.getID(), MEMORY_DECLARED_WAR_ON_FRIEND, 1);
								}
							}
						}
					}
				}
			}
		}

		if (GC.getGameINLINE().isFinalInitialized() && !(gDLL->GetWorldBuilderMode()))
		{
			if (bNewDiplo)
			{
				if (!isHuman())
				{
					for (iJ = 0; iJ < kOtherTeam.getPlayerCount(); iJ++)
					{
						CvPlayer& kLoopPlayer = GET_PLAYER(kOtherTeam.getPlayer(iJ));
						if (kLoopPlayer.isAlive() && kLoopPlayer.isHuman())
						{
							if (GET_PLAYER(getLeaderID()).canContact(kLoopPlayer.getID()))
							{
								pDiplo = new CvDiploParameters(getLeaderID());
								FAssertMsg(pDiplo != NULL, "pDiplo must be valid");
								pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_DECLARE_WAR"));
								pDiplo->setAIContact(true);
								gDLL->beginDiplomacy(pDiplo, kLoopPlayer.getID());
							}
						}
					}
				}
			}

			for (iI = 0; iI < MAX_PLAYERS; iI++)
			{
				CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes)iI);
				if (kLoopPlayer.isAlive())
				{
					if (kLoopPlayer.getTeam() == getID())
					{
						szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_DECLARED_WAR_ON", GET_TEAM(eTeam).getName().GetCString());
						gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, bPlaySound ? "AS2D_DECLAREWAR" : NULL, MESSAGE_TYPE_MAJOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WARNING_TEXT"));

						kLoopPlayer.removeAllAgreementOfTeam(eTeam);
					}
					else if (kLoopPlayer.getTeam() == eTeam)
					{
						szBuffer = gDLL->getText("TXT_KEY_MISC_DECLARED_WAR_ON_YOU", getName().GetCString());
						gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, bPlaySound ? "AS2D_DECLAREWAR" : NULL, MESSAGE_TYPE_MAJOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WARNING_TEXT"));

						kLoopPlayer.removeAllAgreementOfTeam(getID());
					}
					else if ((GET_TEAM(kLoopPlayer.getTeam()).isHasMet(getID()) || hasEuropePlayer()) && GET_TEAM(kLoopPlayer.getTeam()).isHasMet(eTeam))
					{
						szBuffer = gDLL->getText("TXT_KEY_MISC_SOMEONE_DECLARED_WAR", getName().GetCString(), GET_TEAM(eTeam).getName().GetCString());
						gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, bPlaySound ? "AS2D_THEIRDECLAREWAR" : NULL, MESSAGE_TYPE_MAJOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WARNING_TEXT"));
					}
				}
			}

			szBuffer = gDLL->getText("TXT_KEY_MISC_SOMEONE_DECLARES_WAR", getName().GetCString(), GET_TEAM(eTeam).getName().GetCString());
			GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getLeaderID(), szBuffer, -1, -1, (ColorTypes)GC.getInfoTypeForString("COLOR_WARNING_TEXT"));
		}

		for (iI = 0; iI < getPlayerCount(); iI++)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
			if (kLoopPlayer.isAlive())
			{
				for (pLoopDeal = GC.getGameINLINE().firstDeal(&iLoop); pLoopDeal != NULL; pLoopDeal = GC.getGameINLINE().nextDeal(&iLoop))
				{
					bCancelDeal = false;

					if (pLoopDeal->getFirstPlayer() == kLoopPlayer.getID() || pLoopDeal->getSecondPlayer() == kLoopPlayer.getID())
					{
						for (pNode = pLoopDeal->headFirstTradesNode(); (pNode != NULL); pNode = pLoopDeal->nextFirstTradesNode(pNode))
						{
							if (pNode->m_data.m_eItemType == TRADE_DEFENSIVE_PACT)
							{
								bCancelDeal = true;
							}
						}

						for (pNode = pLoopDeal->headSecondTradesNode(); (pNode != NULL); pNode = pLoopDeal->nextSecondTradesNode(pNode))
						{
							if (pNode->m_data.m_eItemType == TRADE_DEFENSIVE_PACT)
							{
								bCancelDeal = true;
							}
						}
					}

					if (bCancelDeal)
					{
						pLoopDeal->kill(true, getID());
					}
				}
			}
		}

		gDLL->getEventReporterIFace()->changeWar(true, getID(), eTeam);

		cancelDefensivePacts(getID());

		for (iI = 0; iI < MAX_TEAMS; iI++)
		{
			CvTeamAI& kTeam = GET_TEAM((TeamTypes)iI);
			if (kTeam.isAlive())
			{
				if (kTeam.isDefensivePact(eTeam))
				{
					kTeam.declareWar(getID(), bNewDiplo, WARPLAN_DOGPILE);
				}
			}
		}

		kOtherTeam.cancelDefensivePacts(getID());
	}
}

void CvTeam::updateFocusEuropeanPlayerInWarTime(TeamTypes eTeam) 
{
	CvTeam& kOtherTeam = GET_TEAM(eTeam);
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive() && kLoopPlayer.isEuropeanAI())
		{
			int iAttackFocusEuropeanPlayer = kLoopPlayer.getAttackFocusEuropeanPlayer();
			int iBestScore = iAttackFocusEuropeanPlayer >= 0 ? GC.getGameINLINE().getPlayerScore((PlayerTypes)iAttackFocusEuropeanPlayer) : 0;
			for (int iJ = 0; iJ < kOtherTeam.getPlayerCount(); iJ++)
			{
				CvPlayer& kLoopOtherPlayer = GET_PLAYER(kOtherTeam.getPlayer(iJ));
				if (kLoopOtherPlayer.isAlive() && kLoopOtherPlayer.isEuropean())
				{
					int iScore = GC.getGameINLINE().getPlayerScore(kLoopOtherPlayer.getID());
					if (iScore > iBestScore)
					{
						iBestScore = iScore;
						kLoopPlayer.setAttackFocusEuropeanPlayer(kLoopOtherPlayer.getID());
					}
				}
			}
		}
	}
}

void CvTeam::updateFocusEuropeanPlayerInPeaceTime(TeamTypes eTeam) 
{
	const bool bIncludeHumanPlayer = false;
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive() && kLoopPlayer.isEuropean())
		{
			int iAttackFocusEuropeanPlayer = kLoopPlayer.getAttackFocusEuropeanPlayer();

			// If the focus player is part of the team that we do peace, we have to remove it from focus player.
			if ( iAttackFocusEuropeanPlayer >= 0 ) 
			{
				CvPlayer& kFocusEuropeanPlayer =  GET_PLAYER((PlayerTypes)iAttackFocusEuropeanPlayer);
				if (kFocusEuropeanPlayer.getTeam() == eTeam) 
				{
					kLoopPlayer.setAttackFocusEuropeanPlayer(-1);
					iAttackFocusEuropeanPlayer = -1;
				}
			}

			int iBestScore = iAttackFocusEuropeanPlayer >= 0 ? GC.getGameINLINE().getPlayerScore((PlayerTypes)iAttackFocusEuropeanPlayer) : 0;

			//If we are at war with another european player, we change our focus player.
			for (int iJ = 0; iJ < MAX_PLAYERS; iJ++)
			{
				CvPlayer& kLoopOtherPlayer = GET_PLAYER((PlayerTypes)iJ);
				if (kLoopOtherPlayer.isAlive() && kLoopOtherPlayer.getTeam() != eTeam && kLoopOtherPlayer.isEuropean() && isAtWar(kLoopOtherPlayer.getTeam()))
				{
					int iScore = GC.getGameINLINE().getPlayerScore(kLoopOtherPlayer.getID());
					if (iScore > iBestScore)
					{
						iBestScore = iScore;
						kLoopPlayer.setAttackFocusEuropeanPlayer(kLoopOtherPlayer.getID());
					}
				}
			}
		}
	}
}

void CvTeam::declareWar(TeamTypes eTeam, bool bNewDiplo, WarPlanTypes eWarPlan)
{
	CvTeam& kOtherTeam = GET_TEAM(eTeam);

	if (kOtherTeam.isParentOf(getID()) || isParentOf(eTeam))
	{
		doRevolution();
	}
	else
	{
		declareWarNoRevolution(eTeam, bNewDiplo, eWarPlan, true);
	}
}

void CvTeam::makePeace(TeamTypes eTeam, bool bBumpUnits)
{
	FAssertMsg(eTeam != NO_TEAM, "eTeam is not assigned a valid value");
	FAssertMsg(eTeam != getID(), "eTeam is not expected to be equal with getID()");

	if (isAtWar(eTeam))
	{
		FAssertMsg(eTeam != getID(), "eTeam is not expected to be equal with getID()");
		CvTeam& kTeam = GET_TEAM(eTeam);
		setAtWar(eTeam, false);
		kTeam.setAtWar(getID(), false);

		AI_setAtWarCounter(eTeam, 0);
		kTeam.AI_setAtWarCounter(getID(), 0);

		AI_setWarSuccess(eTeam, 0);
		kTeam.AI_setWarSuccess(getID(), 0);

		AI_setWarPlan(eTeam, NO_WARPLAN);
		kTeam.AI_setWarPlan(getID(), NO_WARPLAN);

		updateFocusEuropeanPlayerInPeaceTime(eTeam);
		kTeam.updateFocusEuropeanPlayerInPeaceTime(getID());

		if (bBumpUnits)
		{
			GC.getMapINLINE().verifyUnitValidPlot();
		}

		GC.getGameINLINE().AI_makeAssignWorkDirty();

		if ((getID() == GC.getGameINLINE().getActiveTeam()) || (eTeam == GC.getGameINLINE().getActiveTeam()))
		{
			gDLL->getInterfaceIFace()->setDirty(Score_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(CityInfo_DIRTY_BIT, true);
		}

		for (int iI = 0; iI < getPlayerCount(); iI++)
		{
			CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
			if (kLoopPlayer.isAlive())
			{
				PlayerTypes eParent = kLoopPlayer.getParent();
				CvTeam& kOtherTeam = GET_TEAM(eTeam);				
				for (int iJ = 0; iJ < kOtherTeam.getPlayerCount(); iJ++)
				{
					CvPlayer& kLoopPlayer2 = GET_PLAYER(kOtherTeam.getPlayer(iJ));					
					PlayerTypes eOtherParent = kLoopPlayer2.getParent();
					if (eParent != NO_PLAYER &&  eOtherParent != NO_PLAYER && eOtherParent != eParent)
					{
						CvPlayer& kParent = GET_PLAYER(eParent);
						CvTeam& kParentTeam = GET_TEAM(kParent.getTeam());
						CvPlayer& kOtherParent = GET_PLAYER(eOtherParent);
						kParentTeam.makePeace(kOtherParent.getTeam());
					}
				}
			}
		}

		CvWString szBuffer;
		for (int iI = 0; iI < MAX_PLAYERS; iI++)
		{
			CvPlayer& kPlayer = GET_PLAYER((PlayerTypes)iI);
			if (kPlayer.isAlive())
			{
				if (kPlayer.getTeam() == getID())
				{
					szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_MADE_PEACE_WITH", kTeam.getName().GetCString());
					gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_MAKEPEACE", MESSAGE_TYPE_MAJOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_HIGHLIGHT_TEXT"));
				}
				else if (kPlayer.getTeam() == eTeam)
				{
					szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_MADE_PEACE_WITH", getName().GetCString());
					gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_MAKEPEACE", MESSAGE_TYPE_MAJOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_HIGHLIGHT_TEXT"));
				}
				else if (GET_TEAM(kPlayer.getTeam()).isHasMet(getID()) && GET_TEAM(kPlayer.getTeam()).isHasMet(eTeam))
				{
					szBuffer = gDLL->getText("TXT_KEY_MISC_SOMEONE_MADE_PEACE", getName().GetCString(), kTeam.getName().GetCString());
					gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIRMAKEPEACE", MESSAGE_TYPE_MAJOR_EVENT, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_HIGHLIGHT_TEXT"));
				}
			}
		}

		szBuffer = gDLL->getText("TXT_KEY_MISC_SOMEONE_MADE_PEACE", getName().GetCString(), kTeam.getName().GetCString());
		GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getLeaderID(), szBuffer, -1, -1, (ColorTypes)GC.getInfoTypeForString("COLOR_HIGHLIGHT_TEXT"));

		gDLL->getEventReporterIFace()->changeWar(false, getID(), eTeam);
	}
}


bool CvTeam::canContact(TeamTypes eTeam) const
{
	CvTeam& kOtherTeam = GET_TEAM(eTeam);

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			for (int iJ = 0; iJ < kOtherTeam.getPlayerCount(); iJ++)
			{
				CvPlayer& kLoopPlayer2 = GET_PLAYER(kOtherTeam.getPlayer(iJ));

				if (kLoopPlayer2.isAlive())
				{
					if (kLoopPlayer.canContact(kLoopPlayer2.getID()))
					{
						return true;
					}
				}
			}
		}
	}

	return false;
}

void CvTeam::meet(TeamTypes eTeam, bool bNewDiplo)
{
	if (!isHasMet(eTeam))
	{
		CLLNode<IDInfo>* pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();
		if (pSelectedUnitNode != NULL)
		{
			CvUnit* pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);
			makeHasMet(eTeam, bNewDiplo);
			GET_TEAM(eTeam).makeHasMet(getID(), bNewDiplo);
		}
		else
		{
			makeHasMet(eTeam, bNewDiplo);
			GET_TEAM(eTeam).makeHasMet(getID(), bNewDiplo);
		}
	}
}

void CvTeam::signOpenBorders(TeamTypes eTeam)
{
	CLinkList<TradeData> ourList;
	CLinkList<TradeData> theirList;
	TradeData item;

	FAssert(eTeam != NO_TEAM);
	FAssert(eTeam != getID());

	if (!isAtWar(eTeam) && (getID() != eTeam))
	{
		setTradeItem(&item, TRADE_OPEN_BORDERS, 0, NULL);

		if (GET_PLAYER(getLeaderID()).canTradeItem(GET_TEAM(eTeam).getLeaderID(), item) && GET_PLAYER(GET_TEAM(eTeam).getLeaderID()).canTradeItem(getLeaderID(), item))
		{
			ourList.clear();
			theirList.clear();

			ourList.insertAtEnd(item);
			theirList.insertAtEnd(item);

			GC.getGameINLINE().implementDeal(getLeaderID(), (GET_TEAM(eTeam).getLeaderID()), &ourList, &theirList);
		}
	}
}


void CvTeam::signDefensivePact(TeamTypes eTeam)
{
	CLinkList<TradeData> ourList;
	CLinkList<TradeData> theirList;
	TradeData item;

	FAssert(eTeam != NO_TEAM);
	FAssert(eTeam != getID());

	if (!isAtWar(eTeam) && (getID() != eTeam))
	{
		setTradeItem(&item, TRADE_DEFENSIVE_PACT, 0, NULL);

		if (GET_PLAYER(getLeaderID()).canTradeItem(GET_TEAM(eTeam).getLeaderID(), item) && GET_PLAYER(GET_TEAM(eTeam).getLeaderID()).canTradeItem(getLeaderID(), item))
		{
			ourList.clear();
			theirList.clear();

			ourList.insertAtEnd(item);
			theirList.insertAtEnd(item);

			GC.getGameINLINE().implementDeal(getLeaderID(), (GET_TEAM(eTeam).getLeaderID()), &ourList, &theirList);
		}
	}
}

bool CvTeam::canSignDefensivePact(TeamTypes eTeam)
{
	for (int iTeam = 0; iTeam < MAX_TEAMS; ++iTeam)
	{
		if (iTeam != getID() && iTeam != eTeam)
		{
			CvTeam& kLoopTeam = GET_TEAM((TeamTypes)iTeam);
			if (kLoopTeam.isPermanentWarPeace(eTeam) != kLoopTeam.isPermanentWarPeace(getID()))
			{
				return false;
			}

			if (isPermanentWarPeace((TeamTypes)iTeam) != GET_TEAM(eTeam).isPermanentWarPeace((TeamTypes)iTeam))
			{
				return false;
			}
		}
	}

	return true;
}


int CvTeam::getAssets() const
{
	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iCount += kLoopPlayer.getAssets();
		}
	}

	return iCount;
}


int CvTeam::getPower() const
{
	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iCount += kLoopPlayer.getPower();
		}
	}

	return iCount;
}


int CvTeam::getDefensivePower() const
{
	int iCount = 0;

	for (int iI = 0; iI < MAX_TEAMS; iI++)
	{
		CvTeam& kLoopTeam = GET_TEAM((TeamTypes)iI);
		if (kLoopTeam.isAlive())
		{
			if (getID() == iI || isDefensivePact((TeamTypes)iI))
			{
				iCount += kLoopTeam.getPower();
			}
		}
	}

	return iCount;
}


int CvTeam::getEnemyPower() const
{
	int iCount = 0;

	for (int iI = 0; iI < MAX_TEAMS; iI++)
	{
		CvTeam& kLoopTeam = GET_TEAM((TeamTypes)iI);
		if (kLoopTeam.isAlive())
		{
			if (getID() != iI && isAtWar((TeamTypes)iI))
			{
				iCount += kLoopTeam.getPower();
			}
		}
	}

	return iCount;
}


int CvTeam::getAtWarCount() const
{
	int iCount;
	int iI;

	iCount = 0;

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if (GET_TEAM((TeamTypes)iI).isAlive())
		{
			if (isAtWar((TeamTypes)iI))
			{
				FAssert(iI != getID());
				FAssert(!(AI_isSneakAttackPreparing((TeamTypes)iI)));
				iCount++;
			}
		}
	}

	return iCount;
}


int CvTeam::getWarPlanCount(WarPlanTypes eWarPlan) const
{
	int iCount;
	int iI;

	iCount = 0;

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if (GET_TEAM((TeamTypes)iI).isAlive())
		{
			if (AI_getWarPlan((TeamTypes)iI) == eWarPlan)
			{
				FAssert(iI != getID());
				iCount++;
			}
		}
	}

	return iCount;
}


int CvTeam::getAnyWarPlanCount() const
{
	int iCount;
	int iI;

	iCount = 0;

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if (GET_TEAM((TeamTypes)iI).isAlive())
		{
			if (AI_getWarPlan((TeamTypes)iI) != NO_WARPLAN)
			{
				FAssert(iI != getID());
				iCount++;
			}
		}
	}

	FAssert(iCount >= getAtWarCount());

	return iCount;
}


int CvTeam::getChosenWarCount() const
{
	int iCount = 0;

	for (int iI = 0; iI < MAX_TEAMS; iI++)
	{
		if (GET_TEAM((TeamTypes)iI).isAlive())
		{
			if (AI_isChosenWar((TeamTypes)iI))
			{
				FAssert(iI != getID());
				iCount++;
			}
		}
	}

	return iCount;
}


int CvTeam::getHasMetCivCount() const
{
	int iCount = 0;

	for (int iI = 0; iI < MAX_TEAMS; iI++)
	{
		if (GET_TEAM((TeamTypes)iI).isAlive())
		{
			if (iI != getID())
			{
				if (isHasMet((TeamTypes)iI))
				{
					FAssert(iI != getID());
					iCount++;
				}
			}
		}
	}

	return iCount;
}


bool CvTeam::hasMetHuman() const
{
	int iI;

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if (GET_TEAM((TeamTypes)iI).isAlive())
		{
			if (iI != getID())
			{
				if (GET_TEAM((TeamTypes)iI).isHuman())
				{
					if (isHasMet((TeamTypes)iI))
					{
						FAssert(iI != getID());
						return true;
					}
				}
			}
		}
	}

	return false;
}


int CvTeam::getDefensivePactCount(TeamTypes eTeam) const
{
	int iCount;
	int iI;

	iCount = 0;

	for (iI = 0; iI < MAX_TEAMS; iI++)
	{
		if (GET_TEAM((TeamTypes)iI).isAlive())
		{
			if (iI != getID())
			{
				if (isDefensivePact((TeamTypes)iI))
				{
					if (NO_TEAM == eTeam || GET_TEAM(eTeam).isHasMet((TeamTypes)iI))
					{
						iCount++;
					}
				}
			}
		}
	}

	return iCount;
}

int CvTeam::getUnitClassMaking(UnitClassTypes eUnitClass) const
{
	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iCount += kLoopPlayer.getUnitClassMaking(eUnitClass);
		}
	}

	return iCount;
}


int CvTeam::getUnitClassCountPlusMaking(UnitClassTypes eIndex) const
{
	return (getUnitClassCount(eIndex) + getUnitClassMaking(eIndex));
}


int CvTeam::getBuildingClassMaking(BuildingClassTypes eBuildingClass) const
{
	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iCount += kLoopPlayer.getBuildingClassMaking(eBuildingClass);
		}
	}

	return iCount;
}


int CvTeam::getBuildingClassCountPlusMaking(BuildingClassTypes eIndex) const
{
	return (getBuildingClassCount(eIndex) + getBuildingClassMaking(eIndex));
}


int CvTeam::countTotalCulture()
{
	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iCount += kLoopPlayer.countTotalCulture();
		}
	}

	return iCount;
}


int CvTeam::countNumUnitsByArea(CvArea* pArea) const
{
	PROFILE_FUNC();

	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iCount += pArea->getUnitsPerPlayer(kLoopPlayer.getID());
		}
	}

	return iCount;
}


int CvTeam::countNumCitiesByArea(CvArea* pArea) const
{
	PROFILE_FUNC();

	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iCount += pArea->getCitiesPerPlayer(kLoopPlayer.getID());
		}
	}

	return iCount;
}


int CvTeam::countTotalPopulationByArea(CvArea* pArea) const
{
	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iCount += pArea->getPopulationPerPlayer(kLoopPlayer.getID());
		}
	}

	return iCount;
}


int CvTeam::countPowerByArea(CvArea* pArea) const
{
	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iCount += pArea->getPower(kLoopPlayer.getID());
		}
	}

	return iCount;
}


int CvTeam::countEnemyPowerByArea(CvArea* pArea) const
{
	int iCount;
	int iI;

	iCount = 0;

	for (iI = 0; iI < MAX_PLAYERS; iI++)
	{
		if (GET_PLAYER((PlayerTypes)iI).isAlive())
		{
			if (GET_PLAYER((PlayerTypes)iI).getTeam() != getID())
			{
				if (isAtWar(GET_PLAYER((PlayerTypes)iI).getTeam()))
				{
					iCount += pArea->getPower((PlayerTypes)iI);
				}
			}
		}
	}

	return iCount;
}


int CvTeam::countNumAIUnitsByArea(CvArea* pArea, UnitAITypes eUnitAI) const
{
	PROFILE_FUNC();

	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iCount += pArea->getNumAIUnits(kLoopPlayer.getID(), eUnitAI);
		}
	}

	return iCount;
}


int CvTeam::countEnemyDangerByArea(CvArea* pArea) const
{
	PROFILE_FUNC();

	CvPlot* pLoopPlot;
	int iCount;
	int iI;

	iCount = 0;

	for (iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); iI++)
	{
		pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iI);

		if (pLoopPlot != NULL)
		{
			if (pLoopPlot->area() == pArea)
			{
				if (pLoopPlot->getTeam() == getID())
				{
					iCount += pLoopPlot->plotCount(PUF_canDefendEnemy, getLeaderID(), false, NO_PLAYER, NO_TEAM, PUF_isVisible, getLeaderID());
				}
			}
		}
	}

	return iCount;
}

bool CvTeam::isHuman() const
{
	PROFILE_FUNC();

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isHuman())
		{
			return true;
		}
	}

	return false;
}

bool CvTeam::isNative() const
{
	PROFILE_FUNC();

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isNative())
		{
			return true;
		}
	}

	return false;
}

bool CvTeam::hasAnEuropean() const
{
	PROFILE_FUNC();

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isEuropean())
		{
			return true;
		}
	}

	return false;
}

bool CvTeam::isNearTeam(TeamTypes eIndex) const
{
	if (eIndex == NO_TEAM) 
	{
		return false;
	}
	CvTeam& kOtherTeam = GET_TEAM(eIndex);

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		for (int iJ = 0; iJ < kOtherTeam.getPlayerCount(); iJ++)
		{
			CvPlayer& kLoopPlayer2 = GET_PLAYER(kOtherTeam.getPlayer(iJ));

			if (kLoopPlayer.isNearPlayer(kLoopPlayer2.getID()))
			{
				return true;
			}
		}
	}

	return false;
}

bool CvTeam::hasNativePlayer() const
{
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isNative())
		{
			return true;
		}
	}

	return false;
}

bool CvTeam::hasColonialPlayer() const
{
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.getParent() != NO_PLAYER)
		{
			return true;
		}
	}

	return false;
}

bool CvTeam::hasEuropePlayer() const
{
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isEurope())
		{
			return true;
		}
	}

	return false;
}

PlayerTypes CvTeam::getLeaderID() const
{
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			return kLoopPlayer.getID();
		}
	}

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		return getPlayer(iI);
	}

	return NO_PLAYER;
}


HandicapTypes CvTeam::getHandicapType() const
{
	int iGameHandicap;
	int iCount;

	iGameHandicap = 0;
	iCount = 0;
	int iAiHandicap = GC.getDefineINT("AI_HANDICAP");

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iGameHandicap += kLoopPlayer.isHuman() ? kLoopPlayer.getHandicapType() : iAiHandicap;
			iCount++;
		}
	}

	if (iCount > 0)
	{
		FAssertMsg((iGameHandicap / iCount) >= 0, "(iGameHandicap / iCount) is expected to be non-negative (invalid Index)");
		return ((HandicapTypes)(iGameHandicap / iCount));
	}
	else 
	{
		return ((HandicapTypes)iAiHandicap);
	}
}


CvWString CvTeam::getName() const
{
	CvWString szBuffer;
	bool bFirst;

	bFirst = true;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			setListHelp(szBuffer, L"", kLoopPlayer.getName(), L"/", bFirst);
			bFirst = false;
		}
	}

	return szBuffer;
}

CvWString CvTeam::getCivilizationName() const
{
	CvWString szBuffer;
	bool bFirst;

	bFirst = true;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			setListHelp(szBuffer, L"", kLoopPlayer.getCivilizationDescriptionKey(), L"/", bFirst);
			bFirst = false;
		}
	}

	return szBuffer;
}

CvWString CvTeam::getCivilizationAdjectiveKey() const
{
	CvWString szBuffer;
	bool bFirst;

	bFirst = true;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			setListHelp(szBuffer, L"", kLoopPlayer.getCivilizationAdjectiveKey(), L"/", bFirst);
			bFirst = false;
		}
	}

	return szBuffer;
}

int CvTeam::getNumMembers() const
{
	return m_iNumMembers;
}


void CvTeam::changeNumMembers(int iChange)
{
	m_iNumMembers += iChange;
	FAssert(getNumMembers() >= 0);
}


int CvTeam::getAliveCount() const
{
	return m_iAliveCount;
}


int CvTeam::isAlive() const
{
	return (getAliveCount() > 0);
}


void CvTeam::changeAliveCount(int iChange)
{
	m_iAliveCount += iChange;
	FAssert(getAliveCount() >= 0);
}


int CvTeam::getEverAliveCount() const
{
	return m_iEverAliveCount;
}


int CvTeam::isEverAlive() const
{
	return (getEverAliveCount() > 0);
}


void CvTeam::changeEverAliveCount(int iChange)
{
	m_iEverAliveCount += iChange;
	FAssert(getEverAliveCount() >= 0);
}


int CvTeam::getNumCities() const
{
	return m_iNumCities;
}


void CvTeam::changeNumCities(int iChange)
{
	m_iNumCities += iChange;
	FAssert(getNumCities() >= 0);
}


int CvTeam::getTotalPopulation() const
{
	int iPopulation = 0;
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iPopulation += kLoopPlayer.getTotalPopulation();
		}
	}

	return iPopulation;
}


int CvTeam::getTotalLand() const
{
	return m_iTotalLand;
}


void CvTeam::changeTotalLand(int iChange)
{
	m_iTotalLand += iChange;
	FAssert(getTotalLand() >= 0);
}

int CvTeam::getMapTradingCount() const
{
	return m_iMapTradingCount;
}


bool CvTeam::isMapTrading()	const
{
	return (getMapTradingCount() > 0);
}


void CvTeam::changeMapTradingCount(int iChange)
{
	m_iMapTradingCount = (m_iMapTradingCount + iChange);
	FAssert(getMapTradingCount() >= 0);
}

int CvTeam::getGoldTradingCount() const
{
	return m_iGoldTradingCount;
}


bool CvTeam::isGoldTrading() const
{
	return (getGoldTradingCount() > 0);
}


void CvTeam::changeGoldTradingCount(int iChange)
{
	m_iGoldTradingCount += iChange;
	FAssert(getGoldTradingCount() >= 0);
}


int CvTeam::getOpenBordersTradingCount() const
{
	return m_iOpenBordersTradingCount;
}


bool CvTeam::isOpenBordersTrading() const
{
	return (getOpenBordersTradingCount() > 0);
}


void CvTeam::changeOpenBordersTradingCount(int iChange)
{
	m_iOpenBordersTradingCount += iChange;
	FAssert(getOpenBordersTradingCount() >= 0);
}


int CvTeam::getDefensivePactTradingCount() const
{
	return m_iDefensivePactTradingCount;
}


bool CvTeam::isDefensivePactTrading() const
{
	return (getDefensivePactTradingCount() > 0);
}


void CvTeam::changeDefensivePactTradingCount(int iChange)
{
	m_iDefensivePactTradingCount = (m_iDefensivePactTradingCount + iChange);
	FAssert(getDefensivePactTradingCount() >= 0);
}


int CvTeam::getPermanentAllianceTradingCount() const
{
	return m_iPermanentAllianceTradingCount;
}


bool CvTeam::isPermanentAllianceTrading() const
{
	if (!(GC.getGameINLINE().isOption(GAMEOPTION_PERMANENT_ALLIANCES)))
	{
		return false;
	}

	return (getPermanentAllianceTradingCount() > 0);
}


void CvTeam::changePermanentAllianceTradingCount(int iChange)
{
	m_iPermanentAllianceTradingCount = (m_iPermanentAllianceTradingCount + iChange);
	FAssert(getPermanentAllianceTradingCount() >= 0);
}

unsigned char CvTeam::getRandomValue(int eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_RAND_VALUES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aucRandomValues[eIndex];
}

void CvTeam::setRandomValue(int eIndex, unsigned char iValue)
{	
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_RAND_VALUES, "eIndex is expected to be within maximum bounds (invalid Index)");
	if (iValue != getRandomValue(eIndex))
	{
		m_aucRandomValues[eIndex] = iValue;
	}
}



bool CvTeam::isMapCentering() const
{
	return m_bMapCentering;
}

void CvTeam::setMapCentering(bool bNewValue)
{
	if (isMapCentering() != bNewValue)
	{
		m_bMapCentering = bNewValue;

		if (getID() == GC.getGameINLINE().getActiveTeam())
		{
			gDLL->getInterfaceIFace()->setDirty(MinimapSection_DIRTY_BIT, true);
		}
	}
}

TeamTypes CvTeam::getID() const
{
	return m_eID;
}

bool CvTeam::isHasMet(TeamTypes eIndex)	const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");
	//FAssert((eIndex != getID()) || m_abHasMet[eIndex]);
	return m_abHasMet[eIndex];
}

void CvTeam::makeHasMet(TeamTypes eIndex, bool bNewDiplo)
{
	CvDiploParameters* pDiplo;

	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (!isHasMet(eIndex))
	{
		m_abHasMet[eIndex] = true;

		AI_setAtPeaceCounter(eIndex, 0);
		AI_setAtWarCounter(eIndex, 0);

		if (GET_TEAM(eIndex).isHuman())
		{
			for (int iI = 0; iI < getPlayerCount(); iI++)
			{
				CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
				if (kLoopPlayer.isAlive())
				{
					if (!kLoopPlayer.isHuman())
					{
						kLoopPlayer.AI_makeProductionDirty();
					}
				}
			}
		}

		if ((getID() == GC.getGameINLINE().getActiveTeam()) || (eIndex == GC.getGameINLINE().getActiveTeam()))
		{
			gDLL->getInterfaceIFace()->setDirty(Score_DIRTY_BIT, true);
		}

		if (GC.getGameINLINE().isOption(GAMEOPTION_ALWAYS_WAR))
		{
			if (isHuman() && getID() != eIndex && canDeclareWar(eIndex))
			{
				declareWar(eIndex, false, NO_WARPLAN);
			}
		}
		else
		{
			if (GC.getGameINLINE().isFinalInitialized() && !(gDLL->GetWorldBuilderMode()))
			{
				if (bNewDiplo)
				{
					if (!isHuman())
					{
						if (!isAtWar(eIndex))
						{
							CvTeam& kOtherTeam = GET_TEAM(eIndex);
							for (int iI = 0; iI < kOtherTeam.getPlayerCount(); iI++)
							{
								CvPlayer& kLoopPlayer = GET_PLAYER(kOtherTeam.getPlayer(iI));
								if (kLoopPlayer.isAlive() && kLoopPlayer.isHuman())
								{
									if (GET_PLAYER(getLeaderID()).canContact(kLoopPlayer.getID()))
									{
										pDiplo = new CvDiploParameters(getLeaderID());
										FAssertMsg(pDiplo != NULL, "pDiplo must be valid");
										pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_FIRST_CONTACT"));
										pDiplo->setAIContact(true);
										gDLL->beginDiplomacy(pDiplo, kLoopPlayer.getID());
									}
								}
							}
						}
					}
				}
			}
		}

		// report event to Python, along with some other key state
		gDLL->getEventReporterIFace()->firstContact(getID(), eIndex);
	}
}


bool CvTeam::isAtWar(TeamTypes eIndex) const
{
	if (eIndex == UNKNOWN_TEAM)
	{
		return true;
	}
	else if (eIndex == NO_TEAM)
	{
		return false;
	}
	else
	{
		return m_abAtWar[eIndex];
	}
}


void CvTeam::setAtWar(TeamTypes eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (bNewValue)
	{
		AI_setOpenBordersCounter(eIndex, 0);
	}

	m_abAtWar[eIndex] = bNewValue;
}

bool CvTeam::isFirstTurnOfWar(TeamTypes eIndex) const
{
	if (eIndex == UNKNOWN_TEAM)
	{
		return true;
	}
	else if (eIndex == NO_TEAM)
	{
		return false;
	}
	else
	{
		return m_abIsFirstTurnOfWar[eIndex];
	}
}

void CvTeam::setIsFirstTurnOfWar(TeamTypes eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");

	m_abIsFirstTurnOfWar[eIndex] = bNewValue;
}

bool CvTeam::isPermanentWarPeace(TeamTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_abPermanentWarPeace[eIndex];
}


void CvTeam::setPermanentWarPeace(TeamTypes eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_abPermanentWarPeace[eIndex] = bNewValue;
}


bool CvTeam::isOpenBorders(TeamTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (isAtWar(eIndex))
	{
		return false;
	}

	return m_abOpenBorders[eIndex];
}


void CvTeam::setOpenBorders(TeamTypes eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (isOpenBorders(eIndex) != bNewValue)
	{
		m_abOpenBorders[eIndex] = bNewValue;

		AI_setOpenBordersCounter(eIndex, 0);

		GC.getMapINLINE().verifyUnitValidPlot();

		if ((getID() == GC.getGameINLINE().getActiveTeam()) || (eIndex == GC.getGameINLINE().getActiveTeam()))
		{
			gDLL->getInterfaceIFace()->setDirty(Score_DIRTY_BIT, true);
		}
	}

	FAssert(isOpenBorders(eIndex) == bNewValue);
}

bool CvTeam::isSharingVision() const
{
	return m_bSharingVision;
}

void CvTeam::setSharingVision(bool bNewValue)
{
	m_bSharingVision = bNewValue;
}

bool CvTeam::isSharingVision(TeamTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (isAtWar(eIndex))
	{
		return false;
	}

	return m_abSharingVisions[eIndex];
}


void CvTeam::setSharingVision(TeamTypes eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (isSharingVision(eIndex) != bNewValue)
	{
		m_abSharingVisions[eIndex] = bNewValue;

		if (isSharingVision(eIndex))
		{
			setSharingVision(true);
		}
		else 
		{
			bool hasSharingVisionWithTeam = false;
			for (int iI = 0; iI < MAX_TEAMS; iI++)
			{
				if (isSharingVision((TeamTypes)iI))
				{
					hasSharingVisionWithTeam = true;
					break;
				}
			}

			if (!hasSharingVisionWithTeam) 
			{
				setSharingVision(false);
			}
		}

		if ((getID() == GC.getGameINLINE().getActiveTeam()) || (eIndex == GC.getGameINLINE().getActiveTeam()))
		{
			gDLL->getInterfaceIFace()->setDirty(Score_DIRTY_BIT, true);
		}
	}

	FAssert(isSharingVision(eIndex) == bNewValue);
}

bool CvTeam::isDefensivePact(TeamTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_abDefensivePact[eIndex];
}


void CvTeam::setDefensivePact(TeamTypes eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");

	if (isDefensivePact(eIndex) != bNewValue)
	{
		m_abDefensivePact[eIndex] = bNewValue;

		if ((getID() == GC.getGameINLINE().getActiveTeam()) || (eIndex == GC.getGameINLINE().getActiveTeam()))
		{
			gDLL->getInterfaceIFace()->setDirty(Score_DIRTY_BIT, true);
		}

		if (bNewValue && !GET_TEAM(eIndex).isDefensivePact(getID()))
		{
			CvWString szBuffer = gDLL->getText("TXT_KEY_MISC_PLAYERS_SIGN_DEFENSIVE_PACT", getName().GetCString(), GET_TEAM(eIndex).getName().GetCString());
			GC.getGameINLINE().addReplayMessage(REPLAY_MESSAGE_MAJOR_EVENT, getLeaderID(), szBuffer, -1, -1, (ColorTypes)GC.getInfoTypeForString("COLOR_HIGHLIGHT_TEXT"));
		}
	}
}


bool CvTeam::isForcePeace(TeamTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_abForcePeace[eIndex];
}


void CvTeam::setForcePeace(TeamTypes eIndex, bool bNewValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < MAX_TEAMS, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_abForcePeace[eIndex] = bNewValue;

	if (isForcePeace(eIndex))
	{
		if (AI_isSneakAttackPreparing(eIndex))
		{
			AI_setWarPlan(eIndex, NO_WARPLAN);
		}
	}
}

bool CvTeam::hasSeedling(BonusTypes eBonus) const
{
	FAssert(eBonus >= 0 && eBonus < GC.getNumBonusInfos());
	return m_pabHasSeedling[eBonus];
}

void CvTeam::setHasSeedling(BonusTypes eBonus, bool bNewValue)
{
	FAssert(eBonus >= 0 && eBonus < GC.getNumBonusInfos());
	m_pabHasSeedling[eBonus] = bNewValue;
}

int CvTeam::getUnitClassCount(UnitClassTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumUnitClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiUnitClassCount[eIndex];
}

void CvTeam::changeUnitClassCount(UnitClassTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumUnitClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_aiUnitClassCount[eIndex] += iChange;
	FAssert(getUnitClassCount(eIndex) >= 0);
}

int CvTeam::getBuildingClassCount(BuildingClassTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumBuildingClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiBuildingClassCount[eIndex];
}

void CvTeam::changeBuildingClassCount(BuildingClassTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumBuildingClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_aiBuildingClassCount[eIndex] += iChange;
	FAssert(getBuildingClassCount(eIndex) >= 0);
}

int CvTeam::getEuropeUnitsPurchased(UnitClassTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumUnitClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiEuropeUnitsPurchased[eIndex];
}

void CvTeam::changeEuropeUnitsPurchased(UnitClassTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumUnitClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_aiEuropeUnitsPurchased[eIndex] += iChange;
	FAssert(getEuropeUnitsPurchased(eIndex) >= 0);
}

void CvTeam::cancelDefensivePacts(TeamTypes eEndingTeam)
{
	CLLNode<TradeData>* pNode;
	CvDeal* pLoopDeal;
	bool bCancelDeal;
	int iLoop;

	for (pLoopDeal = GC.getGameINLINE().firstDeal(&iLoop); pLoopDeal != NULL; pLoopDeal = GC.getGameINLINE().nextDeal(&iLoop))
	{
		bCancelDeal = false;

		if ((GET_PLAYER(pLoopDeal->getFirstPlayer()).getTeam() == getID()) ||
			(GET_PLAYER(pLoopDeal->getSecondPlayer()).getTeam() == getID()))
		{
			for (pNode = pLoopDeal->headFirstTradesNode(); (pNode != NULL); pNode = pLoopDeal->nextFirstTradesNode(pNode))
			{
				if (pNode->m_data.m_eItemType == TRADE_DEFENSIVE_PACT)
				{
					bCancelDeal = true;
					break;
				}
			}

			if (!bCancelDeal)
			{
				for (pNode = pLoopDeal->headSecondTradesNode(); (pNode != NULL); pNode = pLoopDeal->nextSecondTradesNode(pNode))
				{
					if (pNode->m_data.m_eItemType == TRADE_DEFENSIVE_PACT)
					{
						bCancelDeal = true;
						break;
					}
				}
			}
		}

		if (bCancelDeal)
		{
			pLoopDeal->kill(true, eEndingTeam);
		}
	}
}


bool CvTeam::isFriendlyTerritory(TeamTypes eTeam) const
{
	if (eTeam == NO_TEAM)
	{
		return false;
	}

	if (eTeam == getID())
	{
		return true;
	}

	return false;
}

void CvTeam::setForceRevealedBonus(BonusTypes eBonus, bool bRevealed)
{
	if (isForceRevealedBonus(eBonus) == bRevealed)
	{
		return;
	}

	if (bRevealed)
	{
		m_aeRevealedBonuses.push_back(eBonus);
	}
	else
	{
		std::vector<BonusTypes>::iterator it;

		for (it = m_aeRevealedBonuses.begin(); it != m_aeRevealedBonuses.end(); ++it)
		{
			if (*it == eBonus)
			{
				m_aeRevealedBonuses.erase(it);
				break;
			}
		}
	}

	for (int iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); ++iI)
	{
		CvPlot* pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iI);

		if (pLoopPlot->getBonusType() == eBonus)
		{
			pLoopPlot->updateYield(true);
			pLoopPlot->setLayoutDirty(true);
		}
	}
}

bool CvTeam::isForceRevealedBonus(BonusTypes eBonus) const
{
	std::vector<BonusTypes>::const_iterator it;

	for (it = m_aeRevealedBonuses.begin(); it != m_aeRevealedBonuses.end(); ++it)
	{
		if (*it == eBonus)
		{
			return true;
		}
	}

	return false;
}

int CvTeam::countNumHumanGameTurnActive() const
{
	int iCount = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));

		if (kLoopPlayer.isHuman())
		{
			if (kLoopPlayer.isTurnActive())
			{
				++iCount;
			}
		}
	}

	return iCount;
}

void CvTeam::setTurnActive(bool bNewValue, bool bDoTurn)
{
	FAssert(GC.getGameINLINE().isSimultaneousTeamTurns());

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));

		if (kPlayer.isAlive())
		{
			kPlayer.setTurnActive(bNewValue, bDoTurn);
		}
	}
}

bool CvTeam::isTurnActive() const
{
	FAssert(GC.getGameINLINE().isSimultaneousTeamTurns());

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
		if (kPlayer.isTurnActive())
		{
			return true;
		}
	}

	return false;
}

int CvTeam::getRebelPercent() const
{
	PROFILE_FUNC();

	int iRebelSentiment = 0;
	int iTotalPopulation = 0;
	int iLoop;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
		if (kPlayer.isAlive() && kPlayer.getParent() != NO_PLAYER)
		{
			for (CvCity* pCity = kPlayer.firstCity(&iLoop); pCity != NULL; pCity = kPlayer.nextCity(&iLoop))
			{
				iRebelSentiment += pCity->getRebelSentiment();
				iTotalPopulation += pCity->getPopulation();
			}
			for (CvUnit* pLoopUnit = kPlayer.firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = kPlayer.nextUnit(&iLoop))
			{
				if (pLoopUnit->getUnitInfo().haveRebelPercent())
				{
					iRebelSentiment += pLoopUnit->getRebelSentiment();
					iTotalPopulation += 1;
				}
			}
		}
	}
	if (iTotalPopulation == 0)
	{
		return 0;
	}

	return range(iRebelSentiment / iTotalPopulation, 0, 100);
}

bool CvTeam::hasGoodCommercialRelationship(TeamTypes eTeam) const
{
	if (eTeam == NO_TEAM) 
	{
		return false;
	}
	CvTeam& kOtherTeam = GET_TEAM(eTeam);

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
		if (kPlayer.isAlive()) 
		{
			for (int iJ = 0; iJ < kOtherTeam.getPlayerCount(); iJ++)
			{
				CvPlayer& kOtherPlayer = GET_PLAYER(kOtherTeam.getPlayer(iJ));
				if (kOtherPlayer.isAlive()) 
				{
					if (kPlayer.getRelationTradeTo(kOtherPlayer.getID()) >= 6) 
					{
						return true;
					}
				}
			}
		}
	}

	return false;
}

bool CvTeam::canDoRevolution() const
{
	if (GC.getGameINLINE().getGameState() != GAMESTATE_ON)
	{
		return false;
	}

	bool bRevolution = false;
	for (int iVictory = 0; iVictory < GC.getNumVictoryInfos() && !bRevolution; ++iVictory)
	{
		if (GC.getGameINLINE().isVictoryValid((VictoryTypes) iVictory))
		{
			CvVictoryInfo& kVictory = GC.getVictoryInfo((VictoryTypes) iVictory);
			if (GC.getVictoryInfo((VictoryTypes) iVictory).isRevolution())
			{
				bRevolution = true;
			}
		}
	}

	if (!bRevolution)
	{
		return false;
	}

	if (isInRevolution())
	{
		return false;
	}

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
		if (kPlayer.isAlive()) 
		{
			NewEraTypes eNewEra = (NewEraTypes)kPlayer.getNewEra();
			if (eNewEra != NO_ERA && GC.getNewEraInfo(eNewEra).allowIndependence())
			{
				return true;
			}
		}
	}

	return false;
}

bool CvTeam::isInRevolution() const
{
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
		if (kPlayer.isAlive() && kPlayer.isInRevolution())
		{
			return true;
		}
	}

	return false;
}
void CvTeam::doRevolution()
{
	if (!canDoRevolution())
	{
		return;
	}

	CvGame& kGame = GC.getGameINLINE();
	if (!kGame.isMaxTurnsExtended())
	{
		int iRevolutionTurns = GC.getGameSpeedInfo(kGame.getGameSpeedType()).getRevolutionTurns();
		if (iRevolutionTurns + kGame.getElapsedGameTurns() > kGame.getMaxTurns())
		{
			kGame.setMaxTurns(iRevolutionTurns + kGame.getElapsedGameTurns());
			kGame.setEstimateEndTurn(iRevolutionTurns + kGame.getGameTurn());
			kGame.setMaxTurnsExtended(true);
		}
	}

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
		if (kPlayer.isAlive() && kPlayer.getParent() != NO_PLAYER)
		{
			CvPlayerAI& kParent = GET_PLAYER(kPlayer.getParent());
			GET_TEAM(kParent.getTeam()).declareWarNoRevolution(getID(), true, WARPLAN_TOTAL, false);

			//delete all pirate ships of this country
			int iLoop;
			for (CvUnit* pLoopUnit = kParent.firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = kParent.nextUnit(&iLoop))
			{
				if (pLoopUnit->getUnitInfo().isPirate())
				{
					pLoopUnit->kill(false);
				}
			}
			//create REF in Europe
			for (int i = 0; i < kPlayer.getNumRevolutionEuropeUnits(); ++i)
			{
				CvUnit* pRevolutionUnit = NULL;
				if (GC.getUnitInfo(kPlayer.getRevolutionEuropeUnit(i)).getDomainType() == DOMAIN_SEA)
				{
					CvPlot* pOceanPlot = kParent.AI_getImperialShipSpawnPlot();
					if (pOceanPlot != NULL)
					{
						pRevolutionUnit = kParent.initUnit(kPlayer.getRevolutionEuropeUnit(i), kPlayer.getRevolutionEuropeProfession(i), pOceanPlot->getX_INLINE(), pOceanPlot->getY_INLINE());
						pRevolutionUnit->setUnitTravelState(UNIT_TRAVEL_STATE_IN_EUROPE, false);
						int iRandom = GC.getGameINLINE().getSorenRandNum(100, "random promotions");
						int iMilitaryLevel = std::max(2, iRandom%4 + (iRandom%3 == 0 ? 1 : 0) + (iRandom%2 == 0 ? 1 : 0));
						pRevolutionUnit->addSailorPromotions(0, 3, iMilitaryLevel);
					}
				}
				else
				{
					pRevolutionUnit = kParent.initEuropeUnit(kPlayer.getRevolutionEuropeUnit(i));
					pRevolutionUnit->setProfession(kPlayer.getRevolutionEuropeProfession(i));
				}
				FAssert(pRevolutionUnit != NULL);
			}
			kPlayer.clearRevolutionEuropeUnits();
			kPlayer.setYieldEuropeTradableAll();
			kPlayer.doEra();
			//All european players meet this king and will be able to declare war to him
			for (int iPlayer2 = 0; iPlayer2 < MAX_PLAYERS; ++iPlayer2) 
			{
				CvPlayer& kOtherPlayer = GET_PLAYER((PlayerTypes) iPlayer2);
				if (kOtherPlayer.isAlive() && kOtherPlayer.isEuropean())
				{
					if (kOtherPlayer.getTeam() != getID()) 
					{
						GET_TEAM(kOtherPlayer.getTeam()).makeHasMet(kParent.getTeam(), false);
						GET_TEAM(kParent.getTeam()).makeHasMet(kOtherPlayer.getTeam(), false);
					}
				}
			}
		}
	}

	if (GC.getGameINLINE().getActiveTeam() == getID())
	{
		gDLL->getInterfaceIFace()->setDirty(ColoredPlots_DIRTY_BIT, true);
	}

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
		if (kPlayer.isAlive())
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_MOVIE);
			pInfo->setText(CvWString("ART_DEF_MOVIE_REVOLUTION"));
			gDLL->getInterfaceIFace()->addPopup(pInfo, kPlayer.getID());

			for (int iCivicOption = 0; iCivicOption < GC.getNumCivicOptionInfos(); ++iCivicOption)
			{
				if (kPlayer.isHuman())
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CIVIC_OPTION, iCivicOption);
					gDLL->getInterfaceIFace()->addPopup(pInfo, kPlayer.getID());
				}
				else
				{
					kPlayer.AI_chooseCivic((CivicOptionTypes) iCivicOption);
				}
			}
		}
	}
}

bool CvTeam::isParentOf(TeamTypes eChildTeam) const
{
	for (int i = 0; i < MAX_PLAYERS; ++i)
	{
		CvPlayer& kChildPlayer = GET_PLAYER((PlayerTypes) i);
		if (kChildPlayer.isEverAlive() && (kChildPlayer.getTeam() == eChildTeam))
		{
			PlayerTypes eParent = kChildPlayer.getParent();
			if (eParent != NO_PLAYER)
			{
				CvPlayer& kParentPlayer = GET_PLAYER(eParent);
				if (kParentPlayer.getTeam() == getID())
				{
					return true;
				}
			}
		}
	}

	return false;
}

bool CvTeam::checkIndependence() const
{
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
		if (kPlayer.isAlive())
		{
			// One player on the team is enough for team independence
			if (kPlayer.checkIndependence())
			{
				return true;
			}
		}
	}

	return false;
}

NewEraTypes CvTeam::getNewEra() const
{
	return m_eNewEra;
}

void CvTeam::setNewEra(NewEraTypes eNewValue)
{
	m_eNewEra = eNewValue;
}

void CvTeam::changeToTheNextNewEra()
{
	NewEraTypes eNextNewEra = getNextNewEra();
	if (eNextNewEra != NO_NEW_ERA) 
	{
		bool bExistTeamOnSpecificNewEra = GC.getGameINLINE().existTeamOnSpecificNewEra(eNextNewEra);
		bool hasReachedMaxTurnRequirementThanksToAnotherEuropean = bExistTeamOnSpecificNewEra && hasReachedMaxTurnRequirement(eNextNewEra);

		setNewEra(eNextNewEra);

		// If it is allowed in XML file, we will define the turn where all european players will reach the next Era
		unsigned char minTurnToPassAutomaticlyNewEraAfterOneTeamReachIt = GC.getNewEraInfo(eNextNewEra).getMinTurnToPassAutomaticlyNewEraAfterOneTeamReachIt();
		if (!bExistTeamOnSpecificNewEra && minTurnToPassAutomaticlyNewEraAfterOneTeamReachIt > 0) 
		{
			unsigned char randTurnToPassAutomaticlyNewEraAfterOneTeamReachIt = GC.getNewEraInfo(eNextNewEra).getRandTurnToPassAutomaticlyNewEraAfterOneTeamReachIt();

			GC.getGameINLINE().setAutomaticNewEraTurn(GC.getGameINLINE().getGameTurn() + minTurnToPassAutomaticlyNewEraAfterOneTeamReachIt + GC.getGameINLINE().getSorenRandNum(randTurnToPassAutomaticlyNewEraAfterOneTeamReachIt, "Turn to pass automaticly new era after one team reach it"));
		}

		for (int iI = 0; iI < getPlayerCount(); iI++)
		{
			CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
			if (kPlayer.isAlive())
			{
				kPlayer.haveNewEra(eNextNewEra, hasReachedMaxTurnRequirementThanksToAnotherEuropean);
			}
		}
	}
}

NewEraTypes CvTeam::getNextNewEra() const
{
	if (getNewEra() + 1 >= GC.getNumNewEraInfos())
	{
		return NO_NEW_ERA;
	}
	return (NewEraTypes) (getNewEra() + 1);
}

const CvRequirementsNewEra* CvTeam::getRequirementsNewEra(NewEraTypes eNewEra) const
{
	CvHandicapInfo& kHandicapInfo = GC.getHandicapInfo(getHandicapType());
	return kHandicapInfo.getRequirementsNewEra(eNewEra);
}

int CvTeam::getNewEraValuesRequires(int iOriginal, int iOffset, bool bUseGlobalValue) const
{
	if (iOffset <= 0) 
	{
		return iOriginal;
	}
	int iIndex = (iOriginal/2 + iOffset) % NUM_RAND_VALUES;
	int iRandomVar = bUseGlobalValue ?  GC.getGameINLINE().getRandomValue() : getRandomValue(iIndex);//bUseGlobalValue is true if we don't want difeerence between player
	int iChangeRand = iRandomVar*(iOffset*2)/100 - iOffset;
	return iOriginal + iChangeRand;
}

bool CvTeam::canHaveNextNewEra() const
{
	NewEraTypes eNextNewEra = getNextNewEra();
	if (eNextNewEra == NO_NEW_ERA) 
	{
		return false;
	}

	CvNewEraInfo& kNewEra = GC.getNewEraInfo(eNextNewEra);
	bool isNewEraVictory = GC.getGameINLINE().isNewEraVictory(eNextNewEra);

	// ---------- First case, if we have reached the max turn to have the new era ----------

	// Max turn
	if (hasReachedMaxTurnRequirement(eNextNewEra)) 
	{
		return true;
	}

	// ---------- End case ----------

	// ----------  Second case, we need to reached all criteria below ----------

	// Gold
	if (!hasReachedGoldRequirement(eNextNewEra)) 
	{
		return false;
	}

	// Raw materials sold
	if (!hasReachedRawMaterialsSoldRequirement(eNextNewEra)) 
	{
		return false;
	}

	// Num Cities
	if (!hasReachedNumCitiesRequirement(eNextNewEra)) 
	{
		return false;
	}

	// Min turn
	if (!hasReachedMinTurnRequirement(eNextNewEra)) 
	{
		return false;
	}

	// Rebel percent
	if (!hasReacheRebelPercentRequirement(eNextNewEra)) 
	{
		return false;
	}

	// Crosses stored
	if (!hasReachedCrossesStoredRequirement(eNextNewEra)) 
	{
		return false;
	}

	// Min citizen by colony
	if (!hasReachedCitizenByColonyRequirement(eNextNewEra)) 
	{
		return false;
	}

	// Min defenders by colony
	if (!hasReachedDefendersByColonyRequirement(eNextNewEra)) 
	{
		return false;
	}

	// Land to discover
	if (!hasReachedLandDiscoveredRequirement(eNextNewEra)) 
	{
		return false;
	}

	// ---------- End case ----------

	return true; 
}

int CvTeam::getIdealTurn(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = getRequirementsNewEra(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}

	return getNewEraValuesRequires(pRequirementsNewEra->getIdealTurn(), pRequirementsNewEra->getRandTurn());
}

int CvTeam::getCurrentTurn() const
{
	return GC.getGameINLINE().getGameTurn() + 1;
}

int CvTeam::getRequiredMaxTurn(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = getRequirementsNewEra(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}

	int iRequiredMaxTurn = getNewEraValuesRequires(pRequirementsNewEra->getMaxTurn(), pRequirementsNewEra->getRandTurn());

	if (GC.getNewEraInfo(eNewEra).getMinTurnToPassAutomaticlyNewEraAfterOneTeamReachIt() > 0)
	{
		if (GC.getGameINLINE().existTeamOnSpecificNewEra(eNewEra))
		{
			int iAutomaticNewEraTurn = GC.getGameINLINE().getAutomaticNewEraTurn();
			iRequiredMaxTurn = std::min(iRequiredMaxTurn, iAutomaticNewEraTurn);
		}
	}

	return iRequiredMaxTurn;
}

bool CvTeam::hasReachedMaxTurnRequirement(NewEraTypes eNewEra) const
{
	int iRequiredTurn = getRequiredMaxTurn(eNewEra);
	if (iRequiredTurn <= 0)
	{
		return false;
	}

	return getCurrentTurn() >= iRequiredTurn;
}

int CvTeam::getIndustryHistory(int iTurn) const
{
	int iTotalIndustry = 0;
	int iCountPlayer = 0; 
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kPlayer = GET_PLAYER(getPlayer(iI));
		if (kPlayer.isAlive() && kPlayer.isEuropean())
		{
			iTotalIndustry += kPlayer.getIndustryHistory(iTurn);
			iCountPlayer++;
		}
	}
	return iCountPlayer > 0 ? iTotalIndustry/iCountPlayer : iCountPlayer++;
}

long CvTeam::getGold() const
{
	long lTotalGold = 0;

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			if (kLoopPlayer.isEuropean())
			{
				lTotalGold += kLoopPlayer.getGold();
			}
		}
	}
	return lTotalGold;
}

long CvTeam::getRequiredGold(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = getRequirementsNewEra(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}

	unsigned char ucAliveEuropeanPlayers = getAliveEuropeanPlayers();
	if (ucAliveEuropeanPlayers == 0)
	{
		return 0;
	}

	return getNewEraValuesRequires(pRequirementsNewEra->getMinGold(), pRequirementsNewEra->getRandMinGold()) * ucAliveEuropeanPlayers;
}

bool CvTeam::hasReachedGoldRequirement(NewEraTypes eNewEra) const
{
	long lRequiredGold = getRequiredGold(eNewEra);
	if (lRequiredGold <= 0)
	{
		return true;
	}
	return getGold() >= lRequiredGold;
}

int CvTeam::getNewWorldYieldQuantity() const
{
	int iTotal = 0;
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			int iNewWorldYieldQuantity = kLoopPlayer.getNewWorldYieldQuantity();
			if (!isHuman())
			{
				iNewWorldYieldQuantity += iNewWorldYieldQuantity * 30 / 100;
			}
			iTotal += iNewWorldYieldQuantity;
		}
	}
	
	return iTotal;
}

int CvTeam::getRequiredRawMaterialsSold(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = getRequirementsNewEra(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}

	unsigned char ucAliveEuropeanPlayers = getAliveEuropeanPlayers();
	if (ucAliveEuropeanPlayers == 0)
	{
		return 0;
	}

	return getNewEraValuesRequires(pRequirementsNewEra->getRawMaterialsSold(), pRequirementsNewEra->getRandRawMaterialsSold()) * ucAliveEuropeanPlayers;
}

bool CvTeam::hasReachedRawMaterialsSoldRequirement(NewEraTypes eNewEra) const
{
	int iRequiredRawMaterialsSold = getRequiredRawMaterialsSold(eNewEra);
	if (iRequiredRawMaterialsSold <= 0)
	{
		return true;
	}
	return getNewWorldYieldQuantity() >= iRequiredRawMaterialsSold;
}

int CvTeam::getRequiredNumCities(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = getRequirementsNewEra(eNewEra);
	bool isNewEraVictory = GC.getGameINLINE().isNewEraVictory(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}

	unsigned char ucAliveEuropeanPlayers = getAliveEuropeanPlayers();
	if (ucAliveEuropeanPlayers == 0)
	{
		return 0;
	}

	return getNewEraValuesRequires(pRequirementsNewEra->getNumCities(), pRequirementsNewEra->getRandNumCities(), isNewEraVictory) * ucAliveEuropeanPlayers;
}

bool CvTeam::hasReachedNumCitiesRequirement(NewEraTypes eNewEra) const
{
	int iRequiredNumCities = getRequiredNumCities(eNewEra);
	if (iRequiredNumCities <= 0)
	{
		return true;
	}
	return getNumEuropeanCities() >= iRequiredNumCities;
}

int CvTeam::getRequiredMinTurn(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = getRequirementsNewEra(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}

	return getNewEraValuesRequires(pRequirementsNewEra->getMinTurn(), pRequirementsNewEra->getRandTurn());
}

bool CvTeam::hasReachedMinTurnRequirement(NewEraTypes eNewEra) const
{
	bool isNewEraVictory = GC.getGameINLINE().isNewEraVictory(eNewEra);
	if (isNewEraVictory)
	{
		return true; // We don't have this requirement if the era passed in paremeter is the one to win the game
	}

	int iRequiredTurn = getRequiredMinTurn(eNewEra);
	if (iRequiredTurn <= 0)
	{
		return true;
	}
	return getCurrentTurn() >= iRequiredTurn;
}

int CvTeam::getRequiredRebelPercent(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = getRequirementsNewEra(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}

	return getNewEraValuesRequires(pRequirementsNewEra->getRebelPercent(), pRequirementsNewEra->getRandRebelPercent());
}

bool CvTeam::hasReacheRebelPercentRequirement(NewEraTypes eNewEra, bool bCloseToReach) const
{
	int iRequiredRebelPercent = getRequiredRebelPercent(eNewEra);
	if (iRequiredRebelPercent <= 0)
	{
		return true;
	}

	if (bCloseToReach)
	{
		return getRebelPercent() >= iRequiredRebelPercent * 90 / 100;
	}

	return getRebelPercent() >= iRequiredRebelPercent;
}

int CvTeam::getCrossesStored() const
{
	int iTotal = 0;
	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			iTotal += kLoopPlayer.getCrossesStored();
		}
	}
	return iTotal;
}

int CvTeam::getRequiredCrossesStored(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = getRequirementsNewEra(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}

	unsigned char ucAliveEuropeanPlayers = getAliveEuropeanPlayers();
	if (ucAliveEuropeanPlayers == 0)
	{
		return 0;
	}

	return getNewEraValuesRequires(pRequirementsNewEra->getReligiousInfluence(), pRequirementsNewEra->getRandReligiousInfluence()) * ucAliveEuropeanPlayers;
}

bool CvTeam::hasReachedCrossesStoredRequirement(NewEraTypes eNewEra, bool bCloseToReach) const
{
	int iRequiredCrossesStored = getRequiredCrossesStored(eNewEra);
	if (iRequiredCrossesStored <= 0)
	{
		return true;
	}

	if (bCloseToReach)
	{
		return getCrossesStored() >= iRequiredCrossesStored * 90 / 100;
	}

	return getCrossesStored() >= iRequiredCrossesStored;
}

int CvTeam::getRequiredCitizenByColony(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = GC.getHandicapInfo(getHandicapType()).getRequirementsNewEra(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}
	return getNewEraValuesRequires(pRequirementsNewEra->getCitizenByColony(), pRequirementsNewEra->getRandCitizenByColony());
}		

bool CvTeam::hasReachedCitizenByColonyRequirement(NewEraTypes eNewEra) const
{
	int iRequiredCitizenByColony = getRequiredCitizenByColony(eNewEra);
	if (iRequiredCitizenByColony == 0) 
	{
		return true;
	}

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			if (kLoopPlayer.isEuropean())
			{
				if (!kLoopPlayer.hasEnoughCitizenByColony(iRequiredCitizenByColony))
				{
					return false;
				}
			}
		}
	}
	return true;
}

int CvTeam::getRequiredDefendersByColony(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = GC.getHandicapInfo(getHandicapType()).getRequirementsNewEra(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}
	return getNewEraValuesRequires(pRequirementsNewEra->getDefenderByColony(), pRequirementsNewEra->getRandDefenderByColony());
}		

bool CvTeam::hasReachedDefendersByColonyRequirement(NewEraTypes eNewEra) const
{
	int iRequiredDefendersByColony = getRequiredDefendersByColony(eNewEra);
	if (iRequiredDefendersByColony == 0)
	{
		return true;
	}

	for (int iI = 0; iI < getPlayerCount(); iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER(getPlayer(iI));
		if (kLoopPlayer.isAlive())
		{
			if (kLoopPlayer.isEuropean())
			{
				if (!kLoopPlayer.hasEnoughDefendersByColony(iRequiredDefendersByColony))
				{
					return false;
				}
			}
		}
	}
	return true;
}

int CvTeam::getLandDiscovered() const
{
	return m_iLandDiscovered;
}

void CvTeam::incrementLandDiscovered()
{
	m_iLandDiscovered++;
}

int CvTeam::getRequiredLandDiscovered(NewEraTypes eNewEra) const
{
	const CvRequirementsNewEra* pRequirementsNewEra = GC.getHandicapInfo(getHandicapType()).getRequirementsNewEra(eNewEra);
	if (pRequirementsNewEra == NULL)
	{
		return 0;
	}

	unsigned char ucAliveEuropeanPlayers = getAliveEuropeanPlayers();
	if (ucAliveEuropeanPlayers == 0)
	{
		return 0;
	}

	CvMap& kMap = GC.getMapINLINE();
	int iLandPlots = std::min(kMap.getLandPlots(), GC.getWorldInfo(kMap.getWorldSize()).getLandPlotsToTarget());
	int iPercent = getNewEraValuesRequires(pRequirementsNewEra->getLandDiscoveredPercent(), pRequirementsNewEra->getRandLandDiscoveredPercent());
	iPercent = std::min(iPercent + (ucAliveEuropeanPlayers - 1) * 15, 100);//More we have players in a team, more they have to discover lands

	return iLandPlots * iPercent / 100;
}		

bool CvTeam::hasReachedLandDiscoveredRequirement(NewEraTypes eNewEra) const
{
	int iRequiredLandDiscovered = getRequiredLandDiscovered(eNewEra);
	if (iRequiredLandDiscovered == 0)
	{
		return true;
	}
	return getLandDiscovered() >= iRequiredLandDiscovered;
}

int CvTeam::getAliveEuropeanPlayers() const
{
	return m_ucAliveEuropeanPlayers;
}

void CvTeam::changeAliveEuropeanPlayers(unsigned char ucChange)
{
	m_ucAliveEuropeanPlayers += ucChange;
}

int CvTeam::getNumEuropeanCities() const
{
	return m_ucNumEuropeanCities;
}

void CvTeam::changeNumEuropeanCities(int iChange)
{
	m_ucNumEuropeanCities += iChange;
	FAssert(getNumEuropeanCities() >= 0);
}

void CvTeam::addPlayer(int iPlayerId)
{
	m_aPlayers.push_back(iPlayerId);
}

int CvTeam::getPlayerCount() const
{
	return m_aPlayers.size();
}

PlayerTypes CvTeam::getPlayer(int index) const
{
	return (PlayerTypes) m_aPlayers[index];
}


void CvTeam::read(FDataStreamBase* pStream)
{
	// Init data before load
	reset();

	uint uiFlag=0;
	pStream->Read(&uiFlag);	// flags for expansion

	pStream->Read(&m_iNumMembers);
	pStream->Read(&m_iAliveCount);
	pStream->Read(&m_iEverAliveCount);
	pStream->Read(&m_iNumCities);
	pStream->Read(&m_iTotalLand);
	pStream->Read(&m_iMapTradingCount);
	pStream->Read(&m_iGoldTradingCount);
	pStream->Read(&m_iOpenBordersTradingCount);
	pStream->Read(&m_iDefensivePactTradingCount);
	pStream->Read(&m_iPermanentAllianceTradingCount);
	pStream->Read(&m_iLandDiscovered);

	pStream->Read(&m_ucAliveEuropeanPlayers);
	pStream->Read(&m_ucNumEuropeanCities);

	pStream->Read(NUM_RAND_VALUES, m_aucRandomValues);

	pStream->Read(&m_bMapCentering);
	pStream->Read(&m_bSharingVision);

	pStream->Read((int*)&m_eID);
	pStream->Read((int*)&m_eNewEra);

	pStream->Read(MAX_TEAMS, m_abHasMet);
	pStream->Read(MAX_TEAMS, m_abAtWar);
	pStream->Read(MAX_TEAMS, m_abIsFirstTurnOfWar);
	pStream->Read(MAX_TEAMS, m_abPermanentWarPeace);
	pStream->Read(MAX_TEAMS, m_abOpenBorders);
	pStream->Read(MAX_TEAMS, m_abSharingVisions);
	pStream->Read(MAX_TEAMS, m_abDefensivePact);
	pStream->Read(MAX_TEAMS, m_abForcePeace);
	pStream->Read(GC.getNumBonusInfos(), m_pabHasSeedling);

	pStream->Read(GC.getNumUnitClassInfos(), m_aiUnitClassCount);
	pStream->Read(GC.getNumBuildingClassInfos(), m_aiBuildingClassCount);
	pStream->Read(GC.getNumUnitClassInfos(), m_aiEuropeUnitsPurchased);

	int iSize;
	m_aeRevealedBonuses.clear();
	pStream->Read(&iSize);
	for (int i = 0; i < iSize; ++i)
	{
		BonusTypes eBonus;
		pStream->Read((int*)&eBonus);
		m_aeRevealedBonuses.push_back(eBonus);
	}
}

void CvTeam::write(FDataStreamBase* pStream)
{
	uint uiFlag = 0;
	pStream->Write(uiFlag);		// flag for expansion
	pStream->Write(m_iNumMembers);
	pStream->Write(m_iAliveCount);
	pStream->Write(m_iEverAliveCount);
	pStream->Write(m_iNumCities);
	pStream->Write(m_iTotalLand);
	pStream->Write(m_iMapTradingCount);
	pStream->Write(m_iGoldTradingCount);
	pStream->Write(m_iOpenBordersTradingCount);
	pStream->Write(m_iDefensivePactTradingCount);
	pStream->Write(m_iPermanentAllianceTradingCount);
	pStream->Write(m_iLandDiscovered);

	pStream->Write(m_ucAliveEuropeanPlayers);
	pStream->Write(m_ucNumEuropeanCities);

	pStream->Write(NUM_RAND_VALUES, m_aucRandomValues);

	pStream->Write(m_bMapCentering);
	pStream->Write(m_bSharingVision);

	pStream->Write(m_eID);
	pStream->Write(m_eNewEra);

	pStream->Write(MAX_TEAMS, m_abHasMet);
	pStream->Write(MAX_TEAMS, m_abAtWar);
	pStream->Write(MAX_TEAMS, m_abIsFirstTurnOfWar);
	pStream->Write(MAX_TEAMS, m_abPermanentWarPeace);
	pStream->Write(MAX_TEAMS, m_abOpenBorders);
	pStream->Write(MAX_TEAMS, m_abSharingVisions);
	pStream->Write(MAX_TEAMS, m_abDefensivePact);
	pStream->Write(MAX_TEAMS, m_abForcePeace);
	pStream->Write(GC.getNumBonusInfos(), m_pabHasSeedling);

	pStream->Write(GC.getNumUnitClassInfos(), m_aiUnitClassCount);
	pStream->Write(GC.getNumBuildingClassInfos(), m_aiBuildingClassCount);
	pStream->Write(GC.getNumUnitClassInfos(), m_aiEuropeUnitsPurchased);
	pStream->Write(m_aeRevealedBonuses.size());
	for (std::vector<BonusTypes>::iterator it = m_aeRevealedBonuses.begin(); it != m_aeRevealedBonuses.end(); ++it)
	{
		pStream->Write(*it);
	}
}
// CACHE: cache frequently used values
///////////////////////////////////////
