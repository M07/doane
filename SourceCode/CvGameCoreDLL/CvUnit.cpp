// unit.cpp

#include "CvGameCoreDLL.h"
#include "CvUnit.h"
#include "CvArea.h"
#include "CvPlot.h"
#include "CvCity.h"
#include "CvGlobals.h"
#include "CvGameCoreUtils.h"
#include "CvGameAI.h"
#include "CvMap.h"
#include "CvPlayerAI.h"
#include "CvRandom.h"
#include "CvTeamAI.h"
#include "CvGameCoreUtils.h"
#include "CyUnit.h"
#include "CyArgsList.h"
#include "CyPlot.h"
#include "CvDLLEntityIFaceBase.h"
#include "CvDLLInterfaceIFaceBase.h"
#include "CvDLLEngineIFaceBase.h"
#include "CvDLLEventReporterIFaceBase.h"
#include "CvDLLPythonIFaceBase.h"
#include "CvDLLFAStarIFaceBase.h"
#include "CvGameTextMgr.h"
#include "CvInfos.h"
#include "FProfiler.h"
#include "CvPopupInfo.h"
#include "CvArtFileMgr.h"
#include "CvDiploParameters.h"
#include "CvTradeRoute.h"
#include "CvCityJob.h"
#include "CvAgreement.h"
#include "CvSeaway.h"
#include "CvConvoy.h"

// Public Functions...

CvUnitTemporaryStrengthModifier::CvUnitTemporaryStrengthModifier(CvUnit* pUnit, ProfessionTypes eProfession) :
	m_pUnit(pUnit),
	m_eProfession(eProfession)
{
	if (m_pUnit != NULL)
	{
		m_pUnit->processProfessionStats(m_pUnit->getProfession(), -1);
		m_pUnit->processProfessionStats(m_eProfession, 1);
	}
}

CvUnitTemporaryStrengthModifier::~CvUnitTemporaryStrengthModifier()
{
	if (m_pUnit != NULL)
	{
		m_pUnit->processProfessionStats(m_eProfession, -1);
		m_pUnit->processProfessionStats(m_pUnit->getProfession(), 1);
	}
}


CvUnit::CvUnit() :
	m_pabHasRealPromotion(NULL),
	m_paiFreePromotionCount(NULL),
	m_paiTerrainDoubleMoveCount(NULL),
	m_paiFeatureDoubleMoveCount(NULL),
	m_paiExtraTerrainAttackPercent(NULL),
	m_paiExtraTerrainDefensePercent(NULL),
	m_paiExtraFeatureAttackPercent(NULL),
	m_paiExtraFeatureDefensePercent(NULL),
	m_paiExtraUnitCombatModifier(NULL),
	m_paiExtraUnitClassAttackModifier(NULL),
	m_paiExtraUnitClassDefenseModifier(NULL),
	m_paiProductionToWork(NULL),
	m_eUnitType(NO_UNIT),
	m_iID(-1)
{
	m_aiExtraDomainModifier = new int[NUM_DOMAIN_TYPES];
	m_aiCargoYield = new int[NUM_YIELD_TYPES];

	CvDLLEntity::createUnitEntity(this);		// create and attach entity to unit

	reset(0, NO_UNIT, NO_PLAYER, true);
}


CvUnit::~CvUnit()
{
	if (!gDLL->GetDone() && GC.IsGraphicsInitialized())						// don't need to remove entity when the app is shutting down, or crash can occur
	{
		gDLL->getEntityIFace()->RemoveUnitFromBattle(this);
		CvDLLEntity::removeEntity();		// remove entity from engine
	}

	CvDLLEntity::destroyEntity();			// delete CvUnitEntity and detach from us

	uninit();

	SAFE_DELETE_ARRAY(m_aiExtraDomainModifier);
	SAFE_DELETE_ARRAY(m_aiCargoYield);
}

void CvUnit::reloadEntity()
{
	//has not been initialized so don't reload
	if (!gDLL->getEntityIFace()->isInitialized(getEntity()))
	{
		return;
	}

	bool bSelected = IsSelected();

	//destroy old entity
	if (!gDLL->GetDone() && GC.IsGraphicsInitialized())						// don't need to remove entity when the app is shutting down, or crash can occur
	{
		gDLL->getEntityIFace()->RemoveUnitFromBattle(this);
		CvDLLEntity::removeEntity();		// remove entity from engine
	}

	CvDLLEntity::destroyEntity();			// delete CvUnitEntity and detach from us

	//creat new one
	CvDLLEntity::createUnitEntity(this);		// create and attach entity to unit
	setupGraphical();
	if (bSelected)
	{
		gDLL->getInterfaceIFace()->insertIntoSelectionList(this, false, false);
	}
}


void CvUnit::init(int iID, UnitTypes eUnit, ProfessionTypes eProfession, UnitAITypes eUnitAI, PlayerTypes eOwner, int iX, int iY, DirectionTypes eFacingDirection, int iYieldStored)
{
	CvWString szBuffer;
	int iUnitName;
	int iI, iJ;

	FAssert(NO_UNIT != eUnit);

	//--------------------------------
	// Init saved data
	reset(iID, eUnit, eOwner);

	m_iYieldStored = iYieldStored;
	m_eFacingDirection = eFacingDirection;
	if (m_eFacingDirection == NO_DIRECTION)
	{
		CvPlot* pPlot = GC.getMapINLINE().plotINLINE(iX, iY);
		if ((pPlot != NULL) && pPlot->isWater() && (getDomainType() == DOMAIN_SEA))
		{
			m_eFacingDirection = (DirectionTypes) GC.getDefineINT("WATER_UNIT_FACING_DIRECTION");
		}
		else
		{
			m_eFacingDirection = DIRECTION_SOUTH;
		}
	}

	iUnitName = GC.getGameINLINE().getUnitCreatedCount(getUnitType());
	int iNumNames = m_pUnitInfo->getNumUnitNames();
	if (iUnitName < iNumNames)
	{
		int iOffset = GC.getGameINLINE().getSorenRandNum(iNumNames, "Unit name selection");

		for (iI = 0; iI < iNumNames; iI++)
		{
			int iIndex = (iI + iOffset) % iNumNames;
			CvWString szName = gDLL->getText(m_pUnitInfo->getUnitNames(iIndex));
			if (!GC.getGameINLINE().isGreatGeneralBorn(szName))
			{
				setName(szName);
				GC.getGameINLINE().addGreatGeneralBornName(szName);
				break;
			}
		}
	}

	setGameTurnCreated(GC.getGameINLINE().getGameTurn());

	GC.getGameINLINE().incrementUnitCreatedCount(getUnitType());
	GC.getGameINLINE().incrementUnitClassCreatedCount((UnitClassTypes)(m_pUnitInfo->getUnitClassType()));

	updateOwnerCache(1);

	for (iI = 0; iI < GC.getNumPromotionInfos(); iI++)
	{
		if (m_pUnitInfo->getFreePromotions(iI))
		{
			setHasRealPromotion(((PromotionTypes)iI), true);
		}
	}

	if (NO_UNITCLASS != getUnitClassType())
	{
		for (iJ = 0; iJ < GC.getNumPromotionInfos(); iJ++)
		{
			if (GET_PLAYER(getOwnerINLINE()).isFreePromotion(getUnitClassType(), (PromotionTypes)iJ))
			{
				changeFreePromotionCount(((PromotionTypes)iJ), 1);
			}
		}
	}

	processUnitCombatType((UnitCombatTypes) m_pUnitInfo->getUnitCombatType(), 1);

	updateBestLandCombat();

	AI_init();

	setProfession(eProfession);

	if (isNative() || GET_PLAYER(getOwnerINLINE()).isEurope())
	{
		std::vector<int> aiPromo(GC.getNumPromotionInfos(), 0);
		for (iI = 0; iI < GC.getNumPromotionInfos(); iI++)
		{
			aiPromo[iI] = iI;
		}
		GC.getGameINLINE().getSorenRand().shuffleArray(aiPromo, NULL);

		for (iI = 0; iI < GC.getNumPromotionInfos(); iI++)
		{
			PromotionTypes eLoopPromotion = (PromotionTypes)aiPromo[iI];
			if (canAcquirePromotion(eLoopPromotion))
			{
				if (GC.getPromotionInfo(eLoopPromotion).getPrereqPromotion() != NO_PROMOTION || GC.getPromotionInfo(eLoopPromotion).getPrereqOrPromotion1() != NO_PROMOTION)
				{
					if (GC.getGameINLINE().getSorenRandNum(100, "AI free native/europe promotion") < 25)
					{
						setHasRealPromotion(eLoopPromotion, true);
						break;
					}
				}
			}
		}
	}

	setHaveNewMilitaryProfession(false);

	addToMap(iX, iY);
	AI_setUnitAIType(eUnitAI);

	gDLL->getEventReporterIFace()->unitCreated(this);

	FAssert(GET_PLAYER(getOwnerINLINE()).checkPopulation());
}


void CvUnit::uninit()
{
	SAFE_DELETE_ARRAY(m_pabHasRealPromotion);
	SAFE_DELETE_ARRAY(m_paiFreePromotionCount);
	SAFE_DELETE_ARRAY(m_paiTerrainDoubleMoveCount);
	SAFE_DELETE_ARRAY(m_paiFeatureDoubleMoveCount);
	SAFE_DELETE_ARRAY(m_paiExtraTerrainAttackPercent);
	SAFE_DELETE_ARRAY(m_paiExtraTerrainDefensePercent);
	SAFE_DELETE_ARRAY(m_paiExtraFeatureAttackPercent);
	SAFE_DELETE_ARRAY(m_paiExtraFeatureDefensePercent);
	SAFE_DELETE_ARRAY(m_paiExtraUnitClassAttackModifier);
	SAFE_DELETE_ARRAY(m_paiExtraUnitClassDefenseModifier);
	SAFE_DELETE_ARRAY(m_paiProductionToWork);
	SAFE_DELETE_ARRAY(m_paiExtraUnitCombatModifier);
}


// FUNCTION: reset()
// Initializes data members that are serialized.
void CvUnit::reset(int iID, UnitTypes eUnit, PlayerTypes eOwner, bool bConstructorCall)
{
	int iI;

	//--------------------------------
	// Uninit class
	uninit();

	m_iID = iID;
	m_iGroupID = FFreeList::INVALID_INDEX;
	m_iHotKeyNumber = -1;
	m_iX = INVALID_PLOT_COORD;
	m_iY = INVALID_PLOT_COORD;
	m_iLastMoveTurn = 0;
	m_iGameTurnCreated = 0;
	m_iDamage = 0;
	m_iMoves = 0;
	m_iExperience = 0;
	m_iLevel = 1;
	m_iCargo = 0;
	m_iNewCargo = 0;
	m_iAttackPlotX = INVALID_PLOT_COORD;
	m_iAttackPlotY = INVALID_PLOT_COORD;
	m_iCombatTimer = 0;
	m_iCombatDamage = 0;
	m_iFortifyTurns = 0;
	m_iBlitzCount = 0;
	m_iAmphibCount = 0;
	m_iRiverCount = 0;
	m_iEnemyRouteCount = 0;
	m_iAlwaysHealCount = 0;
	m_iHillsDoubleMoveCount = 0;
	m_iExtraVisibilityRange = 0;
	m_iExtraMoves = 0;
	m_iExtraMoveDiscount = 0;
	m_iExtraWithdrawal = 0;
	m_iExtraBombardRate = 0;
	m_iExtraEnemyHeal = 0;
	m_iExtraNeutralHeal = 0;
	m_iExtraFriendlyHeal = 0;
	m_iSameTileHeal = 0;
	m_iAdjacentTileHeal = 0;
	m_iExtraCombatPercent = 0;
	m_iExtraRealCombatPercent = 0;
	m_iExtraCrossOceanSpeed = 0;
	m_iExtraCityAttackPercent = 0;
	m_iExtraCityDefensePercent = 0;
	m_iExtraHillsAttackPercent = 0;
	m_iExtraHillsDefensePercent = 0;
	m_iPillageChange = 0;
	m_iUpgradeDiscount = 0;
	m_iExperiencePercent = 0;
	m_iAccuracyPercent = 0;
	m_iBombardmentPercent = 0;
	m_eFacingDirection = DIRECTION_SOUTH;
	m_iImmobileTimer = 0;
	m_iYieldStored = 0;
	m_iExtraWorkRate = 0;
	m_iUnitTravelTimer = 0;
	m_iBadCityDefenderCount = 0;
	m_iUnarmedCount = 0;
	m_iNbMinution = 0;
	m_iNbCannon = 0;
	m_iRebelPercent = 0;
	m_iUnitSailorType = -1;
	m_iCrewFormationTurn = 0;
	m_iLearningProfession = -1;
	m_iFormationProfession = -1;
	m_iLevelExploAndNavigation = 0;
	m_iExperienceExploAndNavigation = 0;
	m_iEuropeRecruitPrice = 0;
	m_iSellPrice = 0;
	m_iStolenGold = 0;
	m_iProposePrice = 0;
	m_iNumMap = -1;
	m_iTargetSeawayID = -1;
	m_iMilitaryProfessionTurn = 0;
	m_iTravelRotation = 0;
	m_iDestinationCity = -1;
	m_iCountDiscoveredMapTiles = 0;
	m_iCityJobID = -1;
	m_iConvoyID = -1;

	m_uiTotalDiscoveredMapTiles = 0;

	m_bMadeAttack = false;
	m_bPromotionReady = false;
	m_bHasCrew = true;
	m_bCanBeSailor = false;
	m_bTempInvisible = false;
	m_bTempCrew = false;
	m_bInEuropeDrydock = false;
	m_bMustStayInEurope = false;
	m_bLikelyToLeave = false;
	m_bhasPurchaseBid = false;
	m_bHaveFoundPack = false;
	m_bHaveNewMilitaryProfession = false;
	m_bDeathDelay = false;
	m_bCombatFocus = false;
	m_bInfoBarDirty = false;
	m_bColonistLocked = false;

	m_eOwner = eOwner;
	m_eCapturingPlayer = NO_PLAYER;
	m_eOriginalOwner = NO_PLAYER;
	m_eTargetPlayer = NO_PLAYER;

	m_eUnitType = eUnit;
	m_pUnitInfo = (NO_UNIT != m_eUnitType) ? &GC.getUnitInfo(m_eUnitType) : NULL;
	m_iBaseCombat = (NO_UNIT != m_eUnitType) ? m_pUnitInfo->getCombat() : 0;
	m_eLeaderUnitType = NO_UNIT;
	m_iCargoCapacity = (NO_UNIT != m_eUnitType) ? m_pUnitInfo->getCargoSpace() : 0;
	m_eProfession = NO_PROFESSION;
	m_eUnitTravelState = NO_UNIT_TRAVEL_STATE;
	m_eImmigrationType = NO_IMMIGRATION;
	m_ePostCombatState = NO_POST_COMBAT;

	m_combatUnit.reset();
	m_transportUnit.reset();
	m_homeCity.reset();
	m_workingCity.reset();
	m_iPostCombatPlotIndex = -1;

	for (iI = 0; iI < NUM_DOMAIN_TYPES; iI++)
	{
		m_aiExtraDomainModifier[iI] = 0;
	}
	for (iI = 0; iI < NUM_YIELD_TYPES; iI++)
	{
		m_aiCargoYield[iI] = 0;
	}

	clear(m_szName);
	m_szScriptData ="";

	if (!bConstructorCall)
	{
		FAssertMsg((0 < GC.getNumPromotionInfos()), "GC.getNumPromotionInfos() is not greater than zero but an array is being allocated in CvUnit::reset");
		m_paiFreePromotionCount = new int[GC.getNumPromotionInfos()];
		m_pabHasRealPromotion = new bool[GC.getNumPromotionInfos()];
		for (iI = 0; iI < GC.getNumPromotionInfos(); iI++)
		{
			m_pabHasRealPromotion[iI] = false;
			m_paiFreePromotionCount[iI] = 0;
		}

		FAssertMsg((0 < GC.getNumTerrainInfos()), "GC.getNumTerrainInfos() is not greater than zero but a float array is being allocated in CvUnit::reset");
		m_paiTerrainDoubleMoveCount = new int[GC.getNumTerrainInfos()];
		m_paiExtraTerrainAttackPercent = new int[GC.getNumTerrainInfos()];
		m_paiExtraTerrainDefensePercent = new int[GC.getNumTerrainInfos()];
		for (iI = 0; iI < GC.getNumTerrainInfos(); iI++)
		{
			m_paiTerrainDoubleMoveCount[iI] = 0;
			m_paiExtraTerrainAttackPercent[iI] = 0;
			m_paiExtraTerrainDefensePercent[iI] = 0;
		}

		FAssertMsg((0 < GC.getNumFeatureInfos()), "GC.getNumFeatureInfos() is not greater than zero but a float array is being allocated in CvUnit::reset");
		m_paiFeatureDoubleMoveCount = new int[GC.getNumFeatureInfos()];
		m_paiExtraFeatureDefensePercent = new int[GC.getNumFeatureInfos()];
		m_paiExtraFeatureAttackPercent = new int[GC.getNumFeatureInfos()];
		for (iI = 0; iI < GC.getNumFeatureInfos(); iI++)
		{
			m_paiFeatureDoubleMoveCount[iI] = 0;
			m_paiExtraFeatureAttackPercent[iI] = 0;
			m_paiExtraFeatureDefensePercent[iI] = 0;
		}

		FAssertMsg((0 < GC.getNumUnitCombatInfos()), "GC.getNumUnitCombatInfos() is not greater than zero but an array is being allocated in CvUnit::reset");
		m_paiExtraUnitCombatModifier = new int[GC.getNumUnitCombatInfos()];
		for (iI = 0; iI < GC.getNumUnitCombatInfos(); iI++)
		{
			m_paiExtraUnitCombatModifier[iI] = 0;
		}

		FAssertMsg((0 < GC.getNumUnitClassInfos()), "GC.getNumUnitClassInfos() is not greater than zero but an array is being allocated in CvUnit::reset");
		m_paiExtraUnitClassAttackModifier = new int[GC.getNumUnitClassInfos()];
		for (iI = 0; iI < GC.getNumUnitClassInfos(); iI++)
		{
			m_paiExtraUnitClassAttackModifier[iI] = 0;
		}

		FAssertMsg((0 < GC.getNumUnitClassInfos()), "GC.getNumUnitClassInfos() is not greater than zero but an array is being allocated in CvUnit::reset");
		m_paiExtraUnitClassDefenseModifier = new int[GC.getNumUnitClassInfos()];
		for (iI = 0; iI < GC.getNumUnitClassInfos(); iI++)
		{
			m_paiExtraUnitClassDefenseModifier[iI] = 0;
		}

		FAssertMsg((0 < nbUpgradeWork), "nbUpgradeWork is not greater than zero but an array is being allocated in CvUnit::reset");
		m_paiProductionToWork = new int[nbUpgradeWork];
		for (iI = 0; iI < nbUpgradeWork; iI++)
		{
			m_paiProductionToWork[iI] = 0;
		}

		AI_reset();
	}
}


//////////////////////////////////////
// graphical only setup
//////////////////////////////////////
void CvUnit::setupGraphical()
{
	if (!GC.IsGraphicsInitialized())
	{
		return;
	}

	CvDLLEntity::setup();
}


void CvUnit::convert(CvUnit* pUnit, bool bKill, bool bCaptureUnit)
{
	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pTransportUnit;
	CvUnit* pLoopUnit;

	setGameTurnCreated(pUnit->getGameTurnCreated());
	setDamage(pUnit->getDamage());
	setMoves(pUnit->getMoves());
	setYieldStored(pUnit->getYieldStored());
	setFacingDirection(pUnit->getFacingDirection(false));
	setMunition(pUnit->getMunition());
	setNbCannon(pUnit->getNbCannon());
	setRebelSentiment(pUnit->getRebelSentiment());
	if (isHuman() && !pUnit->isHuman())
	{
		if (m_pUnitInfo->isMechUnit() && getNbCannon() == 0)
		{
			if (getMaxCannon() > 0)
			{
				setNbCannon(getMaxCannon()*(60+GC.getGameINLINE().getSorenRandNum(40, "change setNbCannon"))/100);
			}
			if (getMaxMunition() > 0)
			{
				setMunition(getMaxMunition()*(50+GC.getGameINLINE().getSorenRandNum(30, "change setMunition"))/100);
			}
		}
	}
	setMunition(pUnit->getMunition());
	setLevelExploAndNavigation(pUnit->getLevelExploAndNavigation());
	setExperienceExploAndNavigation(pUnit->getExperienceExploAndNavigation());
	setUnitSailorType(pUnit->getUnitSailorType());

	setLevel(pUnit->getLevel());
	int iOldModifier = std::max(1, 100 + GET_PLAYER(pUnit->getOwnerINLINE()).getLevelExperienceModifier());
	int iOurModifier = std::max(1, 100 + GET_PLAYER(getOwnerINLINE()).getLevelExperienceModifier());
	setExperience(std::max(0, (pUnit->getExperience() * iOurModifier) / iOldModifier));

	setRebelSentiment(pUnit->getRebelSentiment());
	setName(pUnit->getNameNoDesc());
	setLeaderUnitType(pUnit->getLeaderUnitType());
	if (bKill)
	{
		ProfessionTypes eProfession = pUnit->getProfession();
		CvCity* pCity = pUnit->getCity();
		if (pCity != NULL)
		{
			pCity->AI_setWorkforceHack(true);
		}
		pUnit->setProfession(NO_PROFESSION);  // leave equipment behind
		setProfession(eProfession, true);
		if (pCity != NULL)
		{
			pCity->AI_setWorkforceHack(false);
		}
		setHaveNewMilitaryProfession(pUnit->isHaveNewMilitaryProfession());
		setMilitaryProfessionTurn(pUnit->getMilitaryProfessionTurn());
	}
	setUnitTravelState(pUnit->getUnitTravelState(), false);
	setUnitTravelTimer(pUnit->getUnitTravelTimer());

	for (int iI = 0; iI < GC.getNumPromotionInfos(); iI++)
	{
		PromotionTypes ePromotion = (PromotionTypes) iI;
		if (pUnit->isHasRealPromotion(ePromotion))
		{
			setHasRealPromotion(ePromotion, true);
		}
	}

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (pUnit->getNewCargoYield(eYield) > 0)
		{
			setNewCargoYield(eYield, pUnit->getNewCargoYield(eYield));
		}
	}

	pTransportUnit = pUnit->getTransportUnit();

	bool bAlive = true;
	if (pTransportUnit != NULL)
	{
		pUnit->setTransportUnit(NULL, false);
		bAlive = setTransportUnit(pTransportUnit);
	}

	if (bAlive)
	{
		if (pUnit->IsSelected() && isOnMap() && getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->insertIntoSelectionList(this, true, false);
		}
	}

	CvPlot* pPlot = pUnit->plot();
	if (pPlot != NULL)
	{
		if (bAlive)
		{
			pUnitNode = pPlot->headUnitNode();

			while (pUnitNode != NULL)
			{
				pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pPlot->nextUnitNode(pUnitNode);

				if (pLoopUnit->getTransportUnit() == pUnit)
				{
					if (pLoopUnit->getOwnerINLINE() == getOwnerINLINE())
					{
						pLoopUnit->setTransportUnit(this);
					}
					else
					{
						CvUnit* pNewUnit = GET_PLAYER(getOwnerINLINE()).initUnit(pLoopUnit->getUnitType(), pLoopUnit->getProfession(), pLoopUnit->getX_INLINE(), pLoopUnit->getY_INLINE(), pLoopUnit->AI_getUnitAIType(), pLoopUnit->getFacingDirection(false), pLoopUnit->getYieldStored());
						if (pNewUnit != NULL)
						{
							PlayerTypes ePlayer = pLoopUnit->getOriginalOwner();
							if (pLoopUnit->getOriginalOwner() == NO_PLAYER)
							{
								pNewUnit->setOriginalOwner(getOriginalOwner());//We give the originalOwner
							}
							else
							{
								pNewUnit->setOriginalOwner(pLoopUnit->getOriginalOwner());
							}
							pNewUnit->convert(pLoopUnit, true);
							pNewUnit->setTransportUnit(this);
						}
					}
				}
			}
		}

		if (bKill)
		{
			pUnit->kill(true);
		}
	}
	else //off map
	{
		if (bKill)
		{
			pUnit->updateOwnerCache(-1);
			SAFE_DELETE(pUnit);
		}
	}
}


void CvUnit::kill(bool bDelay, CvUnit* pAttacker)
{
	PROFILE_FUNC();

	CvWString szBuffer;
	CvPlayerAI& kPlayer = GET_PLAYER(getOwnerINLINE());

	CvPlot* pPlot = plot();
	FAssertMsg(pPlot != NULL, "Plot is not assigned a valid value");
	FAssert(kPlayer.checkPopulation());

	static std::vector<IDInfo> oldUnits;
	oldUnits.erase(oldUnits.begin(), oldUnits.end());
	CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();

	while (pUnitNode != NULL)
	{
		oldUnits.push_back(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);
	}

	for (int i = 0; i < (int) oldUnits.size(); i++)
	{
		CvUnit* pLoopUnit = ::getUnit(oldUnits[i]);

		if (pLoopUnit != NULL)
		{
			if (pLoopUnit->getTransportUnit() == this)
			{
				//save old units because kill will clear the static list
				std::vector<IDInfo> tempUnits = oldUnits;

				if (pPlot->isValidDomainForLocation(*pLoopUnit))
				{
					pLoopUnit->setCapturingPlayer(getCapturingPlayer());
				}

				if (pLoopUnit->getCapturingPlayer() == NO_PLAYER)
				{
					if (pAttacker != NULL && pAttacker->getUnitInfo().isCapturesCargo())
					{
						pLoopUnit->setCapturingPlayer(pAttacker->getOwnerINLINE());
					}
				}

				pLoopUnit->kill(false, pAttacker);

				oldUnits = tempUnits;
			}
		}
	}

	if (pAttacker != NULL)
	{
		gDLL->getEventReporterIFace()->unitKilled(this, pAttacker->getOwnerINLINE());

		kPlayer.changeUnitKilledCounter(getUnitClassType(), 1);
		kPlayer.helpOftheKing(getUnitClassType());

		if (NO_UNIT != getLeaderUnitType())
		{
			for (int iI = 0; iI < MAX_PLAYERS; iI++)
			{
				if (GET_PLAYER((PlayerTypes)iI).isHuman())
				{
					szBuffer = gDLL->getText("TXT_KEY_MISC_GENERAL_KILLED", getNameKey());
					gDLL->getInterfaceIFace()->addMessage(((PlayerTypes)iI), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, GC.getEraInfo(GC.getGameINLINE().getCurrentEra()).getAudioUnitDefeatScript(), MESSAGE_TYPE_MAJOR_EVENT);
				}
			}
		}
	}

	CvCityJob* pJob = getCityJob();
	if (pJob != NULL) 
	{
		pJob->removeUnit(this);
	}

	CvConvoy* pConvoy = getConvoy();
	if (pConvoy != NULL) 
	{
		pConvoy->removeUnit(this, true);
	}

	AI_setCityIdToProtect(-1);

	if (bDelay)
	{
		startDelayedDeath();
		return;
	}

	//finishMoves();

	int iYieldStored = getYieldStored();
	setYieldStored(0);

	removeFromMap();
	updateOwnerCache(-1);

	PlayerTypes eOwner = getOwnerINLINE();
	PlayerTypes eCapturingPlayer = getCapturingPlayer();
	UnitTypes eCaptureUnitType = NO_UNIT;
	ProfessionTypes eCaptureProfession = getProfession();
	FAssert(eCaptureProfession == NO_PROFESSION || !GC.getProfessionInfo(eCaptureProfession).isCitizen());
	if (eCapturingPlayer != NO_PLAYER)
	{
		eCaptureUnitType = getCaptureUnitType(GET_PLAYER(eCapturingPlayer).getCivilizationType());
	}
	YieldTypes eYield = getYield();

	gDLL->getEventReporterIFace()->unitLost(this);

	kPlayer.AI_removeUnitFromMoveQueue(this);
	kPlayer.deleteUnit(getID());

	FAssert(GET_PLAYER(eOwner).checkPopulation());

	if ((eCapturingPlayer != NO_PLAYER) && (eCaptureUnitType != NO_UNIT))
	{
		if (GET_PLAYER(eCapturingPlayer).isHuman() || GET_PLAYER(eCapturingPlayer).AI_captureUnit(eCaptureUnitType, pPlot) || 0 == GC.getDefineINT("AI_CAN_DISBAND_UNITS"))
		{
			if (!GET_PLAYER(eCapturingPlayer).isProfessionValid(eCaptureProfession, eCaptureUnitType))
			{
				eCaptureProfession = (ProfessionTypes) GC.getUnitInfo(eCaptureUnitType).getDefaultProfession();
			}
			CvUnit* pkCapturedUnit = GET_PLAYER(eCapturingPlayer).initUnit(eCaptureUnitType, eCaptureProfession, pPlot->getX_INLINE(), pPlot->getY_INLINE(), NO_UNITAI, NO_DIRECTION, iYieldStored);

			if (pkCapturedUnit != NULL)
			{
				bool bAlive = true;
				if (pAttacker != NULL && pAttacker->getUnitInfo().isCapturesCargo())
				{
					pkCapturedUnit->setXY(pAttacker->getX_INLINE(), pAttacker->getY_INLINE());
					if (pkCapturedUnit->getTransportUnit() == NULL) //failed to load
					{
						bAlive = false;
						pkCapturedUnit->kill(false);
					}
				}

				if (bAlive)
				{
					szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_CAPTURED_UNIT", GC.getUnitInfo(eCaptureUnitType).getTextKeyWide());
					gDLL->getInterfaceIFace()->addMessage(eCapturingPlayer, false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_UNITCAPTURE", MESSAGE_TYPE_INFO, pkCapturedUnit->getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pPlot->getX_INLINE(), pPlot->getY_INLINE());

					if (!pkCapturedUnit->isCargo())
					{
						// Add a captured mission
						CvMissionDefinition kMission;
						kMission.setMissionTime(GC.getMissionInfo(MISSION_CAPTURED).getTime() * gDLL->getSecsPerTurn());
						kMission.setUnit(BATTLE_UNIT_ATTACKER, pkCapturedUnit);
						kMission.setUnit(BATTLE_UNIT_DEFENDER, NULL);
						kMission.setPlot(pPlot);
						kMission.setMissionType(MISSION_CAPTURED);
						gDLL->getEntityIFace()->AddMission(&kMission);
					}

					for (int iJ = 0; iJ < GC.getNumPromotionInfos(); iJ++)
					{
						PromotionTypes ePromotion = (PromotionTypes)iJ;
						if (pkCapturedUnit->getUnitInfo().getFreePromotions(iJ) && !GC.getPromotionInfo(ePromotion).isExploAndNavigation() && !GC.getPromotionInfo(ePromotion).isOnlyFreePromo())
						{
							pkCapturedUnit->changeLevel(1);
						}
					}
					pkCapturedUnit->finishMoves();

					if (!GET_PLAYER(eCapturingPlayer).isHuman())
					{
						CvPlot* pPlot = pkCapturedUnit->plot();
						if (pPlot && !pPlot->isCity(false))
						{
							if (GET_PLAYER(eCapturingPlayer).AI_getPlotDanger(pPlot) && GC.getDefineINT("AI_CAN_DISBAND_UNITS"))
							{
								pkCapturedUnit->kill(false);
							}
						}
					}
				}
			}
		}
	}
}

void CvUnit::removeFromMap()
{
	if ((getX_INLINE() != INVALID_PLOT_COORD) && (getY_INLINE() != INVALID_PLOT_COORD))
	{
		if (IsSelected())
		{
			if (gDLL->getInterfaceIFace()->getLengthSelectionList() == 1)
			{
				if (!(gDLL->getInterfaceIFace()->isFocused()) && !(gDLL->getInterfaceIFace()->isCitySelection()) && !(gDLL->getInterfaceIFace()->isDiploOrPopupWaiting()))
				{
					GC.getGameINLINE().updateSelectionList();
				}

				if (IsSelected())
				{
					gDLL->getInterfaceIFace()->setCycleSelectionCounter(1);
				}
				else
				{
					gDLL->getInterfaceIFace()->setDirty(SelectionCamera_DIRTY_BIT, true);
				}
			}
		}

		gDLL->getInterfaceIFace()->removeFromSelectionList(this);

		// XXX this is NOT a hack, without it, the game crashes.
		gDLL->getEntityIFace()->RemoveUnitFromBattle(this);

		plot()->setFlagDirty(true);

		FAssertMsg(!isCombat(), "isCombat did not return false as expected");

		CvUnit* pTransportUnit = getTransportUnit();

		if (pTransportUnit != NULL)
		{
			setTransportUnit(NULL);
		}

		AI_setMovePriority(0);

		FAssertMsg(getAttackPlot() == NULL, "The current unit instance's attack plot is expected to be NULL");
		FAssertMsg(getCombatUnit() == NULL, "The current unit instance's combat unit is expected to be NULL");

		if (!gDLL->GetDone() && GC.IsGraphicsInitialized())	// don't need to remove entity when the app is shutting down, or crash can occur
		{
			CvDLLEntity::removeEntity();		// remove entity from engine
		}

		CvDLLEntity::destroyEntity();
		CvDLLEntity::createUnitEntity(this);		// create and attach entity to unit
	}

	AI_setUnitAIType(NO_UNITAI);

	setXY(INVALID_PLOT_COORD, INVALID_PLOT_COORD, true);

	joinGroup(NULL, false, false);
}

void CvUnit::addToMap(int iPlotX, int iPlotY)
{
	if ((iPlotX != INVALID_PLOT_COORD) && (iPlotY != INVALID_PLOT_COORD))
	{
		//--------------------------------
		// Init pre-setup() data
		setXY(iPlotX, iPlotY, false, false);

		//--------------------------------
		// Init non-saved data
		setupGraphical();

		//--------------------------------
		// Init other game data
		plot()->updateCenterUnit();

		plot()->setFlagDirty(true);
	}

	if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		gDLL->getInterfaceIFace()->setDirty(GameData_DIRTY_BIT, true);
	}
}

void CvUnit::updateOwnerCache(int iChange)
{
	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());

	GET_TEAM(getTeam()).changeUnitClassCount(((UnitClassTypes)(m_pUnitInfo->getUnitClassType())), iChange);
	kPlayer.changeUnitClassCount(((UnitClassTypes)(m_pUnitInfo->getUnitClassType())), iChange);
	kPlayer.changeAssets(getAsset() * iChange);
	kPlayer.changePower(getPower() * iChange);
	CvArea* pArea = area();
	if (pArea != NULL)
	{
		pArea->changePower(getOwnerINLINE(), getPower() * iChange);
	}
	if (m_pUnitInfo->isFound())
	{
		GET_PLAYER(getOwnerINLINE()).changeTotalPopulation(iChange);
	}
}


void CvUnit::NotifyEntity(MissionTypes eMission)
{
	gDLL->getEntityIFace()->NotifyEntity(getUnitEntity(), eMission);
}


void CvUnit::doTurn()
{
	PROFILE_FUNC();

	FAssertMsg(!isDead(), "isDead did not return false as expected");
	FAssert(getGroup() != NULL || GET_PLAYER(getOwnerINLINE()).getPopulationUnitCity(getID()) != NULL);

	testPromotionReady();
	processExploAndNavigation();
	processProductionToWork();
	processUpgradeMilitary();
	processHavePurchaseBid();
	processTempInvisible();
	processMilitaryProfessionTurn();

	if (!isHuman())
	{
		establishMission();
	}
	processHeal();

	if (!isCargo())
	{
		changeFortifyTurns(1);
	}
	if (isHaveNewMilitaryProfession())
	{
		setHaveNewMilitaryProfession(false);
	}
	changeImmobileTimer(-1);
	//doUnitTravelTimer();

	setMadeAttack(false);

	setMoves(0);
}

void CvUnit::resolveCombat(CvUnit* pDefender, CvPlot* pPlot, CvBattleDefinition& kBattle)
{

	bool bNavalCombat = getDomainType() == DOMAIN_SEA && pDefender->getDomainType() == DOMAIN_SEA;

	if (bNavalCombat) 
	{
		resolveNavalCombat(pDefender, pPlot, kBattle);
	} 
	else 
	{
		resolveLandCombat(pDefender, pPlot, kBattle);
	}
}

void CvUnit::resolveNavalCombat(CvUnit* pDefender, CvPlot* pPlot, CvBattleDefinition& kBattle)
{
	CombatDetails cdAttackerDetails;
	CombatDetails cdDefenderDetails;

	int iDefenceInitialDamage = pDefender->getDamage();

	int iAttackerStrength = currCombatStr(NULL, NULL, &cdAttackerDetails);
	int iAttackerFirepower = currFirepower(NULL, NULL);
	int iDefenderStrength;
	int iAttackerDamage;
	int iDefenderDamage;
	int iDefenderOdds;

	getDefenderCombatValues(*pDefender, pPlot, iAttackerStrength, iAttackerFirepower, iDefenderOdds, iDefenderStrength, iAttackerDamage, iDefenderDamage, &cdDefenderDetails);

	int iCombatOdds = getCombatOdds(this, pDefender);
	bool bAttackerCanDie = (iCombatOdds < 500);
	bool bDefenderCanDie = (iCombatOdds >= 500);
	bool bPirateAttack = m_pUnitInfo->isPirate();

	if (tryToAccostShip(pDefender, pPlot, iAttackerDamage, iDefenderDamage, iAttackerStrength, iDefenderStrength))
	{		
		return;
	}

	if (pDefender->isOnlyDefensive())
	{
		int probabiltyToDie = GC.getGameINLINE().getSorenRandNum(100, "probabilty To die");
		if (probabiltyToDie < iDefenceInitialDamage) 
		{	
			pDefender->changeDamage(pDefender->maxHitPoints(), this);
			return;
		}
		else 
		{
			CvPlot* pRandomPlot = pDefender->randomEscapePlot(1);
			if (pRandomPlot != NULL)
			{
				int iExperienceFromWithdrawl = GC.getDefineINT("EXPERIENCE_FROM_WITHDRAWL");
				changeExperience(iExperienceFromWithdrawl, pDefender->maxXPValue(), true, pPlot->getOwnerINLINE() == getOwnerINLINE(), true);
				pDefender->changeExperience(iExperienceFromWithdrawl, maxXPValue(), true, pPlot->getOwnerINLINE() == pDefender->getOwnerINLINE(), true);
				pDefender->setPostCombatState(POST_COMBAT_ESCAPE);
				pDefender->setPostCombatPlot(pRandomPlot->getX_INLINE(), pRandomPlot->getY_INLINE());
				return;
			}
		}
	}
	int iCombatdieSides = GC.getDefineINT("COMBAT_DIE_SIDES");

	while (true)
	{
		bool isDefenderCounterAttack = GC.getGameINLINE().getSorenRandNum(iCombatdieSides, "Combat") < iDefenderOdds;
		if (isDefenderCounterAttack)
		{
			if (getDamage() + iAttackerDamage >= maxHitPoints())
			{
				CvPlot* pRandomPlot = randomEscapePlot(1);	

				if (pRandomPlot != NULL)
				{
					bool isAttackerWithdrawal = GC.getGameINLINE().getSorenRandNum(100, "Withdrawal") < withdrawalProbability();
					if (isAttackerWithdrawal || !bAttackerCanDie)
					{
						//The attacker withdraws
						int iExperienceFromWithdrawl = GC.getDefineINT("EXPERIENCE_FROM_WITHDRAWL");
						changeExperience(iExperienceFromWithdrawl, pDefender->maxXPValue(), true, pPlot->getOwnerINLINE() == getOwnerINLINE(), true);
						pDefender->changeExperience(iExperienceFromWithdrawl, maxXPValue(), true, pPlot->getOwnerINLINE() == pDefender->getOwnerINLINE(), true);
						setPostCombatState(POST_COMBAT_ABANDON);
						setPostCombatPlot(pRandomPlot->getX_INLINE(), pRandomPlot->getY_INLINE());
						break;
					}
				}
			}

			changeDamage(iAttackerDamage, pDefender);

		}
		else
		{
			if (pDefender->getDamage() + iDefenderDamage >= pDefender->maxHitPoints())
			{
				CvPlot* pRandomPlot = pDefender->randomEscapePlot(1);	

				if (pRandomPlot != NULL)
				{
					bool isDefenderWithdrawal = GC.getGameINLINE().getSorenRandNum(100, "Withdrawal") < pDefender->withdrawalProbability();
					if (isDefenderWithdrawal || !bDefenderCanDie)
					{
						int iExperienceFromWithdrawl = GC.getDefineINT("EXPERIENCE_FROM_WITHDRAWL");
						changeExperience(iExperienceFromWithdrawl, pDefender->maxXPValue(), true, pPlot->getOwnerINLINE() == getOwnerINLINE(), true);
						pDefender->changeExperience(iExperienceFromWithdrawl, maxXPValue(), true, pPlot->getOwnerINLINE() == pDefender->getOwnerINLINE(), true);
						pDefender->setPostCombatState(POST_COMBAT_ESCAPE);
						pDefender->setPostCombatPlot(pRandomPlot->getX_INLINE(), pRandomPlot->getY_INLINE());
						break;
					}
				}
			}
			pDefender->changeDamage(iDefenderDamage, this);
		}

		if (isDead())
		{
			int iExperience = defenseXPValue();
			iExperience = ((iExperience * iAttackerStrength) / iDefenderStrength);
			iExperience = range(iExperience, GC.getDefineINT("MIN_EXPERIENCE_PER_COMBAT"), GC.getDefineINT("MAX_EXPERIENCE_PER_COMBAT"));
			pDefender->changeExperience(iExperience, maxXPValue(), true, pPlot->getOwnerINLINE() == pDefender->getOwnerINLINE(), true);
			break;
		}
		else if (pDefender->isDead())
		{
			int iExperience = pDefender->attackXPValue();
			iExperience = ((iExperience * iDefenderStrength) / iAttackerStrength);
			iExperience = range(iExperience, GC.getDefineINT("MIN_EXPERIENCE_PER_COMBAT"), GC.getDefineINT("MAX_EXPERIENCE_PER_COMBAT"));
			if (!pDefender->isOnlyDefensive() || (getLevel() == 1 && !isPromotionReady()))
			{
				changeExperience(iExperience, pDefender->maxXPValue(), true, pPlot->getOwnerINLINE() == getOwnerINLINE(), true);
			}
			break;
		}
	}
}

void CvUnit::resolveLandCombat(CvUnit* pDefender, CvPlot* pPlot, CvBattleDefinition& kBattle)
{
	CombatDetails cdAttackerDetails;
	CombatDetails cdDefenderDetails;

	int iDefenceInitialDamage = pDefender->getDamage();

	int iAttackerStrength = currCombatStr(NULL, NULL, &cdAttackerDetails);
	int iAttackerFirepower = currFirepower(NULL, NULL);
	int iDefenderStrength;
	int iAttackerDamage;
	int iDefenderDamage;
	int iDefenderOdds;

	getDefenderCombatValues(*pDefender, pPlot, iAttackerStrength, iAttackerFirepower, iDefenderOdds, iDefenderStrength, iAttackerDamage, iDefenderDamage, &cdDefenderDetails);

	if (isHuman() || pDefender->isHuman())
	{
		CyArgsList pyArgsCD;
		pyArgsCD.add(gDLL->getPythonIFace()->makePythonObject(&cdAttackerDetails));
		pyArgsCD.add(gDLL->getPythonIFace()->makePythonObject(&cdDefenderDetails));
		pyArgsCD.add(getCombatOdds(this, pDefender));
		gDLL->getEventReporterIFace()->genericEvent("combatLogCalc", pyArgsCD.makeFunctionArgs());
	}

	int iCombatOdds = getCombatOdds(this, pDefender);
	bool bAttackerCanDie = (iCombatOdds < 500);
	bool bDefenderCanDie = (iCombatOdds > 500);

	int iCombatdieSides = GC.getDefineINT("COMBAT_DIE_SIDES");
	while (true)
	{
		bool isDefenderCounterAttack = GC.getGameINLINE().getSorenRandNum(iCombatdieSides, "Combat") < iDefenderOdds;
		if (isDefenderCounterAttack)
		{
			if (getDamage() + iAttackerDamage >= maxHitPoints())
			{
				CvPlot* pRandomPlot = randomEscapePlot(1);	

				if (pRandomPlot != NULL)
				{
					bool isAttackerWithdrawal = GC.getGameINLINE().getSorenRandNum(100, "Withdrawal") < withdrawalProbability();
					if (isAttackerWithdrawal || !bAttackerCanDie)
					{
						//The attacker withdraws
						int iExperienceFromWithdrawl = GC.getDefineINT("EXPERIENCE_FROM_WITHDRAWL");
						changeExperience(iExperienceFromWithdrawl, pDefender->maxXPValue(), true, pPlot->getOwnerINLINE() == getOwnerINLINE(), true);
						pDefender->changeExperience(iExperienceFromWithdrawl, maxXPValue(), true, pPlot->getOwnerINLINE() == pDefender->getOwnerINLINE(), true);
						setPostCombatState(POST_COMBAT_ABANDON);
						setPostCombatPlot(pRandomPlot->getX_INLINE(), pRandomPlot->getY_INLINE());
						break;
					}
				}
			}

			changeDamage(iAttackerDamage, pDefender);

		}
		else
		{
			if (pDefender->getDamage() + iDefenderDamage >= pDefender->maxHitPoints())
			{
				CvPlot* pRandomPlot = pDefender->randomEscapePlot(1);	

				if (pRandomPlot != NULL)
				{
					bool isDefenderWithdrawal = GC.getGameINLINE().getSorenRandNum(100, "Withdrawal") < pDefender->withdrawalProbability();
					if (isDefenderWithdrawal || !bDefenderCanDie)
					{
						int iExperienceFromWithdrawl = GC.getDefineINT("EXPERIENCE_FROM_WITHDRAWL");
						changeExperience(iExperienceFromWithdrawl, pDefender->maxXPValue(), true, pPlot->getOwnerINLINE() == getOwnerINLINE(), true);
						pDefender->changeExperience(iExperienceFromWithdrawl, maxXPValue(), true, pPlot->getOwnerINLINE() == pDefender->getOwnerINLINE(), true);
						pDefender->setPostCombatState(POST_COMBAT_ESCAPE);
						pDefender->setPostCombatPlot(pRandomPlot->getX_INLINE(), pRandomPlot->getY_INLINE());
						break;
					}
				}
			}
			pDefender->changeDamage(iDefenderDamage, this);
		}

		if (isDead())
		{
			int iExperience = defenseXPValue();
			iExperience = ((iExperience * iAttackerStrength) / iDefenderStrength);
			iExperience = range(iExperience, GC.getDefineINT("MIN_EXPERIENCE_PER_COMBAT"), GC.getDefineINT("MAX_EXPERIENCE_PER_COMBAT"));
			pDefender->changeExperience(iExperience, maxXPValue(), true, pPlot->getOwnerINLINE() == pDefender->getOwnerINLINE(), true);
			break;
		}
		else if (pDefender->isDead())
		{
			int iExperience = pDefender->attackXPValue();
			iExperience = ((iExperience * iDefenderStrength) / iAttackerStrength);
			iExperience = range(iExperience, GC.getDefineINT("MIN_EXPERIENCE_PER_COMBAT"), GC.getDefineINT("MAX_EXPERIENCE_PER_COMBAT"));
			if (!pDefender->isOnlyDefensive() || (getLevel() == 1 && !isPromotionReady()))
			{
				changeExperience(iExperience, pDefender->maxXPValue(), true, pPlot->getOwnerINLINE() == getOwnerINLINE(), true);
			}
			break;
		}
	}
}

int CvUnit::getAccostedProba(CvUnit* pDefender)
{
	return pDefender->isOnlyDefensive() ? 85 : 20;
}

int CvUnit::getEscapedProba(CvUnit* pAttacker)
{
	if (pAttacker->getUnitInfo().isPirate())
	{
		return 100;// If Pirates attack, we always succeed to escape after the first attack 
	}
	return withdrawalProbability();
}

int CvUnit::getAccostedDamage(int iDamage) const
{
	int iCurrHitPoints = currHitPoints();
	int iResult = std::min(iCurrHitPoints/2, iDamage);

	return iResult;
}


bool CvUnit::hasEscortShip() const
{
	CvPlot* pPlot = plot();
	FAssertMsg(pPlot != NULL, "pPlot can't be NULL");

	for (CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode(); pUnitNode != NULL; pUnitNode = pPlot->nextUnitNode(pUnitNode))
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		if (pLoopUnit->getTeam() == getTeam() && pLoopUnit != this)
		{
			if (pLoopUnit->getUnitInfo().isMechUnit() && pLoopUnit->canDefend(pPlot))
			{
				if (!pLoopUnit->isOnlyDefensive() && !pLoopUnit->getUnitInfo().isPirate())
				{
					if (pLoopUnit-isOnMap() && !pLoopUnit-isHurt())
					{
						return true;
					}
				}
			}
		}
	}
	return false;
}

bool CvUnit::hasShipToProtect() const
{
	CvPlot* pPlot = plot();
	FAssertMsg(pPlot != NULL, "pPlot can't be NULL");

	for (CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode(); pUnitNode != NULL; pUnitNode = pPlot->nextUnitNode(pUnitNode))
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		if (pLoopUnit->getTeam() == getTeam() && pLoopUnit != this)
		{
			if (pLoopUnit->getUnitInfo().isMechUnit())
			{
				return true;
			}
		}
	}
	return false;
}

bool CvUnit::canAccostShip(const CvUnit* pDefender) const
{
	if (!m_pUnitInfo->isMechUnit())
	{
		return false;
	}
	if (!pDefender->getUnitInfo().isMechUnit())
	{
		return false;
	}
	if (pDefender->getUnitInfo().isPirate())
	{
		return false;
	}
	if (pDefender->hasEscortShip() && !pDefender->isOnlyDefensive())
	{
		return false;
	}

	return true;
}


bool CvUnit::tryToAccostShip(CvUnit* pDefender, CvPlot* pPlot, int iAttackerDamage, int iDefenderDamage, int iAttackerStrength, int iDefenderStrength) 
{
	FAssertMsg(pDefender != NULL, "pDefender can't be NULL");
	if (!canAccostShip(pDefender))
	{
		return false;
	}
	int iCurrentProba = GC.getGameINLINE().getSorenRandNum(1000, "iCombatOdds");
	int iCombatOdds = getCombatOdds(this, pDefender);

	if (!pDefender->isOnlyDefensive() && iCurrentProba > iCombatOdds)
	{//We test probability to win the fight. More the attacker is strong plus it will have a chance to accost the defender
		return false;
	}
	iCurrentProba = GC.getGameINLINE().getSorenRandNum(100, "AccostedProba");
	int iAccostedProba = getAccostedProba(pDefender);

	if (iCurrentProba < iAccostedProba)
	{ // It succeed to accost the defender 
		pDefender->setPostCombatState(POST_COMBAT_ROB);
		iCurrentProba = GC.getGameINLINE().getSorenRandNum(100, "AccostedDamageProba");
		int iAccostedDamageProba = 33;
		if (iCurrentProba < iAccostedProba)
		{ // We will endomage the ship
			int iAccostedDamage = getAccostedDamage(iDefenderDamage);
			pDefender->changeDamage(iDefenderDamage, this);
		}
	}
	else
	{ // It failed to accost the defender 
		CvPlot* pRandomPlot = pDefender->randomEscapePlot(1);	
		if (pDefender->tryToEscape(this, pRandomPlot))
		{
			iCurrentProba = GC.getGameINLINE().getSorenRandNum(100, "EscapedProba");
			int iEscapeProba = pDefender->getEscapedProba(this);
			if (iCurrentProba < iEscapeProba)
			{ // It succeed to accost the defender 
				pDefender->setPostCombatState(POST_COMBAT_ESCAPE);
				pDefender->setPostCombatPlot(pRandomPlot->getX_INLINE(), pRandomPlot->getY_INLINE());

			}
			else
			{ //Defender failed to escape and go to fight 
				if (!pDefender->isOnlyDefensive())
				{
					return false;
				}
				pDefender->setPostCombatState(POST_COMBAT_ROB);
				iCurrentProba = GC.getGameINLINE().getSorenRandNum(100, "AccostedDamageProba");
				int iAccostedDamageProba = 33;
				if (iCurrentProba < iAccostedProba)
				{ // We will endomage the ship
					int iAccostedDamage = getAccostedDamage(iDefenderDamage);
					pDefender->changeDamage(iDefenderDamage, this);
				}
			}
		}
		else
		{ //Defender try to fight 
			return false;
		}
	}

	int iExperience = getShipExperience(pDefender, iAttackerStrength, iDefenderStrength);
	if (iExperience > 0) 
	{
		changeExperience(iExperience, pDefender->maxXPValue(), true, false, true);
	}

	return true;
}

int  CvUnit::getShipExperience(CvUnit* pDefender, int iAttackerStrength, int iDefenderStrength)
{
	if (pDefender->isOnlyDefensive() && (getLevel() > 1 || isPromotionReady())) 
	{
		return 0;
	}

	if (pDefender->getPostCombatState() == POST_COMBAT_ESCAPE) 
	{
		return 0;
	}

	int iExperience = pDefender->attackXPValue();
	iExperience = ((iExperience * iDefenderStrength) / iAttackerStrength);

	return range(iExperience, GC.getDefineINT("MIN_EXPERIENCE_PER_COMBAT"), GC.getDefineINT("MAX_EXPERIENCE_PER_COMBAT"));
}

bool  CvUnit::tryToEscape(CvUnit* pAttacker, CvPlot* pRandomPlot)
{
	if (pRandomPlot == NULL) 
	{
		return false;
	}

	if (hasShipToProtect()) // We prefer to protect our ship
	{ 
		return false;
	}

	int iCombatOdds = getCombatOdds(pAttacker, this);
	int iMinValueToEscape = 400;

	if (iCombatOdds < iMinValueToEscape) // If Attaker has less than 40% chance to win the fight
	{
		return false;
	}

	return true;
}

bool CvUnit::isArtilleryMen() const
{
	if (getProfession() == NO_PROFESSION) 
	{
		return false;
	} 

	if (GC.getProfessionInfo(getProfession()).getBombardRate() == -1) 
	{
		return false;
	}

	return true;
}

int CvUnit::getExperienceForBombardment(ArtilleryPenaltyTypes eArtilleryPenalty) const
{
	int iPenalty = GC.getArtilleryPenaltyInfo(eArtilleryPenalty).getPenalty();
	int iBaseExp =  GC.getDefineINT("MAX_EXPERIENCE_FOR_ARTILLERY_PER_COMBAT");

	return iBaseExp - (iPenalty * iBaseExp) / 100;
}

bool CvUnit::isBombardMode(const CvPlot* pDefPlot) const
{
	if (isArtilleryMen())
	{
		return true;
	}
	if (m_pUnitInfo->getBombardRate() <= 0)
	{
		return false;
	}
	CvCity* pCity = pDefPlot->getPlotCity();
	if (pCity == NULL || pCity->isNative())
	{
		return false;
	}
	return true;
}

void CvUnit::resolveArtilleryCombat(CvPlot* pDefPlot)
{
	CvWString szBuffer;
	bool bFinish = false;
	bool bVisible = false;

	if (getCombatTimer() > 0) 
	{
		changeCombatTimer(-1);
		if (getCombatTimer() > 0)
		{
			return;
		}
		else 
		{
			bFinish = true;
		}
	}

	CvPlot* pAttPlot = plot();

	CvUnit* pDefender = NULL;
	if (bFinish) 
	{
		pDefender = getCombatUnit();
	}
	else 
	{
		pDefender = pDefPlot->getBestDefenderToBombard(NO_PLAYER, getOwnerINLINE(), this);
	}

	if (pDefender == NULL) 
	{
		setAttackPlot(NULL);
		setCombatUnit(NULL);
		getGroup()->clearMissionQueue();
		return;
	}

	ArtilleryPenaltyTypes eArtilleryPenalty = getArtilleryPenalty(pDefender->plot());

	int iBombardmentDamagePercent = getBombardmentDamagePercent(eArtilleryPenalty);
	int iMaxBombardmentDamagePercent = getMaxBombardmentDamagePercent(eArtilleryPenalty);
	int iBaseExperience = getExperienceForBombardment(eArtilleryPenalty);

	if (pDefender->getDamage() < iMaxBombardmentDamagePercent)
	{		
		int iRealDamage = std::min(iMaxBombardmentDamagePercent - pDefender->getDamage(), iBombardmentDamagePercent);
		pDefender->changeDamage(iRealDamage, pDefender);

		changeExperience(iBaseExperience, pDefender->maxXPValue(), true, pDefPlot->getOwnerINLINE() == getOwnerINLINE(), true);

		szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_WITHDRAW_CANNON");
		gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_OUR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pDefPlot->getX_INLINE(), pDefPlot->getY_INLINE());
	}
	else
	{
		szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_WITHDRAW_CANNON_FAIL");
		gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pDefPlot->getX_INLINE(), pDefPlot->getY_INLINE());
	}

	setMadeAttack(true);
	changeMoves(GC.getMOVE_DENOMINATOR());
	ammunitionProcess(pDefender);

	if (pAttPlot->isActiveVisible(false))
	{
		// Bombard entity mission
		CvMissionDefinition kDefiniton;
		kDefiniton.setMissionTime(GC.getMissionInfo(MISSION_BOMBARD).getTime() * gDLL->getSecsPerTurn());
		kDefiniton.setMissionType(MISSION_BOMBARD);
		kDefiniton.setPlot(pDefPlot);
		kDefiniton.setUnit(BATTLE_UNIT_ATTACKER, this);
		kDefiniton.setUnit(BATTLE_UNIT_DEFENDER, pDefender);
		gDLL->getEntityIFace()->AddMission(&kDefiniton);
	}
}

ArtilleryPenaltyTypes CvUnit::getArtilleryPenalty(const CvPlot* pDefPlot) const
{
	CvPlot* pAttPlot = plot();
	FAssertMsg(pAttPlot != NULL, "pAttPlot can't be NULL");
	FAssertMsg(pDefPlot != NULL, "pDefPlot can't be NULL");

	CvCity* pAttCity = pAttPlot->getPlotCity();
	CvCity* pDefCity = pDefPlot->getPlotCity();
	for (int iI = 0; iI < GC.getNumArtilleryPenaltyInfos(); ++iI) 
	{
		ArtilleryPenaltyTypes eArtilleryPenalty = (ArtilleryPenaltyTypes)iI;
		CvArtilleryPenaltyInfo& kArtilleryPenalty = GC.getArtilleryPenaltyInfo(eArtilleryPenalty);
		int iLoopPenalty = kArtilleryPenalty.getPenalty();
		TerrainTypes eForbiddenTerrain = (TerrainTypes) kArtilleryPenalty.getForbiddenTerrain();
		FeatureTypes eForbiddenFeature = (FeatureTypes) kArtilleryPenalty.getForbiddenFeature();
		if (eForbiddenTerrain != NO_TERRAIN) 
		{
			if (eForbiddenTerrain == pAttPlot->getTerrainType() || eForbiddenTerrain == pDefPlot->getTerrainType()) 
			{
				FAssertMsg(iLoopPenalty == 100, "forbidden terrain must have 100 for penalty");	
				return eArtilleryPenalty;
			}
		} 
		else if (eForbiddenFeature != NO_FEATURE) 
		{
			if (eForbiddenFeature == pAttPlot->getFeatureType() || eForbiddenFeature == pDefPlot->getFeatureType()) 
			{
				FAssertMsg(iLoopPenalty == 100, "forbidden feature must have 100 for penalty");	
				return eArtilleryPenalty;
			}
		} 
		else if (kArtilleryPenalty.isPeak()) 
		{
			if (pAttPlot->isPeak() || pDefPlot->isPeak()) 
			{
				FAssertMsg(iLoopPenalty == 100, "if is peak, we must have 100 for penalty");	
				return eArtilleryPenalty;
			}
		} 
		else if (kArtilleryPenalty.isCityAtt()) 
		{
			if (pAttCity != NULL && !pAttCity->isNative()) 
			{
				FAssertMsg(iLoopPenalty == 100, "if attack from city we must have 100 for penalty");	
				return eArtilleryPenalty;
			}
		}
		else if (kArtilleryPenalty.isCityDef() == (pDefCity != NULL && !pDefCity->isNative())) 
		{
			if (kArtilleryPenalty.isHillsDef() ? pDefPlot->isHills() : pDefPlot->isFlatlands()) 
			{
				if (kArtilleryPenalty.getFeatureDef() == pDefPlot->getFeatureType()) 
				{
					if (kArtilleryPenalty.isWatterAtt())
					{
						if (pAttPlot->isWater()) 
						{
							return eArtilleryPenalty;
						}
					}
					else if (kArtilleryPenalty.isHillsAtt() ? pAttPlot->isHills() : (pAttPlot->isFlatlands())) 
					{
						if (kArtilleryPenalty.getFeatureAtt() == pAttPlot->getFeatureType()) 
						{
							return eArtilleryPenalty;
						}
					}
				}
			}
		}
	}

	return NO_ARTILLERY_PENALTY;
}

CvPlot* CvUnit::randomEscapePlot(int iDistance, bool bNotCurrentPlot)
{
	CvPlot* pRandomPlot = NULL;
	CvPlot* pCurrentPlot = plot();
	int iTempValue = 0;
	int iMaxValue = -1;
	int iCurrEffectiveStr = currEffectiveStr(pCurrentPlot, NULL);

	if (!bNotCurrentPlot) 
	{
		if (pCurrentPlot->getPlotCity() != NULL || pCurrentPlot->AI_sumStrength(getOwner()) - iCurrEffectiveStr > 0) 
		{
			return pCurrentPlot;
		}
	}

	for (int iDX = -iDistance; iDX <= iDistance; iDX++) 
	{
		for (int iDY = -iDistance; iDY <= iDistance; iDY++) 
		{
			CvPlot* pLoopPlot = plotXY(getX_INLINE(), getY_INLINE(), iDX, iDY);
			if (pLoopPlot != NULL) 
			{
				if (canMoveInto(pLoopPlot, false, false, true)) 
				{
					iTempValue = GC.getGameINLINE().getSorenRandNum(100, "randomEscapePlot");
					iTempValue += pLoopPlot->AI_sumStrength(getOwner())/2;

					if (pLoopPlot->getPlotCity() != NULL) 
					{
						iTempValue *= 2;
					}

					if (iTempValue > iMaxValue) 
					{
						iMaxValue = iTempValue;
						pRandomPlot = pLoopPlot;
					}
				}
			}
		}
	}

	return pRandomPlot;
}

void CvUnit::updateCombat(bool bQuick)
{
	CvWString szBuffer;
	bool bFinish = false;
	bool bVisible = false;

	if (getCombatTimer() > 0)
	{
		changeCombatTimer(-1);

		if (getCombatTimer() > 0)
		{
			return;
		}
		else
		{
			bFinish = true;
		}
	}

	CvPlot* pPlot = getAttackPlot();

	if (pPlot == NULL)
	{
		return;
	}

	CvUnit* pDefender = NULL;
	if (bFinish)
	{
		pDefender = getCombatUnit();
	}
	else
	{
		pDefender = pPlot->getBestDefender(NO_PLAYER, getOwnerINLINE(), this, true);
		if (m_pUnitInfo->isPirate() && pDefender != NULL)
		{
			CvSelectionGroup* pGroup = pDefender->getGroup();
			int iAttackMerchantPercent = getGroup()->getAttackProbability(pGroup);
			if (iAttackMerchantPercent > 0)
			{
				int iRand = GC.getGameINLINE().getSorenRandNum(100, "iAttackMerchantPercent");
				if (iRand < iAttackMerchantPercent)
				{
					pDefender = pGroup->pickRandomMercantUnitIfPossible();
				}
			}
		}
	}

	if (pDefender == NULL)
	{
		setAttackPlot(NULL);
		setCombatUnit(NULL);
		getGroup()->groupMove(pPlot, true, ((canAdvance(pPlot, 0)) ? this : NULL));
		getGroup()->clearMissionQueue();

		return;
	}
	CvPlayer& kDefPlayer = GET_PLAYER(pDefender->getOwnerINLINE());

	//check if quick combat
	if (!bQuick)
	{
		bVisible = isCombatVisible(pDefender);
	}
	if (getProfession() != NO_PROFESSION && GC.getProfessionInfo(getProfession()).getBombardRate() != -1)
	{
		if (pDefender->getUnitInfo().isMechUnit())
		{
			bVisible = false;
		}
	}
	if (m_pUnitInfo->isPirate())
	{
		kDefPlayer.setAttackedByPirates(true);
	}
	//FAssertMsg((pPlot == pDefender->plot()), "There is not expected to be a defender or the defender's plot is expected to be pPlot (the attack plot)");

	//if not finished and not fighting yet, set up combat damage and mission
	if (!bFinish)
	{
		if (!isFighting())
		{
			if (plot()->isFighting() || pPlot->isFighting())
			{
				return;
			}

			setMadeAttack(true);

			//rotate to face plot
			DirectionTypes newDirection = estimateDirection(this->plot(), pDefender->plot());
			if (newDirection != NO_DIRECTION)
			{
				setFacingDirection(newDirection);

			}

			//rotate enemy to face us
			newDirection = estimateDirection(pDefender->plot(), this->plot());
			if (newDirection != NO_DIRECTION)
			{
				pDefender->setFacingDirection(newDirection);
			}

			setCombatUnit(pDefender, true);
			pDefender->setCombatUnit(this, false);

			pDefender->getGroup()->clearMissionQueue();

			bool bFocused = (bVisible && isCombatFocus() && gDLL->getInterfaceIFace()->isCombatFocus());

			if (bFocused)
			{
				DirectionTypes directionType = directionXY(plot(), pPlot);
				//								N			NE				E				SE					S				SW					W				NW
				NiPoint2 directions[8] = {NiPoint2(0, 1), NiPoint2(1, 1), NiPoint2(1, 0), NiPoint2(1, -1), NiPoint2(0, -1), NiPoint2(-1, -1), NiPoint2(-1, 0), NiPoint2(-1, 1)};
				NiPoint3 attackDirection = NiPoint3(directions[directionType].x, directions[directionType].y, 0);
				float plotSize = GC.getPLOT_SIZE();
				NiPoint3 lookAtPoint(plot()->getPoint().x + plotSize / 2 * attackDirection.x, plot()->getPoint().y + plotSize / 2 * attackDirection.y, (plot()->getPoint().z + pPlot->getPoint().z) / 2);
				attackDirection.Unitize();
				gDLL->getInterfaceIFace()->lookAt(lookAtPoint, (((getOwnerINLINE() != GC.getGameINLINE().getActivePlayer()) || gDLL->getGraphicOption(GRAPHICOPTION_NO_COMBAT_ZOOM)) ? CAMERALOOKAT_BATTLE : CAMERALOOKAT_BATTLE_ZOOM_IN), attackDirection);

			}
			else
			{
				PlayerTypes eAttacker = getVisualOwner(pDefender->getTeam());
				CvWString szMessage;
				if (UNKNOWN_PLAYER != eAttacker)
				{
					szMessage = gDLL->getText("TXT_KEY_MISC_YOU_UNITS_UNDER_ATTACK", GET_PLAYER(eAttacker).getNameKey());
				}
				else
				{
					szMessage = gDLL->getText("TXT_KEY_MISC_YOU_UNITS_UNDER_ATTACK_UNKNOWN");
				}

				gDLL->getInterfaceIFace()->addMessage(pDefender->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szMessage, "AS2D_COMBAT", MESSAGE_TYPE_DISPLAY_ONLY, getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE(), true);
			}
		}

		FAssertMsg(pDefender != NULL, "Defender is not assigned a valid value");

		FAssertMsg(plot()->isFighting(), "Current unit instance plot is not fighting as expected");
		FAssertMsg(pPlot->isFighting(), "pPlot is not fighting as expected");

		if (!pDefender->canDefend())
		{
			if (!pPlot->isValidDomainForAction(*pDefender) && pDefender->getUnitInfo().isMechUnit())
			{
				CvPlot* pRandomPlot = pDefender->randomEscapePlot(1, true);
				if (pRandomPlot != NULL) 
				{
					pDefender->setPostCombatState(POST_COMBAT_ESCAPE);
					pDefender->setPostCombatPlot(pRandomPlot->getX_INLINE(), pRandomPlot->getY_INLINE());
					pDefender->setTempInvisible(true);
				} 
				else 
				{
					// Kill them!
					pDefender->setDamage(GC.getMAX_HIT_POINTS(), this);
				}
				bFinish = true;
			}
			else 
			{
				if (!bVisible)
				{
					bFinish = true;
				}
				else
				{
					CvMissionDefinition kMission;
					kMission.setMissionTime(getCombatTimer() * gDLL->getSecsPerTurn());
					kMission.setMissionType(MISSION_SURRENDER);
					kMission.setUnit(BATTLE_UNIT_ATTACKER, this);
					kMission.setUnit(BATTLE_UNIT_DEFENDER, pDefender);
					kMission.setPlot(pPlot);
					gDLL->getEntityIFace()->AddMission(&kMission);

					// Surrender mission
					setCombatTimer(GC.getMissionInfo(MISSION_SURRENDER).getTime());

					GC.getGameINLINE().incrementTurnTimer(getCombatTimer());
				}

				// Kill them!
				pDefender->setDamage(GC.getMAX_HIT_POINTS(), this);
			}
		}
		else
		{
			CvBattleDefinition kBattle;
			kBattle.setUnit(BATTLE_UNIT_ATTACKER, this);
			kBattle.setUnit(BATTLE_UNIT_DEFENDER, pDefender);
			kBattle.setDamage(BATTLE_UNIT_ATTACKER, BATTLE_TIME_BEGIN, getDamage());
			kBattle.setDamage(BATTLE_UNIT_DEFENDER, BATTLE_TIME_BEGIN, pDefender->getDamage());

			//
			// Resolve Combat all logic is here 
			//
			resolveCombat(pDefender, pPlot, kBattle);

			//Munition
			ammunitionProcess(pDefender);

			if (!bVisible)
			{
				bFinish = true;
			}
			else
			{
				kBattle.setDamage(BATTLE_UNIT_ATTACKER, BATTLE_TIME_END, getDamage());
				kBattle.setDamage(BATTLE_UNIT_DEFENDER, BATTLE_TIME_END, pDefender->getDamage());
				kBattle.setAdvanceSquare(canAdvance(pPlot, 1));
				if (isRanged() && pDefender->isRanged()) //ranged
				{
					kBattle.setDamage(BATTLE_UNIT_ATTACKER, BATTLE_TIME_RANGED, kBattle.getDamage(BATTLE_UNIT_ATTACKER, BATTLE_TIME_END));
					kBattle.setDamage(BATTLE_UNIT_DEFENDER, BATTLE_TIME_RANGED, kBattle.getDamage(BATTLE_UNIT_DEFENDER, BATTLE_TIME_END));
				}
				else if (kBattle.isOneStrike()) //melee dies right away
				{
					kBattle.setDamage(BATTLE_UNIT_ATTACKER, BATTLE_TIME_RANGED, kBattle.getDamage(BATTLE_UNIT_ATTACKER, BATTLE_TIME_END));
					kBattle.setDamage(BATTLE_UNIT_DEFENDER, BATTLE_TIME_RANGED, kBattle.getDamage(BATTLE_UNIT_DEFENDER, BATTLE_TIME_END));
				}
				else //melee fighting
				{
					kBattle.addDamage(BATTLE_UNIT_ATTACKER, BATTLE_TIME_RANGED, kBattle.getDamage(BATTLE_UNIT_ATTACKER, BATTLE_TIME_BEGIN));
					kBattle.addDamage(BATTLE_UNIT_DEFENDER, BATTLE_TIME_RANGED, kBattle.getDamage(BATTLE_UNIT_DEFENDER, BATTLE_TIME_BEGIN));
				}

				int iTurns = planBattle( kBattle);
				if (getProfession() == NO_PROFESSION || (GC.getProfessionInfo(getProfession()).getBombardRate() == -1))
				{
					kBattle.setMissionTime(iTurns * gDLL->getSecsPerTurn());
					setCombatTimer(iTurns);
				}
				else
				{
					kBattle.setMissionTime(iTurns * gDLL->getSecsPerTurn()/2);
					setCombatTimer(iTurns/2);
				}
				GC.getGameINLINE().incrementTurnTimer(getCombatTimer());
				if (pPlot->isActiveVisible(false))
				{
					ExecuteMove(0.5f, true);
					gDLL->getEntityIFace()->AddMission(&kBattle);
				}
			}
		}
	}

	if (bFinish)
	{
		if (bVisible)
		{
			if (isCombatFocus() && gDLL->getInterfaceIFace()->isCombatFocus())
			{
				if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
				{
					gDLL->getInterfaceIFace()->releaseLockedCamera();
				}
			}
		}
		//end the combat mission if this code executes first
		gDLL->getEntityIFace()->RemoveUnitFromBattle(this);
		gDLL->getEntityIFace()->RemoveUnitFromBattle(pDefender);
		setAttackPlot(NULL);
		bool bDefenderRobbed = (pDefender->getPostCombatState() == POST_COMBAT_ROB);
		bool bDefenderEscaped = (pDefender->getPostCombatPlot() != pPlot);
		bool bAttackerEscaped = (getPostCombatPlot() != plot());

		setCombatUnit(NULL);
		if (!bDefenderRobbed)
		{
			pDefender->setCombatUnit(NULL);
		}
		pDefender->setPostCombatState(NO_POST_COMBAT);
		NotifyEntity(MISSION_DAMAGE);
		pDefender->NotifyEntity(MISSION_DAMAGE);

		manageDisturbationFromCombat(pDefender);
		if (pDefender->isHuman())
		{
			pDefender->getGroup()->setAutomateType(NO_AUTOMATE);
		}


		if (isDead())
		{
			if (pDefender->getUnitInfo().isPirate())
			{
				pDefender->setTempInvisible(true);
			}
			if (!m_pUnitInfo->isHiddenNationality() && !pDefender->getUnitInfo().isHiddenNationality())
			{
				GET_TEAM(pDefender->getTeam()).AI_changeWarSuccess(getTeam(), GC.getDefineINT("WAR_SUCCESS_DEFENDING"));
			}

			szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_DIED_ATTACKING", getNameOrProfessionKey(), pDefender->getNameOrProfessionKey());
			gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, GC.getEraInfo(GC.getGameINLINE().getCurrentEra()).getAudioUnitDefeatScript(), MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
			szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_KILLED_ENEMY_UNIT", pDefender->getNameOrProfessionKey(), getNameOrProfessionKey(), getVisualCivAdjective(pDefender->getTeam()));
			gDLL->getInterfaceIFace()->addMessage(pDefender->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, GC.getEraInfo(GC.getGameINLINE().getCurrentEra()).getAudioUnitVictoryScript(), MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
			if (pDefender->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->insertIntoSelectionList(pDefender, true, false);
			}
			// report event to Python, along with some other key state
			gDLL->getEventReporterIFace()->combatResult(pDefender, this);
		}
		else if (pDefender->isDead())
		{
			if (m_pUnitInfo->isPirate())
			{
				setTempInvisible(true);
			}

			if (!m_pUnitInfo->isHiddenNationality() && !pDefender->getUnitInfo().isHiddenNationality())
			{
				GET_TEAM(getTeam()).AI_changeWarSuccess(pDefender->getTeam(), GC.getDefineINT("WAR_SUCCESS_ATTACKING"));
				if (GET_PLAYER(getOwnerINLINE()).isNative())
				{
					GET_TEAM(getTeam()).AI_changeDamages(pDefender->getTeam(), -2 * pDefender->getUnitInfo().getAssetValue());
				}
			}
			GET_PLAYER(pDefender->getOwnerINLINE()).kingHelpAIToDefendShips(pDefender->getUnitType(), 100);
			szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_DESTROYED_ENEMY", getNameOrProfessionKey(), pDefender->getNameOrProfessionKey());
			gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, GC.getEraInfo(GC.getGameINLINE().getCurrentEra()).getAudioUnitVictoryScript(), MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
			if (getVisualOwner(pDefender->getTeam()) != getOwnerINLINE())
			{
				szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_WAS_DESTROYED_UNKNOWN", pDefender->getNameOrProfessionKey(), getNameOrProfessionKey());
			}
			else
			{
				szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_WAS_DESTROYED", pDefender->getNameOrProfessionKey(), getNameOrProfessionKey(), getVisualCivAdjective(pDefender->getTeam()));
			}
			gDLL->getInterfaceIFace()->addMessage(pDefender->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer,GC.getEraInfo(GC.getGameINLINE().getCurrentEra()).getAudioUnitDefeatScript(), MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());

			// report event to Python, along with some other key state
			gDLL->getEventReporterIFace()->combatResult(this, pDefender);

			bool bAdvance = false;
			bool bRaided = raidWeapons(pDefender);

			if (pDefender->isUnarmed() && !GET_PLAYER(getOwnerINLINE()).isNative())
			{
				if (!isNoUnitCapture())
				{
					CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
					while (pUnitNode != NULL)
					{
						CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
						pUnitNode = pPlot->nextUnitNode(pUnitNode);

						if (pLoopUnit != pDefender)
						{
							if (isEnemy(pLoopUnit->getCombatTeam(getTeam(), pPlot), pPlot))
							{
								pLoopUnit->setCapturingPlayer(getOwnerINLINE());
							}
						}
					}
				}
			}
			bAdvance = canAdvance(pPlot, ((pDefender->canDefend()) ? 1 : 0));
			if (bAdvance)
			{
				if (!isNoUnitCapture())
				{
					if (!pDefender->canDefend())
					{
						pDefender->setCapturingPlayer(getOwnerINLINE());
					}
				}
			}

			if (pDefender->getUnitInfo().isPirate())
			{
				int iStolenGold = pDefender->getStolenGold() * 2;// We obtain 2 times of the stolen gold of pirates (Kind of reward)
				if (iStolenGold > 0)
				{
					CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());
					kPlayer.changeGold(iStolenGold);
					szBuffer = gDLL->getText("TXT_KEY_GET_PART_OF_STOLEN_GOLD", iStolenGold);
					gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
				} 
			}

			pDefender->kill(false);
			pDefender = NULL;

			if (!bAdvance)
			{
				changeMoves(pPlot->movementCost(this, plot()));

				if (!canMove() || !isBlitz())
				{
					if (IsSelected())
					{
						if (gDLL->getInterfaceIFace()->getLengthSelectionList() > 1)
						{
							gDLL->getInterfaceIFace()->removeFromSelectionList(this);
						}
					}
				}
			}

			if (!bRaided)
			{
				CvCity* pCity = pPlot->getPlotCity();
				if (pCity != NULL)
				{
					if (!raidWeapons(pCity))
					{
						raidGoods(pCity);
					}
				}
			}

			if (pPlot->getNumVisibleEnemyDefenders(this) == 0)
			{
				CvCity* pCity = pPlot->getPlotCity();
				bool bCanMoveToPlot = true;
				if (pCity != NULL && pCity->getPopulation() >= 1 && isNative()) 
				{
					bCanMoveToPlot = false;
				}
				if (bCanMoveToPlot)
				{	
					getGroup()->groupMove(pPlot, true, ((bAdvance) ? this : NULL));
				}
			}
			// This is is put before the plot advancement, the unit will always try to walk back
			// to the square that they came from, before advancing.
			getGroup()->clearMissionQueue();
		}
		else if (bDefenderRobbed)
		{
			pDefender->setTempInvisibleAllPlot(true);
			CvPlayer& pDefOwner = GET_PLAYER(pDefender->getOwner());
			if (isHuman())
			{
				CvUnit* pNewUnit = GET_PLAYER(getOwnerINLINE()).initUnit(pDefender->getUnitType(), pDefender->getProfession(), pDefender->getX_INLINE(), pDefender->getY_INLINE());
				if (pNewUnit != NULL)
				{
					pNewUnit->setOriginalOwner(kDefPlayer.getID());//Important to be before convert method
					CvUnit* pTempCombatUnit = pDefender->getCombatUnit();
					pDefender->setCombatUnit(NULL);
					pNewUnit->setCombatUnit(pTempCombatUnit);
					pNewUnit->convert(pDefender, true);
					setTempInvisibleAllPlot(true);
					pNewUnit->setTempInvisible(true);
					getGroup()->groupMove(pPlot, true, this);
					getGroup()->clearMissionQueue();
					if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
					{
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_ROB_UNIT, pNewUnit->getID(), getID());
						gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
					}
				}
			}
			else
			{
				if (isHuman() || !isAutomated())
				{					
					pDefender->setTempInvisibleAllPlot(true);	
					getGroup()->groupMove(pPlot, true, this);
					getGroup()->clearMissionQueue();
				}
				if (m_pUnitInfo->isPirate() || GC.getGameINLINE().getSorenRandNum(100, "Rob the ship proba") < 80)
				{
					setTempInvisibleAllPlot(true);
					pDefender->setCombatUnit(NULL);
					if (pDefender->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
					{
						gDLL->getInterfaceIFace()->insertIntoSelectionList(pDefender, true, false);
					}
					robShip(pDefender);
				}
				else
				{
					captureShip(pDefender);
				}
			}
		}
		else if (bDefenderEscaped)
		{
			if (m_pUnitInfo->isPirate())
			{
				setTempInvisible(true);
			}
			if (pDefender->getUnitInfo().isPirate())
			{
				pDefender->setTempInvisible(true);
			}
			szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_ESCAPED", getNameOrProfessionKey(), pDefender->getNameOrProfessionKey());
			gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"), pPlot->getX_INLINE(), pPlot->getY_INLINE());

			szBuffer = gDLL->getText("TXT_KEY_MISC_ENEMY_UNIT_ESCAPED", getNameOrProfessionKey(), pDefender->getNameOrProfessionKey());
			gDLL->getInterfaceIFace()->addMessage(pDefender->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, GC.getEraInfo(GC.getGameINLINE().getCurrentEra()).getAudioUnitVictoryScript(), MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pDefender->plot()->getX_INLINE(), pDefender->plot()->getY_INLINE());

			getGroup()->clearMissionQueue();
		}
		else if (bAttackerEscaped)
		{
			if (m_pUnitInfo->isPirate())
			{
				setTempInvisible(true);
			}
			if (pDefender->getUnitInfo().isPirate())
			{
				pDefender->setTempInvisible(true);
			}
			szBuffer = gDLL->getText("TXT_KEY_MISC_ENEMY_UNIT_WIDHDRAW", getNameOrProfessionKey(), pDefender->getNameOrProfessionKey());
			gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_OUR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), plot()->getX_INLINE(), plot()->getY_INLINE());

			szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_WIDHDRAW", getNameOrProfessionKey(), pDefender->getNameOrProfessionKey());
			gDLL->getInterfaceIFace()->addMessage(pDefender->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"), pPlot->getX_INLINE(), pPlot->getY_INLINE());

			if (IsSelected())
			{
				if (gDLL->getInterfaceIFace()->getLengthSelectionList() > 1)
				{
					gDLL->getInterfaceIFace()->removeFromSelectionList(this);
				}
			}

			// This is is put before the plot advancement, the unit will always try to walk back
			// to the square that they came from, before advancing.
			getGroup()->clearMissionQueue();
		}
		else
		{
			if (getProfession() == NO_PROFESSION || (GC.getProfessionInfo(getProfession()).getBombardRate() == -1))
			{
				szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_WITHDRAW", getNameOrProfessionKey(), pDefender->getNameOrProfessionKey());
				gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_OUR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
			}
			szBuffer = gDLL->getText("TXT_KEY_MISC_ENEMY_UNIT_WITHDRAW", getNameOrProfessionKey(), pDefender->getNameOrProfessionKey());
			gDLL->getInterfaceIFace()->addMessage(pDefender->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
			changeMoves(std::max(GC.getMOVE_DENOMINATOR(), pPlot->movementCost(this, plot())));
			CvCity* pCity = pPlot->getPlotCity();
			if (pCity != NULL)
			{
				raidGoods(pCity);
			}

			getGroup()->clearMissionQueue();
		}
	}
}

bool CvUnit::isActionRecommended(int iAction)
{
	if (getOwnerINLINE() != GC.getGameINLINE().getActivePlayer())
	{
		return false;
	}

	if (GET_PLAYER(getOwnerINLINE()).isOption(PLAYEROPTION_NO_UNIT_RECOMMENDATIONS))
	{
		return false;
	}

	//DOANE Python Callbacks | Speed Improvement
	if (GC.getUSE_ON_IS_ACTION_RECOMMENDED_CALLBACK())
	{
		CyUnit* pyUnit = new CyUnit(this);
		CyArgsList argsList;
		argsList.add(gDLL->getPythonIFace()->makePythonObject(pyUnit));	// pass in unit class
		argsList.add(iAction);
		long lResult=0;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "isActionRecommended", argsList.makeFunctionArgs(), &lResult); //Returns false as default
		delete pyUnit;	// python fxn must not hold on to this pointer
		if (lResult == 1)
		{
			return true;
		}
	}
	//END DOANE

	CvPlot* pPlot = gDLL->getInterfaceIFace()->getGotoPlot();
	if (pPlot == NULL)
	{
		if (gDLL->shiftKey())
		{
			pPlot = getGroup()->lastMissionPlot();
		}
	}

	if (pPlot == NULL)
	{
		pPlot = plot();
	}

	switch (GC.getActionInfo(iAction).getMissionType())
	{
	case MISSION_FORTIFY:
		if (pPlot->isCity(true, getTeam()))
		{
			if (canDefend(pPlot) && !isUnarmed())
			{
				if (pPlot->getNumDefenders(getOwnerINLINE()) < ((atPlot(pPlot)) ? 2 : 1))
				{
					return true;
				}
			}
		}
		break;
	case MISSION_HEAL:
		if (isHurt())
		{
			if (!hasMoved())
			{
				if ((pPlot->getTeam() == getTeam()) || (healTurns(pPlot) < 4))
				{
					return true;
				}
			}
		}
		break;

	case MISSION_FOUND:
		if (canFound(pPlot))
		{
			if (pPlot->isBestAdjacentFound(getOwnerINLINE()))
			{
				return true;
			}
		}
		break;

	default:
		break;
	}

	switch (GC.getActionInfo(iAction).getCommandType())
	{
	case COMMAND_PROMOTION:
	case COMMAND_PROMOTE:
	case COMMAND_KING_TRANSPORT:
	case COMMAND_ESTABLISH_MISSION:
	case COMMAND_SPEAK_WITH_CHIEF:
	case COMMAND_YIELD_TRADE:
	case COMMAND_LEARN:	
		return true;
	default:
		break;
	}

	return false;
}




bool CvUnit::isBetterDefenderThan(const CvUnit* pDefender, const CvUnit* pAttacker, bool bBreakTies) const
{
	int iOurDefense;
	int iTheirDefense;

	if (pDefender == NULL)
	{
		return true;
	}

	TeamTypes eAttackerTeam = NO_TEAM;
	if (NULL != pAttacker)
	{
		eAttackerTeam = pAttacker->getTeam();
	}

	if (canCoexistWithEnemyUnit(eAttackerTeam))
	{
		return false;
	}

	if (!canDefend())
	{
		return false;
	}

	if (canDefend() && !(pDefender->canDefend()))
	{
		return true;
	}

	bool bOtherUnarmed = pDefender->isUnarmed();
	if (isUnarmed() != bOtherUnarmed)
	{
		return bOtherUnarmed;
	}

	iOurDefense = currCombatStr(plot(), pAttacker);

	iOurDefense /= (getCargo() + 1);

	iTheirDefense = pDefender->currCombatStr(plot(), pAttacker);

	iTheirDefense /= (pDefender->getCargo() + 1);

	if (iOurDefense == iTheirDefense)
	{
		if (isOnMap() && !pDefender->isOnMap())
		{
			++iOurDefense;
		}
		else if (!isOnMap() && pDefender->isOnMap())
		{
			++iTheirDefense;
		}
		if (NO_UNIT == getLeaderUnitType() && NO_UNIT != pDefender->getLeaderUnitType())
		{
			++iOurDefense;
		}
		else if (NO_UNIT != getLeaderUnitType() && NO_UNIT == pDefender->getLeaderUnitType())
		{
			++iTheirDefense;
		}
		else if (bBreakTies && isBeforeUnitCycle(this, pDefender))
		{
			++iOurDefense;
		}
	}

	return (iOurDefense > iTheirDefense);
}


bool CvUnit::canDoCommand(CommandTypes eCommand, int iData1, int iData2, bool bTestVisible, bool bTestBusy)
{
	CvUnit* pUnit;

	if (getGroup() == NULL)
	{
		return false;
	}

	if (bTestBusy && getGroup()->isBusy())
	{
		return false;
	}

	if (GET_PLAYER(getOwnerINLINE()).isTerritorialInfluenceMode())
	{
		return false;
	}

	switch (eCommand)
	{
	case COMMAND_PROMOTION:
		if (canPromote((PromotionTypes)iData1, iData2))
		{
			return true;
		}
		break;

	case COMMAND_UPGRADE:
		if (canUpgrade(((UnitTypes)iData1), bTestVisible))
		{
			return true;
		}
		break;

	case COMMAND_AUTOMATE:
		if (canAutomate((AutomateTypes)iData1))
		{
			return true;
		}
		break;

	case COMMAND_WAKE:
		if (!isAutomated() && isWaiting())
		{
			return true;
		}
		break;

	case COMMAND_CANCEL:
	case COMMAND_CANCEL_ALL:
		if (!isAutomated() && (getGroup()->getLengthMissionQueue() > 0) && getGroup()->getMissionType(0) != MISSION_MOVE_TO)
		{
			return true;
		}
		break;

	case COMMAND_STOP_AUTOMATION:
		if (isAutomated())
		{
			return true;
		}
		break;

	case COMMAND_DELETE:
		if (canScrap())
		{
			return true;
		}
		break;

	case COMMAND_GIFT:
		if (canGift(bTestVisible) && !isAutomated())
		{
			return true;
		}
		break;

	case COMMAND_LOAD:
		if (canLoad(plot(), true))
		{
			return true;
		}
		break;

	case COMMAND_LOAD_YIELD:
		if (canLoadYield(plot(), (YieldTypes) iData1, false) && !isAutomated())
		{
			return true;
		}
		break;

	case COMMAND_LOAD_CARGO:
		if (canLoadYield(plot(), NO_YIELD, false) && !isAutomated())
		{
			return true;
		}
		break;

	case COMMAND_LOAD_UNIT:
		pUnit = ::getUnit(IDInfo(((PlayerTypes)iData1), iData2));
		if (pUnit != NULL)
		{
			if (canLoadUnit(pUnit, plot(), true))
			{
				return true;
			}
		}
		break;

	case COMMAND_YIELD_TRADE:
		if (canTradeYield(plot()))
		{
			return true;
		}
		break;

	case COMMAND_SAIL_TO_EUROPE:
		if (canCrossOcean(plot(), (UnitTravelStates)iData1) && !isAutomated())
		{
			return true;
		}
		break;

	case COMMAND_CHOOSE_TRADE_ROUTES:
	case COMMAND_ASSIGN_TRADE_ROUTE:
		if (iData2 == 0 || canAssignTradeRoute(iData1))
		{
			return true;
		}
		break;

	case COMMAND_CHOOSE_AGREEMENTS:
		if (canAssignAgreements())
		{
			return true;
		}
		break;

	case COMMAND_PROMOTE:
		{
			if (iData1 == 1)
			{
				if (isPromotionReady())
				{
					return true;
				}
			} 
			else 
			{
				CvSelectionGroup* pSelection = gDLL->getInterfaceIFace()->getSelectionList();
				if (pSelection != NULL)
				{
					if (pSelection->isPromotionReady())
					{
						return true;
					}
				}
			}
		}
		break;

	case COMMAND_PROFESSION:
		{
			if (iData1 == -1)
			{
				CvSelectionGroup* pSelection = gDLL->getInterfaceIFace()->getSelectionList();
				if (pSelection != NULL)
				{
					if (pSelection->canChangeProfession())
					{
						return true;
					}
				}
			}
			else
			{
				if (canHaveProfession((ProfessionTypes) iData1, false))
				{
					return true;
				}
			}
		}
		break;

	case COMMAND_CLEAR_SPECIALTY:
		if (canClearSpecialty())
		{
			return true;
		}
		break;

	case COMMAND_UNLOAD:
		if (canUnload())
		{
			return true;
		}
		break;

	case COMMAND_UNLOAD_ALL:
		if (canUnloadAll())
		{
			return true;
		}
		break;

	case COMMAND_RESUPPLY:
		if (canResupply())
		{
			return true;
		}
		break;

	case COMMAND_RESUPPLY_SHIPS:
		if (canResupplyShips())
		{
			return true;
		}
		break;

	case COMMAND_REPAIR_SHIPS:
		if (canEuropeRepairShip(iData1 == 1))
		{
			return true;
		}
		break;

	case COMMAND_STOP_REPAIR_SHIP:
		if (isInEuropeDrydock())
		{
			return true;
		}
		break;

	case COMMAND_PURCHASE_BID_SHIP:
		return true;
		break;

	case COMMAND_EMBARK_ON_SHIP:
		if (canEmbarkOnShip())
		{
			return true;
		}
		break;

	case COMMAND_LOAD_CREW:
		if (canLoadCrew())
		{
			return true;
		}
		break;

	case COMMAND_UNLOAD_CREW:
		if (canUnloadCrew(getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE))
		{
			return true;
		}
		break;

	case COMMAND_SWAP_CREW:
		if (canSwapCrew())
		{
			return true;
		}
		break;

	case COMMAND_RESUPPLY_ONLY_ONE_SHIP:
	case COMMAND_STOP_SELL_SHIP:
	case COMMAND_TRANSFERT_EUROPE_TO_SHIP:
	case COMMAND_TRANSFERT_WAREHOUSE_TO_SHIP:
	case COMMAND_REMOVE_ASSIGNED_UNIT:
	case COMMAND_COME_BACK_INTO_EUROPE:
	case COMMAND_CHANGE_SELL_SHIP_PRICE:
	case COMMAND_RESUPPLY_SHIPS_EQUIPMENT:
	case COMMAND_TRADE_PROPOSITION:
	case COMMAND_SET_NEW_CARGO_YIELD:
	case COMMAND_SET_RESUPPLY_YIELD:
	case COMMAND_UNLOAD_NEW_CARGO_IN_CITY:
		return true;
		break;

	case COMMAND_UNLOAD_NEW_CARGO:
		if (canUnloadNewCargoInShip())
		{
			return true;
		}
		break;
	case COMMAND_SELL_SHIP:
		if (canSell())
		{
			return true;
		}
		break;

	case COMMAND_RESUPPLY_BUILDER_PACK:
		if (canResupplyBuilderPack(false))
		{
			return true;
		}
		break;

	case COMMAND_DELETE_BUILDER_PACK:
		if (canDeleteBuilderPack())
		{
			return true;
		}
		break;

	case COMMAND_MAKE_SEAWAY:
		if (canMakeSeaway(false))
		{
			return true;
		}
		break;

	case COMMAND_DELETE_SEAWAY:
		if (iData1 != -1 || canMakeSeaway(true))
		{
			return true;
		}
		break;

	case COMMAND_REMOVE_RESSOURCE:
		if (canRemoveRessource())
		{
			return true;
		}
		break;

	case COMMAND_GIVE_MAP:
		if (canGiveMap())
		{
			return true;
		}
		break;

	case COMMAND_LEARN:
		if (canLearn())
		{
			return true;
		}
		break;

	case COMMAND_KING_TRANSPORT:
		if (canKingTransport())
		{
			return true;
		}
		break;

	case COMMAND_ESTABLISH_MISSION:
		if (canEstablishMission())
		{
			return true;
		}
		break;

	case COMMAND_SPEAK_WITH_CHIEF:
		if (canSpeakWithChief(plot()))
		{
			return true;
		}
		break;

	case COMMAND_HOTKEY:
		if (isGroupHead())
		{
			return true;
		}
		break;

	default:
		FAssert(false);
		break;
	}

	return false;
}


void CvUnit::doCommand(CommandTypes eCommand, int iData1, int iData2)
{
	CvUnit* pUnit;
	bool bCycle;

	bCycle = false;

	FAssert(getOwnerINLINE() != NO_PLAYER);

	if (canDoCommand(eCommand, iData1, iData2))
	{
		switch (eCommand)
		{
		case COMMAND_PROMOTION:
			promote((PromotionTypes)iData1, iData2);
			break;

		case COMMAND_UPGRADE:
			upgrade((UnitTypes)iData1);
			bCycle = true;
			break;

		case COMMAND_AUTOMATE:
			automate((AutomateTypes)iData1);
			bCycle = true;
			break;

		case COMMAND_WAKE:
			getGroup()->setActivityType(ACTIVITY_AWAKE);
			break;

		case COMMAND_CANCEL:
			getGroup()->popMission();
			reloadEntity();
			break;

		case COMMAND_CANCEL_ALL:
			getGroup()->clearMissionQueue();
			reloadEntity();
			break;

		case COMMAND_STOP_AUTOMATION:
			getGroup()->setAutomateType(NO_AUTOMATE);
			reloadEntity();
			if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
			{
				gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
			}
			break;

		case COMMAND_DELETE:
			scrap();
			bCycle = true;
			break;

		case COMMAND_GIFT:
			gift();
			bCycle = true;
			break;

		case COMMAND_LOAD:
			load(true);
			bCycle = true;
			break;

		case COMMAND_LOAD_YIELD:
			if (iData2 >= 0)
			{
				loadYieldAmount((YieldTypes) iData1, iData2, false);
			}
			else
			{
				loadYield((YieldTypes) iData1, false);
			}
			break;

		case COMMAND_LOAD_CARGO:
			if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_LOAD_CARGO);
				gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
			}
			break;

		case COMMAND_LOAD_UNIT:
			pUnit = ::getUnit(IDInfo(((PlayerTypes)iData1), iData2));
			if (pUnit != NULL)
			{
				loadUnit(pUnit);
				bCycle = true;
			}
			break;

		case COMMAND_YIELD_TRADE:
			{
				int iMode = iData1 == 1  ? 1 : 0;
				tradeYield(iMode);
			}
			break;

		case COMMAND_SAIL_TO_EUROPE:
			moveToSeaway(/*int*/iData2);			
			crossOcean((UnitTravelStates) iData1);
			break;

		case COMMAND_CHOOSE_TRADE_ROUTES:
			if (GET_PLAYER(getOwnerINLINE()).getNumTradeRoutes() > 0)
			{
				if (gDLL->getInterfaceIFace()->getHeadSelectedUnit() == this)
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_TRADE_ROUTES, getID());
					gDLL->getInterfaceIFace()->addPopup(pInfo);
				}
			}
			break;
		case COMMAND_CHOOSE_AGREEMENTS:
			if (GET_PLAYER(getOwnerINLINE()).getNumAgreements() > 0)
			{
				if (gDLL->getInterfaceIFace()->getHeadSelectedUnit() == this)
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_AGREEMENTS, getID());
					gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true, true);
				}
			}
			break;

		case COMMAND_ASSIGN_TRADE_ROUTE:
			if (isGroupHead())
			{
				//getGroup()->assignTradeRoute(iData1, iData2);
			}
			break;

		case COMMAND_PROMOTE:
			if (iData1 == 1 || gDLL->getInterfaceIFace()->getHeadSelectedUnit() == this)
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_PROMOTE);
				pInfo->setData1(getGroupID());
				gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true, true);
			}
			break;

		case COMMAND_PROFESSION:
			if (iData1 == -1)
			{
				if (gDLL->getInterfaceIFace()->getHeadSelectedUnit() == this)
				{
					CvPlot* pPlot = plot();
					if (pPlot != NULL)
					{
						CvCity* pCity = pPlot->getPlotCity();
						if (pCity != NULL)
						{
							CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_CHOOSE_PROFESSION, pCity->getID(), getID());
							gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true, true);
						}
					}
				}
			}
			else
			{
				setProfession((ProfessionTypes) iData1);
			}
			break;

		case COMMAND_CLEAR_SPECIALTY:
			clearSpecialty();
			break;

		case COMMAND_UNLOAD:
			unload();
			bCycle = true;
			break;

		case COMMAND_UNLOAD_ALL:
			unloadAll();
			bCycle = true;
			break;

		case COMMAND_RESUPPLY:
			if (gDLL->getInterfaceIFace()->getHeadSelectedUnit() == this || getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
			{
				resupplyUnits();
			}
			break;

		case COMMAND_RESUPPLY_SHIPS:
			if (gDLL->getInterfaceIFace()->getHeadSelectedUnit() == this || getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
			{
				resupplyShips();
			}
			break;
		case COMMAND_REPAIR_SHIPS:
			if (getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
			{
				doEuropeRepairShip(iData1 == 1);
			}
			break;
		case COMMAND_STOP_REPAIR_SHIP:
			if (getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
			{
				setInEuropeDrydock(false);
				gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen_DIRTY_BIT, true);
			}
			break;
		case COMMAND_PURCHASE_BID_SHIP:
			if (getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE)
			{
				doPurchaseBid();
			}
			break;
		case COMMAND_EMBARK_ON_SHIP:
			embarkOnShip();
			bCycle = true;
			break;

		case COMMAND_LOAD_CREW:
			loadCrew();
			break;

		case COMMAND_SELL_SHIP:
			sellShip();
			break;

		case COMMAND_TRADE_PROPOSITION:
			if (gDLL->getInterfaceIFace()->getHeadSelectedUnit() == this)
			{
				if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SEND_TRADE_PROPOSITION, getID(), iData1);
					gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
				}
			}
			break;

		case COMMAND_SET_NEW_CARGO_YIELD:
			setNewCargoYield((YieldTypes)iData1, iData2);
			break;

		case COMMAND_SET_RESUPPLY_YIELD:
			setResupplyYield((YieldTypes)iData1, iData2);
			break;

		case COMMAND_STOP_SELL_SHIP:
			stopSellShip();
			break;

		case COMMAND_TRANSFERT_EUROPE_TO_SHIP:
			transfertEuropeToShip((YieldTypes)iData1, iData2, true);
			break;
		case COMMAND_TRANSFERT_WAREHOUSE_TO_SHIP:
			transfertWarehouseToShip((YieldTypes)iData1, iData2);
			break;

		case COMMAND_UNLOAD_NEW_CARGO_IN_CITY:
			unloadNewCargoInCity((YieldTypes)iData1, iData2);
			break;

		case COMMAND_REMOVE_ASSIGNED_UNIT:
			{
				CvSelectionGroup* pGroup = getGroup();
				pGroup->assignAgreement(iData1, false);
			}
			break;

		case COMMAND_COME_BACK_INTO_EUROPE:
			comeBackInToEurope();
			break;

		case COMMAND_RESUPPLY_SHIPS_EQUIPMENT:
			resupplyGroupShips(iData1, iData2, false);
			break;
		case COMMAND_CHANGE_SELL_SHIP_PRICE:
			sellShip();
			break;

		case COMMAND_UNLOAD_NEW_CARGO:
			if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
			{
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_UNLOAD_NEW_CARGO, getID());
				gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true, true);
			}
			break;

		case COMMAND_UNLOAD_CREW:
			unloadCrew(getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);
			bCycle = true;
			break;

		case COMMAND_SWAP_CREW:
			swapCrew();
			break;

		case COMMAND_RESUPPLY_BUILDER_PACK:
			{
				if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
				{
					CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_RESUPPLY_BUILDER_PACK, getID(), canResupplyBuilderPack(true));
					gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
				}
			}
			break;

		case COMMAND_DELETE_BUILDER_PACK:
			{
				gDLL->sendPlayerAction(getOwnerINLINE(), PLAYER_ACTION_RESUPPLY_BUILDER_PACK, getID(), false, -1);
			}
			break;

		case COMMAND_MAKE_SEAWAY:
			{
				if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
				{
					CvPlot* pPlot = plot();
					CyArgsList argsList;
					argsList.add(pPlot->getX());
					argsList.add(pPlot->getY());
					long lResult=0;
					gDLL->getPythonIFace()->callFunction(PYGameModule, "createSeaway", argsList.makeFunctionArgs(), &lResult);
				}
			}
			break;

		case COMMAND_DELETE_SEAWAY:
			deleteSeaway();
			break;

		case COMMAND_RESUPPLY_ONLY_ONE_SHIP:
			resupplyOnlyOneShip(iData1, iData2);
			break;

		case COMMAND_REMOVE_RESSOURCE:
			removeRessource();
			break;

		case COMMAND_GIVE_MAP:
			giveMap();
			break;

		case COMMAND_LEARN:
			learn();
			bCycle = true;
			break;

		case COMMAND_KING_TRANSPORT:
			kingTransport(false);
			bCycle = true;
			break;

		case COMMAND_ESTABLISH_MISSION:
			establishMission();
			bCycle = true;
			break;

		case COMMAND_SPEAK_WITH_CHIEF:
			if (isGroupHead())
			{
				getGroup()->speakWithChief();
			}
			break;

		case COMMAND_HOTKEY:
			setHotKeyNumber(iData1);
			break;

		default:
			FAssert(false);
			break;
		}
	}

	if (bCycle)
	{
		if (IsSelected())
		{
			gDLL->getInterfaceIFace()->setCycleSelectionCounter(1);
		}
	}

	if (getGroup() != NULL)
	{
		getGroup()->doDelayedDeath();
	}
}


FAStarNode* CvUnit::getPathLastNode() const
{
	return getGroup()->getPathLastNode();
}


CvPlot* CvUnit::getPathEndTurnPlot() const
{
	return getGroup()->getPathEndTurnPlot();
}

int CvUnit::getPathCost() const
{
	return getGroup()->getPathCost();
}

bool CvUnit::generatePath(const CvPlot* pToPlot, int iFlags, bool bReuse, int* piPathTurns) const
{
	return getGroup()->generatePath(plot(), pToPlot, iFlags, bReuse, piPathTurns);
}

bool CvUnit::canEnterTerritory(PlayerTypes ePlayer, bool bIgnoreRightOfPassage) const
{
	if (ePlayer == NO_PLAYER)
	{
		return true;
	}

	TeamTypes eTeam = GET_PLAYER(ePlayer).getTeam();

	if (GET_TEAM(getTeam()).isFriendlyTerritory(eTeam))
	{
		return true;
	}

	if (isEnemy(eTeam))
	{
		return true;
	}

	if (isRivalTerritory())
	{
		return true;
	}

	if (alwaysInvisible())
	{
		return true;
	}

	if (GET_PLAYER(getOwnerINLINE()).isAlwaysOpenBorders())
	{
		return true;
	}

	if (GET_PLAYER(ePlayer).isAlwaysOpenBorders())
	{
		return true;
	}

	if (!bIgnoreRightOfPassage)
	{
		if (GET_TEAM(getTeam()).isOpenBorders(eTeam))
		{
			return true;
		}
	}
	if (m_pUnitInfo->isMechUnit() && isOnlyDefensive())
	{
		return true;
	}

	return false;
}

void CvUnit::processHavePurchaseBid()
{
	int iSellPrice = getShipSellPrice();
	int iPrixMarche = getSellPrice();
	if (iSellPrice == 0)
	{
		return;
	}
	if (isHasPurchaseBid())
	{
		int iDelete = GC.getGameINLINE().getSorenRandNum(100, "iRandomProba");
		if (iDelete%2)
		{
			setHasPurchaseBid(false);
			setProposePrice(0);
		}
	}
	int iProba = 100;
	int iRandomProba = GC.getGameINLINE().getSorenRandNum(100, "iRandomProba");

	if (iSellPrice > iPrixMarche/2)
	{
		if (iPrixMarche*3/4 <= iSellPrice)
		{
			iProba = 100 - ((iSellPrice - (iPrixMarche/2))*100/(iPrixMarche/2));
		}
		else
		{
			iProba = (100 - ((iSellPrice - (3*iPrixMarche/4))*100/(5*iPrixMarche/4)))/2;
		}
		if (iRandomProba < iProba)
		{
			int iRandomPrice = 10 + 5*GC.getGameINLINE().getSorenRandNum(100, "iRandomAccept")%7;
			int iProposePrice = iSellPrice - iSellPrice*iRandomPrice/100;
			setProposePrice(iProposePrice);
			setHasPurchaseBid(true);
		}
	}
}
ImmigrationTypes CvUnit::getImmigrationType() const
{
	return m_eImmigrationType;
}

void CvUnit::setImmigrationType(ImmigrationTypes eType)
{
	if (eType != getImmigrationType())
	{
		m_eImmigrationType = eType;
	}
}
bool CvUnit::canEnterArea(PlayerTypes ePlayer, const CvArea* pArea, bool bIgnoreRightOfPassage) const
{
	if (!canEnterTerritory(ePlayer, bIgnoreRightOfPassage))
	{
		return false;
	}

	return true;
}

// Returns the ID of the team to declare war against
TeamTypes CvUnit::getDeclareWarUnitMove(const CvPlot* pPlot) const
{
	FAssert(isHuman());

	if (!pPlot->isVisible(getTeam(), false))
	{
		return NO_TEAM;
	}

	bool bCityThreat = canAttack() && !isNoCityCapture() && getDomainType() == DOMAIN_LAND;
	if (getProfession() == PROFESSION_SCOUT)
	{
		bCityThreat = false;
	}

	//check territory
	TeamTypes eRevealedTeam = pPlot->getRevealedTeam(getTeam(), false);
	PlayerTypes eRevealedPlayer = pPlot->getRevealedOwner(getTeam(), false);
	if (eRevealedTeam != NO_TEAM)
	{
		if (GET_TEAM(getTeam()).canDeclareWar(pPlot->getTeam()))
		{
			if (!canEnterArea(eRevealedPlayer, pPlot->area()))
			{
				return eRevealedTeam;
			}

			if (getDomainType() == DOMAIN_SEA && !canCargoEnterArea(eRevealedPlayer, pPlot->area(), false) && getGroup()->isAmphibPlot(pPlot))
			{
				return eRevealedTeam;
			}

			if (pPlot->isCity() && bCityThreat)
			{
				if (GET_PLAYER(eRevealedPlayer).isAlwaysOpenBorders())
				{
					return eRevealedTeam;
				}
			}
		}
	}

	//check unit
	if (canMoveInto(pPlot, true, true))
	{
		CvUnit* pUnit = pPlot->plotCheck(PUF_canDeclareWar, getOwnerINLINE(), isAlwaysHostile(pPlot), NO_PLAYER, NO_TEAM, PUF_isVisible, getOwnerINLINE());
		if (pUnit != NULL)
		{
			if (!pPlot->isCity() || bCityThreat)
			{
				return pUnit->getTeam();
			}
		}
	}

	return NO_TEAM;
}

bool CvUnit::canMoveInto(const CvPlot* pPlot, bool bAttack, bool bDeclareWar, bool bIgnoreLoad) const
{
	FAssertMsg(pPlot != NULL, "Plot is not assigned a valid value");

	if (atPlot(pPlot))
	{
		return false;
	}

	if (pPlot->isImpassable())
	{
		if (!canMoveImpassable())
		{
			return false;
		}
	}

	CvArea *pPlotArea = pPlot->area();
	TeamTypes ePlotTeam = pPlot->getTeam();
	bool bCanEnterArea = canEnterArea(pPlot->getOwnerINLINE(), pPlotArea);
	if (bCanEnterArea)
	{
		if (pPlot->getFeatureType() != NO_FEATURE)
		{
			if (m_pUnitInfo->getFeatureImpassable(pPlot->getFeatureType()))
			{
				if (DOMAIN_SEA != getDomainType() || pPlot->getTeam() != getTeam())  // sea units can enter impassable in own cultural borders
				{
					return false;
				}
			}
		}
		else
		{
			if (m_pUnitInfo->getTerrainImpassable(pPlot->getTerrainType()))
			{
				if (DOMAIN_SEA != getDomainType() || pPlot->getTeam() != getTeam())  // sea units can enter impassable in own cultural borders
				{
					if (bIgnoreLoad || !canLoad(pPlot, true))
					{
						return false;
					}
				}
			}
		}
	}

	if (m_pUnitInfo->getMoves() == 0)
	{
		return false;
	}

	PlayerTypes ePlotPlayer = pPlot->getOwnerINLINE();
	CvPlot* pCurrentPlot = plot();
	switch (getDomainType())
	{
	case DOMAIN_SEA:
		if (!pPlot->isWater() && !m_pUnitInfo->isCanMoveAllTerrain())
		{
			if ((!pPlot->isFriendlyCity(*this, true) && !canBombard(pPlot)) || !pPlot->isCoastalLand())
			{
				return false;
			}
		}
		//DOANE PatchMod King's Ship can not enter native settlements
		if (pPlot->isCity() && GET_PLAYER(ePlotPlayer).isNative())
		{
			if (GET_PLAYER(ePlotPlayer).isEurope())
			{
				return false;
			}
		}
		//END DOANE
		if (pCurrentPlot->isCity() && pPlot->isLake() && ePlotPlayer != NO_PLAYER && GET_PLAYER(ePlotPlayer).isNative()) 
		{
			return false;
		}
		break;

	case DOMAIN_LAND:
		if (pPlot->isWater() && !m_pUnitInfo->isCanMoveAllTerrain())
		{
			if (bIgnoreLoad || plot()->isWater() || !canLoad(pPlot, false))
			{
				return false;
			}
		}
		break;

	case DOMAIN_IMMOBILE:
		return false;
		break;

	default:
		FAssert(false);
		break;
	}

	if (!bAttack)
	{
		if (isNoCityCapture() && pPlot->isEnemyCity(*this))
		{
			return false;
		}
	}

	if (bAttack)
	{
		if (isMadeAttack() && !isBlitz())
		{
			return false;
		}
	}
	if (canAttack())
	{
		CvTeam& kTeam = GET_TEAM(getTeam());
		if (bAttack || !canCoexistWithEnemyUnit(NO_TEAM, true))
		{
			if (!isHuman() || (pPlot->isVisible(getTeam(), false)))
			{
				if (pPlot->isVisibleEnemyUnit(this) != bAttack)
				{
					//FAssertMsg(isHuman() || (!bDeclareWar || (pPlot->isVisibleOtherUnit(getOwnerINLINE()) != bAttack)), "hopefully not an issue, but tracking how often this is the case when we dont want to really declare war");
					if (!bDeclareWar || (pPlot->isVisibleOtherUnit(getOwnerINLINE()) != bAttack && !(bAttack && pPlot->getPlotCity() && !isNoCityCapture())))
					{
						return false;
					}
				}
			}	
		}

		CvUnit* pUnit = pPlot->getBestDefender(NO_PLAYER, getOwner(), this);
		if (pUnit != NULL)
		{
			TeamTypes eTeam = pUnit->getTeam();
			if (kTeam.isFirstTurnOfWar(eTeam))
			{
				return false;
			}
		} 
		if (pPlot->isEnemyCity(*this)) 
		{
			if (kTeam.isFirstTurnOfWar(pPlot->getTeam()))
			{
				return false;
			}
		}
	}
	else
	{
		if (bAttack)
		{
			return false;
		}

		if (!canCoexistWithEnemyUnit(NO_TEAM, true))
		{
			if (!isHuman() || pPlot->isVisible(getTeam(), false))
			{
				if (pPlot->isEnemyCity(*this))
				{
					return false;
				}

				if (pPlot->isVisibleEnemyUnit(this))
				{
					return false;
				}
			}
		}
	}

	if (isHuman())
	{
		ePlotTeam = pPlot->getRevealedTeam(getTeam(), false);
		bCanEnterArea = canEnterArea(pPlot->getRevealedOwner(getTeam(), false), pPlotArea);
	}

	if (!bCanEnterArea)
	{
		FAssert(ePlotTeam != NO_TEAM);

		if (!(GET_TEAM(getTeam()).canDeclareWar(ePlotTeam)))
		{
			return false;
		}

		if (isHuman())
		{
			if (!bDeclareWar)
			{
				return false;
			}
		}
		else
		{
			if (GET_TEAM(getTeam()).AI_isSneakAttackReady(ePlotTeam))
			{
				if (!(getGroup()->AI_isDeclareWar(pPlot)))
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}

	if (getUnitType() == UNIT_PRIVATEER)
	{
		if (pPlot->getPlotCity() != NULL)
		{
			if (getTeam() != pPlot->getPlotCity()->getTeam())
			{
				return false;
			}
		}
	}

	if (bAttack && isArtilleryMen())
	{
		ArtilleryPenaltyTypes eArtilleryPenalty = getArtilleryPenalty(pPlot);
		if (eArtilleryPenalty != NO_ARTILLERY_PENALTY) 
		{
			CvArtilleryPenaltyInfo& kArtilleryPenalty = GC.getArtilleryPenaltyInfo(eArtilleryPenalty);
			int iLoopPenalty = kArtilleryPenalty.getPenalty();
			if (iLoopPenalty == 100) 
			{
				return false;
			}
		}
	}

	if (GC.getUSE_UNIT_CANNOT_MOVE_INTO_CALLBACK())
	{
		// Python Override
		CyArgsList argsList;
		argsList.add(getOwnerINLINE());	// Player ID
		argsList.add(getID());	// Unit ID
		argsList.add(pPlot->getX());	// Plot X
		argsList.add(pPlot->getY());	// Plot Y
		long lResult=0;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "unitCannotMoveInto", argsList.makeFunctionArgs(), &lResult); //Returns False as default

		if (lResult != 0)
		{
			return false;
		}
	}

	return true;
}

bool CvUnit::canMoveOrAttackInto(const CvPlot* pPlot, bool bDeclareWar) const
{
	return (canMoveInto(pPlot, false, bDeclareWar) || canMoveInto(pPlot, true, bDeclareWar));
}


bool CvUnit::canMoveThrough(const CvPlot* pPlot) const
{
	return canMoveInto(pPlot, false, false, true);
}


void CvUnit::attack(CvPlot* pPlot, bool bQuick)
{
	FAssert(canMoveInto(pPlot, true));
	FAssert(getCombatTimer() == 0);
	if (isBombardMode(pPlot))
	{
		CvCity* pCity = pPlot->getPlotCity();
		if (pCity != NULL && pCity->isBombardable(this)) 
		{
			getGroup()->pushMission(MISSION_BOMBARD, pPlot->getX_INLINE(), pPlot->getY_INLINE());
		} 
		else
		{
			resolveArtilleryCombat(pPlot);
		}		
	} 
	else
	{
		setAttackPlot(pPlot);

		updateCombat(bQuick);
	}
}

void CvUnit::move(CvPlot* pPlot, bool bShow)
{
	FAssert(canMoveOrAttackInto(pPlot) || isMadeAttack());

	CvPlot* pOldPlot = plot();

	changeMoves(pPlot->movementCost(this, plot()));

	setXY(pPlot->getX_INLINE(), pPlot->getY_INLINE(), true, true, bShow, bShow);

	//change feature
	FeatureTypes featureType = pPlot->getFeatureType();
	if (featureType != NO_FEATURE)
	{
		CvString featureString(GC.getFeatureInfo(featureType).getOnUnitChangeTo());
		if (!featureString.IsEmpty())
		{
			FeatureTypes newFeatureType = (FeatureTypes) GC.getInfoTypeForString(featureString);
			pPlot->setFeatureType(newFeatureType);
		}
	}

	if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		if (!(pPlot->isOwned()))
		{
			//spawn birds if trees present - JW
			if (featureType != NO_FEATURE)
			{
				if (GC.getASyncRand().get(100) < GC.getFeatureInfo(featureType).getEffectProbability())
				{
					EffectTypes eEffect = (EffectTypes)GC.getInfoTypeForString(GC.getFeatureInfo(featureType).getEffectType());
					gDLL->getEngineIFace()->TriggerEffect(eEffect, pPlot->getPoint(), (float)(GC.getASyncRand().get(360)));
					gDLL->getInterfaceIFace()->playGeneralSound("AS3D_UN_BIRDS_SCATTER", pPlot->getPoint());
				}
			}
		}
	}

	gDLL->getEventReporterIFace()->unitMove(pPlot, this, pOldPlot);

	if (getProfession() == PROFESSION_SCOUT || m_pUnitInfo->isMechUnit())
	{
		if (!pPlot->isRoute() && !pOldPlot->isRoute())
		{
			if (pPlot->getPlotCity() == NULL || pPlot->getPlotCity()->isNative())
			{
				int iDist = stepDistance(pOldPlot->getX_INLINE(), pOldPlot->getY_INLINE(), pPlot->getX_INLINE(), pPlot->getY_INLINE());

				if (iDist > 0)
				{
					changeExperienceExploAndNavigation(iDist);
				}
			}
		}
	}
	if (getOwnerINLINE() != GC.getGameINLINE().getActivePlayer())
	{
		return;
	}
	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());

	if (kPlayer.isOption(PLAYEROPTION_TUTORIAL_DOANE))
	{
		if (!kPlayer.isLaunchedPopup(BUTTONPOPUP_TUTORIAL_DOANE_0))
		{
			if (isHuman() && isHaveFoundPack())
			{
				CvPlot* pPlot = plot();
				if (pPlot != NULL)
				{
					if (pPlot->canFoundInShip(getOwnerINLINE()))
					{
						CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_TUTORIAL_DOANE_0);
						gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
					}
				}
			}
		}
	}
}

// false if unit is killed
bool CvUnit::jumpToNearestValidPlot()
{
	FAssertMsg(!isAttacking(), "isAttacking did not return false as expected");
	FAssertMsg(!isFighting(), "isFighting did not return false as expected");

	CvCity* pNearestCity = GC.getMapINLINE().findCity(getX_INLINE(), getY_INLINE(), getOwnerINLINE());
	int iBestValue = MAX_INT;
	CvPlot* pBestPlot = NULL;

	for (int iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); iI++)
	{
		CvPlot* pLoopPlot = GC.getMapINLINE().plotByIndexINLINE(iI);

		if (isValidPlot(pLoopPlot))
		{
			if (canMoveInto(pLoopPlot))
			{
				FAssertMsg(!atPlot(pLoopPlot), "atPlot(pLoopPlot) did not return false as expected");

				if (pLoopPlot->isRevealed(getTeam(), false))
				{
					int iValue = (plotDistance(getX_INLINE(), getY_INLINE(), pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE()) * 2);

					if (pNearestCity != NULL)
					{
						iValue += plotDistance(pLoopPlot->getX_INLINE(), pLoopPlot->getY_INLINE(), pNearestCity->getX_INLINE(), pNearestCity->getY_INLINE());
					}

					if (pLoopPlot->area() != area())
					{
						iValue *= 3;
					}

					if (iValue < iBestValue)
					{
						iBestValue = iValue;
						pBestPlot = pLoopPlot;
					}
				}
			}
		}
	}

	bool bValid = true;
	if (pBestPlot != NULL)
	{
		setXY(pBestPlot->getX_INLINE(), pBestPlot->getY_INLINE());
	}
	else
	{
		kill(false);
		bValid = false;
	}

	return bValid;
}

bool CvUnit::isValidPlot(const CvPlot* pPlot) const
{
	if (!pPlot->isValidDomainForLocation(*this))
	{
		return false;
	}

	if (!canEnterArea(pPlot->getOwnerINLINE(), pPlot->area()))
	{
		return false;
	}

	TeamTypes ePlotTeam = pPlot->getTeam();
	if (ePlotTeam != NO_TEAM)
	{
		if (pPlot->isCity(true, ePlotTeam) && !canCoexistWithEnemyUnit(ePlotTeam) && isEnemy(ePlotTeam))
		{
			return false;
		}
	}

	return true;
}


bool CvUnit::canAutomate(AutomateTypes eAutomate) const
{
	if (eAutomate == NO_AUTOMATE)
	{
		return false;
	}
	if (GET_PLAYER(getOwnerINLINE()).isTerritorialInfluenceMode())
	{
		return false;
	}

	switch (eAutomate)
	{
	case AUTOMATE_BUILD:
		if (workRate(true) <= 0 || isHuman())
		{
			return false;
		}
		break;

	case AUTOMATE_CITY:
		if (workRate(true) <= 0)
		{
			return false;
		}
		if (!plot()->isCityRadius())
		{
			return false;
		}
		if ((plot()->getWorkingCity() == NULL) || plot()->getWorkingCity()->getOwnerINLINE() != getOwnerINLINE())
		{
			return false;
		}
		break;

	case AUTOMATE_EXPLORE:
		if (!canExplore())
		{
			return false;
		}
		break;

	case AUTOMATE_SAIL:
		if (!canAutoCrossOcean(plot()) || isAutomated())
		{
			return false;
		}
		break;

	case AUTOMATE_TRANSPORT_ROUTES:
		if (cargoSpace() == 0)
		{
			return false;
		}
		break;

	default:
		FAssert(false);
		break;
	}

	return true;
}


void CvUnit::automate(AutomateTypes eAutomate)
{
	if (canAutomate(eAutomate))
	{
		getGroup()->setAutomateType(eAutomate);
	}
}


bool CvUnit::canScrap() const
{
	if (plot()->isFighting())
	{
		return false;
	}

	return true;
}


void CvUnit::scrap()
{
	if (!canScrap())
	{
		return;
	}

	kill(true);
}


bool CvUnit::canGift(bool bTestVisible, bool bTestTransport)
{
	CvPlot* pPlot = plot();
	CvUnit* pTransport = getTransportUnit();
	CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());
	PlayerTypes ePlotOwner = pPlot->getOwnerINLINE();

	if (!(pPlot->isOwned()))
	{
		return false;
	}

	if (ePlotOwner == getOwnerINLINE())
	{
		return false;
	}
	CvPlayerAI& kPlotOwner = GET_PLAYER(ePlotOwner);

	if (!kPlotOwner.isProfessionValid(getProfession(), getUnitType()))
	{
		return false;
	}

	if (kPlotOwner.isNative())
	{
		return false;
	}

	if (pPlot->isVisibleEnemyUnit(this))
	{
		return false;
	}

	if (pPlot->isVisibleEnemyUnit(ePlotOwner))
	{
		return false;
	}

	if (!pPlot->isValidDomainForLocation(*this) && NULL == pTransport)
	{
		return false;
	}

	if (hasCargo())
	{
		CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
		while (pUnitNode != NULL)
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);

			if (pLoopUnit->getTransportUnit() == this)
			{
				if (!pLoopUnit->canGift(false, false))
				{
					return false;
				}
			}
		}
	}

	if (bTestTransport)
	{
		if (pTransport != NULL && pTransport->getTeam() != pPlot->getTeam())
		{
			return false;
		}
	}

	if (!bTestVisible)
	{
		if (!kPlotOwner.AI_acceptUnit(this))
		{
			return false;
		}
	}

	if (atWar(pPlot->getTeam(), getTeam()))
	{
		return false;
	}

	// to shut down free units from king exploit
	if (kOwner.getNumCities() == 0)
	{
		return false;
	}

	// to shut down free ship from king exploit
	if (kOwner.getParent() != NO_PLAYER)
	{
		CvPlayer& kEurope = GET_PLAYER(kOwner.getParent());
		if (kEurope.isAlive() && kEurope.isEurope() && !::atWar(getTeam(), kEurope.getTeam()) && getDomainType() == DOMAIN_SEA)
		{
			bool bHasOtherShip = false;
			int iLoop;
			for (CvUnit* pLoopUnit = kOwner.firstUnit(&iLoop); pLoopUnit != NULL && !bHasOtherShip; pLoopUnit = kOwner.nextUnit(&iLoop))
			{
				if (pLoopUnit != this && pLoopUnit->getDomainType() == DOMAIN_SEA)
				{
					bHasOtherShip = true;
				}
			}

			if (!bHasOtherShip )
			{
				return false;
			}
		}
	}

	return true;
}


void CvUnit::gift(bool bTestTransport)
{
	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pGiftUnit;
	CvUnit* pLoopUnit;
	CvPlot* pPlot;
	CvWString szBuffer;
	PlayerTypes eOwner;

	if (!canGift(false, bTestTransport))
	{
		return;
	}

	pPlot = plot();

	pUnitNode = pPlot->headUnitNode();

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);

		if (pLoopUnit->getTransportUnit() == this)
		{
			pLoopUnit->gift(false);
		}
	}

	FAssertMsg(plot()->getOwnerINLINE() != NO_PLAYER, "plot()->getOwnerINLINE() is not expected to be equal with NO_PLAYER");
	pGiftUnit = GET_PLAYER(plot()->getOwnerINLINE()).initUnit(getUnitType(), getProfession(), getX_INLINE(), getY_INLINE(), AI_getUnitAIType(), getFacingDirection(false), getYieldStored());

	FAssertMsg(pGiftUnit != NULL, "GiftUnit is not assigned a valid value");

	eOwner = getOwnerINLINE();

	pGiftUnit->convert(this, true);

	int iUnitValue = 0;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		iUnitValue += pGiftUnit->getUnitInfo().getYieldCost(iYield);
	}
	GET_PLAYER(pGiftUnit->getOwnerINLINE()).AI_changePeacetimeGrantValue(eOwner, iUnitValue / 5);

	szBuffer = gDLL->getText("TXT_KEY_MISC_GIFTED_UNIT_TO_YOU", GET_PLAYER(eOwner).getNameKey(), pGiftUnit->getNameKey());
	gDLL->getInterfaceIFace()->addMessage(pGiftUnit->getOwnerINLINE(), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_UNITGIFTED", MESSAGE_TYPE_INFO, pGiftUnit->getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_WHITE"), pGiftUnit->getX_INLINE(), pGiftUnit->getY_INLINE(), true, true);

	// Python Event
	gDLL->getEventReporterIFace()->unitGifted(pGiftUnit, getOwnerINLINE(), plot());
}

bool CvUnit::canPickUnit() const
{
	return getSpaceNewCargo() >= 30;
}

bool CvUnit::canLoadUnit(const CvUnit* pTransport, const CvPlot* pPlot, bool bCheckCity) const
{
	FAssert(pTransport != NULL);
	FAssert(pPlot != NULL);

	if (getUnitTravelState() != pTransport->getUnitTravelState())
	{
		return false;
	}

	if (pTransport == this)
	{
		return false;
	}

	if (getTransportUnit() == pTransport)
	{
		return false;
	}

	if (pTransport->getTeam() != getTeam())
	{
		return false;
	}

	if (getCargo() > 0)
	{
		return false;
	}

	if (pTransport->isCargo())
	{
		return false;
	}
	if (getCrewFormationTurn() > 0)
	{
		return false;
	}

	if (!(pTransport->cargoSpaceAvailable(getSpecialUnitType(), getDomainType())))
	{
		return false;
	}

	if (pTransport->cargoSpace() < getUnitInfo().getRequiredTransportSize())
	{
		return false;
	}

	if (!(pTransport->atPlot(pPlot)))
	{
		return false;
	}
	//startm07
	if (getTransportUnit() != NULL)
	{
		if (!getTransportUnit()->getUnitInfo().isLandCargoHuman())
		{
			if (bCheckCity && !pPlot->isCity(true))
			{
				return false;
			}
		}
	}
	if (pTransport->getUnitType() == UNIT_WAGON_TRAIN_ARMY)
	{
		ProfessionTypes eProfession = getProfession();
		if (eProfession != NO_PROFESSION)
		{
			CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);
			if (kProfession.getYieldEquipmentAmount(YIELD_MUSKETS) > 0)
			{
				return false;
			}
			if (kProfession.getYieldEquipmentAmount(YIELD_HORSES) > 0)
			{
				return false;
			}
			if (kProfession.getYieldEquipmentAmount(YIELD_SWORDS) > 0)
			{
				return false;
			}
			if (kProfession.getYieldEquipmentAmount(YIELD_CANNON) > 0)
			{
				return false;
			}
		}
	}
	if (getUnitType() == UNIT_BUILDER_WAGON && !pTransport->getUnitInfo().isMechUnit())
	{
		return false;
	}
	if (pTransport->getSpaceNewCargo() < 30)
	{
		return false;
	}

	if (pTransport->getUnitInfo().isMechUnit() && pTransport->getUnitInfo().getCargoSpace() > 0 &&  !pTransport->isOnlyDefensive())
	{
		if (!GET_PLAYER(getOwnerINLINE()).isEurope())
		{
			return false;
		}
	}

	return true;
}

void CvUnit::loadUnit(CvUnit* pTransport)
{
	if (!canLoadUnit(pTransport, plot(), true))
	{
		return;
	}

	setTransportUnit(pTransport);
}

bool CvUnit::shouldLoadOnMove(const CvPlot* pPlot) const
{
	if (isCargo())
	{
		return false;
	}

	if (getYield() != NO_YIELD)
	{
		CvCity* pCity = pPlot->getPlotCity();
		if (pCity != NULL && GET_PLAYER(getOwnerINLINE()).canUnloadYield(pCity->getOwnerINLINE()))
		{
			return false;
		}
	}

	if (getUnitTravelState() != NO_UNIT_TRAVEL_STATE)
	{
		return false;
	}

	if (!pPlot->isValidDomainForLocation(*this))
	{
		return true;
	}

	if (m_pUnitInfo->getTerrainImpassable(pPlot->getTerrainType()))
	{
		return true;
	}

	return false;
}

int CvUnit::getLoadedYieldAmount(YieldTypes eYield) const
{
	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return 0;
	}

	int iTotal = getNewCargoYield(eYield);

	return iTotal;
}

int CvUnit::getLoadYieldAmount(YieldTypes eYield) const
{
	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return 0;
	}
	bool bFull = isFullNewCargo();
	if (!bFull)
	{
		return getSpaceNewCargo();
	}

	return 0;
}

bool CvUnit::canLoadYields(const CvPlot* pPlot, bool bTrade) const
{
	for (int iYield = 0; iYield <  NUM_YIELD_TYPES; ++iYield)
	{
		if (canLoadYield(pPlot, (YieldTypes) iYield, bTrade))
		{
			return true;
		}
	}

	return false;
}

bool CvUnit::canLoadYield(const CvPlot* pPlot, YieldTypes eYield, bool bTrade) const
{
	if (eYield == NO_YIELD)
	{
		FAssert(!bTrade);
		return canLoadYields(pPlot, bTrade);
	}

	if (m_pUnitInfo->isMechUnit() && m_pUnitInfo->getCargoSpace() > 0 &&  !isOnlyDefensive())
	{
		return false;
	}

	CvYieldInfo& kYield = GC.getYieldInfo(eYield);
	if (kYield.isCargo() && !isCargo())
	{
		if (pPlot != NULL)
		{
			CvCity* pCity = pPlot->getPlotCity();
			if (NULL != pCity)
			{
				if (GET_PLAYER(getOwnerINLINE()).canLoadYield(pCity->getOwnerINLINE()) || bTrade)
				{
					if (kYield.isCargo())
					{
						if (pCity->getYieldStored(eYield) > 0)
						{
							if (getLoadYieldAmount(eYield) > 0)
							{
								return true;
							}
						}
					}
				}
			}
		}
	}

	return false;
}

void CvUnit::loadYield(YieldTypes eYield, bool bTrade, int iValue)
{
	if (!canLoadYield(plot(), eYield, bTrade))
	{
		return;
	}
	loadYieldAmount(eYield, getMaxLoadYieldAmount(eYield, iValue), bTrade);
}

void CvUnit::loadYieldAmount(YieldTypes eYield, int iAmount, bool bTrade)
{
	if (!canLoadYield(plot(), eYield, bTrade))
	{
		return;
	}

	if (iAmount <= 0 || iAmount > getMaxLoadYieldAmount(eYield, iAmount, -1))
	{
		return;
	}
	if (plot() != NULL)
	{
		CvCity* pCity = plot()->getPlotCity();
		if (pCity != NULL)
		{
			pCity->changeYieldStored(eYield, -iAmount);
		}
	}
	changeNewCargoYield(eYield, iAmount);

	reloadEntity();
}

int CvUnit::getMaxLoadYieldAmount(YieldTypes eYield, int iValue, int iMaxAmount) const
{
	int iSpace = getSpaceNewCargo();

	if (iMaxAmount > 0)
	{
		iSpace = std::min(iSpace, getSpaceNewCargo());
	}

	if (plot() != NULL)
	{
		CvCity* pCity = plot()->getPlotCity();
		if (pCity != NULL)
		{
			iSpace = std::min(pCity->getYieldStored(eYield), iSpace);
		}
	}

	if (iValue >= 0)
	{
		iSpace = std::min(iValue, iSpace);
	}

	return iSpace;
}
bool CvUnit::canTradeYield(const CvPlot* pPlot) const
{
	FAssert(pPlot != NULL);

	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL)
	{
		return false;
	}

	if (pCity->getOwnerINLINE() == getOwnerINLINE())
	{
		return false;
	}
	if (getOnlyNewCargo() == 0 && getSpaceNewCargo() == 0)
	{
		return false;
	}

	//check if we have any yield cargo
	bool bYieldFound = false;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (getNewCargoYield(eYield) > 0)
		{
			bYieldFound = true;
		}
	}

	//check if the city has any cargo that we can fit
	if (!bYieldFound)
	{
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
		{
			YieldTypes eYield = (YieldTypes) iYield;
			if ((pCity->getYieldStored(eYield) > 0) && (getLoadYieldAmount(eYield) > 0))
			{
				bYieldFound = true;
				break;
			}
		}
	}

	if (!bYieldFound)
	{
		return false;
	}

	return true;
}

void CvUnit::tradeYield(int iMode)
{
	if (!canTradeYield(plot()))
	{
		return;
	}

	PlayerTypes eOtherPlayer = plot()->getOwnerINLINE();

	//both human
	if (GET_PLAYER(getOwnerINLINE()).isHuman() && GET_PLAYER(eOtherPlayer).isHuman())
	{
		if (GC.getGameINLINE().isPbem() || GC.getGameINLINE().isHotSeat() || (GC.getGameINLINE().isPitboss() && !gDLL->isConnected(GET_PLAYER(eOtherPlayer).getNetID())))
		{
			if (gDLL->isMPDiplomacy())
			{
				gDLL->beginMPDiplomacy(eOtherPlayer, false, false, getIDInfo());
			}
		}
		else if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			//clicking the flashing goes through CvPlayer::contact where it sends the response message
			gDLL->sendContactCiv(NETCONTACT_INITIAL, eOtherPlayer, getID());
		}
	}
	else if (GET_PLAYER(getOwnerINLINE()).isHuman()) //we're human contacting them
	{
		if (!GET_PLAYER(eOtherPlayer).isNative())
		{
			CvDiploParameters* pDiplo = new CvDiploParameters(eOtherPlayer);
			pDiplo->setDiploComment((DiploCommentTypes) GC.getInfoTypeForString("AI_DIPLOCOMMENT_TRADING"));
			pDiplo->setTransport(getIDInfo());
			pDiplo->setCity(plot()->getPlotCity()->getIDInfo());
			gDLL->beginDiplomacy(pDiplo, getOwnerINLINE());
		}
		else if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			GC.getGameINLINE().doControl(CONTROL_NATIVE_YIELD_TRADE, iMode, getID());
		}
	}
	else if (GET_PLAYER(eOtherPlayer).isHuman()) //they're human contacting us
	{
		if (!GET_PLAYER(getOwnerINLINE()).isNative())
		{
			CvDiploParameters* pDiplo = new CvDiploParameters(getOwnerINLINE());
			pDiplo->setDiploComment((DiploCommentTypes) GC.getInfoTypeForString("AI_DIPLOCOMMENT_TRADING"));
			pDiplo->setTransport(getIDInfo());
			gDLL->beginDiplomacy(pDiplo, getOwnerINLINE());
		}
	}
	else //both AI
	{
		FAssertMsg(false, "Don't go through here. Implement deals directly.");
	}
}

bool CvUnit::canClearSpecialty() const
{
	if (m_pUnitInfo->getTeacherWeight() <= 0)
	{
		return false;
	}

	UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(GC.getDefineINT("DEFAULT_POPULATION_UNIT"));
	if (eUnit == NO_UNIT)
	{
		return false;
	}

	return true;
}

CvUnit* CvUnit::clearSpecialty()
{
	if (!canClearSpecialty())
	{
		return NULL;
	}

	bool bLocked = isColonistLocked();
	UnitTypes eUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(GC.getDefineINT("DEFAULT_POPULATION_UNIT"));
	CvUnit* pNewUnit = GET_PLAYER(getOwnerINLINE()).initUnit(eUnit, NO_PROFESSION, getX_INLINE(), getY_INLINE(), AI_getUnitAIType());
	FAssert(pNewUnit != NULL);

	CvCity* pCity = GET_PLAYER(getOwnerINLINE()).getPopulationUnitCity(getID());
	if (pCity != NULL)
	{
		pNewUnit->convert(this, false);
		pCity->replaceCitizen(pNewUnit->getID(), getID(), false);
		pNewUnit->setColonistLocked(bLocked);
		pCity = GET_PLAYER(getOwnerINLINE()).getPopulationUnitCity(getID());
		if (pCity != NULL)
		{
			pCity->removePopulationUnit(this, true, NO_PROFESSION);
		} 
		else
		{
			kill(false);
		}
	}
	else
	{
		pNewUnit->convert(this, true);
	}

	//Scoot should loose experiences and promotions
	pNewUnit->setExperienceExploAndNavigation(0);
	pNewUnit->setLevelExploAndNavigation(0);

	for (int iPromotionIndex = 0; iPromotionIndex < GC.getNumPromotionInfos(); iPromotionIndex++)
	{
		PromotionTypes ePromotion = (PromotionTypes)iPromotionIndex;
		CvPromotionInfo& kPromotion = GC.getPromotionInfo(ePromotion);
		if (!kPromotion.isGraphicalOnly() && kPromotion.isExploAndNavigation())
		{
			pNewUnit->setHasRealPromotion(ePromotion, false);
		}
	}

	return pNewUnit;
}

bool CvUnit::canAutoCrossOcean(const CvPlot* pPlot) const
{
	if (canCrossOcean(pPlot, UNIT_TRAVEL_STATE_TO_EUROPE))
	{
		return false;
	}

	if (!GET_PLAYER(getOwnerINLINE()).canTradeWithEurope())
	{
		return false;
	}

	if (getDomainType() != DOMAIN_SEA)
	{
		return false;
	}
	if (!pPlot->isEuropeAccessable())
	{
		return false;
	}

	switch (getUnitTravelState())
	{
	case NO_UNIT_TRAVEL_STATE:
		if (isHuman())
		{
			CvSelectionGroup* pGroup = getGroup();
			if (pGroup != NULL && pGroup->IsSelected())
			{
				int iNumSelectedUnits = gDLL->getInterfaceIFace()->getLengthSelectionList();
				int iNumGroupUnits = pGroup->getNumUnits();
				if (iNumSelectedUnits != iNumGroupUnits)
				{
					return false;
				}
			}
		}
		break;
	default:
		break;
	}


	return true;
}

bool CvUnit::canCrossOcean(const CvPlot* pPlot, UnitTravelStates eNewState) const
{
	FAssert(pPlot != NULL);
	if (!pPlot->isEurope())
	{
		return false;
	}

	if (getTransportUnit() != NULL)
	{
		return false;
	}

	if (!hasCrew())
	{
		return false;
	}

	if (getShipSellPrice() != 0)
	{
		return false;
	}

	if (isInEuropeDrydock())
	{
		return false;
	}

	switch (getUnitTravelState())
	{
	case NO_UNIT_TRAVEL_STATE:
		if (eNewState != UNIT_TRAVEL_STATE_TO_EUROPE)
		{
			return false;
		}
		if (!GET_PLAYER(getOwnerINLINE()).canTradeWithEurope())
		{
			return false;
		}

		if (isHuman())
		{
			CvSelectionGroup* pGroup = getGroup();
			if (pGroup != NULL && pGroup->IsSelected() && pGroup->isAllOnMap())
			{
				int iNumSelectedUnits = gDLL->getInterfaceIFace()->getLengthSelectionList();
				int iNumGroupUnits = pGroup->getNumUnits();
				if (iNumSelectedUnits != iNumGroupUnits)
				{
					return false;
				}
			}
		}
		break;
	case UNIT_TRAVEL_STATE_IN_EUROPE:
		if (eNewState != UNIT_TRAVEL_STATE_FROM_EUROPE)
		{
			return false;
		}
		if (isInEuropeDrydock())
		{
			return false;
		}
		if (mustStayInEurope())
		{
			return false;
		}
		break;
	default:
		FAssertMsg(false, "Invalid trip");
		return false;
		break;
	}

	return true;
}

void CvUnit::crossOcean(UnitTravelStates eNewState, bool bTestGroup)
{
	if (!canCrossOcean(plot(), eNewState))
	{
		return;
	}
	getGroup()->clearMissionQueue();

	int iTravelTime = getEuropeTravelTime();

	//DOANE start: 2*7 XP pts to the unit if it hasn't navigation promotion, otherwise give 2*3 XP pts
	//TODO add global variable for XP points given
	if (eNewState == UNIT_TRAVEL_STATE_TO_EUROPE)
	{// We can come back to europe that's why we give points only if we go to europe
		int iExp = getLevelExploAndNavigation() == 0 ? 7 : 3; //0 level means no navigation promotion...
		int moveDenominator = GC.getMOVE_DENOMINATOR();
		iExp += moveDenominator > 0 ? movesLeft() / moveDenominator : 0;
		changeExperienceExploAndNavigation(iExp);
	}

	//DOANE end

	setUnitTravelState(eNewState, false);
	if (iTravelTime > 0)
	{
		setUnitTravelTimer(iTravelTime);
	}
	else
	{
		setUnitTravelTimer(1);
		doUnitTravelTimer();
	}

	if (eNewState == UNIT_TRAVEL_STATE_FROM_EUROPE && bTestGroup)
	{// We test if we are from europe and bTestGroup is for avoid infinity loop
		CvSelectionGroup* pGroup = getGroup();
		if (pGroup != NULL)
		{
			CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
			while (pUnitNode != NULL)
			{
				CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pGroup->nextUnitNode(pUnitNode);
				if (pLoopUnit != this)
				{
					pLoopUnit->crossOcean(eNewState, false); 
				}
			}
		} 
	}
}

bool CvUnit::canExplore() const
{
	if (getProfession() != PROFESSION_SCOUT && getDomainType() != DOMAIN_SEA) 
	{
		return false;
	}

	if (getDomainType() == DOMAIN_IMMOBILE)
	{
		return false;
	}

	if (isAutomated()) 
	{
		return false;
	}

	return true;
}

bool CvUnit::canLoad(const CvPlot* pPlot, bool bCheckCity) const
{
	PROFILE_FUNC();

	FAssert(pPlot != NULL);
	if (isAutomated())
	{
		return false;
	}

	CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);

		if (canLoadUnit(pLoopUnit, pPlot, bCheckCity))
		{
			return true;
		}
	}

	return false;
}


bool CvUnit::load(bool bCheckCity)
{
	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pLoopUnit;
	CvPlot* pPlot;
	int iPass;

	if (!canLoad(plot(), bCheckCity))
	{
		return true;
	}

	pPlot = plot();

	for (iPass = 0; iPass < 2; iPass++)
	{
		pUnitNode = pPlot->headUnitNode();

		while (pUnitNode != NULL)
		{
			pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);

			if (canLoadUnit(pLoopUnit, pPlot, bCheckCity))
			{
				if ((iPass == 0) ? (pLoopUnit->getOwnerINLINE() == getOwnerINLINE()) : (pLoopUnit->getTeam() == getTeam()))
				{
					if (!setTransportUnit(pLoopUnit))
					{
						return false;
					}
					break;
				}
			}
		}

		if (isCargo())
		{
			break;
		}
	}

	return true;
}


bool CvUnit::canUnload() const
{
	if (getTransportUnit() == NULL)
	{
		return false;
	}

	if (!plot()->isValidDomainForLocation(*this))
	{
		return false;
	}

	return true;
}


void CvUnit::unload()
{
	if (!canUnload())
	{
		return;
	}

	setTransportUnit(NULL);
}

bool CvUnit::canUnloadAll() const
{
	if (getCargo() == 0 && getNewCargo() == 0)
	{
		return false;
	}

	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return false;
	}

	CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();

	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);

		if (pLoopUnit->getTransportUnit() == this)
		{
			if (pLoopUnit->canUnload())
			{
				return true;
			}
		}
	}

	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL || !GET_PLAYER(getOwnerINLINE()).canUnloadYield(pCity->getOwnerINLINE()))
	{
		return false;
	}
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (getNewCargoYield(eYield) > 0)
		{
			return true;
		}
	}

	return false;
}


void CvUnit::unloadAll(bool unloadYields, bool bUnloadUnits)
{
	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pLoopUnit;
	CvPlot* pPlot;

	if (!canUnloadAll())
	{
		return;
	}

	pPlot = plot();
	CvCity* pCity = plot()->getPlotCity();
	if (unloadYields && pCity != NULL && GET_PLAYER(getOwnerINLINE()).canUnloadYield(pCity->getOwnerINLINE())) 
	{
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield) 
		{
			YieldTypes eYield = (YieldTypes) iYield;
			int iNewCargoYield = getNewCargoYield(eYield);
			if (iNewCargoYield > 0) 
			{
				pCity->changeYieldStored(eYield, iNewCargoYield);
				changeNewCargoYield(eYield, -iNewCargoYield);
			}
		}
	}

	reloadEntity();

	pUnitNode = pPlot->headUnitNode();

	if (bUnloadUnits)
	{
		while (pUnitNode != NULL)
		{
			pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);

			if (pLoopUnit->getTransportUnit() == this) 
			{
				if (pLoopUnit->canUnload()) 
				{
					pLoopUnit->setTransportUnit(NULL);
					if (pCity != NULL && pCity->getID() == pLoopUnit->getDestinationCity())
					{
						pLoopUnit->setDestinationCity(-1);
					}
				}
				else
				{
					FAssert(isHuman());
					pLoopUnit->getGroup()->setActivityType(ACTIVITY_AWAKE);
				}
			}
		}
	}
}

void CvUnit::unloadNewCargoInCity(YieldTypes eYield, int iValue)
{
	CvPlot* pPlot = plot();
	if (pPlot != NULL)
	{
		CvCity* pCity = pPlot->getPlotCity();
		if (pCity != NULL) 
		{
			int iDiff = getNewCargoYield(eYield)-iValue;
			setNewCargoYield(eYield, iValue);
			pCity->changeYieldStored(eYield, iDiff);
			if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer()) 
			{
				gDLL->getInterfaceIFace()->setDirty(CityScreen_DIRTY_BIT, true);
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_UNLOAD_NEW_CARGO, getID());
				gDLL->getInterfaceIFace()->addPopup(pInfo, getOwner(), true, true);
				reloadEntity();
			}
		}
	}
}

bool CvUnit::canResupply() const
{
	CvPlot* pPlot = plot();
	if (pPlot == NULL) 
	{
		return false;
	}

	CvSelectionGroup* pGroup = getGroup();
	if (pGroup == NULL) 
	{
		return false;
	}

	CvCity* pCity = pPlot->getPlotCity();

	if (pCity != NULL && (pCity->getOwner() == getOwner() || (!pCity->isNative() && GET_TEAM(pCity->getTeam()).isOpenBorders(getTeam()))))
	{
		if ( pCity->getYieldStored(YIELD_AMMUNITION) == 0 ) 
		{
			return false;
		}
	} 
	else 
	{
		bool bMunition = false;
		CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();

		for (int i = 0; i < pPlot->getNumUnits(); i++) 
		{
			CvUnit* pLoopUnit = pPlot->getUnitByIndex(i);
			if (pLoopUnit != NULL && (pLoopUnit->getOwner() == getOwner() || (pLoopUnit->getTeam() != NO_TEAM && GET_TEAM(pLoopUnit->getTeam()).isOpenBorders(getTeam())))) 
			{
				if (pLoopUnit->getNewCargoYield(YIELD_AMMUNITION) > 0 && pLoopUnit->isOnMap()) 
				{
					bMunition = true;
				}
			}
		}

		if (!bMunition) 
		{
			return false;
		}
	}

	bool bIsHuman = isHuman();

	CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
	while (pUnitNode != NULL) 
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pGroup->nextUnitNode(pUnitNode);
		if (pLoopUnit != NULL) 
		{
			if (pLoopUnit->hasToBeResupplied()) 
			{
				if (pLoopUnit->IsSelected() || pLoopUnit->getOwnerINLINE() != GC.getGameINLINE().getActivePlayer() || !bIsHuman) 
				{
					return true;
				}
			}
		}
	}

	return false;
}

void CvUnit::resupplyUnits(bool isAsync)
{
	if (!canResupply()) 
	{
		return;
	}

	CvPlot* pPlot = plot();
	CvCity* pCity = pPlot->getPlotCity();
	CvSelectionGroup* pGroup = getGroup();

	int iTotalMunition = 0;
	int iMaxMunition = -1;
	int iMaxTotalMunition = 0;
	bool bIsHuman = isHuman();

	CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
	while (pUnitNode != NULL) 
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pGroup->nextUnitNode(pUnitNode);
		if (pLoopUnit != NULL && (pLoopUnit->IsSelected() || !bIsHuman))
		{
			iMaxMunition = pLoopUnit->getMaxMunition();
			if (iMaxMunition != -1)
			{
				iMaxTotalMunition += iMaxMunition;
				iTotalMunition += pLoopUnit->getMunition();
			}
		}
	}

	if (iMaxTotalMunition <= 0)
	{
		return;
	}
	int iPercent = 100;
	if (pCity != NULL && (pCity->getOwner() == getOwner() || (!pCity->isNative() && GET_TEAM(pCity->getTeam()).isOpenBorders(getTeam()))))
	{
		int iMunitionCity = pCity->getYieldStored(YIELD_AMMUNITION);
		if (iTotalMunition + iMunitionCity <= iMaxTotalMunition)
		{
			iPercent = (iTotalMunition + iMunitionCity)*100/iMaxTotalMunition;
		}
		int iChange = pGroup->resupplyUnits(YIELD_AMMUNITION, iPercent);
		if (isAsync) 
		{
			if (getOwner() == GC.getGameINLINE().getActivePlayer()) 
			{
				gDLL->sendDoTask(pCity->getID(), TASK_CHANGE_YIELD_STORED, YIELD_AMMUNITION, iChange, false, false, false, false);
			}
		}
		else 
		{
			pCity->changeYieldStored(YIELD_AMMUNITION, iChange);
		}
	} 
	else 
	{
		int iMunitionWagon = 0;
		CvUnit* pLastUnit = NULL;
		for (int i = 0; i < pPlot->getNumUnits(); i++) 
		{
			CvUnit* pLoopUnit = pPlot->getUnitByIndex(i);
			if (pLoopUnit != NULL && (pLoopUnit->getOwner() == getOwner() || (pLoopUnit->getTeam() != NO_TEAM && GET_TEAM(pLoopUnit->getTeam()).isOpenBorders(getTeam())))) 
			{
				int iMunitionUnit = pLoopUnit->getNewCargoYield(YIELD_AMMUNITION);
				if (iMunitionUnit > 0) 
				{
					iMunitionWagon += iMunitionUnit;//30
					iPercent = 100;
					if (iTotalMunition + iMunitionWagon < iMaxTotalMunition)
					{
						iPercent = (iTotalMunition + iMunitionWagon)*100/iMaxTotalMunition;
					}
					int iYieldChanges = pGroup->resupplyUnits(YIELD_AMMUNITION, iPercent);
					iMunitionWagon += iYieldChanges;//iYieldChanges has neg value

					if (isAsync) 
					{	
						if (getOwner() == GC.getGameINLINE().getActivePlayer()) 
						{
							gDLL->sendDoCommand(pLoopUnit->getID(), COMMAND_SET_NEW_CARGO_YIELD, YIELD_AMMUNITION, 0, false);
						}
					}
					else 
					{
						pLoopUnit->setNewCargoYield(YIELD_AMMUNITION, 0);
					}
					pLastUnit = pLoopUnit;
					iTotalMunition -= iYieldChanges;
					if (iPercent == 100) 
					{
						break;
					}
				}
			}
		}
		if (pLastUnit != NULL && iMunitionWagon > 0)
		{
			if (isAsync) 
			{
				if (getOwner() == GC.getGameINLINE().getActivePlayer()) 
				{
					gDLL->sendDoCommand(pLastUnit->getID(), COMMAND_SET_NEW_CARGO_YIELD, YIELD_AMMUNITION, iMunitionWagon, false);
				}
			}
			else 
			{
				pLastUnit->setNewCargoYield(YIELD_AMMUNITION, iMunitionWagon);
			}
		}
	}
	reloadEntity();
}
bool CvUnit::canResupplyShips() const
{
	bool bInEurope = (getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);
	CvPlot* pPlot = plot();

	if (pPlot == NULL && !bInEurope) 
	{
		return false;
	}

	if (!bInEurope) 
	{
		int iShip = 0;
		CvSelectionGroup* pGroup = getGroup();
		if (pGroup == NULL) 
		{
			return false;
		}

		CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
		while (pUnitNode != NULL) 
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pGroup->nextUnitNode(pUnitNode);
			if (pLoopUnit != NULL) 
			{
				if (!pLoopUnit->getUnitInfo().isMechUnit()) 
				{
					return false;
				}
				if (pLoopUnit->getMaxCannon() > 0) 
				{
					iShip++;
				}
			}
		}

		if (iShip == 0) 
		{
			return false;
		}

		CvCity* pCity = pPlot->getPlotCity();
		if (pCity == NULL) 
		{
			return false;
		}
	} 
	else if (getMaxCannon() <= 0) 
	{
		return false;
	}

	return true;
}


void CvUnit::resupplyShips()
{
	bool bIsInEurope = (getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);

	if (!canResupplyShips())
	{
		return;
	}
	if (getOwner() != GC.getGameINLINE().getActivePlayer())
	{
		return;
	}
	if (!bIsInEurope)
	{
		CvPlot* pPlot = plot();
		CvCity* pCity = pPlot->getPlotCity();

		if (pCity != NULL)
		{
			CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_RESUPPLY_SHIPS, getID(), pCity->getID());
			gDLL->getInterfaceIFace()->addPopup(pInfo, getOwner(), true, true);
		}
	}
	else
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_RESUPPLY_SHIPS, getID(), -1);
		gDLL->getInterfaceIFace()->addPopup(pInfo, getOwner(), true, true);
	}
}

int CvUnit::getMissingEquipementPrice() const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvPlayer& kEurope = GET_PLAYER(kPlayer.getParent());
	int iTotal = 0, iQuantity;
	iQuantity = getMaxCannon() - getNbCannon();
	iQuantity = std::max(0, iQuantity - kPlayer.getEuropeWarehouseYield(YIELD_CANNON));
	if (iQuantity > 0 && iQuantity > kEurope.getEuropeWarehouseYield(YIELD_CANNON))
	{
		return -1;
	}

	iTotal += kPlayer.getBuyPriceForYield(YIELD_CANNON, iQuantity);
	iQuantity = getMaxMunition() - getMunition();

	iQuantity = std::max(0, iQuantity - kPlayer.getEuropeWarehouseYield(YIELD_AMMUNITION));
	if (iQuantity > 0 && iQuantity > kEurope.getEuropeWarehouseYield(YIELD_AMMUNITION)) 
	{
		return -1;
	}
	iTotal +=  kPlayer.getBuyPriceForYield(YIELD_AMMUNITION, iQuantity);

	return iTotal;
}

void CvUnit::resupplyGroupShips(int iCannonPercent, int iAmmunitionPercent, bool bPurchase, bool bDisplayPopup)
{
	bool bInEurope = (getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);
	CvPlot* pPlot = plot();
	CvCity* pCity = NULL;
	CvSelectionGroup* pGroup = getGroup();
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvPlayer& kEurope = GET_PLAYER(kPlayer.getParent());

	if (bInEurope) 
	{
		int iChangesCannon = iCannonPercent*getMaxCannon()/100 - getNbCannon();
		int iChangesAmmunition = iAmmunitionPercent*getMaxMunition()/100 - getMunition();
		if (bPurchase)
		{
			int iWarehouseCannon = std::min(kPlayer.getEuropeWarehouseYield(YIELD_CANNON), iChangesCannon);
			int iWarehouseMunition = std::min(kPlayer.getEuropeWarehouseYield(YIELD_AMMUNITION), iChangesAmmunition);
			kEurope.changeEuropeWarehouseYield(YIELD_CANNON, -(iChangesCannon - iWarehouseCannon));
			kEurope.changeEuropeWarehouseYield(YIELD_AMMUNITION, -(iChangesAmmunition-iWarehouseMunition));

			kPlayer.changeEuropeWarehouseYield(YIELD_CANNON, -iWarehouseCannon);
			kPlayer.changeEuropeWarehouseYield(YIELD_AMMUNITION, -iWarehouseMunition);
		} 
		else 
		{
			kPlayer.changeEuropeWarehouseYield(YIELD_CANNON, -iChangesCannon);
			kPlayer.changeEuropeWarehouseYield(YIELD_AMMUNITION, -iChangesAmmunition);
		}
		changeNbCannon(iChangesCannon);
		changeMunition(iChangesAmmunition);

		if (getOwner() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen_DIRTY_BIT, true);
		}
	} 
	else if (getOwner() == GC.getGameINLINE().getActivePlayer()) 
	{
		pCity = pPlot->getPlotCity();
		if (pCity != NULL) 
		{
			int iTotalChangesYield = pGroup->resupplyUnits(YIELD_CANNON, iCannonPercent);
			gDLL->sendDoTask(pCity->getID(), TASK_CHANGE_YIELD_STORED, YIELD_CANNON, iTotalChangesYield, false, false, false, false);
			iTotalChangesYield = pGroup->resupplyUnits(YIELD_AMMUNITION, iAmmunitionPercent);			
			gDLL->sendDoTask(pCity->getID(), TASK_CHANGE_YIELD_STORED, YIELD_AMMUNITION, iTotalChangesYield, false, false, false, false);
			reloadEntity();
		}
	}

	if (bDisplayPopup && getOwner() == GC.getGameINLINE().getActivePlayer()) 
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_RESUPPLY_SHIPS, getID(), pCity != NULL ? pCity->getID() : -1);
		gDLL->getInterfaceIFace()->addPopup(pInfo, getOwner(), true, true);
	}
}

bool CvUnit::canEuropeRepairShip(bool bIncludeGroup) const
{
	bool bIsInEurope = (getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);
	if (!bIsInEurope)
	{
		return false;
	}

	CvSelectionGroup* pGroup = getGroup();
	if (pGroup == NULL) 
	{
		return false;
	}

	CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();

	while (pUnitNode != NULL) 
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pGroup->nextUnitNode(pUnitNode);

		if (bIncludeGroup || pLoopUnit->getID() == getID()) 
		{
			if (pLoopUnit->isHurt() && !pLoopUnit->isInEuropeDrydock()) 
			{
				return true;
			}
		}
	}

	return false;
}

int CvUnit::getReparationCostInEurope(bool bIncludeGroup) const
{
	int iDamage = 0;
	const int iPriceByDamage = 150;
	CvSelectionGroup* pGroup = getGroup();

	if (pGroup == NULL) 
	{
		return 0;
	}

	CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();

	while (pUnitNode != NULL) 
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pGroup->nextUnitNode(pUnitNode);

		if (bIncludeGroup || pLoopUnit->getID() == getID()) 
		{
			if (pLoopUnit->isHurt() && !pLoopUnit->isInEuropeDrydock())
			{
				iDamage += pLoopUnit->getUnitInfo().getCombat() * pLoopUnit->getDamage() / 100;
			}
		}
	}

	return iDamage * iPriceByDamage;
}


void CvUnit::doEuropeRepairShip(bool bIncludeGroup)
{
	if (!canEuropeRepairShip(bIncludeGroup))
	{
		return;
	}

	if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer()) 
	{		
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_REPAIR_SHIP, getID(), bIncludeGroup);
		gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
	}
}

void CvUnit::doBuyShipEurope()
{
	CvPlayer& kEuropePlayer = GET_PLAYER(getOwnerINLINE());
	if (!kEuropePlayer.isEurope())
	{
		return;
	}

	PlayerTypes ePlayer = NO_PLAYER;
	for (int iI = 0; iI < MAX_PLAYERS; iI++)
	{
		CvPlayer& kLoopPlayer = GET_PLAYER((PlayerTypes)iI);
		if (kLoopPlayer.isAlive() && kLoopPlayer.isHuman())
		{
			if (kLoopPlayer.getParent() != NO_PLAYER )
			{
				if (kLoopPlayer.getParent() == kEuropePlayer.getID())
				{
					ePlayer = (PlayerTypes)iI;
				}
			}
		}
	}

	if (ePlayer != NO_PLAYER)
	{
		CvPlayer& kPlayer = GET_PLAYER(ePlayer);
		UnitTypes eUnit = getUnitType();
		CvPlot* pStartingPlot = kPlayer.getStartingPlot();
		CvUnit* pNewUnit = NULL;
		if (eUnit != NO_UNIT  && pStartingPlot != NULL)
		{
			pNewUnit = kPlayer.initUnit(eUnit, NO_PROFESSION, INVALID_PLOT_COORD, INVALID_PLOT_COORD);
		}

		if (pNewUnit != NULL)
		{
			pNewUnit->setUnitTravelState(UNIT_TRAVEL_STATE_IN_EUROPE, false);
			pNewUnit->setDamage(getDamage());
			pNewUnit->addToMap(pStartingPlot->getX_INLINE(), pStartingPlot->getY_INLINE());
			kill(false);
			gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen2_DIRTY_BIT, true);
		}
	}
}

void CvUnit::resupplyOnlyOneShip(int iYield, int iValeur)
{
	if (iYield == 1)
	{
		changeNbCannon(-iValeur);
	}

	if (iYield == 2)
	{
		changeMunition(-iValeur);
	}
}
bool CvUnit::canEmbarkOnShip() const
{
	if (getProfession() == NO_PROFESSION)
	{
		return false;
	}
	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return false;
	}
	if (pPlot->getPlotCity() == NULL)
	{
		return false;
	}
	if (isHuman())
	{
		if (!existsAvailableShips())
		{
			return false;
		}
		if (getProfession() != PROFESSION_SAILOR)
		{
			return false;
		}
		if (isHurt())
		{
			return false;
		}
		if (getCrewFormationTurn() > 0)
		{
			return false;
		}
		//Only one sailor can be load
		int iNumUnit=0;
		CLLNode<IDInfo>* pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();
		while (pSelectedUnitNode != NULL)
		{
			CvUnit* pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);
			pSelectedUnitNode = gDLL->getInterfaceIFace()->nextSelectionListNode(pSelectedUnitNode);

			iNumUnit++;
		}
		if (iNumUnit != 1)
		{
			return false;
		}
	}
	return true;
}
bool CvUnit::canLoadCrew() const
{
	if (!m_pUnitInfo->isMechUnit())
	{
		return false;
	}
	if (getUnitTravelState() != UNIT_TRAVEL_STATE_IN_EUROPE)
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());

	for (int i = 0; i < kPlayer.getNumEuropeUnits(); ++i)
	{
		CvUnit* pLoopUnit = kPlayer.getEuropeUnit(i);
		if (pLoopUnit->getProfession() == PROFESSION_SAILOR)
		{
			if (!pLoopUnit->isHurt() && pLoopUnit->canBeSailor())
			{
				return true;
			}
		}
	}
	return false;
}
bool CvUnit::canUnloadCrew(bool bEurope) const
{
	if (!bEurope)
	{
		CvPlot* pPlot = plot();
		if (pPlot == NULL)
		{
			return false;
		}
		if (pPlot->getPlotCity() == NULL)
		{
			return false;
		}
	}
	if (!m_pUnitInfo->isMechUnit())
	{
		return false;
	}
	if (!hasCrew())
	{
		return false;
	}
	return true;
}
bool CvUnit::canSwapCrew() const
{
	int iCountShip = 0;
	int iCountCrew =0;
	CvPlot* pPlot = plot();

	if (pPlot == NULL || pPlot->getPlotCity() == NULL)
	{
		return false;
	}

	if (isAutomated())
	{
		return false;
	}

	CLLNode<IDInfo>* pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();
	while (pSelectedUnitNode != NULL)
	{
		CvUnit* pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);
		pSelectedUnitNode = gDLL->getInterfaceIFace()->nextSelectionListNode(pSelectedUnitNode);
		if (pSelectedUnit->getUnitInfo().isMechUnit())
		{
			if (pSelectedUnit->hasCrew())
			{
				iCountCrew++;
			}
		}
		else
		{
			return false;
		}
		iCountShip++;
	}

	return (iCountShip == 2 && iCountCrew > 0); // We need  at least 1 crew and 2 ships to swap
}
int CvUnit::getExperienceExploAndNavigationNeeded(int iLevel) const 
{
	FAssertMsg(iLevel > 0, "Level expected to be > 0");

	return 50*(2*(iLevel)-1+(iLevel-1)/2);
}

void CvUnit::addSailorPromotions(int iLevelGeneral, int iLevelNavigation, int iLevelFight) 
{	
	// We put iLevelGeneral > 1 to have random value for iLevelNavigation and iLevelFight
	// Otherwise, we let value caller put into those variables
	if (iLevelGeneral > 1)
	{ 
		int iRandomNavigation = GC.getGameINLINE().getSorenRandNum(iLevelGeneral, "random addSailorPromotions");
		iRandomNavigation = std::min(iRandomNavigation, 2);//We have only 3 promo of navigation
		iLevelNavigation += iRandomNavigation;
		iLevelFight += iLevelGeneral-(iRandomNavigation + 1);
	}

	setLevelExploAndNavigation(iLevelNavigation);
	setExperienceExploAndNavigation(getExperienceExploAndNavigationNeeded(iLevelNavigation));

	if (iLevelFight > 0)
	{
		setLevel(iLevelFight);
		setExperience(experienceNeeded());
		changeLevel(1);
	}

	for (int iPromotionIndex = 0; iPromotionIndex < GC.getNumPromotionInfos(); iPromotionIndex++)
	{
		PromotionTypes ePromotion = (PromotionTypes)iPromotionIndex;
		CvPromotionInfo& kPromotion = GC.getPromotionInfo(ePromotion);
		if (!kPromotion.isGraphicalOnly() && canAcquirePromotion(ePromotion))
		{
			if (kPromotion.isExploAndNavigation() && iLevelNavigation > 0)
			{
				setHasRealPromotion(ePromotion, true);
				iLevelNavigation--;
			}
			if (kPromotion.getCombatPercent() && !kPromotion.isOnlyFreePromo())
			{
				if (iLevelFight > 0)
				{
					setHasRealPromotion(ePromotion, true);
					iLevelFight--;
				}
			}
		}
	}
}

void CvUnit::addSailorPromotionsWithCoef(int iNavigationCoeff, int iPowerCoeff)
{
	int iLevelExploAndNavigation = 0, iLevelVeteran = 0;
	int iProbaExploAndNavigationPalier = 50*iNavigationCoeff/100;
	int iRandomExploAndNavigation, iRandomLevelVeteran;
	int iProbaLevelVeteranPalier = 40*iPowerCoeff/100;
	CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());

	if (!m_pUnitInfo->isMechUnit())
	{
		setCanBeSailor(true);
		setProfession(PROFESSION_SAILOR);

		if (getUnitType() == UNIT_SAILOR_EXPERT)
		{
			setLevelExploAndNavigation(1);
			setExperienceExploAndNavigation(50);
			setExperience(10);
			setLevel(3);
			return;
		}
	}
	else if (getUnitSailorType() == UNIT_SAILOR_EXPERT)
	{
		setLevelExploAndNavigation(1);
		setExperienceExploAndNavigation(50);
		setExperience(10);
		setLevel(3);

		for (int iPromotionIndex = 0; iPromotionIndex < GC.getNumPromotionInfos(); iPromotionIndex++)
		{
			PromotionTypes ePromotion = (PromotionTypes)iPromotionIndex;
			CvPromotionInfo& kPromotion = GC.getPromotionInfo(ePromotion);
			if (!kPromotion.isGraphicalOnly() && canAcquirePromotion(ePromotion))
			{
				if (GC.getUnitInfo(UNIT_SAILOR_EXPERT).getFreePromotions(iPromotionIndex))
				{
					setHasRealPromotion(ePromotion, true);
				}
			}
		}
		return;
	}
	for (int iPromotionIndex = 0; iPromotionIndex < GC.getNumPromotionInfos(); iPromotionIndex++)
	{
		PromotionTypes ePromotion = (PromotionTypes)iPromotionIndex;
		CvPromotionInfo& kPromotion = GC.getPromotionInfo(ePromotion);
		if (!kPromotion.isGraphicalOnly() && canAcquirePromotion(ePromotion))
		{
			if (kPromotion.isExploAndNavigation())
			{
				iRandomExploAndNavigation = GC.getGameINLINE().getSorenRandNum(100, "random ExploAndNavigation");
				if (iProbaExploAndNavigationPalier > iRandomExploAndNavigation)
				{
					setHasRealPromotion(ePromotion, true);
					iProbaExploAndNavigationPalier /= 2;
					iLevelExploAndNavigation++;
				}
			}
			if (kPromotion.getCombatPercent() && !kPromotion.isOnlyFreePromo())
			{
				iRandomLevelVeteran = GC.getGameINLINE().getSorenRandNum(100, "random ExploAndNavigation");
				if (iProbaLevelVeteranPalier > iRandomLevelVeteran)
				{
					setHasRealPromotion(ePromotion, true);
					iProbaLevelVeteranPalier /= 2;
					iLevelVeteran++;
				}
			}
		}
	}
	if (iLevelExploAndNavigation)
	{
		setLevelExploAndNavigation(iLevelExploAndNavigation);
		iLevelExploAndNavigation--;
		setExperienceExploAndNavigation(50*(2*(iLevelExploAndNavigation+1)-1+iLevelExploAndNavigation/2));
	}
	if (iLevelVeteran)
	{
		setLevel(iLevelVeteran);
		int iExp = experienceNeeded();
		changeLevel(1);
		setExperience(iExp);
	}
}

void CvUnit::embarkOnShip()
{
	if (!canEmbarkOnShip())
	{
		return;
	}
	if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_EMBARK_ON_SHIP, getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
	}
}

void CvUnit::loadCrew()
{
	if (!canLoadCrew())
	{
		return;
	}
	if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_LOAD_CREW, getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
	}
}

void CvUnit::sellShip()
{
	if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SELL_SHIP, getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
	}
}

void CvUnit::sendTradeProposition()
{
	if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SEND_TRADE_PROPOSITION, getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
	}
}

void CvUnit::stopSellShip()
{
	if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_STOP_SELL_SHIP, getID());
		gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
	}
}
void CvUnit::transfertEuropeToShip(YieldTypes eYield, int iQuantite, bool bLog)
{
	CvPlayer& pPlayer = GET_PLAYER(getOwnerINLINE());
	PlayerTypes eEuropePlayer = pPlayer.getParent();
	if (eEuropePlayer != NO_PLAYER)
	{
		CvPlayer& pEuropePlayer = GET_PLAYER(eEuropePlayer);
		if (eYield != NO_YIELD)
		{
			int iBuyPrice =  pPlayer.getBuyPriceForYield(eYield, iQuantite);
			int iSellPrice =  pPlayer.getSellPriceForYield(eYield, -iQuantite);
			pEuropePlayer.changeEuropeWarehouseYield(eYield, -iQuantite);
			changeNewCargoYield(eYield, iQuantite);
			if (iQuantite > 0)
			{
				// We buy in Europe
				pPlayer.changeEuropeBuyTradeYield(eYield, iQuantite);
				pPlayer.changeEuropeGoldBuyTradeYield(eYield, iBuyPrice);
				pPlayer.changeGold(-iBuyPrice);
				pPlayer.changeYieldTradedTotal(eYield, iQuantite);

				if (getGroup() == NULL || !getGroup()->isAutomated())
				{
					CvWString szString;
					CvWStringBuffer szMessage;
					GAMETEXT.setEuropeYieldBoughtHelp(szMessage, pPlayer, eYield, iQuantite);
					szString.Format(L"%s", szMessage.getCString());
					pPlayer.addEuropeTradeMessage(szString);
				}
			}
			else
			{
				// We sell in Europe
				int iTaxSellGold = iSellPrice*(pPlayer.getTaxRate())/100;
				int iSellGold = iSellPrice - iTaxSellGold;
				int iLoan = pPlayer.getEuropeLoan();
				int iPrice2 = iSellPrice * pPlayer.getEuropeLoanPercent()/100;
				iPrice2 = std::min(iLoan, iPrice2);
				pPlayer.changeEuropeLoan(-iPrice2);
				iSellGold -= iPrice2;

				pPlayer.changeEuropeTaxTradeYield(eYield, iTaxSellGold + iPrice2);
				pPlayer.changeEuropeSellTradeYield(eYield, -iQuantite);
				pPlayer.changeEuropeGoldSellTradeYield(eYield, iSellGold);
				pEuropePlayer.changeYieldBoughtTotal(eYield, -iQuantite);
				pPlayer.changeYieldTradedTotal(eYield, -iQuantite);
				pPlayer.changeGold(iSellGold);
				if (getGroup() == NULL || !getGroup()->isAutomated())
				{
					CvWStringBuffer szMessage;
					GAMETEXT.setEuropeYieldSoldHelp(szMessage, pPlayer, eYield, -iQuantite, pPlayer.getTaxRate() + pPlayer.getEuropeLoanPercent());
					CvWString szString;
					szString.Format(L"%s", szMessage.getCString());
					pPlayer.addEuropeTradeMessage(szString);
				}
			}
			gDLL->getInterfaceIFace()->setDirty(EuropeC2Screen_DIRTY_BIT, true);
		}
	}
}
void CvUnit::transfertWarehouseToShip(YieldTypes eYield, int iQuantite)
{
	CvPlayer& pPlayer = GET_PLAYER(getOwnerINLINE());
	if (eYield != NO_YIELD)
	{
		pPlayer.changeEuropeWarehouseYield(eYield, -iQuantite);
		changeNewCargoYield(eYield, iQuantite);
		if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(EuropeC2Screen_DIRTY_BIT, true);
		}
	}
}

void CvUnit::doPurchaseBid()
{
	if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_PURCHASE_BID, getID(), getProposePrice(), 0);
		gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER, true);
	}
}

void CvUnit::unloadCrew(bool bEurope)
{
	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());
	CvPlot* pPlot = plot();
	CvUnit* pNewCrew;
	int iNumPromotionInfos = GC.getNumPromotionInfos();
	int iNumPromoValid = 0;

	if (!canUnloadCrew(bEurope))
	{
		return;
	}

	int iD = getUnitSailorType();
	if ( iD == -1)
	{
		iD = GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(GC.getDefineINT("DEFAULT_POPULATION_UNIT"));
	}

	if (!bEurope)
	{
		pNewCrew = GET_PLAYER(getOwnerINLINE()).initUnit(((UnitTypes)iD), NO_PROFESSION, pPlot->getX_INLINE(), pPlot->getY_INLINE());
	}
	else
	{
		pNewCrew = GET_PLAYER(getOwnerINLINE()).initEuropeUnit((UnitTypes)iD);
	}
	setHasCrew(false);

	pNewCrew->setCanBeSailor(true);
	pNewCrew->setProfession(PROFESSION_SAILOR, true);
	PromotionTypes ePromotionLeader;
	for (int iPromotionIndex = 0; iPromotionIndex < iNumPromotionInfos; iPromotionIndex++)
	{
		PromotionTypes ePromotion = (PromotionTypes)iPromotionIndex;
		if (!GC.getPromotionInfo(ePromotion).isGraphicalOnly() && isHasRealPromotion(ePromotion))
		{
			pNewCrew->setHasRealPromotion(ePromotion, true);
			setHasRealPromotion(ePromotion, false);
		}
		if (GC.getPromotionInfo(ePromotion).isLeader())
		{
			ePromotionLeader = ePromotion;
		}
	}
	pNewCrew->setLevelExploAndNavigation(getLevelExploAndNavigation());
	pNewCrew->setExperienceExploAndNavigation(getExperienceExploAndNavigation());
	pNewCrew->setExperience(getExperience());
	pNewCrew->setLevel(getLevel());
	pNewCrew->setNumMap(getNumMap());
	if (!bEurope)
	{
		pNewCrew->testPromotionReady();
		reloadEntity();
	}
	setPromotionReady(false);
	pNewCrew->setDamage(getDamage());
	setExperience(0);
	setLevel(1);
	setNumMap(-1);
	setExperienceExploAndNavigation(0);
	setLevelExploAndNavigation(0);
	joinGroup(NULL);
	getGroup()->setAutomateType(NO_AUTOMATE);
	if (bEurope)
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen_DIRTY_BIT, true);
	}
	FAssert(GET_PLAYER(getOwnerINLINE()).checkPopulation());
}
void CvUnit::swapCrew()
{
	if (!canSwapCrew())
	{
		return;
	}
	CLLNode<IDInfo>* pSelectedUnitNode = gDLL->getInterfaceIFace()->headSelectionListNode();

	while (pSelectedUnitNode != NULL)
	{
		CvUnit* pSelectedUnit = ::getUnit(pSelectedUnitNode->m_data);
		pSelectedUnitNode = gDLL->getInterfaceIFace()->nextSelectionListNode(pSelectedUnitNode);
		if (pSelectedUnit != this)
		{
			gDLL->sendPlayerAction(getOwnerINLINE(), PLAYER_ACTION_SWAP_SAILORS, getID(), pSelectedUnit->getID(), -1);
		}
		return;
	}

}

void CvUnit::swapCrew(CvUnit* pDestUnit)
{
	int iTempLvl;
	int iTempExp;
	int iTempID;
	bool bCanBePromote1 = false;//First boat can be promote with the star button?
	bool bCanBePromote2 = false;//Second boat can be promote with the star button?

	bCanBePromote1 = isPromotionReady();
	bCanBePromote2 = pDestUnit->isPromotionReady();
	// Swtich Exp
	iTempExp = getExperience();
	setExperience(pDestUnit->getExperience());
	pDestUnit->setExperience(iTempExp);
	//Switch Exp nav
	iTempExp = getExperienceExploAndNavigation();
	setExperienceExploAndNavigation(pDestUnit->getExperienceExploAndNavigation());
	pDestUnit->setExperienceExploAndNavigation(iTempExp);
	//Switch Lvl
	iTempLvl = getLevel();
	setLevel(pDestUnit->getLevel());
	pDestUnit->setLevel(iTempLvl);
	//Switch Lvl navig
	iTempLvl = getLevelExploAndNavigation();
	setLevelExploAndNavigation(pDestUnit->getLevelExploAndNavigation());
	pDestUnit->setLevelExploAndNavigation(iTempLvl);
	//Switch IDMarin
	iTempID = getUnitSailorType();
	setUnitSailorType(pDestUnit->getUnitSailorType());
	pDestUnit->setUnitSailorType(iTempID);
	//Switch map
	iTempID = getNumMap();
	setNumMap(pDestUnit->getNumMap());
	pDestUnit->setNumMap(iTempID);
	//Swtich Promotions
	for (int iPromotionIndex = 0; iPromotionIndex < GC.getNumPromotionInfos(); iPromotionIndex++)
	{
		PromotionTypes ePromotion = (PromotionTypes)iPromotionIndex;
		if (!GC.getPromotionInfo(ePromotion).isGraphicalOnly())
		{
			if (!pDestUnit->isHasRealPromotion(ePromotion) && isHasRealPromotion(ePromotion))
			{
				setHasRealPromotion(ePromotion, false);
				pDestUnit->setHasRealPromotion(ePromotion, true);
			}
			else
			{
				if (pDestUnit->isHasRealPromotion(ePromotion) && !isHasRealPromotion(ePromotion))
				{
					setHasRealPromotion(ePromotion, true);
					pDestUnit->setHasRealPromotion(ePromotion, false);
				}
			}
		}
	}
	if (hasCrew() && !pDestUnit->hasCrew())
	{
		setHasCrew(false);
		pDestUnit->setHasCrew(true);
	}
	else 	{
		if (!hasCrew() && pDestUnit->hasCrew())
		{
			setHasCrew(true);
			pDestUnit->setHasCrew(false);
		}
	}

	CvProfessionInfo& kProfession = GC.getProfessionInfo(PROFESSION_SAILOR);
	gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), false, GC.getEVENT_MESSAGE_TIME(), gDLL->getText("TXT_KEY_SWITCH_SAILOR"), "AS2D_POSITIVE_DINK", MESSAGE_TYPE_INFO, kProfession.getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), getX_INLINE(), getY_INLINE(), true, true);

	testPromotionReady();
	pDestUnit->testPromotionReady();

	int iMaxMoves = std::max(getMoves(), pDestUnit->getMoves()) + GC.getMOVE_DENOMINATOR();
	setMoves(iMaxMoves);
	pDestUnit->setMoves(iMaxMoves);

	reloadEntity();
	pDestUnit->reloadEntity();
}

int CvUnit::getUnitSailorType() const
{
	return m_iUnitSailorType;
}

void CvUnit::setUnitSailorType(int ichange, bool bChangeAsset)
{
	if (getUnitSailorType() != ichange)
	{
		if (bChangeAsset && ichange !=  -1)
		{
			UnitTypes eOldType = (UnitTypes) m_iUnitSailorType;
			if (eOldType == NO_UNIT)
			{
				eOldType = (UnitTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(GC.getDefineINT("DEFAULT_POPULATION_UNIT"));
			}
			int iAsset = GC.getUnitInfo((UnitTypes)ichange).getAssetValue() -  GC.getUnitInfo(eOldType).getAssetValue();
			GET_PLAYER(getOwnerINLINE()).changeAssets(iAsset);
		}
		m_iUnitSailorType = ichange;
	}
}

int CvUnit::getCrewFormationTurn() const
{
	return m_iCrewFormationTurn;
}

void CvUnit::setCrewFormationTurn(int ichange)
{
	if (getCrewFormationTurn() != ichange)
	{
		m_iCrewFormationTurn = ichange;
	}
}

void CvUnit::changeCrewFormationTurn(int ichange)
{
	setCrewFormationTurn(getCrewFormationTurn() + ichange);
}

void CvUnit::processCrewFormationTurn()
{
	if (getCrewFormationTurn() > 0)
	{
		changeCrewFormationTurn(-1);
		if (getCrewFormationTurn() == 0)
		{
			setCanBeSailor(true);
		}
	}
}

bool CvUnit::hasCrew() const
{
	return m_bHasCrew;
}

void CvUnit::setHasCrew(bool bNewValue)
{
	if (hasCrew() != bNewValue)
	{
		m_bHasCrew = bNewValue;
	}
}
bool CvUnit::canBeSailor() const
{
	return m_bCanBeSailor;
}

void CvUnit::setCanBeSailor(bool bNewValue)
{
	if (canBeSailor() != bNewValue)
	{
		m_bCanBeSailor = bNewValue;
	}
}


bool CvUnit::hasTempCrew() const
{
	return m_bTempCrew;
}

void CvUnit::setTempCrew(bool bNewValue)
{
	if (hasTempCrew() != bNewValue)
	{
		m_bTempCrew = bNewValue;
	}
}

int CvUnit::getEstimatedPrice(bool bAsync) const
{
	int iFree = -1, iBase = -1, iMax = -1, iDecrease = -1, iIncrease = -1, iPrice = -1;

	getCorruptValues(&iFree, &iBase, &iMax, &iDecrease, &iIncrease, &iPrice);

	if (iFree == -1 || iBase == -1 || iMax == -1 || iDecrease == -1 || iIncrease == -1 || iPrice == -1)
	{
		return -1;
	}

	int iFreeRand = bAsync ? GC.getASyncRand().get(100, "iFreeRand ASYNC") : GC.getGameINLINE().getSorenRandNum(100, "iFreeRand");

	if (iFreeRand < iFree)
	{
		return 0;
	}
	int iBasePrice = iPrice;
	int iRandom = (bAsync ? GC.getASyncRand().get(100, "iRandom Price ASYNC") : GC.getGameINLINE().getSorenRandNum(100, "iRandom Price"));
	iBasePrice += iBasePrice*(iRandom-50)/100;//-+50%

	return iBasePrice;
}

bool CvUnit::acceptCorruption(int iChoosePrice, int iEstimation, bool bAsync) const
{
	CvPlayer& pPlayer = GET_PLAYER(getOwnerINLINE());
	int iFree = -1, iBase = -1, iMax = -1, iDecrease = -1, iIncrease = -1, iPrice = -1;

	if (iEstimation == -1)
	{
		return false;
	}

	getCorruptValues(&iFree, &iBase, &iMax, &iDecrease, &iIncrease, &iPrice);

	if (iEstimation == 0)
	{
		return true;
	}

	int iDiffPrice = iChoosePrice-iEstimation;
	int iPercent = iBase;
	int iTempPercent;
	if (iDiffPrice != 0)
	{
		iTempPercent = iDiffPrice*100/iEstimation;
		if (iIncrease > 0 && iTempPercent > 0)
		{
			iPercent += iTempPercent*iIncrease/10;
		}
		if (iDecrease > 0 && iTempPercent < 0)
		{
			iPercent += iTempPercent*iIncrease/10;
		}
	}

	for (int iI = 0; iI < GC.getNumPromotionInfos(); iI++)
	{
		PromotionTypes ePromotion = (PromotionTypes) iI;
		if (isHasPromotion(ePromotion))
		{
			iPercent -= 5;
		}
	}
	iPercent += pPlayer.getMercantileFactor();
	iPercent = range(iPercent, 0, iMax);
	int iRandom = (bAsync ? GC.getASyncRand().get(100, "iRandom acceptCorruption ASYNC") : GC.getGameINLINE().getSorenRandNum(100, "iRandom acceptCorruption"));

	return (iRandom < iPercent);
}
void CvUnit::getCorruptValues(int* iFree, int* iBase, int* iMax, int* iDecrease, int* iIncrease, int* iPrice) const
{
	ProfessionTypes eProfession = getProfession();
	UnitTypes eUnitType = getUnitType();
	if (iFree == NULL || iBase == NULL || iMax == NULL || iDecrease == NULL || iIncrease == NULL|| iPrice == NULL)
	{
		return ;
	}
	if (eProfession != NO_PROFESSION)
	{
		CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);
		*iFree = kProfession.getFreeCorruptPercent();
		*iBase = kProfession.getBaseCorruptPercent();
		*iMax = kProfession.getMaxCorruptPercent();
		*iDecrease = kProfession.getDecreaseCorruptPercent();
		*iIncrease = kProfession.getIncreaseCorruptPercent();
	}
	if (m_pUnitInfo->isMechUnit() && hasCrew())
	{
		eUnitType = (UnitTypes)getUnitSailorType();
		if (eUnitType == NO_UNIT)
		{
			eUnitType = (UnitTypes)GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(GC.getDefineINT("DEFAULT_POPULATION_UNIT"));
		}
		CvProfessionInfo& kProfession = GC.getProfessionInfo(PROFESSION_SAILOR);
		*iFree = kProfession.getFreeCorruptPercent();
		*iBase = kProfession.getBaseCorruptPercent();
		*iMax = kProfession.getMaxCorruptPercent();
		*iDecrease = kProfession.getDecreaseCorruptPercent();
		*iIncrease = kProfession.getIncreaseCorruptPercent();
	}

	if (eUnitType == NO_UNIT)
	{
		return;
	}
	CvUnitInfo& kUnitInfo = GC.getUnitInfo(eUnitType);
	*iPrice = kUnitInfo.getCorruptBasePrice();
	if (*iFree == -1 || *iBase == -1 || *iMax == -1 || *iDecrease == -1 || *iIncrease == -1)
	{
		*iFree = kUnitInfo.getFreeCorruptPercent();
		*iBase = kUnitInfo.getBaseCorruptPercent();
		*iMax  = kUnitInfo.getMaxCorruptPercent();
		*iDecrease = kUnitInfo.getDecreaseCorruptPercent();
		*iIncrease = kUnitInfo.getIncreaseCorruptPercent();
	}
}

void CvUnit::removeCapturedCrew()
{
	setTempCrew(true);
	setUnitSailorType(NO_UNIT);
	setPromotionReady(false);
	setExperience(0);
	setLevel(1);
	setNumMap(-1);
	setExperienceExploAndNavigation(0);
	setLevelExploAndNavigation(0);
	for (int iJ = 0; iJ < GC.getNumPromotionInfos(); iJ++)
	{
		PromotionTypes ePromotion = (PromotionTypes)iJ;
		if (isHasRealPromotion(ePromotion) && !GC.getPromotionInfo(ePromotion).isOnlyFreePromo())
		{
			setHasRealPromotion(ePromotion, false);
		}
	}
	reloadEntity();
}

bool CvUnit::isTempInvisible() const
{
	return m_bTempInvisible;
}

void CvUnit::setTempInvisible(bool bNewValue)
{
	if (isTempInvisible() != bNewValue)
	{
		m_bTempInvisible = bNewValue;
	}
}

void CvUnit::setTempInvisibleAllPlot(bool bNewValue)
{
	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pLoopUnit;
	CvPlot* pPlot = plot();
	if (pPlot != NULL)
	{
		pUnitNode = pPlot->headUnitNode();
		while (pUnitNode != NULL)
		{
			pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);
			if (pLoopUnit != NULL && pLoopUnit->getOwnerINLINE() == getOwnerINLINE())
			{
				pLoopUnit->setTempInvisible(bNewValue);
			}
		}
	}
}

bool CvUnit::shouldBeInvisibleOnPlot() const
{
	if (!isAutomated())
	{
		return false;
	}
	CvSelectionGroup* pGroup = getGroup();
	if (pGroup == NULL)
	{
		return false;
	}

	if (pGroup->getAutomateType() != AUTOMATE_TRANSPORT_ROUTES)
	{
		return false;
	}

	CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pGroup->nextUnitNode(pUnitNode);
		if (!pLoopUnit->canMove() || !pLoopUnit->hasMoved())
		{
			return false;
		}
	}

	return true;
}

void CvUnit::robShip(CvUnit* pDefender)
{
	if (pDefender == NULL)
	{
		return;
	}
	CvPlot* pPlot = pDefender->plot();
	if (pPlot == NULL)
	{
		return;
	}

	bool bPlundered = false;
	CvWString szBuffer;
	CvWStringBuffer szPlunderedYield, szBribedUnit, szKilledUnit;
	pDefender->setCombatUnit(NULL);

	szPlunderedYield.clear();
	szBribedUnit.clear();
	szKilledUnit.clear();

	CLLNode<IDInfo>* pUnitNode = pDefender->plot()->headUnitNode();
	CvUnit* pLoopUnit;
	std::vector<CvUnit*> apUnits;
	std::vector<CvUnit*> apBribedUnits;
	std::vector<CvUnit*> apKilledUnits;
	int iGold = 0;
	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pDefender->plot()->nextUnitNode(pUnitNode);
		if (pLoopUnit->getTransportUnit() == pDefender)
		{
			if (pLoopUnit->getUnitInfo().isTreasure())
			{
				iGold += pLoopUnit->getYieldStored();
				apUnits.push_back(pLoopUnit);
			}
			else if (m_pUnitInfo->isPirate())
			{
				if (pLoopUnit->getUnitType() == UNIT_CRIMINAL)
				{
					if (pLoopUnit->getProfession() != NO_PROFESSION && GC.getProfessionInfo(pLoopUnit->getProfession()).isUnarmed())
					{
						int iRandom = GC.getGameINLINE().getSorenRandNum(100, "iRand pirate capture criminal");
						if (iRandom < 80)
						{ //TODO
							apBribedUnits.push_back(pLoopUnit);
						}
					}
				}
				else
				{
					int iRandom = GC.getGameINLINE().getSorenRandNum(100, "iRand pirate kill passengers");
					if (iRandom < 20)
					{ //TODO
						apKilledUnits.push_back(pLoopUnit);
					}
				}
			}
		}
	}
	if (pDefender->getOnlyNewCargo() > 0 || iGold > 0)
	{
		//The Ship is only plunder
		bPlundered = true;
		int iStolenGold = 0;
		for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
		{
			YieldTypes eYield = (YieldTypes) iYield;
			CvYieldInfo& kYield = GC.getYieldInfo(eYield);
			int iNbYield = pDefender->getNewCargoYield(eYield);
			if (iNbYield > 0)
			{
				int iBasePrice = (kYield.getBuyPriceHigh() + kYield.getBuyPriceLow())/2;
				iStolenGold += (iBasePrice*iNbYield)*65/100;//Pirates can only have 65% of the sell price
				if (!szPlunderedYield.isEmpty())
				{
					szPlunderedYield.append(L",");
				}
				szPlunderedYield.append(CvWString::format(L" %d%c", iNbYield, GC.getYieldInfo(eYield).getChar()));
				pDefender->setNewCargoYield(eYield, 0);
			}
		}
		if (iGold > 0)
		{
			iStolenGold += 0;
			for (uint i = 0; i < apUnits.size(); ++i)
			{
				apUnits[i]->kill(false);
			}
			if (!szPlunderedYield.isEmpty())
			{
				szPlunderedYield.append(L",");
			}
			szPlunderedYield.append(CvWString::format(L" %d%c", iGold, gDLL->getSymbolID(GOLD_CHAR)));
		}
		if (m_pUnitInfo->isPirate())
		{
			changeStolenGold(iStolenGold);
		} 
		else
		{
			GET_PLAYER(getOwnerINLINE()).changeGold(iStolenGold);
		}
	}
	for (uint i = 0; i < apBribedUnits.size(); ++i)
	{
		if (!szBribedUnit.isEmpty())
		{
			szBribedUnit.append(L",");
		}
		szBribedUnit.append(apBribedUnits[i]->getNameAndProfession());
		apBribedUnits[i]->kill(false);
	}
	for (uint i = 0; i < apKilledUnits.size(); ++i)
	{
		if (!szKilledUnit.isEmpty())
		{
			szKilledUnit.append(L",");
		}
		szKilledUnit.append(apKilledUnits[i]->getNameAndProfession());
		apKilledUnits[i]->kill(false);
	}

	if (!szPlunderedYield.isEmpty())
	{
		szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_PLUNDERED", pDefender->getNameOrProfessionKey(), getNameOrProfessionKey(), szPlunderedYield.getCString());
		gDLL->getInterfaceIFace()->addMessage(pDefender->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
	}
	else
	{
		szBuffer = gDLL->getText("TXT_KEY_MISC_ENEMY_UNIT_ESCAPED", pDefender->getNameOrProfessionKey(), getNameOrProfessionKey());
		gDLL->getInterfaceIFace()->addMessage(pDefender->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
	}
	if (!szBribedUnit.isEmpty())
	{
		szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_BRIBED_UNIT", getNameOrProfessionKey(), szBribedUnit.getCString());
		gDLL->getInterfaceIFace()->addMessage(pDefender->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
	}
	if (!szKilledUnit.isEmpty())
	{
		szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_KILLED_UNIT", getNameOrProfessionKey(), szKilledUnit.getCString());
		gDLL->getInterfaceIFace()->addMessage(pDefender->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
	}
}
void CvUnit::captureShip(CvUnit* pDefender)
{
	CvPlayer& pPlayer = GET_PLAYER(getOwnerINLINE());
	if (pDefender == NULL)
	{
		return;
	}
	CvPlayer& pDefPlayer = GET_PLAYER(pDefender->getOwnerINLINE());
	CvPlot* pPlot = pDefender->plot();
	if (pPlot == NULL)
	{
		return;
	}
	CvWString szBuffer;
	CvWStringBuffer szPlunderedYield, szPassengerUnit;
	int iGold = 0;
	szPlunderedYield.clear();
	pDefender->setCombatUnit(NULL);

	CvUnit* pNewUnit = pPlayer.initUnit(pDefender->getUnitType(), pDefender->getProfession(), pDefender->getX_INLINE(), pDefender->getY_INLINE());
	if (pNewUnit != NULL)
	{
		pNewUnit->convert(pDefender, true);
		setTempInvisibleAllPlot(false);
		CvPlot* pPlot = pNewUnit->plot();
		std::vector<CvUnit*> apPassengerUnits;
		CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
		while(pUnitNode != NULL)
		{
			CvUnit* pCargoUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);
			CvUnit* pTranportUnit = pCargoUnit->getTransportUnit();
			if (pTranportUnit == pNewUnit)
			{
				if (pCargoUnit->getProfession() == PROFESSION_PRISONER)
				{
					if (pCargoUnit->getOriginalOwner() == getOwnerINLINE())
					{
						pCargoUnit->setProfession(PROFESSION_COLONIST, true);
						pCargoUnit->setOriginalOwner(NO_PLAYER);
						apPassengerUnits.push_back(pCargoUnit);
					}
				}
				else if (!pCargoUnit->getUnitInfo().isTreasure())
				{
					int iValueUnit = pCargoUnit->getEstimatedPrice(false);
					bool bBuy = false;
					if (iValueUnit < pPlayer.getGold() && iValueUnit <= 3000)
					{//TODO Max Value
						int iRandom = GC.getGameINLINE().getSorenRandNum(100, "iRand capture unit");
						if (iValueUnit == 0 || iRandom < 50)
						{ //TODO Proba to capture
							pPlayer.changeGold(-iValueUnit);
							bBuy = true;
						}
					}
					if (!bBuy)
					{
						pCargoUnit->setProfession(PROFESSION_PRISONER, true);
						pCargoUnit->setOriginalOwner(pNewUnit->getOwnerINLINE());
					}
					apPassengerUnits.push_back(pCargoUnit);
				}
				else
				{
					iGold += pCargoUnit->getYieldStored();
				}
			}
		}
		pUnitNode = pNewUnit->plot()->headUnitNode();
		if (pNewUnit->getOnlyNewCargo() > 0 || iGold > 0)
		{
			//The Ship is only plunder
			for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
			{
				YieldTypes eYield = (YieldTypes) iYield;
				CvYieldInfo& kYield = GC.getYieldInfo(eYield);
				int iNbYield = pNewUnit->getNewCargoYield(eYield);
				if (iNbYield > 0)
				{
					//int iBasePrice = (kYield.getBuyPriceHigh() + kYield.getBuyPriceLow())/2;
					if (!szPlunderedYield.isEmpty())
					{
						szPlunderedYield.append(L",");
					}
					szPlunderedYield.append(CvWString::format(L" %d%c", iNbYield, GC.getYieldInfo(eYield).getChar()));
				}
			}
			if (iGold > 0)
			{
				if (!szPlunderedYield.isEmpty())
				{
					szPlunderedYield.append(L",");
				}
				szPlunderedYield.append(CvWString::format(L" %d%c", iGold, gDLL->getSymbolID(GOLD_CHAR)));
			}
		}
		for (uint i = 0; i < apPassengerUnits.size(); ++i)
		{
			if (!szPassengerUnit.isEmpty())
			{
				szPassengerUnit.append(L",");
			}
			szPassengerUnit.append(apPassengerUnits[i]->getUnitInfo().getDescription());
			apPassengerUnits[i]->kill(false);
		}

		if (!szPlunderedYield.isEmpty())
		{
			szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_CAPTURED_UNIT_DETAILS", getNameOrProfessionKey(), pNewUnit->getNameOrProfessionKey(), szPlunderedYield.getCString());
			gDLL->getInterfaceIFace()->addMessage(pDefPlayer.getID(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
		}
		else
		{
			szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_CAPTURED_UNIT_NO_YIELD", getNameOrProfessionKey(), pNewUnit->getNameOrProfessionKey());
			gDLL->getInterfaceIFace()->addMessage(pDefPlayer.getID(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
		}
		if (!szPassengerUnit.isEmpty())
		{
			szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_UNIT_CAPTURED_UNIT_PASSENGERS", getNameOrProfessionKey(), szPassengerUnit.getCString());
			gDLL->getInterfaceIFace()->addMessage(pDefPlayer.getID(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_THEIR_WITHDRAWL", MESSAGE_TYPE_INFO, NULL, (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE());
		}
	}
}
void CvUnit::processTempInvisible()
{
	int iPercent = m_pUnitInfo->getPercentInvisible();
	if (iPercent > 0) 
	{
		bool bShouldBeInvisible = false;
		CvSelectionGroup* pGroup = getGroup();
		CvPlot* pPlot = plot();
		if (pGroup != NULL && pGroup->getNumUnits() == 1)
		{
			if (pPlot != NULL && pPlot->getPlotCity() == NULL) //No invisibility on city
			{
				int iRandom = GC.getGameINLINE().getSorenRandNum(100, "processTempInvisible");
				if (iRandom <= iPercent) 
				{
					bShouldBeInvisible = true;
				}
			}
		}
		if (!bShouldBeInvisible && m_pUnitInfo->isMechUnit() && AI_isReservedForWarPlan()) 
		{
			bShouldBeInvisible = true;
		}

		if (bShouldBeInvisible) 
		{
			PlayerTypes eOwner = pPlot->getOwner(); 
			if (eOwner != getID() && eOwner != NO_PLAYER)
			{
				if (::atWar(getTeam(), GET_PLAYER(eOwner).getTeam()))
				{
					bShouldBeInvisible = false;
				}
			}
		}

		setTempInvisible(bShouldBeInvisible);
	} 
	else
	{
		setTempInvisible(false);
	}
}
bool CvUnit::isInEuropeDrydock() const
{
	return m_bInEuropeDrydock;
}

void CvUnit::setInEuropeDrydock(bool bNewValue)
{
	if (isInEuropeDrydock() != bNewValue)
	{
		m_bInEuropeDrydock = bNewValue;
	}
}
bool CvUnit::mustStayInEurope() const
{
	return m_bMustStayInEurope;
}

void CvUnit::setMustStayInEurope(bool bNewValue)
{
	if (mustStayInEurope() != bNewValue)
	{
		m_bMustStayInEurope = bNewValue;
	}
}
bool CvUnit::isLikelyToLeave() const
{
	return m_bLikelyToLeave;
}

void CvUnit::setLikelyToLeave(bool bNewValue)
{
	if (isLikelyToLeave() != bNewValue)
	{
		m_bLikelyToLeave = bNewValue;
	}
}
bool CvUnit::isHasPurchaseBid() const
{
	return m_bhasPurchaseBid;
}

void CvUnit::setHasPurchaseBid(bool bNewValue)
{
	if (isHasPurchaseBid() != bNewValue)
	{
		m_bhasPurchaseBid = bNewValue;
	}
}
bool CvUnit::existsAvailableShips() const
{
	CvPlot* pPlot = plot();
	CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);
		if (pLoopUnit->canHaveCrew())
		{
			return true;
		}
	}
	return false;
}
bool CvUnit::canHaveCrew() const
{
	if (!m_pUnitInfo->isMechUnit())
	{
		return false;
	}
	if (hasCrew())
	{
		return false;
	}
	return true;
}
bool CvUnit::canRemoveRessource() const
{
	if (getProfession() == NO_PROFESSION)
	{
		return false;
	}

	if (GC.getProfessionInfo(getProfession()).getWorkRate() == 0)
	{
		return false;
	}
	if (!canMove())
	{
		return false;
	}

	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return false;
	}
	if (pPlot->getBonusType() == NO_BONUS)
	{
		return false;
	}
	if (!GC.getBonusInfo(pPlot->getBonusType()).canBeRemoved())
	{
		return false;
	}
	if (pPlot->isWater())
	{
		return false;
	}
	if (GET_PLAYER(getOwnerINLINE()).getTeam() != pPlot->getTeam())
	{
		return false;
	}

	return true;
}

void CvUnit::removeRessource()
{
	if (!canRemoveRessource())
	{
		return;
	}
	CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_REMOVE_RESSOURCE, getID(), 0);
	gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true, true);
}

bool CvUnit::canGiveMap() const
{
	if (getNumMap() == -1)
	{
		return false;
	}
	CvPlot* pUnitPlot = plot();
	if (pUnitPlot == NULL)
	{
		return false;
	}
	if (gDLL->getInterfaceIFace()->getLengthSelectionList() > 1)
	{
		return false;
	}
	int iDistance = 1;
	for (int iDX = -iDistance; iDX <= iDistance; iDX++)
	{
		for (int iDY = -iDistance; iDY <= iDistance; iDY++)
		{
			CvPlot* pPlot = plotXY(pUnitPlot->getX_INLINE(), pUnitPlot->getY_INLINE(), iDX, iDY);
			if (pPlot != NULL)
			{
				CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
				while (pUnitNode != NULL)
				{
					CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
					pUnitNode = pPlot->nextUnitNode(pUnitNode);
					if (pLoopUnit != this && pLoopUnit->getOwner() == getOwner())
					{
						if (pLoopUnit->canHaveMap())
						{
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}

bool CvUnit::canHaveMap() const
{
	if (!isHuman())
	{
		return false;
	}
	if (!isOnMap())
	{
		return false;
	}
	if (m_pUnitInfo->isMechUnit() && hasCrew())
	{
		return true;
	}
	if (getProfession() == PROFESSION_SCOUT)
	{
		return true;
	}
	return false;
}

void CvUnit::giveMap()
{
	if (!canGiveMap())
	{
		return;
	}
	CvPopupInfo* pPopupInfo = new CvPopupInfo(BUTTONPOPUP_GIVE_MAP, getID(), -1);
	gDLL->getInterfaceIFace()->addPopup(pPopupInfo, getOwnerINLINE(), true, true);
}

void CvUnit::showMapToPlayer()
{
	int iNumMap = getNumMap();
	TeamTypes eTeam = getTeam();
	if (eTeam == NO_TEAM)
	{
		return;
	}
	setNumMap(-1);
	CvPlot* pPlot = NULL;
	for (int iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); iI++)
	{
		pPlot = GC.getMapINLINE().plotByIndexINLINE(iI);
		if (pPlot != NULL && pPlot->isRevealedMap(eTeam, iNumMap))
		{
			pPlot->setRevealedMap(eTeam, iNumMap, false);
			if (!pPlot->isDefinitelyRevealed(eTeam))
			{
				pPlot->setDefinitelyRevealed(eTeam, true);
				pPlot->setRevealed(eTeam, true, false, NO_TEAM, false);
			}
		}
	}
	GET_PLAYER(getOwnerINLINE()).deleteUnitMap(iNumMap);
}

int CvUnit::stepDistanceWithUnit(CvUnit* pUnit) const
{
	return stepDistance(getX(), getY(), pUnit->getX(), pUnit->getY());
}

bool CvUnit::canTransfertMapTo(CvUnit* pUnit) const
{
	TeamTypes eTeam = getTeam();
	if (eTeam == NO_TEAM)
	{
		return false;
	}

	//if (stepDistanceWithUnit(pUnit) > 1) 
	//{
	//	return false;
	//}

	if (getNumMap() == -1)
	{
		return false;
	}

	return true;
}

void CvUnit::transfertMapTo(CvUnit* pUnit)
{
	TeamTypes eTeam = getTeam();
	if (!canTransfertMapTo(pUnit))
	{
		return;
	}

	//We are supposing than pUnit can optain a map.
	//First unit could have a forced map from goody, let it give its map
	int iNumMap = getNumMapForced(true);
	//Second unit must satisfy common map rules
	int iNumMap2 = pUnit->getNumMap(true);

	if (iNumMap2 == -1) //Second unit has no map
	{
		//Just transfer the old map
		pUnit->setNumMap(iNumMap);
		/*Note: don't delete this map since now it's owned by second unit!*/
		pUnit->setCountDiscoveredMapTiles(getCountDiscoveredMapTiles());
	} 
	else if (iNumMap != -1) //Second unit has already a map
	{  
		CvPlot* pPlot = NULL;
		//Add plots from old map to secon unit's map:
		for (int iI = 0; iI < GC.getMapINLINE().numPlotsINLINE(); iI++)
		{
			pPlot = GC.getMapINLINE().plotByIndexINLINE(iI);
			if (pPlot != NULL && pPlot->isRevealedMap(eTeam, iNumMap))
			{
				pPlot->setRevealedMap(eTeam, iNumMap, false);
				if (!pPlot->isRevealedMap(eTeam, iNumMap2))
				{
					pPlot->setRevealedMap(eTeam, iNumMap2, true);
					pUnit->incrementCountDiscoveredMapTiles();
				}
				if (pPlot->isDefinitelyRevealed(eTeam) && pPlot->isRevealedMap(eTeam, iNumMap2))
				{
					pPlot->setRevealedMap(eTeam, iNumMap2, false);
				}
			}
		}
		//Delete old map
		GET_PLAYER(getOwnerINLINE()).deleteUnitMap(iNumMap);
	}

	setNumMap(-1); //Take away unit's map

	reloadEntity();
}
//DOANE
//Unit Ammunition

bool CvUnit::hasToBeResupplied(bool bTestAmmunition, bool bTestCannon) const
{
	if (bTestAmmunition) 
	{
		int iMaxMunitions = getMaxMunition();
		if (iMaxMunitions != -1 && getMunition() < iMaxMunitions) 
		{
			return true;
		}
	}

	if (bTestCannon) 
	{
		int iMaxCannon = getMaxCannon();
		if (iMaxCannon != -1 && getNbCannon() < iMaxCannon) 
		{
			return true;
		}
	}

	return false;
}

int CvUnit::getMaxMunition() const
{
	ProfessionTypes eProfession = getProfession();
	if (eProfession != NO_PROFESSION)
	{
		return GC.getProfessionInfo(eProfession).getMaxMunition();
	}
	else
	{
		return m_pUnitInfo->getMaxMunition();
	}
}

int CvUnit::getMunition() const
{
	return m_iNbMinution;
}

void CvUnit::setMunition(int iNewValue)
{
	if (getMunition() != iNewValue)
	{
		m_iNbMinution = iNewValue;
	}
	if (iNewValue < 0)
	{
		m_iNbMinution = 0;
	}
}

void CvUnit::setMaxMunition()
{
	short wNewValue = getMaxMunition();
	m_iNbMinution = (wNewValue >= 0) ? (int)wNewValue : 0;
}

void CvUnit::changeMunition(int iChange)
{
	int iNewValue = getMunition() + iChange;
	setMunition(iNewValue >= 0 ? iNewValue : 0);
}

int CvUnit::getConsommationMunition(bool bShip) const
{
	int iConsom = -1;

	if (bShip)
	{
		if (m_pUnitInfo->getMaxMunition() != -1)
		{
			iConsom = getNbCannon()/10;
		}
	}
	else
	{
		if (getProfession() != NO_PROFESSION && GC.getProfessionInfo(getProfession()).getBombardRate() != -1)
		{
			iConsom = GC.getProfessionInfo(getProfession()).getYieldEquipmentAmount(YIELD_CANNON)/10;
		}
	}
	if (iConsom == 0)
	{
		return -1;
	}
	return iConsom;
}

void CvUnit::ammunitionProcess(CvUnit* pDefender)
{
	ProfessionTypes pProfessionA = getProfession();
	ProfessionTypes pProfessionD = pDefender->getProfession();

	if ( pProfessionA != NO_PROFESSION && GC.getProfessionInfo(pProfessionA).getMaxMunition() != -1 && isHuman())
	{
		if (GC.getProfessionInfo(pProfessionA).getBombardRate() > 0)
		{
			if (getMunition() >= 4)
			{
				changeMunition(-4);
			}
			else
			{
				changeMunition(-getMunition());
			}
		}
		else
		{
			changeMunition(-1);
		}
	}
	if ( pProfessionD != NO_PROFESSION && GC.getProfessionInfo(pProfessionD).getMaxMunition() != -1 && pDefender->isHuman())
	{
		if (GC.getProfessionInfo(pProfessionD).getBombardRate() > 0)
		{
			if (pDefender->getMunition() >= 4)
			{
				pDefender->changeMunition(-4);
			}
			else
			{
				pDefender->changeMunition(-pDefender->getMunition());
			}
		}
		else
		{
			pDefender->changeMunition(-1);
		}
	}
	// for the Ship
	int iAttackerConsum = getConsommationMunition(true);
	int iDefenderConsum = pDefender->getConsommationMunition(true);

	if (iAttackerConsum != -1 && isHuman())
	{
		if (getMunition() >= iAttackerConsum)
		{
			changeMunition(-iAttackerConsum);
		}
		else
		{
			changeMunition(-getMunition());
		}
	}
	if (iDefenderConsum != -1 && pDefender->isHuman())
	{
		if (pDefender->getMunition() >= iDefenderConsum)
		{
			pDefender->changeMunition(-iDefenderConsum);
		}
		else
		{
			pDefender->changeMunition(-pDefender->getMunition());
		}
	}
}

int CvUnit::getMaxCannon() const
{
	return m_pUnitInfo->getMaxCannon();
}

int CvUnit::getNbCannon() const
{
	return m_iNbCannon;
}

void CvUnit::setNbCannon(int iNewValue)
{
	if (getNbCannon() != iNewValue)
	{
		m_iNbCannon = iNewValue;
	}
}

void CvUnit::setNbMaxCannon()
{
	short wNewValue = getMaxCannon();
	m_iNbCannon = (wNewValue >= 0) ? (int)wNewValue : 0;
}

void CvUnit::changeNbCannon(int iChange)
{
	setNbCannon(getNbCannon() + iChange);
}
//END DOANE
int CvUnit::getRebelSentiment() const
{
	return m_iRebelPercent;
}

void CvUnit::setRebelSentiment(int iNewValue)
{

	int iNewEra = GET_PLAYER(getOwnerINLINE()).getNewEra();
	if (iNewEra == 0)
	{
		iNewValue = std::min(30, iNewValue);
	}
	if (iNewEra == 1)
	{
		iNewValue = std::min(60, iNewValue);
	}
	if (getRebelSentiment() != iNewValue)
	{
		m_iRebelPercent = iNewValue;
	}
}

void CvUnit::changeRebelSentiment(int iChange)
{
	setRebelSentiment(getRebelSentiment() + iChange);
}

bool CvUnit::canSell() const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());
	if (m_pUnitInfo->getEuropeCost() <= 0)
	{
		return false;
	}
	if (!m_pUnitInfo->isMechUnit())
	{
		return false;
	}
	if (getUnitType() == UNIT_PRIVATEER)
	{
		return false;
	}
	CvPlot* pPlot = plot();
	if (isOnMap())
	{
		bool bOk = false;
		if (pPlot != NULL)
		{
			CvCity* pCity = pPlot->getPlotCity();
			if (pCity != NULL)
			{
				if (getTeam() == pCity->getTeam())
				{
					bOk = true;
				}
			}
		}
		if (!bOk)
		{
			return false;
		}
	}
	//if unit is  not a war ship we try to find if a other trade ship existed
	if (m_pUnitInfo->getMaxCannon() <= 0)
	{
		int iLoop;
		for (CvUnit* pLoopUnit = kPlayer.firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = kPlayer.nextUnit(&iLoop))
		{
			if (pLoopUnit != this)
			{
				if (pLoopUnit->getDomainType() == DOMAIN_SEA && pLoopUnit->getShipSellPrice() == 0)
				{
					if (pLoopUnit->getUnitInfo().getMaxCannon() <= 0)
					{
						return true;
					}
				}
			}
		}

		for (int i = 0; i < kPlayer.getNumEuropeUnits(); ++i)
		{
			CvUnit* pLoopUnit = kPlayer.getEuropeUnit(i);
			if (pLoopUnit != this)
			{
				if (pLoopUnit->getDomainType() == DOMAIN_SEA && pLoopUnit->getShipSellPrice() == 0)
				{
					if (pLoopUnit->getUnitInfo().getMaxCannon() <= 0)
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	return true;
}

bool CvUnit::canUnloadNewCargoInShip() const
{
	if (getOnlyNewCargo() == 0)
	{
		return false;
	}
	if (isAutomated())
	{
		return false;
	}
	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return false;
	}
	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL)
	{
		return false;
	}
	if (getTeam() != pCity->getTeam())
	{
		return false;
	}
	return true;
}
//DOANE builder Pack
bool CvUnit::canResupplyBuilderPack(bool bTestYieldCity) const
{
	if (isHaveFoundPack())
	{
		return false;
	}
	if (!m_pUnitInfo->isMechUnit())
	{
		return false;
	}
	if (!m_pUnitInfo->isOnlyDefensive())
	{
		return false;
	}
	if (isAutomated())
	{
		return false;
	}
	CvCity* pCity = getCity(); //Also checks pPlot == NULL
	if (pCity == NULL)
	{
		return false;
	}
	if (getTeam() != pCity->getTeam())
	{
		return false;
	}
	if (gDLL->getInterfaceIFace()->getLengthSelectionList() > 1)
	{
		return false;
	}
	if (bTestYieldCity)
	{
		//Get the builder wagon cost
		unsigned char iModifier = GC.getBUILDER_PACK_YIELD_COST_PERCENT();
		short iToolCost = GC.getBuilderPackToolCost();
		short iLumberCost = GC.getBuilderPackLumberCost();

		//Apply Cost Modifier
		iToolCost *= iModifier;
		iToolCost /= 100;
		iLumberCost *= iModifier;
		iLumberCost /= 100;

		//Check City yields
		if (pCity->getYieldStored(YIELD_TOOLS) < iToolCost)
		{
			return false;
		}
		if (pCity->getYieldStored(YIELD_LUMBER) < iLumberCost)
		{
			return false;
		}
	}
	return true;
}

bool CvUnit::canDeleteBuilderPack() const
{
	if (!isHaveFoundPack())
	{
		return false;
	}
	if (isAutomated())
	{
		return false;
	}
	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return false;
	}
	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL)
	{
		return false;
	}
	if (getTeam() != pCity->getTeam())
	{
		return false;
	}
	if (gDLL->getInterfaceIFace()->getLengthSelectionList() > 1)
	{
		return false;
	}
	return true;

}
bool CvUnit::canMakeSeaway(bool bDelete) const
{
	CvPlot* pPlot = plot();

	if (pPlot == NULL)
	{
		return false;
	}

	if (!pPlot->isEurope())
	{
		return false;
	}
	if (!m_pUnitInfo->isMechUnit())
	{
		return false;
	}
	if (getOwnerINLINE() == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());

	if (!bDelete)
	{
		if (gDLL->getInterfaceIFace()->getLengthSelectionList() > 1)
		{
			return false;
		}
	}

	CLLNode<CvSeaway>* pSeawayNode = kPlayer.headSeawayNode();

	while (pSeawayNode != NULL)
	{
		if (pSeawayNode->m_data.isSamePlot(pPlot))
		{
			return bDelete;
		}
		pSeawayNode = kPlayer.nextSeawayNode(pSeawayNode);
	}
	return !bDelete;
}

void CvUnit::deleteSeaway()
{
	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());

	CvSeaway* pSeaway = kPlayer.findSeaway(plot());
	if ( pSeaway != NULL)
	{
		kPlayer.deleteSeawayByID(pSeaway->getID());
		reloadEntity();
	}
}

bool CvUnit::canChooseSeaway() const
{
	if (!m_pUnitInfo->isMechUnit())
	{
		return false;
	}
	if (getOwnerINLINE() == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());

	if (kPlayer.getNumSeaways() == 0)
	{
		return false;
	}

	return true;
}


int CvUnit::getLearningProfession() const
{
	return m_iLearningProfession;
}

void CvUnit::setLearningProfession(int iValue)
{
	m_iLearningProfession = iValue;
}
int CvUnit::getFormationProfession() const
{
	return m_iFormationProfession;
}

void CvUnit::setFormationProfession(int iValue)
{
	m_iFormationProfession = iValue;
}

int CvUnit::getLevelExploAndNavigation() const
{
	return m_iLevelExploAndNavigation;
}

void CvUnit::setLevelExploAndNavigation(int iValue)
{
	m_iLevelExploAndNavigation = iValue;
}

void CvUnit::changeLevelExploAndNavigation(int iValue)
{
	setLevelExploAndNavigation(getLevelExploAndNavigation() + iValue);
}


int CvUnit::getEuropeRecruitPrice() const
{
	return m_iEuropeRecruitPrice;
}

void CvUnit::setEuropeRecruitPrice(int iValue)
{
	m_iEuropeRecruitPrice = iValue;
}

int CvUnit::getShipSellPrice() const
{
	return m_iSellPrice;
}

void CvUnit::setShipSellPrice(int iValue)
{
	m_iSellPrice = iValue;
}

int CvUnit::getStolenGold() const
{
	return m_iStolenGold;
}

void CvUnit::changeStolenGold(int iChange)
{
	m_iStolenGold += iChange;
	if (m_pUnitInfo->isPirate())
	{
		int iUpgradeGold1 = 15000, iUpgradeGold2 = 30000;
		if (m_pUnitInfo->getPirateLevel() == 1 && getStolenGold() >= iUpgradeGold1)
		{
			m_iStolenGold -= iUpgradeGold1;
			upgradePirate();
		}
		if (m_pUnitInfo->getPirateLevel() == 2 && getStolenGold() >= iUpgradeGold2)
		{
			m_iStolenGold -= iUpgradeGold2;
			upgradePirate();
		}
	}
}

void CvUnit::upgradePirate()
{
	PlayerTypes eTargetPlayer = getTargetPlayer();
	if (eTargetPlayer != NO_PLAYER)
	{
		CvPlayer& kPlayer = GET_PLAYER(eTargetPlayer);
		CvUnit* pPirateUnit = kPlayer.addPirate(m_pUnitInfo->getPirateLevel()+1, true);
		if (pPirateUnit != NULL)
		{
			pPirateUnit->setName(getName());
			pPirateUnit->setExperienceExploAndNavigation(getExperienceExploAndNavigation());
			pPirateUnit->setLevel(getLevel());
			pPirateUnit->setLevelExploAndNavigation(getLevelExploAndNavigation());
			pPirateUnit->setUnitSailorType(getUnitSailorType());
			pPirateUnit->convert(this, true);
		}
		else
		{
			CvString szError;
			szError.Format("pirateUpgrade WARNING pPirateUnit == NULL");
			gDLL->logMsg("DoaNE.log", szError);
		}
	}	
}


int CvUnit::getProposePrice() const
{
	return m_iProposePrice;
}

void CvUnit::setProposePrice(int iValue)
{
	m_iProposePrice = iValue;
}

int CvUnit::getNumMap(bool bOnlyRead)
{
	if (!m_pUnitInfo->isMechUnit() && getProfession() != PROFESSION_SCOUT)
	{
		return -1;
	}

	if (m_iNumMap != -1 || bOnlyRead)
	{
		return m_iNumMap;
	}

	if (getOwnerINLINE() == NO_PLAYER)
	{
		return -1;
	}

	int iNewMap = GET_PLAYER(getOwnerINLINE()).getNextUnitMap();
	if (iNewMap != -1)
	{
		setNumMap(iNewMap);
		return iNewMap;
	}

	return -1;
}

int CvUnit::getNumMap() const
{
	return m_iNumMap;
}

int CvUnit::getNumMapForced(bool bOnlyRead)
{
	PlayerTypes eOwner = getOwnerINLINE();
	if (m_iNumMap != -1 || bOnlyRead) 
	{
		return m_iNumMap;
	}

	if (eOwner == NO_PLAYER || hasTempCrew()) 
	{
		return -1;
	}

	int iNewMap = GET_PLAYER(eOwner).getNextUnitMap();
	if (iNewMap != -1) 
	{
		setNumMap(iNewMap);
		return iNewMap;
	}
	return -1;
}

void CvUnit::setNumMap(int iValue)
{
	m_iNumMap = iValue;
	if (iValue < 0)
	{
		resetCountDiscoveredMapTiles();
	}
}

int CvUnit::getExperienceExploAndNavigation() const
{
	return m_iExperienceExploAndNavigation;
}

void CvUnit::setExperienceExploAndNavigation(int iValue)
{
	m_iExperienceExploAndNavigation = std::min(300, iValue);
}

void CvUnit::changeExperienceExploAndNavigation(int iValue)
{
	if (hasTempCrew())
	{
		return;
	}
	setExperienceExploAndNavigation(std::min(300, getExperienceExploAndNavigation() + iValue));
}

int CvUnit::getExploAndNavigationNecessaryExperience() const
{
	int iExperience = 0;
	switch (getLevelExploAndNavigation())
	{
	case 0:
		iExperience = 50;
		break;
	case 1:
		iExperience = 150;
		break;
	case 2:
	case 3:
		iExperience = 300;
		break;
	}
	return iExperience;
}

void CvUnit::processExploAndNavigation()
{
	bool bGivePromo = false;
	if (m_pUnitInfo->isMechUnit() || getProfession() == PROFESSION_SCOUT)
	{
		switch(getLevelExploAndNavigation())
		{
		case 0:
			if (getExperienceExploAndNavigation() >= 50)
			{
				setLevelExploAndNavigation(1);
				bGivePromo = true;
			}
			break;
		case 1:
			if (getExperienceExploAndNavigation() >= 150)
			{
				setLevelExploAndNavigation(2);
				bGivePromo = true;
			}
			break;
		case 2:
			if (getExperienceExploAndNavigation() == 300)
			{
				setLevelExploAndNavigation(3);
				bGivePromo = true;
			}
			break;
		}

		if (bGivePromo) 
		{
			for (int iJ = 0; iJ < GC.getNumPromotionInfos(); iJ++) 
			{
				PromotionTypes ePromotion = (PromotionTypes)iJ;
				if (GC.getPromotionInfo(ePromotion).isExploAndNavigation() && canAcquirePromotion(ePromotion)) 
				{
					setHasRealPromotion(ePromotion, true);
					break;
				}
			}
		}

		processExploAndNavigationUpgrade();
	}
}

bool CvUnit::canProcessExploAndNavigationUpgrade() const
{
	CvPlot* pPlot = plot();
	if (getExperienceExploAndNavigation() < 50)
	{
		return false;
	}
	//Si c'est d�ja un explorateur aguerris
	if (getUnitType() == UNIT_SCOUT)
	{
		return false;
	}
	if (pPlot == NULL)
	{
		return false;//Le bateau doit etre dans le nouveau  monde pour l'upgrade
	}
	if (getProfession() != NO_PROFESSION)
	{
		//Scout part
		if (getProfession() != PROFESSION_SCOUT)
		{
			return false;
		}
		if (isHuman())
		{
			if (canClearSpecialty())
			{
				return false;
			}
		}
	}
	else
	{
		//Sailor part

		if (getUnitSailorType() == UNIT_SAILOR_EXPERT)
		{
			return false; //Si l'unit� est marin exp�riment� on retourne faux
		}
		for (int iJ = 0; iJ < GC.getNumPromotionInfos(); iJ++)
		{
			PromotionTypes ePromotion = (PromotionTypes)iJ;
			if (GC.getUnitInfo(UNIT_SAILOR_EXPERT).getFreePromotions(iJ))
			{
				if (!isHasRealPromotion(ePromotion) && !GC.getPromotionInfo(ePromotion).isOnlyFreePromo())
				{
					return false;//S'il n'a pas les promos gratuites du sailor expert pas d'upgrades
				}
			}
		}

	}
	return true;
}
void CvUnit::processExploAndNavigationUpgrade()
{
	CvPlot* pPlot = plot();
	CvUnit* pNewUnit;

	if (!canProcessExploAndNavigationUpgrade())
	{
		return;
	}

	if (getProfession() == NO_PROFESSION)
	{
		//C'est le marin dans le bateau
		setUnitSailorType(UNIT_SAILOR_EXPERT);

		for (int iPromotionIndex = 0; iPromotionIndex < GC.getNumPromotionInfos(); iPromotionIndex++)
		{
			PromotionTypes ePromotion = (PromotionTypes)iPromotionIndex;
			if (!GC.getPromotionInfo(ePromotion).isGraphicalOnly() && !isHasRealPromotion(ePromotion))
			{
				if (GC.getPromotionInfo(ePromotion).isOnlyFreePromo() && canAcquirePromotion(ePromotion))
				{
					setHasRealPromotion(ePromotion, true);
				}
			}
		}
		return;
	}
	//On s'occupe du scout desormais
	pNewUnit = GET_PLAYER(getOwnerINLINE()).initUnit(UNIT_SCOUT, getProfession(), pPlot->getX_INLINE(), pPlot->getY_INLINE(), NO_UNITAI, getFacingDirection(false));


	for (int iPromotionIndex = 0; iPromotionIndex < GC.getNumPromotionInfos(); iPromotionIndex++)
	{
		PromotionTypes ePromotion = (PromotionTypes)iPromotionIndex;
		if (!GC.getPromotionInfo(ePromotion).isGraphicalOnly() && isHasRealPromotion(ePromotion))
		{
			pNewUnit->setHasRealPromotion(ePromotion, true);
		}
	}
	pNewUnit->setLevel(getLevel());
	pNewUnit->setExperience(getExperience());
	pNewUnit->testPromotionReady();
	pNewUnit->setDamage(getDamage());
	pNewUnit->setExperienceExploAndNavigation(getExperienceExploAndNavigation());
	pNewUnit->setLevelExploAndNavigation(getLevelExploAndNavigation());
	transfertMapTo(pNewUnit);

	kill(true);
	reloadEntity();
}

int CvUnit::getSellPrice() const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());
	PlayerTypes eEuropePlayer = kPlayer.getParent();
	int iGold = m_pUnitInfo->getEuropeCost();

	if (isHurt())
	{
		iGold -= iGold*getDamage()/100;
	}

	iGold /= 2;
	if (eEuropePlayer != NO_PLAYER )
	{
		iGold += iGold * GET_PLAYER(eEuropePlayer).getUnitMarketPrice((UnitClassTypes) m_pUnitInfo->getUnitClassType()) / 100;
	}

	iGold -= iGold * kPlayer.getTaxRate() / 100;

	return iGold;
}

int CvUnit::getShipUsedBuyPrice() const
{
	int iGold = m_pUnitInfo->getEuropeCost()*75/100;
	if (isHurt())
	{
		iGold -= iGold*getDamage()/250;
	}

	PlayerTypes eEuropePlayer = GET_PLAYER(getOwnerINLINE()).getParent();
	if (eEuropePlayer != NO_PLAYER )
	{
		iGold += iGold*GET_PLAYER(eEuropePlayer).getUnitMarketPrice((UnitClassTypes) m_pUnitInfo->getUnitClassType())/100;
	}
	return iGold;

}

void CvUnit::sellUnit(int iGold)
{
	CvUnit* pLoopUnit;
	CvPlot* pPlot = plot();

	bool bRefreshEurope = (getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);

	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());
	kPlayer.changeGold(iGold);

	if (hasCrew())
	{
		unloadCrew(!isOnMap());
	}

	CvCity* pCity = pPlot->getPlotCity();
	if (pCity != NULL)
	{
		unloadAll();
		if (getMunition() > 0)
		{
			pCity->changeYieldStored(YIELD_AMMUNITION, getMunition());
		}
		if (getNbCannon() > 0)
		{
			pCity->changeYieldStored(YIELD_CANNON, getNbCannon());
		}
	}
	else
	{
		CLLNode<IDInfo>* pUnitNode;
		pUnitNode = pPlot->headUnitNode();
		while (pUnitNode != NULL)
		{
			pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);
			if (pLoopUnit->getTransportUnit() == this)
			{
				if (pLoopUnit->isGoods())
				{
					kPlayer.doAction(PLAYER_ACTION_SELL_YIELD_UNIT, pLoopUnit->getID(), pLoopUnit->getYieldStored(), 0);
				}
				else
				{
					kPlayer.doAction(PLAYER_ACTION_UNLOAD_UNIT_TO_EUROPE, pLoopUnit->getID(), 0, 0);
				}
			}
		}
	}

	setDamage(GC.getMAX_HIT_POINTS());

	if (bRefreshEurope && GC.getGameINLINE().getActivePlayer() == getOwnerINLINE())
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen2_DIRTY_BIT, true);
	}
}

bool CvUnit::canLearn() const
{
	UnitTypes eUnitType = getLearnUnitType(plot());
	if (eUnitType == NO_UNIT)
	{
		return false;
	}

	if (isCargo() && !canUnload())
	{
		return false;
	}

	CvCity* pCity = plot()->getPlotCity();
	if (isHuman() && !pCity->allowsToFormUnits(getOwner()))
	{	
		return false;
	}

	return true;
}

void CvUnit::learn()
{
	if (!canLearn())
	{
		return;
	}

	CvCity* pCity = plot()->getPlotCity();
	PlayerTypes eNativePlayer = pCity->getOwnerINLINE();

	if (isHuman() && !getGroup()->AI_isControlled() && !GET_PLAYER(eNativePlayer).isHuman())
	{
		UnitTypes eUnitType = getLearnUnitType(plot());
		FAssert(eUnitType != NO_UNIT);
		PlayerTypes ePrivilegedPlayer = pCity->getPriviledEuropeanPlayer();

		CvDiploParameters* pDiplo = new CvDiploParameters(eNativePlayer);

		// It means the player is allowed to form units
		if (pCity->allowsToFormUnits(getOwner(), ePrivilegedPlayer))
		{	
			pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_LIVE_AMONG_NATIVES"));
			pDiplo->addDiploCommentVariable(pCity->getNameKey());
			pDiplo->addDiploCommentVariable(GC.getUnitInfo(eUnitType).getTextKeyWide());
			pDiplo->setData(getID());
			pDiplo->setCity(pCity->getIDInfo());
		}
		else // It means an other european player in a different team has a contract with the Native player. So the player cannot form units here
		{
			pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_LIVE_AMONG_NATIVES_REFUSED"));
			pDiplo->addDiploCommentVariable(GC.getUnitInfo(eUnitType).getDescription());
			pDiplo->addDiploCommentVariable(GET_PLAYER(ePrivilegedPlayer).getCivilizationAdjectiveKey());
		}
		pDiplo->setAIContact(true);
		gDLL->beginDiplomacy(pDiplo, getOwnerINLINE());
	}
	else
	{
		doLiveAmongNatives();
	}
}


void CvUnit::doLiveAmongNatives()
{
	if (!canLearn())
	{
		return;
	}

	unload();

	CvCity* pCity = plot()->getPlotCity();

	pCity->setTeachUnitMultiplier(pCity->getTeachUnitMultiplier() * (100 + GC.getDefineINT("NATIVE_TEACH_THRESHOLD_INCREASE")) / 100);
	int iLearnTime = getLearnTime();
	if (iLearnTime > 0)
	{
		setUnitTravelState(UNIT_TRAVEL_STATE_LIVE_AMONG_NATIVES, false);
		setUnitTravelTimer(iLearnTime);
	}
	else
	{
		doLearn();
	}
}

void CvUnit::doLearn()
{
	if (!canLearn())
	{
		return;
	}

	UnitTypes eUnitType = getLearnUnitType(plot());
	FAssert(eUnitType != NO_UNIT);
	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());
	CvCityJob* pJob = getCityJob();

	CvUnit* pLearnUnit = kPlayer.initUnit(eUnitType, getProfession(), getX_INLINE(), getY_INLINE(), AI_getUnitAIType());
	FAssert(pLearnUnit != NULL);
	pLearnUnit->joinGroup(getGroup());
	pLearnUnit->setNumMap(getNumMap());
	pLearnUnit->convert(this, true);
	pLearnUnit->AI_setUnitAIType(UNITAI_NATIVE_LEARN);
	if (pJob != NULL)
	{
		pJob->addUnit(pLearnUnit);
	}
	gDLL->getEventReporterIFace()->unitLearned(pLearnUnit->getOwnerINLINE(), pLearnUnit->getID());
}

UnitTypes CvUnit::getLearnUnitType(const CvPlot* pPlot) const
{
	if (getUnitInfo().getLearnTime() < 0)
	{
		return NO_UNIT;
	}

	if (pPlot == NULL)
	{
		return NO_UNIT;
	}

	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL)
	{
		return NO_UNIT;
	}

	if (pCity->getOwnerINLINE() == getOwnerINLINE())
	{
		return NO_UNIT;
	}

	UnitClassTypes eTeachUnitClass = pCity->getTeachUnitClass();
	if (eTeachUnitClass == NO_UNITCLASS)
	{
		return NO_UNIT;
	}

	UnitTypes eTeachUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eTeachUnitClass);
	if (eTeachUnit == getUnitType())
	{
		return NO_UNIT;
	}

	return eTeachUnit;
}

int CvUnit::getLearnTime() const
{
	CvCity* pCity = plot()->getPlotCity();
	if (pCity == NULL)
	{
		return MAX_INT;
	}

	int iLearnTime = m_pUnitInfo->getLearnTime() * pCity->getTeachUnitMultiplier() / 100;

	iLearnTime *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent();
	iLearnTime /= 100;

	for (int iTrait = 0; iTrait < GC.getNumTraitInfos(); ++iTrait)
	{
		TraitTypes eTrait = (TraitTypes) iTrait;
		if (GET_PLAYER(getOwnerINLINE()).hasTrait(eTrait) || GET_PLAYER(pCity->getOwnerINLINE()).hasTrait(eTrait))
		{
			iLearnTime *= 100 + GC.getTraitInfo(eTrait).getLearnTimeModifier();
			iLearnTime /= 100;
		}
	}

	return iLearnTime;
}


bool CvUnit::canKingTransport() const
{
	//DOANE: This should be in the first place instead at the end!
	if (!m_pUnitInfo->isTreasure())
	{
		return false;
	}
	//END DOANE

	PlayerTypes eParent = GET_PLAYER(getOwnerINLINE()).getParent();
	if (eParent == NO_PLAYER || !GET_PLAYER(eParent).isAlive() || ::atWar(getTeam(), GET_PLAYER(eParent).getTeam()))
	{
		return false;
	}

	if (!canMove())
	{
		return false;
	}

	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return false;
	}

	if (pPlot->getTeam() != getTeam())
	{
		return false;
	}

	//DOANE Treasure King Transport: the city needs a dock to transport the treasure (forbid "Abandoning City" cheat)
	CvCity* pCity = pPlot->getPlotCity();
	//Is a city?
	if (pCity == NULL)
	{
		return false;
	}

	//Is a coastal city?
	if (!pCity->isCoastal(GC.getMIN_WATER_SIZE_FOR_OCEAN()))
	{
		return false;
	}

	if (getYieldStored() == 0)
	{
		return false;
	}

	return true;
}

void CvUnit::kingTransport(bool bSkipPopup)
{
	if (!canKingTransport())
	{
		return;
	}

	if (isHuman() && !bSkipPopup)
	{
		CvDiploParameters* pDiplo = new CvDiploParameters(GET_PLAYER(getOwnerINLINE()).getParent());
		pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_TREASURE_TRANSPORT"));
		pDiplo->setData(getID());
		int iCommission = GC.getDefineINT("KING_TRANSPORT_TREASURE_COMISSION");
		pDiplo->addDiploCommentVariable(iCommission);
		int iAmount = getYieldStored();
		iAmount -= (iAmount * iCommission) / 100;
		iAmount -= (iAmount * GET_PLAYER(getOwnerINLINE()).getTaxRate()) / 100;
		pDiplo->addDiploCommentVariable(iAmount);
		pDiplo->setAIContact(true);
		gDLL->beginDiplomacy(pDiplo, getOwnerINLINE());
	}
	else
	{
		doKingTransport();
	}
}

void CvUnit::doKingTransport()
{
	GET_PLAYER(getOwnerINLINE()).sellYieldUnitToEurope(this, getYieldStored(), GC.getDefineINT("KING_TRANSPORT_TREASURE_COMISSION"), NO_YIELD);
}


bool CvUnit::canEstablishMission() const
{
	if (getProfession() == NO_PROFESSION)
	{
		return false;
	}

	if (GC.getProfessionInfo(getProfession()).getMissionaryRate() <= 0)
	{
		return false;
	}

	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return false;
	}

	CvCity* pCity = pPlot->getPlotCity();
	if (pCity == NULL)
	{
		return false;
	}

	CvPlayer& kCityOwner = GET_PLAYER(pCity->getOwnerINLINE());
	if (!kCityOwner.canHaveMission(getOwnerINLINE()))
	{
		return false;
	}

	if (pCity->getMissionaryCivilization() == getCivilizationType())
	{
		return false;
	}

	return true;
}

void CvUnit::establishMission()
{
	if (!canEstablishMission())
	{
		return;
	}

	CvCity* pCity = plot()->getPlotCity();

	if (GC.getGameINLINE().getSorenRandNum(100, "Mission failure roll") > getMissionarySuccessPercent())
	{
		CvWString szBuffer = gDLL->getText("TXT_KEY_MISSION_FAILED", plot()->getPlotCity()->getNameKey());
		gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_POSITIVE_DINK", MESSAGE_TYPE_MINOR_EVENT, GC.getCommandInfo(COMMAND_ESTABLISH_MISSION).getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_HIGHLIGHT_TEXT"), getX_INLINE(), getY_INLINE(), true, true);
		GET_PLAYER(pCity->getOwnerINLINE()).AI_changeMemoryCount((getOwnerINLINE()), MEMORY_MISSIONARY_FAIL, 1);
	}
	else
	{
		GET_PLAYER(getOwnerINLINE()).setMissionarySuccessPercent(GET_PLAYER(getOwnerINLINE()).getMissionarySuccessPercent() * GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getMissionFailureThresholdPercent() / 100);

		int iMissionaryRate = GC.getProfessionInfo(getProfession()).getMissionaryRate() * (100 + getUnitInfo().getMissionaryRateModifier()) / 100;
		if (!isHuman())
		{
			iMissionaryRate = (iMissionaryRate * 100 + 50) / GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIGrowthPercent();
		}
		pCity->setMissionaryPlayer(getOwnerINLINE());
		pCity->setMissionaryRate(iMissionaryRate);
	}

	kill(true);
}

int CvUnit::getMissionarySuccessPercent() const
{
	return GET_PLAYER(getOwnerINLINE()).getMissionarySuccessPercent() * (100 + (getUnitInfo().getMissionaryRateModifier() * GC.getDefineINT("MISSIONARY_RATE_EFFECT_ON_SUCCESS") / 100)) / 100;
}

bool CvUnit::canSpeakWithChief(CvPlot* pPlot) const
{
	ProfessionTypes eProfession = getProfession();
	if (m_pUnitInfo->isMechUnit()) 
	{
		if (!hasCrew())
		{
			return false;
		}
	} 
	else if (eProfession != PROFESSION_SCOUT) 
	{
		return false;
	}

	if (pPlot != NULL)
	{
		CvCity* pCity = pPlot->getPlotCity();
		if (pCity == NULL)
		{
			return false;
		}

		if (!pCity->isNative())
		{
			return false;
		}

		if (pCity->isScoutVisited(getTeam()))
		{
			return false;
		}
	}

	if (isNative())
	{
		return false;
	}

	return true;
}

void CvUnit::speakWithChief()
{
	if (!canSpeakWithChief(plot()))
	{
		return;
	}

	CvCity* pCity = plot()->getPlotCity();
	GoodyTypes eGoody = pCity->getGoodyType(this);
	PlayerTypes eNativePlayer = pCity->getOwnerINLINE();

	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvTeam& kTeam = GET_TEAM(kPlayer.getTeam());
	BonusTypes eNativeBonus = NO_BONUS;
	YieldTypes eProduceYield = pCity->getNativeYieldProduce();
	if (eProduceYield != NO_YIELD)
	{
		if (kPlayer.isHasYieldUnknown(eProduceYield))
		{
			kPlayer.setHasYieldUnknown(eProduceYield, false);
		}
		for (int iI = 0; iI < GC.getNumBonusInfos(); ++iI)
		{
			BonusTypes eBonus = (BonusTypes)iI;
			CvBonusInfo& kBonus = GC.getBonusInfo(eBonus);
			if (kBonus.isSeed())
			{
				if (kBonus.getYieldChange(eProduceYield) > 0 && kBonus.getRefBonus() != 5)
				{//Temp system avoid the wheat
					if (!kTeam.hasSeedling(eBonus))
					{
						kTeam.setHasSeedling(eBonus, true);
						eNativeBonus = eBonus;
						break;
					}
				}
			}
		}
	}


	if (isHuman() && !GET_PLAYER(eNativePlayer).isHuman())
	{
		CvWString szExpertText;
		int iGoodyValue = pCity->doGoody(this, eGoody);
		UnitClassTypes eTeachUnitClass = pCity->getTeachUnitClass();
		if (eTeachUnitClass != NO_UNITCLASS)
		{
			UnitTypes eTeachUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eTeachUnitClass);
			if (eTeachUnit != NO_UNIT)
			{
				szExpertText = gDLL->getText("AI_DIPLO_CHIEF_LEARN_UNIT_DESCRIPTION", GC.getUnitInfo(eTeachUnit).getTextKeyWide());
			}
		}

		CvWString szYieldText;
		if (eProduceYield != NO_YIELD)
		{
			szYieldText = gDLL->getText("AI_DIPLO_CHIEF_PRODUCED_YIELD_DESCRIPTION", GC.getYieldInfo(eProduceYield).getTextKeyWide());
		}

		CvWString szGoodyText;
		if (eGoody != NO_GOODY)
		{
			szGoodyText = gDLL->getText(GC.getGoodyInfo(eGoody).getChiefTextKey(), iGoodyValue);
		}
		CvDiploParameters* pDiplo = new CvDiploParameters(pCity->getOwnerINLINE());
		pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_CHIEF_GOODY"));
		pDiplo->addDiploCommentVariable(pCity->getNameKey());
		pDiplo->addDiploCommentVariable(szExpertText);
		pDiplo->addDiploCommentVariable(szYieldText);
		pDiplo->addDiploCommentVariable(szGoodyText);
		pDiplo->setData(eNativeBonus);
		pDiplo->setAIContact(true);
		pDiplo->setCity(pCity->getIDInfo());
		gDLL->beginDiplomacy(pDiplo, getOwnerINLINE());
	}
	else
	{
		pCity->doGoody(this, eGoody);
		AI_checkPotentialAgreement();
		if (eNativeBonus != NO_BONUS)
		{
			kPlayer.kingGiveAgronomist(eNativeBonus);
		}
	}
	if (m_pUnitInfo->isMechUnit())
	{
		changeMoves(GC.getMOVE_DENOMINATOR());
	}
}


bool CvUnit::canHold(const CvPlot* pPlot) const
{
	if (isHuman() && isAutomated() && IsSelected())
	{
		return false;
	}
	return true;
}


bool CvUnit::canSleep(const CvPlot* pPlot) const
{
	if (isAutomated()) 
	{
		return false;
	}
	if (isFortifyable())
	{
		return false;
	}

	if (isWaiting())
	{
		return false;
	}

	return true;
}

bool CvUnit::canAccessToAffectedTradeRoute() const
{
	if (!isAutomated()) 
	{
		return false;
	}

	if (getTradeRoute() == NULL) 
	{
		return false;
	}
	return true;
}


bool CvUnit::canFortify(const CvPlot* pPlot) const
{
	if (!isFortifyable())
	{
		return false;
	}

	if (isWaiting())
	{
		return false;
	}

	return true;
}


bool CvUnit::canHeal(const CvPlot* pPlot) const
{
	if (!isHurt())
	{
		return false;
	}

	if (isWaiting())
	{
		return false;
	}

	if (healRate(pPlot) <= 0)
	{
		return false;
	}
	if (isAutomated())
	{
		return false;
	}

	return true;
}


bool CvUnit::canSentry(const CvPlot* pPlot) const
{
	if (!canDefend(pPlot))
	{
		return false;
	}

	if (isWaiting())
	{
		return false;
	}

	return true;
}

int CvUnit::healRate(const CvPlot* pPlot) const
{
	PROFILE_FUNC();

	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pLoopUnit;
	CvPlot* pLoopPlot;
	int iHeal;
	int iBestHeal;
	int iI;
	int iTotalHeal = 0;

	//DOANE Pirate Units Heal Rate
	if (m_pUnitInfo->isPirate())
	{
		iTotalHeal = GC.getDefineINT("PIRATES_HEAL_RATE");
		return iTotalHeal;
	}
	//END DOANE

	//DOANE Heal Rate
	bool bIsInEurope = (getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE);
	if (!bIsInEurope)
	{
		if (pPlot->isCity(true, getTeam()))
		{
			iTotalHeal += GC.getDefineINT("CITY_HEAL_RATE") + (GET_TEAM(getTeam()).isFriendlyTerritory(pPlot->getTeam()) ? getExtraFriendlyHeal() : getExtraNeutralHeal());
			CvCity* pCity = pPlot->getPlotCity();
			if (pCity && !pCity->isOccupation())
			{
				if (m_pUnitInfo->isMechUnit())
				{
					iTotalHeal += pCity->getRepairShips();
				}
				else
				{
					iTotalHeal += pCity->getHealRate();
				}
			}
		}
		else
		{
			if (!GET_TEAM(getTeam()).isFriendlyTerritory(pPlot->getTeam()))
			{
				if (isEnemy(pPlot->getTeam(), pPlot))
				{
					iTotalHeal += (GC.getDefineINT("ENEMY_HEAL_RATE") + getExtraEnemyHeal());
				}
				else
				{
					iTotalHeal += (GC.getDefineINT("NEUTRAL_HEAL_RATE") + getExtraNeutralHeal());
				}
			}
			else
			{
				iTotalHeal += (GC.getDefineINT("FRIENDLY_HEAL_RATE") + getExtraFriendlyHeal());
			}
		}

		// XXX optimize this (save it?)
		iBestHeal = 0;

		pUnitNode = pPlot->headUnitNode();

		while (pUnitNode != NULL)
		{
			pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);

			if (pLoopUnit->getTeam() == getTeam()) // XXX what about alliances?
			{
				iHeal = pLoopUnit->getSameTileHeal();

				if (iHeal > iBestHeal && !m_pUnitInfo->isMechUnit())
				{
					iBestHeal = iHeal;
				}
			}
		}

		for (iI = 0; iI < NUM_DIRECTION_TYPES; iI++)
		{
			pLoopPlot = plotDirection(pPlot->getX_INLINE(), pPlot->getY_INLINE(), ((DirectionTypes)iI));

			if (pLoopPlot != NULL)
			{
				if (pLoopPlot->area() == pPlot->area())
				{
					pUnitNode = pLoopPlot->headUnitNode();

					while (pUnitNode != NULL)
					{
						pLoopUnit = ::getUnit(pUnitNode->m_data);
						pUnitNode = pLoopPlot->nextUnitNode(pUnitNode);

						if (pLoopUnit->getTeam() == getTeam()) // XXX what about alliances?
						{
							iHeal = pLoopUnit->getAdjacentTileHeal();

							if (iHeal > iBestHeal && !m_pUnitInfo->isMechUnit())
							{
								iBestHeal = iHeal;
							}
						}
					}
				}
			}
		}

		iTotalHeal += iBestHeal;
	}
	else
	{
		//DOANE Europe Heal Rate
		if (m_pUnitInfo->isMechUnit())
		{
			if (isInEuropeDrydock() || isAutomated())
			{ //Ship is being repaired in DryDock
				iTotalHeal = GC.getDefineINT("EUROPE_SHIP_IN_DRYDOCK_HEAL_RATE");
			}
			else
			{ //Standard heal rate, usually Zero so repairing is required
				iTotalHeal = GC.getDefineINT("EUROPE_SHIP_HEAL_RATE");
			}
		}
		else
		{ //land Units heal rate
			iTotalHeal = GC.getDefineINT("EUROPE_LAND_UNITS_HEAL_RATE");
		}
	}

	return iTotalHeal;
}


int CvUnit::healTurns(const CvPlot* pPlot) const
{
	int iHeal;
	int iTurns;

	if (!isHurt())
	{
		return 0;
	}

	iHeal = healRate(pPlot);

	if (iHeal > 0)
	{
		iTurns = (getDamage() / iHeal);

		if ((getDamage() % iHeal) != 0)
		{
			iTurns++;
		}

		return iTurns;
	}
	else
	{
		return MAX_INT;
	}
}


void CvUnit::doHeal()
{
	changeDamage(-(healRate(plot())));
}

void CvUnit::processHeal()
{
	CvSelectionGroup* pGroup = getGroup();
	if (hasMoved()) 
	{
		if (isAlwaysHeal())
		{
			doHeal();
		}
		if (pGroup != NULL && isAutomated() && pGroup->AI_shouldAutoHeal()) 
		{
			doHeal();
		}
	} 
	else if (isHurt()) 
	{
		doHeal();
	}

	if (isInEuropeDrydock() && !isHurt()) 
	{
		setInEuropeDrydock(false);
	}
}


CvCity* CvUnit::bombardTarget(const CvPlot* pPlot) const
{
	int iBestValue = MAX_INT;
	CvCity* pBestCity = NULL;

	for (int iI = 0; iI < NUM_DIRECTION_TYPES; iI++)
	{
		CvPlot* pLoopPlot = plotDirection(pPlot->getX_INLINE(), pPlot->getY_INLINE(), ((DirectionTypes)iI));

		if (pLoopPlot != NULL)
		{
			CvCity* pLoopCity = pLoopPlot->getPlotCity();

			if (pLoopCity != NULL)
			{
				if (pLoopCity->isBombardable(this))
				{
					int iValue = pLoopCity->getDefenseDamage();

					// always prefer cities we are at war with
					if (isEnemy(pLoopCity->getTeam(), pPlot))
					{
						iValue *= 128;
					}

					if (iValue < iBestValue)
					{
						iBestValue = iValue;
						pBestCity = pLoopCity;
					}
				}
			}
		}
	}

	return pBestCity;
}
void CvUnit::hurtByBombard(CvUnit* pAttacker, int iDamage)
{
	bool bDestroyUnit = false;
	switch(getUnitType())
	{
	case UNIT_WAGON_TRAIN:
	case UNIT_WAGON_TRAIN_ARMY:
	case UNIT_BUILDER_WAGON:
		bDestroyUnit = true;
		break;
	default:
		break;
	}
	if (bDestroyUnit)
	{
		//TODO Add text
		CvWString szBuffer = gDLL->getText("TXT_KEY_COMBAT_KILLED_BY_BOMBARD", getNameKey(), GET_PLAYER(pAttacker->getOwner()).getCivilizationAdjectiveKey());
		gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_ENEMY_TROOPS", MESSAGE_TYPE_INFO, getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), getX(), getY(), true, true);
		kill(true);
	}
	else
	{
		int iMinBombardUnitHealthPercent = GC.getDefineINT("MIN_BOMBARD_UNIT_HEALTH_PERCENT");
		int iMinBombardUnitHealth = maxHitPoints() * iMinBombardUnitHealthPercent/100;
		iDamage = std::min(currHitPoints() - iMinBombardUnitHealth, iDamage);//We can't kill the unit
		if (iDamage > 0)
		{
			changeDamage(iDamage);
			CvWString szBuffer = gDLL->getText("TXT_KEY_COMBAT_HURT_BY_BOMBARD", plot()->getPlotCity()->getNameKey(), GET_PLAYER(pAttacker->getOwner()).getCivilizationAdjectiveKey());
			gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_ENEMY_TROOPS", MESSAGE_TYPE_INFO, getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), getX(), getY(), true, true);
		}
	}
}

bool CvUnit::canBombard(int iPlotX, int iPlotY) const 
{
	if (iPlotX == -1 || iPlotY == -1)
	{
		return false;
	}

	CvPlot* pPlot = GC.getMapINLINE().plot(iPlotX, iPlotY);
	if (pPlot == NULL )
	{
		return false;
	}

	return canBombard(pPlot);
}

bool CvUnit::canBombard(const CvPlot* pPlot) const
{
	if (!isBombardMode(pPlot)) 
	{
		return false;
	} 

	if (isMadeAttack())
	{
		return false;
	}

	CvCity* pCity = pPlot->getPlotCity();

	if (pCity == NULL)
	{
		return false;
	}

	if (getMunition() == 0)
	{
		return false;
	}

	TeamTypes eTeam = pCity->getTeam();
	CvTeam& kTeam = GET_TEAM(getTeam());
	if (kTeam.isFirstTurnOfWar(eTeam))
	{
		return false;
	}

	if (!canCoexistWithEnemyUnit(NO_TEAM))
	{
		if (!isHuman() || pPlot->isVisible(getTeam(), false))
		{
			if (pPlot->isEnemyCity(*this) && !pPlot->isVisibleEnemyUnit(this))
			{
				return false;
			}
		}
	}

	return true;
}

int CvUnit::getAmmunitionPercentageForOneAttack(int iAmmunitionConsumption) const
{
	int iPercent = 100;
	int iCurrentAmmunitionAmount = getMunition();

	// Should never happened
	if (iAmmunitionConsumption == 0) 
	{
		return iPercent;
	}

	// AI is not concerned for the moment of ammunition penalties
	if (!isHuman()) 
	{
		return iPercent;
	}

	if (iCurrentAmmunitionAmount >= 0 && iCurrentAmmunitionAmount < iAmmunitionConsumption)
	{
		iPercent = iPercent * iCurrentAmmunitionAmount / iAmmunitionConsumption;
	}

	return iPercent;
}

int CvUnit::getBombardmentDamagePercent(ArtilleryPenaltyTypes eArtilleryPenalty) const
{
	int iBaseBombard = 8; // TODO use XML 
	int iAmmunitionConsumption = 4; // TODO use XML 

	int iPenalty = GC.getArtilleryPenaltyInfo(eArtilleryPenalty).getPenalty();
	int iBombardmentPercent = getBombardmentPercent();
	int iAmmunitionPercentageForOneAttack = getAmmunitionPercentageForOneAttack(iAmmunitionConsumption);

	// If the unit is in lack of ammunition, the damage percent will be reduced
	int iDamagePercent  = (iBaseBombard + iBombardmentPercent) * iAmmunitionPercentageForOneAttack / 100;

	// If the unit bombard in a protected area, penalties will be applied
	iDamagePercent -= iPenalty*iDamagePercent/100;

	return iDamagePercent;
}

int CvUnit::getMaxBombardmentDamagePercent(ArtilleryPenaltyTypes eArtilleryPenalty) const
{
	int iBasePrecision = 35;

	int iPenalty = GC.getArtilleryPenaltyInfo(eArtilleryPenalty).getPenalty();
	int iAccuracyPercent = getAccuracyPercent();

	int iMaxDamagePercent = iBasePrecision + iAccuracyPercent;

	// If the unit bombard in a protected area, penalties will be applied
	iMaxDamagePercent -= iMaxDamagePercent * iPenalty/100;

	return iMaxDamagePercent;
}

bool CvUnit::canHurtUnitWithBombardment(const CvUnit* pDefender, int iMaxBombardmentDamagePercent) const
{
	ArtilleryPenaltyTypes eArtilleryPenalty = getArtilleryPenalty(pDefender->plot());

	return 	pDefender->getDamage() < iMaxBombardmentDamagePercent;
}

bool CvUnit::bombard(int iPlotX, int iPlotY)
{
	CvPlot* pPlot = plot();
	CvPlot* pTargetPlot =  GC.getMapINLINE().plot(iPlotX, iPlotY);
	if (!canBombard(pTargetPlot))
	{
		return false;
	}

	CvCity* pBombardCity = pTargetPlot->getPlotCity();
	FAssertMsg(pBombardCity != NULL, "BombardCity is not assigned a valid value");

	if (!isEnemy(pTargetPlot->getTeam()))
	{
		getGroup()->groupDeclareWar(pTargetPlot, true);
	}

	if (!isEnemy(pTargetPlot->getTeam()))
	{
		return false;
	}
	ArtilleryPenaltyTypes eArtilleryPenalty = getArtilleryPenalty(pTargetPlot);
	int iPenalty =  GC.getArtilleryPenaltyInfo(eArtilleryPenalty).getPenalty();
	int iBombard = bombardRate();
	int iConsom;

	if (isArtilleryMen())
	{
		iBombard = GC.getProfessionInfo(getProfession()).getBombardRate();
		iBombard += iBombard*getBombardmentPercent()/100;
		iBombard += iBombard*getExtraBombardRate()/100;
		iBombard /= 2;//We decrease artillery power 
		iConsom = getConsommationMunition(false);
	} 
	else 
	{
		iConsom = getConsommationMunition(true);
	}

	int iBaseExp = getExperienceForBombardment(eArtilleryPenalty);//In fact we use now this penalty for everyone even 

	if ( iConsom > 0)
	{
		int iRealConsom = std::min(iConsom, getMunition());
		changeMunition(-iRealConsom);
		iBombard = iBombard*iRealConsom/iConsom;
	}

	iBombard -= iBombard*iPenalty/100;
	int iMinBombardDefensePercent = GC.getDefineINT("MIN_BOMBARD_DEFENSE_PERCENT");
	int iBuildingBombardDefense = pBombardCity->getBuildingDefense();
	int iMinBombardDefense = iBuildingBombardDefense*iMinBombardDefensePercent/100;
	int iDefenceModifier = pBombardCity->getDefenseModifier();
	int iDefenceDamage = std::min(iBombard, iDefenceModifier - iMinBombardDefense);

	if (iBombard > 0)
	{
		if (iDefenceDamage > 0) 
		{
			pBombardCity->changeDefenseModifier(-iDefenceDamage);

			if (pBombardCity->getDefenseModifier() < GC.getFORTIFICATION_BREAK_DAMAGE() && pBombardCity->getDefenseModifier() + iDefenceDamage >= GC.getFORTIFICATION_BREAK_DAMAGE()) 
			{
				pBombardCity->changeDefenseModifier(-std::max(0, (pBombardCity->getDefenseModifier() - 3)));
				CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_OPEN_BREACH_FORTIFICATION);
				gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
			}
		}

		CvUnit* pBombardedUnit = pBombardCity->getRandomUnitOnGarnison(iPenalty);
		if (pBombardedUnit != NULL) 
		{
			pBombardedUnit->hurtByBombard(this, iBombard);		
		}

		changeExperience(iBaseExp);

		if (iDefenceDamage > 0) 
		{			
			CvWString szBuffer = gDLL->getText("TXT_KEY_MISC_DEFENSES_IN_CITY_REDUCED_TO", pBombardCity->getNameKey(), pBombardCity->getDefenseModifier(), GET_PLAYER(getOwnerINLINE()).getNameKey());
			gDLL->getInterfaceIFace()->addMessage(pBombardCity->getOwnerINLINE(), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_BOMBARDED", MESSAGE_TYPE_INFO, getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pBombardCity->getX_INLINE(), pBombardCity->getY_INLINE(), true, true);

			szBuffer = gDLL->getText("TXT_KEY_MISC_YOU_REDUCE_CITY_DEFENSES", getNameOrProfessionKey(), pBombardCity->getNameKey(), pBombardCity->getDefenseModifier());
			gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_BOMBARD", MESSAGE_TYPE_INFO, getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pBombardCity->getX_INLINE(), pBombardCity->getY_INLINE());
		}
	}
	setMadeAttack(true);
	changeMoves(GC.getMOVE_DENOMINATOR());

	if (pPlot->isActiveVisible(false))
	{
		// Bombard entity mission
		CvMissionDefinition kDefiniton;
		kDefiniton.setMissionTime(GC.getMissionInfo(MISSION_BOMBARD).getTime() * gDLL->getSecsPerTurn());
		kDefiniton.setMissionType(MISSION_BOMBARD);
		kDefiniton.setPlot(pBombardCity->plot());
		kDefiniton.setUnit(BATTLE_UNIT_ATTACKER, this);
		kDefiniton.setUnit(BATTLE_UNIT_DEFENDER, NULL);
		gDLL->getEntityIFace()->AddMission(&kDefiniton);
	}

	return true;
}

bool CvUnit::canPillage(const CvPlot* pPlot) const
{
	if (!canAttack())
	{
		return false;
	}

	if (isBadlyHurt()) 
	{
		return false;
	}

	if (pPlot->isCity())
	{
		return false;
	}

	if (pPlot->getImprovementType() == NO_IMPROVEMENT)
	{
		if (!(pPlot->isRoute()))
		{
			return false;
		}
	}
	else
	{
		if (GC.getImprovementInfo(pPlot->getImprovementType()).isPermanent())
		{
			return false;
		}
	}

	if (pPlot->isOwned())
	{
		if (!potentialWarAction(pPlot))
		{
			if ((pPlot->getImprovementType() == NO_IMPROVEMENT) || (pPlot->getOwnerINLINE() != getOwnerINLINE()))
			{
				return false;
			}
		} 
		else
		{
			TeamTypes eTeam = pPlot->getTeam();
			CvTeam& kTeam = GET_TEAM(getTeam());
			if (kTeam.isFirstTurnOfWar(eTeam))
			{
				return false;
			}
		}
	}

	if (!(pPlot->isValidDomainForAction(*this)))
	{
		return false;
	}

	return true;
}


bool CvUnit::pillage()
{
	CvWString szBuffer;
	int iPillageGold;
	ImprovementTypes eTempImprovement = NO_IMPROVEMENT;
	RouteTypes eTempRoute = NO_ROUTE;

	CvPlot* pPlot = plot();

	if (!canPillage(pPlot))
	{
		return false;
	}

	if (pPlot->isOwned())
	{
		// we should not be calling this without declaring war first, so do not declare war here
		if (!isEnemy(pPlot->getTeam(), pPlot))
		{
			if ((pPlot->getImprovementType() == NO_IMPROVEMENT) || (pPlot->getOwnerINLINE() != getOwnerINLINE()))
			{
				return false;
			}
		}
	}

	if (pPlot->getImprovementType() != NO_IMPROVEMENT)
	{
		eTempImprovement = pPlot->getImprovementType();

		if (pPlot->getTeam() != getTeam())
		{
			// Use python to determine pillage amounts...
			iPillageGold = 0;

			//DOANE Python Callbacks | Speed Improvement: This is used by Doane to determine pillage gold, so we want this to be used...
			if (GC.getUSE_ON_DO_PILLAGE_GOLD_CALLBACK())
			{
				long lPillageGold(0);
				CyPlot* pyPlot = new CyPlot(pPlot);
				CyUnit* pyUnit = new CyUnit(this);

				CyArgsList argsList;
				argsList.add(gDLL->getPythonIFace()->makePythonObject(pyPlot));	// pass in plot class
				argsList.add(gDLL->getPythonIFace()->makePythonObject(pyUnit));	// pass in unit class

				gDLL->getPythonIFace()->callFunction(PYGameModule, "doPillageGold", argsList.makeFunctionArgs(),&lPillageGold); //Controls the gold result of pillaging

				delete pyPlot;	// python fxn must not hold on to this pointer
				delete pyUnit;	// python fxn must not hold on to this pointer

				iPillageGold = (int)lPillageGold;
			}
			//END DOANE

			if (iPillageGold > 0) //Removed factor x10 since it is applied directly to XML file.
			{
				GET_PLAYER(getOwnerINLINE()).changeGold(iPillageGold);

				szBuffer = gDLL->getText("TXT_KEY_MISC_PLUNDERED_GOLD_FROM_IMP", iPillageGold, GC.getImprovementInfo(pPlot->getImprovementType()).getTextKeyWide());
				gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_PILLAGE", MESSAGE_TYPE_INFO, getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pPlot->getX_INLINE(), pPlot->getY_INLINE());

				if (pPlot->isOwned() && pPlot->getImprovementLevel() == 0)
				{
					szBuffer = gDLL->getText("TXT_KEY_MISC_IMP_DESTROYED", GC.getImprovementInfo(pPlot->getImprovementType()).getTextKeyWide(), getNameOrProfessionKey(), getVisualCivAdjective(pPlot->getTeam()));
					gDLL->getInterfaceIFace()->addMessage(pPlot->getOwnerINLINE(), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_PILLAGED", MESSAGE_TYPE_INFO, getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE(), true, true);
				}
			}
		}

		if (pPlot->getImprovementLevel() > 0)
		{
			pPlot->changeImprovementLevel(-1);
			if (pPlot->getImprovementLevel() == 0)
			{
				pPlot->setImprovementType((ImprovementTypes)(GC.getImprovementInfo(pPlot->getImprovementType()).getImprovementPillage()));
				if (pPlot->getBonusType() != NO_BONUS)
				{
					pPlot->setBonusType(NO_BONUS); //TODO give the player a wagon with some units of that bonus. That wagon would die while unloading that cargo.
				}
			}
			else
			{
				if (pPlot->isOwned())
				{
					szBuffer = gDLL->getText("TXT_KEY_MISC_IMP_DESTROYED_FARM_LEVEL", GC.getImprovementInfo(pPlot->getImprovementType()).getTextKeyWide(), getNameOrProfessionKey(), getVisualCivAdjective(pPlot->getTeam()));
					gDLL->getInterfaceIFace()->addMessage(pPlot->getOwnerINLINE(), false, GC.getEVENT_MESSAGE_TIME(), szBuffer, "AS2D_PILLAGED", MESSAGE_TYPE_INFO, getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pPlot->getX_INLINE(), pPlot->getY_INLINE(), true, true);
				}
			}
		}
		else
		{
			pPlot->setImprovementType((ImprovementTypes)(GC.getImprovementInfo(pPlot->getImprovementType()).getImprovementPillage()));
		}
	}
	else if (pPlot->isRoute()) //TODO doPillageRoute
	{
		eTempRoute = pPlot->getRouteType();
		pPlot->setRouteType(NO_ROUTE); // XXX downgrade rail???
	}

	finishMoves();

	if (pPlot->isActiveVisible(false))
	{
		// Pillage entity mission
		CvMissionDefinition kDefiniton;
		kDefiniton.setMissionTime(GC.getMissionInfo(MISSION_PILLAGE).getTime() * gDLL->getSecsPerTurn());
		kDefiniton.setMissionType(MISSION_PILLAGE);
		kDefiniton.setPlot(pPlot);
		kDefiniton.setUnit(BATTLE_UNIT_ATTACKER, this);
		kDefiniton.setUnit(BATTLE_UNIT_DEFENDER, NULL);
		gDLL->getEntityIFace()->AddMission(&kDefiniton);
	}

	if (eTempImprovement != NO_IMPROVEMENT || eTempRoute != NO_ROUTE)
	{
		gDLL->getEventReporterIFace()->unitPillage(this, eTempImprovement, eTempRoute, getOwnerINLINE());
	}

	return true;
}

bool CvUnit::isHaveFoundPack() const
{
	return m_bHaveFoundPack;
}

void CvUnit::setHaveFoundPack(bool bChange)
{
	if (isHaveFoundPack() != bChange)
	{
		m_bHaveFoundPack = bChange;
	}
}

bool CvUnit::isHaveNewMilitaryProfession() const
{
	return m_bHaveNewMilitaryProfession;
}

void CvUnit::setHaveNewMilitaryProfession(bool bChange)
{
	if (isHaveNewMilitaryProfession() != bChange)
	{
		m_bHaveNewMilitaryProfession = bChange;
	}
}

bool CvUnit::canFound(const CvPlot* pPlot, bool bTestVisible, bool bIgnoreProfession) const
{
	if (!m_pUnitInfo->isFound())
	{
		return false;
	}

	if (pPlot != NULL)
	{
		if (pPlot->isCity())
		{
			return false;
		}

		if (!(GET_PLAYER(getOwnerINLINE()).canFound(pPlot->getX_INLINE(), pPlot->getY_INLINE(), bTestVisible)))
		{
			return false;
		}
		if (!bIgnoreProfession)
		{
			if (getProfession() != NO_PROFESSION)
			{
				if (GC.getProfessionInfo(getProfession()).canFound())
				{ //Builder wagon
					return true;
				}
				//If it is not the  builder wagon, the colony must be coastal
				if (!pPlot->isCoastalLand())
				{
					return false;
				}
				if (pPlot->haveFoundPackInShip(getOwnerINLINE()))
				{ // Ship with found pack or IA...
					return true;
				}
				return false;
			}
		}
	}
	//A unit with  job mission cannot found
	if (getCityJobID() >= 0)
	{
		return false;
	}

	return true;
}


bool CvUnit::found()
{
	if (!canFound(plot()))
	{
		return false;
	}

	CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());

	PlayerTypes eParent = kPlayer.getParent();
	if (eParent != NO_PLAYER && !GC.getEraInfo(kPlayer.getCurrentEra()).isRevolution() && !isAutomated())
	{
		int iFoodDifference = plot()->calculateNatureYield(YIELD_FOOD, getTeam(), true) - GC.getFOOD_CONSUMPTION_PER_POPULATION();
		bool bInland = !plot()->isCoastalLand(GC.getDefineINT("MIN_WATER_SIZE_FOR_OCEAN"));

		DiploCommentTypes eDiploComment = NO_DIPLOCOMMENT;
		if (iFoodDifference < 0 && kPlayer.shouldDisplayFeatPopup(FEAT_CITY_NO_FOOD))
		{
			eDiploComment = (DiploCommentTypes) GC.getInfoTypeForString("AI_DIPLOCOMMENT_FOUND_CITY_NO_FOOD");
			kPlayer.setFeatAccomplished(FEAT_CITY_NO_FOOD, true);
		}
		else if (bInland && kPlayer.shouldDisplayFeatPopup(FEAT_CITY_INLAND))
		{
			eDiploComment = (DiploCommentTypes) GC.getInfoTypeForString("AI_DIPLOCOMMENT_FOUND_CITY_INLAND");
			kPlayer.setFeatAccomplished(FEAT_CITY_INLAND, true);
		}

		if (eDiploComment != NO_DIPLOCOMMENT)
		{
			CvDiploParameters* pDiplo = new CvDiploParameters(eParent);
			pDiplo->setDiploComment(eDiploComment);
			pDiplo->setData(getID());
			pDiplo->setAIContact(true);
			gDLL->beginDiplomacy(pDiplo, getOwnerINLINE());
			return true;
		}
	}

	return doFoundCheckNatives();
}

bool CvUnit::doFoundCheckNatives()
{
	if (!canFound(plot()))
	{
		return false;
	}

	if (isHuman() && !isAutomated())
	{
		PlayerTypes eNativeOwner = NO_PLAYER;
		int iCost = 0;
		for (int i = 0; i < NUM_CITY_PLOTS; ++i)
		{
			CvPlot* pLoopPlot = ::plotCity(getX_INLINE(), getY_INLINE(), i);
			if (pLoopPlot != NULL)
			{
				if (pLoopPlot->isOwned() && !pLoopPlot->isCity())
				{
					if (GET_PLAYER(pLoopPlot->getOwnerINLINE()).isNative() && !GET_TEAM(pLoopPlot->getTeam()).isAtWar(getTeam()))
					{
						eNativeOwner = pLoopPlot->getOwnerINLINE();
						iCost += pLoopPlot->getBuyPrice(getOwnerINLINE());
					}
				}
			}
		}

		if (eNativeOwner != NO_PLAYER)
		{
			GET_TEAM(getTeam()).meet(GET_PLAYER(eNativeOwner).getTeam(), false);
		}

		if (eNativeOwner != NO_PLAYER && !GET_PLAYER(eNativeOwner).isHuman())
		{
			CvDiploParameters* pDiplo = new CvDiploParameters(eNativeOwner);
			if (GET_PLAYER(getOwnerINLINE()).getNumCities() == 0)
			{
				pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_FOUND_FIRST_CITY"));
			}
			else if (GET_PLAYER(getOwnerINLINE()).getNumCities() == 1)
			{
				pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_FOUND_CITY_FREE"));
				pDiplo->addDiploCommentVariable(iCost);
			}
			else if (iCost > GET_PLAYER(getOwnerINLINE()).getGold())
			{
				pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_FOUND_CITY_CANT_AFFORD"));
				pDiplo->addDiploCommentVariable(iCost);
			}
			else
			{
				pDiplo->setDiploComment((DiploCommentTypes)GC.getInfoTypeForString("AI_DIPLOCOMMENT_FOUND_CITY"));
				pDiplo->addDiploCommentVariable(iCost);
			}
			pDiplo->setData(getID());
			pDiplo->setAIContact(true);
			gDLL->beginDiplomacy(pDiplo, getOwnerINLINE());
		}
		else
		{
			doFound(false);
		}
	}
	else
	{
		AI_doFound();
	}

	return true;
}

bool CvUnit::doFound(bool bBuyLand)
{
	if (!canFound(plot()))
	{
		return false;
	}

	if (GC.getGameINLINE().getActivePlayer() == getOwnerINLINE())
	{
		gDLL->getInterfaceIFace()->lookAt(plot()->getPoint(), CAMERALOOKAT_NORMAL);
	}

	//first city takes land for free
	bool bIsFirstCity = (GET_PLAYER(getOwnerINLINE()).getNumCities() == 0) || (GET_PLAYER(getOwnerINLINE()).getNumCities() == 1);
	if (bBuyLand || bIsFirstCity)
	{
		for (int i = 0; i < NUM_CITY_PLOTS; ++i)
		{
			CvPlot* pLoopPlot = ::plotCity(getX_INLINE(), getY_INLINE(), i);
			if (pLoopPlot != NULL)
			{
				if (pLoopPlot->isOwned() && !pLoopPlot->isCity())
				{
					//don't buy land if at war, it will be taken
					if (GET_PLAYER(pLoopPlot->getOwnerINLINE()).isNative() && !GET_TEAM(pLoopPlot->getTeam()).isAtWar(getTeam()))
					{
						GET_PLAYER(getOwnerINLINE()).buyLand(pLoopPlot, bIsFirstCity);
					}
				}
			}
		}
	}
	CvPlayerAI& kPlayer = GET_PLAYER(getOwnerINLINE());
	CvTeam& kTeam = GET_TEAM(kPlayer.getTeam());
	bool bDisplayHelp = kPlayer.isHuman() && kPlayer.haveAlreadySawSeeds();

	kPlayer.found(getX_INLINE(), getY_INLINE());

	CvPlot* pCityPlot = GC.getMapINLINE().plotINLINE(getX_INLINE(), getY_INLINE());
	FAssert(NULL != pCityPlot);
	if (pCityPlot != NULL)
	{
		if (pCityPlot->isActiveVisible(false))
		{
			NotifyEntity(MISSION_FOUND);
			EffectTypes eEffect = (EffectTypes)GC.getInfoTypeForString("EFFECT_SETTLERSMOKE");
			gDLL->getEngineIFace()->TriggerEffect(eEffect, pCityPlot->getPoint(), (float)(GC.getASyncRand().get(360)));
			gDLL->getInterfaceIFace()->playGeneralSound("AS3D_UN_FOUND_CITY", pCityPlot->getPoint());
		}

		CvCity* pCity = pCityPlot->getPlotCity();
		FAssert(NULL != pCity);
		if (pCity != NULL)
		{
			//start
			pCity->setYieldStored(YIELD_FOOD, 25);
			if (!isNative())
			{
				pCityPlot->deleteFoundPackInShip(getOwnerINLINE());
			}
			//end
			if (getUnitType() != UNIT_BUILDER_WAGON)
			{
				pCity->addPopulationUnit(this, NO_PROFESSION);
			}
			else
			{
				kill(false);
			}			
			if (bDisplayHelp)
			{
				int iBestValue = 0;
				BonusTypes eIdealBonus = NO_BONUS;
				for (int iI = 0; iI < GC.getNumBonusInfos(); ++iI)
				{
					BonusTypes eBonus = (BonusTypes)iI;
					CvBonusInfo& kBonus = GC.getBonusInfo(eBonus);
					if (kBonus.isSeed() && kTeam.hasSeedling(eBonus))
					{
						int iValue = pCity->getBonusLandsValue(eBonus);
						if (iValue > iBestValue)
						{
							iBestValue = iValue;
							eIdealBonus = eBonus;
						}
					}
				}
				if (eIdealBonus != NO_BONUS )
				{
					YieldTypes eMainYield = (YieldTypes)GC.getBonusInfo(eIdealBonus).getMainYield();
					if (kPlayer.getNumCities() > 1 || (eMainYield != NO_YIELD && GC.getYieldInfo(eMainYield).isNewWorld()))
					{
						if (kPlayer.isHuman())
						{
							if (GC.getGameINLINE().getActivePlayer() == kPlayer.getID())
							{
								CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_SEEDLING_DISCOVERY, eIdealBonus, -1, pCity->getID());
								gDLL->getInterfaceIFace()->addPopup(pInfo, NO_PLAYER);
							}
						}
						else
						{
							kPlayer.kingGiveAgronomist(eIdealBonus);
						}
					}
				}
			}
		}
	}

	return true;
}

bool CvUnit::canJoinCity(const CvPlot* pPlot, bool bTestVisible) const
{
	CvCity* pCity = pPlot->getPlotCity();

	if (pCity == NULL)
	{
		return false;
	}

	if (pCity->getOwnerINLINE() != getOwnerINLINE())
	{
		return false;
	}

	if (pCity->isDisorder())
	{
		return false;
	}

	if (!m_pUnitInfo->isFound())
	{
		return false;
	}

	if (m_pUnitInfo->isColonBuild())
	{
		return false;
	}

	if (isDelayedDeath())
	{
		return false;
	}

	if (!bTestVisible)
	{
		if (pCity->getRawYieldProduced(YIELD_FOOD) < pCity->getPopulation() * GC.getFOOD_CONSUMPTION_PER_POPULATION())
		{
			if (!canJoinStarvingCity(*pCity))
			{
				return false;
			}
		}
	}
	return true;
}

bool CvUnit::canJoinStarvingCity(const CvCity& kCity) const
{
	if (kCity.getYieldStored(YIELD_FOOD) >= GC.getGameINLINE().getCargoYieldCapacity() / 4)
	{
		return true;
	}

	int iNewPop = kCity.getPopulation() + 1;
	if (kCity.AI_getFoodGatherable(iNewPop, 0) >= iNewPop * GC.getFOOD_CONSUMPTION_PER_POPULATION())
	{
		return true;
	}

	if (kCity.getYieldStored(YIELD_FOOD) > 0)
	{
		return true;
	}

	if (!isHuman())
	{
		ProfessionTypes eProfession = AI_getIdealProfession();
		if (eProfession != NO_PROFESSION)
		{
			if (GC.getProfessionInfo(eProfession).getYieldProduced() == YIELD_FOOD)
			{
				return true;
			}
		}
	}

	return false;
}

bool CvUnit::joinCity()
{
	if (!canJoinCity(plot()))
	{
		return false;
	}

	if (plot()->isActiveVisible(false))
	{
		NotifyEntity(MISSION_JOIN_CITY);
	}

	CvCity* pCity = plot()->getPlotCity();

	if (pCity != NULL)
	{
		pCity->addPopulationUnit(this, NO_PROFESSION);
	}

	return true;
}

bool CvUnit::canBuild(const CvPlot* pPlot, BuildTypes eBuild, bool bTestVisible) const
{
	FAssertMsg(eBuild < GC.getNumBuildInfos(), "Index out of bounds");

	if (!(m_pUnitInfo->getBuilds(eBuild)))
	{
		return false;
	}

	if (workRate(true) <= 0)
	{
		return false;
	}

	if (!(GET_PLAYER(getOwnerINLINE()).canBuild(pPlot, eBuild, false, bTestVisible)))
	{
		return false;
	}

	if (!pPlot->isValidDomainForAction(*this))
	{
		return false;
	}

	if (!bTestVisible)
	{
		if ((FeatureTypes)GC.getBuildInfo(eBuild).getFeatureType() != NO_FEATURE)
		{ //Only for plant trees
			CLLNode<IDInfo>* pUnitNode;
			CvUnit* pLoopUnit;
			pUnitNode = pPlot->headUnitNode();
			while (pUnitNode != NULL)
			{
				pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pPlot->nextUnitNode(pUnitNode);
				if (pLoopUnit != this && pLoopUnit->getBuildType() == eBuild)
				{//Warning with MISSION_ROUTE_TO -> stack overflow
					return false;
				}
			}
		}
	}

	return true;
}

// Returns true if build finished...
bool CvUnit::build(BuildTypes eBuild)
{
	bool bFinished;

	FAssertMsg(eBuild < GC.getNumBuildInfos(), "Invalid Build");

	if (!canBuild(plot(), eBuild))
	{
		return false;
	}

	// Note: notify entity must come before changeBuildProgress - because once the unit is done building,
	// that function will notify the entity to stop building.
	NotifyEntity((MissionTypes)GC.getBuildInfo(eBuild).getMissionType());

	GET_PLAYER(getOwnerINLINE()).changeGold(-(GET_PLAYER(getOwnerINLINE()).getBuildCost(plot(), eBuild)));

	bFinished = plot()->changeBuildProgress(eBuild, workRate(false), getTeam());

	finishMoves(); // needs to be at bottom because movesLeft() can affect workRate()...

	if (bFinished)
	{
		if (GC.getBuildInfo(eBuild).isKill())
		{
			kill(true);
		}
	}

	// Python Event
	gDLL->getEventReporterIFace()->unitBuildImprovement(this, eBuild, bFinished);

	return bFinished;
}


bool CvUnit::canPromote(PromotionTypes ePromotion, int iLeaderUnitId, bool bTestPromotionReady) const
{
	if (ePromotion == NO_PROMOTION)
	{
		return false;
	}

	if (iLeaderUnitId >= 0)
	{
		if (iLeaderUnitId == getID())
		{
			return false;
		}

		if (isArtilleryMen()) 
		{
			return false;
		}

		if (!GC.getPromotionInfo(ePromotion).isLeader())
		{
			return false;
		}

		CvUnit* pWarlord = GET_PLAYER(getOwnerINLINE()).getUnit(iLeaderUnitId);
		if (pWarlord == NULL)
		{
			return false;
		}

		if (pWarlord->getUnitInfo().getLeaderPromotion() != ePromotion)
		{
			return false;
		}
		DomainTypes eDomainType = DOMAIN_LAND;
		if (pWarlord->getUnitType() == UNIT_GREAT_ADMIRAL)
		{
			eDomainType = DOMAIN_SEA;
		}

		ProfessionTypes eProfession = getProfession();

		if (getDomainType() != eDomainType && eProfession != PROFESSION_SAILOR)
		{
			return false;
		}

		if (eDomainType == DOMAIN_LAND) 
		{
			if (eProfession == NO_PROFESSION || eProfession == PROFESSION_SCOUT) 
			{
				return false;
			}

			if (GC.getProfessionInfo(eProfession).getCombatChange() == 0) 
			{
				return false;
			}

			if (GC.getProfessionInfo(eProfession).isUnarmed())
			{
				return false;
			}
		}
		else
		{
			if (isOnlyDefensive())
			{
				return false;
			}
		}

		if (!canAcquirePromotion(ePromotion))
		{
			return false;
		}

		if (!canAcquirePromotionAny())
		{
			return false;
		}
	}
	else
	{
		if (GC.getPromotionInfo(ePromotion).isLeader())
		{
			return false;
		}

		if (!canAcquirePromotion(ePromotion))
		{
			return false;
		}

		if (bTestPromotionReady) 
		{
			if (!isPromotionReady())
			{
				return false;
			}
		}
	}

	if (GC.getPromotionInfo(ePromotion).isExploAndNavigation())
	{
		return false;
	}

	if (GC.getPromotionInfo(ePromotion).isOnlyFreePromo())
	{
		return false;
	}

	if (m_pUnitInfo->isMechUnit())
	{
		if (!hasCrew())
		{
			return false;
		}
	}
	return true;
}

void CvUnit::promote(PromotionTypes ePromotion, int iLeaderUnitId)
{
	if (!canPromote(ePromotion, iLeaderUnitId))
	{
		return;
	}

	if (iLeaderUnitId >= 0)
	{
		CvUnit* pWarlord = GET_PLAYER(getOwnerINLINE()).getUnit(iLeaderUnitId);
		if (pWarlord)
		{
			//pWarlord->giveExperience();
			if (!isHuman())
			{
				changeExperience(20);
			}
			if (!m_pUnitInfo->isMechUnit())
			{
				if (!pWarlord->getNameNoDesc().empty())
				{
					setName(pWarlord->getNameKey());
				}
			}

			//update graphics models
			m_eLeaderUnitType = pWarlord->getUnitType();
			reloadEntity();
		}
	}

	if (!GC.getPromotionInfo(ePromotion).isLeader())
	{
		changeLevel(1);
	}

	setHasRealPromotion(ePromotion, true);

	testPromotionReady();

	if (IsSelected())
	{
		gDLL->getInterfaceIFace()->playGeneralSound(GC.getPromotionInfo(ePromotion).getSound());

		gDLL->getInterfaceIFace()->setDirty(UnitInfo_DIRTY_BIT, true);
	}
	else
	{
		setInfoBarDirty(true);
	}

	if (getUnitTravelState() == UNIT_TRAVEL_STATE_IN_EUROPE) 
	{
		gDLL->getInterfaceIFace()->setDirty(EuropeC1Screen_DIRTY_BIT, true);
	}

	gDLL->getEventReporterIFace()->unitPromoted(this, ePromotion);
}

bool CvUnit::lead(int iUnitId)
{
	if (!canLead(plot(), iUnitId))
	{
		return false;
	}

	PromotionTypes eLeaderPromotion = (PromotionTypes)m_pUnitInfo->getLeaderPromotion();

	if (iUnitId == -1)
	{
		CvPopupInfo* pInfo = new CvPopupInfo(BUTTONPOPUP_LEADUNITEXP, getID(), GC.getDefineINT("GREAT_GENERALS_DISTRIBUTED_POINTS"));
		gDLL->getInterfaceIFace()->addPopup(pInfo, getOwnerINLINE(), true);
		return false;
	}
	else
	{
		CvUnit* pUnit = GET_PLAYER(getOwnerINLINE()).getUnit(iUnitId);
		if (!pUnit || !pUnit->canPromote(eLeaderPromotion, getID()))
		{
			return false;
		}

		pUnit->promote(eLeaderPromotion, getID());

		if (plot()->isActiveVisible(false))
		{
			NotifyEntity(MISSION_LEAD);
		}

		// Have to be done done twice in case of kill unit update the current unit
		pUnit->SetSelected(true);

		kill(true);
		
		pUnit->SetSelected(true);

		return true;
	}
}

int CvUnit::canLead(const CvPlot* pPlot, int iUnitId) const
{
	PROFILE_FUNC();

	if (isDelayedDeath())
	{
		return 0;
	}

	if (NO_UNIT == getUnitType())
	{
		return 0;
	}

	int iNumUnits = 0;
	CvUnitInfo& kUnitInfo = getUnitInfo();

	if (-1 == iUnitId)
	{
		CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
		while(pUnitNode != NULL)
		{
			CvUnit* pUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);

			if (pUnit && pUnit != this && pUnit->getOwnerINLINE() == getOwnerINLINE() && pUnit->canPromote((PromotionTypes)kUnitInfo.getLeaderPromotion(), getID()))
			{
				++iNumUnits;
			}
		}
	}
	else
	{
		CvUnit* pUnit = GET_PLAYER(getOwnerINLINE()).getUnit(iUnitId);
		if (pUnit && pUnit != this && pUnit->canPromote((PromotionTypes)kUnitInfo.getLeaderPromotion(), getID()))
		{
			iNumUnits = 1;
		}
	}
	return iNumUnits;
}


int CvUnit::canGiveExperience(const CvPlot* pPlot) const
{
	int iNumUnits = 0;

	if (NO_UNIT != getUnitType() && m_pUnitInfo->getLeaderExperience() > 0)
	{
		CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
		while(pUnitNode != NULL)
		{
			CvUnit* pUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);

			if (pUnit && pUnit != this && pUnit->getOwnerINLINE() == getOwnerINLINE() && pUnit->canAcquirePromotionAny())
			{
				++iNumUnits;
			}
		}
	}

	return iNumUnits;
}

bool CvUnit::giveExperience()
{
	CvPlot* pPlot = plot();

	if (pPlot)
	{
		int iNumUnits = canGiveExperience(pPlot);
		if (iNumUnits > 0)
		{
			int iTotalExperience = getStackExperienceToGive(iNumUnits);

			int iMinExperiencePerUnit = iTotalExperience / iNumUnits;
			int iRemainder = iTotalExperience % iNumUnits;

			CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();
			int i = 0;
			while(pUnitNode != NULL)
			{
				CvUnit* pUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pPlot->nextUnitNode(pUnitNode);

				if (pUnit && pUnit != this && pUnit->getOwnerINLINE() == getOwnerINLINE() && pUnit->canAcquirePromotionAny())
				{
					pUnit->changeExperience(i < iRemainder ? iMinExperiencePerUnit+1 : iMinExperiencePerUnit);
					pUnit->testPromotionReady();
				}

				i++;
			}

			return true;
		}
	}

	return false;
}

int CvUnit::getStackExperienceToGive(int iNumUnits) const
{
	return (m_pUnitInfo->getLeaderExperience() * (100 + std::min(50, (iNumUnits - 1) * GC.getDefineINT("WARLORD_EXTRA_EXPERIENCE_PER_UNIT_PERCENT")))) / 100;
}

int CvUnit::upgradePrice(UnitTypes eUnit) const
{
	int iPrice;

	//DOANE Python Callbacks | Speed Improvement
	if (GC.getUSE_GET_UPGRADE_PRICE_OVERRIDE_CALLBACK())
	{
		CyArgsList argsList;
		argsList.add(getOwner());
		argsList.add(getID());
		argsList.add((int) eUnit);
		long lResult=0;
		gDLL->getPythonIFace()->callFunction(PYGameModule, "getUpgradePriceOverride", argsList.makeFunctionArgs(), &lResult); //Returns -1 as default
		if (lResult >= 0)
		{
			return lResult;
		}
	}
	//END DOANE

	iPrice = GC.getDefineINT("BASE_UNIT_UPGRADE_COST");

	iPrice += (std::max(0, (GET_PLAYER(getOwnerINLINE()).getYieldProductionNeeded(eUnit, YIELD_HAMMERS) - GET_PLAYER(getOwnerINLINE()).getYieldProductionNeeded(getUnitType(), YIELD_HAMMERS))) * GC.getDefineINT("UNIT_UPGRADE_COST_PER_PRODUCTION"));

	if (!isHuman())
	{
		iPrice *= GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIUnitUpgradePercent();
		iPrice /= 100;

		iPrice *= std::max(0, ((GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIPerEraModifier() * GET_PLAYER(getOwnerINLINE()).getCurrentEra()) + 100));
		iPrice /= 100;
	}

	iPrice -= (iPrice * getUpgradeDiscount()) / 100;

	return iPrice;
}



bool CvUnit::upgradeAvailable(UnitTypes eFromUnit, UnitClassTypes eToUnitClass, int iCount) const
{
	UnitTypes eLoopUnit;
	int iI;
	int numUnitClassInfos = GC.getNumUnitClassInfos();

	if (iCount > numUnitClassInfos)
	{
		return false;
	}

	CvUnitInfo &fromUnitInfo = GC.getUnitInfo(eFromUnit);

	if (fromUnitInfo.getUpgradeUnitClass(eToUnitClass))
	{
		return true;
	}

	for (iI = 0; iI < numUnitClassInfos; iI++)
	{
		if (fromUnitInfo.getUpgradeUnitClass(iI))
		{
			eLoopUnit = ((UnitTypes)(GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(iI)));

			if (eLoopUnit != NO_UNIT)
			{
				if (upgradeAvailable(eLoopUnit, eToUnitClass, (iCount + 1)))
				{
					return true;
				}
			}
		}
	}

	return false;
}


bool CvUnit::canUpgrade(UnitTypes eUnit, bool bTestVisible) const
{
	if (eUnit == NO_UNIT)
	{
		return false;
	}

	if (!isReadyForUpgrade())
	{
		return false;
	}

	if (!bTestVisible)
	{
		if (GET_PLAYER(getOwnerINLINE()).getGold() < upgradePrice(eUnit))
		{
			return false;
		}
	}

	if (hasUpgrade(eUnit))
	{
		return true;
	}

	return false;
}

bool CvUnit::isReadyForUpgrade() const
{
	if (!canMove())
	{
		return false;
	}

	if (plot()->getTeam() != getTeam())
	{
		return false;
	}

	return true;
}

// has upgrade is used to determine if an upgrade is possible,
// it specifically does not check whether the unit can move, whether the current plot is owned, enough gold
// those are checked in canUpgrade()
// does not search all cities, only checks the closest one
bool CvUnit::hasUpgrade(bool bSearch) const
{
	return (getUpgradeCity(bSearch) != NULL);
}

// has upgrade is used to determine if an upgrade is possible,
// it specifically does not check whether the unit can move, whether the current plot is owned, enough gold
// those are checked in canUpgrade()
// does not search all cities, only checks the closest one
bool CvUnit::hasUpgrade(UnitTypes eUnit, bool bSearch) const
{
	return (getUpgradeCity(eUnit, bSearch) != NULL);
}

// finds the 'best' city which has a valid upgrade for the unit,
// it specifically does not check whether the unit can move, or if the player has enough gold to upgrade
// those are checked in canUpgrade()
// if bSearch is true, it will check every city, if not, it will only check the closest valid city
// NULL result means the upgrade is not possible
CvCity* CvUnit::getUpgradeCity(bool bSearch) const
{
	CvPlayerAI& kPlayer = GET_PLAYER(getOwnerINLINE());
	UnitAITypes eUnitAI = AI_getUnitAIType();
	CvArea* pArea = area();

	int iCurrentValue = kPlayer.AI_unitValue(getUnitType(), eUnitAI, pArea);

	int iBestSearchValue = MAX_INT;
	CvCity* pBestUpgradeCity = NULL;

	for (int iI = 0; iI < GC.getNumUnitInfos(); iI++)
	{
		int iNewValue = kPlayer.AI_unitValue(((UnitTypes)iI), eUnitAI, pArea);
		if (iNewValue > iCurrentValue)
		{
			int iSearchValue;
			CvCity* pUpgradeCity = getUpgradeCity((UnitTypes)iI, bSearch, &iSearchValue);
			if (pUpgradeCity != NULL)
			{
				// if not searching or close enough, then this match will do
				if (!bSearch || iSearchValue < 16)
				{
					return pUpgradeCity;
				}

				if (iSearchValue < iBestSearchValue)
				{
					iBestSearchValue = iSearchValue;
					pBestUpgradeCity = pUpgradeCity;
				}
			}
		}
	}

	return pBestUpgradeCity;
}

// finds the 'best' city which has a valid upgrade for the unit, to eUnit type
// it specifically does not check whether the unit can move, or if the player has enough gold to upgrade
// those are checked in canUpgrade()
// if bSearch is true, it will check every city, if not, it will only check the closest valid city
// if iSearchValue non NULL, then on return it will be the city's proximity value, lower is better
// NULL result means the upgrade is not possible
CvCity* CvUnit::getUpgradeCity(UnitTypes eUnit, bool bSearch, int* iSearchValue) const
{
	if (eUnit == NO_UNIT)
	{
		return false;
	}

	CvPlayerAI& kPlayer = GET_PLAYER(getOwnerINLINE());
	CvUnitInfo& kUnitInfo = GC.getUnitInfo(eUnit);

	if (GC.getCivilizationInfo(kPlayer.getCivilizationType()).getCivilizationUnits(kUnitInfo.getUnitClassType()) != eUnit)
	{
		return false;
	}

	if (!upgradeAvailable(getUnitType(), ((UnitClassTypes)(kUnitInfo.getUnitClassType()))))
	{
		return false;
	}

	if (kUnitInfo.getCargoSpace() < getCargo())
	{
		return false;
	}

	CLLNode<IDInfo>* pUnitNode = plot()->headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = plot()->nextUnitNode(pUnitNode);

		if (pLoopUnit->getTransportUnit() == this)
		{
			if (kUnitInfo.getSpecialCargo() != NO_SPECIALUNIT)
			{
				if (kUnitInfo.getSpecialCargo() != pLoopUnit->getSpecialUnitType())
				{
					return false;
				}
			}

			if (kUnitInfo.getDomainCargo() != NO_DOMAIN)
			{
				if (kUnitInfo.getDomainCargo() != pLoopUnit->getDomainType())
				{
					return false;
				}
			}
		}
	}

	// sea units must be built on the coast
	bool bCoastalOnly = (getDomainType() == DOMAIN_SEA);

	// results
	int iBestValue = MAX_INT;
	CvCity* pBestCity = NULL;

	// if search is true, check every city for our team
	if (bSearch)
	{
		TeamTypes eTeam = getTeam();
		int iArea = getArea();
		int iX = getX_INLINE(), iY = getY_INLINE();

		// check every player on our team's cities
		for (int iI = 0; iI < MAX_PLAYERS; iI++)
		{
			// is this player on our team?
			CvPlayerAI& kLoopPlayer = GET_PLAYER((PlayerTypes)iI);
			if (kLoopPlayer.isAlive() && kLoopPlayer.getTeam() == eTeam)
			{
				int iLoop;
				for (CvCity* pLoopCity = kLoopPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kLoopPlayer.nextCity(&iLoop))
				{
					// if coastal only, then make sure we are coast
					CvArea* pWaterArea = NULL;
					if (!bCoastalOnly || ((pWaterArea = pLoopCity->waterArea()) != NULL && !pWaterArea->isLake()))
					{
						// can this city tran this unit?
						if (pLoopCity->canTrain(eUnit, false, false, true))
						{
							int iValue = plotDistance(iX, iY, pLoopCity->getX_INLINE(), pLoopCity->getY_INLINE());

							// if not same area, not as good (lower numbers are better)
							if (iArea != pLoopCity->getArea() && (!bCoastalOnly || iArea != pWaterArea->getID()))
							{
								iValue *= 16;
							}

							// if we cannot path there, not as good (lower numbers are better)
							if (!generatePath(pLoopCity->plot(), 0, true))
							{
								iValue *= 16;
							}

							if (iValue < iBestValue)
							{
								iBestValue = iValue;
								pBestCity = pLoopCity;
							}
						}
					}
				}
			}
		}
	}
	else
	{
		// find the closest city
		CvCity* pClosestCity = GC.getMapINLINE().findCity(getX_INLINE(), getY_INLINE(), NO_PLAYER, getTeam(), true, bCoastalOnly);
		if (pClosestCity != NULL)
		{
			// if we can train, then return this city (otherwise it will return NULL)
			if (pClosestCity->canTrain(eUnit, false, false, true))
			{
				// did not search, always return 1 for search value
				iBestValue = 1;

				pBestCity = pClosestCity;
			}
		}
	}

	// return the best value, if non-NULL
	if (iSearchValue != NULL)
	{
		*iSearchValue = iBestValue;
	}

	return pBestCity;
}

void CvUnit::upgrade(UnitTypes eUnit)
{
	CvUnit* pUpgradeUnit;

	if (!canUpgrade(eUnit))
	{
		return;
	}

	GET_PLAYER(getOwnerINLINE()).changeGold(-(upgradePrice(eUnit)));

	pUpgradeUnit = GET_PLAYER(getOwnerINLINE()).initUnit(eUnit, getProfession(), getX_INLINE(), getY_INLINE(), AI_getUnitAIType());

	FAssertMsg(pUpgradeUnit != NULL, "UpgradeUnit is not assigned a valid value");

	pUpgradeUnit->joinGroup(getGroup());

	pUpgradeUnit->convert(this, true);

	pUpgradeUnit->finishMoves();

	if (pUpgradeUnit->getLeaderUnitType() == NO_UNIT)
	{
		if (pUpgradeUnit->getExperience() > GC.getDefineINT("MAX_EXPERIENCE_AFTER_UPGRADE"))
		{
			pUpgradeUnit->setExperience(GC.getDefineINT("MAX_EXPERIENCE_AFTER_UPGRADE"));
		}
	}
}

bool CvUnit::canUpgradeRefUnit(UnitTypes eUnit) const
{
	if (eUnit == NO_UNIT)
	{
		return false;
	}

	if (getUnitType() == eUnit)
	{
		return false;
	}

	if (m_pUnitInfo->isMechUnit())
	{
		return false;
	}

	if (canClearSpecialty())
	{
		return false;
	}
	if (getProfession() == NO_PROFESSION)
	{
		return false;
	}
	if (GC.getProfessionInfo(getProfession()).isCitizen())
	{
		CvCity* pCity = GET_PLAYER(getOwnerINLINE()).getPopulationUnitCity(getID());
		if (pCity == NULL)
		{
			return false;
		}
		return true;
	}
	if (!isOnMap())
	{
		return false;
	}

	return true;
}

void CvUnit::upgradeRefUnit(UnitTypes eUpgradeUnit)
{
	CvUnit* pUpgradeUnit;

	if (!canUpgradeRefUnit(eUpgradeUnit))
	{
		return;
	}

	CvCity* pCity = GET_PLAYER(getOwnerINLINE()).getPopulationUnitCity(getID());
	if (pCity != NULL)
	{
		pUpgradeUnit = GET_PLAYER(getOwnerINLINE()).initUnit(eUpgradeUnit, (ProfessionTypes)GC.getUnitInfo(eUpgradeUnit).getDefaultProfession(), pCity->getX_INLINE(), pCity->getY_INLINE(), AI_getUnitAIType());
	}
	else
	{
		pUpgradeUnit = GET_PLAYER(getOwnerINLINE()).initUnit(eUpgradeUnit, NO_PROFESSION, getX_INLINE(), getY_INLINE(), AI_getUnitAIType());
	}
	FAssert(pUpgradeUnit != NULL);

	ProfessionTypes eProfession = getProfession();
	if (pCity != NULL)
	{
		pCity->removePopulationUnit(this, false, (ProfessionTypes)m_pUnitInfo->getDefaultProfession());
	}
	pUpgradeUnit->convert(this, true);
	if (pCity != NULL)
	{
		pUpgradeUnit->setProfession(eProfession);
	}
}

HandicapTypes CvUnit::getHandicapType() const
{
	return GET_PLAYER(getOwnerINLINE()).getHandicapType();
}


CivilizationTypes CvUnit::getCivilizationType() const
{
	return GET_PLAYER(getOwnerINLINE()).getCivilizationType();
}

const wchar* CvUnit::getVisualCivAdjective(TeamTypes eForTeam) const
{
	if (getVisualOwner(eForTeam) == getOwnerINLINE())
	{
		return GC.getCivilizationInfo(getCivilizationType()).getAdjectiveKey();
	}

	return L"";
}

SpecialUnitTypes CvUnit::getSpecialUnitType() const
{
	return ((SpecialUnitTypes)(m_pUnitInfo->getSpecialUnitType()));
}


UnitTypes CvUnit::getCaptureUnitType(CivilizationTypes eCivilization) const
{
	FAssert(eCivilization != NO_CIVILIZATION);
	UnitTypes eCaptureUnit = NO_UNIT;
	if (m_pUnitInfo->getUnitCaptureClassType() != NO_UNITCLASS)
	{
		eCaptureUnit = (UnitTypes)GC.getCivilizationInfo(eCivilization).getCivilizationUnits(m_pUnitInfo->getUnitCaptureClassType());
	}

	if (eCaptureUnit == NO_UNIT && isUnarmed())
	{
		eCaptureUnit = (UnitTypes)GC.getCivilizationInfo(eCivilization).getCivilizationUnits(getUnitClassType());
	}

	if (eCaptureUnit == NO_UNIT)
	{
		return NO_UNIT;
	}

	CvUnitInfo& kUnitInfo = GC.getUnitInfo(eCaptureUnit);
	if (kUnitInfo.getDefaultProfession() != NO_PROFESSION)
	{
		CvCivilizationInfo& kCivInfo = GC.getCivilizationInfo(eCivilization);
		if (!kCivInfo.isValidProfession(kUnitInfo.getDefaultProfession()))
		{
			return NO_UNIT;
		}
	}

	return eCaptureUnit;
}

UnitCombatTypes CvUnit::getProfessionUnitCombatType(ProfessionTypes eProfession) const
{
	if (eProfession != NO_PROFESSION)
	{
		UnitCombatTypes eUnitCombat = (UnitCombatTypes) GC.getProfessionInfo(eProfession).getUnitCombatType();
		if (eUnitCombat != NO_UNITCOMBAT)
		{
			return eUnitCombat;
		}
	}

	return ((UnitCombatTypes)(m_pUnitInfo->getUnitCombatType()));
}

void CvUnit::processUnitCombatType(UnitCombatTypes eUnitCombat, int iChange)
{
	if (iChange != 0)
	{
		//update unit combat changes
		for (int iI = 0; iI < GC.getNumTraitInfos(); iI++)
		{
			if (GET_PLAYER(getOwnerINLINE()).hasTrait((TraitTypes)iI))
			{
				for (int iJ = 0; iJ < GC.getNumPromotionInfos(); iJ++)
				{
					if (GC.getTraitInfo((TraitTypes) iI).isFreePromotion(iJ))
					{
						if ((eUnitCombat != NO_UNITCOMBAT) && GC.getTraitInfo((TraitTypes) iI).isFreePromotionUnitCombat(eUnitCombat))
						{
							changeFreePromotionCount(((PromotionTypes)iJ), iChange);
						}
					}
				}
			}
		}

		if (NO_UNITCOMBAT != eUnitCombat)
		{
			for (int iJ = 0; iJ < GC.getNumPromotionInfos(); iJ++)
			{
				if (GET_PLAYER(getOwnerINLINE()).isFreePromotion(eUnitCombat, (PromotionTypes)iJ))
				{
					changeFreePromotionCount(((PromotionTypes)iJ), iChange);
				}
			}
		}

		if (IsSelected())
		{
			gDLL->getInterfaceIFace()->setDirty(SelectionButtons_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(InfoPane_DIRTY_BIT, true);
		}
	}
}

UnitCombatTypes CvUnit::getUnitCombatType() const
{
	return getProfessionUnitCombatType(getProfession());
}


DomainTypes CvUnit::getDomainType() const
{
	return ((DomainTypes)(m_pUnitInfo->getDomainType()));
}

int CvUnit::getBombardProfession() const
{
	int iResultat = -1;
	ProfessionTypes eProfession = getProfession();
	if (eProfession != NO_PROFESSION)
	{
		iResultat = GC.getProfessionInfo(eProfession).getBombardRate();
	}
	return iResultat;
}


InvisibleTypes CvUnit::getInvisibleType() const
{
	return ((InvisibleTypes)(m_pUnitInfo->getInvisibleType()));
}

int CvUnit::getNumSeeInvisibleTypes() const
{
	return m_pUnitInfo->getNumSeeInvisibleTypes();
}

InvisibleTypes CvUnit::getSeeInvisibleType(int i) const
{
	return (InvisibleTypes)(m_pUnitInfo->getSeeInvisibleType(i));
}

bool CvUnit::isHuman() const
{
	return GET_PLAYER(getOwnerINLINE()).isHuman();
}

bool CvUnit::isNative() const
{
	return GET_PLAYER(getOwnerINLINE()).isNative();
}

int CvUnit::visibilityRange() const
{
	return (GC.getDefineINT("UNIT_VISIBILITY_RANGE") + getExtraVisibilityRange());
}

int CvUnit::baseMoves() const
{
	int iBaseMoves = m_pUnitInfo->getMoves();

	if (getProfession() != PROFESSION_SAILOR)
	{
		iBaseMoves += getExtraMoves();//Sailors don't have to move faster on land
	}

	iBaseMoves += GET_PLAYER(getOwnerINLINE()).getUnitMoveChange(getUnitClassType());

	if (getProfession() != NO_PROFESSION)
	{
		iBaseMoves += GET_PLAYER(getOwnerINLINE()).getProfessionMoveChange(getProfession());
	}

	return iBaseMoves;
}

int CvUnit::maxMoves() const
{
	return (baseMoves() * GC.getMOVE_DENOMINATOR());
}


int CvUnit::movesLeft() const
{
	return std::max(0, (maxMoves() - getMoves()));
}


bool CvUnit::canMove() const
{
	if (GET_PLAYER(getOwnerINLINE()).isTerritorialInfluenceMode())
	{
		return false;
	}
	if (isDead())
	{
		return false;
	}

	if (getProfession() == PROFESSION_PRISONER)
	{
		return false;
	}

	if (getMoves() >= maxMoves())
	{
		return false;
	}

	if (getImmobileTimer() > 0)
	{
		return false;
	}

	if (!isOnMap())
	{
		return false;
	}
	if (m_pUnitInfo->isMechUnit())
	{
		if (!hasCrew())
		{
			return false;
		}
	}
	return true;
}


bool CvUnit::hasMoved()	const
{
	return (getMoves() > 0);
}


// XXX should this test for coal?
bool CvUnit::canBuildRoute() const
{
	int iI;

	for (iI = 0; iI < GC.getNumBuildInfos(); iI++)
	{
		if (GC.getBuildInfo((BuildTypes)iI).getRoute() != NO_ROUTE)
		{
			if (m_pUnitInfo->getBuilds(iI))
			{
				return true;
			}
		}
	}

	return false;
}

BuildTypes CvUnit::getBuildType() const
{
	CvSelectionGroup* pGroup = getGroup();
	if (pGroup == NULL)
	{
		return NO_BUILD;
	}

	if (pGroup->headMissionQueueNode() != NULL)
	{
		switch (pGroup->headMissionQueueNode()->m_data.eMissionType)
		{
		case MISSION_MOVE_TO:
		case MISSION_FOUND_TO:
			break;

		case MISSION_ROUTE_TO:
			{
				BuildTypes eBuild;
				if (pGroup->getBestBuildRoute(plot(), &eBuild) != NO_ROUTE)
				{
					return eBuild;
				}
			}
			break;

		case MISSION_MOVE_TO_UNIT:
		case MISSION_SKIP:
		case MISSION_SLEEP:
		case MISSION_FORTIFY:
		case MISSION_HEAL:
		case MISSION_SENTRY:
		case MISSION_BOMBARD:
		case MISSION_PILLAGE:
		case MISSION_FOUND:
		case MISSION_JOIN_CITY:
		case MISSION_LEAD:
			break;

		case MISSION_BUILD:
			return (BuildTypes)pGroup->headMissionQueueNode()->m_data.iData1;
			break;

		default:
			FAssert(false);
			break;
		}
	}

	return NO_BUILD;
}


int CvUnit::workRate(bool bMax) const
{
	if (!bMax)
	{
		if (!canMove())
		{
			return 0;
		}
	}

	int iRate = m_pUnitInfo->getWorkRate() + getExtraWorkRate();

	iRate *= std::max(0, (GET_PLAYER(getOwnerINLINE()).getWorkerSpeedModifier() + m_pUnitInfo->getWorkRateModifier() + 100));
	iRate /= 100;

	if (!isHuman())
	{
		iRate *= std::max(0, (GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIWorkRateModifier() + 100));
		iRate /= 100;
	}

	return iRate;
}

void CvUnit::changeExtraWorkRate(int iChange)
{
	m_iExtraWorkRate += iChange;
}

int CvUnit::getExtraWorkRate() const
{
	return m_iExtraWorkRate;

}

bool CvUnit::isNoBadGoodies() const
{
	return m_pUnitInfo->isNoBadGoodies();
}


bool CvUnit::isOnlyDefensive() const
{
	return m_pUnitInfo->isOnlyDefensive();
}


bool CvUnit::isNoUnitCapture() const
{
	return m_pUnitInfo->isNoCapture();
}


bool CvUnit::isNoCityCapture() const
{
	return m_pUnitInfo->isNoCapture();
}


bool CvUnit::isRivalTerritory() const
{
	return m_pUnitInfo->isRivalTerritory();
}

bool CvUnit::canCoexistWithEnemyUnit(TeamTypes eTeam, bool isDoingTheAction) const
{
	if (isDoingTheAction) 
	{
		return false;
	}

	return isInvisible(eTeam, false, true);
}

bool CvUnit::isFighting() const
{
	return (getCombatUnit() != NULL);
}


bool CvUnit::isAttacking() const
{
	return (getAttackPlot() != NULL && !isDelayedDeath());
}


bool CvUnit::isDefending() const
{
	return (isFighting() && !isAttacking());
}


bool CvUnit::isCombat() const
{
	return (isFighting() || isAttacking());
}


int CvUnit::maxHitPoints() const
{
	return GC.getMAX_HIT_POINTS();
}


int CvUnit::currHitPoints()	const
{
	return (maxHitPoints() - getDamage());
}


bool CvUnit::isHurt() const
{
	return (getDamage() > 0);
}


bool CvUnit::isDead() const
{
	return (getDamage() >= maxHitPoints());
}

bool CvUnit::isBadlyHurt() const
{
	return (getDamage() >= maxHitPoints() / 2);
}


void CvUnit::setBaseCombatStr(int iCombat)
{
	m_iBaseCombat = iCombat;
	updateBestLandCombat();
}
int CvUnit::baseOriginCombatStr() const
{
	return m_iBaseCombat;
}
int CvUnit::baseCombatStr() const
{
	int iBaseCombat = m_iBaseCombat;
	if (hasTempCrew())
	{
		return 0;
	}
	if (isHuman())
	{
		ProfessionTypes pProfession = getProfession();
		if (m_pUnitInfo->isMechUnit())
		{
			int iConsom = getConsommationMunition(true);
			int iMunition = getMunition();
			int iCannon = getNbCannon();
			int iCannonMax = m_pUnitInfo->getMaxCannon();


			if (iCannon < iCannonMax && iCannonMax != 0)
			{
				iBaseCombat = (iBaseCombat*iCannon)/iCannonMax;
			}
			if (iMunition < iConsom)
			{
				if (iConsom != 0 && iConsom != -1)
				{
					iBaseCombat = (iBaseCombat*getMunition()/iConsom);
				}
			}

		}
	}

	return iBaseCombat;
}

void CvUnit::updateBestLandCombat()
{
	if (getDomainType() == DOMAIN_LAND)
	{
		if (baseCombatStr() > GC.getGameINLINE().getBestLandUnitCombat())
		{
			GC.getGameINLINE().setBestLandUnitCombat(baseCombatStr());
		}
	}
}


// maxCombatStr can be called in four different configurations
//		pPlot == NULL, pAttacker == NULL for combat when this is the attacker
//		pPlot valid, pAttacker valid for combat when this is the defender
//		pPlot valid, pAttacker == NULL (new case), when this is the defender, attacker unknown
//		pPlot valid, pAttacker == this (new case), when the defender is unknown, but we want to calc approx str
//			note, in this last case, it is expected pCombatDetails == NULL, it does not have to be, but some
//			values may be unexpectedly reversed in this case (iModifierTotal will be the negative sum)
int CvUnit::maxCombatStr(const CvPlot* pPlot, const CvUnit* pAttacker, CombatDetails* pCombatDetails) const
{
	int iCombat;

	FAssertMsg((pPlot == NULL) || (pPlot->getTerrainType() != NO_TERRAIN), "(pPlot == NULL) || (pPlot->getTerrainType() is not expected to be equal with NO_TERRAIN)");

	// handle our new special case
	const	CvPlot*	pAttackedPlot = NULL;
	bool	bAttackingUnknownDefender = false;
	if (pAttacker == this)
	{
		bAttackingUnknownDefender = true;
		pAttackedPlot = pPlot;

		// reset these values, we will fiddle with them below
		pPlot = NULL;
		pAttacker = NULL;
	}
	// otherwise, attack plot is the plot of us (the defender)
	else if (pAttacker != NULL)
	{
		pAttackedPlot = plot();
	}

	if (pCombatDetails != NULL)
	{
		pCombatDetails->iExtraCombatPercent = 0;
		pCombatDetails->iNativeCombatModifierTB = 0;
		pCombatDetails->iNativeCombatModifierAB = 0;
		pCombatDetails->iPlotDefenseModifier = 0;
		pCombatDetails->iFortifyModifier = 0;
		pCombatDetails->iCityDefenseModifier = 0;
		pCombatDetails->iHillsAttackModifier = 0;
		pCombatDetails->iHillsDefenseModifier = 0;
		pCombatDetails->iFeatureAttackModifier = 0;
		pCombatDetails->iFeatureDefenseModifier = 0;
		pCombatDetails->iTerrainAttackModifier = 0;
		pCombatDetails->iTerrainDefenseModifier = 0;
		pCombatDetails->iCityAttackModifier = 0;
		pCombatDetails->iDomainDefenseModifier = 0;
		pCombatDetails->iClassDefenseModifier = 0;
		pCombatDetails->iClassAttackModifier = 0;
		pCombatDetails->iCombatModifierA = 0;
		pCombatDetails->iCombatModifierT = 0;
		pCombatDetails->iDomainModifierA = 0;
		pCombatDetails->iDomainModifierT = 0;
		pCombatDetails->iRiverAttackModifier = 0;
		pCombatDetails->iAmphibAttackModifier = 0;
		pCombatDetails->iRebelPercentModifier = 0;
		pCombatDetails->iModifierTotal = 0;
		pCombatDetails->iBaseCombatStr = 0;
		pCombatDetails->iCombat = 0;
		pCombatDetails->iMaxCombatStr = 0;
		pCombatDetails->iCurrHitPoints = 0;
		pCombatDetails->iMaxHitPoints = 0;
		pCombatDetails->iCurrCombatStr = 0;
		pCombatDetails->eOwner = getOwnerINLINE();
		pCombatDetails->eVisualOwner = getVisualOwner();
		if (getProfession() == NO_PROFESSION)
		{
			pCombatDetails->sUnitName = getName().GetCString();
		}
		else
		{
			pCombatDetails->sUnitName = CvWString::format(L"%s (%s)", GC.getProfessionInfo(getProfession()).getDescription(), getName().GetCString());
		}
	}

	if (baseCombatStr() == 0)
	{
		return 0;
	}

	int iModifier = 0;
	int iExtraModifier;

	iExtraModifier = getExtraCombatPercent();
	iModifier += iExtraModifier;
	if (pCombatDetails != NULL)
	{
		pCombatDetails->iExtraCombatPercent = iExtraModifier;
	}

	if (pAttacker != NULL)
	{
		if (isNative())
		{
			iExtraModifier = -GET_PLAYER(pAttacker->getOwnerINLINE()).getNativeCombatModifier();
			if (!pAttacker->isHuman())
			{
				iExtraModifier -= GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAINativeCombatModifier();
			}
			iModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iNativeCombatModifierTB = iExtraModifier;
			}
		}

		if (pAttacker->isNative())
		{
			iExtraModifier = GET_PLAYER(getOwnerINLINE()).getNativeCombatModifier();
			if (!isHuman())
			{
				iExtraModifier += GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAINativeCombatModifier();
			}
			iModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iNativeCombatModifierAB = iExtraModifier;
			}
		}

		//Start M07 : rebellion promo suppr
		//iExtraModifier = rebelModifier(pAttacker->getOwnerINLINE()) - pAttacker->rebelModifier(getOwnerINLINE());
		//iModifier += iExtraModifier;
		//End M07
		if (pCombatDetails != NULL)
		{
			pCombatDetails->iRebelPercentModifier = iExtraModifier;
		}
	}

	// add defensive bonuses (leaving these out for bAttackingUnknownDefender case)
	if (pPlot != NULL)
	{
		if (!noDefensiveBonus())
		{
			iExtraModifier = pPlot->defenseModifier(getTeam());
			iModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iPlotDefenseModifier = iExtraModifier;
			}
		}

		iExtraModifier = fortifyModifier();
		iModifier += iExtraModifier;
		if (pCombatDetails != NULL)
		{
			pCombatDetails->iFortifyModifier = iExtraModifier;
		}

		if (pPlot->isCity(true, getTeam()))
		{
			iExtraModifier = cityDefenseModifier();
			iModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iCityDefenseModifier = iExtraModifier;
			}
		}

		if (pPlot->isHills() || pPlot->isPeak())
		{
			iExtraModifier = hillsDefenseModifier();
			iModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iHillsDefenseModifier = iExtraModifier;
			}
		}

		if (pPlot->getFeatureType() != NO_FEATURE)
		{
			iExtraModifier = featureDefenseModifier(pPlot->getFeatureType());
			iModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iFeatureDefenseModifier = iExtraModifier;
			}
		}
		else
		{
			iExtraModifier = terrainDefenseModifier(pPlot->getTerrainType());
			iModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iTerrainDefenseModifier = iExtraModifier;
			}
		}
	}

	// if we are attacking to an plot with an unknown defender, the calc the modifier in reverse
	if (bAttackingUnknownDefender)
	{
		pAttacker = this;
	}

	// calc attacker bonueses
	if (pAttacker != NULL)
	{
		int iTempModifier = 0;

		if (pAttackedPlot->isCity(true, getTeam()))
		{
			iExtraModifier = -pAttacker->cityAttackModifier();
			iTempModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iCityAttackModifier = iExtraModifier;
			}
		}

		if (pAttackedPlot->isHills() || pAttackedPlot->isPeak())
		{
			iExtraModifier = -pAttacker->hillsAttackModifier();
			iTempModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iHillsAttackModifier = iExtraModifier;
			}
		}

		if (pAttackedPlot->getFeatureType() != NO_FEATURE)
		{
			iExtraModifier = -pAttacker->featureAttackModifier(pAttackedPlot->getFeatureType());
			iTempModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iFeatureAttackModifier = iExtraModifier;
			}
		}
		else
		{
			iExtraModifier = -pAttacker->terrainAttackModifier(pAttackedPlot->getTerrainType());
			iModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iTerrainAttackModifier = iExtraModifier;
			}
		}

		// only compute comparisions if we are the defender with a known attacker
		if (!bAttackingUnknownDefender)
		{
			FAssertMsg(pAttacker != this, "pAttacker is not expected to be equal with this");

			iExtraModifier = unitClassDefenseModifier(pAttacker->getUnitClassType());
			iTempModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iClassDefenseModifier = iExtraModifier;
			}

			iExtraModifier = -pAttacker->unitClassAttackModifier(getUnitClassType());
			iTempModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iClassAttackModifier = iExtraModifier;
			}

			if (pAttacker->getUnitCombatType() != NO_UNITCOMBAT)
			{
				iExtraModifier = unitCombatModifier(pAttacker->getUnitCombatType());
				iTempModifier += iExtraModifier;
				if (pCombatDetails != NULL)
				{
					pCombatDetails->iCombatModifierA = iExtraModifier;
				}
			}
			if (getUnitCombatType() != NO_UNITCOMBAT)
			{
				iExtraModifier = -pAttacker->unitCombatModifier(getUnitCombatType());
				iTempModifier += iExtraModifier;
				if (pCombatDetails != NULL)
				{
					pCombatDetails->iCombatModifierT = iExtraModifier;
				}
			}

			iExtraModifier = domainModifier(pAttacker->getDomainType());
			iTempModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iDomainModifierA = iExtraModifier;
			}

			iExtraModifier = -pAttacker->domainModifier(getDomainType());
			iTempModifier += iExtraModifier;
			if (pCombatDetails != NULL)
			{
				pCombatDetails->iDomainModifierT = iExtraModifier;
			}
		}

		if (!(pAttacker->isRiver()))
		{
			if (pAttacker->plot()->isRiverCrossing(directionXY(pAttacker->plot(), pAttackedPlot)))
			{
				iExtraModifier = -GC.getRIVER_ATTACK_MODIFIER();
				iTempModifier += iExtraModifier;
				if (pCombatDetails != NULL)
				{
					pCombatDetails->iRiverAttackModifier = iExtraModifier;
				}
			}
		}

		if (!(pAttacker->isAmphib()))
		{
			if (!(pAttackedPlot->isWater()) && pAttacker->plot()->isWater())
			{
				iExtraModifier = -GC.getAMPHIB_ATTACK_MODIFIER();
				iTempModifier += iExtraModifier;
				if (pCombatDetails != NULL)
				{
					pCombatDetails->iAmphibAttackModifier = iExtraModifier;
				}
			}
		}
		CvCity* pCity = pAttackedPlot->getPlotCity();
		if (pCity != NULL)
		{
			if (pCity->getDefenseModifier() > GC.getFORTIFICATION_BREAK_DAMAGE())
			{
				iExtraModifier = GC.getPOWERFUL_FORTIFICATION_PENALTY();
				iTempModifier += iExtraModifier;
				if (pCombatDetails != NULL)
				{
					pCombatDetails->iCityDefenseModifier += iExtraModifier;
				}
			}
		}

		// if we are attacking an unknown defender, then use the reverse of the modifier
		if (bAttackingUnknownDefender)
		{
			iModifier -= iTempModifier;
		}
		else
		{
			iModifier += iTempModifier;
		}
	}

	iModifier -= baseCombatStr() * getMilitaryProfessionPenalty() / 100;

	if (pCombatDetails != NULL)
	{
		pCombatDetails->iModifierTotal = iModifier;
		pCombatDetails->iBaseCombatStr = baseCombatStr();
	}

	if (iModifier > 0)
	{
		iCombat = (baseCombatStr() * (iModifier + 100));
	}
	else
	{
		iCombat = ((baseCombatStr() * 10000) / (100 - iModifier));
	}

	if (pCombatDetails != NULL)
	{
		pCombatDetails->iCombat = iCombat;
		pCombatDetails->iMaxCombatStr = std::max(1, iCombat);
		pCombatDetails->iCurrHitPoints = currHitPoints();
		pCombatDetails->iMaxHitPoints = maxHitPoints();
		pCombatDetails->iCurrCombatStr = ((pCombatDetails->iMaxCombatStr * pCombatDetails->iCurrHitPoints) / pCombatDetails->iMaxHitPoints);
	}

	return std::max(1, iCombat);
}


int CvUnit::currCombatStr(const CvPlot* pPlot, const CvUnit* pAttacker, CombatDetails* pCombatDetails) const
{
	return ((maxCombatStr(pPlot, pAttacker, pCombatDetails) * currHitPoints()) / maxHitPoints());
}


int CvUnit::currFirepower(const CvPlot* pPlot, const CvUnit* pAttacker) const
{
	return ((maxCombatStr(pPlot, pAttacker) + currCombatStr(pPlot, pAttacker) + 1) / 2);
}

// this nomalizes str by firepower, useful for quick odds calcs
// the effect is that a damaged unit will have an effective str lowered by firepower/maxFirepower
// doing the algebra, this means we mulitply by 1/2(1 + currHP)/maxHP = (maxHP + currHP) / (2 * maxHP)
int CvUnit::currEffectiveStr(const CvPlot* pPlot, const CvUnit* pAttacker, CombatDetails* pCombatDetails) const
{
	int currStr = currCombatStr(pPlot, pAttacker, pCombatDetails);

	currStr *= (maxHitPoints() + currHitPoints());
	currStr /= (2 * maxHitPoints());

	return currStr;
}

float CvUnit::maxCombatStrFloat(const CvPlot* pPlot, const CvUnit* pAttacker) const
{
	return (((float)(maxCombatStr(pPlot, pAttacker))) / 100.0f);
}


float CvUnit::currCombatStrFloat(const CvPlot* pPlot, const CvUnit* pAttacker) const
{
	return (((float)(currCombatStr(pPlot, pAttacker))) / 100.0f);
}

bool CvUnit::isUnarmed() const
{
	if (!canFight())
	{
		return true;
	}

	if (getUnarmedCount() > 0)
	{
		return true;
	}

	return false;
}

bool CvUnit::isUnemployed(int iTurn) const
{
	int iTurnBeforeBeConsideringAsUnemployed = 3; // TODO Global

	if (m_pUnitInfo->isMechUnit())
	{
		return false;
	}

	if (getProfession() != PROFESSION_COLONIST) 
	{
		return false;
	}

	if (getSameTileHeal() > 0) 
	{
		return false;
	}

	if (getUnitType() == UNIT_AGRONOME)
	{
		return false;
	}

	if (iTurn - getGameTurnCreated() <= iTurnBeforeBeConsideringAsUnemployed)
	{
		return false;
	}

	return true;
}

bool CvUnit::isMilitary() const
{
	ProfessionTypes eProfession = getProfession();

	if (eProfession == NO_PROFESSION)
	{
		return false;
	}

	CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);

	if (kProfession.isCitizen())
	{
		return false;
	}

	if (kProfession.isUnarmed() && getSameTileHeal() == 0)
	{
		return false;
	}

	if (eProfession == PROFESSION_SCOUT)
	{
		return false;
	}

	return true;
}

int CvUnit::getPower() const
{
	int iPower = m_pUnitInfo->getPowerValue();
	if (getProfession() != NO_PROFESSION)
	{
		iPower += GC.getProfessionInfo(getProfession()).getPowerValue();
		for (int i = 0; i < NUM_YIELD_TYPES; ++i)
		{
			YieldTypes eYield = (YieldTypes) i;
			iPower += GC.getYieldInfo(eYield).getPowerValue() * GET_PLAYER(getOwnerINLINE()).getYieldEquipmentAmount(getProfession(), eYield);
		}
	}

	YieldTypes eYield = getYield();
	if (eYield != NO_YIELD)
	{
		iPower += GC.getYieldInfo(eYield).getPowerValue() * getYieldStored();
	}

	return iPower;
}

int CvUnit::getAsset() const
{
	int iAsset = m_pUnitInfo->getAssetValue();
	if (getProfession() != NO_PROFESSION)
	{
		iAsset += GC.getProfessionInfo(getProfession()).getAssetValue();
		for (int i = 0; i < NUM_YIELD_TYPES; ++i)
		{
			YieldTypes eYield = (YieldTypes) i;
			iAsset += GC.getYieldInfo(eYield).getAssetValue() * GET_PLAYER(getOwnerINLINE()).getYieldEquipmentAmount(getProfession(), eYield);
		}
	}

	YieldTypes eYield = getYield();
	if (eYield != NO_YIELD)
	{
		iAsset += GC.getYieldInfo(eYield).getAssetValue() * getYieldStored();
	}
	return iAsset;
}

bool CvUnit::canFight() const
{
	if (baseCombatStr() > 0)
	{
		return true;
	}
	if (getProfession() != NO_PROFESSION && GC.getProfessionInfo(getProfession()).getBombardRate() > 0)
	{
		return true;
	}
	return false;
}

bool CvUnit::isNoAttack1() const
{
	if (getProfession() != NO_PROFESSION)
	{
		return GC.getProfessionInfo(getProfession()).isNoAttack();
	}
	else
	{
		return false;
	}
}


bool CvUnit::canAttack() const
{
	if (isNoAttack1())
	{
		return false;
	}

	if (!canFight())
	{
		return false;
	}

	if (isOnlyDefensive())
	{
		return false;
	}

	if (isUnarmed())
	{
		return false;
	}

	if (isHaveNewMilitaryProfession())
	{
		return false;
	}

	if (isHuman() && getMunition() == 0)
	{
		if (isArtilleryMen()) 
		{
			return false;
		}
		if (m_pUnitInfo->getMaxMunition() != -1)
		{
			return false;
		}
	}

	return true;
}

bool CvUnit::canDefend(const CvPlot* pPlot, bool bOnlyRealDefender) const
{
	if (pPlot == NULL)
	{
		pPlot = plot();
	}

	if (!canFight())
	{
		return false;
	}

	if (isArtilleryMen()) 
	{
		return false;
	}

	if (getCapturingPlayer() != NO_PLAYER)
	{
		return false;
	}

	if (!pPlot->isValidDomainForAction(*this))
	{
		return false;
	}

	if (isHuman())
	{
		ProfessionTypes pProfession = getProfession();
		if (pProfession != NO_PROFESSION && GC.getProfessionInfo(pProfession).getMaxMunition() != -1)
		{
			if (getMunition()==0)
			{
				return false;
			}
		}

		if (m_pUnitInfo->getMaxMunition() != -1)
		{
			if (getMunition()==0)
			{
				return false;
			}
		}
	}

	if (bOnlyRealDefender) 
	{
		if (getProfession() == PROFESSION_SCOUT) 
		{
			return false;
		}
	}

	return true;
}

bool CvUnit::canSiege(TeamTypes eTeam) const
{
	if (!canDefend())
	{
		return false;
	}

	if (!isEnemy(eTeam))
	{
		return false;
	}

	if (!isNeverInvisible())
	{
		return false;
	}

	return true;
}

bool CvUnit::isAutomated() const
{
	return getGroup()->isAutomated();
}

bool CvUnit::isWaiting() const
{
	return getGroup()->isWaiting();
}

bool CvUnit::isFortifyable() const
{
	if (!canFight())
	{
		return false;
	}

	if (noDefensiveBonus())
	{
		return false;
	}

	if (!isOnMap())
	{
		return false;
	}

	if (getDomainType() == DOMAIN_SEA)
	{
		return false;
	}

	if (!isOnMap())
	{
		return false;
	}
	if (isAutomated())
	{
		return false;
	}
	if (getProfession() != NO_PROFESSION && GC.getProfessionInfo(getProfession()).getBombardRate() != -1)
	{
		if (isMadeAttack())
		{
			return false;
		}
	}

	return true;
}


int CvUnit::fortifyModifier() const
{
	if (!isFortifyable())
	{
		return 0;
	}

	return (getFortifyTurns() * GC.getFORTIFY_MODIFIER_PER_TURN());
}

int CvUnit::getRecruitPrice() const
{
	int iIncreasePromo = 350 + 50*GC.getGameINLINE().getSorenRandNum(4, "iIncreasePromo");
	int iPrice = 200 + 50*GC.getGameINLINE().getSorenRandNum(4, "iPrice");
	for (int iI = 0; iI < GC.getNumPromotionInfos(); iI++)
	{
		PromotionTypes eLoopPromotion = (PromotionTypes) iI;
		if (isHasPromotion(eLoopPromotion))
		{
			iPrice += iIncreasePromo;
		}
	}
	return iPrice;
}

int CvUnit::experienceNeeded() const
{
	// Use python to set how much experience a unit needs...
	//DOANE Python Callbacks | Speed Improvement
	int iExperienceNeeded = 0;
	long lExperienceNeeded = 0;

	if (GC.getUSE_GET_EXPERIENCE_NEEDED_CALLBACK())
	{
		CyArgsList argsList;
		argsList.add(getLevel());	// pass in the units level
		argsList.add(getOwner());	// pass in the units

		gDLL->getPythonIFace()->callFunction(PYGameModule, "getExperienceNeeded", argsList.makeFunctionArgs(), &lExperienceNeeded);
	}
	else //This is a copy of the python code... so it works as vanilla
	{
		int iLevel = getLevel();
		// regular epic game experience
		lExperienceNeeded = 10*(2 * iLevel * iLevel + 1); //Added factor 2...
		int iModifier = GET_PLAYER(getOwnerINLINE()).getLevelExperienceModifier();
		if (iModifier != 0)
		{
			lExperienceNeeded += (lExperienceNeeded * iModifier + 99) / 100;   //ROUND UP
		}
	}

	iExperienceNeeded = (int)lExperienceNeeded;
	//END DOANE

	return iExperienceNeeded;
}

int CvUnit::attackXPValue() const
{
	return m_pUnitInfo->getXPValueAttack();
}

int CvUnit::defenseXPValue() const
{
	return m_pUnitInfo->getXPValueDefense();
}

int CvUnit::maxXPValue() const
{
	int iMaxValue;

	iMaxValue = MAX_INT;

	return iMaxValue;
}

bool CvUnit::isRanged() const
{
	CvUnitInfo * pkUnitInfo = &getUnitInfo();
	for (int i = 0; i < pkUnitInfo->getGroupDefinitions(getProfession()); i++ )
	{
		if ( !getArtInfo(i)->getActAsRanged() )
		{
			return false;
		}
	}
	return true;
}

bool CvUnit::alwaysInvisible() const
{
	if (!isOnMap())
	{
		return true;
	}

	return m_pUnitInfo->isInvisible();
}

bool CvUnit::noDefensiveBonus() const
{
	ProfessionTypes eProfession = getProfession();
	if (eProfession != NO_PROFESSION && GC.getProfessionInfo(eProfession).isNoDefensiveBonus())
	{
		return true;
	}

	if (m_pUnitInfo->isNoDefensiveBonus())
	{
		return true;
	}

	return false;
}

bool CvUnit::canMoveImpassable() const
{
	return m_pUnitInfo->isCanMoveImpassable();
}

bool CvUnit::flatMovementCost() const
{
	return m_pUnitInfo->isFlatMovementCost();
}


bool CvUnit::ignoreTerrainCost() const
{
	return m_pUnitInfo->isIgnoreTerrainCost();
}


bool CvUnit::isNeverInvisible() const
{
	return (!alwaysInvisible() && (getInvisibleType() == NO_INVISIBLE));
}


bool CvUnit::isInvisible(TeamTypes eTeam, bool bDebug, bool bCheckCargo) const
{
	if (!isOnMap())
	{
		return true;
	}

	if (bDebug && GC.getGameINLINE().isDebugMode())
	{
		return false;
	}

	if (shouldBeInvisibleOnPlot())
	{
		return true;
	}

	if (getTeam() == eTeam)
	{
		return false;
	}

	if (alwaysInvisible())
	{
		return true;
	}

	if (bCheckCargo && isCargo())
	{
		return true;
	}

	if (isTempInvisible())
	{
		if (!(GC.getGameINLINE().isDebugMode() && eTeam != NO_TEAM && GET_TEAM(eTeam).isHuman()))
		{
			return true;
		}
	}

	if (getInvisibleType() == NO_INVISIBLE)
	{
		return false;
	}

	if (NO_TEAM == eTeam)
	{//avoid crash to isInvisibleVisible
		return false;
	}

	if (m_pUnitInfo->isPirate())
	{
		return false;
	}

	return !(plot()->isInvisibleVisible(eTeam, getInvisibleType()));
}

int CvUnit::withdrawalProbability() const
{
	int withdrawalProbability = std::max(0, (m_pUnitInfo->getWithdrawalProbability() + getExtraWithdrawal()));

	withdrawalProbability -= getDamage() * withdrawalProbability / maxHitPoints();

	return withdrawalProbability;
}

int CvUnit::getEvasionProbability(const CvUnit& kAttacker) const
{
	CvCity* pEvasionCity = getEvasionCity();
	if (pEvasionCity == NULL)
	{
		return 0;
	}

	return 100 * maxMoves() / std::max(1, maxMoves() + kAttacker.maxMoves());
}

CvCity* CvUnit::getEvasionCity() const
{
	if (!isOnMap())
	{
		return NULL;
	}

	CvCity* pBestCity = NULL;
	int iBestDistance = MAX_INT;
	for (int iPlayer = 0; iPlayer < MAX_PLAYERS; ++iPlayer)
	{
		CvPlayer& kPlayer = GET_PLAYER((PlayerTypes) iPlayer);
		if (kPlayer.isAlive() && kPlayer.getTeam() == getTeam())
		{
			int iLoop;
			for (CvCity* pLoopCity = kPlayer.firstCity(&iLoop); pLoopCity != NULL; pLoopCity = kPlayer.nextCity(&iLoop))
			{
				if (pLoopCity->getArea() == getArea() || pLoopCity->plot()->isAdjacentToArea(getArea()))
				{
					if (pLoopCity->plot()->isFriendlyCity(*this, false))
					{
						for (int iBuildingClass = 0; iBuildingClass < GC.getNumBuildingClassInfos(); ++iBuildingClass)
						{
							if (m_pUnitInfo->isEvasionBuilding(iBuildingClass))
							{
								if (pLoopCity->isHasBuildingClass((BuildingClassTypes) iBuildingClass))
								{
									int iDistance = ::plotDistance(getX_INLINE(), getY_INLINE(), pLoopCity->getX_INLINE(), pLoopCity->getY_INLINE());
									if (iDistance < iBestDistance)
									{
										iBestDistance = iDistance;
										pBestCity = pLoopCity;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return pBestCity;
}

int CvUnit::cityAttackModifier() const
{
	return (m_pUnitInfo->getCityAttackModifier() + getExtraCityAttackPercent());
}


int CvUnit::cityDefenseModifier() const
{
	return (m_pUnitInfo->getCityDefenseModifier() + getExtraCityDefensePercent());
}

int CvUnit::hillsAttackModifier() const
{
	return (m_pUnitInfo->getHillsAttackModifier() + getExtraHillsAttackPercent());
}


int CvUnit::hillsDefenseModifier() const
{
	return (m_pUnitInfo->getHillsDefenseModifier() + getExtraHillsDefensePercent());
}


int CvUnit::terrainAttackModifier(TerrainTypes eTerrain) const
{
	FAssertMsg(eTerrain >= 0, "eTerrain is expected to be non-negative (invalid Index)");
	FAssertMsg(eTerrain < GC.getNumTerrainInfos(), "eTerrain is expected to be within maximum bounds (invalid Index)");
	return (m_pUnitInfo->getTerrainAttackModifier(eTerrain) + getExtraTerrainAttackPercent(eTerrain));
}


int CvUnit::terrainDefenseModifier(TerrainTypes eTerrain) const
{
	FAssertMsg(eTerrain >= 0, "eTerrain is expected to be non-negative (invalid Index)");
	FAssertMsg(eTerrain < GC.getNumTerrainInfos(), "eTerrain is expected to be within maximum bounds (invalid Index)");
	return (m_pUnitInfo->getTerrainDefenseModifier(eTerrain) + getExtraTerrainDefensePercent(eTerrain));
}


int CvUnit::featureAttackModifier(FeatureTypes eFeature) const
{
	FAssertMsg(eFeature >= 0, "eFeature is expected to be non-negative (invalid Index)");
	FAssertMsg(eFeature < GC.getNumFeatureInfos(), "eFeature is expected to be within maximum bounds (invalid Index)");
	return (m_pUnitInfo->getFeatureAttackModifier(eFeature) + getExtraFeatureAttackPercent(eFeature));
}

int CvUnit::featureDefenseModifier(FeatureTypes eFeature) const
{
	FAssertMsg(eFeature >= 0, "eFeature is expected to be non-negative (invalid Index)");
	FAssertMsg(eFeature < GC.getNumFeatureInfos(), "eFeature is expected to be within maximum bounds (invalid Index)");
	return (m_pUnitInfo->getFeatureDefenseModifier(eFeature) + getExtraFeatureDefensePercent(eFeature));
}

int CvUnit::unitClassAttackModifier(UnitClassTypes eUnitClass) const
{
	FAssertMsg(eUnitClass >= 0, "eUnitClass is expected to be non-negative (invalid Index)");
	FAssertMsg(eUnitClass < GC.getNumUnitClassInfos(), "eUnitClass is expected to be within maximum bounds (invalid Index)");
	return m_pUnitInfo->getUnitClassAttackModifier(eUnitClass) + getExtraUnitClassAttackModifier(eUnitClass);
}


int CvUnit::unitClassDefenseModifier(UnitClassTypes eUnitClass) const
{
	FAssertMsg(eUnitClass >= 0, "eUnitClass is expected to be non-negative (invalid Index)");
	FAssertMsg(eUnitClass < GC.getNumUnitClassInfos(), "eUnitClass is expected to be within maximum bounds (invalid Index)");
	return m_pUnitInfo->getUnitClassDefenseModifier(eUnitClass) + getExtraUnitClassDefenseModifier(eUnitClass);
}


int CvUnit::unitCombatModifier(UnitCombatTypes eUnitCombat) const
{
	FAssertMsg(eUnitCombat >= 0, "eUnitCombat is expected to be non-negative (invalid Index)");
	FAssertMsg(eUnitCombat < GC.getNumUnitCombatInfos(), "eUnitCombat is expected to be within maximum bounds (invalid Index)");
	return (m_pUnitInfo->getUnitCombatModifier(eUnitCombat) + getExtraUnitCombatModifier(eUnitCombat));
}


int CvUnit::domainModifier(DomainTypes eDomain) const
{
	FAssertMsg(eDomain >= 0, "eDomain is expected to be non-negative (invalid Index)");
	FAssertMsg(eDomain < NUM_DOMAIN_TYPES, "eDomain is expected to be within maximum bounds (invalid Index)");
	return (m_pUnitInfo->getDomainModifier(eDomain) + getExtraDomainModifier(eDomain));
}

int CvUnit::rebelModifier(PlayerTypes eOtherPlayer) const
{
	if (GET_PLAYER(getOwnerINLINE()).getParent() != eOtherPlayer)
	{
		return 0;
	}

	int iModifier = std::max(0, GET_TEAM(getTeam()).getRebelPercent() - GC.getDefineINT("REBEL_PERCENT_FOR_REVOLUTION"));

	iModifier *= GET_PLAYER(getOwnerINLINE()).getRebelCombatPercent();
	iModifier /= 100;

	if (!isHuman())
	{
		iModifier += GC.getHandicapInfo(GC.getGameINLINE().getHandicapType()).getAIKingCombatModifier();
	}

	return iModifier;
}


int CvUnit::bombardRate() const
{
	return (m_pUnitInfo->getBombardRate() + m_pUnitInfo->getBombardRate()*getExtraBombardRate()/100);
}


SpecialUnitTypes CvUnit::specialCargo() const
{
	return ((SpecialUnitTypes)(m_pUnitInfo->getSpecialCargo()));
}


DomainTypes CvUnit::domainCargo() const
{
	return ((DomainTypes)(m_pUnitInfo->getDomainCargo()));
}


int CvUnit::cargoSpace() const
{
	return m_iCargoCapacity;
}

void CvUnit::changeCargoSpace(int iChange)
{
	if (iChange != 0)
	{
		m_iCargoCapacity += iChange;
		FAssert(m_iCargoCapacity >= 0);
		setInfoBarDirty(true);
	}
}

bool CvUnit::isFull() const
{
	if (m_pUnitInfo->isMechUnit() && !isOnlyDefensive())
	{
		CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());
		if (!kOwner.isEurope())
		{
			return true;
		}
	}
	return (getCargo() >= cargoSpace() || isFullNewCargo());
}


int CvUnit::cargoSpaceAvailable(SpecialUnitTypes eSpecialCargo, DomainTypes eDomainCargo) const
{
	if (specialCargo() != NO_SPECIALUNIT)
	{
		if (specialCargo() != eSpecialCargo)
		{
			return 0;
		}
	}

	if (domainCargo() != NO_DOMAIN)
	{
		if (domainCargo() != eDomainCargo)
		{
			return 0;
		}
	}

	return std::max(0, (cargoSpace() - getCargo()));
}


bool CvUnit::hasCargo() const
{
	return (getCargo() > 0 || getOnlyNewCargo() > 0);
}


bool CvUnit::canCargoAllMove() const
{
	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pLoopUnit;
	CvPlot* pPlot;

	pPlot = plot();

	pUnitNode = pPlot->headUnitNode();

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);

		if (pLoopUnit->getTransportUnit() == this)
		{
			if (pLoopUnit->getDomainType() == DOMAIN_LAND)
			{
				if (!(pLoopUnit->canMove()))
				{
					return false;
				}
			}
		}
	}

	return true;
}

bool CvUnit::canCargoEnterArea(PlayerTypes ePlayer, const CvArea* pArea, bool bIgnoreRightOfPassage) const
{
	CvPlot* pPlot = plot();

	CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode();

	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);

		if (pLoopUnit->getTransportUnit() == this)
		{
			if (!pLoopUnit->canEnterArea(ePlayer, pArea, bIgnoreRightOfPassage))
			{
				return false;
			}
		}
	}

	return true;
}

int CvUnit::getUnitAICargo(UnitAITypes eUnitAI) const
{
	CLLNode<IDInfo>* pUnitNode;
	CvUnit* pLoopUnit;
	CvPlot* pPlot;
	int iCount;

	iCount = 0;

	pPlot = plot();

	pUnitNode = pPlot->headUnitNode();

	while (pUnitNode != NULL)
	{
		pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = pPlot->nextUnitNode(pUnitNode);

		if (pLoopUnit->getTransportUnit() == this)
		{
			if (pLoopUnit->AI_getUnitAIType() == eUnitAI)
			{
				iCount++;
			}
		}
	}

	return iCount;
}

bool CvUnit::canAssignTradeRoute(int iRouteID, bool bReusePath, bool bIgnorePath) const
{
	PROFILE_FUNC();

	if (cargoSpace() < 1 || GET_PLAYER(getOwnerINLINE()).getNumTradeRoutes() < 1)
	{
		return false;
	}

	CvSelectionGroup* pGroup = getGroup();
	if (pGroup == NULL)
	{
		return false;
	}

	if (getTradeRoute() != NULL)
	{
		return false;
	}

	std::vector<CvAgreement*> aiAgreements;
	GET_PLAYER(getOwnerINLINE()).getAgreements(aiAgreements);
	for (uint i = 0; i < aiAgreements.size(); ++i)
	{
		CvAgreement* pAgreement = aiAgreements[i];
		if (pGroup->isAssignedAgreement(pAgreement->getID()))
		{
			return false;
		}
	}

	if (iRouteID == -1)
	{
		return true;
	}

	PlayerTypes ePlayer = getOwnerINLINE();
	FAssert(ePlayer != NO_PLAYER);
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvTradeRoute* pTradeRoute = kPlayer.getTradeRoute(iRouteID);
	if (pTradeRoute == NULL)
	{
		return false;
	}

	if (pTradeRoute->getDestinationCity() == IDInfo(ePlayer, CvTradeRoute::EUROPE_CITY_ID) || pTradeRoute->getSourceCity() == IDInfo(ePlayer, CvTradeRoute::EUROPE_CITY_ID))
	{
		if (getDomainType() != DOMAIN_SEA)
		{
			return false;
		}
	}

	CvCity* pSource = ::getCity(pTradeRoute->getSourceCity());
	if (pSource != NULL && !bIgnorePath && !generatePath(pSource->plot(), 0, bReusePath))
	{
		return false;
	}

	CvCity* pDestination = ::getCity(pTradeRoute->getDestinationCity());
	if (pDestination != NULL && !bIgnorePath && !generatePath(pDestination->plot(), 0, bReusePath))
	{
		return false;
	}
	return true;
}

bool CvUnit::canAssignAgreement(int iRouteID, bool bReusePath) const
{
	if (cargoSpace() < 1 || GET_PLAYER(getOwnerINLINE()).getNumAgreements() < 1)
	{
		return false;
	}

	CvSelectionGroup* pGroup = getGroup();
	if (pGroup == NULL)
	{
		return false;
	}

	if (iRouteID == -1)
	{
		return true;
	}

	CLinkList<IDInfo> listCargo;
	pGroup->buildCargoUnitList(listCargo);
	CLLNode<IDInfo>* pUnitNode = listCargo.head();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = listCargo.next(pUnitNode);

		if (pLoopUnit->getYield() == NO_YIELD)
		{
			return false;
		}
	}

	PlayerTypes ePlayer = getOwnerINLINE();
	FAssert(ePlayer != NO_PLAYER);
	CvPlayer& kPlayer = GET_PLAYER(ePlayer);

	CvAgreement* pAgreement = kPlayer.getAgreement(iRouteID);
	if (pAgreement == NULL)
	{
		return false;
	}

	if (pAgreement->getDestinationCity() == IDInfo(ePlayer, CvAgreement::EUROPE_CITY_ID))
	{
		if (getDomainType() != DOMAIN_SEA)
		{
			return false;
		}
	}

	CvCity* pSource = ::getCity(pAgreement->getSourceCity());
	if (pSource == NULL || !generatePath(pSource->plot(), 0, bReusePath))
	{
		return false;
	}
	return true;

}

bool CvUnit::canAssignAgreements() const
{
	if (cargoSpace() < 1 || GET_PLAYER(getOwnerINLINE()).getNumAgreements() < 1)
	{
		return false;
	}
	CvSelectionGroup* pGroup = getGroup();
	if (pGroup == NULL)
	{
		return false;
	}
	if (getTradeRoute() != NULL)
	{
		return false;
	}
	CLinkList<IDInfo> listCargo;
	pGroup->buildCargoUnitList(listCargo);
	CLLNode<IDInfo>* pUnitNode = listCargo.head();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = listCargo.next(pUnitNode);

		if (pLoopUnit->getYield() == NO_YIELD)
		{
			return false;
		}
	}


	std::vector<CvAgreement*> aiAgreements;
	GET_PLAYER(getOwnerINLINE()).getAgreements(aiAgreements);
	for (uint i = 0; i < aiAgreements.size(); ++i)
	{
		CvAgreement* pAgreement = aiAgreements[i];
		if (pGroup->canAssignAgreement(pAgreement->getID()))
		{
			return true;
		}
	}

	return false;
}

int CvUnit::getID() const
{
	return m_iID;
}


int CvUnit::getIndex() const
{
	return (getID() & FLTA_INDEX_MASK);
}


IDInfo CvUnit::getIDInfo() const
{
	IDInfo unit(getOwnerINLINE(), getID());
	return unit;
}


void CvUnit::setID(int iID)
{
	m_iID = iID;
}


int CvUnit::getGroupID() const
{
	return m_iGroupID;
}


bool CvUnit::isInGroup() const
{
	return(getGroupID() != FFreeList::INVALID_INDEX);
}


bool CvUnit::isGroupHead() const // XXX is this used???
{
	return (getGroup()->getHeadUnit() == this);
}


CvSelectionGroup* CvUnit::getGroup() const
{
	return GET_PLAYER(getOwnerINLINE()).getSelectionGroup(getGroupID());
}

bool CvUnit::canJoinGroup(const CvPlot* pPlot, CvSelectionGroup* pSelectionGroup) const
{
	CvUnit* pHeadUnit;

	// do not allow someone to join a group that is about to be split apart
	// this prevents a case of a never-ending turn
	if (pSelectionGroup->AI_isForceSeparate())
	{
		return false;
	}

	if (pSelectionGroup->getOwnerINLINE() == NO_PLAYER)
	{
		pHeadUnit = pSelectionGroup->getHeadUnit();

		if (pHeadUnit != NULL)
		{
			if (pHeadUnit->getOwnerINLINE() != getOwnerINLINE())
			{
				return false;
			}
		}
	}
	else
	{
		if (pSelectionGroup->getOwnerINLINE() != getOwnerINLINE())
		{
			return false;
		}
	}

	if (pSelectionGroup->getNumUnits() > 0)
	{
		if (!(pSelectionGroup->atPlot(pPlot)))
		{
			return false;
		}

		if (pSelectionGroup->getDomainType() != getDomainType())
		{
			return false;
		}

		if (m_pUnitInfo->isPirate())
		{
			return false;
		}
	}

	return true;
}

void CvUnit::joinGroup(CvSelectionGroup* pSelectionGroup, bool bRemoveSelected, bool bRejoin)
{
	CvSelectionGroup* pOldSelectionGroup;
	CvSelectionGroup* pNewSelectionGroup;
	CvPlot* pPlot;

	pOldSelectionGroup = GET_PLAYER(getOwnerINLINE()).getSelectionGroup(getGroupID());

	if ((pSelectionGroup != pOldSelectionGroup) || (pOldSelectionGroup == NULL))
	{
		pPlot = plot();

		if (pSelectionGroup != NULL)
		{
			pNewSelectionGroup = pSelectionGroup;
		}
		else
		{
			if (bRejoin)
			{
				pNewSelectionGroup = GET_PLAYER(getOwnerINLINE()).addSelectionGroup();
				pNewSelectionGroup->init(pNewSelectionGroup->getID(), getOwnerINLINE());
			}
			else
			{
				pNewSelectionGroup = NULL;
			}
		}

		if ((pNewSelectionGroup == NULL) || canJoinGroup(plot(), pNewSelectionGroup))
		{
			if (pOldSelectionGroup != NULL)
			{
				bool bWasHead = false;
				if (!isHuman())
				{
					if (pOldSelectionGroup->getNumUnits() > 1)
					{
						if (pOldSelectionGroup->getHeadUnit() == this)
						{
							bWasHead = true;
						}
					}
				}

				pOldSelectionGroup->removeUnit(this);

				// if we were the head, if the head unitAI changed, then force the group to separate (non-humans)
				if (bWasHead && getConvoy() == NULL)
				{
					FAssert(pOldSelectionGroup->getHeadUnit() != NULL);
					if (pOldSelectionGroup->getHeadUnit()->AI_getUnitAIType() != AI_getUnitAIType())
					{
						pOldSelectionGroup->AI_makeForceSeparate();
					}
				}
			}

			if ((pNewSelectionGroup != NULL) && pNewSelectionGroup->addUnit(this, !isOnMap()))
			{
				m_iGroupID = pNewSelectionGroup->getID();
			}
			else
			{
				m_iGroupID = FFreeList::INVALID_INDEX;
			}

			if (getGroup() != NULL)
			{
				if (getGroup()->getNumUnits() > 1)
				{
					getGroup()->setActivityType(ACTIVITY_AWAKE);
				}
				else
				{
					GET_PLAYER(getOwnerINLINE()).updateGroupCycle(this);
				}
			}

			if (getTeam() == GC.getGameINLINE().getActiveTeam())
			{
				if (pPlot != NULL)
				{
					pPlot->setFlagDirty(true);
				}
			}

			if (pPlot == gDLL->getInterfaceIFace()->getSelectionPlot())
			{
				gDLL->getInterfaceIFace()->setDirty(PlotListButtons_DIRTY_BIT, true);
			}
		}

		if (bRemoveSelected)
		{
			if (IsSelected())
			{
				gDLL->getInterfaceIFace()->removeFromSelectionList(this);
			}
		}
	}
}


int CvUnit::getHotKeyNumber()
{
	return m_iHotKeyNumber;
}


void CvUnit::setHotKeyNumber(int iNewValue)
{
	CvUnit* pLoopUnit;
	int iLoop;

	FAssert(getOwnerINLINE() != NO_PLAYER);

	if (getHotKeyNumber() != iNewValue)
	{
		if (iNewValue != -1)
		{
			for (pLoopUnit = GET_PLAYER(getOwnerINLINE()).firstUnit(&iLoop); pLoopUnit != NULL; pLoopUnit = GET_PLAYER(getOwnerINLINE()).nextUnit(&iLoop))
			{
				if (pLoopUnit->getHotKeyNumber() == iNewValue)
				{
					pLoopUnit->setHotKeyNumber(-1);
				}
			}
		}

		m_iHotKeyNumber = iNewValue;

		if (IsSelected())
		{
			gDLL->getInterfaceIFace()->setDirty(InfoPane_DIRTY_BIT, true);
		}
	}
}


int CvUnit::getX() const
{
	return m_iX;
}


int CvUnit::getY() const
{
	return m_iY;
}


void CvUnit::setXY(int iX, int iY, bool bGroup, bool bUpdate, bool bShow, bool bCheckPlotVisible)
{
	CLLNode<IDInfo>* pUnitNode;
	CvCity* pOldCity;
	CvCity* pNewCity;
	CvCity* pWorkingCity;
	CvUnit* pTransportUnit;
	CvUnit* pLoopUnit;
	CvPlot* pOldPlot;
	CvPlot* pNewPlot;
	CvPlot* pLoopPlot;
	CLinkList<IDInfo> oldUnits;
	ActivityTypes eOldActivityType;
	int iI;

	// OOS!! Temporary for Out-of-Sync madness debugging...
	if (GC.getLogging())
	{
		if (gDLL->getChtLvl() > 0)
		{
			char szOut[1024];
			sprintf(szOut, "Player %d Unit %d (%S's %S) moving from %d:%d to %d:%d\n", getOwnerINLINE(), getID(), GET_PLAYER(getOwnerINLINE()).getNameKey(), getName().GetCString(), getX_INLINE(), getY_INLINE(), iX, iY);
			gDLL->messageControlLog(szOut);
		}
	}

	FAssert(!at(iX, iY) || (iX == INVALID_PLOT_COORD) || (iY == INVALID_PLOT_COORD));
	FAssert(!isFighting());
	FAssert((iX == INVALID_PLOT_COORD) || (GC.getMapINLINE().plotINLINE(iX, iY)->getX_INLINE() == iX));
	FAssert((iY == INVALID_PLOT_COORD) || (GC.getMapINLINE().plotINLINE(iX, iY)->getY_INLINE() == iY));

	if (getGroup() != NULL)
	{
		eOldActivityType = getGroup()->getActivityType();
	}
	else
	{
		eOldActivityType = NO_ACTIVITY;
	}

	if (!bGroup)
	{
		joinGroup(NULL, true);
	}

	pNewPlot = GC.getMapINLINE().plotINLINE(iX, iY);

	if (pNewPlot != NULL)
	{
		pTransportUnit = getTransportUnit();

		if (pTransportUnit != NULL)
		{
			if (!(pTransportUnit->atPlot(pNewPlot)))
			{
				setTransportUnit(NULL);
			}
		}

		if (canFight() && isOnMap())
		{
			oldUnits.clear();

			pUnitNode = pNewPlot->headUnitNode();

			while (pUnitNode != NULL)
			{
				oldUnits.insertAtEnd(pUnitNode->m_data);
				pUnitNode = pNewPlot->nextUnitNode(pUnitNode);
			}

			pUnitNode = oldUnits.head();

			while (pUnitNode != NULL)
			{
				pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = oldUnits.next(pUnitNode);

				if (pLoopUnit != NULL && pLoopUnit->isOnMap())
				{
					if (isEnemy(pLoopUnit->getTeam(), pNewPlot) || pLoopUnit->isEnemy(getTeam()))
					{
						if (!pLoopUnit->canCoexistWithEnemyUnit(getTeam()) && !pLoopUnit->isFighting())
						{
							if (NO_UNITCLASS == pLoopUnit->getUnitInfo().getUnitCaptureClassType() && pLoopUnit->canDefend(pNewPlot))
							{
								pLoopUnit->jumpToNearestValidPlot(); // can kill unit
							}
							else
							{
								if (!m_pUnitInfo->isHiddenNationality() && !pLoopUnit->getUnitInfo().isHiddenNationality())
								{
									GET_TEAM(getTeam()).AI_changeWarSuccess(pLoopUnit->getTeam(), GC.getDefineINT("WAR_SUCCESS_UNIT_CAPTURING"));
								}

								if (!isNoUnitCapture() && !m_pUnitInfo->isMechUnit())
								{
									pLoopUnit->setCapturingPlayer(getOwnerINLINE());
								}
								pLoopUnit->kill(false, this);
							}
						}
					}
				}
			}
		}
	}

	pOldPlot = plot();

	if (pOldPlot != NULL)
	{
		pOldPlot->removeUnit(this, bUpdate);

		pOldPlot->changeAdjacentSight(getTeam(), visibilityRange(), false, this);

		pOldPlot->area()->changeUnitsPerPlayer(getOwnerINLINE(), -1);
		pOldPlot->area()->changePower(getOwnerINLINE(), -getPower());

		if (AI_getUnitAIType() != NO_UNITAI)
		{
			pOldPlot->area()->changeNumAIUnits(getOwnerINLINE(), AI_getUnitAIType(), -1);
		}

		setLastMoveTurn(GC.getGameINLINE().getTurnSlice());

		pOldCity = pOldPlot->getPlotCity();

		pWorkingCity = pOldPlot->getWorkingCity();

		if (pWorkingCity != NULL)
		{
			if (canSiege(pWorkingCity->getTeam()))
			{
				pWorkingCity->AI_setAssignWorkDirty(true);
			}
		}

		if (pOldPlot->isWater())
		{
			for (iI = 0; iI < NUM_DIRECTION_TYPES; iI++)
			{
				pLoopPlot = plotDirection(pOldPlot->getX_INLINE(), pOldPlot->getY_INLINE(), ((DirectionTypes)iI));

				if (pLoopPlot != NULL)
				{
					if (pLoopPlot->isWater())
					{
						pWorkingCity = pLoopPlot->getWorkingCity();

						if (pWorkingCity != NULL)
						{
							if (canSiege(pWorkingCity->getTeam()))
							{
								pWorkingCity->AI_setAssignWorkDirty(true);
							}
						}
					}
				}
			}
		}

		if (pOldPlot->isActiveVisible(true))
		{
			pOldPlot->updateMinimapColor();
		}

		if (pOldPlot == gDLL->getInterfaceIFace()->getSelectionPlot())
		{
			gDLL->getInterfaceIFace()->verifyPlotListColumn();

			gDLL->getInterfaceIFace()->setDirty(PlotListButtons_DIRTY_BIT, true);
		}
	}

	if (pNewPlot != NULL)
	{
		m_iX = pNewPlot->getX_INLINE();
		m_iY = pNewPlot->getY_INLINE();
	}
	else
	{
		m_iX = INVALID_PLOT_COORD;
		m_iY = INVALID_PLOT_COORD;
		AI_setMovePriority(0);
	}

	FAssertMsg(plot() == pNewPlot, "plot is expected to equal pNewPlot");

	if (pNewPlot != NULL)
	{
		pNewCity = pNewPlot->getPlotCity();

		if (pNewCity != NULL)
		{
			if (isEnemy(pNewCity->getTeam()) && !canCoexistWithEnemyUnit(pNewCity->getTeam()) && canFight())
			{
				bool acquireCity = true;
				if (isNative() && pNewCity->getPopulation() > 0)
				{
					if (!raidWeapons(pNewCity))
					{
						raidGoods(pNewCity);
					}
					acquireCity = false;
				}
				if (acquireCity)
				{
					GET_TEAM(getTeam()).AI_changeWarSuccess(pNewCity->getTeam(), GC.getDefineINT("WAR_SUCCESS_CITY_CAPTURING"));
					PlayerTypes eNewOwner = GET_PLAYER(getOwnerINLINE()).pickConqueredCityOwner(*pNewCity);

					if (NO_PLAYER != eNewOwner)
					{
						GET_PLAYER(eNewOwner).acquireCity(pNewCity, true, false); // will delete the pointer
						pNewCity = NULL;
					}
				}
			}
		}

		//update facing direction
		if (pOldPlot != NULL)
		{
			DirectionTypes newDirection = estimateDirection(pOldPlot, pNewPlot);
			if (newDirection != NO_DIRECTION)
				m_eFacingDirection = newDirection;
		}

		//update cargo mission animations
		if (isCargo())
		{
			if (eOldActivityType != ACTIVITY_MISSION)
			{
				getGroup()->setActivityType(eOldActivityType);
			}
		}

		setFortifyTurns(0);

		pNewPlot->changeAdjacentSight(getTeam(), visibilityRange(), true, this); // needs to be here so that the square is considered visible when we move into it...

		pNewPlot->addUnit(this, bUpdate);

		pNewPlot->area()->changeUnitsPerPlayer(getOwnerINLINE(), 1);
		pNewPlot->area()->changePower(getOwnerINLINE(), getPower());

		if (AI_getUnitAIType() != NO_UNITAI)
		{
			pNewPlot->area()->changeNumAIUnits(getOwnerINLINE(), AI_getUnitAIType(), 1);
		}

		if (shouldLoadOnMove(pNewPlot))
		{
			load(false);
		}

		for (int iDX = -1; iDX <= 1; ++iDX)
		{
			for (int iDY = -1; iDY <= 1; ++iDY)
			{
				CvPlot* pLoopPlot = ::plotXY(getX_INLINE(), getY_INLINE(), iDX, iDY);
				if (pLoopPlot != NULL)
				{
					for (iI = 0; iI < MAX_TEAMS; iI++)
					{
						TeamTypes eLoopTeam = (TeamTypes) iI;
						if (GET_TEAM(eLoopTeam).isAlive())
						{
							if (!isInvisible(eLoopTeam, false) && getVisualOwner(eLoopTeam) == getOwnerINLINE())
							{
								if (pLoopPlot->plotCount(PUF_isVisualTeam, eLoopTeam, getTeam(), NO_PLAYER, eLoopTeam, PUF_isVisible, getOwnerINLINE(), -1) > 0)
								{
									if (!GET_TEAM(eLoopTeam).hasNativePlayer() || (!m_pUnitInfo->isMechUnit() && getTransportUnit() == NULL) || pNewCity != NULL)
									{
										GET_TEAM(eLoopTeam).meet(getTeam(), true);
									}
								}
							}
						}
					}

					if (pLoopPlot->isOwned() && getVisualOwner(pLoopPlot->getTeam()) == getOwnerINLINE())
					{
						if (pLoopPlot->isCity() || !GET_PLAYER(pLoopPlot->getOwnerINLINE()).isAlwaysOpenBorders())
						{
							if (!GET_TEAM(pLoopPlot->getTeam()).hasNativePlayer() || (!m_pUnitInfo->isMechUnit() && getTransportUnit() == NULL) || pNewCity != NULL)
							{
								GET_TEAM(pLoopPlot->getTeam()).meet(getTeam(), true);
							}
						}
					}
				}
			}
		}

		pNewCity = pNewPlot->getPlotCity();

		pWorkingCity = pNewPlot->getWorkingCity();

		if (pWorkingCity != NULL)
		{
			if (canSiege(pWorkingCity->getTeam()))
			{
				pWorkingCity->verifyWorkingPlot(pWorkingCity->getCityPlotIndex(pNewPlot));
			}
		}

		if (pNewPlot->isWater())
		{
			for (iI = 0; iI < NUM_DIRECTION_TYPES; iI++)
			{
				pLoopPlot = plotDirection(pNewPlot->getX_INLINE(), pNewPlot->getY_INLINE(), ((DirectionTypes)iI));

				if (pLoopPlot != NULL)
				{
					if (pLoopPlot->isWater())
					{
						pWorkingCity = pLoopPlot->getWorkingCity();

						if (pWorkingCity != NULL)
						{
							if (canSiege(pWorkingCity->getTeam()))
							{
								pWorkingCity->verifyWorkingPlot(pWorkingCity->getCityPlotIndex(pLoopPlot));
							}
						}
					}
				}
			}
		}

		if (pNewPlot->isActiveVisible(true))
		{
			pNewPlot->updateMinimapColor();
		}

		if (GC.IsGraphicsInitialized())
		{
			//override bShow if check plot visible
			if (bCheckPlotVisible)
			{
				if (!pNewPlot->isActiveVisible(true) && ((pOldPlot == NULL) || !pOldPlot->isActiveVisible(true)))
				{
					bShow = false;
				}
			}

			if (bShow)
			{
				QueueMove(pNewPlot);
			}
			else
			{
				SetPosition(pNewPlot);
			}
		}

		if (pNewPlot == gDLL->getInterfaceIFace()->getSelectionPlot())
		{
			gDLL->getInterfaceIFace()->verifyPlotListColumn();

			gDLL->getInterfaceIFace()->setDirty(PlotListButtons_DIRTY_BIT, true);
		}
	}

	if (pOldPlot != NULL && pNewPlot != NULL)
	{
		if (hasCargo())
		{
			pNewCity = pNewPlot->getPlotCity();
			int iNumPrisoners = 0;

			pUnitNode = pOldPlot->headUnitNode();
			while (pUnitNode != NULL)
			{
				pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pOldPlot->nextUnitNode(pUnitNode);

				if (pLoopUnit->getTransportUnit() == this)
				{
					if (pNewCity != NULL && pLoopUnit->getProfession() == PROFESSION_PRISONER)
					{
						pLoopUnit->kill(false);
						iNumPrisoners += 1;
					}
					else
					{
						pLoopUnit->setXY(iX, iY, bGroup, bUpdate);

						if (pLoopUnit->getYield() != NO_YIELD)
						{
							pNewPlot->addCrumbs(10);
						}
					}
				}
			}

			if (hasTempCrew())
			{
				if (pNewCity != NULL && pNewCity->getOwner() == getOwnerINLINE())
				{
					setTempCrew(false);
					setHasCrew(false);
					gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), gDLL->getText("TXT_KEY_INFO_TEMP_CREW_LEAVE"), "AS2D_POSITIVE_DINK", MESSAGE_TYPE_INFO);
				}
			}
			if (getOnlyNewCargo() > 0)
			{
				pNewPlot->addCrumbs(10);
			}
			if (iNumPrisoners > 0 && isHuman())
			{
				gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), gDLL->getText("TXT_KEY_INFO_PRISONERS_FREEDOM"), "AS2D_POSITIVE_DINK", MESSAGE_TYPE_INFO);
			}
		}
		if (m_pUnitInfo->isMechUnit() && isHuman())
		{
			CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());
			if (kOwner.getNumCities() == 1 && kOwner.getNumSeaways() == 0)
			{
				if (pNewPlot->isEurope())
				{
					getGroup()->pushMission(MISSION_SKIP);					
					CvPopupInfo* pPopupInfo = new CvPopupInfo(BUTTONPOPUP_MAKE_THE_FIRST_SEAWAY, getID());
					gDLL->getInterfaceIFace()->addPopup(pPopupInfo, getOwnerINLINE());
				}
			}
		}
	}

	FAssert(pOldPlot != pNewPlot || pNewPlot == NULL);
	GET_PLAYER(getOwnerINLINE()).updateGroupCycle(this);

	setInfoBarDirty(true);

	if (IsSelected())
	{
		gDLL->getInterfaceIFace()->setDirty(ColoredPlots_DIRTY_BIT, true);
	}

	//update glow
	gDLL->getEntityIFace()->updateEnemyGlow(getUnitEntity());

	// report event to Python, along with some other key state
	gDLL->getEventReporterIFace()->unitSetXY(pNewPlot, this);

	if (pNewPlot != NULL)
	{
		if (pNewPlot->isGoody(getTeam()))
		{
			GET_PLAYER(getOwnerINLINE()).doGoody(pNewPlot, this);
		}
	}
}

bool CvUnit::at(int iX, int iY) const
{
	return((getX_INLINE() == iX) && (getY_INLINE() == iY));
}


bool CvUnit::atPlot(const CvPlot* pPlot) const
{
	return (plot() == pPlot);
}


CvPlot* CvUnit::plot() const
{
	if ((getX_INLINE() == INVALID_PLOT_COORD) || (getY_INLINE() == INVALID_PLOT_COORD))
	{
		CvCity *pCity = GET_PLAYER(getOwnerINLINE()).getPopulationUnitCity(getID());
		if (pCity == NULL)
		{
			return NULL;
		}
		else
		{
			return pCity->plot();
		}
	}
	else
	{
		return GC.getMapINLINE().plotSorenINLINE(getX_INLINE(), getY_INLINE());
	}
}

CvCity* CvUnit::getCity() const
{
	CvPlot* pPlot = plot();
	if (pPlot != NULL)
	{
		return pPlot->getPlotCity();
	}
	return NULL;
}

int CvUnit::getArea() const
{
	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return FFreeList::INVALID_INDEX;
	}

	return pPlot->getArea();
}


CvArea* CvUnit::area() const
{
	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return NULL;
	}

	return pPlot->area();
}


int CvUnit::getLastMoveTurn() const
{
	return m_iLastMoveTurn;
}


void CvUnit::setLastMoveTurn(int iNewValue)
{
	m_iLastMoveTurn = iNewValue;
	FAssert(getLastMoveTurn() >= 0);
}


int CvUnit::getGameTurnCreated() const
{
	return m_iGameTurnCreated;
}


void CvUnit::setGameTurnCreated(int iNewValue)
{
	m_iGameTurnCreated = iNewValue;
	FAssert(getGameTurnCreated() >= 0);
}

int CvUnit::getDamage() const
{
	return m_iDamage;
}

void CvUnit::setDamage(int iNewValue, CvUnit* pAttacker, bool bNotifyEntity)
{
	int iOldValue;

	iOldValue = getDamage();

	m_iDamage = range(iNewValue, 0, maxHitPoints());

	FAssertMsg(currHitPoints() >= 0, "currHitPoints() is expected to be non-negative (invalid Index)");

	if ((iOldValue != getDamage()) && isOnMap())
	{
		if (GC.getGameINLINE().isFinalInitialized() && bNotifyEntity)
		{
			NotifyEntity(MISSION_DAMAGE);
		}

		setInfoBarDirty(true);

		if (IsSelected())
		{
			gDLL->getInterfaceIFace()->setDirty(InfoPane_DIRTY_BIT, true);
		}

		if (plot() == gDLL->getInterfaceIFace()->getSelectionPlot())
		{
			gDLL->getInterfaceIFace()->setDirty(PlotListButtons_DIRTY_BIT, true);
		}
	}

	if (isDead())
	{
		kill(true, pAttacker);
	}
}

void CvUnit::changeDamage(int iChange, CvUnit* pAttacker)
{
	setDamage((getDamage() + iChange), pAttacker);
}

int CvUnit::getMoves() const
{
	return m_iMoves;
}

void CvUnit::setMoves(int iNewValue)
{
	CvPlot* pPlot;

	if (getMoves() != iNewValue)
	{
		pPlot = plot();

		m_iMoves = iNewValue;

		FAssert(getMoves() >= 0);

		if (getTeam() == GC.getGameINLINE().getActiveTeam())
		{
			if (pPlot != NULL)
			{
				pPlot->setFlagDirty(true);
			}
		}

		if (IsSelected())
		{
			gDLL->getFAStarIFace()->ForceReset(&GC.getInterfacePathFinder());

			gDLL->getInterfaceIFace()->setDirty(InfoPane_DIRTY_BIT, true);
		}

		if (pPlot == gDLL->getInterfaceIFace()->getSelectionPlot())
		{
			gDLL->getInterfaceIFace()->setDirty(PlotListButtons_DIRTY_BIT, true);
		}
	}
}

void CvUnit::changeMoves(int iChange)
{
	setMoves(getMoves() + iChange);
}

void CvUnit::finishMoves()
{
	setMoves(maxMoves());
}

int CvUnit::getExperience() const
{
	return m_iExperience;
}

void CvUnit::setExperience(int iNewValue, int iMax)
{
	if ((getExperience() != iNewValue) && (getExperience() < ((iMax == -1) ? MAX_INT : iMax)))
	{
		m_iExperience = std::min(((iMax == -1) ? MAX_INT : iMax), iNewValue);
		FAssert(getExperience() >= 0);

		if (IsSelected())
		{
			gDLL->getInterfaceIFace()->setDirty(InfoPane_DIRTY_BIT, true);
		}
	}
}

void CvUnit::changeExperience(int iChange, int iMax, bool bFromCombat, bool bInBorders, bool bUpdateGlobal)
{
	int iUnitExperience = iChange;

	if (bFromCombat)
	{
		CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());

		int iCombatExperienceMod = 100 + kPlayer.getGreatGeneralRateModifier();

		if (bInBorders)
		{
			iCombatExperienceMod += kPlayer.getDomesticGreatGeneralRateModifier() + kPlayer.getExpInBorderModifier();
			iUnitExperience += (iChange * kPlayer.getExpInBorderModifier()) / 100;
		}

		if (bUpdateGlobal)
		{
			int iGlobalExp = (iChange * iCombatExperienceMod) / (100*10);//We added facter 10 for xp so we we have to divde by 10 xp for obtain generals
			if (getDomainType() == DOMAIN_LAND)
			{
				kPlayer.changeCombatLandExperience(iGlobalExp);
			}
			else if (getDomainType() == DOMAIN_SEA)
			{
				kPlayer.changeCombatSeaExperience(iGlobalExp);
			}
		}

		if (getExperiencePercent() != 0)
		{
			iUnitExperience *= std::max(0, 100 + getExperiencePercent());
			iUnitExperience /= 100;
		}
	}

	setExperience((getExperience() + iUnitExperience), iMax);
}

int CvUnit::getLevel() const
{
	return m_iLevel;
}

void CvUnit::setLevel(int iNewValue)
{
	if (getLevel() != iNewValue)
	{
		m_iLevel = iNewValue;
		FAssert(getLevel() >= 0);

		if (getLevel() > GET_PLAYER(getOwnerINLINE()).getHighestUnitLevel())
		{
			GET_PLAYER(getOwnerINLINE()).setHighestUnitLevel(getLevel());
		}

		if (IsSelected())
		{
			gDLL->getInterfaceIFace()->setDirty(InfoPane_DIRTY_BIT, true);
		}
	}
}

void CvUnit::changeLevel(int iChange)
{
	setLevel(getLevel() + iChange);
}

int CvUnit::getCargo() const
{
	return m_iCargo;
}

void CvUnit::changeCargo(int iChange)
{
	m_iCargo += iChange;
	FAssert(getCargo() >= 0);
}

int CvUnit::getNewCargo(bool bShowTreasure) const
{
	int iResult = m_iNewCargo;
	CLLNode<IDInfo>* pUnitNode;
	CvPlot* pPlot = plot();
	if (pPlot != NULL)
	{
		pUnitNode = pPlot->headUnitNode();
		while (pUnitNode != NULL)
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pPlot->nextUnitNode(pUnitNode);
			if (pLoopUnit->getTransportUnit() == this)
			{
				if (bShowTreasure || !pLoopUnit->getUnitInfo().isTreasure())
				{
					iResult += 30;
				}
			}
		}
	}
	return iResult;
}

void CvUnit::changeNewCargo(int iChange)
{
	m_iNewCargo += iChange;
	FAssert(getOnlyNewCargo() >= 0);
}
void CvUnit::setNewCargo(int iNewValue)
{
	if (iNewValue != getOnlyNewCargo())
	{
		m_iNewCargo = iNewValue;
	}
}

bool CvUnit::isFullNewCargo() const
{
	return (getNewCargo() == m_pUnitInfo->getCargoNewSpace());
}

int CvUnit::getSpaceNewCargo() const
{
	return (m_pUnitInfo->getCargoNewSpace() - getNewCargo());
}

int CvUnit::getOnlyNewCargo() const
{
	return m_iNewCargo;
}

CvPlot* CvUnit::getAttackPlot() const
{
	return GC.getMapINLINE().plotSorenINLINE(m_iAttackPlotX, m_iAttackPlotY);
}


void CvUnit::setAttackPlot(const CvPlot* pNewValue)
{
	if (getAttackPlot() != pNewValue)
	{
		if (pNewValue != NULL)
		{
			m_iAttackPlotX = pNewValue->getX_INLINE();
			m_iAttackPlotY = pNewValue->getY_INLINE();
		}
		else
		{
			m_iAttackPlotX = INVALID_PLOT_COORD;
			m_iAttackPlotY = INVALID_PLOT_COORD;
		}
	}
}

int CvUnit::getCombatTimer() const
{
	return m_iCombatTimer;
}


void CvUnit::setCombatTimer(int iNewValue)
{
	m_iCombatTimer = iNewValue;
	FAssert(getCombatTimer() >= 0);
}


void CvUnit::changeCombatTimer(int iChange)
{
	setCombatTimer(getCombatTimer() + iChange);
}

int CvUnit::getCombatDamage() const
{
	return m_iCombatDamage;
}


void CvUnit::setCombatDamage(int iNewValue)
{
	m_iCombatDamage = iNewValue;
	FAssert(getCombatDamage() >= 0);
}


int CvUnit::getFortifyTurns() const
{
	return m_iFortifyTurns;
}


void CvUnit::setFortifyTurns(int iNewValue)
{
	iNewValue = range(iNewValue, 0, GC.getDefineINT("MAX_FORTIFY_TURNS"));

	if (iNewValue != getFortifyTurns())
	{
		m_iFortifyTurns = iNewValue;
		setInfoBarDirty(true);
	}
}


void CvUnit::changeFortifyTurns(int iChange)
{
	setFortifyTurns(getFortifyTurns() + iChange);
}


int CvUnit::getBlitzCount() const
{
	return m_iBlitzCount;
}


bool CvUnit::isBlitz() const
{
	return (getBlitzCount() > 0);
}


void CvUnit::changeBlitzCount(int iChange)
{
	m_iBlitzCount = (m_iBlitzCount + iChange);
	FAssert(getBlitzCount() >= 0);
}

int CvUnit::getAmphibCount() const
{
	return m_iAmphibCount;
}


bool CvUnit::isAmphib() const
{
	return (getAmphibCount() > 0);
}


void CvUnit::changeAmphibCount(int iChange)
{
	m_iAmphibCount = (m_iAmphibCount + iChange);
	FAssert(getAmphibCount() >= 0);
}


int CvUnit::getRiverCount() const
{
	return m_iRiverCount;
}


bool CvUnit::isRiver() const
{
	return (getRiverCount() > 0);
}


void CvUnit::changeRiverCount(int iChange)
{
	m_iRiverCount = (m_iRiverCount + iChange);
	FAssert(getRiverCount() >= 0);
}


int CvUnit::getEnemyRouteCount() const
{
	return m_iEnemyRouteCount;
}


bool CvUnit::isEnemyRoute() const
{
	return (getEnemyRouteCount() > 0);
}


void CvUnit::changeEnemyRouteCount(int iChange)
{
	m_iEnemyRouteCount = (m_iEnemyRouteCount + iChange);
	FAssert(getEnemyRouteCount() >= 0);
}


int CvUnit::getAlwaysHealCount() const
{
	return m_iAlwaysHealCount;
}


bool CvUnit::isAlwaysHeal() const
{
	return (getAlwaysHealCount() > 0);
}


void CvUnit::changeAlwaysHealCount(int iChange)
{
	m_iAlwaysHealCount = (m_iAlwaysHealCount + iChange);
	FAssert(getAlwaysHealCount() >= 0);
}


int CvUnit::getHillsDoubleMoveCount() const
{
	return m_iHillsDoubleMoveCount;
}


bool CvUnit::isHillsDoubleMove() const
{
	return (getHillsDoubleMoveCount() > 0);
}


void CvUnit::changeHillsDoubleMoveCount(int iChange)
{
	m_iHillsDoubleMoveCount = (m_iHillsDoubleMoveCount + iChange);
	FAssert(getHillsDoubleMoveCount() >= 0);
}

int CvUnit::getExtraVisibilityRange() const
{
	return m_iExtraVisibilityRange;
}


void CvUnit::changeExtraVisibilityRange(int iChange)
{
	if (iChange != 0)
	{
		plot()->changeAdjacentSight(getTeam(), visibilityRange(), false, this);

		m_iExtraVisibilityRange += iChange;
		FAssert(getExtraVisibilityRange() >= 0);

		plot()->changeAdjacentSight(getTeam(), visibilityRange(), true, this);
	}
}


int CvUnit::getExtraMoves() const
{
	return m_iExtraMoves;
}


void CvUnit::changeExtraMoves(int iChange)
{
	m_iExtraMoves += iChange;
	FAssert(getExtraMoves() >= 0);
}


int CvUnit::getExtraMoveDiscount() const
{
	return m_iExtraMoveDiscount;
}


void CvUnit::changeExtraMoveDiscount(int iChange)
{
	m_iExtraMoveDiscount = (m_iExtraMoveDiscount + iChange);
	FAssert(getExtraMoveDiscount() >= 0);
}

int CvUnit::getExtraWithdrawal() const
{
	return m_iExtraWithdrawal;
}


void CvUnit::changeExtraWithdrawal(int iChange)
{
	m_iExtraWithdrawal = (m_iExtraWithdrawal + iChange);
	FAssert(getExtraWithdrawal() >= 0);
}

int CvUnit::getExtraBombardRate() const
{
	return m_iExtraBombardRate;
}


void CvUnit::changeExtraBombardRate(int iChange)
{
	m_iExtraBombardRate = (m_iExtraBombardRate + iChange);
	FAssert(getExtraBombardRate() >= 0);
}


int CvUnit::getExtraEnemyHeal() const
{
	return m_iExtraEnemyHeal;
}


void CvUnit::changeExtraEnemyHeal(int iChange)
{
	m_iExtraEnemyHeal = (m_iExtraEnemyHeal + iChange);
	FAssert(getExtraEnemyHeal() >= 0);
}


int CvUnit::getExtraNeutralHeal() const
{
	return m_iExtraNeutralHeal;
}


void CvUnit::changeExtraNeutralHeal(int iChange)
{
	m_iExtraNeutralHeal = (m_iExtraNeutralHeal + iChange);
	FAssert(getExtraNeutralHeal() >= 0);
}


int CvUnit::getExtraFriendlyHeal() const
{
	return m_iExtraFriendlyHeal;
}


void CvUnit::changeExtraFriendlyHeal(int iChange)
{
	m_iExtraFriendlyHeal = (m_iExtraFriendlyHeal + iChange);
	FAssert(getExtraFriendlyHeal() >= 0);
}


int CvUnit::getSameTileHeal() const
{
	return m_iSameTileHeal;
}


void CvUnit::changeSameTileHeal(int iChange)
{
	m_iSameTileHeal = (m_iSameTileHeal + iChange);
	FAssert(getSameTileHeal() >= 0);
}


int CvUnit::getAdjacentTileHeal() const
{
	return m_iAdjacentTileHeal;
}


void CvUnit::changeAdjacentTileHeal(int iChange)
{
	m_iAdjacentTileHeal = (m_iAdjacentTileHeal + iChange);
	FAssert(getAdjacentTileHeal() >= 0);
}


int CvUnit::getExtraCombatPercent() const
{
	return m_iExtraCombatPercent + GET_PLAYER(getOwnerINLINE()).getUnitStrengthModifier(getUnitClassType());
}


void CvUnit::changeExtraCombatPercent(int iChange)
{
	if (iChange != 0)
	{
		m_iExtraCombatPercent += iChange;

		setInfoBarDirty(true);
	}
}

int CvUnit::getExtraRealCombatPercent() const
{
	return m_iExtraRealCombatPercent;
}

void CvUnit::changeExtraRealCombatPercent(int iChange)
{
	if (iChange != 0)
	{
		m_iExtraRealCombatPercent += iChange;
	}
}

int CvUnit::getExtraCrossOceanSpeed() const
{
	return m_iExtraCrossOceanSpeed;
}

void CvUnit::changeExtraCrossOceanSpeed(int iChange)
{
	if (iChange != 0)
	{
		m_iExtraCrossOceanSpeed += iChange;
	}
}

int CvUnit::getExtraCityAttackPercent() const
{
	return m_iExtraCityAttackPercent;
}


void CvUnit::changeExtraCityAttackPercent(int iChange)
{
	if (iChange != 0)
	{
		m_iExtraCityAttackPercent = (m_iExtraCityAttackPercent + iChange);

		setInfoBarDirty(true);
	}
}


int CvUnit::getExtraCityDefensePercent() const
{
	return m_iExtraCityDefensePercent;
}


void CvUnit::changeExtraCityDefensePercent(int iChange)
{
	if (iChange != 0)
	{
		m_iExtraCityDefensePercent = (m_iExtraCityDefensePercent + iChange);

		setInfoBarDirty(true);
	}
}

int CvUnit::getExtraHillsAttackPercent() const
{
	return m_iExtraHillsAttackPercent;
}


void CvUnit::changeExtraHillsAttackPercent(int iChange)
{
	if (iChange != 0)
	{
		m_iExtraHillsAttackPercent = (m_iExtraHillsAttackPercent + iChange);

		setInfoBarDirty(true);
	}
}


int CvUnit::getExtraHillsDefensePercent() const
{
	return m_iExtraHillsDefensePercent;
}


void CvUnit::changeExtraHillsDefensePercent(int iChange)
{
	if (iChange != 0)
	{
		m_iExtraHillsDefensePercent = (m_iExtraHillsDefensePercent + iChange);

		setInfoBarDirty(true);
	}
}

int CvUnit::getPillageChange() const
{
	return m_iPillageChange;
}

void CvUnit::changePillageChange(int iChange)
{
	if (iChange != 0)
	{
		m_iPillageChange += iChange;

		setInfoBarDirty(true);
	}
}

int CvUnit::getUpgradeDiscount() const
{
	return m_iUpgradeDiscount;
}

void CvUnit::changeUpgradeDiscount(int iChange)
{
	if (iChange != 0)
	{
		m_iUpgradeDiscount += iChange;

		setInfoBarDirty(true);
	}
}

int CvUnit::getExperiencePercent() const
{
	return m_iExperiencePercent;
}

void CvUnit::changeExperiencePercent(int iChange)
{
	if (iChange != 0)
	{
		m_iExperiencePercent += iChange;

		setInfoBarDirty(true);
	}
}

int CvUnit::getAccuracyPercent() const
{
	return m_iAccuracyPercent;
}

void CvUnit::changeAccuracyPercent(int iChange)
{
	if (iChange != 0)
	{
		m_iAccuracyPercent += iChange;

		setInfoBarDirty(true);
	}
}

int CvUnit::getBombardmentPercent() const
{
	return m_iBombardmentPercent;
}

void CvUnit::changeBombardmentPercent(int iChange)
{
	if (iChange != 0)
	{
		m_iBombardmentPercent += iChange;

		setInfoBarDirty(true);
	}
}

DirectionTypes CvUnit::getFacingDirection(bool checkLineOfSightProperty) const
{
	if (checkLineOfSightProperty)
	{
		if (m_pUnitInfo->isLineOfSight())
		{
			return m_eFacingDirection; //only look in facing direction
		}
		else
		{
			return NO_DIRECTION; //look in all directions
		}
	}
	else
	{
		return m_eFacingDirection;
	}
}

void CvUnit::setFacingDirection(DirectionTypes eFacingDirection)
{
	if (eFacingDirection != m_eFacingDirection)
	{
		if (m_pUnitInfo->isLineOfSight())
		{
			//remove old fog
			plot()->changeAdjacentSight(getTeam(), visibilityRange(), false, this);

			//change direction
			m_eFacingDirection = eFacingDirection;

			//clear new fog
			plot()->changeAdjacentSight(getTeam(), visibilityRange(), true, this);

			gDLL->getInterfaceIFace()->setDirty(ColoredPlots_DIRTY_BIT, true);
		}
		else
		{
			m_eFacingDirection = eFacingDirection;
		}

		if (isOnMap())
		{
			//update formation
			NotifyEntity(NO_MISSION);
		}
	}
}

void CvUnit::rotateFacingDirectionClockwise()
{
	//change direction
	DirectionTypes eNewDirection = (DirectionTypes) ((m_eFacingDirection + 1) % NUM_DIRECTION_TYPES);
	setFacingDirection(eNewDirection);
}

void CvUnit::rotateFacingDirectionCounterClockwise()
{
	//change direction
	DirectionTypes eNewDirection = (DirectionTypes) ((m_eFacingDirection + NUM_DIRECTION_TYPES - 1) % NUM_DIRECTION_TYPES);
	setFacingDirection(eNewDirection);
}

ProfessionTypes CvUnit::getProfession() const
{
	return m_eProfession;
}

void CvUnit::setProfession(ProfessionTypes eProfession, bool bForce)
{
	if (!bForce && !canHaveProfession(eProfession, false))
	{
		FAssertMsg(false, "Unit can not have profession");
		return;
	}

	bool bDeleteMunition = true;
	ProfessionTypes eOldProfession = getProfession();
	CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());

	if (eOldProfession != eProfession)
	{
		if (eOldProfession != NO_PROFESSION)
		{
			if (GC.getProfessionInfo(eOldProfession).isCitizen())
			{
				AI_setOldProfession(eOldProfession);
			}
			if (eOldProfession == PROFESSION_STUDENT)
			{
				UnitTypes eLearningUnit = (UnitTypes) getLearningProfession();
				ProfessionTypes eFormationProfession = (ProfessionTypes) getFormationProfession();
				CvCity* pCity = kOwner.getPopulationUnitCity(getID());
				if (pCity != NULL && getYieldStored() == 0)
				{
					//Student don't learn anithing yet
					int iPrice = 0;
					if (eLearningUnit != NO_UNIT)
					{
						iPrice = pCity->getSpecialistTuition(eLearningUnit);
					}
					else if (eFormationProfession != NO_PROFESSION)
					{
						iPrice = 200;
					}
					if (iPrice > 0)
					{
						kOwner.changeGold(iPrice);
					}
				}
			}
			if (bForce)
			{
				int iMaxMunition = GC.getProfessionInfo(eProfession).getMaxMunition();
				if (iMaxMunition != -1)
				{
					setMunition(iMaxMunition);
				}
			}
			else
			{
				if (isHuman() && eProfession != NO_PROFESSION)
				{
					int iMaxMunitionOld = GC.getProfessionInfo(eOldProfession).getMaxMunition();
					int iMaxMunitionNew = GC.getProfessionInfo(eProfession).getMaxMunition();
					int iMunition = getMunition();
					int iCityMunition;

					//Out of cities and not in europe
					CvCity* pCity = NULL;
					CvPlot* pPlot = plot();
					if (pPlot != NULL)
					{
						if (pPlot->getOwnerINLINE() == getOwnerINLINE())
						{
							pCity = pPlot->getPlotCity();
						}
					}
					if (pCity == NULL)
					{
						pCity = kOwner.getPopulationUnitCity(getID());
					}


					if (pCity != NULL)
					{
						iCityMunition = pCity->getYieldStored(YIELD_AMMUNITION);
						if (iMaxMunitionOld != -1)
						{
							if (iMaxMunitionNew != -1)
							{
								bDeleteMunition = false;
								if (iMunition > iMaxMunitionNew)
								{
									pCity->changeYieldStored(YIELD_AMMUNITION, iMunition - iMaxMunitionNew);
									setMunition(iMaxMunitionNew);
								}
							}
							else
							{
								pCity->changeYieldStored(YIELD_AMMUNITION, iMunition);
								setMunition(0);
							}
						}
						else
						{
							if (iMaxMunitionNew != -1)
							{
								bDeleteMunition = false;
								if (iCityMunition >= iMaxMunitionNew)
								{
									pCity->changeYieldStored(YIELD_AMMUNITION, -iMaxMunitionNew);
									setMunition(iMaxMunitionNew);
								}
								else
								{
									if (iCityMunition != 0)
									{
										pCity->changeYieldStored(YIELD_AMMUNITION, -iCityMunition);
										setMunition(iCityMunition);
									}
								}
							}
						}
					}
				}
			}
		}
		if (!isHuman() && eProfession == PROFESSION_STUDENT)
		{
			CvCity* pCity = NULL;
			CvPlot* pPlot = plot();
			if (pPlot != NULL)
			{
				if (pPlot->getOwnerINLINE() == getOwnerINLINE())
				{
					pCity = pPlot->getPlotCity();
				}
			}
			if (pCity == NULL)
			{
				pCity = kOwner.getPopulationUnitCity(getID());
			}
			if (pCity != NULL)
			{
				UnitClassTypes eUnitClass = pCity->getBestTeachUnit(this);
				if (eUnitClass != NO_UNITCLASS)
				{
					UnitTypes eTeachUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClass);
					pCity->educateStudentStart(getID(), eTeachUnit);
				}
			}
		}

		bool isCitizenProfession = eProfession != NO_PROFESSION && GC.getProfessionInfo(eProfession).isCitizen();
		if (isOnMap() && isCitizenProfession)
		{
			CvCity* pCity = plot()->getPlotCity();
			if (pCity != NULL)
			{
				if (canJoinCity(plot()))
				{
					pCity->addPopulationUnit(this, eProfession);
					bool bLock = true;
					if (GC.getProfessionInfo(eProfession).isWorkPlot())
					{
						int iPlotIndex = pCity->AI_bestProfessionPlot(eProfession, this);
						if (iPlotIndex != -1)
						{
							pCity->alterUnitWorkingPlot(iPlotIndex, getID(), false);
						}
						else
						{
							bLock = false;
						}
					}

					if (isHuman())
					{
						int iMunition = getMunition();

						pCity->changeYieldStored(YIELD_AMMUNITION, iMunition);
						setMunition(0);
					}
					setColonistLocked(bLock);
					return;
				}
			}
		}

		processProfession(getProfession(), -1, false);
		if (getProfessionUnitCombatType(getProfession()) != getProfessionUnitCombatType(eProfession))
		{
			for (int iPromotion = 0; iPromotion < GC.getNumPromotionInfos(); ++iPromotion)
			{
				if (isHasRealPromotion((PromotionTypes) iPromotion))
				{
					processPromotion((PromotionTypes) iPromotion, -1);
				}
			}
		}
		ProfessionTypes eOldProfession = getProfession();
		m_eProfession = eProfession;
		if (getProfessionUnitCombatType(eOldProfession) != getProfessionUnitCombatType(getProfession()))
		{
			for (int iPromotion = 0; iPromotion < GC.getNumPromotionInfos(); ++iPromotion)
			{
				if (isHasRealPromotion((PromotionTypes) iPromotion))
				{
					processPromotion((PromotionTypes) iPromotion, 1);
				}
			}
		}
		processProfession(getProfession(), 1, true);
		if (isCitizenProfession && !isHuman())
		{
			bool shouldLockCitizen = eProfession == (ProfessionTypes) m_pUnitInfo->getIdealProfession();
			setColonistLocked(shouldLockCitizen);
		}
		//reload unit model
		reloadEntity();
		gDLL->getInterfaceIFace()->setDirty(Domestic_Advisor_DIRTY_BIT, true);
	}

	if (eProfession != NO_PROFESSION)
	{
		CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);
		if (!kProfession.isCitizen())
		{
			if (kProfession.getDefaultUnitAIType() != NO_UNITAI)
			{
				AI_setUnitAIType((UnitAITypes)kProfession.getDefaultUnitAIType());
			}
		}
	}

	//DOANE Patriot Promotion
	if (GET_PLAYER(getOwnerINLINE()).getCapitalCity() != NULL)
	{
		BuildingTypes pBuilding = NO_BUILDING;
		for (int iJ = 0; iJ < GC.getNumBuildingClassInfos(); iJ++)
		{
			if (GC.getBuildingInfo((BuildingTypes) iJ).isCapital())
			{
				pBuilding = (BuildingTypes) iJ;
			}
		}
		if (pBuilding != NO_BUILDING)
		{
			setHasRealPromotion(((PromotionTypes) GC.getBuildingInfo(pBuilding).getFreePromotionUnit()), true);
		}
	}
	//END DOANE Patriot Promotion

	if (isHuman())
	{
		CvPlot* pPlot = plot();
		if (pPlot != NULL)
		{
			CvCity* pCity = pPlot->getPlotCity();
			if (pCity != NULL && bDeleteMunition)
			{
				pCity->deleteMunitionCitizen();
			}
		}
	}
	if (eProfession != NO_PROFESSION && isHuman())
	{
		CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);
		if (!kProfession.isCitizen() && !kProfession.isUnarmed())
		{
			CvCity* pCity = NULL;
			CvPlot* pPlot = plot();
			if (pPlot != NULL)
			{
				if (pPlot->getOwnerINLINE() == getOwnerINLINE())
				{
					pCity = pPlot->getPlotCity();
					CvUnit* pTransportUnit = getTransportUnit();
					if (pTransportUnit != NULL && !pTransportUnit->getUnitInfo().isMechUnit())
					{
						setTransportUnit(NULL, false);
					}
				}
			}
			if (pCity == NULL)
			{
				pCity = kOwner.getPopulationUnitCity(getID());
			}
			if (pCity != NULL)
			{
				setHaveNewMilitaryProfession(true);
			}
		}
	}
}

bool CvUnit::canHaveProfession(ProfessionTypes eProfession, bool bBumpOther, const CvPlot* pPlot) const
{
	if (NO_PROFESSION == eProfession)
	{
		return true;
	}

	if (eProfession == getProfession())
	{
		return true;
	}

	if (eProfession == PROFESSION_PRISONER || getProfession() == PROFESSION_PRISONER)
	{
		return false;
	}

	if (eProfession == PROFESSION_SCOUT && canBeSailor()) 
	{
		return false;
	}

	CvProfessionInfo& kNewProfession = GC.getProfessionInfo(eProfession);
	CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());

	if (!kOwner.isProfessionValid(eProfession, getUnitType()))
	{
		return false;
	}

	if (kNewProfession.getYieldProduced() == YIELD_EDUCATION)
	{
		if (m_pUnitInfo->getStudentWeight() <= 0)
		{
			return false;
		}
	}

	if (pPlot == NULL)
	{
		pPlot = plot();
	}

	CvCity* pCity = NULL;
	if (pPlot != NULL)
	{
		if (pPlot->getOwnerINLINE() == getOwnerINLINE())
		{
			pCity = pPlot->getPlotCity();
		}
	}
	if (pCity == NULL)
	{
		pCity = kOwner.getPopulationUnitCity(getID());
	}

	bool bEuropeUnit = false;
	if (pCity == NULL)
	{
		CvUnit* pUnit = kOwner.getEuropeUnitById(getID());
		bEuropeUnit = (pUnit != NULL);
		FAssert(pUnit == this || pUnit == NULL);
	}

	if (pCity != NULL)
	{
		//make sure all equipment is available
		if (!pCity->AI_isWorkforceHack())
		{
			for (int i=0; i < NUM_YIELD_TYPES; ++i)
			{
				YieldTypes eYieldType = (YieldTypes) i;
				int iYieldCarried = 0;
				if (getProfession() != NO_PROFESSION)
				{
					iYieldCarried += kOwner.getYieldEquipmentAmount(getProfession(), eYieldType);
				}
				int iYieldRequired = kOwner.getYieldEquipmentAmount(eProfession, eYieldType);
				if (iYieldRequired > 0)
				{
					int iMissing = iYieldRequired - iYieldCarried;
					if (iMissing > pCity->getYieldStored(eYieldType))
					{
						return false;
					}
				}
			}
		}
	}
	if (eProfession == PROFESSION_SAILOR)
	{
		if (!canBeSailor())
		{
			return false;
		}
	}

	ProfessionTypes eCurrrentProfession = getProfession();
	if (bEuropeUnit && !kOwner.isEurope())
	{
		for (int i=0; i < NUM_YIELD_TYPES; ++i)
		{
			YieldTypes eYield = (YieldTypes) i;
			if (!kOwner.isYieldEuropeTradable(eYield))
			{
				int iYieldAmountCurrentProfession = eCurrrentProfession != NO_PROFESSION ? kOwner.getYieldEquipmentAmount(eCurrrentProfession, eYield) : 0;
				if (kOwner.getYieldEquipmentAmount(eProfession, eYield) > iYieldAmountCurrentProfession)
				{
					return false;
				}
			}
		}
	}

	if (pCity != NULL)
	{
		if (!pCity->AI_isWorkforceHack())
		{
			//check if special building has been built
			if (kNewProfession.getSpecialBuilding() != NO_SPECIALBUILDING)
			{
				if (pCity->getProfessionOutput(eProfession, this) <= 0)
				{
					return false;
				}
			}

			// check against building max
			if (!bBumpOther)
			{
				if (!pCity->isAvailableProfessionSlot(eProfession, this))
				{
					return false;
				}
			}

			if (kNewProfession.isCitizen() && isOnMap())
			{
				if (!canJoinCity(pPlot))
				{
					return false;
				}
			}
		}
	}
	else
	{
		if (kNewProfession.isCitizen())
		{
			return false;
		}

		if (isOnMap())
		{
			return false;
		}
	}

	int iProfessionNewEra = kNewProfession.getNewEra();
	int iTurn = GC.getGameINLINE().getGameTurn();
	int iTurnMax = GC.getGameINLINE().getMaxTurns();

	if (GC.getCivilizationInfo(getCivilizationType()).isEurope())
	{
		return true;
	}

	if (iProfessionNewEra > kOwner.getNewEra())
	{
		return false;
	}

	if (kOwner.getNewEra() > 1)
	{
		switch(eProfession)
		{
		case PROFESSION_ARQUEBUSIER:
		case PROFESSION_MOUNTED_ARQUEBUSIER:
			return false;
		}
	}

	return true;
}
bool CvUnit::canHaveProfessionInEurope(ProfessionTypes eProfession) const
{
	if (eProfession == NO_PROFESSION)
	{
		return false;
	}
	if (eProfession == PROFESSION_PRISONER)
	{
		return false;
	}
	CvProfessionInfo& kNewProfession = GC.getProfessionInfo(eProfession);

	CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());
	if (kOwner.getParent() == NO_PLAYER)
	{
		return false;
	}

	if (!kOwner.isProfessionValid(eProfession, getUnitType()))
	{
		return false;
	}

	if (eProfession == PROFESSION_SAILOR)
	{
		if (!canBeSailor())
		{
			return false;
		}
	}
	if (kNewProfession.isCitizen())
	{
		return false;
	}

	if (isOnMap())
	{
		return false;
	}

	int iProfessionNewEra = kNewProfession.getNewEra();
	int iTurn = GC.getGameINLINE().getGameTurn();
	int iTurnMax = GC.getGameINLINE().getMaxTurns();

	if (GC.getCivilizationInfo(getCivilizationType()).isEurope())
	{
		return true;
	}

	if (iProfessionNewEra > kOwner.getNewEra())
	{
		return false;
	}

	if (kOwner.getNewEra() > 1)
	{
		switch(eProfession)
		{
		case PROFESSION_ARQUEBUSIER:
		case PROFESSION_MOUNTED_ARQUEBUSIER:
			return false;
		}
	}

	return true;
}
void CvUnit::processProfession(ProfessionTypes eProfession, int iChange, bool bUpdateCity)
{
	CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());

	if (iChange != 0)
	{
		processProfessionStats(eProfession, iChange);

		if (eProfession != NO_PROFESSION)
		{
			CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);

			kOwner.changeAssets(iChange * kProfession.getAssetValue());

			int iPower = iChange * kProfession.getPowerValue();
			for (int i = 0; i < NUM_YIELD_TYPES; ++i)
			{
				YieldTypes eYield = (YieldTypes) i;
				int iYieldAmount = GET_PLAYER(getOwnerINLINE()).getYieldEquipmentAmount(eProfession, eYield);
				iPower += iChange * GC.getYieldInfo(eYield).getPowerValue() * iYieldAmount;
				kOwner.changeAssets(iChange * GC.getYieldInfo(eYield).getAssetValue() * iYieldAmount);
			}

			kOwner.changePower(iPower);
			CvArea* pArea = area();
			if (pArea != NULL)
			{
				pArea->changePower(getOwnerINLINE(), iPower);
			}
		}
	}

	CvCity* pCity = kOwner.getPopulationUnitCity(getID());
	if (pCity == NULL)
	{
		CvPlot* pPlot = plot();
		if (pPlot != NULL)
		{
			pCity = pPlot->getPlotCity();
		}
	}

	if (pCity != NULL && pCity->getOwnerINLINE() == getOwnerINLINE())
	{
		if (iChange != 0)
		{
			if (eProfession != NO_PROFESSION && (pCity->getPopulation() > 0 || GC.getDefineINT("CONSUME_EQUIPMENT_ON_FOUND") != 0))
			{
				for (int i = 0; i < NUM_YIELD_TYPES; i++)
				{
					YieldTypes eYield = (YieldTypes) i;
					pCity->changeYieldStored(eYield, -iChange * kOwner.getYieldEquipmentAmount(eProfession, eYield));
				}
			}
		}

		if (bUpdateCity)
		{
			pCity->setYieldRateDirty();
			pCity->updateYield();
			CvPlot* pPlot = pCity->getPlotWorkedByUnit(this);
			if (pPlot != NULL)
			{
				pCity->verifyWorkingPlot(pCity->getCityPlotIndex(pPlot));
			}
			pCity->AI_setAssignWorkDirty(true);
		}
	}
}

void CvUnit::processProfessionStats(ProfessionTypes eProfession, int iChange)
{
	if (iChange != 0)
	{
		CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());
		if (eProfession != NO_PROFESSION)
		{
			CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);
			setBaseCombatStr(baseOriginCombatStr() + iChange * (kProfession.getCombatChange() + kOwner.getProfessionCombatChange(eProfession)));
			changeExtraMoves(iChange * kProfession.getMovesChange());
			changeExtraWorkRate(iChange *  kProfession.getWorkRate());
			if (!kProfession.isCityDefender())
			{
				changeBadCityDefenderCount(iChange);
			}
			if (kProfession.isUnarmed())
			{
				changeUnarmedCount(iChange);
			}

			for (int iPromotion = 0; iPromotion < GC.getNumPromotionInfos(); iPromotion++)
			{
				if (kProfession.isFreePromotion(iPromotion))
				{
					changeFreePromotionCount((PromotionTypes) iPromotion, iChange);
				}
			}
		}

		processUnitCombatType(getProfessionUnitCombatType(eProfession), iChange);
	}
}


int CvUnit::getProfessionChangeYieldRequired(ProfessionTypes eProfession, YieldTypes eYield) const
{
	CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());
	int iYieldCarried = 0;
	if (getProfession() != NO_PROFESSION)
	{
		iYieldCarried += kOwner.getYieldEquipmentAmount(getProfession(), eYield);
	}
	return (kOwner.getYieldEquipmentAmount(eProfession, eYield) - iYieldCarried);
}

bool CvUnit::canComeBackInToEurope() const
{
	if (getUnitTravelState() != UNIT_TRAVEL_STATE_FROM_EUROPE)
	{
		return false;
	}
	if (isAutomated())
	{
		return false;
	}
	return getUnitTravelTimer() == getEuropeTravelTime();// We can only come back to europe if didn't pass the turn after cross ocean

}
void CvUnit::comeBackInToEurope()
{
	if (!canComeBackInToEurope())
	{
		return;
	}
	CvSelectionGroup* pGroup = getGroup();
	if (pGroup != NULL)
	{
		CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
		while (pUnitNode != NULL)
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pGroup->nextUnitNode(pUnitNode);
			pLoopUnit->setUnitTravelTimer(0);
			pLoopUnit->setUnitTravelState(UNIT_TRAVEL_STATE_IN_EUROPE, true);
		}
	}
	else
	{
		setUnitTravelTimer(0);
		setUnitTravelState(UNIT_TRAVEL_STATE_IN_EUROPE, true);
	}
}

bool CvUnit::canTradeInEurope(bool bIncludeShipsWithPb, bool bIncludeWarShipWithoutGoods) const
{
	if (getUnitTravelState() != UNIT_TRAVEL_STATE_IN_EUROPE)
	{
		return false;
	}

	if (!m_pUnitInfo->isMechUnit())
	{
		return false;
	}

	if (!isOnlyDefensive() && !bIncludeWarShipWithoutGoods)
	{
		if (getOnlyNewCargo() == 0 && !bIncludeShipsWithPb)
		{
			return false;
		}
	}
	if (!bIncludeShipsWithPb)
	{
		if (!hasCrew())
		{
			return false;
		}
	}

	if (getShipSellPrice() > 0)
	{
		return false;
	}

	return true;
}

int CvUnit::getEuropeTravelTime() const
{
	int iTravelTime = GC.getEuropeInfo(plot()->getEurope()).getTripLength();

	iTravelTime *= GC.getGameSpeedInfo(GC.getGameINLINE().getGameSpeedType()).getGrowthPercent();
	iTravelTime /= 100;

	int iMaxSpeed = -1;
	CvSelectionGroup* pGroup = getGroup();
	if (pGroup != NULL)
	{
		CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
		while (pUnitNode != NULL)
		{
			CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
			pUnitNode = pGroup->nextUnitNode(pUnitNode);
			int iCurrentSpeed = pLoopUnit->getExtraCrossOceanSpeed();
			if (iCurrentSpeed < iMaxSpeed || iMaxSpeed == -1)
			{
				iMaxSpeed = iCurrentSpeed;
			}
		}
	}

	if (iMaxSpeed > 0)
	{
		iTravelTime /= iMaxSpeed;
	}

	if (getUnitTravelState() ==  UNIT_TRAVEL_STATE_IN_EUROPE || getUnitTravelState() == UNIT_TRAVEL_STATE_FROM_EUROPE)
	{
		iTravelTime = std::max(iTravelTime, 1);
	}		

	return iTravelTime;
}

int CvUnit::getImmobileTimer() const
{
	return m_iImmobileTimer;
}

void CvUnit::setImmobileTimer(int iNewValue)
{
	if (iNewValue != getImmobileTimer())
	{
		m_iImmobileTimer = iNewValue;

		setInfoBarDirty(true);
	}
}

void CvUnit::changeImmobileTimer(int iChange)
{
	if (iChange != 0)
	{
		setImmobileTimer(std::max(0, getImmobileTimer() + iChange));
	}
}

bool CvUnit::isMadeAttack() const
{
	return m_bMadeAttack;
}


void CvUnit::setMadeAttack(bool bNewValue)
{
	m_bMadeAttack = bNewValue;
}


bool CvUnit::isPromotionReady() const
{
	return m_bPromotionReady;
}

void CvUnit::setPromotionReady(bool bNewValue)
{
	if (isPromotionReady() != bNewValue)
	{
		m_bPromotionReady = bNewValue;

		if (m_bPromotionReady)
		{
			getGroup()->setAutomateType(NO_AUTOMATE);
			getGroup()->clearMissionQueue();
			getGroup()->setActivityType(ACTIVITY_AWAKE);
		}

		gDLL->getEntityIFace()->showPromotionGlow(getUnitEntity(), bNewValue);

		if (IsSelected())
		{
			gDLL->getInterfaceIFace()->setDirty(SelectionButtons_DIRTY_BIT, true);
		}
	}
}

void CvUnit::testPromotionReady()
{
	setPromotionReady((getExperience() >= experienceNeeded()) && canAcquireMilitaryPromotionAny());
}

PostCombatTypes CvUnit::getPostCombatState() const
{
	return m_ePostCombatState;
}

void CvUnit::setPostCombatState(PostCombatTypes ePostCombatState)
{
	if (getPostCombatState() != ePostCombatState)
	{
		m_ePostCombatState = ePostCombatState;
	}
}

bool CvUnit::isDelayedDeath() const
{
	return m_bDeathDelay;
}


void CvUnit::startDelayedDeath()
{
	m_bDeathDelay = true;
}


// Returns true if killed...
bool CvUnit::doDelayedDeath()
{
	if (m_bDeathDelay && !isFighting())
	{
		kill(false);
		return true;
	}

	return false;
}


bool CvUnit::isCombatFocus() const
{
	return m_bCombatFocus;
}


bool CvUnit::isInfoBarDirty() const
{
	return m_bInfoBarDirty;
}


void CvUnit::setInfoBarDirty(bool bNewValue)
{
	m_bInfoBarDirty = bNewValue;
}

PlayerTypes CvUnit::getOwner() const
{
	return getOwnerINLINE();
}

PlayerTypes CvUnit::getVisualOwner(TeamTypes eForTeam) const
{
	if (NO_TEAM == eForTeam)
	{
		eForTeam = GC.getGameINLINE().getActiveTeam();
	}

	if (getTeam() != eForTeam)
	{
		if (m_pUnitInfo->isHiddenNationality())
		{
			if (!plot()->isCity(true, getTeam()))
			{
				return UNKNOWN_PLAYER;
			}
		}
	}

	return getOwnerINLINE();
}


PlayerTypes CvUnit::getCombatOwner(TeamTypes eForTeam, const CvPlot* pPlot) const
{
	if (eForTeam != UNKNOWN_TEAM && getTeam() != eForTeam && eForTeam != NO_TEAM)
	{
		if (isAlwaysHostile(pPlot))
		{
			return UNKNOWN_PLAYER;
		}
	}

	return getOwnerINLINE();
}

TeamTypes CvUnit::getTeam() const
{
	return GET_PLAYER(getOwnerINLINE()).getTeam();
}

TeamTypes CvUnit::getCombatTeam(TeamTypes eForTeam, const CvPlot* pPlot) const
{
	TeamTypes eTeam;
	PlayerTypes eOwner = getCombatOwner(eForTeam, pPlot);
	switch (eOwner)
	{
	case UNKNOWN_PLAYER:
		eTeam = UNKNOWN_TEAM;
		break;
	case NO_PLAYER:
		eTeam = NO_TEAM;
		break;
	default:
		eTeam = GET_PLAYER(eOwner).getTeam();
		break;
	}

	return eTeam;
}

CivilizationTypes CvUnit::getVisualCiv(TeamTypes eForTeam) const
{
	PlayerTypes eOwner = getVisualOwner(eForTeam);
	if (eOwner == UNKNOWN_PLAYER)
	{
		return (CivilizationTypes) GC.getDefineINT("BARBARIAN_CIVILIZATION");
	}

	return GET_PLAYER(eOwner).getCivilizationType();
}

PlayerColorTypes CvUnit::getPlayerColor(TeamTypes eForTeam) const
{
	PlayerTypes eOwner = getVisualOwner(eForTeam);
	if (eOwner == UNKNOWN_PLAYER || eOwner == NO_PLAYER)
	{
		return (PlayerColorTypes) GC.getCivilizationInfo(getVisualCiv(eForTeam)).getDefaultPlayerColor();
	}

	return GET_PLAYER(eOwner).getPlayerColor();
}

PlayerTypes CvUnit::getCapturingPlayer() const
{
	return m_eCapturingPlayer;
}

void CvUnit::setCapturingPlayer(PlayerTypes eNewValue)
{
	m_eCapturingPlayer = eNewValue;
}

PlayerTypes CvUnit::getOriginalOwner() const
{
	return m_eOriginalOwner;
}

void CvUnit::setOriginalOwner(PlayerTypes eNewValue)
{
	m_eOriginalOwner = eNewValue;
}
// Start Target System Piracy
PlayerTypes CvUnit::getTargetPlayer() const
{
	return m_eTargetPlayer;
}

void CvUnit::setTargetPlayer(PlayerTypes eNewValue)
{
	m_eTargetPlayer = eNewValue;
}

int CvUnit::getTargetSeawayID() const
{
	return m_iTargetSeawayID;
}

void CvUnit::setTargetSeawayID(int iValue)
{
	m_iTargetSeawayID = iValue;
}
// End Target System Piracy

UnitTypes CvUnit::getUnitType() const
{
	return m_eUnitType;
}

CvUnitInfo &CvUnit::getUnitInfo() const
{
	return *m_pUnitInfo;
}


UnitClassTypes CvUnit::getUnitClassType() const
{
	return (UnitClassTypes)m_pUnitInfo->getUnitClassType();
}

UnitTypes CvUnit::getLeaderUnitType() const
{
	return m_eLeaderUnitType;
}

void CvUnit::setLeaderUnitType(UnitTypes leaderUnitType)
{
	if (m_eLeaderUnitType != leaderUnitType)
	{
		m_eLeaderUnitType = leaderUnitType;
		reloadEntity();
	}
}

CvUnit* CvUnit::getCombatUnit() const
{
	return getUnit(m_combatUnit);
}


void CvUnit::setCombatUnit(CvUnit* pCombatUnit, bool bAttacking)
{
	if (isCombatFocus())
	{
		gDLL->getInterfaceIFace()->setCombatFocus(false);
	}

	if (pCombatUnit != NULL)
	{
		if (bAttacking)
		{
			if (GC.getLogging())
			{
				if (gDLL->getChtLvl() > 0)
				{
					// Log info about this combat...
					char szOut[1024];
					sprintf( szOut, "*** KOMBAT!\n     ATTACKER: Player %d Unit %d (%S's %S), CombatStrength=%d\n     DEFENDER: Player %d Unit %d (%S's %S), CombatStrength=%d\n",
						getOwnerINLINE(), getID(), GET_PLAYER(getOwnerINLINE()).getName(), getName().GetCString(), currCombatStr(NULL, NULL),
						pCombatUnit->getOwnerINLINE(), pCombatUnit->getID(), GET_PLAYER(pCombatUnit->getOwnerINLINE()).getName(), pCombatUnit->getName().GetCString(), pCombatUnit->currCombatStr(pCombatUnit->plot(), this));
					gDLL->messageControlLog(szOut);
				}
			}
		}

		FAssertMsg(getCombatUnit() == NULL, "Combat Unit is not expected to be assigned");
		FAssertMsg(!(plot()->isFighting()), "(plot()->isFighting()) did not return false as expected");
		m_bCombatFocus = (bAttacking && !(gDLL->getInterfaceIFace()->isFocusedWidget()) && ((getOwnerINLINE() == GC.getGameINLINE().getActivePlayer()) || ((pCombatUnit->getOwnerINLINE() == GC.getGameINLINE().getActivePlayer()) && !(GC.getGameINLINE().isMPOption(MPOPTION_SIMULTANEOUS_TURNS)))));
		m_combatUnit = pCombatUnit->getIDInfo();
		setCombatDamage(0);
		setPostCombatPlot(getX_INLINE(), getY_INLINE());
	}
	else
	{
		if (getCombatUnit() != NULL)
		{
			FAssertMsg(getCombatUnit() != NULL, "getCombatUnit() is not expected to be equal with NULL");
			FAssertMsg(plot()->isFighting(), "plot()->isFighting is expected to be true");
			m_bCombatFocus = false;
			m_combatUnit.reset();
			setCombatDamage(0);

			if (IsSelected())
			{
				gDLL->getInterfaceIFace()->setDirty(InfoPane_DIRTY_BIT, true);
			}

			if (plot() == gDLL->getInterfaceIFace()->getSelectionPlot())
			{
				gDLL->getInterfaceIFace()->setDirty(PlotListButtons_DIRTY_BIT, true);
			}

			CvPlot* pPlot = getPostCombatPlot();
			if (pPlot != plot())
			{
				setXY(pPlot->getX_INLINE(), pPlot->getY_INLINE());
				if (pPlot->isFriendlyCity(*this, true))
				{
					finishMoves();
				}
			}
			setPostCombatPlot(INVALID_PLOT_COORD, INVALID_PLOT_COORD);
		}
	}

	setCombatTimer(0);
	setInfoBarDirty(true);

	if (isCombatFocus())
	{
		gDLL->getInterfaceIFace()->setCombatFocus(true);
	}
}

CvPlot* CvUnit::getPostCombatPlot() const
{
	return GC.getMapINLINE().plotByIndexINLINE(m_iPostCombatPlotIndex);
}

void CvUnit::setPostCombatPlot(int iX, int iY)
{
	m_iPostCombatPlotIndex = GC.getMapINLINE().isPlotINLINE(iX, iY) ? GC.getMapINLINE().plotNumINLINE(iX, iY) : -1;
}

CvUnit* CvUnit::getTransportUnit() const
{
	return getUnit(m_transportUnit);
}


bool CvUnit::isCargo() const
{
	return (getTransportUnit() != NULL);
}

// returns false if the unit is killed
bool CvUnit::setTransportUnit(CvUnit* pTransportUnit, bool bUnload)
{
	CvUnit* pOldTransportUnit = getTransportUnit();

	if (pOldTransportUnit != pTransportUnit)
	{
		if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(SelectionButtons_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(ResourceTable_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(CityScreen_DIRTY_BIT, true);
		}

		CvPlot* pPlot = plot();

		if (pOldTransportUnit != NULL)
		{
			pOldTransportUnit->changeCargo(-1);
		}
		m_transportUnit.reset();

		if (pTransportUnit != NULL)
		{
			FAssertMsg(pTransportUnit->cargoSpaceAvailable(getSpecialUnitType(), getDomainType()) > 0 || getYield() != NO_YIELD, "Cargo space is expected to be available");

			setUnitTravelState(pTransportUnit->getUnitTravelState(), false);

			//check if combining cargo

			joinGroup(NULL, true); // Because what if a group of 3 tries to get in a transport which can hold 2...

			m_transportUnit = pTransportUnit->getIDInfo();

			getGroup()->setActivityType(ACTIVITY_SLEEP);

			if (pPlot != pTransportUnit->plot())
			{
				FAssert(getUnitTravelState() != NO_UNIT_TRAVEL_STATE);
				setXY(pTransportUnit->getX_INLINE(), pTransportUnit->getY_INLINE());
			}

			pTransportUnit->changeCargo(1);
			pTransportUnit->getGroup()->setActivityType(ACTIVITY_AWAKE);
		}
		else //dropped off of vehicle
		{
			if (!isHuman() && (getMoves() < maxMoves()))
			{
				if (pOldTransportUnit != NULL)
				{
					AI_setMovePriority(pOldTransportUnit->AI_getMovePriority() + 1);
				}
			}
			else
			{
				if (getGroup()->getActivityType() != ACTIVITY_MISSION)
				{
					getGroup()->setActivityType(ACTIVITY_AWAKE);
				}
			}
		}

		if (pPlot != NULL)
		{
			pPlot->updateCenterUnit();
		}
	}

	return true;
}


int CvUnit::getExtraDomainModifier(DomainTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_DOMAIN_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiExtraDomainModifier[eIndex];
}


void CvUnit::changeExtraDomainModifier(DomainTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_DOMAIN_TYPES, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_aiExtraDomainModifier[eIndex] = (m_aiExtraDomainModifier[eIndex] + iChange);
}

int CvUnit::getNewCargoYield(YieldTypes eYield) const
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	return m_aiCargoYield[eYield];
}

void CvUnit::changeNewCargoYield(YieldTypes eYield, int iChange)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	m_aiCargoYield[eYield] += iChange;

	refreshNewCargo();
}
void CvUnit::setNewCargoYield(YieldTypes eYield, int iNewValue)
{
	FAssert(eYield > -1);
	FAssert(eYield < NUM_YIELD_TYPES);
	if (iNewValue != getNewCargoYield(eYield))
	{
		m_aiCargoYield[eYield] = iNewValue;
		refreshNewCargo();
	}
}

void CvUnit::setResupplyYield(YieldTypes eYield, int iNewValue)
{
	switch(eYield)
	{
	case YIELD_AMMUNITION:
		setMunition(iNewValue);
		break;
	case YIELD_CANNON:
		setNbCannon(iNewValue);
		break;
	default:
		FAssert(false);
	}
}

void CvUnit::refreshNewCargo()
{
	int iNewValue = 0;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		int iNbYield = getNewCargoYield(eYield);
		iNewValue += iNbYield;
	}
	setNewCargo(iNewValue);
}

int CvUnit::getDestinationCity() const
{
	return m_iDestinationCity;
}

void CvUnit::setDestinationCity(int iValue)
{
	m_iDestinationCity = iValue;
}

int CvUnit::getCountDiscoveredMapTiles() const
{
	return m_iCountDiscoveredMapTiles;
}

void CvUnit::setCountDiscoveredMapTiles(int iValue)
{
	m_iCountDiscoveredMapTiles = iValue;
}

void CvUnit::resetCountDiscoveredMapTiles()
{
	changeTotalDiscoveredMapTiles(getCountDiscoveredMapTiles());
	m_iCountDiscoveredMapTiles = 0;
}

void CvUnit::incrementCountDiscoveredMapTiles()
{
	m_iCountDiscoveredMapTiles++;
}

int CvUnit::getCityJobID() const
{
	return m_iCityJobID;
}

void CvUnit::setCityJobID(int iID)
{
	m_iCityJobID = iID;
}

CvCityJob* CvUnit::getCityJob() const
{
	CvCityJob* pCityJob = NULL;
	if (getCityJobID() >= 0)
	{
		pCityJob = GET_PLAYER(getOwnerINLINE()).getCityJob(getCityJobID());
	}
	return pCityJob;
}

int CvUnit::getConvoyID() const
{
	return m_iConvoyID;
}

void CvUnit::setConvoyID(int iID)
{
	FAssertMsg(getConvoyID() == -1 || iID == -1, "setConvoyID is not used correctly");
	m_iConvoyID = iID;
}

CvConvoy* CvUnit::getConvoy() const
{
	return GET_PLAYER(getOwnerINLINE()).getConvoy(getConvoyID());
}

void CvUnit::setConvoy(CvConvoy* pConvoy)
{
	if (pConvoy != NULL && !pConvoy->hasUnit(getID()) && !mustStayInEurope())
	{
		pConvoy->addUnit(this);
		getGroup()->setAutomateType(AUTOMATE_TRANSPORT_ROUTES);
	}
}

CvTradeRoute* CvUnit::getTradeRoute() const
{
	CvConvoy* pConvoy = getConvoy();
	return pConvoy != NULL ? pConvoy->getAssignedTradeRoute() : NULL;
}

void CvUnit::learnFromJobProfession() 
{
	if (AI_isReservedForWarPlan())
	{
		CvPlayerAI& kPlayer = GET_PLAYER(getOwnerINLINE());
		for (int iProfession = 0; iProfession < GC.getNumProfessionInfos(); ++iProfession)
		{
			ProfessionTypes eProfession = (ProfessionTypes) iProfession;
			if (kPlayer.AI_getWarPlanProfessionTypes(eProfession) > 0 && canHaveProfessionInEurope(eProfession))
			{
				setProfession(eProfession);
				kPlayer.AI_decreaseWarPlanProfessionTypes(eProfession);
				AI_setUnitAIType(kPlayer.AI_getUnitAIForCurrentWarPlan());
				break;
			}
		}
		return;
	}

	CvCityJob* pJob = getCityJob();
	if (pJob != NULL) 
	{
		ProfessionTypes eJobProfession = pJob->getEuropeProfession();
		if (eJobProfession != NO_PROFESSION && eJobProfession != getProfession()) 
		{
			if (canHaveProfessionInEurope(eJobProfession))
			{
				setProfession(eJobProfession);
				if (!GC.getProfessionInfo(eJobProfession).isCitizen()) 
				{
					setWorkingCity(NULL);
				}
			}
		}
		UnitAITypes eUnitAI = pJob->getUnitAIType();
		if (eUnitAI != NO_UNITAI)
		{			
			AI_setUnitAIType(eUnitAI);
		}
	}
}

unsigned int CvUnit::getTotalDiscoveredMapTiles() const
{
	return m_uiTotalDiscoveredMapTiles;
}

void CvUnit::changeTotalDiscoveredMapTiles(unsigned int iChange)
{
	m_uiTotalDiscoveredMapTiles += iChange;
}

const CvWString CvUnit::getName(uint uiForm) const
{
	CvWString szBuffer;

	if (isEmpty(m_szName))
	{
		return m_pUnitInfo->getDescription(uiForm);
	}

	szBuffer.Format(L"%s", m_szName.GetCString());

	return szBuffer;
}


const wchar* CvUnit::getNameKey() const
{
	if (isEmpty(m_szName))
	{
		return m_pUnitInfo->getTextKeyWide();
	}
	else
	{
		return m_szName.GetCString();
	}
}


const CvWString CvUnit::getNameNoDesc() const
{
	return m_szName.GetCString();
}

const CvWString CvUnit::getNameAndProfession() const
{
	CvWString szText;

	if (NO_PROFESSION != getProfession())
	{
		szText.Format(L"%s (%s)", GC.getProfessionInfo(getProfession()).getDescription(), getName().GetCString());
	}
	else
	{
		szText = getName();
	}

	return szText;
}
const CvWString CvUnit::getNameDescAndProfession() const
{
	CvWString szText;

	if (NO_PROFESSION != getProfession())
	{
		szText.Format(L"%s (%s)", GC.getProfessionInfo(getProfession()).getDescription(), m_pUnitInfo->getDescription());
	}
	else
	{
		szText = m_pUnitInfo->getDescription();
	}

	return szText;
}
const wchar* CvUnit::getNameOrProfessionKey() const
{
	if (getProfession() != NO_PROFESSION)
	{
		return GC.getProfessionInfo(getProfession()).getTextKeyWide();
	}
	else
	{
		return getNameKey();
	}
}

void CvUnit::setName(CvWString szNewValue)
{
	gDLL->stripSpecialCharacters(szNewValue);

	m_szName = szNewValue;

	if (IsSelected())
	{
		gDLL->getInterfaceIFace()->setDirty(InfoPane_DIRTY_BIT, true);
	}
}


std::string CvUnit::getScriptData() const
{
	return m_szScriptData;
}


void CvUnit::setScriptData(std::string szNewValue)
{
	m_szScriptData = szNewValue;
}


int CvUnit::getTerrainDoubleMoveCount(TerrainTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumTerrainInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiTerrainDoubleMoveCount[eIndex];
}


bool CvUnit::isTerrainDoubleMove(TerrainTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumTerrainInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return (getTerrainDoubleMoveCount(eIndex) > 0);
}


void CvUnit::changeTerrainDoubleMoveCount(TerrainTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumTerrainInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_paiTerrainDoubleMoveCount[eIndex] = (m_paiTerrainDoubleMoveCount[eIndex] + iChange);
	FAssert(getTerrainDoubleMoveCount(eIndex) >= 0);
}


int CvUnit::getFeatureDoubleMoveCount(FeatureTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumFeatureInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiFeatureDoubleMoveCount[eIndex];
}


bool CvUnit::isFeatureDoubleMove(FeatureTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumFeatureInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return (getFeatureDoubleMoveCount(eIndex) > 0);
}


void CvUnit::changeFeatureDoubleMoveCount(FeatureTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumFeatureInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_paiFeatureDoubleMoveCount[eIndex] = (m_paiFeatureDoubleMoveCount[eIndex] + iChange);
	FAssert(getFeatureDoubleMoveCount(eIndex) >= 0);
}


int CvUnit::getExtraTerrainAttackPercent(TerrainTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumTerrainInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiExtraTerrainAttackPercent[eIndex];
}


void CvUnit::changeExtraTerrainAttackPercent(TerrainTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumTerrainInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		m_paiExtraTerrainAttackPercent[eIndex] += iChange;

		setInfoBarDirty(true);
	}
}

int CvUnit::getExtraTerrainDefensePercent(TerrainTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumTerrainInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiExtraTerrainDefensePercent[eIndex];
}


void CvUnit::changeExtraTerrainDefensePercent(TerrainTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumTerrainInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		m_paiExtraTerrainDefensePercent[eIndex] += iChange;

		setInfoBarDirty(true);
	}
}

int CvUnit::getExtraFeatureAttackPercent(FeatureTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumFeatureInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiExtraFeatureAttackPercent[eIndex];
}


void CvUnit::changeExtraFeatureAttackPercent(FeatureTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumFeatureInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		m_paiExtraFeatureAttackPercent[eIndex] += iChange;

		setInfoBarDirty(true);
	}
}

int CvUnit::getExtraFeatureDefensePercent(FeatureTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumFeatureInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiExtraFeatureDefensePercent[eIndex];
}


void CvUnit::changeExtraFeatureDefensePercent(FeatureTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumFeatureInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");

	if (iChange != 0)
	{
		m_paiExtraFeatureDefensePercent[eIndex] += iChange;

		setInfoBarDirty(true);
	}
}

int CvUnit::getExtraUnitClassAttackModifier(UnitClassTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumUnitClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiExtraUnitClassAttackModifier[eIndex];
}

void CvUnit::changeExtraUnitClassAttackModifier(UnitClassTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumUnitClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_paiExtraUnitClassAttackModifier[eIndex] += iChange;
}

int CvUnit::getExtraUnitClassDefenseModifier(UnitClassTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumUnitClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiExtraUnitClassDefenseModifier[eIndex];
}

void CvUnit::changeExtraUnitClassDefenseModifier(UnitClassTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumUnitClassInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_paiExtraUnitClassDefenseModifier[eIndex] += iChange;
}

int CvUnit::getProductionToWork(int eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < nbUpgradeWork, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiProductionToWork[eIndex];
}

void CvUnit::changeProductionToWork(int eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < nbUpgradeWork, "eIndex is expected to be within maximum bounds (invalid Index)");
	m_paiProductionToWork[eIndex] += iChange;
}
int CvUnit::getProductionYield(YieldTypes eYield) const
{
	int iProduction = 0;
	CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());
	CvCity* pCity = NULL;

	pCity = kOwner.getPopulationUnitCity(getID());

	if (pCity == NULL || eYield == NO_YIELD)
	{
		return 0;
	}
	for (int i = 0; i < NUM_CITY_PLOTS; ++i)
	{
		CvPlot* pPlot = pCity->getCityIndexPlot(i);
		if (pPlot != NULL)
		{
			if (i != CITY_HOME_PLOT)
			{
				CvUnit* pUnit = pCity->getUnitWorkingPlot(i);
				if (pUnit == this)
				{
					ProfessionTypes eProfession = pUnit->getProfession();
					if (NO_PROFESSION != eProfession)
					{
						if (GC.getProfessionInfo(eProfession).getYieldProduced() == eYield)
						{
							iProduction += pPlot->getYield(eYield);
							iProduction *= (100+m_pUnitInfo->getYieldModifier(eYield))/100;
							iProduction *= (100+pCity->getOrganizationWork())/100;
						}
					}
				}
			}
		}
	}
	return iProduction;
}
void CvUnit::processProductionToWork()
{
	if (isNative())
	{
		return;
	}
	ProfessionTypes pProfession = getProfession();
	if (pProfession != NO_PROFESSION && GC.getProfessionInfo(pProfession).isWorkPlot())
	{
		int iProduction;
		YieldTypes eYield = (YieldTypes)GC.getProfessionInfo(pProfession).getYieldProduced();
		switch(eYield)
		{
		case YIELD_COTTON:
			iProduction = getProductionYield(eYield);
			changeProductionToWork(0, iProduction);
			break;

		case YIELD_FUR:
			iProduction = getProductionYield(eYield);
			changeProductionToWork(1, iProduction);
			break;

		case YIELD_SUGAR:
			iProduction = getProductionYield(eYield);
			changeProductionToWork(2, iProduction);
			break;

		case YIELD_TOBACCO:
			iProduction = getProductionYield(eYield);
			changeProductionToWork(3, iProduction);
			break;

		default:
			return;
		}
		for (int i=0; i<nbUpgradeWork; ++i)
		{
			if (getProductionToWork(i)>= 175)
			{
				UnitTypes eUnit = NO_UNIT;
				switch(i)
				{
				case 0:
					eUnit = UNIT_COTTON_PLANTER;
					break;
				case 1:
					eUnit = UNIT_TRAPPER;
					break;
				case 2:
					eUnit = UNIT_SUGAR_PLANTER;
					break;
				case 3:
					eUnit = UNIT_TOBACCO_PLANTER;
					break;

				}
				upgradeRefUnit(eUnit);
			}
		}
	}
}


void CvUnit::processUpgradeMilitary()
{
	bool bContinue = false;
	UnitTypes eUpgradeUnit = NO_UNIT;

	if (isNative() && GET_PLAYER(getOwnerINLINE()).isEurope())
	{
		return;
	}

	ProfessionTypes pProfession = getProfession();
	if (pProfession != NO_PROFESSION)
	{
		UnitClassTypes eUnitClass = (UnitClassTypes)GC.getProfessionInfo(pProfession).getUpgradeUnitClassType();
		if (eUnitClass != NO_UNITCLASS)
		{
			eUpgradeUnit = (UnitTypes) GC.getCivilizationInfo(getCivilizationType()).getCivilizationUnits(eUnitClass);
			if (eUpgradeUnit != NO_UNIT)
			{
				CvUnitInfo& kUnit = GC.getUnitInfo(eUpgradeUnit);
				bContinue = true;
				for (int iJ = 0; iJ < GC.getNumPromotionInfos(); iJ++)
				{
					PromotionTypes ePromotion = (PromotionTypes)iJ;
					if (kUnit.getFreePromotions(iJ))
					{
						if (!isHasPromotion(ePromotion) && !GC.getPromotionInfo(ePromotion).isOnlyFreePromo())
						{
							bContinue = false;//S'il n'a pas les promos gratuites de l'unit� pas d'upgrade
						}
					}
				}
			}
		}
	}
	if (bContinue)
	{
		upgradeRefUnit(eUpgradeUnit);
	}
}

void CvUnit::processMilitaryProfessionTurn()
{
	ProfessionTypes eProfession = getProfession();

	if (eProfession == NO_PROFESSION)
	{
		return;
	}

	CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);

	if (kProfession.getPowerValue() > 0 && eProfession != PROFESSION_SCOUT) 
	{
		if (getMilitaryProfessionTurn() < GC.getMAX_MILITARY_PROFESSION_TURN()) 
		{
			changeMilitaryProfessionTurn(1);
		}
	}
	else
	{
		if (getMilitaryProfessionTurn() > 0) 
		{
			changeMilitaryProfessionTurn(-1);
		}
	}
}

int CvUnit::getMilitaryProfessionTurn() const
{
	if (isNative() || GET_PLAYER(getOwnerINLINE()).isEurope() || m_pUnitInfo->isMechUnit()) 
	{
		return GC.getMAX_MILITARY_PROFESSION_TURN();
	}
	return m_iMilitaryProfessionTurn;
}

void CvUnit::setMilitaryProfessionTurn(int iValue)
{
	m_iMilitaryProfessionTurn = iValue;
}

void CvUnit::changeMilitaryProfessionTurn(int iChange)
{
	m_iMilitaryProfessionTurn += iChange;
}

int CvUnit::getMilitaryProfessionPenalty() const
{
	int iStartPenaltyTurn = GC.getMAX_MILITARY_PROFESSION_TURN() * 75 / 100;

	if (isNative() || GET_PLAYER(getOwnerINLINE()).isEurope()) 
	{
		return 0;
	}

	if (getMilitaryProfessionTurn() > iStartPenaltyTurn)
	{
		return 0;
	}

	if (getUnitType() == UNIT_VETERAN || getUnitType() == UNIT_ARTILLERYMAN_EXPERT) 
	{
		return 0;
	}

	int iPenalty = GC.getSTART_MILITARY_PROFESSION_PENALTY();
	iPenalty = (int)((float)iPenalty*pow((float)75.0/(float)100.0, (float) getMilitaryProfessionTurn()));

	return iPenalty;
}

bool CvUnit::haveToShowMilitaryProfessionTurnExperience() const
{
	ProfessionTypes eProfession = getProfession();
	if (eProfession == NO_PROFESSION) 
	{
		return false;
	}

	CvProfessionInfo& kProfession = GC.getProfessionInfo(eProfession);
	if (kProfession.getPowerValue() == 0) 
	{
		return false;
	} 

	if (eProfession == PROFESSION_SCOUT)
	{
		return false;
	}

	if (getUnitType() == UNIT_VETERAN || getUnitType() == UNIT_ARTILLERYMAN_EXPERT)
	{
		return false;
	}

	return true;
}

int CvUnit::getExtraUnitCombatModifier(UnitCombatTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumUnitCombatInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiExtraUnitCombatModifier[eIndex];
}

void CvUnit::changeExtraUnitCombatModifier(UnitCombatTypes eIndex, int iChange)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumUnitCombatInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	m_paiExtraUnitCombatModifier[eIndex] += iChange;
}

bool CvUnit::canAcquirePromotion(PromotionTypes ePromotion, bool bIncludeFreePromotions) const
{
	FAssertMsg(ePromotion >= 0, "ePromotion is expected to be non-negative (invalid Index)");
	FAssertMsg(ePromotion < GC.getNumPromotionInfos(), "ePromotion is expected to be within maximum bounds (invalid Index)");

	if (isHasPromotion(ePromotion))
	{
		return false;
	}

	CvPromotionInfo& kPromotion = GC.getPromotionInfo(ePromotion);
	
	if (!bIncludeFreePromotions) 
	{
		if (kPromotion.isOnlyFreePromo())
		{
			return false;
		}
	}

	if (kPromotion.getPrereqPromotion() != NO_PROMOTION)
	{
		if (!isHasPromotion((PromotionTypes)(GC.getPromotionInfo(ePromotion).getPrereqPromotion())))
		{
			return false;
		}
	}

	if (kPromotion.getPrereqOrPromotion1() != NO_PROMOTION)
	{
		if (!isHasPromotion((PromotionTypes)(GC.getPromotionInfo(ePromotion).getPrereqOrPromotion1())))
		{
			if ((kPromotion.getPrereqOrPromotion2() == NO_PROMOTION) || !isHasPromotion((PromotionTypes)(kPromotion.getPrereqOrPromotion2())))
			{
				return false;
			}
		}
	}

	if (!isPromotionValid(ePromotion))
	{
		return false;
	}

	return true;
}

bool CvUnit::isPromotionValid(PromotionTypes ePromotion) const
{
	CvPromotionInfo& kPromotion = GC.getPromotionInfo(ePromotion);

	if (kPromotion.isGraphicalOnly() && !kPromotion.isLeader())
	{
		return false;
	}

	if (isOnlyDefensive())
	{
		if (kPromotion.getCityAttackPercent() != 0)
		{
			return false;
		}
		if (kPromotion.isBlitz())
		{
			return false;
		}
		if (kPromotion.isAmphib())
		{
			return false;
		}
		if (kPromotion.isRiver())
		{
			return false;
		}
		if (kPromotion.getHillsAttackPercent() != 0)
		{
			return false;
		}
		for (int iTerrain = 0; iTerrain < GC.getNumTerrainInfos(); ++iTerrain)
		{
			if (kPromotion.getTerrainAttackPercent(iTerrain) != 0)
			{
				return false;
			}
		}
		for (int iFeature = 0; iFeature < GC.getNumFeatureInfos(); ++iFeature)
		{
			if (kPromotion.getFeatureAttackPercent(iFeature) != 0)
			{
				return false;
			}
		}
	}

	if (NO_PROMOTION != kPromotion.getPrereqPromotion())
	{
		if (!isPromotionValid((PromotionTypes)kPromotion.getPrereqPromotion()))
		{
			return false;
		}
	}

	PromotionTypes ePrereq1 = (PromotionTypes)kPromotion.getPrereqOrPromotion1();
	PromotionTypes ePrereq2 = (PromotionTypes)kPromotion.getPrereqOrPromotion2();
	if (NO_PROMOTION != ePrereq1 || NO_PROMOTION != ePrereq2)
	{
		bool bValid = false;
		if (!bValid)
		{
			if (NO_PROMOTION != ePrereq1 && isPromotionValid(ePrereq1))
			{
				bValid = true;
			}
		}

		if (!bValid)
		{
			if (NO_PROMOTION != ePrereq2 && isPromotionValid(ePrereq2))
			{
				bValid = true;
			}
		}

		if (!bValid)
		{
			return false;
		}
	}
	if (getUnitCombatType() == NO_UNITCOMBAT)
	{
		return false;
	}


	if (!kPromotion.getUnitCombat(getUnitCombatType()))
	{
		return false;
	}

	if (kPromotion.getWithdrawalChange() + withdrawalProbability() > GC.getDefineINT("MAX_WITHDRAWAL_PROBABILITY"))
	{
		return false;
	}

	return true;
}


bool CvUnit::canAcquirePromotionAny() const
{
	for (int iI = 0; iI < GC.getNumPromotionInfos(); iI++)
	{
		if (canAcquirePromotion((PromotionTypes)iI, false))
		{
			return true;
		}
	}

	return false;
}

bool CvUnit::canAcquireMilitaryPromotionAny() const
{
	for (int iI = 0; iI < GC.getNumPromotionInfos(); iI++)
	{
		if (canPromote((PromotionTypes)iI, -1, false))
		{
			return true;
		}
	}

	return false;
}

bool CvUnit::isHasPromotion(PromotionTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumPromotionInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	CvPromotionInfo& kPromotion = GC.getPromotionInfo(eIndex);

	UnitCombatTypes eUnitCombat = getUnitCombatType();
	if (eUnitCombat == NO_UNITCOMBAT)
	{
		return false;
	}

	if (!kPromotion.getUnitCombat(eUnitCombat))
	{
		return false;
	}

	if (getFreePromotionCount(eIndex) <= 0 && !isHasRealPromotion(eIndex))
	{
		return false;
	}

	return true;
}

bool CvUnit::isHasRealPromotion(PromotionTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumPromotionInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_pabHasRealPromotion[eIndex];
}

void CvUnit::setHasRealPromotion(PromotionTypes eIndex, bool bValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumPromotionInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");

	if (isHasRealPromotion(eIndex) != bValue)
	{
		if (isHasPromotion(eIndex))
		{
			processPromotion(eIndex, -1);
		}

		m_pabHasRealPromotion[eIndex] = bValue;

		if (isHasPromotion(eIndex))
		{
			processPromotion(eIndex, 1);
		}

		if (IsSelected())
		{
			gDLL->getInterfaceIFace()->setDirty(SelectionButtons_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(InfoPane_DIRTY_BIT, true);
		}
	}
}

void CvUnit::changeFreePromotionCount(PromotionTypes eIndex, int iChange)
{
	if (iChange != 0)
	{
		setFreePromotionCount(eIndex, getFreePromotionCount(eIndex) + iChange);
	}
}

void CvUnit::processPromotion(PromotionTypes ePromotion, int iChange)
{
	CvPromotionInfo& kPromotion = GC.getPromotionInfo(ePromotion);

	changeBlitzCount((kPromotion.isBlitz()) ? iChange : 0);
	changeAmphibCount((kPromotion.isAmphib()) ? iChange : 0);
	changeRiverCount((kPromotion.isRiver()) ? iChange : 0);
	changeEnemyRouteCount((kPromotion.isEnemyRoute()) ? iChange : 0);
	changeAlwaysHealCount((kPromotion.isAlwaysHeal()) ? iChange : 0);
	changeHillsDoubleMoveCount((kPromotion.isHillsDoubleMove()) ? iChange : 0);

	changeExtraVisibilityRange(kPromotion.getVisibilityChange() * iChange);
	changeExtraMoves(kPromotion.getMovesChange() * iChange);
	changeExtraMoveDiscount(kPromotion.getMoveDiscountChange() * iChange);
	changeExtraWithdrawal(kPromotion.getWithdrawalChange() * iChange);
	changeExtraBombardRate(kPromotion.getBombardRateChange() * iChange);
	changeExtraEnemyHeal(kPromotion.getEnemyHealChange() * iChange);
	changeExtraNeutralHeal(kPromotion.getNeutralHealChange() * iChange);
	changeExtraFriendlyHeal(kPromotion.getFriendlyHealChange() * iChange);
	changeSameTileHeal(kPromotion.getSameTileHealChange() * iChange);
	changeAdjacentTileHeal(kPromotion.getAdjacentTileHealChange() * iChange);
	changeExtraCombatPercent(kPromotion.getCombatPercent() * iChange);
	if (!kPromotion.isOnlyFreePromo())
	{
		changeExtraRealCombatPercent(kPromotion.getCombatPercent() * iChange);
	}
	changeExtraCrossOceanSpeed(kPromotion.getCrossOceanSpeed() * iChange);
	changeExtraCityAttackPercent(kPromotion.getCityAttackPercent() * iChange);
	changeExtraCityDefensePercent(kPromotion.getCityDefensePercent() * iChange);
	changeExtraHillsAttackPercent(kPromotion.getHillsAttackPercent() * iChange);
	changeExtraHillsDefensePercent(kPromotion.getHillsDefensePercent() * iChange);
	changePillageChange(kPromotion.getPillageChange() * iChange);
	changeUpgradeDiscount(kPromotion.getUpgradeDiscount() * iChange);
	changeExperiencePercent(kPromotion.getExperiencePercent() * iChange);
	changeAccuracyPercent(kPromotion.getAccuracyPercent() * iChange);
	changeBombardmentPercent(kPromotion.getBombardementPercent() * iChange);
	changeCargoSpace(kPromotion.getCargoChange() * iChange);

	for (int iI = 0; iI < GC.getNumTerrainInfos(); iI++)
	{
		changeExtraTerrainAttackPercent(((TerrainTypes)iI), (kPromotion.getTerrainAttackPercent(iI) * iChange));
		changeExtraTerrainDefensePercent(((TerrainTypes)iI), (kPromotion.getTerrainDefensePercent(iI) * iChange));
		changeTerrainDoubleMoveCount(((TerrainTypes)iI), ((kPromotion.getTerrainDoubleMove(iI)) ? iChange : 0));
	}

	for (int iI = 0; iI < GC.getNumFeatureInfos(); iI++)
	{
		changeExtraFeatureAttackPercent(((FeatureTypes)iI), (kPromotion.getFeatureAttackPercent(iI) * iChange));
		changeExtraFeatureDefensePercent(((FeatureTypes)iI), (kPromotion.getFeatureDefensePercent(iI) * iChange));
		changeFeatureDoubleMoveCount(((FeatureTypes)iI), ((kPromotion.getFeatureDoubleMove(iI)) ? iChange : 0));
	}

	for (int iI = 0; iI < GC.getNumUnitClassInfos(); ++iI)
	{
		changeExtraUnitClassAttackModifier((UnitClassTypes)iI, kPromotion.getUnitClassAttackModifier(iI) * iChange);
		changeExtraUnitClassDefenseModifier((UnitClassTypes)iI, kPromotion.getUnitClassDefenseModifier(iI) * iChange);
	}

	for (int iI = 0; iI < GC.getNumUnitCombatInfos(); iI++)
	{
		changeExtraUnitCombatModifier(((UnitCombatTypes)iI), (kPromotion.getUnitCombatModifierPercent(iI) * iChange));
	}

	for (int iI = 0; iI < NUM_DOMAIN_TYPES; iI++)
	{
		changeExtraDomainModifier(((DomainTypes)iI), (kPromotion.getDomainModifierPercent(iI) * iChange));
	}
}

void CvUnit::setFreePromotionCount(PromotionTypes eIndex, int iValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumPromotionInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	FAssertMsg(iValue >= 0, "promotion value going negative");

	if (getFreePromotionCount(eIndex) != iValue)
	{
		if (isHasPromotion(eIndex))
		{
			processPromotion(eIndex, -1);
		}

		m_paiFreePromotionCount[eIndex] = iValue;

		if (isHasPromotion(eIndex))
		{
			processPromotion(eIndex, 1);
		}

		if (IsSelected())
		{
			gDLL->getInterfaceIFace()->setDirty(SelectionButtons_DIRTY_BIT, true);
			gDLL->getInterfaceIFace()->setDirty(InfoPane_DIRTY_BIT, true);
		}
	}
}

int CvUnit::getFreePromotionCount(PromotionTypes eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < GC.getNumPromotionInfos(), "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_paiFreePromotionCount[eIndex];
}

int CvUnit::getSubUnitCount() const
{
	return m_pUnitInfo->getGroupSize(getProfession());
}


int CvUnit::getSubUnitsAlive() const
{
	return getSubUnitsAlive( getDamage());
}


int CvUnit::getSubUnitsAlive(int iDamage) const
{
	if (iDamage >= maxHitPoints())
	{
		return 0;
	}
	else
	{
		return std::max(1, (((m_pUnitInfo->getGroupSize(getProfession()) * (maxHitPoints() - iDamage)) + (maxHitPoints() / ((m_pUnitInfo->getGroupSize(getProfession()) * 2) + 1))) / maxHitPoints()));
	}
}
// returns true if unit can initiate a war action with plot (possibly by declaring war)
bool CvUnit::potentialWarAction(const CvPlot* pPlot) const
{
	TeamTypes ePlotTeam = pPlot->getTeam();
	TeamTypes eUnitTeam = getTeam();

	if (ePlotTeam == NO_TEAM)
	{
		return false;
	}

	if (isEnemy(ePlotTeam, pPlot))
	{
		return true;
	}

	if (getGroup()->AI_isDeclareWar(pPlot) && GET_TEAM(eUnitTeam).AI_getWarPlan(ePlotTeam) != NO_WARPLAN)
	{
		return true;
	}

	return false;
}

void CvUnit::read(FDataStreamBase* pStream)
{
	// Init data before load
	reset();

	uint uiFlag=0;
	pStream->Read(&uiFlag);	// flags for expansion

	pStream->Read(&m_iID);
	pStream->Read(&m_iGroupID);
	pStream->Read(&m_iHotKeyNumber);
	pStream->Read(&m_iX);
	pStream->Read(&m_iY);
	pStream->Read(&m_iLastMoveTurn);
	pStream->Read(&m_iGameTurnCreated);
	pStream->Read(&m_iDamage);
	pStream->Read(&m_iMoves);
	pStream->Read(&m_iExperience);
	pStream->Read(&m_iLevel);
	pStream->Read(&m_iCargo);
	pStream->Read(&m_iNewCargo);
	pStream->Read(&m_iCargoCapacity);
	pStream->Read(&m_iAttackPlotX);
	pStream->Read(&m_iAttackPlotY);
	pStream->Read(&m_iCombatTimer);
	pStream->Read(&m_iCombatDamage);
	pStream->Read(&m_iFortifyTurns);
	pStream->Read(&m_iBlitzCount);
	pStream->Read(&m_iAmphibCount);
	pStream->Read(&m_iRiverCount);
	pStream->Read(&m_iEnemyRouteCount);
	pStream->Read(&m_iAlwaysHealCount);
	pStream->Read(&m_iHillsDoubleMoveCount);
	pStream->Read(&m_iExtraVisibilityRange);
	pStream->Read(&m_iExtraMoves);
	pStream->Read(&m_iExtraMoveDiscount);
	pStream->Read(&m_iExtraWithdrawal);
	pStream->Read(&m_iExtraBombardRate);
	pStream->Read(&m_iExtraEnemyHeal);
	pStream->Read(&m_iExtraNeutralHeal);
	pStream->Read(&m_iExtraFriendlyHeal);
	pStream->Read(&m_iSameTileHeal);
	pStream->Read(&m_iAdjacentTileHeal);
	pStream->Read(&m_iExtraCombatPercent);
	pStream->Read(&m_iExtraRealCombatPercent);
	pStream->Read(&m_iExtraCrossOceanSpeed);	
	pStream->Read(&m_iExtraCityAttackPercent);
	pStream->Read(&m_iExtraCityDefensePercent);
	pStream->Read(&m_iExtraHillsAttackPercent);
	pStream->Read(&m_iExtraHillsDefensePercent);
	pStream->Read(&m_iPillageChange);
	pStream->Read(&m_iUpgradeDiscount);
	pStream->Read(&m_iExperiencePercent);
	pStream->Read(&m_iAccuracyPercent);
	pStream->Read(&m_iBombardmentPercent);
	pStream->Read(&m_iBaseCombat);
	pStream->Read((int*)&m_eFacingDirection);
	pStream->Read(&m_iImmobileTimer);
	pStream->Read(&m_iYieldStored);
	pStream->Read(&m_iExtraWorkRate);
	pStream->Read((int*)&m_eProfession);
	pStream->Read(&m_iUnitTravelTimer);
	pStream->Read(&m_iBadCityDefenderCount);
	pStream->Read(&m_iUnarmedCount);
	pStream->Read(&m_iNbMinution);
	pStream->Read(&m_iNbCannon);
	pStream->Read(&m_iRebelPercent);
	pStream->Read(&m_iUnitSailorType);
	pStream->Read(&m_iCrewFormationTurn);
	pStream->Read(&m_iLearningProfession);
	pStream->Read(&m_iFormationProfession);
	pStream->Read(&m_iLevelExploAndNavigation);
	pStream->Read(&m_iExperienceExploAndNavigation);
	pStream->Read(&m_iEuropeRecruitPrice);
	pStream->Read(&m_iSellPrice);
	pStream->Read(&m_iStolenGold);
	pStream->Read(&m_iProposePrice);
	pStream->Read(&m_iNumMap);
	pStream->Read(&m_iTargetSeawayID);
	pStream->Read(&m_iMilitaryProfessionTurn);
	pStream->Read(&m_iTravelRotation);
	pStream->Read(&m_iDestinationCity);
	pStream->Read(&m_iCountDiscoveredMapTiles);
	pStream->Read(&m_iCityJobID);
	pStream->Read(&m_iConvoyID);	

	pStream->Read(&m_uiTotalDiscoveredMapTiles);

	pStream->Read((int*)&m_eUnitTravelState);
	pStream->Read((int*)&m_eImmigrationType);
	pStream->Read((int*)&m_ePostCombatState);

	pStream->Read(&m_bMadeAttack);
	pStream->Read(&m_bPromotionReady);
	pStream->Read(&m_bHasCrew);
	pStream->Read(&m_bCanBeSailor);
	pStream->Read(&m_bTempInvisible);
	pStream->Read(&m_bTempCrew);
	pStream->Read(&m_bInEuropeDrydock);
	pStream->Read(&m_bMustStayInEurope);
	pStream->Read(&m_bLikelyToLeave);
	pStream->Read(&m_bhasPurchaseBid);
	pStream->Read(&m_bHaveFoundPack);
	pStream->Read(&m_bHaveNewMilitaryProfession);
	pStream->Read(&m_bDeathDelay);
	pStream->Read(&m_bCombatFocus);
	// m_bInfoBarDirty not saved...
	pStream->Read(&m_bColonistLocked);

	pStream->Read((int*)&m_eOwner);
	pStream->Read((int*)&m_eCapturingPlayer);
	pStream->Read((int*)&m_eOriginalOwner);
	pStream->Read((int*)&m_eTargetPlayer);
	pStream->Read((int*)&m_eUnitType);
	FAssert(NO_UNIT != m_eUnitType);
	m_pUnitInfo = (NO_UNIT != m_eUnitType) ? &GC.getUnitInfo(m_eUnitType) : NULL;
	pStream->Read((int*)&m_eLeaderUnitType);

	m_combatUnit.read(pStream);
	pStream->Read(&m_iPostCombatPlotIndex);
	m_transportUnit.read(pStream);
	m_homeCity.read(pStream);
	m_workingCity.read(pStream);

	pStream->Read(NUM_DOMAIN_TYPES, m_aiExtraDomainModifier);
	pStream->Read(NUM_YIELD_TYPES, m_aiCargoYield);

	pStream->ReadString(m_szName);
	pStream->ReadString(m_szScriptData);

	pStream->Read(GC.getNumPromotionInfos(), m_pabHasRealPromotion);
	pStream->Read(GC.getNumPromotionInfos(), m_paiFreePromotionCount);
	pStream->Read(GC.getNumTerrainInfos(), m_paiTerrainDoubleMoveCount);
	pStream->Read(GC.getNumFeatureInfos(), m_paiFeatureDoubleMoveCount);
	pStream->Read(GC.getNumTerrainInfos(), m_paiExtraTerrainAttackPercent);
	pStream->Read(GC.getNumTerrainInfos(), m_paiExtraTerrainDefensePercent);
	pStream->Read(GC.getNumFeatureInfos(), m_paiExtraFeatureAttackPercent);
	pStream->Read(GC.getNumFeatureInfos(), m_paiExtraFeatureDefensePercent);
	pStream->Read(GC.getNumUnitClassInfos(), m_paiExtraUnitClassAttackModifier);
	pStream->Read(GC.getNumUnitClassInfos(), m_paiExtraUnitClassDefenseModifier);
	pStream->Read(nbUpgradeWork, m_paiProductionToWork);
	pStream->Read(GC.getNumUnitCombatInfos(), m_paiExtraUnitCombatModifier);
}


void CvUnit::write(FDataStreamBase* pStream)
{
	uint uiFlag=0;
	pStream->Write(uiFlag);		// flag for expansion

	pStream->Write(m_iID);
	pStream->Write(m_iGroupID);
	pStream->Write(m_iHotKeyNumber);
	pStream->Write(m_iX);
	pStream->Write(m_iY);
	pStream->Write(m_iLastMoveTurn);
	pStream->Write(m_iGameTurnCreated);
	pStream->Write(m_iDamage);
	pStream->Write(m_iMoves);
	pStream->Write(m_iExperience);
	pStream->Write(m_iLevel);
	pStream->Write(m_iCargo);
	pStream->Write(m_iNewCargo);
	pStream->Write(m_iCargoCapacity);
	pStream->Write(m_iAttackPlotX);
	pStream->Write(m_iAttackPlotY);
	pStream->Write(m_iCombatTimer);
	pStream->Write(m_iCombatDamage);
	pStream->Write(m_iFortifyTurns);
	pStream->Write(m_iBlitzCount);
	pStream->Write(m_iAmphibCount);
	pStream->Write(m_iRiverCount);
	pStream->Write(m_iEnemyRouteCount);
	pStream->Write(m_iAlwaysHealCount);
	pStream->Write(m_iHillsDoubleMoveCount);
	pStream->Write(m_iExtraVisibilityRange);
	pStream->Write(m_iExtraMoves);
	pStream->Write(m_iExtraMoveDiscount);
	pStream->Write(m_iExtraWithdrawal);
	pStream->Write(m_iExtraBombardRate);
	pStream->Write(m_iExtraEnemyHeal);
	pStream->Write(m_iExtraNeutralHeal);
	pStream->Write(m_iExtraFriendlyHeal);
	pStream->Write(m_iSameTileHeal);
	pStream->Write(m_iAdjacentTileHeal);
	pStream->Write(m_iExtraCombatPercent);
	pStream->Write(m_iExtraRealCombatPercent);
	pStream->Write(m_iExtraCrossOceanSpeed);	
	pStream->Write(m_iExtraCityAttackPercent);
	pStream->Write(m_iExtraCityDefensePercent);
	pStream->Write(m_iExtraHillsAttackPercent);
	pStream->Write(m_iExtraHillsDefensePercent);
	pStream->Write(m_iPillageChange);
	pStream->Write(m_iUpgradeDiscount);
	pStream->Write(m_iExperiencePercent);
	pStream->Write(m_iAccuracyPercent);
	pStream->Write(m_iBombardmentPercent);
	pStream->Write(m_iBaseCombat);
	pStream->Write(m_eFacingDirection);
	pStream->Write(m_iImmobileTimer);
	pStream->Write(m_iYieldStored);
	pStream->Write(m_iExtraWorkRate);
	pStream->Write(m_eProfession);
	pStream->Write(m_iUnitTravelTimer);
	pStream->Write(m_iBadCityDefenderCount);
	pStream->Write(m_iUnarmedCount);
	pStream->Write(m_iNbMinution);
	pStream->Write(m_iNbCannon);
	pStream->Write(m_iRebelPercent);
	pStream->Write(m_iUnitSailorType);
	pStream->Write(m_iCrewFormationTurn);
	pStream->Write(m_iLearningProfession);
	pStream->Write(m_iFormationProfession);
	pStream->Write(m_iLevelExploAndNavigation);
	pStream->Write(m_iExperienceExploAndNavigation);
	pStream->Write(m_iEuropeRecruitPrice);
	pStream->Write(m_iSellPrice);
	pStream->Write(m_iStolenGold);
	pStream->Write(m_iProposePrice);
	pStream->Write(m_iNumMap);
	pStream->Write(m_iTargetSeawayID);
	pStream->Write(m_iMilitaryProfessionTurn);
	pStream->Write(m_iTravelRotation);
	pStream->Write(m_iDestinationCity);
	pStream->Write(m_iCountDiscoveredMapTiles);
	pStream->Write(m_iCityJobID);
	pStream->Write(m_iConvoyID);	

	pStream->Write(m_uiTotalDiscoveredMapTiles);

	pStream->Write(m_eUnitTravelState);
	pStream->Write(m_eImmigrationType);
	pStream->Write(m_ePostCombatState);

	pStream->Write(m_bMadeAttack);
	pStream->Write(m_bPromotionReady);
	pStream->Write(m_bHasCrew);
	pStream->Write(m_bCanBeSailor);
	pStream->Write(m_bTempInvisible);
	pStream->Write(m_bTempCrew);
	pStream->Write(m_bInEuropeDrydock);
	pStream->Write(m_bMustStayInEurope);
	pStream->Write(m_bLikelyToLeave);
	pStream->Write(m_bhasPurchaseBid);
	pStream->Write(m_bHaveFoundPack);
	pStream->Write(m_bHaveNewMilitaryProfession);
	pStream->Write(m_bDeathDelay);
	pStream->Write(m_bCombatFocus);
	// m_bInfoBarDirty not saved...
	pStream->Write(m_bColonistLocked);

	pStream->Write(m_eOwner);
	pStream->Write(m_eCapturingPlayer);
	pStream->Write(m_eOriginalOwner);
	pStream->Write(m_eTargetPlayer);
	pStream->Write(m_eUnitType);
	pStream->Write(m_eLeaderUnitType);

	m_combatUnit.write(pStream);
	pStream->Write(m_iPostCombatPlotIndex);
	m_transportUnit.write(pStream);
	m_homeCity.write(pStream);
	m_workingCity.write(pStream);

	pStream->Write(NUM_DOMAIN_TYPES, m_aiExtraDomainModifier);
	pStream->Write(NUM_YIELD_TYPES, m_aiCargoYield);

	pStream->WriteString(m_szName);
	pStream->WriteString(m_szScriptData);

	pStream->Write(GC.getNumPromotionInfos(), m_pabHasRealPromotion);
	pStream->Write(GC.getNumPromotionInfos(), m_paiFreePromotionCount);
	pStream->Write(GC.getNumTerrainInfos(), m_paiTerrainDoubleMoveCount);
	pStream->Write(GC.getNumFeatureInfos(), m_paiFeatureDoubleMoveCount);
	pStream->Write(GC.getNumTerrainInfos(), m_paiExtraTerrainAttackPercent);
	pStream->Write(GC.getNumTerrainInfos(), m_paiExtraTerrainDefensePercent);
	pStream->Write(GC.getNumFeatureInfos(), m_paiExtraFeatureAttackPercent);
	pStream->Write(GC.getNumFeatureInfos(), m_paiExtraFeatureDefensePercent);
	pStream->Write(GC.getNumUnitClassInfos(), m_paiExtraUnitClassAttackModifier);
	pStream->Write(GC.getNumUnitClassInfos(), m_paiExtraUnitClassDefenseModifier);
	pStream->Write(nbUpgradeWork, m_paiProductionToWork);
	pStream->Write(GC.getNumUnitCombatInfos(), m_paiExtraUnitCombatModifier);
}

// Protected Functions...

bool CvUnit::canAdvance(const CvPlot* pPlot, int iThreshold) const
{
	FAssert(canFight());
	FAssert(getDomainType() != DOMAIN_IMMOBILE);

	if (pPlot->getNumVisibleEnemyDefenders(this) > iThreshold)
	{
		return false;
	}

	if (isNoCityCapture() && pPlot->isEnemyCity(*this))
	{
		return false;
	}

	return true;
}

//------------------------------------------------------------------------------------------------
// FUNCTION:    CvUnit::planBattle
//! \brief      Determines in general how a battle will progress.
//!
//!				Note that the outcome of the battle is not determined here. This function plans
//!				how many sub-units die and in which 'rounds' of battle.
//! \param      kBattleDefinition The battle definition, which receives the battle plan.
//! \retval     The number of game turns that the battle should be given.
//------------------------------------------------------------------------------------------------
int CvUnit::planBattle( CvBattleDefinition & kBattleDefinition ) const
{
#define BATTLE_TURNS_SETUP 4
#define BATTLE_TURNS_ENDING 4
#define BATTLE_TURNS_MELEE 6
#define BATTLE_TURNS_RANGED 6
#define BATTLE_TURN_RECHECK 4

	int								aiUnitsBegin[BATTLE_UNIT_COUNT];
	int								aiUnitsEnd[BATTLE_UNIT_COUNT];
	int								aiToKillMelee[BATTLE_UNIT_COUNT];
	int								aiToKillRanged[BATTLE_UNIT_COUNT];
	CvBattleRoundVector::iterator	iIterator;
	int								i, j;
	bool							bIsLoser;
	int								iRoundIndex;
	int								iRoundCheck = BATTLE_TURN_RECHECK;

	// Initial conditions
	kBattleDefinition.setNumRangedRounds(0);
	kBattleDefinition.setNumMeleeRounds(0);

	increaseBattleRounds( kBattleDefinition);

	// Keep randomizing until we get something valid
	do
	{
		iRoundCheck++;
		if (( iRoundCheck >= BATTLE_TURN_RECHECK ) && !kBattleDefinition.isOneStrike())
		{
			increaseBattleRounds( kBattleDefinition);
			iRoundCheck = 0;
		}

		// Make sure to clear the battle plan, we may have to do this again if we can't find a plan that works.
		kBattleDefinition.clearBattleRounds();

		// Create the round list
		CvBattleRound kRound;
		int iTotalRounds = kBattleDefinition.getNumRangedRounds() + kBattleDefinition.getNumMeleeRounds();
		kBattleDefinition.setBattleRound(iTotalRounds, kRound);

		// For the attacker and defender
		for ( i = 0; i < BATTLE_UNIT_COUNT; i++ )
		{
			// Gather some initial information
			BattleUnitTypes unitType = (BattleUnitTypes) i;
			aiUnitsBegin[unitType] = kBattleDefinition.getUnit(unitType)->getSubUnitsAlive(kBattleDefinition.getDamage(unitType, BATTLE_TIME_BEGIN));
			aiToKillRanged[unitType] = computeUnitsToDie( kBattleDefinition, true, unitType);
			aiToKillMelee[unitType] = computeUnitsToDie( kBattleDefinition, false, unitType);
			aiUnitsEnd[unitType] = aiUnitsBegin[unitType] - aiToKillMelee[unitType] - aiToKillRanged[unitType];

			// Make sure that if they aren't dead at the end, they have at least one unit left
			if ( aiUnitsEnd[unitType] == 0 && !kBattleDefinition.getUnit(unitType)->isDead() )
			{
				aiUnitsEnd[unitType]++;
				if ( aiToKillMelee[unitType] > 0 )
				{
					aiToKillMelee[unitType]--;
				}
				else
				{
					aiToKillRanged[unitType]--;
				}
			}

			// If one unit is the loser, make sure that at least one of their units dies in the last round
			if ( aiUnitsEnd[unitType] == 0 )
			{
				kBattleDefinition.getBattleRound(iTotalRounds - 1).addNumKilled(unitType, 1);
				if ( aiToKillMelee[unitType] > 0)
				{
					aiToKillMelee[unitType]--;
				}
				else
				{
					aiToKillRanged[unitType]--;
				}
			}

			// Randomize in which round each death occurs
			bIsLoser = aiUnitsEnd[unitType] == 0;

			// Randomize the ranged deaths
			for ( j = 0; j < aiToKillRanged[unitType]; j++ )
			{
				iRoundIndex = GC.getGameINLINE().getSorenRandNum( range( kBattleDefinition.getNumRangedRounds(), 0, kBattleDefinition.getNumRangedRounds()), "Ranged combat death");
				kBattleDefinition.getBattleRound(iRoundIndex).addNumKilled(unitType, 1);
			}

			// Randomize the melee deaths
			for ( j = 0; j < aiToKillMelee[unitType]; j++ )
			{
				iRoundIndex = GC.getGameINLINE().getSorenRandNum( range( kBattleDefinition.getNumMeleeRounds() - (bIsLoser ? 1 : 2 ), 0, kBattleDefinition.getNumMeleeRounds()), "Melee combat death");
				kBattleDefinition.getBattleRound(kBattleDefinition.getNumRangedRounds() + iRoundIndex).addNumKilled(unitType, 1);
			}

			// Compute alive sums
			int iNumberKilled = 0;
			for (int j=0;j<kBattleDefinition.getNumBattleRounds();j++)
			{
				CvBattleRound &round = kBattleDefinition.getBattleRound(j);
				round.setRangedRound(j < kBattleDefinition.getNumRangedRounds());
				iNumberKilled += round.getNumKilled(unitType);
				round.setNumAlive(unitType, aiUnitsBegin[unitType] - iNumberKilled);
			}
		}

		// Now compute wave sizes
		for (int i=0;i<kBattleDefinition.getNumBattleRounds();i++)
		{
			CvBattleRound &round = kBattleDefinition.getBattleRound(i);
			round.setWaveSize(computeWaveSize(round.isRangedRound(), round.getNumAlive(BATTLE_UNIT_ATTACKER) + round.getNumKilled(BATTLE_UNIT_ATTACKER), round.getNumAlive(BATTLE_UNIT_DEFENDER) + round.getNumKilled(BATTLE_UNIT_DEFENDER)));
		}

		if ( iTotalRounds > 400 )
		{
			kBattleDefinition.setNumMeleeRounds(1);
			kBattleDefinition.setNumRangedRounds(0);
			break;
		}
	}
	while ( !verifyRoundsValid( kBattleDefinition ) && !kBattleDefinition.isOneStrike());

	//add a little extra time for leader to surrender
	bool attackerLeader = false;
	bool defenderLeader = false;
	bool attackerDie = false;
	bool defenderDie = false;
	int lastRound = kBattleDefinition.getNumBattleRounds() - 1;
	if (kBattleDefinition.getUnit(BATTLE_UNIT_ATTACKER)->getLeaderUnitType() != NO_UNIT)
		attackerLeader = true;
	if (kBattleDefinition.getUnit(BATTLE_UNIT_DEFENDER)->getLeaderUnitType() != NO_UNIT)
		defenderLeader = true;
	if (kBattleDefinition.getBattleRound(lastRound).getNumAlive(BATTLE_UNIT_ATTACKER) == 0)
		attackerDie = true;
	if (kBattleDefinition.getBattleRound(lastRound).getNumAlive(BATTLE_UNIT_DEFENDER) == 0)
		defenderDie = true;

	int extraTime = 0;
	if ((attackerLeader && attackerDie) || (defenderLeader && defenderDie))
		extraTime = BATTLE_TURNS_MELEE;

	return BATTLE_TURNS_SETUP + BATTLE_TURNS_ENDING + kBattleDefinition.getNumMeleeRounds() * BATTLE_TURNS_MELEE + kBattleDefinition.getNumRangedRounds() * BATTLE_TURNS_MELEE + extraTime;
}

//------------------------------------------------------------------------------------------------
// FUNCTION:	CvBattleManager::computeDeadUnits
//! \brief		Computes the number of units dead, for either the ranged or melee portion of combat.
//! \param		kDefinition The battle definition.
//! \param		bRanged true if computing the number of units that die during the ranged portion of combat,
//!					false if computing the number of units that die during the melee portion of combat.
//! \param		iUnit The index of the unit to compute (BATTLE_UNIT_ATTACKER or BATTLE_UNIT_DEFENDER).
//! \retval		The number of units that should die for the given unit in the given portion of combat
//------------------------------------------------------------------------------------------------
int CvUnit::computeUnitsToDie( const CvBattleDefinition & kDefinition, bool bRanged, BattleUnitTypes iUnit ) const
{
	FAssertMsg( iUnit == BATTLE_UNIT_ATTACKER || iUnit == BATTLE_UNIT_DEFENDER, "Invalid unit index");

	BattleTimeTypes iBeginIndex = bRanged ? BATTLE_TIME_BEGIN : BATTLE_TIME_RANGED;
	BattleTimeTypes iEndIndex = bRanged ? BATTLE_TIME_RANGED : BATTLE_TIME_END;
	return kDefinition.getUnit(iUnit)->getSubUnitsAlive(kDefinition.getDamage(iUnit, iBeginIndex)) -
		kDefinition.getUnit(iUnit)->getSubUnitsAlive( kDefinition.getDamage(iUnit, iEndIndex));
}

//------------------------------------------------------------------------------------------------
// FUNCTION:    CvUnit::verifyRoundsValid
//! \brief      Verifies that all rounds in the battle plan are valid
//! \param      vctBattlePlan The battle plan
//! \retval     true if the battle plan (seems) valid, false otherwise
//------------------------------------------------------------------------------------------------
bool CvUnit::verifyRoundsValid( const CvBattleDefinition & battleDefinition ) const
{
	for (int i=0;i<battleDefinition.getNumBattleRounds();i++)
	{
		if (!battleDefinition.getBattleRound(i).isValid())
			return false;
	}
	return true;
}

//------------------------------------------------------------------------------------------------
// FUNCTION:    CvUnit::increaseBattleRounds
//! \brief      Increases the number of rounds in the battle.
//! \param      kBattleDefinition The definition of the battle
//------------------------------------------------------------------------------------------------
void CvUnit::increaseBattleRounds( CvBattleDefinition & kBattleDefinition ) const
{
	if (kBattleDefinition.isOneStrike())
	{
		kBattleDefinition.addNumRangedRounds(1);
	}
	else if ( kBattleDefinition.getUnit(BATTLE_UNIT_ATTACKER)->isRanged() && kBattleDefinition.getUnit(BATTLE_UNIT_DEFENDER)->isRanged())
	{
		kBattleDefinition.addNumRangedRounds(1);
	}
	else
	{
		kBattleDefinition.addNumMeleeRounds(1);
	}
}

//------------------------------------------------------------------------------------------------
// FUNCTION:    CvUnit::computeWaveSize
//! \brief      Computes the wave size for the round.
//! \param      bRangedRound true if the round is a ranged round
//! \param		iAttackerMax The maximum number of attackers that can participate in a wave (alive)
//! \param		iDefenderMax The maximum number of Defenders that can participate in a wave (alive)
//! \retval     The desired wave size for the given parameters
//------------------------------------------------------------------------------------------------
int CvUnit::computeWaveSize( bool bRangedRound, int iAttackerMax, int iDefenderMax ) const
{
	FAssertMsg( getCombatUnit() != NULL, "You must be fighting somebody!" );
	int aiDesiredSize[BATTLE_UNIT_COUNT];
	if ( bRangedRound )
	{
		aiDesiredSize[BATTLE_UNIT_ATTACKER] = getUnitInfo().getRangedWaveSize(getProfession());
		aiDesiredSize[BATTLE_UNIT_DEFENDER] = getCombatUnit()->getUnitInfo().getRangedWaveSize(getProfession());
	}
	else
	{
		aiDesiredSize[BATTLE_UNIT_ATTACKER] = getUnitInfo().getMeleeWaveSize(getProfession());
		aiDesiredSize[BATTLE_UNIT_DEFENDER] = getCombatUnit()->getUnitInfo().getMeleeWaveSize(getProfession());
	}

	aiDesiredSize[BATTLE_UNIT_DEFENDER] = aiDesiredSize[BATTLE_UNIT_DEFENDER] <= 0 ? iDefenderMax : aiDesiredSize[BATTLE_UNIT_DEFENDER];
	aiDesiredSize[BATTLE_UNIT_ATTACKER] = aiDesiredSize[BATTLE_UNIT_ATTACKER] <= 0 ? iDefenderMax : aiDesiredSize[BATTLE_UNIT_ATTACKER];
	return std::min( std::min( aiDesiredSize[BATTLE_UNIT_ATTACKER], iAttackerMax ), std::min( aiDesiredSize[BATTLE_UNIT_DEFENDER],
		iDefenderMax) );
}

bool CvUnit::isEnemy(TeamTypes eTeam, const CvPlot* pPlot) const
{
	if (NULL == pPlot)
	{
		pPlot = plot();
	}

	return (::atWar(getCombatTeam(eTeam, pPlot), eTeam));
}

bool CvUnit::isPotentialEnemy(TeamTypes eTeam, const CvPlot* pPlot) const
{
	if (NULL == pPlot)
	{
		pPlot = plot();
	}

	return (::isPotentialEnemy(getCombatTeam(eTeam, pPlot), eTeam));
}

void CvUnit::getDefenderCombatValues(CvUnit& kDefender, const CvPlot* pPlot, int iOurStrength, int iOurFirepower, int& iTheirOdds, int& iTheirStrength, int& iOurDamage, int& iTheirDamage, CombatDetails* pTheirDetails) const
{
	iTheirStrength = kDefender.currCombatStr(pPlot, this, pTheirDetails);
	int iTheirFirepower = kDefender.currFirepower(pPlot, this);

	FAssert((iOurStrength + iTheirStrength) > 0);
	FAssert((iOurFirepower + iTheirFirepower) > 0);

	iTheirOdds = ((GC.getDefineINT("COMBAT_DIE_SIDES") * iTheirStrength) / (iOurStrength + iTheirStrength));
	int iStrengthFactor = ((iOurFirepower + iTheirFirepower + 1) / 2);

	iOurDamage = std::max(1, ((GC.getDefineINT("COMBAT_DAMAGE") * (iTheirFirepower + iStrengthFactor)) / (iOurFirepower + iStrengthFactor)));
	iTheirDamage = std::max(1, ((GC.getDefineINT("COMBAT_DAMAGE") * (iOurFirepower + iStrengthFactor)) / (iTheirFirepower + iStrengthFactor)));
}

int CvUnit::getTriggerValue(EventTriggerTypes eTrigger, const CvPlot* pPlot, bool bCheckPlot) const
{
	CvEventTriggerInfo& kTrigger = GC.getEventTriggerInfo(eTrigger);
	if (kTrigger.getNumUnits() <= 0)
	{
		return MIN_INT;
	}

	if (!isEmpty(kTrigger.getPythonCanDoUnit()))
	{
		long lResult;

		CyArgsList argsList;
		argsList.add(eTrigger);
		argsList.add(getOwnerINLINE());
		argsList.add(getID());

		gDLL->getPythonIFace()->callFunction(PYRandomEventModule, kTrigger.getPythonCanDoUnit(), argsList.makeFunctionArgs(), &lResult);

		if (0 == lResult)
		{
			return MIN_INT;
		}
	}

	if (kTrigger.getNumUnitsRequired() > 0)
	{
		bool bFoundValid = false;
		for (int i = 0; i < kTrigger.getNumUnitsRequired(); ++i)
		{
			if (getUnitClassType() == kTrigger.getUnitRequired(i))
			{
				bFoundValid = true;
				break;
			}
		}

		if (!bFoundValid)
		{
			return MIN_INT;
		}
	}

	if (bCheckPlot)
	{
		if (kTrigger.isUnitsOnPlot())
		{
			if (!plot()->canTrigger(eTrigger, getOwnerINLINE()))
			{
				return MIN_INT;
			}
		}
	}

	int iValue = 0;

	if (0 == getDamage() && kTrigger.getUnitDamagedWeight() > 0)
	{
		return MIN_INT;
	}

	iValue += getDamage() * kTrigger.getUnitDamagedWeight();

	iValue += getExperience() * kTrigger.getUnitExperienceWeight();

	if (NULL != pPlot)
	{
		iValue += plotDistance(getX_INLINE(), getY_INLINE(), pPlot->getX_INLINE(), pPlot->getY_INLINE()) * kTrigger.getUnitDistanceWeight();
	}

	return iValue;
}

bool CvUnit::canApplyEvent(EventTypes eEvent) const
{
	CvEventInfo& kEvent = GC.getEventInfo(eEvent);

	if (0 != kEvent.getUnitExperience())
	{
		if (!canAcquirePromotionAny())
		{
			return false;
		}
	}

	if (NO_PROMOTION != kEvent.getUnitPromotion())
	{
		if (!canAcquirePromotion((PromotionTypes)kEvent.getUnitPromotion()))
		{
			return false;
		}
	}

	if (kEvent.getUnitImmobileTurns() > 0)
	{
		if (!canAttack())
		{
			return false;
		}
	}

	return true;
}

void CvUnit::applyEvent(EventTypes eEvent)
{
	if (!canApplyEvent(eEvent))
	{
		return;
	}

	CvEventInfo& kEvent = GC.getEventInfo(eEvent);

	if (0 != kEvent.getUnitExperience())
	{
		setDamage(0);
		changeExperience(kEvent.getUnitExperience());
	}

	if (NO_PROMOTION != kEvent.getUnitPromotion())
	{
		setHasRealPromotion((PromotionTypes)kEvent.getUnitPromotion(), true);
	}

	if (kEvent.getUnitImmobileTurns() > 0)
	{
		changeImmobileTimer(kEvent.getUnitImmobileTurns());
		CvWString szText = gDLL->getText("TXT_KEY_EVENT_UNIT_IMMOBILE", getNameOrProfessionKey(), kEvent.getUnitImmobileTurns());
		gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), false, GC.getEVENT_MESSAGE_TIME(), szText, "AS2D_UNITGIFTED", MESSAGE_TYPE_INFO, getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_UNIT_TEXT"), getX_INLINE(), getY_INLINE(), true, true);
	}

	CvWString szNameKey(kEvent.getUnitNameKey());

	if (!szNameKey.empty())
	{
		setName(gDLL->getText(kEvent.getUnitNameKey()));
	}

	if (kEvent.isDisbandUnit())
	{
		kill(false);
	}
}

const CvArtInfoUnit* CvUnit::getArtInfo(int i) const
{
	return m_pUnitInfo->getUnitArtStylesArtInfo(i, getProfession(), (UnitArtStyleTypes) GC.getCivilizationInfo(getCivilizationType()).getUnitArtStyleType());
}

const TCHAR* CvUnit::getButton() const
{
	const CvArtInfoUnit* pArtInfo = getArtInfo(0);

	if (NULL != pArtInfo)
	{
		return pArtInfo->getButton();
	}

	return m_pUnitInfo->getButton();
}

const TCHAR* CvUnit::getFullLengthIcon() const
{
	const CvArtInfoUnit* pArtInfo = getArtInfo(0);

	if (NULL != pArtInfo)
	{
		return pArtInfo->getFullLengthIcon();
	}

	return NULL;
}

const TCHAR* CvUnit::getFullLengthReverseIcon() const
{
	const CvArtInfoUnit* pArtInfo = getArtInfo(0);

	if (NULL != pArtInfo)
	{
		return pArtInfo->getFullLengthReverseIcon();
	}

	return NULL;
}

const TCHAR* CvUnit::getEuropePicture() const
{
	const CvArtInfoUnit* pArtInfo = getArtInfo(0);

	if (NULL != pArtInfo)
	{
		return pArtInfo->getEuropePicture();
	}

	return NULL;
}
const TCHAR* CvUnit::getSelectedPicture() const
{
	const CvArtInfoUnit* pArtInfo = getArtInfo(0);

	if (NULL != pArtInfo)
	{
		return pArtInfo->getSelectedPicture();
	}

	return NULL;
}

bool CvUnit::isAlwaysHostile(const CvPlot* pPlot) const
{
	if (!m_pUnitInfo->isAlwaysHostile())
	{
		return false;
	}

	if (NULL != pPlot && pPlot->isCity(true, getTeam()))
	{
		return false;
	}

	return true;
}

bool CvUnit::verifyStackValid()
{
	if (plot()->isVisibleEnemyUnit(this))
	{
		return jumpToNearestValidPlot();
	}

	return true;
}

void CvUnit::setYieldStored(int iYieldAmount)
{
	int iChange = (iYieldAmount - getYieldStored());
	if (iChange != 0)
	{
		FAssert(iYieldAmount >= 0);
		m_iYieldStored = iYieldAmount;


		YieldTypes eYield = getYield();
		if (eYield == NO_YIELD)
		{
			if (!m_pUnitInfo->isTreasure() && getYieldStored() > 0)
			{
				CvPlayer& kPlayer = GET_PLAYER(getOwnerINLINE());
				CvCity* pCity = kPlayer.getPopulationUnitCity(getID());
				if (pCity != NULL)
				{
					int iLeaningProfession = getLearningProfession();
					int iFormationProfession = getFormationProfession();
					if (isHuman())
					{
						if (iLeaningProfession != -1)
						{
							if (getYieldStored() >= pCity->educationThreshold()*GC.getUnitInfo((UnitTypes)iLeaningProfession).getClassEducation()/3)
							{
								pCity->educateStudent(getID(), (UnitTypes)iLeaningProfession);
								setLearningProfession(-1);
							}
						}
						if (iFormationProfession != -1)
						{
							if (getYieldStored() >= pCity->educationThreshold()/3)
							{
								pCity->removePopulationUnit(this, false, (ProfessionTypes)m_pUnitInfo->getDefaultProfession());
								setCanBeSailor(true);
								if (canHaveProfession((ProfessionTypes)iFormationProfession, false))
								{
									setProfession((ProfessionTypes)iFormationProfession);
								}
								setFormationProfession(-1);
							}
						}
					}
					else
					{
						UnitTypes eLearningUnit = (UnitTypes)iLeaningProfession;
						FAssertMsg(eLearningUnit != NO_UNIT, "eLearningUnit is expected to be different to NO_UNIT");
						if (eLearningUnit != NO_UNIT)
						{
							if (getYieldStored() >= pCity->educationThreshold()*GC.getUnitInfo(eLearningUnit).getClassEducation()/3)
							{
								pCity->educateStudent(getID(), eLearningUnit);
								setLearningProfession(-1);
							}
						}
					}
				}
			}
		}
	}
}

int CvUnit::getYieldStored() const
{
	return m_iYieldStored;
}

YieldTypes CvUnit::getYield() const
{
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (getUnitClassType() == GC.getYieldInfo(eYield).getUnitClass())
		{
			return eYield;
		}
	}

	return NO_YIELD;
}

bool CvUnit::isGoods() const
{
	if (getYieldStored() > 0)
	{
		if (m_pUnitInfo->isTreasure())
		{
			return true;
		}

		if (getYield() != NO_YIELD)
		{
			if (GC.getYieldInfo(getYield()).isCargo())
			{
				return true;
			}
		}
	}

	return false;
}


// Private Functions...

//check if quick combat
bool CvUnit::isCombatVisible(const CvUnit* pDefender) const
{
	bool bVisible = false;

	if (!m_pUnitInfo->isQuickCombat())
	{
		if (NULL == pDefender || !pDefender->getUnitInfo().isQuickCombat())
		{
			if (isHuman())
			{
				if (!GET_PLAYER(getOwnerINLINE()).isOption(PLAYEROPTION_QUICK_ATTACK))
				{
					bVisible = true;
				}
			}
			else if (NULL != pDefender && pDefender->isHuman())
			{
				if (!GET_PLAYER(pDefender->getOwnerINLINE()).isOption(PLAYEROPTION_QUICK_DEFENSE))
				{
					bVisible = true;
				}
			}
		}
	}

	return bVisible;
}

void CvUnit::changeBadCityDefenderCount(int iChange)
{
	m_iBadCityDefenderCount += iChange;
	FAssert(getBadCityDefenderCount() >= 0);
}

int CvUnit::getBadCityDefenderCount() const
{
	return m_iBadCityDefenderCount;
}

bool CvUnit::isCityDefender() const
{
	return (getBadCityDefenderCount() == 0);
}

bool CvUnit::isInsideNationCity() const
{
	CvPlot* pPlot = plot();
	if (pPlot == NULL)
	{
		return false;
	}

	return pPlot->getOwner() == getOwner();
}

void CvUnit::changeUnarmedCount(int iChange)
{
	m_iUnarmedCount += iChange;
	FAssert(getUnarmedCount() >= 0);
}

int CvUnit::getUnarmedCount() const
{
	return m_iUnarmedCount;
}

int CvUnit::getUnitTravelTimer() const
{
	return m_iUnitTravelTimer;
}

void CvUnit::setUnitTravelTimer(int iValue)
{
	m_iUnitTravelTimer = iValue;
	FAssert(getUnitTravelTimer() >= 0);
}

UnitTravelStates CvUnit::getUnitTravelState() const
{
	return m_eUnitTravelState;
}

void CvUnit::setUnitTravelState(UnitTravelStates eState, bool bShowEuropeScreen)
{
	if (getUnitTravelState() != eState)
	{
		CvPlot* pPlot = plot();

		if (pPlot != NULL)
		{
			pPlot->changeAdjacentSight(getTeam(), visibilityRange(), false, this);
		}

		UnitTravelStates eFromState = getUnitTravelState();
		m_eUnitTravelState = eState;

		if (pPlot != NULL)
		{
			if (eFromState == UNIT_TRAVEL_STATE_FROM_EUROPE)
			{
				EuropeTypes eEurope = pPlot->getEurope();
				if (eEurope != NO_EUROPE)
				{
					switch (GC.getEuropeInfo(eEurope).getCardinalDirection())
					{
					case CARDINALDIRECTION_EAST:
						setFacingDirection(DIRECTION_WEST);
						break;
					case CARDINALDIRECTION_WEST:
						setFacingDirection(DIRECTION_EAST);
						break;
					case CARDINALDIRECTION_NORTH:
						setFacingDirection(DIRECTION_SOUTH);
						break;
					case CARDINALDIRECTION_SOUTH:
						setFacingDirection(DIRECTION_NORTH);
						break;
					}
				}
			}

			
			CvTradeRoute* pDirturbedTradeRoute = NULL;
			if (eState == UNIT_TRAVEL_STATE_TO_EUROPE)
			{
				int iDistance = 1;
				for (int iDX = -iDistance; iDX <= iDistance; iDX++) 
				{
					for (int iDY = -iDistance; iDY <= iDistance; iDY++) 
					{
						CvPlot* pLoopPlot = plotXY(getX_INLINE(), getY_INLINE(), iDX, iDY);
						if (pLoopPlot != NULL && pLoopPlot->isVisibleEnemyUnit(this)) 
						{							
							pDirturbedTradeRoute = getTradeRoute();
						}
					}
				}
			}

			pPlot->changeAdjacentSight(getTeam(), visibilityRange(), true, this);
			
			if (eFromState == UNIT_TRAVEL_STATE_FROM_EUROPE)
			{
				int iDistance = 1;
				for (int iDX = -iDistance; iDX <= iDistance; iDX++) 
				{
					for (int iDY = -iDistance; iDY <= iDistance; iDY++) 
					{
						CvPlot* pLoopPlot = plotXY(getX_INLINE(), getY_INLINE(), iDX, iDY);
						if (pLoopPlot != NULL && pLoopPlot->isVisibleEnemyUnit(this)) 
						{							
							pDirturbedTradeRoute = getTradeRoute();
						}
					}
				}
			}

			if (pDirturbedTradeRoute != NULL)
			{
				CvCity* pCity = pDirturbedTradeRoute->getSourceOrDestinationCity();
				if (pCity != NULL)
				{
					pCity->AI_changeNavalDisturbationCount(1);
				}
			}

			if (hasCargo())
			{
				int iNumPrisoners = 0;
				for (CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode(); pUnitNode != NULL; pUnitNode = pPlot->nextUnitNode(pUnitNode))
				{
					CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
					if (pLoopUnit->getTransportUnit() == this)
					{
						pLoopUnit->setUnitTravelState(eState, false);
						if (pLoopUnit->getProfession() == PROFESSION_PRISONER && eState == UNIT_TRAVEL_STATE_IN_EUROPE)
						{
							pLoopUnit->kill(false);
							iNumPrisoners += 1;
						}
					}
				}
				if (iNumPrisoners > 0 && isHuman())
				{
					gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), gDLL->getText("TXT_KEY_INFO_PRISONERS_FREEDOM"), "AS2D_POSITIVE_DINK", MESSAGE_TYPE_INFO);
				}
				CvPlayer& pPlayer = GET_PLAYER(getOwner());
				if (getOnlyNewCargo() > 0 && pPlayer.getParent() != NO_PLAYER)
				{
					CvPlayer& pEuropePlayer = GET_PLAYER(pPlayer.getParent());
					for (int iYield = 0; iYield < NUM_YIELD_TYPES; iYield++)
					{
						YieldTypes eYield = (YieldTypes) iYield;
						int iNewCargo = getNewCargoYield(eYield);
						if (iNewCargo > 0)
						{
							int iSellPrice = pPlayer.getSellPriceForYield(eYield, iNewCargo);
							pPlot->addCrumbs(iSellPrice/100);
						}
					}
				}
			}

			if (eState == UNIT_TRAVEL_STATE_IN_EUROPE)
			{
				if (hasTempCrew())
				{
					setTempCrew(false);
					setHasCrew(false);
					joinGroup(NULL);
					gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), gDLL->getText("TXT_KEY_INFO_TEMP_CREW_LEAVE"), "AS2D_POSITIVE_DINK", MESSAGE_TYPE_INFO);
				}

				std::vector<CvUnit*> aTreasures;
				for (CLLNode<IDInfo>* pUnitNode = pPlot->headUnitNode(); pUnitNode != NULL; pUnitNode = pPlot->nextUnitNode(pUnitNode))
				{
					CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
					if (pLoopUnit->getTransportUnit() == this)
					{
						if (pLoopUnit->getUnitInfo().isTreasure())
						{
							aTreasures.push_back(pLoopUnit);							 
						}
					}
				}
				for (uint iTreasure = 0; iTreasure < aTreasures.size(); ++iTreasure)
				{
					GET_PLAYER(getOwner()).sellYieldUnitToEurope(aTreasures[iTreasure], aTreasures[iTreasure]->getYieldStored(), 0, NO_YIELD);
				}
			}
		}

		if (!isOnMap())
		{
			if (IsSelected())
			{
				gDLL->getInterfaceIFace()->removeFromSelectionList(this);
			}
		}
		else
		{
			GET_PLAYER(getOwnerINLINE()).updateGroupCycle(this);
		}

		if (getOwnerINLINE() == GC.getGameINLINE().getActivePlayer())
		{
			gDLL->getInterfaceIFace()->setDirty(EuropeScreen_DIRTY_BIT, true);
		}

		gDLL->getEventReporterIFace()->unitTravelStateChanged(getOwnerINLINE(), eState, getID());

		if (pPlot != NULL)
		{
			pPlot->updateCenterUnit();

			CvSelectionGroup* pGroup = getGroup();
			if (pGroup != NULL)
			{
				if (bShowEuropeScreen && eState == UNIT_TRAVEL_STATE_IN_EUROPE)
				{
					pGroup->reavealMaps(this);
				}

				if (eFromState == UNIT_TRAVEL_STATE_FROM_EUROPE && eState == NO_UNIT_TRAVEL_STATE)
				{
					pGroup->disableLikelyToLeaveForImmigrant(this);//Les immigrants ne pourront plus partir des que leur convoi sera arriv� dans le nouveau monde.
					if (m_pUnitInfo->isMechUnit())
					{						
						int iExp = getLevelExploAndNavigation() == 0 ? 7 : 3; //0 level means no navigation promotion...
						changeExperienceExploAndNavigation(iExp);
					}
				}
			}
		}

	}
}

void CvUnit::moveToSeaway(int iIndex)
{
	if (iIndex != -1)
	{
		CvPlayer& pPlayer = GET_PLAYER(getOwner());
		moveToSeaway(pPlayer.getSeaway(iIndex));
	}
}
void CvUnit::moveToSeaway(CvSeaway* pSeaway)
{
	FAssert(pSeaway != NULL);
	if (pSeaway == NULL)
	{
		return;
	}
	CvSelectionGroup* pGroup = getGroup();
	if (pGroup != NULL)
	{
		if (!pGroup->at(pSeaway->getX(), pSeaway->getY()))
		{
			CLLNode<IDInfo>* pUnitNode = pGroup->headUnitNode();
			while (pUnitNode != NULL)
			{
				CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
				pUnitNode = pGroup->nextUnitNode(pUnitNode);
				pLoopUnit->setXY(pSeaway->getX(), pSeaway->getY(), true);
			}
		}
	}
	else if (!at(pSeaway->getX(), pSeaway->getY()))
	{
		setXY(pSeaway->getX(), pSeaway->getY());
	}
}

void CvUnit::setHomeCity(CvCity* pNewValue)
{
	if (pNewValue == NULL)
	{
		m_homeCity.reset();
	}
	else
	{
		if (AI_getUnitAIType() == UNITAI_WORKER)
		{
			CvCity* pExistingCity = getHomeCity();
			if (pExistingCity != NULL && pExistingCity != pNewValue)
			{
				getHomeCity()->AI_changeWorkersHave(-1);
			}
			pNewValue->AI_changeWorkersHave(+1);
		}
		m_homeCity = pNewValue->getIDInfo();
	}
}

CvCity* CvUnit::getHomeCity() const
{
	return ::getCity(m_homeCity);
}


void CvUnit::setWorkingCity(CvCity* pNewValue)
{
	CvCity* pCity = getWorkingCity();
	if (pNewValue == NULL)
	{
		m_workingCity.reset();
	}
	else
	{
		m_workingCity = pNewValue->getIDInfo();
	}
}

CvCity* CvUnit::getWorkingCity() const
{
	return ::getCity(m_workingCity);
}

bool CvUnit::isOnMap() const
{
	if (getUnitTravelState() != NO_UNIT_TRAVEL_STATE)
	{
		return false;
	}

	if ((getX_INLINE() == INVALID_PLOT_COORD) || (getY_INLINE() == INVALID_PLOT_COORD))
	{
		return false;
	}

	return true;
}


void CvUnit::doUnitTravelTimer()
{
	if (getUnitTravelTimer() > 0)
	{
		setUnitTravelTimer(getUnitTravelTimer() - 1);

		if (getUnitTravelTimer() == 0)
		{
			switch (getUnitTravelState())
			{
			case UNIT_TRAVEL_STATE_FROM_EUROPE:
				setUnitTravelState(NO_UNIT_TRAVEL_STATE, false);
				break;
			case UNIT_TRAVEL_STATE_TO_EUROPE:
				setUnitTravelState(UNIT_TRAVEL_STATE_IN_EUROPE, true);
				break;
			case UNIT_TRAVEL_STATE_LIVE_AMONG_NATIVES:
				setUnitTravelState(NO_UNIT_TRAVEL_STATE, false);
				doLearn();
				break;
			default:
				FAssertMsg(false, "Unit arriving from nowhere");
				break;
			}
		}
	}
}

bool CvUnit::isColonistLocked()
{
	return m_bColonistLocked;
}

void CvUnit::setColonistLocked(bool bNewValue)
{
	if (m_bColonistLocked != bNewValue)
	{
		m_bColonistLocked = bNewValue;

		if (bNewValue == true)
		{
			CvCity* pCity = GET_PLAYER(getOwnerINLINE()).getPopulationUnitCity(getID());

			FAssert(pCity != NULL);

			CvPlot* pPlot = pCity->getPlotWorkedByUnit(this);

			if (pPlot != NULL)
			{
				//Ensure it is not stolen.
				pPlot->setWorkingCityOverride(pCity);
			}
		}
	}
}

bool CvUnit::raidWeapons(std::vector<int>& aYields)
{
	CvPlayer& kOwner = GET_PLAYER(getOwnerINLINE());
	ProfessionTypes eCurrentProfession = getProfession();
	std::vector<ProfessionTypes> aProfessions;
	for (int iProfession = 0; iProfession < GC.getNumProfessionInfos(); ++iProfession)
	{
		ProfessionTypes eProfession = (ProfessionTypes) iProfession;
		if (canHaveProfession(eProfession, false))
		{
			if (eCurrentProfession == NO_PROFESSION || GC.getProfessionInfo(eProfession).getCombatChange() > GC.getProfessionInfo(eCurrentProfession).getCombatChange())
			{
				bool bCanHaveProfession = false;
				for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
				{
					YieldTypes eYield = (YieldTypes) iYield;
					int iYieldRequired = kOwner.getYieldEquipmentAmount(eProfession, eYield);
					if (iYieldRequired > 0)
					{
						bCanHaveProfession = true;
						if (eCurrentProfession != NO_PROFESSION)
						{
							iYieldRequired -= kOwner.getYieldEquipmentAmount(eCurrentProfession, eYield);
						}

						if (iYieldRequired > 0 && aYields[iYield] == 0)
						{
							bCanHaveProfession = false;
							break;
						}
					}
				}

				if (bCanHaveProfession)
				{
					aProfessions.push_back(eProfession);
				}
			}
		}
	}

	if (aProfessions.empty())
	{
		return false;
	}

	ProfessionTypes eProfession = aProfessions[GC.getGameINLINE().getSorenRandNum(aProfessions.size(), "Choose raid weapons")];
	setProfession(eProfession);

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		int iYieldRequired = kOwner.getYieldEquipmentAmount(eProfession, eYield);
		if (eCurrentProfession != NO_PROFESSION)
		{
			iYieldRequired -= kOwner.getYieldEquipmentAmount(eCurrentProfession, eYield);
		}

		if (iYieldRequired > 0)
		{
			aYields[iYield] = iYieldRequired;
		}
		else
		{
			aYields[iYield] = 0;
		}
	}

	return true;

}

bool CvUnit::raidWeapons(CvCity* pCity)
{
	if (!isNative())
	{
		return false;
	}

	if (!isEnemy(pCity->getTeam()))
	{
		return false;
	}

	if (GC.getGameINLINE().getSorenRandNum(100, "Weapons raid") < pCity->getDefenseModifier())
	{
		return false;
	}

	std::vector<int> aYields(NUM_YIELD_TYPES);
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		aYields[iYield] = pCity->getYieldStored((YieldTypes) iYield);
	}

	if (!raidWeapons(aYields))
	{
		return false;
	}

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (aYields[iYield] > 0)
		{
			pCity->changeYieldStored(eYield, -aYields[iYield]);

			CvWString szString = gDLL->getText("TXT_KEY_GOODS_RAIDED", GC.getCivilizationInfo(getCivilizationType()).getAdjectiveKey(), pCity->getNameKey(), aYields[iYield], GC.getYieldInfo(eYield).getTextKeyWide());
			gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szString, "AS2D_UNITCAPTURE", MESSAGE_TYPE_INFO, GC.getYieldInfo(eYield).getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pCity->getX_INLINE(), pCity->getY_INLINE());
			gDLL->getInterfaceIFace()->addMessage(pCity->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szString, "AS2D_UNITCAPTURE", MESSAGE_TYPE_INFO, GC.getYieldInfo(eYield).getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pCity->getX_INLINE(), pCity->getY_INLINE());
		}
	}
	return true;
}

bool CvUnit::raidWeapons(CvUnit* pUnit)
{
	if (!isNative())
	{
		return false;
	}

	FAssert(pUnit->isDead());

	if (!isEnemy(pUnit->getTeam()))
	{
		return false;
	}

	std::vector<int> aYields(NUM_YIELD_TYPES, 0);
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		CvPlayer& kOwner = GET_PLAYER(pUnit->getOwnerINLINE());
		if (pUnit->getProfession() != NO_PROFESSION)
		{
			aYields[iYield] += kOwner.getYieldEquipmentAmount(pUnit->getProfession(), (YieldTypes) iYield);
		}
	}

	if (!raidWeapons(aYields))
	{
		return false;
	}

	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (aYields[iYield] > 0)
		{
			CvWString szString = gDLL->getText("TXT_KEY_WEAPONS_CAPTURED", GC.getCivilizationInfo(getCivilizationType()).getAdjectiveKey(), pUnit->getNameOrProfessionKey(), aYields[iYield], GC.getYieldInfo(eYield).getTextKeyWide());
			gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szString, "AS2D_UNITCAPTURE", MESSAGE_TYPE_INFO, GC.getYieldInfo(eYield).getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pUnit->getX_INLINE(), pUnit->getY_INLINE());
			gDLL->getInterfaceIFace()->addMessage(pUnit->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szString, "AS2D_UNITCAPTURE", MESSAGE_TYPE_INFO, GC.getYieldInfo(eYield).getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pUnit->getX_INLINE(), pUnit->getY_INLINE());
		}
	}
	return true;
}

bool CvUnit::raidGoods(CvCity* pCity)
{
	if (!isNative())
	{
		return false;
	}

	if (!isEnemy(pCity->getTeam()))
	{
		return false;
	}

	if (GC.getGameINLINE().getSorenRandNum(100, "Goods raid") < pCity->getDefenseModifier())
	{
		return false;
	}

	std::vector<YieldTypes> aYields;
	for (int iYield = 0; iYield < NUM_YIELD_TYPES; ++iYield)
	{
		YieldTypes eYield = (YieldTypes) iYield;
		if (pCity->getYieldStored(eYield) > 0 && GC.getYieldInfo(eYield).isCargo())
		{
			aYields.push_back(eYield);
		}
	}

	if (aYields.empty())
	{
		return false;
	}

	YieldTypes eYield = aYields[GC.getGameINLINE().getSorenRandNum(aYields.size(), "Choose raid goods")];
	int iYieldsStolen = std::min(pCity->getYieldStored(eYield), GC.getGameINLINE().getCargoYieldCapacity() * GC.getDefineINT("NATIVE_GOODS_RAID_PERCENT") / 100);

	FAssert(iYieldsStolen > 0);
	if (iYieldsStolen <= 0)
	{
		return false;
	}

	pCity->changeYieldStored(eYield, -iYieldsStolen);

	GET_TEAM(getTeam()).AI_changeDamages(pCity->getTeam(), -GET_PLAYER(getOwnerINLINE()).AI_yieldValue(eYield, true, iYieldsStolen));

	CvCity* pHomeCity = getHomeCity();
	if (pHomeCity == NULL)
	{
		pHomeCity = GC.getMapINLINE().findCity(pCity->getX_INLINE(), pCity->getY_INLINE(), getOwnerINLINE());
	}
	if (pHomeCity != NULL)
	{
		pHomeCity->changeYieldStored(eYield, iYieldsStolen);
	}

	CvWString szString = gDLL->getText("TXT_KEY_GOODS_RAIDED", GC.getCivilizationInfo(getCivilizationType()).getAdjectiveKey(), pCity->getNameKey(), iYieldsStolen, GC.getYieldInfo(eYield).getTextKeyWide());
	gDLL->getInterfaceIFace()->addMessage(getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szString, "AS2D_UNITCAPTURE", MESSAGE_TYPE_INFO, GC.getYieldInfo(eYield).getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_GREEN"), pCity->getX_INLINE(), pCity->getY_INLINE());
	gDLL->getInterfaceIFace()->addMessage(pCity->getOwnerINLINE(), true, GC.getEVENT_MESSAGE_TIME(), szString, "AS2D_UNITCAPTURE", MESSAGE_TYPE_INFO, GC.getYieldInfo(eYield).getButton(), (ColorTypes)GC.getInfoTypeForString("COLOR_RED"), pCity->getX_INLINE(), pCity->getY_INLINE());
	return true;
}

void CvUnit::manageDisturbationFromCombat(CvUnit* pDefender)
{
	//Disturbation for trade route
	bool bDefenderRobbed = (pDefender->getPostCombatState() == POST_COMBAT_ROB);

	CvTradeRoute* pTradeRoute = pDefender->getTradeRoute();
	if (pTradeRoute != NULL)
	{
		CvCity* pCity = pTradeRoute->getSourceOrDestinationCity();
		if (pCity != NULL)
		{
			pCity->AI_changeNavalDisturbationCount(GC.getNAVAL_DISTURBATION_FOR_TRADE_ROUTE());
		}
	}

	if (isDead())
	{
		CvCity* pCityToProtect = AI_getCityToProtect();
		if (pCityToProtect != NULL)
		{
			pCityToProtect->AI_changeNavalDisturbationCount(GC.getNAVAL_DISTURBATION_FOR_MILITARY_SHIP_KILLED_PROTECTING_A_CITY());
		}
	}

	if (pDefender->isDead() || bDefenderRobbed)
	{
		CvCity* pCityToProtect = pDefender->AI_getCityToProtect();
		if (pCityToProtect != NULL)
		{
			pCityToProtect->AI_changeNavalDisturbationCount(GC.getNAVAL_DISTURBATION_FOR_MILITARY_SHIP_KILLED_PROTECTING_A_CITY());
		}
	}
}
