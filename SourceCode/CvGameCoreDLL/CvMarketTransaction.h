#pragma once

#ifndef CVSMARKETTRANSACTION_H
#define CVSMARKETTRANSACTION_H

#define NUM_AMOUNT_HIST_TURN 15

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  CLASS:      CvMarketTransaction
//!  \brief		Transaction betwwen europeans
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class DllExport CvMarketTransaction
{
public:
	CvMarketTransaction();
	~CvMarketTransaction();

	void init(PlayerTypes eToPlayer, YieldTypes eYield);
	void reset(PlayerTypes eToPlayer = NO_PLAYER, YieldTypes eYield = NO_YIELD);
	
	int getID() const;
	void setID(int iId);

	
	bool isAtWar();
	void setIsAtWar(bool bIsAtWar);

	static int getNumAmountHistTurn() { return NUM_AMOUNT_HIST_TURN; }

	int getTradeAmount() const;
	int getEuropeTradeTotalAmount() const;
	int getTradeAmountHistory(int eIndex) const;
	void addTradeAmountHistory(int iValue);

	PlayerTypes getToPlayer() const;
	YieldTypes getYield() const;

	void read(FDataStreamBase* pStream);
	void write(FDataStreamBase* pStream);

protected:	
	int m_iId;

	bool m_bIsAtWar;

	PlayerTypes m_eToPlayer;
	YieldTypes m_eYield;
	
	int* m_aiTradeAmountHistory;

	void setTradeAmountHistory(int eIndex, int iValue);
};


#endif	// CVSMARKETTRANSACTION_H
