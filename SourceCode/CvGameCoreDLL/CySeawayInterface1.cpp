#include "CvGameCoreDLL.h"
#include "CySeaway.h"
//
// published python interface for CySeaway
//
void CySeawayPythonInterface1(python::class_<CySeaway>& x)
{
	OutputDebugString("Python Extension Module - CySeawayPythonInterface1\n");
	x
		.def("isNone", &CySeaway::isNone, "bool () - Is this a valid unit instance?")
		.def("getID", &CySeaway::getID, "int ()")
		.def("getName", &CySeaway::getName, "string ()")
		.def("setName", &CySeaway::setName, "void (str)")
		.def("plot", &CySeaway::plot, python::return_value_policy<python::manage_new_object>(), "CyPlot* ()")
		.def("stepDistanceToPlot", &CySeaway::stepDistanceToPlot, "int (CyPlot*)")
		.def("isKnownTo", &CySeaway::isKnownTo, "bool (PlayerTypes ePlayer)")
		;
}
