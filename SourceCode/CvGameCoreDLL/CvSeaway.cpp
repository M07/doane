//------------------------------------------------------------------------------------------------
//
//  ***************** CIV4 GAME ENGINE   ********************
//
//! \file		CvSeaway.cpp
//! \author		M07
//! \brief		Implementation of seaways
//
//------------------------------------------------------------------------------------------------
//  Copyright (c) 2011 Fratelli Marconni, Inc. All rights reserved.
//------------------------------------------------------------------------------------------------

#include "CvGameCoreDLL.h"
#include "CvPlayerAI.h"
#include "CvSeaway.h"

//------------------------------------------------------------------------------------------------
// FUNCTION:    CvSeaway::CvSeaway
//! \brief      Default constructor.
//------------------------------------------------------------------------------------------------
CvSeaway::CvSeaway() :
	m_iId(-1),
	m_iX(-1),
	m_iY(-1),
	m_szName(L"")
{
}

void CvSeaway::init(const CvWString & szName, int iX, int iY)
{
	m_iX = iX;
	m_iY = iY;
	setName(szName);
	m_iId = m_iTotalSeaway++;
}

int CvSeaway::getID() const
{
	return m_iId;
}

int CvSeaway::getX() const
{
	return m_iX;
}

int CvSeaway::getY() const
{
	return m_iY;
}

void CvSeaway::setID(int iId)
{
	m_iId = iId;
}

const wchar* CvSeaway::getNameKey() const
{
	return m_szName.GetCString();
}

const CvWString & CvSeaway::getName() const
{
	return m_szName;
}


void CvSeaway::setName(const CvWString & szName)
{
	m_szName = szName == L"" ? CvWString::format(L"Seaway (%d, %d)", getX(), getY()): szName;
}

bool CvSeaway::isSamePlot(CvPlot* pPlot) const
{
	if (pPlot == NULL)
	{
		return false;
	}

	return pPlot->getX() == m_iX && pPlot->getY() == m_iY;
}

CvPlot* CvSeaway::plot() const
{
	return GC.getMapINLINE().plot(m_iX, m_iY);
}

int CvSeaway::stepDistanceToPlot(CvPlot* destinationPlot) const
{
	CvPlot* pStartingPlot = plot();

	if (destinationPlot == NULL || pStartingPlot == NULL)
	{
		return -1;
	}

	return stepDistanceAsTheCrowFlies(pStartingPlot->getX_INLINE(), pStartingPlot->getY_INLINE(), destinationPlot->getX_INLINE(), destinationPlot->getY_INLINE());
}

bool CvSeaway::isKnownTo(PlayerTypes ePlayer) const
{
	if (ePlayer == NO_PLAYER)
	{
		return false;
	}
	CvPlayer& pPlayer = GET_PLAYER(ePlayer);
	TeamTypes eTeam = pPlayer.getTeam();
	if (eTeam == NO_TEAM)
	{
		return false;
	}
	CvPlot* pPlot = plot();
	if (pPlot != NULL)
	{
		if (pPlot->isDefinitelyRevealed(eTeam))
		{
			return true;
		}
	}
	return false;
}

void CvSeaway::read(FDataStreamBase* pStream)
{
	uint uiFlag = 0;
	pStream->Read(&uiFlag);	// flags for expansion

	pStream->Read(&m_iTotalSeaway);
	pStream->Read(&m_iId);
	pStream->Read(&m_iX);
	pStream->Read(&m_iY);
	pStream->ReadString(m_szName);
}

void CvSeaway::write(FDataStreamBase* pStream)
{
	uint uiFlag = 0;
	pStream->Write(uiFlag);		// flag for expansion

	pStream->Write(m_iTotalSeaway);
	pStream->Write(m_iId);
	pStream->Write(m_iX);
	pStream->Write(m_iY);
	pStream->WriteString(m_szName);
}

int CvSeaway::m_iTotalSeaway = 0;
