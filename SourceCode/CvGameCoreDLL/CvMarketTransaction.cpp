//------------------------------------------------------------------------------------------------
//
//  ***************** CIV4 GAME ENGINE   ********************
//
//! \file		CvMarketTransaction.cpp
//! \author		M07
//! \brief		Implementation of market transaction betwwen europeans
//
//------------------------------------------------------------------------------------------------
//  Copyright (c) 2018 Fratelli Marconni, Inc. All rights reserved.
//------------------------------------------------------------------------------------------------

#include "CvGameCoreDLL.h"
#include "CvGlobals.h"
#include "CvGameAI.h"
#include "CvMarketTransaction.h"

//------------------------------------------------------------------------------------------------
// FUNCTION:    CvMarketTransaction::CvMarketTransaction
//! \brief      Default constructor.
//------------------------------------------------------------------------------------------------
CvMarketTransaction::CvMarketTransaction() :
	m_iId(-2),
	m_bIsAtWar(false),
	m_eToPlayer(NO_PLAYER),
	m_eYield(NO_YIELD)
{
	m_aiTradeAmountHistory = new int[NUM_AMOUNT_HIST_TURN];
	reset();
}

CvMarketTransaction::~CvMarketTransaction()
{
	SAFE_DELETE_ARRAY(m_aiTradeAmountHistory);
}

void CvMarketTransaction::init(PlayerTypes eToPlayer, YieldTypes eYield)
{
	reset(eToPlayer, eYield);
}

void CvMarketTransaction::reset(PlayerTypes eToPlayer, YieldTypes eYield)
{
	m_eToPlayer = eToPlayer;
	m_eYield = eYield;
	m_bIsAtWar = false;

	for (int iI = 0; iI < NUM_AMOUNT_HIST_TURN; ++iI)
	{
		m_aiTradeAmountHistory[iI] = -1;
	}
}

int CvMarketTransaction::getID() const
{
	return m_iId;
}

void CvMarketTransaction::setID(int iId)
{
	m_iId = iId;
}

bool CvMarketTransaction::isAtWar()
{
	return m_bIsAtWar;
}

void CvMarketTransaction::setIsAtWar(bool bIsAtWar)
{
	m_bIsAtWar = bIsAtWar;
}

int CvMarketTransaction::getEuropeTradeTotalAmount() const
{
	int iTotal = 0;
	for (int i = 0; i < NUM_AMOUNT_HIST_TURN; ++i)
	{
		int iResult = getTradeAmountHistory(i);
		iTotal += iResult > 0 ? iResult : 0;
	}
	return iTotal;
}

int CvMarketTransaction::getTradeAmount() const
{	
	int iTurn = GC.getGameINLINE().getGameTurn();
	return getTradeAmountHistory(iTurn % NUM_AMOUNT_HIST_TURN);
}

int CvMarketTransaction::getTradeAmountHistory(int eIndex) const
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_AMOUNT_HIST_TURN, "eIndex is expected to be within maximum bounds (invalid Index)");
	return m_aiTradeAmountHistory[eIndex];
}

void CvMarketTransaction::setTradeAmountHistory(int eIndex, int iValue)
{
	FAssertMsg(eIndex >= 0, "eIndex is expected to be non-negative (invalid Index)");
	FAssertMsg(eIndex < NUM_AMOUNT_HIST_TURN, "eIndex is expected to be within maximum bounds (invalid Index)");
	if (iValue != getTradeAmountHistory(eIndex))
	{
		m_aiTradeAmountHistory[eIndex] = iValue;
	}
}

void CvMarketTransaction::addTradeAmountHistory(int iValue)
{	
	int iTurn = GC.getGameINLINE().getGameTurn();
	setTradeAmountHistory(iTurn % NUM_AMOUNT_HIST_TURN, iValue);
}

PlayerTypes CvMarketTransaction::getToPlayer() const
{
	return m_eToPlayer;
}

YieldTypes CvMarketTransaction::getYield() const
{
	return m_eYield;
}

void CvMarketTransaction::read(FDataStreamBase* pStream)
{
	// Init data before load
	reset();

	uint uiFlag = 0;
	pStream->Read(&uiFlag);	// flags for expansion
	
	pStream->Read(&m_iId);
	pStream->Read(&m_bIsAtWar);
	pStream->Read((int*)&m_eToPlayer);
	pStream->Read((int*)&m_eYield);

	pStream->Read(NUM_AMOUNT_HIST_TURN, m_aiTradeAmountHistory);
}

void CvMarketTransaction::write(FDataStreamBase* pStream)
{
	uint uiFlag = 0;
	pStream->Write(uiFlag);		// flag for expansion
	
	pStream->Write(m_iId);
	pStream->Write(m_bIsAtWar);
	pStream->Write(m_eToPlayer);
	pStream->Write(m_eYield);

	pStream->Write(NUM_AMOUNT_HIST_TURN, m_aiTradeAmountHistory);
}