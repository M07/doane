//------------------------------------------------------------------------------------------------
//
//  ***************** CIV4 GAME ENGINE   ********************
//
//! \file		CvConvoy.cpp
//! \author		M07
//! \brief		Handle units inside a convoy
//
//------------------------------------------------------------------------------------------------
//  Copyright (c) 2020 Fratelli Marconni, Inc. All rights reserved.
//------------------------------------------------------------------------------------------------

#include "CvGameCoreDLL.h"
#include "CvTradeRoute.h"
#include "CvPlayerAI.h"
#include "CvInfos.h"
#include "CvDLLInterfaceIFaceBase.h"
#include "CvConvoy.h"


//----------------------------------------------------------------------------
//
//	FUNCTION:	CvConvoy()
//
//	PURPOSE:	Constructor
//
//----------------------------------------------------------------------------
CvConvoy::CvConvoy() 
{
	m_aucLoadedCargoPercent = new unsigned char[GC.getNUM_TRADE_ROUTE_HIST_TURN()];
	m_aiTradeProfits = new int[GC.getNUM_TRADE_ROUTE_HIST_TURN()];

	reset();
}

CvConvoy::~CvConvoy()
{
	m_units.clear();
	SAFE_DELETE_ARRAY(m_aucLoadedCargoPercent);
	SAFE_DELETE_ARRAY(m_aiTradeProfits);
}

void CvConvoy::init(int iId, PlayerTypes eOwner)
{
	reset(iId, eOwner);
}

void CvConvoy::reset(int iId, PlayerTypes eOwner)
{
	m_iId = iId;
	m_eOwner = eOwner;
	m_iAssignedTradeRouteId = -1;
	m_iTravelRotation = 0;
	m_iCurrentTurnLoadedAmount = 0;
	m_iCurrentTurnMaxCargoAmount = 0;
	m_iCurrentTurnTradeProfits = 0;
	m_bHasProcessedTravelRotation = false;
	
	int iI;
	for (iI = 0; iI < GC.getNUM_TRADE_ROUTE_HIST_TURN(); iI++)
	{
		m_aucLoadedCargoPercent[iI] = 0;
		m_aiTradeProfits[iI] = 0;
	}

}

int CvConvoy::getID() const
{
	return m_iId;
}

void CvConvoy::setID(int iId)
{
	m_iId = iId;
}

PlayerTypes CvConvoy::getOwner() const
{
	return m_eOwner;
}

int CvConvoy::getAssignedTradeRouteId() const
{
	return m_iAssignedTradeRouteId;
}

CvTradeRoute* CvConvoy::getAssignedTradeRoute() const
{
	int iAssignedTradeRouteId = getAssignedTradeRouteId();
	if (iAssignedTradeRouteId < 0)
	{
		return NULL;
	}

	CvPlayer& kPlayer = GET_PLAYER(getOwner());	
	return kPlayer.getTradeRoute(iAssignedTradeRouteId);
}

CvCity* CvConvoy::getDestinationCity() const
{
	CvTradeRoute* pTradeRoute = getAssignedTradeRoute();
	if (pTradeRoute == NULL)
	{		
		return NULL;
	}

	if (pTradeRoute->getDestinationCity().iID == EUROPE_CITY_ID)
	{
		return NULL; 
	}

	return ::getCity(pTradeRoute->getDestinationCity());
}

void CvConvoy::setAssignedTradeRouteId(int iTradeRouteId)
{
	m_iAssignedTradeRouteId = iTradeRouteId;
}

int CvConvoy::getNumUnits() const
{
	return m_units.getLength();
}

void CvConvoy::addUnit(CvUnit* pUnit)
{
	CvPlayerAI& kPlayer = GET_PLAYER(getOwner());
	CvTradeRoute* pTradeRoute = getAssignedTradeRoute();
	const bool isFleet = pTradeRoute->isEuropeanRoute();
	if (isFleet)
	{
		int countMerchantUnits = 0;
		int countMilitaryUnits = 0;
		const bool isOnlyDefensive = pUnit->getUnitInfo().isOnlyDefensive();
		UnitTypes eBestUnitType = isOnlyDefensive ? kPlayer.AI_getBestAllowedMerchantUnit() : kPlayer.AI_getBestAllowedMilitaryUnit();
		countUnitByType(countMerchantUnits, countMilitaryUnits, NO_UNIT, NO_UNIT);

		const int countUnits = isOnlyDefensive ? countMerchantUnits : countMilitaryUnits;
		// At least one on the convoy unit should not be the best unit to have in the convoy
		if (countUnits >= (isOnlyDefensive ? MAX_FLEET_MERCHANT_UNITS : MAX_FLEET_MILITARY_UNITS))
		{
			CvUnit* pWorstUnit = findWorstUnit(isOnlyDefensive, eBestUnitType);
			if (pWorstUnit != NULL)
			{
				pWorstUnit->setMustStayInEurope(true);
			}
		}
	}
	
	m_units.insertAtEnd(pUnit->getIDInfo());
	pUnit->setConvoyID(getID());
}

CvUnit* CvConvoy::findWorstUnit(bool isOnlyDefensive, UnitTypes eBestUnitType) const
{
	int iBestScore = 0;
	CvUnit* pWorstunit = NULL;
	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);
		FAssertMsg(pLoopUnit != NULL, "Expected pLoopUnit to be not null");
		if (pLoopUnit != NULL)
		{
			const bool isLoopUnitOnlyDefensive = pLoopUnit->isOnlyDefensive();
			const UnitTypes eUnitType = pLoopUnit->getUnitType();
			if (eBestUnitType == eUnitType || pLoopUnit->mustStayInEurope())
			{
				continue;
			}

			int iScore = 0;
			if (isOnlyDefensive && isLoopUnitOnlyDefensive)
			{
				iScore = GC.getUnitInfo(eBestUnitType).getCargoNewSpace() - GC.getUnitInfo(eUnitType).getCargoNewSpace();
			}
			if ( !isOnlyDefensive && !isLoopUnitOnlyDefensive)
			{
				iScore = GC.getUnitInfo(eBestUnitType).getCombat() - GC.getUnitInfo(eUnitType).getCombat();
			}

			if (iScore > iBestScore)
			{
				iBestScore = iScore;
				pWorstunit = pLoopUnit;
			}
		}
	}
	return pWorstunit;
}

void CvConvoy::removeUnit(CvUnit* pUnit, bool bRemoveLink) 
{
	CLinkList<IDInfo> otherUnits;
	CLLNode<IDInfo>* pUnitNode = headUnitNode();

	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = getUnit(pUnitNode->m_data);
		FAssertMsg(pLoopUnit != NULL, "Expected pLoopUnit to be not null");
		if (pLoopUnit != NULL)
		{
			if (pLoopUnit->getID() != pUnit->getID())
			{				
				otherUnits.insertAtEnd(pUnitNode->m_data);
			}
			else
			{
				if (bRemoveLink) 
				{
					pUnit->setConvoyID(-1);
				}
			}
		}
		pUnitNode = nextUnitNode(pUnitNode);
	}

	m_units.clear();

	pUnitNode = otherUnits.head();
	while (pUnitNode != NULL)
	{
		m_units.insertAtEnd(pUnitNode->m_data);
		pUnitNode = otherUnits.next(pUnitNode);
	}
}

void CvConvoy::detach()
{
	CLLNode<IDInfo>* pUnitNode = headUnitNode();

	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);
		FAssertMsg(pLoopUnit != NULL, "Expected pLoopUnit to be not null");
		if (pLoopUnit != NULL)
		{
			pLoopUnit->setConvoyID(-1);
		}
	}
	m_units.clear();
}

bool CvConvoy::hasUnit(int iUnitId) const
{
	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);
		FAssertMsg(pLoopUnit != NULL, "Expected pLoopUnit to be not null");
		if (pLoopUnit != NULL && pLoopUnit->getID() == iUnitId)
		{
			return true;
		}
	}
	return false;
}


CvUnit* CvConvoy::getUnit(IDInfo idInfo) const
{
	CvPlayer& kPlayer = GET_PLAYER(getOwner());
	CvUnit* pLoopUnit = ::getUnit(idInfo);
	return pLoopUnit != NULL ? pLoopUnit : kPlayer.getEuropeUnitById(idInfo.iID);
}

CLLNode<IDInfo>* CvConvoy::nextUnitNode(CLLNode<IDInfo>* pNode) const
{
	return m_units.next(pNode);
}

CLLNode<IDInfo>* CvConvoy::headUnitNode() const
{
	return m_units.head();
}

void CvConvoy::calculateRequiredUnits(int& requiredMerchantUnits, int& requiredMilitaryUnits, DomainTypes domainType, bool bCheckLoading, UnitTypes eTargetUnit) const
{
	CvPlayerAI& kPlayer = GET_PLAYER(getOwner());
	CvTradeRoute* pTradeRoute = getAssignedTradeRoute();
	const bool isFleet = pTradeRoute->isEuropeanRoute();
	UnitTypes eBestMerchantUnitType = NO_UNIT;
	UnitTypes eBestMilitaryUnitType = NO_UNIT;
	bool isGoodunit = true;

	switch (domainType)
	{
		case DOMAIN_SEA:
			if (!isFleet)
				return;
			eBestMerchantUnitType = kPlayer.AI_getBestAllowedMerchantUnit();
			eBestMilitaryUnitType = kPlayer.AI_getBestAllowedMilitaryUnit();
			if (eTargetUnit != NO_UNIT && eTargetUnit != eBestMerchantUnitType && eTargetUnit != eBestMilitaryUnitType)
			{
				isGoodunit = false;
			}
			break;
		case DOMAIN_LAND:
			if (isFleet)
				return;
			break;
	}

	int iTempRequiredMilitaryUnits = isFleet ? MAX_FLEET_MILITARY_UNITS : MAX_WAGON_MILITARY_UNITS;
	int iTempRequiredMerchantUnits = isFleet? MAX_FLEET_MERCHANT_UNITS : MAX_WAGON_MERCHANT_UNITS;

	int countMerchantUnits = 0;
	int countMilitaryUnits = 0;
	countUnitByType(countMerchantUnits, countMilitaryUnits, eBestMerchantUnitType, eBestMilitaryUnitType);

	iTempRequiredMerchantUnits -= countMerchantUnits;
	iTempRequiredMilitaryUnits -= countMilitaryUnits;

	if (bCheckLoading && countMerchantUnits >= 2 && getLoadedCargoPercent() < 80)
	{
		iTempRequiredMerchantUnits = 0;
	}

	if (countMilitaryUnits * 2 >= countMerchantUnits)
	{
		iTempRequiredMilitaryUnits = 0;
	}

	// We only accept bad units when the convoy is not really operational
	if (!isGoodunit)
	{
		countMerchantUnits = 0;
		countMilitaryUnits = 0;
		countUnitByType(countMerchantUnits, countMilitaryUnits, NO_UNIT, NO_UNIT);
		if (GC.getUnitInfo(eTargetUnit).isOnlyDefensive())
		{
			if (countMerchantUnits > 4)
			{
				iTempRequiredMerchantUnits = 0;
			}
		}
		else
		{
			if (countMilitaryUnits > 2)
			{
				iTempRequiredMilitaryUnits = 0;
			}
		}
	}

	requiredMerchantUnits += iTempRequiredMerchantUnits;
	requiredMilitaryUnits += iTempRequiredMilitaryUnits;
}

void CvConvoy::countUnitByType(int& countMerchantUnits, int& countMilitaryUnits, UnitTypes eBestMerchantType, UnitTypes eBestMilitaryType) const
{
	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);
		FAssertMsg(pLoopUnit != NULL, "Expected pLoopUnit to be not null");
		if (pLoopUnit != NULL)
		{
			if (pLoopUnit->isOnlyDefensive())
			{
				if (eBestMerchantType == NO_UNIT || pLoopUnit->getUnitType() == eBestMerchantType)
				{
					countMerchantUnits++;
				}
			}
			else
			{
				if (eBestMilitaryType == NO_UNIT || pLoopUnit->getUnitType() == eBestMilitaryType)
				{
					countMilitaryUnits++;
				}
			}
		}
	}
}

int CvConvoy::getNewCargoYield(YieldTypes eYield) const
{
	int iCount = 0;
	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);
		FAssertMsg(pLoopUnit != NULL, "Expected pLoopUnit to be not null");
		if (pLoopUnit != NULL)
		{
			if (pLoopUnit->isOnlyDefensive())
			{
				iCount += pLoopUnit->getNewCargoYield(eYield);
			}
		}
	}
	return iCount;
}

DomainTypes CvConvoy::getDomainType() const
{
	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	if (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = getUnit(pUnitNode->m_data);
		FAssertMsg(pLoopUnit != NULL, "Expected pLoopUnit to be not null");
		if (pLoopUnit != NULL)
		{
			return pLoopUnit->getDomainType();
		}
	}
	return NO_DOMAIN;
}

int CvConvoy::getCargoNewSpace() const
{
	int iTotalAmount = 0;
	CLLNode<IDInfo>* pUnitNode = headUnitNode();
	while (pUnitNode != NULL)
	{
		CvUnit* pLoopUnit = ::getUnit(pUnitNode->m_data);
		pUnitNode = nextUnitNode(pUnitNode);
		FAssertMsg(pLoopUnit != NULL, "Expected pLoopUnit to be not null");
		if (pLoopUnit != NULL && pLoopUnit->isOnlyDefensive())
		{
			iTotalAmount += pLoopUnit->getUnitInfo().getCargoNewSpace();
		}
	}
	return iTotalAmount;
}

int CvConvoy::getLoadedCargoPercent() const
{
	return getLoadedCargoPercent(getTravelRotation());
}

unsigned char CvConvoy::getLoadedCargoPercent(int iRotation) const
{
	return m_aucLoadedCargoPercent[iRotation % GC.getNUM_TRADE_ROUTE_HIST_TURN()];
}

void CvConvoy::setLoadedCargoPercent(int iRotation, unsigned char ucNewValue)
{
	m_aucLoadedCargoPercent[iRotation % GC.getNUM_TRADE_ROUTE_HIST_TURN()] = ucNewValue;
}

int CvConvoy::getTradeProfits(int iRotation) const
{
	return m_aiTradeProfits[iRotation % GC.getNUM_TRADE_ROUTE_HIST_TURN()];
}

void CvConvoy::setTradeProfits(int iRotation, int iTradeProfits)
{
	m_aiTradeProfits[iRotation % GC.getNUM_TRADE_ROUTE_HIST_TURN()] = iTradeProfits;
}

bool CvConvoy::hasProcessedTravelRotation() const
{
	return m_bHasProcessedTravelRotation;
}

void CvConvoy::setProcessedTravelRotation(bool bNewValue)
{
	m_bHasProcessedTravelRotation = bNewValue;
}

void CvConvoy::doTurn()
{
	if (!hasProcessedTravelRotation())
	{
		return;
	}
	int amountLoaded = getCurrentTurnLoadedAmount();
	int totalNewCargo = getCurrentTurnMaxCargoAmount();
	unsigned char ucLoadedCargoPercent = totalNewCargo > 0 ? amountLoaded * 100 / totalNewCargo : 0;
	
	incrementTravelRotation();
	setLoadedCargoPercent(getTravelRotation(), ucLoadedCargoPercent);
	setTradeProfits(getTravelRotation(), getCurrentTurnTradeProfitsAmount());
	
	setProcessedTravelRotation(false);
	setCurrentTurnLoadedAmount(0);
	setCurrentTurnMaxCargoAmount(0);
	setCurrentTurnTradeProfitsAmount(0);
}

bool CvConvoy::isFullCapacity() const
{
	int requiredMerchantUnit = 0;
	int requiredMilitaryUnits = 0;
	DomainTypes domainType = getDomainType();
	if (domainType == NO_DOMAIN)
	{
		return false;
	}

	calculateRequiredUnits(requiredMerchantUnit, requiredMilitaryUnits, domainType, false);
	int iLoadedCargoPercent = getLoadedCargoPercent();
	return requiredMerchantUnit == 0 && iLoadedCargoPercent == 100;
}

void CvConvoy::processTravelRotation(int iAmount, int iMaxCargoAmount, int iProfits)
{
	FAssert(iProfits >= 0);
	changeCurrentTurnLoadedAmount(iAmount);
	changeCurrentTurnTradeProfitsAmount(iProfits);
	changeCurrentTurnMaxCargoAmount(iMaxCargoAmount);
	setProcessedTravelRotation(true);
}

int CvConvoy::getTravelRotation() const
{
	return m_iTravelRotation;
}

void CvConvoy::incrementTravelRotation()
{
	m_iTravelRotation++;
}

int CvConvoy::getCurrentTurnLoadedAmount() const
{
	return m_iCurrentTurnLoadedAmount;
}

void CvConvoy::setCurrentTurnLoadedAmount(int iCurrentTurnLoadedAmount)
{
	m_iCurrentTurnLoadedAmount = iCurrentTurnLoadedAmount;
}

void CvConvoy::changeCurrentTurnLoadedAmount(int iChange)
{
	setCurrentTurnLoadedAmount(getCurrentTurnLoadedAmount() + iChange);
}

int CvConvoy::getCurrentTurnMaxCargoAmount() const
{
	return m_iCurrentTurnMaxCargoAmount;
}

void CvConvoy::setCurrentTurnMaxCargoAmount(int iCurrentTurnMaxCargoAmount)
{
	m_iCurrentTurnMaxCargoAmount = iCurrentTurnMaxCargoAmount;
}

void CvConvoy::changeCurrentTurnMaxCargoAmount(int iChange)
{
	setCurrentTurnMaxCargoAmount(getCurrentTurnMaxCargoAmount() + iChange);
}

int CvConvoy::getCurrentTurnTradeProfitsAmount() const
{
	return m_iCurrentTurnTradeProfits;
}

void CvConvoy::setCurrentTurnTradeProfitsAmount(int iCurrentTurnTradeProfits)
{
	m_iCurrentTurnTradeProfits = iCurrentTurnTradeProfits;
}

void CvConvoy::changeCurrentTurnTradeProfitsAmount(int iChange)
{
	setCurrentTurnTradeProfitsAmount(getCurrentTurnTradeProfitsAmount() + iChange);
}

//Average
int CvConvoy::getLoadedCargoHistPercent() const
{	
	unsigned int iAmount = 0, iTotalCargoSpace = 0;
	
	int iTravelRotation = getTravelRotation();
	int iCargoSpace = getCargoNewSpace();
	if (iTravelRotation > 0 && iCargoSpace > 0)
	{
		int iMaxTravelRotation = std::min(iTravelRotation, (int)GC.getNUM_TRADE_ROUTE_HIST_TURN());
		for (int iI = 0; iI < iMaxTravelRotation; ++iI)
		{
			iAmount += getLoadedCargoPercent(iTravelRotation - iI) * iCargoSpace;
			iTotalCargoSpace += iCargoSpace;
		}
	}
	return iTotalCargoSpace > 0 ? iAmount/iTotalCargoSpace : 0;
}

int CvConvoy::getTradeProfits() const
{
	return getTradeProfits(getTravelRotation());
}

// Average
int CvConvoy::getTradeProfitsHist() const
{	
	unsigned int iProfits = 0;
	
	int iTravelRotation = getTravelRotation();
	if (iTravelRotation > 0)
	{
		int iMaxTravelRotation = std::min(iTravelRotation, (int)GC.getNUM_TRADE_ROUTE_HIST_TURN());
		int iUnitProfits = 0;
		for (int iI = 0; iI < iMaxTravelRotation; ++iI)
		{
			iUnitProfits += getTradeProfits(iTravelRotation - iI);
		}

		if (iMaxTravelRotation > 0)
		{
			iProfits += iUnitProfits / iMaxTravelRotation;
		}
	}
	return iProfits;
}

void CvConvoy::read(FDataStreamBase* pStream)
{
	// Init data before load
	reset();

	uint uiFlag = 0;
	pStream->Read(&uiFlag);	// flags for expansion
	
	pStream->Read(&m_iId);
	pStream->Read(&m_iTravelRotation);
	pStream->Read(&m_iCurrentTurnLoadedAmount);
	pStream->Read(&m_iCurrentTurnMaxCargoAmount);	
	pStream->Read(&m_iCurrentTurnTradeProfits);

	pStream->Read(&m_bHasProcessedTravelRotation);
	
	pStream->Read((int*)&m_eOwner);
	pStream->Read(&m_iAssignedTradeRouteId);
	
	pStream->Read(GC.getNUM_TRADE_ROUTE_HIST_TURN(), m_aucLoadedCargoPercent);
	pStream->Read(GC.getNUM_TRADE_ROUTE_HIST_TURN(), m_aiTradeProfits);

	m_units.Read(pStream);
}

void CvConvoy::write(FDataStreamBase* pStream)
{
	uint uiFlag = 0;
	pStream->Write(uiFlag);		// flag for expansion
	
	pStream->Write(m_iId);
	pStream->Write(m_iTravelRotation);
	pStream->Write(m_iCurrentTurnLoadedAmount);
	pStream->Write(m_iCurrentTurnMaxCargoAmount);	
	pStream->Write(m_iCurrentTurnTradeProfits);

	pStream->Write(m_bHasProcessedTravelRotation);
	
	pStream->Write(m_eOwner);
	pStream->Write(m_iAssignedTradeRouteId);

	pStream->Write(GC.getNUM_TRADE_ROUTE_HIST_TURN(), m_aucLoadedCargoPercent);
	pStream->Write(GC.getNUM_TRADE_ROUTE_HIST_TURN(), m_aiTradeProfits);

	m_units.Write(pStream);
}
