#pragma once

#ifndef CyTradeRoute_h
#define CyTradeRoute_h


class CyCity;
class CvTradeRoute;

class CyTradeRoute
{
public:
	CyTradeRoute();
	CyTradeRoute(CvTradeRoute* pTradeRoute);

	int getID() const;
	int getGoldAmountToAlwaysConserve() const;
	std::wstring getName();
	void setName(std::wstring szNewValue);
	IDInfo getSourceCity() const;
	IDInfo getDestinationCity() const;
	std::wstring getSourceCityNameKey() const;
	std::wstring getDestinationCityNameKey() const;
	std::wstring getSourceCityName() const;
	std::wstring getDestinationCityName() const;
	PlayerTypes getOwner() const;
	TradeTypes getTradeType(/*YieldTypes*/int iYield) const;
	OptionInfo getRadioOption(/*YieldTypes*/int iYield, /*TradeRouteRadioTypes*/int iTradeRouteRadioType) const;
	AdvancedOptionInfo getEuropeanRadioOption(/*YieldTypes*/int iYield, /*TradeRouteRadioEuropeanTypes*/int iTradeRouteRadioEuropeanType) const;
	OptionInfo getCheckBoxOption(/*YieldTypes*/int iYield, /*TradeRouteCheckBoxTypes*/int iTradeRouteCheckBoxType) const;
	int getTransportUnitsCapacity() const;
	int getYieldValue(/*YieldTypes*/int iYield) const;
	int getLoadedCargoPercent() const;
	int getLoadedCargoHistPercent() const;
	int getTradeProfits() const;
	int getTradeProfitsHist() const;
	unsigned int getTotalProfits() const;
	TransportTypes getTransportType() const;

protected:
	CvTradeRoute* m_pTradeRoute;
};

#endif	// #ifndef CyTradeRoute_h
