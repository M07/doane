#pragma once

#ifndef CyAgreement_h
#define CyAgreement_h

class CyCity;
class CvAgreement;

class CyAgreement
{
public:
	CyAgreement();
	CyAgreement(CvAgreement* pAgreement);

	int getID() const;
	std::wstring getMerchantName() const;
	IDInfo getSourceCity() const;
	IDInfo getDestinationCity() const;
	std::wstring getSourceCityName() const;
	std::wstring getDestinationCityName() const;

	int getTurnMax() const;
	int getTurnCreated() const;
	int getImportYield() const;
	int getExportYield() const;
	int getFixedPrice() const;
	int getPrime() const;
	int getNumAssignedGroups() const;
	int getTurnLeft() const;
	int getOriginalAmount(int /*YieldTypes*/ eYield) const;
	int getActualAmount(int /*YieldTypes*/ eYield) const;
	
protected:
	CvAgreement* m_pAgreement;
};

#endif	// #ifndef CyAgreement_h
