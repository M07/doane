#pragma once

#ifndef CIV4_TRADE_ROUTE_H
#define CIV4_TRADE_ROUTE_H

class CvConvoy;

class CvTradeRoute
{
public:
	CvTradeRoute();
	~CvTradeRoute();

	void init(int iID, const IDInfo& kSourceCity, const IDInfo& kDestinationCity);
	void reset(int iID = 0, bool bConstructorCall = false);

	int getID() const;
	void setID(int iId);

	int getGoldAmountToAlwaysConserve() const;
	void setGoldAmountToAlwaysConserve(int iValue);

	const wchar* getNameKey() const;
	const CvWString & getName() const;
	void setName(const CvWString & szName);

	CvCity* getSourceOrDestinationCity() const;
	const IDInfo& getSourceCity() const;
	void setSourceCity(const IDInfo& kCity);
	const wchar* getSourceCityNameKey() const;

	const IDInfo& getDestinationCity() const;
	void setDestinationCity(const IDInfo& kCity);
	const wchar* getDestinationCityNameKey() const;
	const wchar* getCityNameKey() const;

	bool isEuropeanRoute() const;
	
	PlayerTypes getOwner() const;

	TradeTypes getTradeType(YieldTypes eYield) const;
	void setTradeType(YieldTypes eYield, TradeTypes eValue);

	void ensureTheNeeds(YieldTypes eYield, TradeTypes eTrade, int iQuantityToEnsure);
	void buyUntilQuantityReached(YieldTypes eYield, int iQuantityToReach, int iMaxQuantityByTravel);

	unsigned int getTotalProfits() const;
	unsigned int getTotalProfitsYield(YieldTypes eYield) const;
	void setTotalProfitsYield(YieldTypes eYield, unsigned int eValue);
	void changeTotalProfitsYield(YieldTypes eYield, unsigned int iChange);

	TradeRouteRadioTypes getSelectedRadioOption(YieldTypes eYield) const;
	OptionInfo getRadioOption(YieldTypes eYield, TradeRouteRadioTypes eType) const;
	void setRadioOption(YieldTypes eYield, TradeRouteRadioTypes eType, bool bValue);
	void setRadioOptionQuantity(YieldTypes eYield, int iValue);
	void changeRadioOptionQuantity(YieldTypes eYield, TradeRouteRadioTypes eType, int iChange);

	TradeRouteRadioEuropeanTypes getSelectedEuropeanRadioOption(YieldTypes eYield) const;
	AdvancedOptionInfo getEuropeanRadioOption(YieldTypes eYield, TradeRouteRadioEuropeanTypes eType) const;
	void setEuropeanRadioOption(YieldTypes eYield, TradeRouteRadioEuropeanTypes eType, bool bValue);
	void setEuropeanRadioOptionQuantity(YieldTypes eYield, int iValue);
	void changeEuropeanRadioOptionQuantity(YieldTypes eYield, TradeRouteRadioEuropeanTypes eType, int iChange);

	OptionInfo getCheckBoxOption(YieldTypes eYield, TradeRouteCheckBoxTypes eType) const;
	void setCheckBoxOption(YieldTypes eYield, TradeRouteCheckBoxTypes eType, bool bValue);
	void setCheckBoxOptionQuantity(YieldTypes eYield, TradeRouteCheckBoxTypes eType, int iValue);
	
	bool checkValid(PlayerTypes ePlayer) const;

	int getLoadedCargoPercent() const;
	int getLoadedCargoHistPercent() const;
	int getTradeProfits() const;
	int getTradeProfitsHist() const;
	int getTransportUnitsCapacity() const;
	int getYieldValue(YieldTypes eYield) const;

	bool isRadioOptionWithQuantity(TradeRouteRadioTypes eType) const;
	bool isCheckBoxOptionWithQuantity(TradeRouteCheckBoxTypes eType) const;
	int getNumYieldsByTradeTypes(TradeTypes eTrade) const;

	TransportTypes getTransportType() const;
	void calculateRequiredUnits(int& requiredMerchantUnits, int& requiredMilitaryUnits, DomainTypes domainType) const;
	CvConvoy* AI_getBestConvoyForUnit(UnitTypes eUnit, int& iScore) const;

	CvConvoy* addConvoy();
	void removeConvoy(int iConvoyId);
	int getNumUnitsAffectedToRoute() const;
	void AI_processConvoys();

	void doTurn();

	void read(FDataStreamBase* pStream);
	void write(FDataStreamBase* pStream);

	static const int EUROPE_CITY_ID = -1;
	static const int ANYWHERE_CITY_ID = -2;

protected:
	int m_iId;
	int m_iGoldAmountToAlwaysConserve;

	IDInfo m_kSourceCity;
	IDInfo m_kDestinationCity;

	CvWString m_szName;
	char* m_aYields;
	OptionInfo** m_ppRadioOption;
	AdvancedOptionInfo** m_ppEuropeanRadioOption;
	OptionInfo** m_ppCheckBoxOption;
	unsigned int* m_aTotalProfitsYield;
	std::vector<int> m_aiConcoys;
};


#endif  // CIV4_TRADE_ROUTE_H
