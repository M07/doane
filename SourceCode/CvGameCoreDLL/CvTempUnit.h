#pragma once

#ifndef CVTEMPUNIT_H
#define CVTEMPUNIT_H

#include "CvString.h"

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  CLASS:      CvTempUnit
//!  \brief		TempUnits for ships and immigrants
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class DllExport CvTempUnit
{
public:
	CvTempUnit();	
	virtual ~CvTempUnit();

	void init(int iID, UnitTypes eUnit, PlayerTypes eOwner, ProfessionTypes eProfessionType);
	void uninit();
	void reset(int iID = 0, UnitTypes eUnit = NO_UNIT, PlayerTypes eOwner = NO_PLAYER, ProfessionTypes eProfessionType = NO_PROFESSION, bool bConstructorCall = false);
	int getID() const;	
	void setID(int iId);
	int getGameTurnCreated() const;
	void setGameTurnCreated(int iTurn);
	int getPrice() const;
	void setPrice(int iPrice);
	int getDamage() const;
	void setDamage(int iDamage);
	int getPromotionLevel() const;
	void setPromotionLevel(int iPromotionLevel);
	
	bool requestTraining() const;
	void setRequestTraining(bool bNewValue);
	
	UnitTypes getUnitType() const;
	CvUnitInfo &getUnitInfo() const;	
	
	ProfessionTypes getProfession() const;
	void setProfession(ProfessionTypes eProfession);
	
	ImmigrationTypes getImmigrationType() const;
	void setImmigrationType(ImmigrationTypes eType);
	
	PlayerTypes getOwner() const;
	CivilizationTypes getCivilizationType() const;
	const CvArtInfoUnit* getArtInfo(int i) const;
	const TCHAR* getFullLengthIcon() const;
	
	const CvWString getName(uint uiForm = 0) const;
	const CvWString getNameAndProfession() const;
	
	CvUnit* doRecruitImmigrant();
	void buyShip(int iPrice);	
	int getSailorPrice() const;
	void generatePromotionLevel();
	void generatePrice();
	void doRecruitCrew(int iFormationType = 0);
	
	
	void read(FDataStreamBase* pStream);
	void write(FDataStreamBase* pStream);

protected:
	int m_iId;
	int m_iTurnCreated;
	int m_iPrice;
	int m_iDamage;
	int m_iPromotionLevel;
	
	bool m_bRequestTraining;
	
	UnitTypes m_eUnitType;
	PlayerTypes	m_eOwner;
	ProfessionTypes	m_eProfessionType;
	CvUnitInfo *m_pUnitInfo;	
	ImmigrationTypes m_eImmigrationType;
};


#endif	// CVTEMPUNIT_H
