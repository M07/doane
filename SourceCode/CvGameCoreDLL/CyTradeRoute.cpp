#include "CvGameCoreDLL.h"
#include "CyTradeRoute.h"
#include "CvTradeRoute.h"
#include "CyCity.h"


CyTradeRoute::CyTradeRoute() :
m_pTradeRoute(NULL)
{
}

CyTradeRoute::CyTradeRoute(CvTradeRoute* pTradeRoute) :
	m_pTradeRoute(pTradeRoute)
{
}

int CyTradeRoute::getID() const
{
	return m_pTradeRoute ? m_pTradeRoute->getID() : -1;
}

int CyTradeRoute::getGoldAmountToAlwaysConserve() const
{
	return m_pTradeRoute ? m_pTradeRoute->getGoldAmountToAlwaysConserve() : -1;
}

std::wstring CyTradeRoute::getName()
{
	return m_pTradeRoute ? m_pTradeRoute->getName() : std::wstring();
}

void CyTradeRoute::setName(std::wstring szNewValue)
{
	if (m_pTradeRoute)
		m_pTradeRoute->setName(szNewValue);
}

IDInfo CyTradeRoute::getSourceCity() const
{
	return (m_pTradeRoute ? m_pTradeRoute->getSourceCity() : IDInfo());
}

IDInfo CyTradeRoute::getDestinationCity() const
{
	return (m_pTradeRoute ? m_pTradeRoute->getDestinationCity() : IDInfo());
}

std::wstring CyTradeRoute::getSourceCityNameKey() const
{
	return (m_pTradeRoute ? m_pTradeRoute->getSourceCityNameKey() : L"");
}

std::wstring CyTradeRoute::getDestinationCityNameKey() const
{
	return (m_pTradeRoute ? m_pTradeRoute->getDestinationCityNameKey() : L"");
}

std::wstring CyTradeRoute::getSourceCityName() const
{
	return gDLL->getObjectText(m_pTradeRoute ? m_pTradeRoute->getSourceCityNameKey() : L"", 0);
}

std::wstring CyTradeRoute::getDestinationCityName() const
{
	return gDLL->getObjectText(m_pTradeRoute ? m_pTradeRoute->getDestinationCityNameKey() : L"", 0);
}

PlayerTypes CyTradeRoute::getOwner() const
{
	return m_pTradeRoute ? m_pTradeRoute->getOwner() : NO_PLAYER;
}

TradeTypes CyTradeRoute::getTradeType(/*YieldTypes*/int iYield) const
{
	return (m_pTradeRoute ? m_pTradeRoute->getTradeType((YieldTypes)iYield) : NO_TRADE);
}

OptionInfo CyTradeRoute::getRadioOption(/*YieldTypes*/int iYield, /*TradeRouteRadioTypes*/int iTradeRouteRadioType) const
{
	return (m_pTradeRoute ? m_pTradeRoute->getRadioOption((YieldTypes)iYield, (TradeRouteRadioTypes)iTradeRouteRadioType) : OptionInfo());
}

AdvancedOptionInfo CyTradeRoute::getEuropeanRadioOption(/*YieldTypes*/int iYield, /*TradeRouteRadioEuropeanTypes*/int iTradeRouteRadioEuropeanType) const
{
	return (m_pTradeRoute ? m_pTradeRoute->getEuropeanRadioOption((YieldTypes)iYield, (TradeRouteRadioEuropeanTypes)iTradeRouteRadioEuropeanType) : AdvancedOptionInfo());
}

OptionInfo CyTradeRoute::getCheckBoxOption(/*YieldTypes*/int iYield, /*TradeRouteCheckBoxTypes*/int iTradeRouteCheckBoxType) const
{
	return (m_pTradeRoute ? m_pTradeRoute->getCheckBoxOption((YieldTypes)iYield, (TradeRouteCheckBoxTypes)iTradeRouteCheckBoxType) : OptionInfo());
}

int CyTradeRoute::getTransportUnitsCapacity() const
{
	return m_pTradeRoute ? m_pTradeRoute->getTransportUnitsCapacity() : -1;
}
int CyTradeRoute::getYieldValue(/*YieldTypes*/int iYield) const
{
	return m_pTradeRoute ? m_pTradeRoute->getYieldValue(((YieldTypes)iYield)) : -1;
}
int CyTradeRoute::getLoadedCargoPercent() const
{
	return m_pTradeRoute ? m_pTradeRoute->getLoadedCargoPercent() : -1;
}
int CyTradeRoute::getLoadedCargoHistPercent() const
{
	return m_pTradeRoute ? m_pTradeRoute->getLoadedCargoHistPercent() : -1;
}
int CyTradeRoute::getTradeProfits() const
{
	return m_pTradeRoute ? m_pTradeRoute->getTradeProfits() : -1;
}
int CyTradeRoute::getTradeProfitsHist() const
{
	return m_pTradeRoute ? m_pTradeRoute->getTradeProfitsHist() : -1;
}
unsigned int CyTradeRoute::getTotalProfits() const
{
	return m_pTradeRoute ? m_pTradeRoute->getTotalProfits() : 0;
}
TransportTypes CyTradeRoute::getTransportType() const
{
	return m_pTradeRoute ? m_pTradeRoute->getTransportType() : NO_TRANSPORT;
}